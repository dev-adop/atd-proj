<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/login',function (){
    return redirect('/');
});
Route::get('/join',function (){
    return view('auth/join');
});
Route::get('/forgot',function (){
    return view('auth/forgot');
});

Route::get('/header', function () {
    return view('public/header');
});
Route::get('/footer', function () {
    return view('public/footer');
});
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/menu', function () {
        return view('public/menu');
    });
    Route::get('/menu_mobile', function () {
        return view('public/menu_mobile');
    });
    Route::get('/profile', function () {
        return view('public/profile');
    });
    Route::get('/logout', function () {
        return redirect('/');
    });
    Route::get('/myCampaigns', function () {
        return view('ad/myCampaigns');
    });
    Route::get('/express', function () {
        return view('ad/express');
    });
    Route::get('/newCampaignModal', function () {
        return view('ad/newCampaignModal');
    });
    Route::get('/help', function () {
        return view('auth/help');
    });
    Route::get('/info', function () {
        return view('auth/info');
    });

    Route::get('/billing', function () {
        return view('auth/billing');
    });
});