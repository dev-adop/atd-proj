(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["onboarding"], {
    "030b": function (e, t, a) {
    }, "04d0": function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("1a2d"), i = a("1585"), s = a("5670"), o = a("ebb6"), r = a("b047"), c = a.n(r);
            a("7d8e");
            t["a"] = {
                props: {
                    options: {
                        type: Array, default: function () {
                            return []
                        }
                    }, selected: {
                        type: Object, default: function () {
                            return {}
                        }
                    }, type: {type: String, default: "schedule"}, inputValue: String | Number
                }, render: function () {
                    var e = arguments[0];
                    return e("div", {class: "onboarding-radio-group"}, [this.options.map(this.renderBox)])
                }, watch: {
                    _inputValue: {
                        handler: c()(function (e) {
                            var t = document.createElement("span");
                            t.classList.add("onboarding-length-tester-span"), t.innerText = e, document.body.appendChild(t);
                            var a = this.windowDimension.width < 800 ? 5 : 20;
                            this.$refs.input.style.width = t.offsetWidth + a + "px", t.parentNode.removeChild(t)
                        }, 200), immediate: !0
                    }
                }, mounted: function () {
                    var t = this,
                        a = this.$watchAsObservable("_value", {immediate: !0}).pipe(Object(n["a"])(100), Object(i["a"])("newValue"), Object(s["a"])(e.equals("custom")), Object(o["a"])(function () {
                            return t.$refs.input
                        }), Object(s["a"])(e.identity));
                    this.$subscribeTo(a, function (e) {
                        e.focus()
                    })
                }, computed: {
                    _value: function () {
                        return "fixed" === this.selected.type ? this.selected.value : "custom"
                    }, _inputValue: {
                        get: e.prop("inputValue"), set: function (e) {
                            this.$emit("inputValue", e)
                        }
                    }
                }, methods: {
                    renderBox: function (e) {
                        var t = this, a = e.left, n = e.value, i = e.hasInput, s = void 0 !== i && i, o = e.banner,
                            r = void 0 === o ? "" : o, c = this.$createElement, l = this.isSelected(n, s),
                            u = "budget" === this.type, p = ["radio-box"];
                        return l && p.push("selected"), r && p.push("hasBanner"), s && p.push("hasInput"), c("div", {class: p.join(" ")}, [c("b-radio", {
                            attrs: {
                                value: this._value,
                                "native-value": n
                            }, on: {
                                input: function () {
                                    return t.handleClick(n, s ? "custom" : "fixed")
                                }
                            }
                        }, [s ? c("div", {class: "box--contents input-type ".concat(u ? "budget" : "")}, [u && this.getCurrencySymbol(), c("input", {
                            attrs: {type: "number"},
                            domProps: {value: this.inputValue},
                            ref: "input",
                            on: {
                                input: function (e) {
                                    return t._inputValue = e.target.value
                                }, focus: function () {
                                    return !l && t.handleClick(n, "custom")
                                }, blur: function () {
                                    return t.$emit("blur")
                                }
                            }
                        }), u ? " / ".concat(this.$t("PROPOSAL.price_day")) : ""]) : c("div", {class: "box--contents"}, [this.renderBanner(r), c("span", {class: "box--left"}, [a])])])])
                    }, isSelected: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                        return "fixed" === this.selected.type ? this.selected.value === e && !t : t
                    }, renderBanner: function (e) {
                        var t = this.$createElement;
                        return e ? t("div", {class: "banner"}, [t("span", {class: "banner--text"}, [e])]) : null
                    }, handleClick: function (e, t) {
                        "custom" === t && (e = this.inputValue), e = Number(e), this.$emit("input", {type: t, value: e})
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "09ea": function (e, t, a) {
    }, "0b23": function (e, t, a) {
        "use strict";
        (function (e) {
            a("7f7f");
            var n = a("2638"), i = a.n(n), s = a("cebc"), o = a("a745"), r = a.n(o),
                c = (a("6762"), a("2fdb"), a("ac6a"), a("7514"), a("aede")), l = a("9c56"), u = a("e25b"),
                p = a("dde5"), d = a("4c6b"), h = a("fa7d"), f = a("c2e3"), v = a("0a7c"), m = a("ad27"), b = a("8fda"),
                g = a("3d93"), y = a("a1f6"), O = a("938b"), _ = a("34b0"), x = a("08fe");

            function w() {
                var e = Object(c["a"])(["\n    @media screen and (max-width: 800px) {\n        width: 100%;\n        height: auto;\n        min-height: 210px;\n    }\n    .card-view {\n        height: 210px;\n        width: 325px;\n        margin: 5px;\n        border: 1px solid #999;\n        .card-view-header {\n            background-color: #fff;\n            box-shadow: none;\n        }\n        @media screen and (max-width: 800px) {\n            width: 100%;\n            min-height: 190px;\n            height: auto;\n            margin: 5px 0;\n        }\n    }\n    .card-view-body {\n        display: flex;\n        align-items: center;\n        justify-content: center;\n    }\n"]);
                return w = function () {
                    return e
                }, e
            }

            function k() {
                var e = Object(c["a"])(["\n    width: 100%;\n    display: flex;\n    flex-wrap: wrap;\n    justify-content: center;\n"]);
                return k = function () {
                    return e
                }, e
            }

            var A = l["b"].div(k()), N = l["b"].div(w());
            t["a"] = {
                name: "Audience",
                props: {value: {type: Array, default: e.always([])}},
                components: {
                    Age: f["a"],
                    DeviceTarget: v["a"],
                    GenderGauge: m["a"],
                    Locations: b["a"],
                    Income: g["a"],
                    Interests: y["a"],
                    Relationship: O["a"],
                    Keywords: _["a"]
                },
                computed: {validAudiences: e.compose(e.filter(e.prop("visible")), e.prop("value"))},
                methods: {
                    renderElement: function (t) {
                        var a = this, n = this.$createElement, o = function (e) {
                            switch (e) {
                                case"gender":
                                    return "GenderGauge";
                                case"location":
                                    return "Locations";
                                case"relationshipStatus":
                                    return "Relationship";
                                case"audienceInterests":
                                    return "Interests";
                                case"searchKeywords":
                                    return "Keywords";
                                default:
                                    return Object(h["fb"])(e)
                            }
                        }(t), c = this.value.find(e.propEq("name", t)), l = {
                            attrs: {
                                values: c.values,
                                resetable: !1,
                                editable: ["Locations", "Interests", "Keywords"].includes(o)
                            }, on: {
                                "update:value": function (e) {
                                    r()(e) ? c.values = c.values.map(function (t, a) {
                                        return Object(s["a"])({}, t, {value: e[a]})
                                    }) : c.values[e.idx].value = e.value, a.$emit("input", a.value)
                                }, "edit:start": function () {
                                    a.modalType = t
                                }, "update:remove": function (e) {
                                    c.values.splice(e, 1), a.$emit("input", a.value)
                                }
                            }
                        };
                        return n(N, [n(o, i()([{}, l]))])
                    }, renderInputsModal: function () {
                        var t = this, a = this.$createElement, n = this.value.find(e.propEq("name", this.modalType)),
                            o = {
                                attrs: Object(s["a"])({
                                    type: "general",
                                    active: Boolean(this.modalType)
                                }, this.modalConfig[this.modalType], {addedValues: n ? n.values : []}),
                                on: {
                                    cancel: function () {
                                        return t.modalType = null
                                    }, confirm: function (e) {
                                        n.values = e, t.modalType = null, t.$emit("input", t.value)
                                    }
                                }
                            };
                        return a(x["a"], i()([{}, o]))
                    }
                },
                data: function () {
                    return {
                        modalType: null,
                        modalConfig: {
                            location: {
                                headerText: this.$t("PROPOSAL.add_location_modal_title"),
                                placeholder: this.$t("PROPOSAL.add_location_modal_placeholder"),
                                addType: "search",
                                nameGetter: e.prop("text"),
                                valueGetter: e.prop("place_id"),
                                searchFunc: p["i"].autocomplete,
                                editable: !1
                            },
                            audienceInterests: {
                                headerText: "Add more interests",
                                placeholder: "Add more interests",
                                addType: "search",
                                nameGetter: e.prop("name"),
                                valueGetter: e.prop("id"),
                                searchFunc: u["a"].getInterests,
                                editable: !1
                            },
                            searchKeywords: {
                                headerText: this.$t("PROPOSAL.add_keywords_modal_title"),
                                placeholder: this.$t("PROPOSAL.add_keywords_modal_placeholder"),
                                addType: "general",
                                nameGetter: e.identity(),
                                valueGetter: e.identity(),
                                inputValidator: e.test(d["p"]),
                                editable: !0
                            }
                        }
                    }
                },
                render: function () {
                    var e = this, t = arguments[0];
                    return t(A, {class: "audience"}, [this.validAudiences.map(function (t) {
                        var a = t.name;
                        return e.renderElement(a)
                    }), this.modalType ? this.renderInputsModal() : ""])
                }
            }
        }).call(this, a("b17e"))
    }, 1268: function (e, t, a) {
    }, "201a": function (e, t, a) {
        "use strict";
        (function (e) {
            a("7f7f"), a("c5f6"), a("dde5");
            var n = a("2d2d"), i = a("4a48");
            t["a"] = {
                name: "Objectives",
                components: {Tags: n["a"], ObjectiveSelector: i["a"]},
                props: {
                    value: {
                        type: Array, default: function () {
                            return []
                        }
                    }, campaign: {type: Object, default: e.always({})}, campaignId: String | Number
                },
                data: function () {
                    return {selectedObjective: ""}
                },
                computed: {
                    selectedObjectives: {
                        get: e.prop("value"), set: function (e) {
                            this.$emit("input", e)
                        }
                    }
                },
                methods: {
                    setSelector: function (e) {
                        this.$emit("input", e)
                    }, valueRemover: function (e) {
                        this.selectedObjectives.splice(e, 1)
                    }, valueGetter: function () {
                        var e, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        return e = "LINK" === t.type ? t.value : t.name, "ga" === t.analyticType ? e += " (Google)" : "pixel" === t.analyticType && (e += " (Facebook)"), e
                    }
                }
            }
        }).call(this, a("b17e"))
    }, 2379: function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "onboarding-website-visitors"}, [e.isTrackerModalOpen ? a("tracker-modal", {
                    attrs: {
                        headerText: "'Select Google Analytics account'",
                        active: e.isTrackerModalOpen
                    }, on: {
                        "update:active": function (t) {
                            e.isTrackerModalOpen = t
                        }, confirm: e.handleGa
                    }
                }) : e._e(), e.isPixelModalOpen ? a("pixel-modal", {
                    attrs: {
                        active: e.isPixelModalOpen,
                        campaignIdProp: e.campaignId
                    }, on: {
                        "update:active": function (t) {
                            e.isPixelModalOpen = t
                        }, confirm: e.handlePixel
                    }
                }) : e._e(), e.noTools ? a("div", {staticClass: "visitors-no-tools"}, [a("no-content", [a("span", {
                    staticClass: "ment-no-tools word-break",
                    domProps: {innerHTML: e._s(e.$t("PROPOSAL.ap_card_no_tracker"))}
                })])], 1) : a("div", {
                    staticClass: "visitors-has-tools",
                    class: {both: e.google && e.pixel}
                }, [e.google ? a("div", {staticClass: "tracker-container google"}, [a("span", {staticClass: "visitors-ment-guide"}, [e._v(e._s(e.$t("PROPOSAL.ap_card_tracker_info")))]), a("div", [a("span", {staticClass: "page-elem-box"}, [a("span", {staticClass: "adriel-ellipsis"}, [a("img", {attrs: {src: "/img/proposal/login_google_icon.png"}}), a("span", {staticClass: "page-name"}, [e._v(e._s(e.google.name || "-"))]), a("span", {staticClass: "page-url adriel-ellipsis"}, [e._v(e._s(" | " + (e.google.websiteUrl || "-") + " " + (e.google.propertyId || e.google.id || "-")))])]), a("img", {
                    staticClass: "delete-icon",
                    attrs: {src: "/img/proposal/profile_delete_icon_dark.png"},
                    on: {
                        click: function (t) {
                            return e.handleRemove("googleAnalytics")
                        }
                    }
                })])])]) : e._e(), e.pixel ? a("div", {staticClass: "tracker-container pixel"}, [a("span", {staticClass: "visitors-ment-guide"}, [e._v(e._s(e.$t("PROPOSAL.ap_card_tracker_pixel_info")))]), a("div", [a("span", {staticClass: "page-elem-box"}, [a("span", {staticClass: "adriel-ellipsis"}, [a("img", {attrs: {src: "/img/proposal/facebook_icon.png"}}), a("span", {staticClass: "page-name"}, [e._v(e._s(e.pixel.name || "-"))]), a("span", {staticClass: "page-url adriel-ellipsis"}, [e._v(e._s(" |  " + (e.pixel.propertyId || e.pixel.id || "-")))])]), a("img", {
                    staticClass: "delete-icon",
                    attrs: {src: "/img/proposal/profile_delete_icon_dark.png"},
                    on: {
                        click: function (t) {
                            return e.handleRemove("facebookPixel")
                        }
                    }
                })])])]) : e._e()]), a("div", {staticClass: "tracker-buttons"}, [e.google ? e._e() : [e.noTools ? e._e() : a("div", {staticClass: "visitors-tooltip"}, [a("span", [e._v(e._s(e.$t("PROPOSAL.ap_card_tracker_google_content")))])]), a("button", {
                    staticClass: "auth-btn google",
                    on: {click: e.handleLogin}
                }, [a("img", {attrs: {src: "/img/proposal/login_google_icon.png"}}), a("span", [e._v(e._s(e.$t("PROPOSAL.ap_card_login_google_tracker")))])])], e.hasFb && !e.pixel ? [e.noTools ? e._e() : a("div", {staticClass: "visitors-tooltip"}, [a("span", [e._v(e._s(e.$t("PROPOSAL.ap_card_tracker_pixel_content")))])]), a("button", {
                    staticClass: "auth-btn pixel",
                    on: {click: e.handlePixelLogin}
                }, [a("img", {attrs: {src: "/img/proposal/login_pixel_icon.png"}}), a("span", [e._v(e._s(e.$t("PROPOSAL.ap_card_login_pixel_tracker")))])])] : e._e()], 2)], 1)
            }, i = [], s = a("3a6c"), o = s["a"], r = (a("a234"), a("96d6"), a("2877")),
            c = Object(r["a"])(o, n, i, !1, null, null, null);
        t["a"] = c.exports
    }, "28c8": function (e, t, a) {
        "use strict";
        (function (e) {
            a("7514");
            var n = a("cebc"), i = a("2638"), s = a.n(i), o = a("c1df"), r = a.n(o), c = a("4179"), l = a("2029"),
                u = a("dde5"), p = a("51c6"), d = a("4379"), h = a("fa7d"), f = a("a772"), v = (a("73c7"), a("2f62")),
                m = v.mapState;
            t["a"] = {
                name: "SummaryAndPayment",
                props: {
                    value: {type: Object}, campaign: {
                        type: Object, default: function () {
                            return {}
                        }
                    }, data: {type: Object}
                },
                data: function () {
                    return {
                        isCouponsModalActive: !1,
                        isBizNoModalActive: !1,
                        couponAssigned: 0,
                        couponAmount: 0,
                        coupons: []
                    }
                },
                render: function () {
                    var t = this, a = arguments[0], n = "";
                    if (this.isCouponsModalActive) {
                        var i = {
                            on: {
                                "update:add": this.addCoupon,
                                "update:bind": this.bindCoupons,
                                "update:active": function (e) {
                                    t.isCouponsModalActive = e
                                }
                            },
                            attrs: {
                                active: this.isCouponsModalActive,
                                coupons: this.coupons,
                                campaignId: this.campaignId
                            }
                        };
                        n = a(l["a"], s()([{}, i]))
                    }
                    var o = "";
                    if (this.isBizNoModalActive) {
                        var r = {
                            attrs: {active: !0, hasButtons: !1, headerText: this.$t("VAT.submit_business_no")},
                            on: {
                                "update:active": function (e) {
                                    return t.isBizNoModalActive = e
                                }
                            }
                        };
                        o = a(p["a"], s()([{}, r]), [a(d["a"], {
                            attrs: {type: "modal-type"}, on: {
                                cancel: function () {
                                    return t.isBizNoModalActive = !1
                                }, complete: function () {
                                    return t.isBizNoModalActive = !1
                                }
                            }
                        })])
                    }
                    return a("div", {class: "onboarding-summary-container"}, [a("div", {class: "summary-part"}, [e.prop("websiteUrl", this.getData("webOrApp")) && "web" === e.prop("type", this.getData("webOrApp")) ? a("div", {class: "data-group clickable"}, [a("label", [this.$t("ONBOARDING.summary_website")]), a("span", {
                        class: "data-value",
                        style: {"word-break": "break-all"}
                    }, [this.getData("webOrApp").websiteUrl, a("span", {class: "icons"}, [this.renderLinkIcon(this.getData("webOrApp").websiteUrl), this.renderPenIcon("webOrApp")])])]) : "", this.getData("webOrApp") && "app" == e.prop("type", this.getData("webOrApp")) ? a("div", {class: "data-group clickable mobile-selected"}, [a("label", [this.$t("ONBOARDING.summary_mobile_app")]), a("span", {class: "data-value"}, [a("div", [this.$t("ONBOARDING.app_url_app_store"), " :", " ", this.getData("webOrApp")["appStoreUrl"], a("span", {class: "icons"}, [this.renderLinkIcon(this.getData("webOrApp")["appStoreUrl"]), this.renderPenIcon("webOrApp")])]), a("br"), a("div", [this.$t("ONBOARDING.app_url_play_store"), " :", " ", this.getData("webOrApp")["playStoreUrl"], a("span", {class: "icons"}, [this.renderLinkIcon(this.getData("webOrApp")["playStoreUrl"]), this.renderPenIcon("webOrApp")])]), a("br"), a("div", [this.$t("ONBOARDING.web_or_app_developer_site"), " ", ":", " ", this.getData("webOrApp")["developerSiteUrl"], a("span", {class: "icons"}, [this.renderLinkIcon(this.getData("webOrApp")["developerSiteUrl"]), this.renderPenIcon("webOrApp")])])])]) : "form" == e.prop("type", this.getData("webOrApp")) ? a("div", {class: "data-group clickable mobile-selected"}, [a("label", [this.$t("FORM.summary_url_title")]), a("span", {class: "data-value"}, [a("div", [this.getUrl(), a("span", {class: "icons"}, [this.renderLinkIcon(this.getUrl()), this.renderPenIcon("form")])])])]) : "", this.showData("locations") ? a("div", {class: "data-group clickable"}, [a("label", [this.$t("ONBOARDING.summary_locations")]), a("span", {class: "data-value"}, [(this.data["locations"].value || []).map(e.prop("text")).join(", "), a("span", {class: "icons"}, [this.renderPenIcon("locations")])])]) : "", a("div", {class: "data-group ads"}, [a("label", [this.$t("ONBOARDING.summary_ads")]), this.renderAdTypes()]), this.showData("schedule") && a("div", {class: "data-group clickable"}, [a("label", [this.$t("ONBOARDING.summary_schedule")]), this.data["schedule"].value.continuously ? a("span", {class: "data-value"}, [this.$t("ONBOARDING.schedule_first_box_title"), a("span", {class: "icons"}, [this.renderPenIcon("schedule")])]) : a("span", {class: "data-value"}, [this.dateFormat(this.data["schedule"].value.startDate), " ~ ", this.dateFormat(this.data["schedule"].value.endDate), a("span", {class: "icons"}, [this.renderPenIcon("schedule")])])])]), a("hr"), a("div", {class: "price-part"}, [a("div", {class: "mb-10"}, [a("p", {class: "description"}, [this.$t("PROPOSAL.price_total_tooltip")])]), a("div", {class: "price-details"}, [a("div", {class: "row"}, [a("span", {class: "adriel-flex-center"}, [this.$t("PROPOSAL.price_platform_fee"), a("info-tooltip", {attrs: {label: this.$t("PROPOSAL.price_platform_fee_tooltip")}})]), a("span", [this.$t("ONBOARDING.budget_radio_format", {value: this.formatCurrency(this.budget)})])]), a("div", {class: "row"}, [a("span", {class: "adriel-flex-center"}, [this.$t("ONBOARDING.summary_service_fee"), a("info-tooltip", {attrs: {label: this.$t("PROPOSAL.price_service_fee_tooltip")}})]), a("span", [this.$t("ONBOARDING.budget_radio_format", {value: this.formatCurrency(this.fee)})])]), this.maybeKorean && a("div", {class: "row vat"}, [a("span", {class: "adriel-flex-center"}, [this.$t("VAT.vat"), a("button", {
                        on: {
                            click: function () {
                                return t.isBizNoModalActive = !0
                            }
                        }
                    }, [this.$t("VAT.vat_qualify")])]), a("span", [this.vatText])]), a("div", {class: "row"}, [a("span", {class: "adriel-flex-center"}, [this.$t("ONBOARDING.summary_budget_total"), a("info-tooltip", {attrs: {label: this.$t("PROPOSAL.price_total_tooltip")}})]), a("span", [this.$t("ONBOARDING.budget_radio_format", {value: this.formatCurrency(this.total)})])]), a("div", {class: "row credit"}, [a("span", [this.$t("ONBOARDING.summary_credit")]), a("span", {class: "right"}, [a("span", {class: "credit-used"}, [this.formatCurrency(this.couponAssigned)]), a("span", {class: "credit-total"}, [" ", "/ ", this.formatCurrency(this.couponAmount)]), a("button", {
                        on: {
                            click: function () {
                                return t.openCouponModal()
                            }
                        }
                    }, [this.$t("ONBOARDING.summary_coupon")])])])])]), a(c["a"], {attrs: {deletable: !1}}), o, n])
                },
                computed: Object(n["a"])({}, m("form", ["formCreate"]), {
                    bizNo: e.path(["globalUser", "business_no"]),
                    budget: function () {
                        return e.path(["budget", "value", "value"])(this.data) || 0
                    },
                    fee: function () {
                        return this.budget * this.serviceFee
                    },
                    total: function () {
                        return this.budget + this.fee + this.vat
                    },
                    serviceFee: function () {
                        return this.campaign.service_fee / 100
                    },
                    blockedAmount: function () {
                        return 3 * this.total
                    },
                    comments: function () {
                        return e.path(["summary", "value", "comments"])(this.data)
                    },
                    vat: function () {
                        return !this.maybeKorean || this.bizNo ? 0 : .1 * (this.fee + this.budget)
                    },
                    vatText: function () {
                        return this.$t("ONBOARDING.budget_radio_format", {value: this.formatCurrency(this.vat)})
                    },
                    campaignId: e.path(["campaign", "id"])
                }),
                methods: {
                    renderAppLink: function (e) {
                        var t = this.$createElement, a = this.getData("webOrApp")[e];
                        return t("a", {attrs: {href: a, target: "_blank", rel: "noopener noreferrer"}}, [a])
                    }, findAdFromType: function (e) {
                        return this.data["ads"].find(function (t) {
                            var a = t.value;
                            switch (e) {
                                case"facebookFeed":
                                    return Object(f["n"])(a);
                                case"instagramFeed":
                                    return Object(f["u"])(a);
                                case"googleSearch":
                                    return Object(f["r"])(a);
                                case"googleDisplay":
                                    return Object(f["q"])(a);
                                case"googleUniversalApp":
                                    return Object(f["t"])(a);
                                case"appleSearch":
                                    return Object(f["m"])(a)
                            }
                        })
                    }, renderAdTypes: function () {
                        var t = this, a = this.$createElement,
                            n = e.pipe(e.pathOr([], ["data", "adTypes", "value"]), e.filter(e.pipe(this.findAdFromType, e.path(["value", "enabled"]))), e.map(function (e) {
                                return a("span", {class: "clickable"}, [a("span", {class: "text"}, [t.$t("ONBOARDING.ad_types_type_".concat(e))]), " "])
                            }))(this);
                        return a("span", {class: "data-value"}, [n, a("span", {class: "icons"}, [this.renderPenIcon("adsList")])])
                    }, showData: function (e) {
                        var t = this.data[e];
                        return !(!t || t.skip) && !(!t.value || "N" === t.value.value)
                    }, getData: function (t) {
                        return this.showData(t) ? e.path(["data", t, "value"], this) : null
                    }, goToKey: function (e) {
                        this.$emit("go", {key: e})
                    }, dateFormat: function (e) {
                        return r()(e).format("YYYY-MM-DD")
                    }, getAvailableCoupons: function () {
                        var t = this;
                        return u["d"].coupons(this.campaignId).then(function () {
                            var a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            t.coupons = e.compose(e.reverse, e.sortBy(e.prop("created_at")))(a)
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, openCouponModal: function () {
                        this.getAvailableCoupons(), this.isCouponsModalActive = !0
                    }, getAvailableCouponAmount: function () {
                        var e = this;
                        u["d"].availableCouponAmount(this.campaignId).then(function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            e.couponAssigned = t.amountAssigned, e.couponAmount = t.totalAmount
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, addCoupon: function (e) {
                        var t = this;
                        u["e"].consume(e).then(function () {
                            t.getAvailableCoupons().then(function () {
                                var a = t.coupons.find(function (t) {
                                    return t.code === e
                                });
                                t.bindCoupons([a.id])
                            })
                        }).catch(function () {
                            t.toast(t.$t("PROPOSAL.failed_to_register_coupon"))
                        })
                    }, bindCoupons: function (e) {
                        var t = this;
                        u["d"].bindCoupons(this.campaignId, e).then(function () {
                            t.getAvailableCouponAmount(), t.getAvailableCoupons()
                        })
                    }, renderPenIcon: function (e) {
                        var t = this, a = this.$createElement;
                        return a("b-icon", {
                            attrs: {pack: "far", icon: "pen"},
                            class: "pen-icon",
                            nativeOn: {
                                click: function () {
                                    return t.goToKey(e)
                                }
                            }
                        })
                    }, renderLinkIcon: function (e) {
                        var t = this.$createElement;
                        return t("b-icon", {
                            attrs: {pack: "fas", icon: "link"},
                            class: "link-icon",
                            nativeOn: {
                                click: function () {
                                    return window.open(Object(h["b"])(e), "_blank")
                                }
                            }
                        })
                    }, getUrl: function () {
                        return "".concat("https://form.adriel.ai", "/").concat(this.campaign.id)
                    }
                },
                watch: {
                    campaignId: {
                        handler: function (e) {
                            void 0 !== e && this.getAvailableCouponAmount()
                        }, immediate: !0
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "2dc3": function (e, t, a) {
        "use strict";
        var n = function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return a("ObjectiveSelector", {
                attrs: {value: e.value, campaignId: e.campaignId},
                on: {input: e.setSelector}
            }, [a("tags", {
                staticClass: "objectives-tags",
                attrs: {
                    slot: "tags",
                    values: e.selectedObjectives,
                    "value-getter": e.valueGetter,
                    "max-width": "265",
                    mainColor: "",
                    deleteImgSrc: "/img/proposal/profile_delete_icon_dark.png"
                },
                on: {"update:remove": e.valueRemover},
                slot: "tags"
            })], 1)
        }, i = [], s = a("201a"), o = s["a"], r = a("2877"), c = Object(r["a"])(o, n, i, !1, null, null, null);
        t["a"] = c.exports
    }, "2fd4": function (e, t, a) {
        "use strict";
        var n = a("cebc"), i = a("aede"), s = a("9c56"), o = a("2f62");

        function r() {
            var e = Object(i["a"])(["\n    position: fixed;\n    right: 35px;\n    top: 45px;\n    z-index: 2;\n    i {\n        font-size: 40px;\n        color: #cccccc;\n        cursor: pointer;\n        &:hover {\n            color: #999;\n        }\n    }\n    @media screen and (max-width: 800px) {\n        &.isWeb {\n            display: none;\n        }\n        top: 19px;\n        right: 10px;\n    }\n    @media screen and (min-width: 801px) {\n        &.isMobile {\n            display: none;\n        }\n    }\n"]);
            return r = function () {
                return e
            }, e
        }

        var c = s["b"].div(r());
        t["a"] = {
            name: "CloseButton",
            props: {className: {type: String, default: ""}, isFormType: Boolean},
            methods: Object(n["a"])({}, Object(o["mapActions"])("form", ["clearData"]), {
                draftCampaign: function () {
                    this.isFormType && this.clearData(["formCreate"]), this.pushToRoute("/myCampaigns")
                }
            }),
            render: function () {
                var e = this, t = arguments[0];
                return t(c, {class: this.className}, [t("i", {
                    class: "fal fa-times",
                    on: {
                        click: function () {
                            return e.draftCampaign()
                        }
                    },
                    directives: [{
                        name: "tooltip",
                        value: "isWeb" == this.className && this.$t("ONBOARDING.close_button")
                    }]
                })])
            }
        }
    }, 3812: function (e, t, a) {
        "use strict";
        var n = a("09ea"), i = a.n(n);
        i.a
    }, "3a6c": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("cebc"), i = (a("7514"), a("c5f6"), a("4a1d")), s = a("3de4"), o = a("f754"), r = a("8f6b"),
                c = a("fa7d"), l = a("2f62"), u = a("d3fb"), p = a("d96a"), d = a("d337");
            t["a"] = {
                mixins: [s["a"]],
                data: function () {
                    return {isTrackerModalOpen: !1, isPixelModalOpen: !1}
                },
                props: {
                    value: {
                        type: Array, default: function () {
                            return []
                        }
                    }, campaignId: String | Number, hasFb: Boolean
                },
                computed: {
                    google: e.compose(e.find(e.propEq("type", "googleAnalytics")), e.prop("_value")),
                    pixel: e.compose(e.find(e.propEq("type", "facebookPixel")), e.prop("_value")),
                    noTools: function () {
                        return !Object(c["N"])(this.google) && !Object(c["N"])(this.pixel)
                    },
                    _value: {
                        get: e.prop("value"), set: function (e) {
                            this.$emit("input", e)
                        }
                    }
                },
                components: {TrackerModal: o["a"], PixelModal: r["a"]},
                methods: Object(n["a"])({}, Object(l["mapMutations"])("user", ["setGaInfo", "setPixelInfo"]), Object(l["mapActions"])("user", ["fetchFacebookProfile"]), {
                    handleGa: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = t.properties;
                        this.isTrackerModalOpen = !1;
                        var s = this._value.filter(e.propEq("type", "facebookPixel"));
                        s.push(Object(n["a"])({type: "googleAnalytics"}, a[0])), this._value = s;
                        var o = ["analytics2-4@seventh-country-251404.iam.gserviceaccount.com", "adrielanalytics2@gmail.com"];
                        a[0] && i["a"].updateGa(this.campaignId, {
                            emails: o,
                            accountId: a[0].accountId,
                            properties: a
                        }).catch(function (e) {
                            return console.error("cannot update ga", e)
                        })
                    }, handlePixel: function (t) {
                        this.isPixelModalOpen = !1;
                        var a = this._value.filter(e.propEq("type", "googleAnalytics"));
                        a.push(Object(n["a"])({type: "facebookPixel"}, t)), this._value = a
                    }, handleLogin: function () {
                        var e = this;
                        this.isDemo ? this.setDemoAccountModalActive(!0) : Object(c["v"])().then(function (t) {
                            e.setGaInfo(t), e.isTrackerModalOpen = !0
                        }).catch(function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            console.log(t), 403 === t.status && (e.setGaInfo(), e.$emit("edit:start"))
                        })
                    }, handlePixelLogin: function () {
                        var e = this;
                        this.isDemo ? this.setDemoAccountModalActive(!0) : Object(u["a"])(Object(c["u"])()).pipe(Object(p["a"])(function () {
                            return e.isPixelModalOpen = !0
                        }), Object(d["a"])(2e3)).subscribe(this.fetchFacebookProfile, function () {
                            console.log("rejected")
                        })
                    }, getTooltip: function (e) {
                        if (!this.noTools) {
                            var t, a = {
                                show: !0,
                                autoHide: !1,
                                hideOnTargetClick: !1,
                                trigger: "manual",
                                placement: "top",
                                classes: "tooltip adriel-tooltip tracker-tooltip",
                                offset: 10
                            };
                            return t = "google" === e ? this.$t("PROPOSAL.ap_card_tracker_google_content") : this.$t("PROPOSAL.ap_card_tracker_pixel_content"), Object(n["a"])({}, a, {content: t})
                        }
                    }, handleRemove: function (t) {
                        this._value = e.reject(e.propEq("type", t), this._value)
                    }
                })
            }
        }).call(this, a("b17e"))
    }, "3d65": function (e, t, a) {
        "use strict";
        var n = a("cebc"), i = a("2f62");
        a("a835");
        t["a"] = {
            name: "Finish",
            props: {type: {type: String, default: "web"}},
            methods: Object(n["a"])({}, Object(i["mapActions"])("form", ["clearData"]), {
                onClickConfirm: function () {
                    "form" === this.type && this.clearData(["formCreate"]), this.pushToRoute("/myCampaigns")
                }
            }),
            render: function () {
                var e = this, t = arguments[0];
                return t("div", {class: "onboarding-finish"}, [t("div", {class: "onboarding-finish--title"}, [this.$t("ONBOARDING.finish_title")]), t("div", {class: "onboarding-finish--content"}, [this.$t("ONBOARDING.finish_content")]), t("div", {class: "adriel-flex-center onboarding-finish--buttons"}, [t("span", {
                    on: {
                        click: function () {
                            return e.$emit("prev")
                        }
                    }
                }, [this.$t("ONBOARDING.finish_go_back")]), t("button", {on: {click: this.onClickConfirm}}, [this.$t("ONBOARDING.finish_confirm")])])])
            }
        }
    }, "3d6e": function (e, t, a) {
    }, "40f9": function (e, t, a) {
        "use strict";
        (function (e) {
            a("96cf");
            var n = a("3b8d"), i = (a("7514"), a("2638")), s = a.n(i), o = (a("ac6a"), a("75fc")), r = a("cebc"),
                c = a("aede"), l = a("9c56"), u = a("c4cc"), p = a("4677"), d = a("ebb6"), h = a("5670"), f = a("d792"),
                v = a("dde5"), m = a("da7a"), b = a("2d2d"), g = a("4c6b"), y = a("fa7d"), O = a("b047"), _ = a.n(O);

            function x() {
                var e = Object(c["a"])(["\n            input {\n                border-color: #ef646f;\n                &:focus,\n                &:active {\n                    outline-color: #ef646f;\n                    border-color: #ef646f;\n                    box-shadow: none;\n                    outline-offset: -2px;\n                    outline-style: auto;\n                    outline-width: 5px;\n                }\n            }\n            button {\n                position: absolute;\n                z-index: 1;\n                top: 0px;\n                height: 100%;\n                right: 2px;\n                .fas {\n                    cursor: pointer;\n                    font-size: 20px;\n                    color: #ef646f;\n                }\n            }\n            @media screen and (max-width: 800px) {\n                button {\n                    .fas {\n                        font-size: 16px;\n                    }\n                }\n            }\n        "]);
                return x = function () {
                    return e
                }, e
            }

            function w() {
                var e = Object(c["a"])(["\n    position: relative;\n    flex-grow: 1;\n    padding: 0 5px;\n    width: 185px;\n    display: flex;\n    .control {\n        align-self: center;\n        width: 100%;\n        padding: 1px 0;\n    }\n    input {\n        height: 35px;\n        background-color: #ffffff;\n        border: 0;\n        border-radius: 0px;\n        box-shadow: none;\n        ", "\n        padding: 0;\n        line-height: 36px;\n        &:focus,\n        &:active {\n            box-shadow: none;\n        }\n        &::placeholder {\n            opacity: 0.7;\n            font-size: 16px;\n            font-weight: normal;\n            letter-spacing: normal;\n            color: rgba(51, 51, 51, 0.4);\n            ", "\n        }\n    }\n    .enter-icon-wrapper {\n        width: 30px;\n        height: 30px;\n        border: solid 1px #e6e5ea;\n        background-color: #f7f7f7;\n        cursor: pointer;\n        border-radius: 50%;\n        transition: background-color 0.2s;\n        &:hover {\n            background-color: #f0efef;\n        }\n    }\n\n    .v-spinner {\n        top: 12px;\n        right: 5px;\n        position: absolute;\n    }\n\n    ", "\n\n    @media screen and (max-width: 800px) {\n        input {\n            height: 35px;\n        }\n        .v-spinner {\n            top: 6px;\n            right: 5px;\n        }\n    }\n"]);
                return w = function () {
                    return e
                }, e
            }

            function k() {
                var e = Object(c["a"])(["\n            border: solid 2px #5aabe3;\n            box-shadow: 0 0 10px 0 rgba(90, 171, 227, 0.5);\n        "]);
                return k = function () {
                    return e
                }, e
            }

            function A() {
                var e = Object(c["a"])(["\n    width: 100%;\n    border: solid 1px #999999;\n    ", "\n    border-radius: 3px;\n    display: flex;\n    flex-wrap: wrap;\n    .tags-container.keywords-tags {\n        margin: 0;\n        padding: 0 5px;\n        width: 100%;\n        display: flex;\n        flex-wrap: wrap;\n        .tags-element {\n            margin: 5px 3.5px;\n            color: #ffffff;\n            height: 25px;\n            padding: 0 10px;\n            border-radius: 45px;\n            display: flex;\n            align-items: center;\n            font-size: 12px;\n            max-width: 100%;\n            background-color: #5aabe3;\n            height: 30px;\n            .tags-text {\n                ", "\n                overflow: hidden;\n                text-overflow: ellipsis;\n                white-space: nowrap;\n                height: 100%;\n                font-size: 14px;\n                line-height: 31px;\n                margin: 5px 3.5px;\n            }\n        }\n    }\n    .tags-container.placeholder-tags {\n        .tags-element {\n            background-color: #fff;\n            color: #5aabe3;\n            border: 1.5px solid #5aabe3;\n\n            .tags-text {\n                font-family: $font-rubik;\n                font-weight: 600;\n                font-size: 14px;\n                line-height: 29px;\n            }\n        }\n    }\n    @media screen and (max-width: 800px) {\n        .tags-container.locations-tags {\n            .tags-element {\n                .tags-text {\n                    font-size: 12px;\n                }\n            }\n        }\n    }\n"]);
                return A = function () {
                    return e
                }, e
            }

            function N() {
                var e = Object(c["a"])(["\n    font-size: 16px;\n    font-weight: bold;\n    color: #333333;\n    margin-bottom: 10px;\n    i {\n        position: relative;\n        color: #5aabe3;\n        top: 2px;\n        left: 4px;\n    }\n    @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {\n        i {\n            top: 7px;\n        }\n    }\n"]);
                return N = function () {
                    return e
                }, e
            }

            function $() {
                var e = Object(c["a"])(["\n    width: 100%;\n    .suggestions {\n        border-radius: 3px;\n        border: solid 1px #dddddd;\n        height: 150px;\n        padding: 10px;\n        margin-top: 10px;\n        overflow-y: scroll;\n        div {\n            display: flex;\n        }\n        h3 {\n            font-size: 14px;\n            color: #333333;\n            margin-bottom: 20px;\n        }\n        i {\n            width: 25px;\n            height: 25px;\n            border-radius: 12.5px;\n            background-color: #eeeeee;\n            line-height: 25px;\n            text-align: center;\n            font-size: 13px;\n            margin-left: 5px;\n            position: relative;\n            cursor: pointer;\n            bottom: 3px;\n        }\n        i:hover {\n            opacity: 0.7;\n        }\n    }\n    .suggestions-tags {\n        .tags-element {\n            border-radius: 15px;\n            border: solid 1px #dddddd;\n            background-color: #f2f4f5;\n            color: #2b80ba;\n            height: 30px;\n            &:hover {\n                background-color: #ddd;\n            }\n            .tags-text {\n                line-height: 27px;\n                font-size: 14px;\n                ", "\n            }\n        }\n    }\n"]);
                return $ = function () {
                    return e
                }, e
            }

            function I() {
                var e = Object(c["a"])(["\n    font-family: 'Roboto', 'Noto Sans', 'Font Awesome 5 Pro', sans-serif;\n"]);
                return I = function () {
                    return e
                }, e
            }

            var j = {focus: Boolean}, D = {focus: Boolean}, T = Object(l["a"])(I()), P = l["b"].div($(), T),
                R = l["b"].div(N()), C = Object(l["b"])("div", j)(A(), function (e) {
                    return e.focus && Object(l["a"])(k())
                }, T), B = Object(l["b"])("div", D)(w(), T, T, function (e) {
                    return e.hasError && Object(l["a"])(x())
                });
            t["a"] = {
                name: "Keyword",
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        var a = t.$watchAsObservable("interestText", {immediate: !0}).pipe(Object(u["a"])(function () {
                            return t.results = []
                        }), Object(p["a"])(250), Object(d["a"])(function () {
                            return t.interestText ? t.interestText.trim() : ""
                        }), Object(h["a"])(e.propSatisfies(e.gt(e.__, 0), "length")), Object(u["a"])(function () {
                            return t.isLoading = !0
                        }), Object(f["a"])(v["p"].getInterests), Object(d["a"])(function (a) {
                            return e.without(t._value, a)
                        }), Object(u["a"])(function () {
                            return t.isLoading = !1
                        }));
                        t.$subscribeTo(a, function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            t.results = e
                        });
                        var n = t.$refs.input;
                        n && n.focus()
                    })
                },
                props: {
                    value: {
                        type: Object,
                        default: e.always({keywords: [], interests: [], suggestions: {keywords: [], interests: []}})
                    }, pending: {type: Boolean, default: !1}, dirty: {type: Boolean, default: !1}
                },
                computed: {
                    _value: {
                        get: e.prop("value"), set: function (e) {
                            this.$emit("input", e)
                        }
                    }, suggestionsKeywords: function () {
                        return e.without(this._value.keywords, this._value.suggestions.keywords)
                    }, suggestionsInterests: function () {
                        return e.without(this._value.interests, this._value.suggestions.interests)
                    }, renderKeywordInput: function () {
                        var e = this, t = this.$createElement, a = void 0 !== this.keywordText ? this.keywordText : "";
                        a = String(a).trim();
                        var n = Object(y["N"])(this._value.keywords) ? this.$t("ONBOARDING.keywords_input_placeholder") : this.$t("ONBOARDING.keywords_input_placeholder_empty_tag");
                        return n = "��".concat(" ", n), t(B, {slot: "input"}, [t("b-input", {
                            attrs: {
                                value: a,
                                placeholder: n
                            }, on: {
                                input: function (t) {
                                    return e.keywordText = t
                                }, focus: function () {
                                    return e.focusKeyword = !0
                                }, blur: function () {
                                    return e.focusKeyword = !1
                                }
                            }, nativeOn: {
                                keyup: function (t) {
                                    return t.keyCode === g["q"].enter && e.addKeyword()
                                }
                            }
                        }), a && t("span", {
                            class: "adriel-flex-center enter-icon-wrapper",
                            on: {click: this.addKeyword}
                        }, [t("img", {
                            class: "enter-icon",
                            attrs: {src: "/img/svg_icons/enter_arrow_symbol.svg"}
                        })])])
                    }, renderInterestInput: function () {
                        var t = this, a = this.$createElement, n = this.interestText,
                            i = Object(y["N"])(this._value.interests) ? this.$t("ONBOARDING.interests_input_placeholder") : this.$t("ONBOARDING.interests_input_placeholder_empty_tag");
                        i = "��".concat(" ", i);
                        var s = "", o = !1;
                        return this.dirty ? o = !0 : this.isLoading && (s = a(m["a"], {
                            attrs: {
                                color: "#999",
                                size: "20px"
                            }
                        })), a(B, {slot: "input", attrs: {hasError: o}}, [a("b-autocomplete", {
                            attrs: {
                                value: n,
                                data: this.results,
                                "custom-formatter": e.prop("name"),
                                "open-on-focus": !0,
                                placeholder: i,
                                "clear-on-select": !0
                            }, on: {
                                input: function (e) {
                                    return t.interestText = e
                                }, select: this.addInterest, focus: function () {
                                    return t.focusInterest = !0
                                }, blur: function () {
                                    return t.focusInterest = !1
                                }
                            }, ref: "searchInput"
                        }), s])
                    }
                },
                render: function () {
                    var e = arguments[0],
                        t = {position: "is-bottom", multilined: !0, size: "is-large", style: "margin-left: 5px;"};
                    return e(P, [e(R, [this.$t("ONBOARDING.search_keywords"), e("b-tooltip", s()([{}, {props: Object(r["a"])({label: this.$t("ONBOARDING.keyword_tooltip")}, t)}]), [e("b-icon", {
                        attrs: {
                            pack: "fas",
                            icon: "question-circle"
                        }
                    })])]), e(C, {attrs: {focus: this.focusKeyword}}, [this.renderTags("keywords")]), this.pending || this.suggestionsKeywords.length > 0 ? this.renderSuggestions("keywords") : "", e("div", {class: "mb-40"}), e(R, [this.$t("ONBOARDING.search_interests"), e("b-tooltip", s()([{}, {props: Object(r["a"])({label: this.$t("ONBOARDING.interest_tooltip")}, t)}]), [e("b-icon", {
                        attrs: {
                            pack: "fas",
                            icon: "question-circle"
                        }
                    })])]), e(C, {attrs: {focus: this.focusInterest}}, [this.renderTags("interests")]), this.pending || this.suggestionsInterests.length > 0 ? this.renderSuggestions("interests") : ""])
                },
                data: function () {
                    return {
                        keywordText: "",
                        interestText: "",
                        results: [],
                        isLoading: !1,
                        keywordTooltip: !1,
                        interestTooltip: !1,
                        focusKeyword: !1,
                        focusInterest: !1
                    }
                },
                methods: {
                    isKeywords: function (e) {
                        return "keywords" === e
                    }, addKeyword: function (t) {
                        "string" != typeof t && (t = "");
                        var a = t || this.keywordText;
                        if (a) {
                            var n = this._value.keywords;
                            -1 === n.indexOf(a) && (this._value = e.over(e.lensProp("keywords"), e.append(a), this._value)), this.keywordText = ""
                        }
                    }, renderTags: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "keywords",
                            a = this.$createElement, n = Object(o["a"])(this._value[t]), i = {
                                attrs: {
                                    className: "keywords-tags",
                                    values: n,
                                    "value-getter": this.isKeywords(t) ? e.identity : e.prop("name"),
                                    "max-width": "100%"
                                }, on: {"update:remove": this.removeFrom(t)}
                            }, r = !0;
                        if (!Object(y["N"])(n)) {
                            var c = this.$t("PROPOSAL.ap_card_let_adriel"), l = this.isKeywords(t) ? c : {name: c};
                            i.attrs.values.push(l), i.attrs.className += " placeholder-tags", r = !1
                        }
                        return a(b["a"], s()([{}, i, {attrs: {removable: r}}]), [this.isKeywords(t) ? this.renderKeywordInput : this.renderInterestInput])
                    }, renderSuggestions: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "keywords",
                            a = this.$createElement;
                        if (this.pending) return a("div", {class: "suggestions"}, [a("h3", [this.$t("ONBOARDING.list_of_suggested_".concat(t))]), a("b-icon", {
                            style: "width:100%;margin-top:20px;",
                            attrs: {pack: "fas", icon: "spinner", size: "is-large", "custom-class": "fa-spin"}
                        })]);
                        var n, i = this._value.suggestions[t];
                        i = e.without(this._value[t], i), n = this.isKeywords(t) ? this.addKeyword : this.addInterest;
                        var o = function (e) {
                            n(i[e])
                        }, r = {
                            attrs: {
                                className: "suggestions-tags",
                                values: i,
                                removable: !1,
                                hasTooltip: !1,
                                "value-getter": this.isKeywords(t) ? e.identity : e.prop("name"),
                                "max-width": "100%"
                            }, on: {click: o}
                        };
                        return a("div", {class: "suggestions"}, [a("div", [a("h3", [this.$t("ONBOARDING.list_of_suggested_".concat(t))]), "interests" === t && a("i", {
                            class: "far fa-redo-alt",
                            on: {click: this.refreshInterestSuggestions}
                        })]), a(b["a"], s()([{}, r]))])
                    }, addInterest: function (t) {
                        if (Object(y["N"])(t)) {
                            var a = this._value;
                            a.interests.find(e.propEq("id", t.id)) || (this._value = e.over(e.lensProp("interests"), e.append(t), a))
                        }
                    }, removeFrom: function () {
                        var e = this, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "keywords";
                        return function (a) {
                            e._value[t].splice(a, 1), e.$emit("input", e._value)
                        }
                    }, refreshInterestSuggestions: _()(Object(n["a"])(regeneratorRuntime.mark(function e() {
                        var t, a;
                        return regeneratorRuntime.wrap(function (e) {
                            while (1) switch (e.prev = e.next) {
                                case 0:
                                    return t = this._value.suggestions["interests"].slice(0, 5), e.next = 3, v["p"].interestSuggestions({interests: t});
                                case 3:
                                    a = e.sent, this._value.suggestions["interests"] = a;
                                case 5:
                                case"end":
                                    return e.stop()
                            }
                        }, e, this)
                    })), 300)
                }
            }
        }).call(this, a("b17e"))
    }, "45c9": function (e, t, a) {
    }, 4866: function (e, t, a) {
        "use strict";
        a("030b");
        t["a"] = {
            name: "Selection", render: function () {
                var e = arguments[0];
                return e("div", {class: "onboarding-selection"}, [e("span", {class: "onboarding-selection--title"}, [this.trans("selection_top_title")]), e("div", {class: "onboarding-selection--boxes"}, [this.renderFirstBox(), this.renderSecondBox()])])
            }, methods: {
                trans: function (e) {
                    return this.$t("ONBOARDING.".concat(e))
                }, renderFirstBox: function () {
                    var e = this, t = this.$createElement;
                    return t("div", {
                        class: "box first", on: {
                            click: function () {
                                return e.pushToRoute("/express")
                            }
                        }
                    }, [t("span", {class: "title"}, [this.trans("selection_first_box_title")]), t("span", {class: "contents"}, [this.trans("selection_first_box_desc")]), t("button", [t("span", {class: "text"}, [this.trans("selection_first_box_btn_text")]), t("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "chevron-right"
                        }, style: {color: "white"}
                    })])])
                }, renderSecondBox: function () {
                    var e = this, t = this.$createElement;
                    return t("div", {
                        class: "box second", on: {
                            click: function () {
                                return e.$emit("next")
                            }
                        }
                    }, [t("span", {class: "title"}, [this.trans("selection_second_box_title")]), t("span", {class: "contents"}, [this.trans("selection_second_box_desc")]), t("button", [t("span", {class: "text"}, [this.trans("selection_second_box_btn_text")]), t("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "chevron-right"
                        }, style: {color: "white"}
                    })])])
                }
            }
        }
    }, "49e0": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {
                    ref: "wrapper",
                    staticClass: "fb-pages-modal-wrapper"
                }, [a("div", {staticClass: "fb-pages-modal-body"}, ["notAuth" === e.status ? a("div", {staticClass: "notAuth-contents"}, [a("span", {staticClass: "desc-span"}, [e.optional ? a("span", [e._v("\n                    (" + e._s(e.$t("COMMON.optional")) + ")\n                ")]) : e._e(), e._v("\n                " + e._s(e.$t("PROPOSAL.select_fb_page_desc")) + "\n            ")]), a("div", {
                    staticClass: "fetch-btn-wrapper",
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.fetchFacebookProfile()
                        }
                    }
                }, [e._t("auth-btn", [a("img", {attrs: {src: "/img/proposal/ads_fb_login_btn.png"}})])], 2)]) : e._e(), "noPages" === e.status ? a("div", {staticClass: "noPages-contents"}, [a("img", {attrs: {src: "/img/proposal/ads_no_fb_pages.png"}}), a("span", {
                    staticClass: "desc-span",
                    domProps: {innerHTML: e._s(e.$t("PROPOSAL.no_fb_admin_pages"))}
                })]) : e._e(), "pages" === e.status ? a("div", {staticClass: "pages-contents"}, [a("span", {staticClass: "desc-span"}, [e._v("\n                " + e._s(e.$t("PROPOSAL.select_fb_page_ment")) + "\n            ")]), e.selectablePages.length > 0 ? a("div", {staticClass: "page-elems-wrapper"}, e._l(e.selectablePages, function (t) {
                    return a("div", {key: t.id, staticClass: "page-elem"}, [a("b-radio", {
                        staticClass: "custom-radio",
                        class: {selected: e.compFacebookProfile.id === t.id},
                        attrs: {value: e.compFacebookProfile.id, "native-value": t.id},
                        on: {
                            input: function (a) {
                                return e.radioSelected(t)
                            }
                        }
                    }, [a("span", {staticClass: "page-elem-box"}, [a("img", {
                        staticClass: "page-elem-img",
                        attrs: {src: t.logo || "/img/proposal/fb_logo_none.png"}
                    }), a("span", {staticClass: "page-name"}, [e._v(e._s(t.name))]), t.website ? a("span", {staticClass: "page-url"}, [e._v("\n                                " + e._s("| " + t.website) + "\n                            ")]) : e._e()])])], 1)
                }), 0) : e._e(), a("div", {staticClass: "page-elems-wrapper"}, e._l(e.unselectablePages, function (t) {
                    return a("div", {
                        key: t.id,
                        staticClass: "page-elem disabled"
                    }, [a("span", {staticClass: "page-elem-box"}, [a("img", {
                        staticClass: "page-elem-img",
                        attrs: {src: t.logo || "/img/proposal/fb_logo_none.png"}
                    }), a("span", {staticClass: "page-name"}, [e._v(e._s(t.name))]), t.reason ? a("span", {staticClass: "page-url"}, [e._v("\n                            " + e._s(e.$t("PROPOSAL.fb_reason_" + t.reason)) + "\n                        ")]) : e._e()])])
                }), 0), e.instaPage ? a("div", {staticClass: "add-instagram-page"}, [a("b-checkbox", {
                    attrs: {size: "is-small"},
                    model: {
                        value: e.addInstaPage, callback: function (t) {
                            e.addInstaPage = t
                        }, expression: "addInstaPage"
                    }
                }, [a("img", {attrs: {src: "/img/icons/instagram_top_logo.png"}}), a("span", [e._v(e._s(e.$t("PROPOSAL.add_instagram_page")))])]), a("div", {staticClass: "page-elems-wrapper"}, [a("div", {staticClass: "page-elem"}, [a("span", {
                    staticClass: "page-elem-box",
                    class: {selected: e.addInstaPage}
                }, [a("img", {
                    staticClass: "page-elem-img",
                    attrs: {src: e.instaPage.profile_pic || "/img/proposal/fb_logo_none.png"}
                }), a("span", {staticClass: "page-name"}, [e._v(e._s(e.instaPage.username || e.instaPage.name))])])])])], 1) : e._e()]) : e._e()])])
            }, i = [], s = a("d81a"), o = s["a"], r = (a("3812"), a("d62a"), a("d707"), a("2877")),
            c = Object(r["a"])(o, n, i, !1, null, "4f452e56", null);
        t["a"] = c.exports
    }, "4c73": function (e, t, a) {
    }, "4e38": function (e, t, a) {
        "use strict";
        var n = a("49e0");
        a("c3d6");
        t["a"] = {
            render: function () {
                var e = this, t = arguments[0];
                return t("div", {class: "onboarding-facebook-page"}, [t(n["a"], {
                    class: "fb-page-box",
                    attrs: {optional: !1},
                    on: {
                        update: function (t) {
                            return e.$emit("input", t)
                        }
                    }
                }, [t("button", {
                    slot: "auth-btn",
                    class: "auth-btn"
                }, [t("img", {attrs: {src: "/img/icons/facebook_logo.png"}}), this.$t("COMMON.log_in_to_fb")])])])
            }
        }
    }, "577e": function (e, t, a) {
        "use strict";
        (function (e) {
            a("967d");
            t["a"] = {
                props: {
                    value: {
                        type: Object, default: function () {
                            return {}
                        }
                    }, show: {
                        type: Object, default: function () {
                            return {}
                        }
                    }
                }, render: function (e) {
                    var t = this;
                    return e("div", {class: "onboarding-no-ads-wrapper"}, [e("div", {
                        class: "onboarding-no-ads--box",
                        on: {
                            click: function () {
                                t.$emit("go", {key: t.key})
                            }
                        }
                    }, [e("span", {class: "box-title"}, [this.buttonText])]), e("div", {
                        class: "onboarding-no-ads--box",
                        on: {
                            click: function () {
                                return t.$router.push("/express")
                            }
                        }
                    }, [e("span", {class: "box-badge"}, [e("span", {class: "strike"}, ["$19"]), e("span", {class: "normal"}, ["$9"])]), e("span", {class: "box-title"}, [this.$t("ONBOARDING.selection_first_box_title")])])])
                }, computed: {
                    status: function () {
                        return this.show.reason
                    }, buttonText: function () {
                        return "EMPTY" === this.status ? "Go back to select ads" : this.$t("ONBOARDING.no_ads_edit_ad")
                    }, key: function () {
                        return "EMPTY" === this.status ? "adTypes" : e.pathOr("facebookFeed", ["show", "target"], this)
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "5d5a": function (e, t, a) {
    }, "6c20": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), i = a.n(n), s = a("75fc"), o = a("aede"), r = a("9c56"), c = a("c4cc"), l = a("4677"),
                u = a("ebb6"), p = a("5670"), d = a("d792"), h = a("dde5"), f = a("da7a"), v = a("2d2d"), m = a("fa7d");

            function b() {
                var e = Object(o["a"])(["\n            input {\n                border-color: #ef646f;\n                &:focus,\n                &:active {\n                    outline-color: #ef646f;\n                    border-color: #ef646f;\n                    box-shadow: none;\n                    outline-offset: -2px;\n                    outline-style: auto;\n                    outline-width: 5px;\n                }\n            }\n            button {\n                position: absolute;\n                z-index: 1;\n                top: 0px;\n                height: 100%;\n                right: 2px;\n                .fas {\n                    cursor: pointer;\n                    font-size: 20px;\n                    color: #ef646f;\n                }\n            }\n            @media screen and (max-width: 800px) {\n                button {\n                    .fas {\n                        font-size: 16px;\n                    }\n                }\n            }\n        "]);
                return b = function () {
                    return e
                }, e
            }

            function g() {
                var e = Object(o["a"])(["\n    position: relative;\n    flex-grow: 1;\n    padding: 0 5px;\n    width: 185px;\n    display: flex;\n    .control {\n        align-self: center;\n        width: 100%;\n        padding: 1px 0;\n    }\n    input {\n        height: 35px;\n        background-color: #ffffff;\n        border: 0;\n        border-radius: 0px;\n        box-shadow: none;\n        ", "\n        padding: 0;\n        line-height: 36px;\n        &:focus,\n        &:active {\n            box-shadow: none;\n        }\n        &::placeholder {\n            opacity: 0.7;\n            font-size: 16px;\n            font-weight: normal;\n            letter-spacing: normal;\n            color: rgba(51, 51, 51, 0.4);\n            ", "\n        }\n    }\n    .v-spinner {\n        top: 12px;\n        right: 5px;\n        position: absolute;\n    }\n\n    ", "\n\n    @media screen and (max-width: 800px) {\n        input {\n            height: 35px;\n        }\n        .v-spinner {\n            top: 6px;\n            right: 5px;\n        }\n    }\n"]);
                return g = function () {
                    return e
                }, e
            }

            function y() {
                var e = Object(o["a"])(["\n            border: solid 2px #5aabe3;\n            box-shadow: 0 0 10px 0 rgba(90, 171, 227, 0.5);\n        "]);
                return y = function () {
                    return e
                }, e
            }

            function O() {
                var e = Object(o["a"])(["\n    width: 100%;\n    border: solid 1px #999999;\n    ", "\n    border-radius: 3px;\n    display: flex;\n    flex-wrap: wrap;\n    .tags-container.locations-tags {\n        margin: 0;\n        padding: 0 5px;\n        width: 100%;\n        display: flex;\n        flex-wrap: wrap;\n        .tags-element {\n            margin: 5px 3.5px;\n            color: #ffffff;\n            height: 25px;\n            padding: 0 10px;\n            border-radius: 45px;\n            display: flex;\n            align-items: center;\n            font-size: 12px;\n            max-width: 100%;\n            background-color: #f3b94d;\n            height: 30px;\n            .tags-text {\n                ", "\n                overflow: hidden;\n                text-overflow: ellipsis;\n                white-space: nowrap;\n                height: 100%;\n                font-size: 14px;\n                line-height: 31px;\n                margin: 5px 3.5px;\n            }\n        }\n    }\n    @media screen and (max-width: 800px) {\n        .tags-container.locations-tags {\n            .tags-element {\n                .tags-text {\n                    font-size: 12px;\n                }\n            }\n        }\n    }\n"]);
                return O = function () {
                    return e
                }, e
            }

            function _() {
                var e = Object(o["a"])(["\n    font-family: 'Roboto', 'Noto Sans', 'Font Awesome 5 Pro', sans-serif;\n"]);
                return _ = function () {
                    return e
                }, e
            }

            var x = {hasError: Boolean, focus: Boolean}, w = Object(r["a"])(_()),
                k = Object(r["b"])("div", x)(O(), function (e) {
                    return e.focus && Object(r["a"])(y())
                }, w), A = Object(r["b"])("div", x)(g(), w, w, function (e) {
                    return e.hasError && Object(r["a"])(b())
                }), N = e.map(e.prop("text"));
            t["a"] = {
                name: "Location",
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        var a = t.$watchAsObservable("text", {immediate: !0}).pipe(Object(c["a"])(function () {
                            return t.results = []
                        }), Object(l["a"])(250), Object(u["a"])(function () {
                            return t.text ? t.text.trim() : ""
                        }), Object(p["a"])(e.propSatisfies(e.gt(e.__, 0), "length")), Object(c["a"])(function () {
                            return t.isLoading = !0
                        }), Object(d["a"])(h["i"].autocomplete), Object(u["a"])(function (a) {
                            return e.without(t.value, a)
                        }), Object(c["a"])(function () {
                            return t.isLoading = !1
                        }));
                        t.$subscribeTo(a, function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            t.results = e
                        });
                        var n = t.$refs.input;
                        n && n.focus()
                    })
                },
                props: {value: {type: Array, default: e.always([])}, dirty: {type: Boolean, default: !1}},
                render: function () {
                    var e = arguments[0];
                    return e(k, {attrs: {focus: this.focusInput}}, [this.renderLocations])
                },
                data: function () {
                    return {text: "", results: [], isLoading: !1, focusInput: !0}
                },
                methods: {
                    addValue: function (e) {
                        if (Object(m["N"])(e)) {
                            var t = N(this.results).indexOf(e);
                            t > -1 && this.$emit("input", [].concat(Object(s["a"])(this.value), [this.results[t]]))
                        }
                    }, removeValue: function (e) {
                        var t = Object(s["a"])(this.value);
                        t.splice(e, 1), this.$emit("input", t)
                    }
                },
                computed: {
                    renderInput: function () {
                        var e = this, t = this.$createElement, a = "", n = !1;
                        if (!0 === this.dirty) {
                            n = !0;
                            var s = [{
                                name: "tooltip",
                                value: {content: this.$t("COMMON.required"), position: "bottom-end"}
                            }];
                            a = t("button", i()([{}, {directives: s}]), [t("i", {class: "fas fa-map-marker-exclamation"})])
                        } else this.isLoading && (a = t(f["a"], {attrs: {color: "#999", size: "20px"}}));
                        return t(A, {
                            slot: "input",
                            attrs: {hasError: n}
                        }, [t("b-autocomplete", {
                            attrs: {
                                value: this.text,
                                data: N(this.results),
                                placeholder: "��".concat(" ", this.value.length > 0 ? this.$t("ONBOARDING.location_input_placeholder") : this.$t("ONBOARDING.location_input_placeholder_empty_tag")),
                                "clear-on-select": !0
                            }, on: {
                                input: function (t) {
                                    return e.text = t
                                }, select: this.addValue, focus: function () {
                                    return e.focusInput = !0
                                }, blur: function () {
                                    return e.focusInput = !1
                                }
                            }, ref: "input"
                        }), a])
                    }, renderLocations: function () {
                        var t = this.$createElement, a = {
                            attrs: {
                                className: "locations-tags",
                                values: this.value,
                                "value-getter": e.prop("text"),
                                "max-width": "100%"
                            }, on: {"update:remove": this.removeValue}
                        };
                        return t(v["a"], i()([{}, a]), [this.renderInput])
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "73c7": function (e, t, a) {
    }, "73d7": function (e, t, a) {
    }, "77fb": function (e, t, a) {
    }, "7d8e": function (e, t, a) {
    }, "80a6": function (e, t, a) {
    }, "8a23": function (e, t, a) {
        "use strict";
        a("c5f6");
        var n = a("04d0"), i = (a("4c73"), {value: 29, type: "fixed"});
        t["a"] = {
            props: {value: {type: Object}}, data: function () {
                return {inputValue: 12}
            }, render: function () {
                var e = this, t = arguments[0];
                return t("div", {class: "onboarding-budget-selection-container"}, [t(n["a"], {
                    attrs: {
                        options: this.options,
                        selected: this.value,
                        inputValue: this.inputValue,
                        type: "budget"
                    }, on: {
                        input: function (t) {
                            var a = t.value, n = t.type;
                            return e.handleClick(a, n)
                        }, inputValue: this.handleInput, blur: this.checkMin
                    }
                }), t("p", {class: "text-center onboarding-budget-selection--description"}, [this.$t("ONBOARDING.budget_minimum_budget", {value: "".concat(this.formatCurrency(12))})])])
            }, computed: {
                _value: function () {
                    var e = this.value;
                    return "fixed" === e.type ? e.value : "custom"
                }, options: function () {
                    return [{
                        left: this.$t("ONBOARDING.budget_radio_format", {value: this.formatCurrency(12)}),
                        value: 12
                    }, {
                        left: this.$t("ONBOARDING.budget_radio_format", {value: this.formatCurrency(29)}),
                        value: 29,
                        banner: this.$t("ONBOARDING.budget_most_used")
                    }, {
                        left: this.$t("ONBOARDING.budget_radio_format", {value: this.formatCurrency(49)}),
                        banner: this.$t("ONBOARDING.budget_best_option"),
                        value: 49
                    }, {value: "custom", hasInput: !0}]
                }
            }, watch: {
                value: {
                    handler: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = e.type,
                            a = e.value;
                        "custom" === t && (this.inputValue = a)
                    }, immediate: !0, deep: !0
                }
            }, methods: {
                handleInput: function (e) {
                    e = Number(e), this.$emit("input", {type: "custom", value: e})
                }, handleClick: function (e, t) {
                    "custom" === t && (e = this.inputValue), e = Number(e), this.$emit("input", {type: t, value: e})
                }, checkMin: function () {
                    if (this.inputValue < 12) return this.handleInput(12)
                }
            }, created: function () {
                this.value || this.$emit("input", i)
            }
        }
    }, "967d": function (e, t, a) {
    }, "96d6": function (e, t, a) {
        "use strict";
        var n = a("ed3b2"), i = a.n(n);
        i.a
    }, "97ec": function (e, t, a) {
    }, "9eeb": function (e, t, a) {
        "use strict";
        (function (e) {
            a("7514");
            var n = a("768b"), i = a("b6d0"), s = a.n(i), o = a("2638"), r = a.n(o), c = a("cebc"), l = a("fa7d"),
                u = a("a772"), p = a("649d"), d = a("4c6b"),
                h = (a("73d7"), a("2f62"), [[d["h"], "facebookFeed"], [d["o"], "instagramFeed"], [d["k"], "googleSearch"], [d["i"], "googleDisplay"], [d["l"], "googleUniversalApp"], [d["c"], "appleSearch"]]);
            t["a"] = {
                props: {
                    value: {type: Array},
                    isApp: {type: Boolean, default: !1},
                    data: {
                        type: Object, default: function () {
                            return {}
                        }
                    }
                }, data: function () {
                    return {isCouponVisible: !1, options: []}
                }, mounted: function () {
                    var t = e.path(["data", "webOrApp", "value", "type"], this),
                        a = Object(u["c"])({type: t, appstoreAvailable: this.appstoreExist}), n = a.options,
                        i = void 0 === n ? [] : n, s = a.selected, o = void 0 === s ? [] : s;
                    this.options = Object(l["f"])(i), Object(l["N"])(this._value) || this.updateValue(o)
                }, render: function () {
                    var t = arguments[0], a = {
                        attrs: Object(c["a"])({
                            type: this.isApp ? "APP" : "WEB",
                            appstoreAvailable: this.appstoreExist,
                            showLabel: !1
                        }, e.pick(["selected", "options"], this)), on: {"update:selected": this.updateValue}
                    };
                    return t("div", {class: "onboarding-ad-types-wrapper"}, [t(p["a"], r()([{}, a])), this.shouldRenderCoupon && this.renderCoupon()])
                }, computed: {
                    _value: {
                        get: e.propOr([], "value"), set: function (e) {
                            this.$emit("input", e)
                        }
                    }, shouldRenderCoupon: function () {
                        return e.pathEq(["globalUser", "country"], "GB", this) && 0 === e.intersection(this._value, ["googleSearch", "googleDisplay", "googleUniversalApp"]).length
                    }, selected: function () {
                        var e = new s.a(this._value);
                        return h.reduce(function (t, a) {
                            var i = Object(n["a"])(a, 2), s = i[0], o = i[1];
                            return e.has(o) ? t.concat(s) : t
                        }, [])
                    }, appstoreExist: e.compose(l["N"], e.path(["data", "webOrApp", "value", "appstore"]))
                }, methods: {
                    renderCoupon: function () {
                        var e = this, t = this.$createElement;
                        return t("div", {class: "onboarding-ad-types--coupon"}, [t("v-popover", {
                            attrs: {
                                open: this.isCouponVisible,
                                trigger: "manual"
                            }, on: {
                                hide: function () {
                                    return e.isCouponVisible = !1
                                }
                            }
                        }, [t("div", {
                            class: "popover-trigger", on: {
                                click: function () {
                                    return e.isCouponVisible = !0
                                }
                            }
                        }, [t("span", {class: "coupon-title"}, ["Get 짙75 Google Ads Credit"]), t("b-icon", {
                            attrs: {
                                pack: "fas",
                                icon: "question-circle"
                            }
                        })]), t("div", {
                            slot: "popover",
                            class: "onboarding-ad-types-coupon-popover"
                        }, ["You will receive 짙75 Google Ads credit automatically when you spend 짙25 on either Google Search or Google Display."])])])
                    }, updateValue: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
                            a = e.find(e.__, t), i = e.pick(["channel", "creativeType"]);
                        this._value = h.reduce(function (t, s) {
                            var o = Object(n["a"])(s, 2), r = o[0], c = o[1];
                            return a(e.whereEq(i(r))) ? t.concat(c) : t
                        }, [])
                    }
                }
            }
        }).call(this, a("b17e"))
    }, a234: function (e, t, a) {
        "use strict";
        var n = a("45c9"), i = a.n(n);
        i.a
    }, a835: function (e, t, a) {
    }, ad04: function (e, t, a) {
    }, ba2c: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), i = a.n(n), s = a("3ec5"), o = a("a772"), r = a("3352");
            a("80a6");
            t["a"] = {
                name: "AdsList",
                props: {
                    data: {
                        type: Object, default: function () {
                            return {}
                        }
                    }, dirty: {type: Boolean, default: !1}, animateCheckbox: {type: Boolean, default: !1}
                },
                data: function () {
                    return {modalMeta: {appstore: !1}}
                },
                watch: {
                    animateCheckbox: function (e) {
                        !0 === e && window.scrollTo(0, 0)
                    }
                },
                render: function () {
                    var e = this, t = arguments[0], a = this.ads || [];
                    return t("div", {class: "adriel-flex-center onboarding-ads-list-container"}, [this.renderAppstoreModal(), t("div", {class: "onboarding---mobile-nav-container full-page"}, [this.showGoToReview && t("span", {
                        class: "adriel-flex-center go-to-review-payment",
                        on: {
                            click: function () {
                                e.from = null, e.$emit("go", {key: "summary"})
                            }
                        }
                    }, [t("span", {class: "text-span"}, [this.$t("ONBOARDING.go_to_review_and_payment")]), t("b-icon", {
                        attrs: {
                            pack: "fal",
                            icon: "arrow-right"
                        }
                    })]), this.renderButton("prev"), this.renderButton("next"), this.renderMobileDirty()]), t("div", {class: "onboarding-ads-list--title"}, [this.$t("ONBOARDING.ads_list_title")]), t("div", {class: "adriel-flex-center onboarding-ads-list"}, [a.map(function () {
                        var n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, r = n.value,
                            c = arguments.length > 1 ? arguments[1] : void 0, l = {
                                attrs: {
                                    ad: r,
                                    dirty: e.dirty,
                                    type: e.baseType,
                                    ads: a,
                                    animateCheckbox: e.animateCheckbox,
                                    copyable: 0 !== c && !Object(o["m"])(r),
                                    autofocus: 0 === c,
                                    campaignId: e.data.campaignId,
                                    editable: !Object(o["m"])(r),
                                    appleDisabled: !e.appleConnected
                                }, on: {
                                    go: function (t) {
                                        return e.$emit("go", t)
                                    }, input: function (t) {
                                        return e.$emit("input", t)
                                    }, copy: function () {
                                        return e.$emit("copy", r)
                                    }, "click:appstore": function () {
                                        return e.updateModalMeta("appstore", !0)
                                    }
                                }
                            };
                        return t(s["a"], i()([{}, l]))
                    })]), t("div", {class: "onboarding-nav-buttons-container full-page"}, [this.showGoToReview && t("span", {
                        class: "adriel-flex-center go-to-review-payment",
                        on: {
                            click: function () {
                                e.from = null, e.$emit("go", {key: "summary"})
                            }
                        }
                    }, [t("span", {class: "text-span"}, [this.$t("ONBOARDING.go_to_review_and_payment")]), t("b-icon", {
                        attrs: {
                            pack: "fal",
                            icon: "arrow-right"
                        }
                    })]), this.renderButton("prev"), this.renderButton("next")])])
                },
                methods: {
                    renderButton: function (e) {
                        var t = this, a = this.$createElement;
                        return "next" === e ? a("button", {
                            class: "nav-btn next", on: {
                                click: function () {
                                    return t.$emit("next")
                                }
                            }
                        }, [a("span", {class: "adriel-flex-center"}, [a("span", {class: "text-span"}, [this.$t("ONBOARDING.next")]), a("b-icon", {
                            attrs: {
                                pack: "fal",
                                icon: "arrow-right"
                            }
                        })])]) : "prev" === e ? a("button", {
                            class: "nav-btn prev", on: {
                                click: function () {
                                    return t.$emit("prev")
                                }
                            }
                        }, [a("span", [a("b-icon", {
                            attrs: {
                                pack: "fal",
                                icon: "arrow-left"
                            }
                        }), a("span", {class: "text-span"}, [this.$t("ONBOARDING.back")])])]) : void 0
                    },
                    previewVisible: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = t.value,
                            n = void 0 === a ? {} : a, i = arguments.length > 1 ? arguments[1] : void 0;
                        return e.contains(this.adType(n), i)
                    },
                    adType: e.cond([[o["u"], e.always("instagramFeed")], [o["n"], e.always("facebookFeed")], [o["q"], e.always("googleDisplay")], [o["r"], e.always("googleSearch")], [o["t"], e.always("googleUniversalApp")], [o["m"], e.always("appleSearch")]]),
                    renderAppstoreModal: function () {
                        var t = this, a = this.$createElement, n = "appstore";
                        if (e.path(["modalMeta", n], this)) {
                            var s = {
                                attrs: {active: !0, id: this.appleAppId, campaignId: this.campaignId},
                                on: {
                                    "update:active": this.updateModalMeta(n).bind(this), confirm: function (e) {
                                        t.$emit("update:campaign", {path: ["intel", "appleAppId"], value: e})
                                    }
                                }
                            };
                            return a(r["a"], i()([{}, s]))
                        }
                    },
                    updateModalMeta: e.curry(function (e, t) {
                        this.modalMeta[e] = t
                    }),
                    renderMobileDirty: function () {
                        var e = this.$createElement, t = this.windowDimension, a = this.dirty;
                        if (!(t.width > 800) && a) return e("div", {class: "onboarding-mobile-ad-dirty"}, [this.$t("ONBOARDING.ads_list_mobile_dirty")])
                    }
                },
                computed: {
                    allAds: e.pathOr([], ["data", "ads"]),
                    adTypes: e.pathOr([], ["data", "adTypes", "value"]),
                    baseType: e.pathOr("web", ["data", "webOrApp", "value", "type"]),
                    showGoToReview: function () {
                        return !!e.path(["$route", "query", "f"], this)
                    },
                    ads: function () {
                        var t = this;
                        return e.compose(e.filter(function (e) {
                            return t.previewVisible(e, t.adTypes)
                        }), e.defaultTo([]))(this.allAds)
                    },
                    appleConnected: e.path(["data", "campaign", "intel", "appleAppId"]),
                    appleAppId: e.path(["data", "webOrApp", "value", "appstore", "id"]),
                    campaignId: e.path(["data", "campaignId"])
                }
            }
        }).call(this, a("b17e"))
    }, c0bb: function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("cebc"), i = a("c1df"), s = a.n(i), o = a("04d0"), r = a("fa7d"), c = (a("5d5a"), {
                startDate: new Date,
                endDate: s()(new Date).add(30, "day").toDate(),
                continuously: !0,
                input: {type: "fixed", value: 30}
            });
            t["a"] = {
                name: "Schedule", props: {value: {type: Object}}, render: function () {
                    var e = arguments[0];
                    if (Object(r["N"])(this.value)) return e("div", {class: "onboarding-schedule-container"}, [e("div", {class: "onboarding-schedule--boxes"}, [this.renderBox("continuously"), this.renderBox("startEnd")]), this.renderInputs(this.value.continuously)])
                }, computed: {
                    inputValue: e.pathOr(3, ["value", "input", "value"]), minStartDate: function () {
                        var e = s()();
                        return e.add(e.weekday() > 4 ? 2 : 1, "day").startOf("day").toDate()
                    }, minEndDate: function () {
                        return s()(this.startDate).add(3, "day").toDate()
                    }, _startDate: {
                        get: e.compose(r["cb"], e.path(["value", "startDate"])), set: function (e) {
                            this.setStartDate(e), s()(this._endDate).isBefore(this.minEndDate) && this.$emit("input", Object(n["a"])({}, this.value, {endDate: s()(e).add(this.value.input.value, "day").toDate()}))
                        }
                    }, _endDate: {
                        get: e.compose(r["cb"], e.path(["value", "endDate"])), set: function (e) {
                            this.setEndDate(e)
                        }
                    }, _value: function () {
                        return "fixed" === this.value.type ? this.value.value : "custom"
                    }, options: function () {
                        return [{
                            left: this.$t("ONBOARDING.schedule_period_format", {value: 7}),
                            value: 7
                        }, {
                            left: this.$t("ONBOARDING.schedule_period_format", {value: 15}),
                            value: 15
                        }, {
                            left: this.$t("ONBOARDING.schedule_period_format", {value: 30}),
                            banner: this.$t("ONBOARDING.schedule_recommended"),
                            value: 30
                        }, {value: "custom", hasInput: !0}]
                    }
                }, methods: {
                    setEndDate: function (t) {
                        this.$emit("input", e.assoc("endDate", t, this.value))
                    }, setStartDate: function (t) {
                        if (this._startDate !== t) {
                            var a = e.assoc("startDate", t, this.value), n = this.value.input, i = n.value, o = n.type;
                            "fixed" === o && (a = e.assoc("endDate", s()(t).add(i, "day").toDate(), a)), this.$emit("input", a)
                        }
                    }, dateFormat: function (e) {
                        return s()(e).format(this.$t("PROPOSAL.schedule_date_format"))
                    }, renderDatePicker: function () {
                        var e, t, a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "start",
                            n = this.$createElement;
                        return "start" === a ? (e = this._startDate, t = this.minStartDate) : (e = this._endDate, t = this.minEndDate), n("b-datepicker", {
                            attrs: {
                                placeholder: "Click to select...",
                                icon: "calendar-today",
                                value: e,
                                "min-date": t,
                                "date-formatter": this.dateFormat,
                                "month-names": this.$t("PROPOSAL.schedule_date_month_names"),
                                "day-names": this.$t("PROPOSAL.schedule_day_names")
                            }, on: {input: this.setStartDate}
                        })
                    }, renderInputs: function () {
                        var e = this, t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0],
                            a = this.$createElement;
                        return a("transition", {attrs: {name: "fade"}}, [!t && a("div", {class: "onboarding-schedule--inputs"}, [a("div", {class: "input-box"}, [a("span", [this.$t("ONBOARDING.schedule_start_date")]), this.renderDatePicker("start")]), a("div", {class: "input-box"}, [a("span", [this.$t("ONBOARDING.schedule_end_date")]), a(o["a"], {
                            attrs: {
                                options: this.options,
                                selected: this.value.input,
                                inputValue: this.inputValue,
                                type: "schedule"
                            }, on: {
                                input: function (t) {
                                    var a = t.value, n = t.type;
                                    return e.handleRadio(a, n)
                                }, blur: this.checkMin, inputValue: this.handleInput
                            }
                        })])])])
                    }, renderBox: function () {
                        var e = this,
                            t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "continuously",
                            a = this.$createElement, n = ["box"], i = this.value.continuously;
                        return "continuously" === t && i ? n.push("selected") : "startEnd" !== t || i || n.push("selected"), a("div", {
                            class: n.join(" "),
                            on: {
                                click: function () {
                                    return e.handleClick(t)
                                }
                            }
                        }, ["continuously" === t ? this.$t("ONBOARDING.schedule_first_box_title") : this.$t("ONBOARDING.schedule_second_box_title")])
                    }, isSelected: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                        return "fixed" === this.value.type ? this.value.value === e && !t : t
                    }, renderRadio: function (e) {
                        var t = this, a = e.value, n = e.banner, i = this.$createElement, s = "custom" === a,
                            o = this.isSelected(a, s), r = ["schedule-box"];
                        return o && r.push("selected"), i("div", {class: r.join(" ")}, [i("b-radio", {
                            attrs: {
                                value: this._value,
                                "native-value": a
                            }, on: {
                                input: function () {
                                    return t.handleRadio(a, s ? "custom" : "fixed")
                                }
                            }
                        }, [i("div", {class: "box--contents"}, s ? ["$", " ", i("input", {
                            attrs: {type: "number"},
                            domProps: {value: this.inputValue},
                            on: {
                                input: function (e) {
                                    return t.handleInput(e.target.value)
                                }
                            }
                        })] : [this.renderBanner(n), i("span", {class: "box--left"}, [a])])])])
                    }, handleClick: function (e) {
                        this.$emit("input", Object(n["a"])({}, this.value, {continuously: "continuously" === e}))
                    }, renderBanner: function (e) {
                        var t = this.$createElement;
                        return e ? t("div", {class: "banner"}, [t("span", {class: "banner--text"}, [e])]) : null
                    }, handleInput: function (e) {
                        e = Number(e), this.$emit("input", Object(n["a"])({}, this.value, {
                            endDate: s()(this._startDate).add(e, "day").toDate(),
                            input: {type: "custom", value: e}
                        }))
                    }, handleRadio: function (e, t) {
                        "custom" === t && (e = this.inputValue), e = Number(e), this.$emit("input", Object(n["a"])({}, this.value, {
                            endDate: s()(this._startDate).add(e, "day").toDate(),
                            input: {type: t, value: e}
                        }))
                    }, checkMin: function () {
                        if (this.inputValue < 3) return this.handleInput(3)
                    }
                }, created: function () {
                    this.value || this.$emit("input", c)
                }
            }
        }).call(this, a("b17e"))
    }, c392: function (e, t, a) {
        "use strict";
        a.r(t), function (e) {
            a("6762"), a("2fdb");
            var n = a("5176"), i = a.n(n), s = (a("ac6a"), a("768b")), o = (a("a481"), a("96cf"), a("3b8d")),
                r = a("2638"), c = a.n(r), l = (a("7514"), a("75fc")), u = (a("20d6"), a("cebc")), p = a("dde5"),
                d = a("2f62"), h = a("5d07"), f = a("c7c5"), v = a("0c2f"), m = a("dcee"), b = a("6e77"), g = a("a748"),
                y = a("808d"), O = a("2bd2"), _ = a("9586"), x = a("1b92"), w = a("4b59"), k = a("c4cc"), A = a("1a2d"),
                N = a("5670"), $ = a("1585"), I = a("87ee"), j = a("ebb6"), D = a("b7d7"), T = a("d792"), P = a("66aa"),
                R = a("f59d"), C = a("a772"), B = a("fa7d"), G = (a("a79c"), a("b047")), E = a.n(G), S = a("337f"),
                F = e.always({value: !0}),
                M = e.compose(e.ifElse(e.is(Boolean), e.identity, e.prop("value")), e.defaultTo({})),
                L = e.pathEq(["webOrApp", "value", "type"]), q = L("app"), V = L("web"), U = L("form"),
                K = e.any(e.test(/(facebook|instagram)/gi)), z = e.pathOr([], ["adTypes", "value"]),
                H = e.compose(e.map(e.path(["value", "channel"])), e.filter(e.path(["value", "enabled"])), e.prop("ads")),
                W = function (t, a) {
                    return function () {
                        var n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, i = n.ads,
                            s = void 0 === i ? [] : i, o = {channel: t};
                        return "google" === t && (o = Object(u["a"])({}, o, {creativeType: a})), e.compose(function (e) {
                            return ["ads", e < 0 ? s.length : e]
                        }, e.findIndex(e.compose(e.whereEq(o), e.propOr({}, "value"))))(s)
                    }
                }, Y = {type: "inputContainer", contents: ["ONBOARDING.can_change_anytime"]},
                J = {enabled: !0, meta: {adEdited: !1, cardIndex: 0, isEditing: !0}, executable: {value: !1}}, Q = [{
                    type: "fullPage",
                    childType: "selection",
                    key: "selection",
                    showLang: !0
                }, Object(u["a"])({}, Y, {
                    title: "ONBOARDING.app_url_title",
                    childType: "webOrApp",
                    key: "webOrApp",
                    updatePath: e.always(["webOrApp"]),
                    skippable: !1,
                    nextEnabled: function (t, a) {
                        var n = e.path([].concat(Object(l["a"])(a.updatePath(t)), ["value"]), t);
                        return !!Object(B["N"])(n) && Object(C["x"])(n)
                    },
                    popover: "ONBOARDING.website_url_popover",
                    progress: 1
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.form_title",
                    contents: ["ONBOARDING.can_download"],
                    childType: "form",
                    key: "form",
                    updatePath: e.always(["form"]),
                    skippable: !1,
                    nextEnabled: function (e, t) {
                        return !0
                    },
                    progress: 2,
                    popover: "ONBOARDING.form_popover"
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.location_title",
                    childType: "location",
                    key: "locations",
                    updatePath: e.always(["locations"]),
                    skippable: !1,
                    nextEnabled: e.compose(Boolean, e.path(["locations", "value", "length"])),
                    popover: "ONBOARDING.location_popover",
                    progress: 3
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.keyword_title",
                    childType: "keyword",
                    key: "keywords",
                    updatePath: e.always(["keywords"]),
                    skippable: !0,
                    progress: 4,
                    contents: ["ONBOARDING.keywords_can_change"]
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.ad_types_title",
                    childType: "adTypes",
                    contents: ["ONBOARDING.can_change_ad_platforms"],
                    key: "adTypes",
                    updatePath: e.always(["adTypes"]),
                    skippable: !1,
                    nextEnabled: e.path(["adTypes", "value", "length"]),
                    progress: 5
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.ads_title",
                    childType: "facebookFeed",
                    contents: ["ONBOARDING.can_change_ad_contents"],
                    key: "facebookFeed",
                    updatePath: W("facebook", "photo"),
                    show: e.compose(e.objOf("value"), e.contains("facebookFeed"), e.pathOr([], ["adTypes", "value"])),
                    nextEnabled: function (t, a) {
                        var n = e.path([].concat(Object(l["a"])(a.updatePath(t)), ["value"]), t);
                        return Object(C["h"])(n)
                    },
                    popover: "ONBOARDING.facebook_popover",
                    progress: 6
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.ads_title",
                    childType: "instagramFeed",
                    contents: ["ONBOARDING.can_change_ad_contents"],
                    key: "instagramFeed",
                    updatePath: W("instagram", "photo"),
                    show: function (t) {
                        if (!e.contains("instagramFeed", z(t))) return {value: !1};
                        var a = e.find(e.propEq("key", "facebookFeed"), Q);
                        return !M(a.show(t))
                    },
                    nextEnabled: function (t, a) {
                        var n = e.path([].concat(Object(l["a"])(a.updatePath(t)), ["value"]), t);
                        return Object(C["h"])(n)
                    },
                    popover: "ONBOARDING.instagram_popover",
                    progress: 6
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.ads_title",
                    childType: "googleSearch",
                    key: "googleSearch",
                    updatePath: W("google", "ExpandedTextAd"),
                    contents: ["ONBOARDING.can_change_ad_contents"],
                    show: function (t) {
                        if (!e.contains("googleSearch", z(t))) return {value: !1};
                        var a = e.find(e.propEq("key", "facebookFeed"), Q), n = e.find(e.propEq("key", "instagramFeed"), Q),
                            i = e.find(e.propEq("key", "googleDisplay"), Q);
                        return !M(a.show(t)) && !M(n.show(t)) && !M(i.show(t))
                    },
                    nextEnabled: function (t, a) {
                        var n = e.path([].concat(Object(l["a"])(a.updatePath(t)), ["value"]), t);
                        return Object(C["h"])(n)
                    },
                    popover: "ONBOARDING.google_search_popover",
                    progress: 6
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.ads_title",
                    contents: ["ONBOARDING.can_change_ad_contents"],
                    childType: "googleDisplay",
                    key: "googleDisplay",
                    updatePath: W("google", "ResponsiveDisplayAd"),
                    show: function (t) {
                        if (!e.contains("googleDisplay", z(t))) return {value: !1};
                        var a = e.find(e.propEq("key", "facebookFeed"), Q), n = e.find(e.propEq("key", "instagramFeed"), Q);
                        return !M(a.show(t)) && !M(n.show(t))
                    },
                    nextEnabled: function (t, a) {
                        var n = e.path([].concat(Object(l["a"])(a.updatePath(t)), ["value"]), t);
                        return Object(C["h"])(n)
                    },
                    popover: "ONBOARDING.google_display_popover",
                    progress: 6
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.ads_title",
                    contents: ["ONBOARDING.can_change_ad_contents"],
                    childType: "googleUniversalApp",
                    key: "googleUniversalApp",
                    updatePath: W("google", "universalApp"),
                    show: function (t) {
                        if (!e.contains("googleUniversalApp", z(t))) return {value: !1};
                        var a = e.find(e.__, Q), n = e.propEq("key"), i = a(n("facebookFeed")), s = a(n("instagramFeed")),
                            o = a(n("googleDisplay"));
                        return !M(i.show(t)) && !M(s.show(t)) && !M(o.show(t))
                    },
                    nextEnabled: function (t, a) {
                        var n = e.path([].concat(Object(l["a"])(a.updatePath(t)), ["value"]), t);
                        return Object(C["h"])(n)
                    },
                    popover: "ONBOARDING.google_universal_app_popover",
                    progress: 6
                }), Object(u["a"])({}, Y, {
                    key: "appleSearch", updatePath: W("apple", "search"), show: function (e) {
                        return !1
                    }
                }), {
                    type: "fullPage",
                    key: "adsList",
                    childType: "adsList",
                    show: F,
                    progress: 6
                }, Object(u["a"])({}, Y, {
                    title: "ONBOARDING.budget_title",
                    childType: "budget",
                    key: "budget",
                    updatePath: e.always(["budget"]),
                    skippable: !1,
                    popover: "ONBOARDING.budget_popover",
                    progress: 7
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.schedule_title",
                    childType: "schedule",
                    key: "schedule",
                    updatePath: e.always(["schedule"]),
                    popover: "ONBOARDING.schedule_popover",
                    progress: 8
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.summary_title",
                    childType: "summaryAndPayment",
                    skippable: !1,
                    key: "summary",
                    updatePath: e.always(["summary"]),
                    modal: v["a"],
                    modalType: "modal",
                    progress: 9
                }), {
                    type: "fullPage",
                    childType: "reviewProcess",
                    key: "reviewProcess"
                }, Object(u["a"])({}, Y, {
                    title: "ONBOARDING.facebookpage_title",
                    childType: "facebookPage",
                    key: "facebookPage",
                    contents: ["ONBOARDING.need_fb_page"],
                    updatePath: e.always(["facebookPage"]),
                    show: function (t) {
                        return e.objOf("value", K(H(t)))
                    }
                }), {
                    type: "inputContainer",
                    title: "ONBOARDING.fb_app_title",
                    contents: [],
                    childType: "fbApp",
                    key: "fbApp",
                    updatePath: e.always(["fbApp"]),
                    show: function (e) {
                        return q(e) && K(H(e))
                    }
                }, Object(u["a"])({}, Y, {
                    title: "ONBOARDING.tracker_title",
                    childType: "tracker",
                    key: "tracker",
                    updatePath: e.always(["tracker"]),
                    show: V
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.objectives",
                    childType: "objectives",
                    key: "objectives",
                    updatePath: e.always(["objectives"]),
                    show: function (t) {
                        var a = e.pathOr([], ["tracker", "value"], t);
                        return e.objOf("value", a.length > 0 && !0 !== t.tracker.skip && !U(t))
                    }
                }), Object(u["a"])({}, Y, {
                    title: "ONBOARDING.audience_title",
                    childType: "audience",
                    key: "audience",
                    updatePath: e.always(["audience"]),
                    skippable: !0
                }), {type: "fullPage", childType: "finish", key: "finish"}];
            t["default"] = {
                render: function () {
                    var e = arguments[0], t = ["onboarding-container"];
                    return !0 === this.animate && t.push("onboarding-animate"), e("div", {class: t.join(" ")}, [this.renderProgress(this.index), this.renderChild(this.index, this.data)])
                },
                name: "Onboarding",
                watch: {
                    "$route.query.c": {
                        handler: function (e) {
                            e && (this.go({key: e, from: this.$route.query.f}), this.handleComponentChange(e))
                        }, immediate: !0
                    }, formCreate: {
                        handler: function (e) {
                            "form" === this.webOrApp && this.updateData(["form"], e, "form")
                        }, immediate: !0, deep: !0
                    }, "currentConfig.key": {
                        handler: function (e) {
                            "reviewProcess" !== e && "audience" !== e || h["audience"].chooseAudienceVisibility({
                                onboarding: Object(u["a"])({}, this.data, {ads: this.availableAds}),
                                intel: this.campaign.intel,
                                type: this.webOrApp
                            })
                        }, deep: !0, immediate: !0
                    }
                },
                computed: Object(u["a"])({}, Object(d["mapGetters"])("form", ["formCreate"]), {
                    nextFunc: function () {
                        var t = this, a = Q[this.index] || {}, n = a.updatePath, i = void 0 === n ? e.T : n, s = a.key;
                        this.index;
                        return "webOrApp" === s ? function () {
                            var a = e.path(["data", "webOrApp", "value"], t), n = a.type, s = a.websiteUrl,
                                o = a.playStoreUrl, r = a.appStoreUrl, c = a.developerSiteUrl, l = s;
                            if ("app" === n && (l = c || o || r), !l) return t.pending.ads = !1, void t.next(i(t.data));
                            t.pending.ads = !0, p["b"].generateByMeta({webOrApp: a}).then(function (a) {
                                Object(B["N"])(a) && (t.data.ads = a.map(function (t) {
                                    return {value: Object(u["a"])({}, t, e.clone(J))}
                                })), t.pending.ads = !1
                            }), t.$set(t.pending, "keywords", !0), p["p"].getSuggestions(l).then(function (e) {
                                t.data.keywords = {value: e}, t.$set(t.pending, "keywords", !1)
                            }), t.next(i(t.data))
                        } : "locations" === s ? function () {
                            var a = e.path(["data", "locations", "value"], t),
                                n = t.data.audience.value.find(e.propEq("name", "location"));
                            n && (n.values = a), t.next(i(t.data))
                        } : "keywords" === s ? function () {
                            var a = e.path(["data", "keywords", "value"], t);
                            a || t.next(i(t.data));
                            var n = a.keywords, s = a.interests,
                                o = e.find(e.__, e.path(["data", "audience", "value"], t) || []), r = e.propEq("name"),
                                c = o(r("searchKeywords"));
                            c && (c.values = n);
                            var l = o(r("audienceInterests"));
                            l && (l.values = s), t.next(i(t.data))
                        } : function () {
                            return t.next(i(t.data))
                        }
                    },
                    currentConfig: function () {
                        return Q[this.index]
                    },
                    webOrApp: e.path(["data", "webOrApp", "value", "type"]),
                    nextEnabled: function () {
                        var t = this.currentConfig || {}, a = t.nextEnabled, n = void 0 === a ? e.T : a,
                            i = n(this.data || {}, t);
                        return e.is(Object, i) ? i : void 0 !== i ? e.objOf("value", i) : {value: !0}
                    },
                    isLast: function () {
                        if (this.index >= Q.length - 1) return !0;
                        for (var e = !1, t = this.index + 1; t < Q.length; t++) {
                            var a = Q[t].show;
                            if (e = !a || a(this.data), e = M(e), e) break
                        }
                        return !e
                    },
                    adTypes: e.pathOr([], ["data", "adTypes", "value"]),
                    availableAdsTypes: e.compose(e.map(e.path(["value", "channel"])), e.filter(e.path(["value", "enabled"])), e.prop("availableAds")),
                    availableAds: function () {
                        var t = this;
                        return e.compose(e.filter(function (e) {
                            return t.previewVisible(e, t.adTypes)
                        }), e.defaultTo([]))(this.data.ads)
                    },
                    getCampaignId: function () {
                        return e.path(["$route", "params", "id"], this)
                    }
                }),
                methods: Object(u["a"])({}, Object(d["mapMutations"])("campaign", ["setCampaign"]), Object(d["mapMutations"])("form", ["SET_DATA"]), Object(d["mapActions"])("form", ["clearData"]), {
                    renderChild: function (e) {
                        var t = Q[e];
                        return this["render".concat(Object(B["fb"])(t.type))](t)
                    },
                    renderFullPage: function () {
                        var t = this, a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                            n = a.childType, i = a.showLang, s = void 0 !== i && i, o = this.$createElement, r = {
                                attrs: {
                                    childType: n,
                                    showLang: s,
                                    type: this.webOrApp,
                                    data: Object(u["a"])({}, this.data, {
                                        campaignId: this.campaignId,
                                        campaign: this.campaign,
                                        availableAds: this.availableAds
                                    })
                                }, on: {
                                    next: this.nextFunc, prev: function () {
                                        return t.decIndex()
                                    }, skip: function () {
                                        switch (n) {
                                            case"reviewProcess":
                                                t.$router.push({name: "MyCampaigns"});
                                                break;
                                            default:
                                                break
                                        }
                                    }, go: this.go, input: function (e) {
                                        switch (n) {
                                            case"adsList":
                                                return t.updateSpecificAd(e)
                                        }
                                    }, copy: function (e) {
                                        switch (n) {
                                            case"adsList":
                                                return t.handleCopy(e);
                                            default:
                                                break
                                        }
                                    }, "update:campaign": function () {
                                        var a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                            n = a.path, i = a.value;
                                        Object(B["N"])(n) && (t.campaign = e.assocPath(n, i, t.campaign))
                                    }
                                }
                            };
                        return o(f["a"], c()([{}, r]))
                    },
                    renderInputContainer: function (t) {
                        var a = this, n = this.$createElement, i = t.title, s = t.contents, o = void 0 === s ? [] : s,
                            r = t.childType, c = t.key, p = t.updatePath, d = t.show, h = void 0 === d ? F : d,
                            f = this.data;
                        p && (f = e.path([].concat(Object(l["a"])(p(this.data)), ["value"]), this.data));
                        var v = function () {
                            p && (a.data = e.over(e.lensPath(p(a.data)), e.compose(e.merge(e.__, {skip: !0}), e.defaultTo({})), a.data)), a.isLast ? a.$router.push({name: "MyCampaigns"}) : a.incIndex(!0)
                        }, b = function (e) {
                            return a.updateData(p(a.data), e, c)
                        };
                        return "audience" === c && (b = function (e) {
                            return a.updateFromAudience(e)
                        }), n(m["a"], {
                            attrs: {
                                title: i,
                                contents: o,
                                childType: r,
                                type: this.webOrApp,
                                isLast: this.isLast,
                                data: Object(u["a"])({}, t, {
                                    value: f,
                                    allData: this.data,
                                    pending: this.pending[c] || !1,
                                    campaignId: this.campaignId,
                                    campaign: this.campaign,
                                    nextEnabled: !!this.nextEnabled.value,
                                    show: h(this.data),
                                    availableAds: this.availableAds
                                })
                            },
                            class: this.inputContainerClass,
                            on: {
                                next: this.nextFunc,
                                skip: v,
                                prev: this.decIndex,
                                go: this.go,
                                input: b,
                                copy: this.handleCopy
                            }
                        })
                    },
                    updateData: function (t, a, n) {
                        if (t) {
                            var i = e.path([].concat(Object(l["a"])(t), ["value"])), s = i(this.data);
                            this.data = e.compose(e.dissoc("undefined"), e.over(e.lensPath(t), e.compose(e.merge(e.__, "form" === n ? a : {value: a}), e.defaultTo({}))))(this.data);
                            var o = i(this.data), r = e.dissoc("meta"),
                                c = e.contains(n, ["facebookFeed", "instagramFeed", "googleSearch"]);
                            if (c && !e.equals(r(s), r(o))) {
                                var p = o,
                                    d = e.compose(e.anyPass([e.equals(p), e.pathEq(["meta", "adEdited"], !0), C["m"], C["t"]]), e.defaultTo({}));
                                this.data = e.over(e.lensProp("ads"), e.map(function (e) {
                                    var t = e.value;
                                    return d(t) ? e : Object(u["a"])({}, e, {value: Object(C["w"])(p, t)})
                                }), this.data)
                            }
                            this.save()
                        }
                    },
                    next: function (t) {
                        var a = this;
                        if (t && this.nextEnabled) {
                            this.data = e.over(e.lensPath(t), e.compose(e.merge(e.__, {skip: !1}), e.defaultTo({})), this.data);
                            var n = this.currentConfig.key;
                            "summary" === n ? (this.pending.summary = !0, this.startOnBoarding().then(function () {
                                a.pending.summary = !1, a.incIndex(), a.save(), a.$ga.event({
                                    eventCategory: "App",
                                    eventAction: "Campaign",
                                    eventValue: 1,
                                    eventLabel: "ExecuteCampaign"
                                }), window.fbq("trackCustom", "ExecuteCampaign"), a.$intercom.trackEvent("ExecuteCampaign")
                            }).catch(function (t) {
                                a.pending.summary = !1;
                                var n = e.path(["response", "data", "code"])(t);
                                "NO_CREDIT_CARD" === n && alert(a.$t("PROPOSAL.launch_check_payment"))
                            })) : "webOrApp" === n ? "form" === this.webOrApp ? this.incIndex() : this.index += 2 : "form" === n ? this.incIndex() : this.isLast ? (this.save(), this.$router.push({name: "MyCampaigns"})) : (this.incIndex(), this.save())
                        }
                    },
                    go: function (t) {
                        var a = t.key, n = t.from, i = Q.findIndex(e.propEq("key", a));
                        i >= 0 && this.index !== i && (this.index = i, this.from = n)
                    },
                    save: function () {
                        this.save$.next({data: this.processData(this.data), currentKey: this.currentConfig.key})
                    },
                    startOnBoarding: function () {
                        var e = Object(o["a"])(regeneratorRuntime.mark(function e() {
                            var t;
                            return regeneratorRuntime.wrap(function (e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        return e.next = 2, p["d"].startOnBoardingCampaign(this.campaignId);
                                    case 2:
                                        t = e.sent, this.campaign = t, this.setCampaign(t);
                                    case 5:
                                    case"end":
                                        return e.stop()
                                }
                            }, e, this)
                        }));

                        function t() {
                            return e.apply(this, arguments)
                        }

                        return t
                    }(),
                    setQuery: function (t, a) {
                        var n = Q[t].key, i = e.path(["$route", "query"], this), s = this.getCampaignId;
                        if (n && (i.c !== n || !s)) {
                            var o = {id: this.campaignId},
                                r = {name: "Onboarding", params: o, query: Object(u["a"])({}, i, {c: n, f: a, idx: t})};
                            this.$route.query.idx == t ? this.$router.replace(r) : this.$router.push(r), this.from = null
                        }
                    },
                    watchIndex$: function () {
                        var t = this, a = this.$watchAsObservable("index", {immediate: !0}).pipe(Object(w["a"])()),
                            n = a.pipe(Object(k["a"])(function () {
                                t.animate = !1, t.$nextTick(function () {
                                    return t.animate = !0
                                })
                            }));
                        this.$subscribeTo(n, e.always(void 0)), this.$subscribeTo(a, function (a) {
                            var n, i = a.newValue, s = a.oldValue, o = t.index;
                            do {
                                var r = Q[o] || {}, c = e.defaultTo(F, r.show);
                                if (n = M(c(t.data)), n) break;
                                i - s > 0 ? o += 1 : o -= 1
                            } while (Object(B["E"])(o, 0, Q.length - 1) && !n);
                            n && t.pushToJourney(e.path([o, "key"], Q)), t.index !== o && (t.index = o);
                            var l = t.currentConfig.key, u = t.webOrApp;
                            "form" === l && "form" !== u && s - i > 0 && --t.index, t.setQuery(t.index, t.from)
                        })
                    },
                    watchArrow: function () {
                        var t = this,
                            a = Object(b["a"])(document, "keyup").pipe(Object(A["a"])(150), Object(N["a"])(e.both(e.compose(e.contains(e.__, [39, 37]), e.prop("keyCode")), e.compose(e.not, e.contains(e.__, ["TEXTAREA", "INPUT"]), e.toUpper, e.path(["target", "tagName"])))), Object($["a"])("keyCode"), Object(w["a"])(), Object(I["a"])(e.equals(37))),
                            n = Object(s["a"])(a, 2), i = n[0], o = n[1];
                        this.$subscribeTo(Object(g["a"])(i.pipe(Object(j["a"])(function () {
                            return t.decIndex
                        })), o.pipe(Object(j["a"])(function () {
                            return t.nextFunc
                        }))), e.call)
                    },
                    incIndex: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                        (this.nextEnabled.value || !0 === t) && (this.index = e.clamp(0, Q.length - 1, this.index + 1))
                    },
                    decIndex: function () {
                        var t = this.currentConfig.key;
                        "reviewProcess" != t && (this.index = e.clamp(0, Q.length - 1, this.index - 1))
                    },
                    handleCopy: function (t) {
                        var a = this.availableAds.map(e.prop("value")), n = a.indexOf(t),
                            i = e.findIndex(e.propEq("value", t), this.data.ads), s = a[e.dec(n)];
                        if (Object(B["N"])(s) && Object(B["N"])(t)) {
                            var o = Object(C["w"])(s, t);
                            Object(B["N"])(o) && this.updateData(["ads", i], o)
                        }
                    },
                    updateSpecificAd: function () {
                        var t, a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                            n = e.propEq("key"), i = e.find(e.__, Q);
                        Object(C["n"])(a) ? t = i(n("facebookFeed")) : Object(C["u"])(a) ? t = i(n("instagramFeed")) : Object(C["r"])(a) ? t = i(n("googleSearch")) : Object(C["q"])(a) ? t = i(n("googleDisplay")) : Object(C["t"])(a) ? t = i(n("googleUniversalApp")) : Object(C["m"])(a) && (t = i(n("appleSearch"))), Object(B["N"])(t) && this.updateData(t.updatePath(this.data), a)
                    },
                    updateFromAudience: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = t,
                            n = e.find(e.__, Q)(e.propEq("key")("audience")), i = e.propEq("name"),
                            s = a.find(i("location"));
                        this.data.locations.value = s.values;
                        var o = a.find(i("searchKeywords"));
                        this.data.keywords.value.keywords = o.values;
                        var r = a.find(i("audienceInterests"));
                        this.data.keywords.value.interests = r.values, Object(B["N"])(n) && this.updateData(n.updatePath(this.data), t, "audience")
                    },
                    handleComponentChange: E()(function (t) {
                        var a = this;
                        switch (t) {
                            case"adsList":
                                var n = this.data.ads || [], i = e.assocPath(["meta", "isEditing"], !0);
                                n.forEach(function (e, t) {
                                    var n = e.value;
                                    return a.updateData(["ads", t], i(n))
                                });
                                break;
                            default:
                                break
                        }
                    }, 500),
                    renderProgress: function (t) {
                        var a = this.$createElement, n = 1, i = 11, s = 8;
                        if (Object(B["E"])(t, n, i)) {
                            var o = e.path([t, "progress"], Q);
                            o = Math.floor(o / s * 100);
                            var r = {width: "".concat(o, "%")};
                            return a("div", {class: "onboarding-progressbar--wrapper"}, [a("div", {
                                class: "onboarding-progressbar--bar",
                                style: r
                            }), a("div", {class: "adriel-flex-center onboarding-progressbar--text"}, [this.$t("ONBOARDING.progress_text", {value: o})])])
                        }
                    },
                    previewVisible: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = t.value,
                            n = void 0 === a ? {} : a, i = arguments.length > 1 ? arguments[1] : void 0;
                        return e.contains(this.adType(n), i)
                    },
                    adType: e.cond([[C["u"], e.always("instagramFeed")], [C["n"], e.always("facebookFeed")], [C["q"], e.always("googleDisplay")], [C["r"], e.always("googleSearch")], [C["t"], e.always("googleUniversalApp")], [C["m"], e.always("appleSearch")]]),
                    pushToJourney: E()(function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "KEY_NOT_FOUND",
                            a = e.compose(e.prop("key"), e.last, e.path(["data", "journey"]))(this);
                        a !== t && (this.data.journey.push({
                            key: t,
                            timestamp: new Date,
                            meta: S.parse(window.navigator.userAgent)
                        }), this.save())
                    }, 100),
                    processData: function (t) {
                        return t = e.over(e.lensProp("ads"), e.map(function (e) {
                            var t = e.skip, a = e.value;
                            return {skip: t, value: Object(C["i"])(a)}
                        }), t), i()(this.data, t)
                    }
                }),
                beforeDestroy: function () {
                    this.$intercom.update({hide_default_launcher: !1})
                },
                mounted: function () {
                    this.$subscribeTo(this.save$, e.identity, function (e) {
                        return console.log(e)
                    }), this.watchIndex$(), this.watchArrow()
                },
                created: function () {
                    var t = Object(o["a"])(regeneratorRuntime.mark(function t() {
                        var a, n, i, s, o = this;
                        return regeneratorRuntime.wrap(function (t) {
                            while (1) switch (t.prev = t.next) {
                                case 0:
                                    if (this.$subscribeTo(Object(y["a"])(0, 500).pipe(Object(D["a"])(3)), function () {
                                            return o.$intercom.update({hide_default_launcher: !0})
                                        }), a = this.getCampaignId, !a) {
                                        t.next = 8;
                                        break
                                    }
                                    return t.next = 5, p["d"].get(a);
                                case 5:
                                    n = t.sent, t.next = 11;
                                    break;
                                case 8:
                                    return t.next = 10, p["d"].getOnBoardingCampaign();
                                case 10:
                                    n = t.sent;
                                case 11:
                                    if (n = n || {}, this.campaign = n, this.campaignId = n.id, i = n.onboarding, i) {
                                        t.next = 17;
                                        break
                                    }
                                    return t.abrupt("return");
                                case 17:
                                    if (U(i.data) && this.SET_DATA(["formCreate", i.data.form]), this.data = Object(u["a"])({}, this.data, i.data), s = e.path(["currentConfig", "key"], this), i.currentKey || a || !["webOrApp", "selection"].includes(s)) {
                                        t.next = 22;
                                        break
                                    }
                                    return t.abrupt("return");
                                case 22:
                                    this.index = Math.max(0, Q.findIndex(e.propEq("key", i.currentKey)));
                                case 23:
                                case"end":
                                    return t.stop()
                            }
                        }, t, this)
                    }));

                    function a() {
                        return t.apply(this, arguments)
                    }

                    return a
                }(),
                data: function () {
                    var e = this, t = (new O["a"]).pipe(Object(A["a"])(500), Object(T["a"])(function (t) {
                        return Object(_["a"])(function () {
                            return p["d"].updateOnBoarding(e.campaignId, t)
                        }).pipe(Object(P["a"])(Object(B["p"])()), Object(R["a"])(function () {
                            return Object(x["b"])()
                        }))
                    }));
                    return {
                        index: 0,
                        from: null,
                        animate: !1,
                        data: {
                            journey: [],
                            adEdited: !1,
                            audience: {
                                value: [{
                                    name: "gender",
                                    values: [{name: "male", value: !0}, {name: "female", value: !0}]
                                }, {name: "location", values: []}, {
                                    name: "audienceInterests",
                                    values: []
                                }, {
                                    name: "age",
                                    values: [{name: {max: 24, min: 18}, value: !0}, {
                                        name: {max: 34, min: 25},
                                        value: !0
                                    }, {name: {max: 44, min: 35}, value: !0}, {
                                        name: {max: 54, min: 45},
                                        value: !0
                                    }, {name: {max: 99, min: 55}, value: !0}]
                                }, {name: "searchKeywords", values: []}, {
                                    name: "income",
                                    values: [{name: "low", value: !0}, {name: "medium", value: !0}, {
                                        name: "high",
                                        value: !0
                                    }, {name: "top", value: !0}]
                                }, {
                                    name: "relationshipStatus",
                                    values: [{name: "single", value: !0}, {
                                        name: "relationship",
                                        value: !0
                                    }, {name: "married", value: !0}]
                                }, {
                                    name: "deviceTarget",
                                    values: [{name: "desktop", value: !0}, {name: "mobile", value: !0}]
                                }]
                            },
                            ads: [{
                                skip: !1,
                                value: {
                                    channel: "facebook",
                                    creativeType: "photo",
                                    creative: {media: "", description: ""}
                                }
                            }, {
                                skip: !1,
                                value: {
                                    channel: "instagram",
                                    creativeType: "photo",
                                    creative: {media: "", description: ""}
                                }
                            }, {
                                skip: !1,
                                value: {
                                    channel: "google",
                                    creativeType: "ResponsiveDisplayAd",
                                    creative: {
                                        image: "",
                                        squareImage: "",
                                        shortTitle: "",
                                        description: "",
                                        businessName: ""
                                    }
                                }
                            }, {
                                skip: !1,
                                value: {
                                    channel: "google",
                                    creativeType: "ExpandedTextAd",
                                    creative: {
                                        title1: "",
                                        title2: "",
                                        title3: "",
                                        description: "",
                                        description2: "",
                                        url: ""
                                    }
                                }
                            }, {
                                skip: !1,
                                value: {
                                    channel: "google",
                                    creativeType: "universalApp",
                                    creative: {description1: "", description2: "", description3: "", description4: ""}
                                }
                            }, {
                                skip: !1,
                                value: {
                                    channel: "apple",
                                    creativeType: "search",
                                    creative: {
                                        mobileAppInfo: {
                                            title: "",
                                            screenshotUrls: "",
                                            logo: "",
                                            rating: "",
                                            ratingCount: ""
                                        }
                                    }
                                }
                            }],
                            form: {}
                        },
                        campaignId: void 0,
                        pending: {ads: !0, keywords: !1},
                        save$: t,
                        campaign: {}
                    }
                }
            }
        }.call(this, a("b17e"))
    }, c3d6: function (e, t, a) {
    }, c7c5: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), i = a.n(n), s = a("5d73"), o = a.n(s), r = a("cebc"), c = a("4866"), l = a("e393"),
                u = a("ba2c"), p = a("3d65"), d = a("a772"), h = a("dde5"), f = a("2f62"), v = a("2fd4"),
                m = (a("97ec"), function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = e.channel,
                        a = e.creativeType;
                    switch (t) {
                        case"facebook":
                        case"instagram":
                            return "".concat(t, "Feed");
                        case"google":
                            if ("ResponsiveDisplayAd" === a) return "googleDisplay";
                            if ("ExpandedTextAd" === a) return "googleSearch";
                            if ("universalApp" === a) return "googleUniversalApp";
                            break;
                        case"apple":
                            if ("search" === a) return "appleSearch";
                            break;
                        default:
                            break
                    }
                });
            t["a"] = {
                name: "FullPage",
                props: {
                    childType: String,
                    showLang: {type: Boolean, default: !1},
                    data: {
                        type: Object, default: function () {
                            return {}
                        }
                    },
                    type: {type: String, defulat: "web"},
                    isAppType: Boolean
                },
                data: function () {
                    return {dirty: !1, animateCheckbox: !1}
                },
                render: function () {
                    var e = this, t = arguments[0];
                    return t("div", {class: "onboarding-full-page ".concat(this.childType)}, [t("span", {
                        class: "onboarding-full-page--logo",
                        on: {
                            click: function () {
                                return e.pushToRoute("/myCampaigns")
                            }
                        }
                    }, [t("img", {attrs: {src: "/img/logos/logo_n_f.svg"}})]), this.renderLang(this.showLang), t(v["a"], {attrs: {isFormType: this.isFormType}}), t("div", {class: "onboarding-full-page--contents"}, [this.renderChild(this.childType)])])
                },
                computed: {
                    adTypes: e.compose(e.map(e.path(["value", "channel"])), e.filter(e.path(["value", "enabled"])), e.pathOr([], ["data", "availableAds"])),
                    isFormType: e.propEq("type", "form")
                },
                methods: Object(r["a"])({}, Object(f["mapMutations"])("user", ["login"]), {
                    next: function () {
                        if ("adsList" === this.childType) {
                            var t = !0;
                            this.dirty = !0;
                            var a = e.contains(e.__, this.data.adTypes.value),
                                n = this.data.ads.filter(e.both(e.path(["value", "enabled"]), e.pipe(e.prop("value"), m, a)));
                            if (!n.length) return this.animateCheckbox = !0, this.toast(this.$t("ONBOARDING.adsList_at_least_one"));
                            var i = !0, s = !1, r = void 0;
                            try {
                                for (var c, l = o()(n); !(i = (c = l.next()).done); i = !0) {
                                    var u = c.value, p = u.value, h = Object(d["h"])(p), f = h.value, v = h.reasons,
                                        b = void 0 === v ? [{index: 0}] : v;
                                    f || (t = !1, p.meta.isEditing = !0, p.meta.cardIndex = b[0].index)
                                }
                            } catch (g) {
                                s = !0, r = g
                            } finally {
                                try {
                                    i || null == l.return || l.return()
                                } finally {
                                    if (s) throw r
                                }
                            }
                            t && (this.$emit("next"), this.dirty = !1)
                        } else this.$emit("next")
                    }, renderChild: function (t) {
                        var a = this, n = this.$createElement, s = {
                            attrs: {
                                data: this.data,
                                dirty: this.dirty,
                                animateCheckbox: this.animateCheckbox,
                                type: this.type
                            }, on: {
                                next: this.next, skip: function () {
                                    return a.$emit("skip")
                                }, prev: function () {
                                    return a.$emit("prev")
                                }, go: function (e) {
                                    return a.$emit("go", e)
                                }, input: function () {
                                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                    !0 === e.enabled && (a.animateCheckbox = !1), a.$emit("input", e)
                                }, "update:campaign": function (e) {
                                    return a.$emit("update:campaign", e)
                                }
                            }
                        };
                        switch (t) {
                            case"selection":
                                return n(c["a"], i()([{}, s]));
                            case"reviewProcess":
                                return s.attrs.hasFb = e.any(e.test(/(facebook|instagram)/gi))(this.adTypes), n(l["a"], i()([{}, s]));
                            case"adsList":
                                return n(u["a"], i()([{}, s, {
                                    on: {
                                        copy: function (e) {
                                            return a.$emit("copy", e)
                                        }
                                    }
                                }]));
                            case"finish":
                                return n(p["a"], i()([{}, s]));
                            default:
                                break
                        }
                    }, renderLang: function (t) {
                        var a = this, n = this.$createElement;
                        if (t) {
                            var i = "ko" === e.path(["globalUser", "language"], this);
                            return n("div", {class: "onboarding-full-page--lang"}, [n("span", {
                                class: i ? "" : "selected",
                                on: {
                                    click: function () {
                                        return a.updateLang("en")
                                    }
                                }
                            }, ["ENG"]), n("span", {
                                class: i ? "selected" : "", on: {
                                    click: function () {
                                        return a.updateLang("ko")
                                    }
                                }
                            }, ["KOR"])])
                        }
                    }, updateLang: function (t) {
                        var a = this, n = this.globalUser, i = Object(r["a"])({}, n, {language: t});
                        this.login(i), h["o"].update(i).then(e.identity).catch(function (e) {
                            console.log(e), a.login(n)
                        })
                    }
                })
            }
        }).call(this, a("b17e"))
    }, ccfc: function (e, t, a) {
        "use strict";
        var n = a("2638"), i = a.n(n), s = (a("7f7f"), a("cebc")), o = (a("c5f6"), a("172d")), r = a("fa7d"),
            c = (a("ad04"), function (e) {
                return "onboarding-fb-app--".concat(e)
            });
        t["a"] = {
            name: "FbApp", data: function () {
                return {isModalActive: !1}
            }, props: {
                value: {
                    type: Object, default: function () {
                        return {}
                    }
                }, campaignId: String | Number
            }, computed: {
                _value: function () {
                    return this.value
                }
            }, render: function (e) {
                return e("div", {class: c("container")}, [this.renderModal(), Object(r["N"])(this.value) ? this.renderContent() : this.renderInput()])
            }, methods: {
                renderInput: function () {
                    var e = this, t = this.$createElement;
                    return t("div", {class: c("input-type")}, [t("div", {
                        class: c("title"),
                        domProps: Object(s["a"])({}, {innerHTML: this.$t("COMMON.fb_app_box_content")})
                    }), t("button", {
                        class: "adriel-flex-center ".concat(c("btn")), on: {
                            click: function () {
                                return e.isModalActive = !0
                            }
                        }
                    }, [t("img", {attrs: {src: "/img/proposal/login_pixel_icon.png"}}), t("span", [this.$t("COMMON.fb_share_app")])])])
                }, renderContent: function () {
                    var e = this, t = this.$createElement, a = this._value;
                    return t("div", {class: c("content-type")}, [t("span", {class: "content-title"}, [this.$t("PROPOSAL.ap_card_tracker_fb_app_info")]), t("div", [t("span", {class: "page-elem-box"}, [t("span", {class: "adriel-ellipsis"}, [t("img", {attrs: {src: "/img/proposal/facebook_icon.png"}}), t("span", {class: "page-name"}, [a.name || "-"]), t("span", {class: "page-url adriel-ellipsis"}, [" |  " + (a.propertyId || a.id || "-")])]), t("img", {
                        attrs: {src: "/img/proposal/profile_delete_icon_dark.png"},
                        class: "delete-icon",
                        on: {
                            click: function () {
                                return e.update({})
                            }
                        }
                    })])])])
                }, renderModal: function () {
                    var e = this, t = this.$createElement;
                    if (this.isModalActive) {
                        var a = {
                            attrs: {active: !0, campaignIdProp: this.campaignId},
                            on: {
                                "update:active": function (t) {
                                    return e.isModalActive = t
                                }, confirm: function () {
                                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                    return e.update(t)
                                }
                            }
                        };
                        return t(o["a"], i()([{}, a]))
                    }
                }, update: function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    this.$emit("input", e)
                }
            }
        }
    }, d62a: function (e, t, a) {
        "use strict";
        var n = a("ec09"), i = a.n(n);
        i.a
    }, d707: function (e, t, a) {
        "use strict";
        var n = a("77fb"), i = a.n(n);
        i.a
    }, d81a: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("cebc"), i = a("2f62");
            a("dde5"), e.max(0);
            t["a"] = {
                created: function () {
                    this.compFacebookProfile = e.clone(this.facebookProfile) || {}
                },
                props: {optional: {type: Boolean, default: !1}},
                data: function () {
                    return {compFacebookProfile: {}, isWarning: !1, addInstaPage: !1}
                },
                computed: Object(n["a"])({}, Object(i["mapGetters"])("user", ["fbInfo"]), Object(i["mapState"])("user", ["permissionsMissing"]), {
                    _compFacebookProfile: {
                        get: e.prop("compFacebookProfile"),
                        set: function (e) {
                            this.compFacebookProfile = e
                        }
                    },
                    pages: e.pathOr([], ["fbInfo", "pages"]),
                    selectablePages: e.compose(e.filter(e.prop("selectable")), e.prop("pages")),
                    unselectablePages: e.compose(e.reject(e.prop("selectable")), e.prop("pages")),
                    status: function () {
                        return e.cond([[e.pathSatisfies(e.equals(0), ["pages", "length"]), e.always("noPages")], [e.isEmpty, e.always("notAuth")], [e.T, e.always("pages")]])(this.fbInfo)
                    },
                    instaPage: function () {
                        return this._compFacebookProfile ? this._compFacebookProfile.instagram : null
                    }
                }),
                components: {},
                methods: Object(n["a"])({}, Object(i["mapActions"])("user", ["fetchFacebookProfile"]), {
                    radioSelected: function (t) {
                        this.compFacebookProfile = t, this.$emit("update", {
                            facebookPageId: t.id,
                            facebookUserToken: e.path(["fbInfo", "longLifeToken"], this),
                            instagram: this.instaPage
                        })
                    }
                })
            }
        }).call(this, a("b17e"))
    }, dcee: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), i = a.n(n), s = a("7618"), o = (a("6762"), a("2fdb"), a("cebc")), r = a("97a6"),
                c = a("8a23"), l = a("6c20"), u = a("40f9"), p = a("0b23"), d = a("c0bb"), h = a("9eeb"), f = a("577e"),
                v = a("28c8"), m = a("4e38"), b = a("2379"), g = a("2dc3"), y = a("3ec5"), O = a("ccfc"), _ = a("8bd7"),
                x = a("51c6"), w = a("a772"), k = a("2fd4"), A = (a("1268"), a("2f62")), N = A.mapGetters,
                $ = A.mapActions, I = function () {
                    return a.e("chunk-b1925690").then(a.bind(null, "146f"))
                };
            t["a"] = {
                name: "InputContainer",
                props: {
                    title: String,
                    contents: {type: Array, default: e.always([])},
                    data: {type: Object, default: e.always({})},
                    childType: String,
                    type: {type: String, defulat: "web"},
                    isLast: {type: Boolean, default: !1}
                },
                watch: {
                    data: {
                        handler: function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = e.value;
                            this._value = t
                        }, immediate: !0, deep: !0
                    }
                },
                data: function () {
                    return {
                        _value: void 0,
                        campaignId: void 0,
                        isTermsActive: !1,
                        termsType: "",
                        isPopoverVisible: !1,
                        isModalVisible: !1,
                        isFormDirty: !1,
                        from: null
                    }
                },
                computed: Object(o["a"])({}, N("user", ["hasPaymentMethod"]), {
                    isAdType: function () {
                        return ["facebookFeed", "instagramFeed", "googleDisplay", "googleSearch", "googleUniversalApp", "appleSearch"].includes(this.childType)
                    },
                    adTypes: e.compose(e.map(e.path(["value", "channel"])), e.filter(e.path(["value", "enabled"])), e.pathOr([], ["data", "availableAds"])),
                    showGoToReview: function () {
                        return this.from && !["facebookPage", "tracker", "objectives", "summaryAndPayment"].includes(this.childType)
                    },
                    nextText: function () {
                        if (this.isLast) return this.$t("ONBOARDING.done");
                        switch (this.childType) {
                            case"summaryAndPayment":
                                return this.$t("ONBOARDING.launch_campaign");
                            default:
                                return this.$t("ONBOARDING.next")
                        }
                    },
                    skipText: function () {
                        switch (this.childType) {
                            case"facebookFeed":
                            case"instagramFeed":
                            case"googleDisplay":
                            case"googleSearch":
                            case"googleUniversalApp":
                            case"appleSearch":
                                return this.$t("ONBOARDING.save_and_skip");
                            case"noAds":
                                return this.$t("ONBOARDING.save_draft_and_continue");
                            case"facebookPage":
                            case"tracker":
                            case"objectives":
                                return this.$t("ONBOARDING.do_it_later");
                            default:
                                return this.$t("ONBOARDING.skip")
                        }
                    },
                    popover: e.path(["data", "popover"]),
                    modal: e.path(["data", "modal"]),
                    isApp: e.pathEq(["data", "allData", "webOrApp", "value", "type"], "app"),
                    isFormType: e.propEq("type", "form")
                }),
                render: function () {
                    var e = this, t = arguments[0];
                    return t("div", {class: "onboarding-input--container ".concat(this.childType)}, [this.renderTermsModal(), t("div", {class: "onboarding-input--first"}, [t("span", {
                        class: "logo-container",
                        on: {
                            click: function () {
                                return e.pushToRoute("/myCampaigns")
                            }
                        }
                    }, [t("img", {
                        class: "big-logo",
                        attrs: {src: "/img/logos/logo_w_f.svg"}
                    }), t("img", {
                        class: "small-logo",
                        attrs: {src: "/img/logos/logo_n_f.png"}
                    })]), t(k["a"], {
                        attrs: {
                            className: "isMobile",
                            isFormType: this.isFormType
                        }
                    }), this.showGoToReview && t("span", {
                        class: "adriel-flex-center go-to-review-payment go-to-review-payment--mobile",
                        on: {
                            click: function () {
                                e.from = null, e.$emit("go", {key: "summary"})
                            }
                        }
                    }, [t("span", {class: "text-span"}, [this.$t("ONBOARDING.go_to_review_and_payment")]), t("b-icon", {
                        attrs: {
                            pack: "fal",
                            icon: "arrow-right"
                        }
                    })]), t("div", {class: "onboarding---mobile-nav-container"}, [this.renderButton("prev"), !1 !== this.data.skippable && this.renderButton("skip"), !1 !== this.data.nextable && this.renderButton("next"), this.renderWarning(), this.renderMobileDirty()]), t("div", {class: "onboarding-input--contents"}, [t("span", {
                        attrs: Object(o["a"])({}, {class: "title-span break-word"}),
                        domProps: Object(o["a"])({}, {innerHTML: this.$t(this.title)})
                    }), t("ul", {class: "contents-ul"}, [this.renderContents()])]), this.renderButton("prev")]), t("div", {class: "adriel-flex-center onboarding-input--second"}, [this.renderModal(), this.renderPopover(), t(k["a"], {
                        attrs: {
                            className: "isWeb",
                            isFormType: this.isFormType
                        }
                    }), t("div", {class: "child-component-container"}, [t("div", {class: "child-component ".concat(this.childType)}, [this.renderChild(this.childType)])]), t("div", {class: "onboarding-nav-buttons"}, [this.showGoToReview && t("span", {
                        class: "adriel-flex-center go-to-review-payment",
                        on: {
                            click: function () {
                                e.from = null, e.$emit("go", {key: "summary"})
                            }
                        }
                    }, [t("span", {class: "text-span"}, [this.$t("ONBOARDING.go_to_review_and_payment")]), t("b-icon", {
                        attrs: {
                            pack: "fal",
                            icon: "arrow-right"
                        }
                    })]), t("div", {class: "onboarding-nav-buttons-container"}, [this.renderWarning(), !1 !== this.data.skippable && this.renderButton("skip"), !1 !== this.data.nextable && this.renderButton("next")])])]), t("div", {class: "contents-ul-bottom"}, [this.renderModal(), this.renderPopover(), t("ul", {class: "contents-ul"}, [this.renderContents()])])])
                },
                methods: Object(o["a"])({}, $("form", ["clearData"]), {
                    renderModal: function () {
                        var e = this, t = this.$createElement, a = this.data, n = a.modal, o = a.modal_title,
                            r = a.modalType, c = void 0 === r ? "modal" : r;
                        if (n) {
                            var l, u = {
                                attrs: {active: this.isModalVisible}, on: {
                                    "update:active": function (t) {
                                        return e.isModalVisible = t
                                    }, confirm: function () {
                                        return e.isModalVisible = !1
                                    }
                                }
                            };
                            if ("modal" === c) l = t(n, i()([{}, u])); else {
                                var p = {
                                    acttrs: {
                                        hasCancelBtn: !1,
                                        headerText: o ? this.$t(o) : "",
                                        confirmText: this.$t("COMMON.close")
                                    }, scopedSlots: {
                                        default: function () {
                                            return "object" === Object(s["a"])(n) ? t(n) : t("div", {domProps: {innerHTML: e.$t(n)}})
                                        }
                                    }
                                };
                                l = t(x["a"], i()([{}, u, {}, p]))
                            }
                            return t("div", {class: "popover-container"}, [t("b-icon", {
                                attrs: {
                                    pack: "fas",
                                    icon: "question-circle"
                                }, nativeOn: {
                                    click: function () {
                                        return e.isModalVisible = !e.isModalVisible
                                    }
                                }
                            }), l])
                        }
                    }, renderPopover: function () {
                        var e = this, t = this.$createElement, a = this.data.popover;
                        if (a) {
                            var n = {attrs: {class: "center"}, domProps: {innerHTML: this.$t(a)}};
                            return t("div", {class: "popover-container"}, [t("b-icon", {
                                attrs: {
                                    pack: "fas",
                                    icon: "question-circle"
                                }, nativeOn: {
                                    click: function () {
                                        return e.isPopoverVisible = !e.isPopoverVisible
                                    }
                                }
                            }), this.isPopoverVisible && t("div", {class: "popover-content"}, [t("b-icon", {
                                attrs: {
                                    pack: "fas",
                                    icon: "times"
                                }, class: "close", nativeOn: {
                                    click: function () {
                                        return e.isPopoverVisible = !1
                                    }
                                }
                            }), t("div", i()([{}, n]))])])
                        }
                    }, renderContents: function () {
                        var e = this, t = this.$createElement;
                        return this.contents.map(function (i) {
                            return n(e.$t(i)) ? t("li", {domProps: Object(o["a"])({}, {innerHTML: e.$t(i)})}) : t("li", [a(e.$t(i))])
                        });

                        function a() {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                            return e = e.trim(), /[.|��]$/gi.test(e) ? e : "".concat(e, ".")
                        }

                        function n(e) {
                            return 0 == e.indexOf("<")
                        }
                    }, renderChild: function (t) {
                        var a = this, n = this.$createElement, s = {
                            attrs: Object(o["a"])({
                                value: this._value,
                                dirty: this.isFormDirty,
                                isApp: this.isApp,
                                data: this.data.allData,
                                type: this.type
                            }, e.pick(["campaignId", "campaign", "show", "pending"], this.data)),
                            on: {
                                go: function (e) {
                                    a.from = t, a.$emit("go", Object(o["a"])({}, e, {from: t}))
                                }, input: function (e) {
                                    return a.$emit("input", e)
                                }, next: function (e) {
                                    return a.nextFunc(e)
                                }, "update:edit": function (e) {
                                    return a.isEditing = e
                                }
                            }
                        };
                        switch (t) {
                            case"webOrApp":
                                return n(r["a"], i()([{}, s]));
                            case"budget":
                                return n(c["a"], i()([{}, s]));
                            case"keyword":
                                return n(u["a"], i()([{}, s]));
                            case"audience":
                                return n(p["a"], i()([{}, s]));
                            case"location":
                                return n(l["a"], i()([{}, s]));
                            case"form":
                                return s.attrs.page = "formCreate", n(_["default"], i()([{}, s]));
                            case"schedule":
                                return n(d["a"], i()([{}, s]));
                            case"adTypes":
                                return n(h["a"], i()([{}, s]));
                            case"noAds":
                                return n(f["a"], i()([{}, s]));
                            case"summaryAndPayment":
                                return s.attrs.data = this.data.allData, n(v["a"], i()([{}, s]));
                            case"facebookPage":
                                return n(m["a"], i()([{}, s]));
                            case"tracker":
                                return s.attrs.hasFb = e.any(e.test(/(facebook|instagram)/gi))(this.adTypes), n(b["a"], i()([{}, s]));
                            case"objectives":
                                return n(g["a"], i()([{}, s]));
                            case"googleSearch":
                            case"googleDisplay":
                            case"instagramFeed":
                            case"facebookFeed":
                            case"googleUniversalApp":
                            case"appleSearch":
                                var x = e.pathOr([], ["data", "allData", "ads"], this),
                                    k = e.contains(e.__, e.pathOr([], ["data", "allData", "adTypes", "value"], this));
                                return x = x.filter(function (e) {
                                    var t, a = e.value;
                                    return Object(w["n"])(a) ? t = "facebookFeed" : Object(w["u"])(a) ? t = "instagramFeed" : Object(w["r"])(a) ? t = "googleSearch" : Object(w["q"])(a) ? t = "googleDisplay" : Object(w["t"])(a) ? t = "googleUniversalApp" : Object(w["m"])(a) && (t = "appleSearch"), k(t)
                                }).map(e.prop("value")), n(y["a"], i()([{}, s, {
                                    attrs: Object(o["a"])({}, {
                                        ad: this._value,
                                        mode: "standard",
                                        enablable: !1,
                                        showPopover: !1
                                    })
                                }]));
                            case"fbApp":
                                return n(O["a"], i()([{}, s]))
                        }
                    }, renderButton: function (e) {
                        var t, a, n, s = this, o = this.$createElement;
                        switch (e) {
                            case"prev":
                                t = "nav-btn prev", a = function () {
                                    return s.$emit("prev")
                                }, n = o("span", [o("b-icon", {
                                    attrs: {
                                        pack: "fal",
                                        icon: "arrow-left"
                                    }
                                }), o("span", {class: "text-span"}, [this.$t("ONBOARDING.back")])]);
                                break;
                            case"skip":
                                t = "nav-btn skip", a = function () {
                                    return s.$emit("skip")
                                }, n = this.skipText;
                                break;
                            case"next":
                                t = "nav-btn next", a = function (e) {
                                    return s.nextFunc(e)
                                }, n = o("span", {class: "adriel-flex-center"}, [o("span", {class: "text-span"}, [this.nextText]), o("b-icon", {
                                    attrs: {
                                        pack: "fal",
                                        icon: "arrow-right"
                                    }
                                })]);
                                break
                        }
                        var r = {attrs: {class: t}, on: {click: a}};
                        return "summaryAndPayment" === this.childType && (this.hasPaymentMethod || (r.attrs.class += " disabled", r.directives = [{
                            name: "tooltip",
                            value: {content: this.$t("PROPOSAL.launch_check_payment")}
                        }])), o("button", i()([{}, r]), [n])
                    }, renderWarning: function () {
                        var e = this, t = this.$createElement;
                        if ("summaryAndPayment" === this.childType) return t("span", {class: "launch-warning"}, [this.$t("PROPOSAL.terms_services_1"), t("br"), this.$t("PROPOSAL.terms_services_2"), " ", t("a", {
                            on: {
                                click: function () {
                                    return e.showTerms("terms")
                                }
                            }
                        }, [this.$t("PROPOSAL.terms_services_terms")]), " ", this.$t("PROPOSAL.terms_services_3"), " ", t("a", {
                            on: {
                                click: function () {
                                    return e.showTerms("policy")
                                }
                            }
                        }, [this.$t("PROPOSAL.terms_services_policy")]), " ", this.$t("PROPOSAL.terms_services_4")])
                    }, showTerms: function (e) {
                        this.termsType = e, this.isTermsActive = !0
                    }, renderTermsModal: function () {
                        var e = this, t = this.$createElement;
                        if (this.isTermsActive) {
                            var a = {
                                attrs: {type: this.termsType, active: this.isTermsActive},
                                on: {
                                    "update:active": function (t) {
                                        return e.isTermsActive = t
                                    }
                                }
                            };
                            return t(I, i()([{}, a]))
                        }
                    }, nextFunc: function (e) {
                        var t = this.data.nextEnabled;
                        if (this.isAdType) {
                            var a = Object(w["h"])(this.data.value), n = a.value, i = a.reasons;
                            n || (this.data.value.meta.isEditing = !0, this.data.value.meta.cardIndex = i[0].index), this.isFormDirty = !0, t && (this.$emit("next", e), this.data.value.meta.isEditing = !1, this.isFormDirty = !1)
                        } else t ? (this.$emit("next", e), this.isFormDirty = !1) : this.isFormDirty = !0
                    }, renderMobileDirty: function () {
                        var e = this.$createElement, t = this.windowDimension, a = this.isFormDirty;
                        if (!(t.width > 800) && a) return e("div", {class: "onboarding-mobile-ad-dirty"}, [this.$t("ONBOARDING.ads_list_mobile_dirty")])
                    }
                })
            }
        }).call(this, a("b17e"))
    }, e393: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("cebc");
            a("3d6e");
            t["a"] = {
                name: "ReviewProcess",
                props: {hasFb: Boolean, type: {type: String, default: "web"}},
                computed: {
                    isAppType: e.propEq("type", "app"),
                    isFormType: e.propEq("type", "form"),
                    buttonText: function () {
                        return this.hasFb ? this.$t("ONBOARDING.review_process_further_facebook") : this.isAppType || this.isFormType ? this.$t("ONBOARDING.review_process_further_audience") : this.$t("ONBOARDING.review_process_further_ga")
                    },
                    processAdditional: function () {
                        return this.hasFb ? this.$t("ONBOARDING.review_process_additional_facebook") : this.isAppType || this.isFormType ? this.$t("ONBOARDING.review_process_additional_audience") : this.$t("ONBOARDING.review_process_additional_ga")
                    }
                },
                render: function () {
                    var e = this, t = arguments[0];
                    return t("div", {class: "onboarding-review-process"}, [t("div", {class: "onboarding-review-process--contents"}, [t("div", {class: "top-title"}, [this.$t("ONBOARDING.review_process_title")]), t("div", {class: "details"}, [this.$t("ONBOARDING.review_process_details")]), t("div", {class: "adriel-flex-center circles-container"}, [t("div", {class: "adriel-flex-center circle-each"}, [this.$t("ONBOARDING.review_process_step_1")]), t("b-icon", {
                        attrs: {
                            pack: "fas",
                            icon: "caret-right"
                        }, style: {"font-size": "22px", color: "#5aabe3"}
                    }), t("div", {class: "adriel-flex-center circle-each"}, [this.$t("ONBOARDING.review_process_step_2")]), t("b-icon", {
                        attrs: {
                            pack: "fas",
                            icon: "caret-right"
                        }, style: {"font-size": "22px", color: "#5aabe3"}
                    }), t("div", {class: "adriel-flex-center circle-each"}, [this.$t("ONBOARDING.review_process_step_3")])]), t("div", {
                        class: "additional",
                        domProps: Object(n["a"])({}, {innerHTML: this.processAdditional})
                    }), t("button", {
                        class: "adriel-flex-center next-btn", on: {
                            click: function () {
                                return e.$emit("next")
                            }
                        }
                    }, [t("span", {class: "btn-text"}, [this.buttonText]), t("b-icon", {
                        attrs: {
                            pack: "fas",
                            icon: "chevron-right"
                        }
                    })]), t("span", {
                        class: "not-now", on: {
                            click: function () {
                                return e.$emit("skip")
                            }
                        }
                    }, [this.$t("ONBOARDING.review_process_later")])])])
                }
            }
        }).call(this, a("b17e"))
    }, ec09: function (e, t, a) {
    }, ed3b2: function (e, t, a) {
    }
}]);
//# sourceMappingURL=onboarding.8131026d.js.map