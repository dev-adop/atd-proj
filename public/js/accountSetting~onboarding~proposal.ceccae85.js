(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["accountSetting~onboarding~proposal"], {
    1077: function (t, e, n) {
    }, 4379: function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("2638"), s = n.n(a), i = (n("a481"), n("cebc")), r = n("2f62"), o = n("dde5"), c = n("6510");
            n("4406");
            e["a"] = {
                name: "BusinessNumberModal",
                props: {type: {type: String, default: "inline"}, highlight: {type: Boolean, default: !1}},
                data: function () {
                    return {_inputValue: "", isEditing: !1}
                },
                watch: {
                    value: {
                        immediate: !0, handler: function (t) {
                            this._value = t
                        }
                    }
                },
                render: function () {
                    var t = this, e = arguments[0], n = ["business-cert-box", this.type];
                    return this.highlight && n.push("highlight"), e("div", {class: n.join(" ")}, [this.bizNotInKorea ? "" : this.renderTop(this._isEditingCert), this.bizNotInKorea ? "" : e("br"), e("b-checkbox", {
                        attrs: {value: this.bizNotInKorea},
                        nativeOn: {
                            input: function (e) {
                                var n = e.target.checked;
                                t.bizNotInKorea != n && (t._inputValue = n ? "NOT_IN_KOREA" : "", t.update(n))
                            }
                        }
                    }, [e("b", [this.$t("VAT.not_in_korea")])]), e("div", {class: "bottom-area"}, [e("img", {attrs: {src: "/img/icons/tooltip_icon.png"}}), e("span", {domProps: {innerHTML: this.$t("VAT.vat_description")}})]), "modal-type" === this.type && e(c["a"], {
                        attrs: {
                            "confirm-text": this.$t("COMMON.ok"),
                            "cancel-text": this.$t("COMMON.cancel")
                        }, on: {
                            cancel: function () {
                                return t.$emit("cancel")
                            }, confirm: function () {
                                return t.update()
                            }
                        }
                    })])
                },
                methods: Object(i["a"])({}, Object(r["mapActions"])("user", ["updateBusinessNumber"]), {
                    uploadBusinessCert: function () {
                        var t = this;
                        this.businessCertModal = !1, o["o"].submitBusinessCertification(this.certFile).then(function () {
                            t.checkSession(!0), t.certFile = null
                        }).catch(function (t) {
                            console.error(t), alert("fail to upload")
                        })
                    }, renderTop: function (t) {
                        var e = this, n = this.$createElement;
                        if (t) {
                            var a = {
                                attrs: {
                                    class: "cert-file-modal--input",
                                    placeholder: "000-00-00000",
                                    value: this._inputValue || this.bizNo
                                }, directives: [{name: "mask", value: "###-##-#####"}], on: {
                                    input: function (t) {
                                        e._inputValue = t.target.value.replace(/[^\d|^-]/gi, ""), e.$forceUpdate()
                                    }
                                }
                            };
                            return n("div", {class: "top-area edit-mode"}, ["inline" === this.type && n("span", {class: "top-area--title"}, [this.$t("VAT.business_no")]), n("input", s()([{}, a])), "inline" === this.type && n("button", {
                                on: {
                                    click: function () {
                                        return e.update()
                                    }
                                }
                            }, [this.$t("COMMON.submit")])])
                        }
                        return n("div", {class: "top-area"}, ["inline" === this.type && n("span", {class: "top-area--title"}, [this.$t("VAT.business_no")]), n("span", {
                            class: "top-area--number",
                            on: {
                                click: function () {
                                    return e.isEditing = !0
                                }
                            }
                        }, [this.bizNo || "000-00-000"]), n("img", {
                            attrs: {src: "/img/icons/edit_icon2.png"},
                            on: {
                                click: function () {
                                    return e.isEditing = !0
                                }
                            }
                        })])
                    }, update: function () {
                        var t = this, e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
                        this.updateBusinessNumber(this._inputValue).then(function () {
                            t.isEditing = !1, t._inputValue = "", e && t.$emit("complete")
                        })
                    }
                }),
                computed: {
                    _isEditingCert: function () {
                        return !this.bizNo || this.isEditing || "modal-type" === this.type
                    }, bizNo: t.path(["globalUser", "business_no"]), bizNotInKorea: function () {
                        return "NOT_IN_KOREA" === this.bizNo
                    }
                }
            }
        }).call(this, n("b17e"))
    }, 4406: function (t, e, n) {
    }, 6510: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "adriel-buttons-wrapper"}, [t._t("extra"), t.types.includes("cancel") ? n("button", {
                    staticClass: "cancel",
                    on: {
                        click: function (e) {
                            return t.$emit("cancel")
                        }
                    }
                }, [t._v("\n        " + t._s(t._cancelText) + "\n    ")]) : t._e(), t.types.includes("confirm") ? n("button", {
                    staticClass: "confirm",
                    on: {
                        click: function (e) {
                            return t.$emit("confirm")
                        }
                    }
                }, [t._v("\n        " + t._s(t._confirmText) + "\n    ")]) : t._e()], 2)
            }, s = [], i = n("8861"), r = i["a"], o = (n("d8ba"), n("2877")),
            c = Object(o["a"])(r, a, s, !1, null, "7fbf0169", null);
        e["a"] = c.exports
    }, "6e50": function (t, e, n) {
    }, "724e": function (t, e, n) {
        "use strict";
        var a = n("1077"), s = n.n(a);
        s.a
    }, "853d": function (t, e, n) {
    }, 8861: function (t, e, n) {
        "use strict";
        (function (t) {
            e["a"] = {
                props: {
                    types: {type: Array, default: t.always(["cancel", "confirm"])},
                    confirmText: {type: String},
                    cancelText: {type: String}
                }, computed: {
                    _confirmText: function () {
                        return this.confirmText || this.$t("COMMON.ok")
                    }, _cancelText: function () {
                        return this.cancelText || this.$t("COMMON.cancel")
                    }
                }
            }
        }).call(this, n("b17e"))
    }, "930e": function (t, e, n) {
        "use strict";
        var a = n("853d"), s = n.n(a);
        s.a
    }, "96f1": function (t, e, n) {
        "use strict";
        var a = n("6e50"), s = n.n(a);
        s.a
    }, ac5d: function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {
                staticClass: "payment-method media",
                class: {selected: t.isDefault && t.selectable}
            }, [t.selectable ? n("div", [n("b-radio", {
                attrs: {"native-value": "true"},
                model: {
                    value: t.isDefault, callback: function (e) {
                        t.isDefault = e
                    }, expression: "isDefault"
                }
            }, [n("img", {
                staticClass: "img-card-type",
                attrs: {src: "/img/icons/payment_" + t.brandImage(t.method) + ".png"}
            })])], 1) : n("div", [n("img", {
                staticClass: "img-card-type",
                attrs: {src: "/img/icons/payment_" + t.brandImage(t.method) + ".png"}
            })]), n("div", {staticClass: "media-content"}, [n("b", {staticClass: "card-number"}, [t._v("\n            " + t._s(t.method.brand) + "*" + t._s(t.method.last4) + "\n        ")]), n("div", {staticClass: "expiration"}, [n("small", [t._v("\n                Expires on " + t._s(t.method.exp_month) + " / " + t._s(t.method.exp_year) + "\n            ")])])]), t.deletable ? n("div", {staticClass: "media-right"}, [n("button", {
                on: {
                    click: function (e) {
                        return t.remove()
                    }
                }
            }, [n("img", {
                staticClass: "card-view-setting-icon",
                attrs: {src: "/img/icons/delete_btn.png"}
            })])]) : t._e()])
        }, s = [], i = (n("6762"), n("2fdb"), n("4c6b")), r = {
            name: "PaymentMethod",
            props: {
                default: {type: Boolean, default: !1},
                method: {type: Object},
                selectable: {type: Boolean, default: !1},
                deletable: {type: Boolean, default: !0}
            },
            methods: {
                changeDefault: function () {
                    console.log("here"), this.$emit("set-default", this.method.id)
                }, remove: function () {
                    this.$emit("remove", this.method.id)
                }, brandImage: function (t) {
                    var e = t.brand.toLowerCase();
                    return i["m"].includes(e) ? e : "nocard"
                }
            },
            computed: {
                isDefault: {
                    get: function () {
                        return this.default
                    }, set: function (t) {
                        t && this.$emit("set-default", this.method.id)
                    }
                }
            }
        }, o = r, c = (n("930e"), n("2877")), u = Object(c["a"])(o, a, s, !1, null, null, null);
        e["a"] = u.exports
    }, c10c: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "add-payment-method"}, [n("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.loading,
                        expression: "loading"
                    }], staticClass: "has-text-centered"
                }, [n("spinner-adriel", {attrs: {text: t.loadingText}})], 1), n("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: !t.loading,
                        expression: "!loading"
                    }]
                }, [t.isLangType("ko") ? n("div", {staticClass: "payment-info-ko word-break"}, [t._v("\n            鍮꾩옄, 留덉뒪�곗뭅��, �꾨찓由ъ뭏�듭뒪�꾨젅�� �� �댁쇅 寃곗젣媛� 媛��ν븳 移대뱶留� �ъ슜�섏떎 �� �덉뒿�덈떎.\n        ")]) : t._e(), "nocard" !== t.cardType ? n("div", {staticClass: "has-text-centered card-type"}, [n("img", {attrs: {src: "/img/icons/payment_" + t.cardType + ".png"}})]) : t._e(), n("div", {attrs: {id: "card-form"}}, [n("div", {staticClass: "form-input"}, [n("label", [t._v(t._s(t.$t("ACCOUNT_SETTING.payment_card_number")))]), n("div", {
                    staticClass: "input-wrapper",
                    attrs: {id: "card-number"}
                })]), n("div", {staticClass: "form-input"}, [n("label", [t._v(t._s(t.$t("ACCOUNT_SETTING.payment_cvc")))]), n("div", {
                    staticClass: "input-wrapper",
                    attrs: {id: "card-cvc"}
                })]), n("div", {staticClass: "form-input"}, [n("label", [t._v(t._s(t.$t("ACCOUNT_SETTING.payment_expire")))]), n("div", {
                    staticClass: "input-wrapper",
                    attrs: {id: "card-expiry"}
                })])]), n("p", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.errorMessage,
                        expression: "errorMessage"
                    }], staticClass: "has-text-centered has-text-color34"
                }, [t._v("\n            " + t._s(t.errorMessage) + "\n        ")])]), t.hasButtons ? n("div", {staticClass: "buttons-wrapper"}, [n("button", {
                    attrs: {disabled: t.loading},
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.cancel
                    }
                }, [t._v("\n            " + t._s(t.$t("COMMON.cancel")) + "\n        ")]), n("button", {
                    attrs: {disabled: t.loading},
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.confirm
                    }
                }, [t._v("\n            " + t._s(t.$t("COMMON.ok")) + "\n        ")])]) : t._e()])
            }, s = [], i = (n("6762"), n("2fdb"), n("96cf"), n("3b8d")), r = n("1bc2"), o = n("ea3b"), c = n("4c6b"),
            u = "pk_live_OKISgmc18NAmcQfyGPUncS5z", l = {};
        "undefined" !== typeof Stripe && (l = Stripe(u));
        var d = {
            name: "AddPaymentMethod",
            components: {SpinnerAdriel: o["a"]},
            props: {hasButtons: {type: Boolean, default: !0}},
            data: function () {
                return {card: {}, cardType: "nocard", loading: !0, loadingText: "loading...", errorMessage: ""}
            },
            methods: {
                create: function () {
                    var t = Object(i["a"])(regeneratorRuntime.mark(function t() {
                        var e, n = this;
                        return regeneratorRuntime.wrap(function (t) {
                            while (1) switch (t.prev = t.next) {
                                case 0:
                                    this.elements = l.elements({fonts: [{cssSrc: "https://fonts.googleapis.com/css?family=Roboto"}]}), e = {
                                        base: {
                                            fontSize: "16px",
                                            fontFamily: "Roboto, Noto Sans, sans-serif",
                                            color: "#393c3f",
                                            "::placeholder": {color: "#b0b1b2"},
                                            ":focus": {color: "#393c3f"}
                                        }, complete: {color: "#b1ebab"}, invalid: {color: "#fbd8db"}
                                    }, this.card = {
                                        number: this.elements.create("cardNumber", {style: e}),
                                        expiry: this.elements.create("cardExpiry", {style: e}),
                                        cvc: this.elements.create("cardCvc", {style: e})
                                    }, this.card.number.mount("#card-number"), this.card.cvc.mount("#card-cvc"), this.card.expiry.mount("#card-expiry"), this.card.number.on("change", function (t) {
                                        t.brand && (c["m"].includes(t.brand) ? n.cardType = t.brand : n.cardType = "nocard")
                                    }), this.card.number.on("ready", function () {
                                        return n.loading = !1
                                    });
                                case 8:
                                case"end":
                                    return t.stop()
                            }
                        }, t, this)
                    }));

                    function e() {
                        return t.apply(this, arguments)
                    }

                    return e
                }(), confirm: function () {
                    var t = Object(i["a"])(regeneratorRuntime.mark(function t() {
                        var e, n, a, s;
                        return regeneratorRuntime.wrap(function (t) {
                            while (1) switch (t.prev = t.next) {
                                case 0:
                                    if (this.$ga.event({
                                            eventCategory: "App",
                                            eventAction: "Payment",
                                            eventLabel: "AddPaymentMethod",
                                            eventValue: 1
                                        }), fbq("track", "AddPaymentInfo"), this.errorMessage = "", !this.loading) {
                                        t.next = 5;
                                        break
                                    }
                                    return t.abrupt("return");
                                case 5:
                                    return this.loadingText = "processing...", this.loading = !0, t.next = 9, l.createToken(this.card.number);
                                case 9:
                                    if (e = t.sent, n = e.token, a = e.error, !a) {
                                        t.next = 19;
                                        break
                                    }
                                    throw console.log(a), this.errorMessage = this.$t("ACCOUNT_SETTING.invalid_card_number"), this.loading = !1, a;
                                case 19:
                                    return t.prev = 19, t.next = 22, r["a"].addPaymentMethod(n);
                                case 22:
                                    this.$emit("refresh"), this.hasButtons && this.$parent.close(), this.loading = !1, t.next = 34;
                                    break;
                                case 27:
                                    if (t.prev = 27, t.t0 = t["catch"](19), s = t.t0.response, s && s.data && s.data.message && (this.errorMessage = s.data.message), this.loading = !1, this.hasButtons) {
                                        t.next = 34;
                                        break
                                    }
                                    throw t.t0;
                                case 34:
                                case"end":
                                    return t.stop()
                            }
                        }, t, this, [[19, 27]])
                    }));

                    function e() {
                        return t.apply(this, arguments)
                    }

                    return e
                }(), cancel: function () {
                    this.$parent.close()
                }
            },
            mounted: function () {
                this.create()
            }
        }, p = d, m = (n("cd8d"), n("2877")), f = Object(m["a"])(p, a, s, !1, null, null, null);
        e["a"] = f.exports
    }, cd8d: function (t, e, n) {
        "use strict";
        var a = n("f0cd"), s = n.n(a);
        s.a
    }, d8ba: function (t, e, n) {
        "use strict";
        var a = n("e267"), s = n.n(a);
        s.a
    }, e267: function (t, e, n) {
    }, ea3b: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "loading-adriel-wrapper"}, [n("div", {staticClass: "loading-adriel-content"}, [n("div", {staticClass: "animated-logo"}), t.text ? n("span", {staticClass: "loading-msg"}, [t._v("\n            " + t._s(t.text) + "\n        ")]) : t._e()])])
            }, s = [], i = {props: {text: {type: String, default: ""}}, name: "SpinnerAdriel"}, r = i,
            o = (n("724e"), n("2877")), c = Object(o["a"])(r, a, s, !1, null, "01abfa69", null);
        e["a"] = c.exports
    }, f0cd: function (t, e, n) {
    }, f43e: function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {
                staticClass: "coupon",
                class: {selectable: t.selectable}
            }, [t._t("head"), n("img", {
                staticClass: "coupon-img",
                attrs: {src: "/img/coupon.png"}
            }), n("div", {staticClass: "coupon-top"}, [n("b", [t._v("\n            " + t._s(t.info.code) + " - " + t._s(t.info.title) + "\n            "), t.amountLeft != t.info.amount ? n("span", [n("br"), t._v(t._s(t.formatCurrency(t.info.amount)) + "\n            ")]) : t._e(), !t.isConsumed && t.info.end_date && t.info.expired ? n("b", {staticClass: "expired"}, [n("br"), t._v(" " + t._s(t.$t("COUPON.expired")) + "\n            ")]) : t._e(), t.isConsumed || t.info.expired || !t.info.end_date ? t._e() : n("b", {staticClass: "end-date"}, [n("br"), t._v(" " + t._s(t.$t("COUPON.expires_on", {date: t.formattedEndDate})) + "\n            ")])])]), n("div", {staticClass: "coupon-bottom"}), t.amountLeft != t.info.amount && t.amountLeft > 0 ? n("span", {staticClass: "coupon-amount"}, [t._v("\n        " + t._s(t.$t("COUPON.remaining", {amount: t.formatCurrency(t.amountLeft)})) + "\n    ")]) : t._e(), t.amountLeft == t.info.amount ? n("span", {staticClass: "coupon-amount"}, [t._v("\n        " + t._s(t.formatCurrency(t.amountLeft)) + "\n    ")]) : t._e(), t.isConsumed ? n("span", {staticClass: "coupon-amount"}, [t._v("\n        " + t._s(t.$t("COUPON.consumed")) + "\n    ")]) : t._e()], 2)
        }, s = [], i = n("59ad"), r = n.n(i), o = n("c1df"), c = n.n(o), u = {
            name: "Coupon",
            props: {info: {type: Object, required: !0}, selectable: {type: Boolean, default: !0}},
            data: function () {
                return {}
            },
            computed: {
                formattedEndDate: function () {
                    return this.info.end_date ? c()(this.info.end_date).format("YYYY/MM/DD") : null
                }, isConsumed: function () {
                    return this.amountLeft <= 0
                }, amountLeft: function () {
                    return r()(this.info.amount) - r()(this.info.amount_consumed)
                }
            }
        }, l = u, d = (n("96f1"), n("2877")), p = Object(d["a"])(l, a, s, !1, null, "079aeb4b", null);
        e["a"] = p.exports
    }
}]);
//# sourceMappingURL=accountSetting~onboarding~proposal.ceccae85.js.map