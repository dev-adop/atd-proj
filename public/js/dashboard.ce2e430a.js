(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard"], {
    "046b": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement;
                t._self._c;
                return t._m(0)
            }, i = [function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "campaign-no-result"}, [a("img", {attrs: {src: "/img/campaign/campaign_no_result.png"}}), a("span", [t._v("\n        There is not enough data to show your campaign results. "), a("br"), t._v("\n        Please check again in a few hours.\n    ")])])
            }], r = {components: {}}, o = r, s = (a("dec3"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "25cc81c8", null);
        e["a"] = l.exports
    }, "0523": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-keywords-wrapper"}, [a("card-view", {
                    attrs: {
                        label: t.$t("DASHBOARD.card_title_keywords"),
                        className: t.className,
                        editable: t.editable
                    }
                }, [t.available ? a("impression-click-box", {
                    attrs: {
                        getKey: t.getKey,
                        values: t._values
                    }
                }) : t._e(), t.available ? t._e() : a("not-available", {
                    staticClass: "not-available-keywords",
                    attrs: {imgUrl: "/img/dashboard/time_no_data.png"}
                }, [a("span", {
                    staticClass: "not-available-bottom-keywords",
                    attrs: {slot: "not-available-bottom"},
                    slot: "not-available-bottom"
                }, [t._v("\n                " + t._s(t.$t("DASHBOARD.run_search_ad")) + "\n            ")])])], 1)], 1)
            }, i = [], r = a("7abf"), o = r["a"], s = (a("60ab"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, "071c": function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("795b"), i = a.n(n), r = a("cebc"), o = (a("c5f6"), a("8bd7")), s = a("2f62");
            e["a"] = {
                created: function () {
                },
                props: {
                    type: {type: String, default: "formCreate"},
                    active: {type: Boolean, default: !1},
                    campaignId: {type: Number | String, required: !0}
                },
                data: function () {
                    return {currentPage: 1, scroll: !0}
                },
                components: {Form: o["default"]},
                computed: Object(r["a"])({}, Object(s["mapState"])("form", ["formEdit"]), {
                    _active: {
                        get: t.prop("active"),
                        set: function (t) {
                            this.$emit("update:active", t)
                        }
                    }
                }),
                methods: {
                    close: function () {
                        this._active = !1, this.$emit("close")
                    }, handleOk: function () {
                        console.log("handleOk")
                    }, resetModal: function () {
                        console.log("resetModal")
                    }, clickSave: function () {
                        var t = this;
                        return new i.a(function (e, a) {
                            t.$store.dispatch("form/restApi", ["update", t.campaignId, e, a])
                        }).then(function (e) {
                            t.form = e, console.log("sucess", e + ": �낅뜲�댄듃 �깃났"), t.close()
                        }).catch(function (t) {
                            console.log("err", t + " �낅뜲�댄듃 �ㅽ뙣 ")
                        })
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "0ad0": function (t, e, a) {
        "use strict";
        var n = a("568f"), i = a.n(n);
        i.a
    }, "0ee7": function (t, e, a) {
    }, "0fd8": function (t, e, a) {
    }, "0fd8d": function (t, e, a) {
        "use strict";
        var n = a("77a3"), i = a.n(n);
        i.a
    }, 1187: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("5176"), i = a.n(n), r = a("cebc"), o = a("2f62"), s = a("5633"), l = a("0523"), c = a("18e2"),
                d = a("26ce"), u = a("e3ff"), h = a("1517");

            function f(t) {
                return t.charAt(0).toUpperCase() + t.slice(1)
            }

            e["a"] = {
                name: "CardCompWrapper",
                props: {
                    type: {type: String, required: !0},
                    isEditing: {type: Boolean, default: !1},
                    dimmed: {type: Boolean, default: !1}
                },
                computed: Object(r["a"])({}, Object(o["mapGetters"])("dashboard", ["allAge", "allGender", "allLocation", "allKeywords", "allNewAndReturning", "allTimeSpentOnSite", "allConversion", "conversionList", "selectedConversion"])),
                components: {},
                methods: {
                    getComp: function () {
                        switch (this.type) {
                            case"allAge":
                                return u["a"];
                            case"allGender":
                                return h["a"];
                            case"allLocation":
                                return d["a"];
                            case"allKeywords":
                                return l["a"];
                            case"allTimeSpentOnSite":
                                return s["a"];
                            case"allNewAndReturning":
                                return c["a"]
                        }
                    }, getProps: function () {
                        var e = {};
                        return "allConversion" === this.type && (e.conversionList = this.conversionList, e.selectedConversion = this.selectedConversion), t.compose(t.merge(i()(e, this.$props)), function (t) {
                            return {values: t || {}}
                        }, t.prop(this.type))(this)
                    }, update: function (t) {
                        var e = this["update".concat(f(this.type))];
                        e && e(t)
                    }, remove: function (t) {
                        var e = this["remove".concat(f(this.type))];
                        e && e(t)
                    }, reset: function () {
                        var t = this["reset".concat(f(this.type))];
                        t && t()
                    }, add: function (t) {
                        switch (this.type) {
                            case"listOfCustomers":
                                return this.addListOfCustomers(t)
                        }
                    }
                },
                render: function (t) {
                    var e = this;
                    return t(this.getComp(), {
                        props: this.getProps(), on: {
                            "edit:start": function () {
                                return e.$emit("edit:start")
                            },
                            "edit:end": function () {
                                return e.$emit("edit:end")
                            },
                            "edit:reset": this.reset,
                            "edit:add": this.add,
                            "update:value": this.update,
                            "update:remove": this.remove,
                            "show:modal": function (t) {
                                return e.$emit("show:modal", t)
                            }
                        }
                    })
                }
            }
        }).call(this, a("b17e"))
    }, 1517: function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-gender-wrapper"}, [a("card-view", {
                    attrs: {
                        label: t.$t("DASHBOARD.card_title_gender"),
                        className: t.className,
                        editable: t.editable
                    }
                }, [a("impression-click-box", {
                    attrs: {
                        getImgUrl: t.getImgUrl,
                        getKey: t.getKey,
                        values: t._values,
                        getParentClass: function (t) {
                            return "element-" + t.key
                        }
                    }
                })], 1)], 1)
            }, i = [], r = a("5583"), o = r["a"], s = (a("4e70"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, 1584: function (t, e, a) {
    }, "18e2": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-new-return-wrapper"}, [a("card-view", {
                    attrs: {
                        label: t.$t("DASHBOARD.card_title_new_returning"),
                        className: t.className,
                        editable: t.editable
                    }
                }, [this._values ? t._e() : a("not-available", {
                    attrs: {
                        imgUrl: "/img/dashboard/time_no_data.png",
                        text: t.$t("DASHBOARD.not_available")
                    }
                }), this._values ? a("impression-click-box", {
                    attrs: {
                        getImgUrl: t.getImgUrl,
                        getKey: t.getKey,
                        values: t._values,
                        "get-parent-class": function (t) {
                            return "box-element-" + t.key
                        },
                        "get-tooltip": t.getTooltip
                    }
                }) : t._e()], 1)], 1)
            }, i = [], r = a("5176"), o = a.n(r), s = a("a4bb"), l = a.n(s), c = (a("ac6a"), a("43c9")), d = a("c15c"),
            u = {
                mixins: [d["a"]], created: function () {
                }, data: function () {
                    return {}
                }, props: {}, computed: {
                    _values: function () {
                        var t = this;
                        if (this.values) return l()(this.values).reduce(function (e, a) {
                            return e.push(o()({key: a}, t.values[a])), e
                        }, [])
                    }
                }, methods: {
                    getImgUrl: function (t) {
                        var e = t.key;
                        return "/img/dashboard/customer_".concat(e, ".png")
                    }, getKey: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        return this.$t("DASHBOARD.new_returning_".concat(t.key))
                    }, getTooltip: function (t) {
                        var e = t.key;
                        return "new" === e ? this.$t("DASHBOARD.customer_new_tooltip") : this.$t("DASHBOARD.customer_return_tooltip")
                    }
                }, components: {ImpressionClickBox: c["a"]}
            }, h = u, f = (a("d5f9"), a("2877")), p = Object(f["a"])(h, n, i, !1, null, null, null);
        e["a"] = p.exports
    }, "1b61": function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("cebc"), i = a("2f62");
            e["a"] = {
                props: {values: {type: [Array, Object], required: !0}},
                computed: Object(n["a"])({}, Object(i["mapGetters"])("dashboard", ["totalSpent"]), {
                    ratio: function () {
                        return t.pathOr(0, ["values", "spend"], this) / t.defaultTo(0, this.totalSpent || 0) * 100
                    }, spent: function () {
                        return t.pathOr("-", ["values", "spend"], this)
                    }, cpc: function () {
                        return t.pathOr("-", ["values", "cpc"], this)
                    }
                })
            }
        }).call(this, a("b17e"))
    }, "1d83": function (t, e, a) {
        "use strict";
        var n = a("1e35"), i = a.n(n);
        i.a
    }, "1e35": function (t, e, a) {
    }, "1fca": function (t, e, a) {
        "use strict";

        function n(t, e) {
            if (e) {
                var a = this.$data._chart, n = t.datasets.map(function (t) {
                    return t.label
                }), i = e.datasets.map(function (t) {
                    return t.label
                }), r = JSON.stringify(i), o = JSON.stringify(n);
                o === r && e.datasets.length === t.datasets.length ? (t.datasets.forEach(function (t, n) {
                    var i = Object.keys(e.datasets[n]), r = Object.keys(t), o = i.filter(function (t) {
                        return "_meta" !== t && -1 === r.indexOf(t)
                    });
                    for (var s in o.forEach(function (t) {
                        delete a.data.datasets[n][t]
                    }), t) t.hasOwnProperty(s) && (a.data.datasets[n][s] = t[s])
                }), t.hasOwnProperty("labels") && (a.data.labels = t.labels, this.$emit("labels:update")), t.hasOwnProperty("xLabels") && (a.data.xLabels = t.xLabels, this.$emit("xlabels:update")), t.hasOwnProperty("yLabels") && (a.data.yLabels = t.yLabels, this.$emit("ylabels:update")), a.update(), this.$emit("chart:update")) : (a && (a.destroy(), this.$emit("chart:destroy")), this.renderChart(this.chartData, this.options), this.$emit("chart:render"))
            } else this.$data._chart && (this.$data._chart.destroy(), this.$emit("chart:destroy")), this.renderChart(this.chartData, this.options), this.$emit("chart:render")
        }

        var i = a("30ef"), r = a.n(i);

        function o(t, e) {
            return {
                render: function (t) {
                    return t("div", {
                        style: this.styles,
                        class: this.cssClasses
                    }, [t("canvas", {
                        attrs: {id: this.chartId, width: this.width, height: this.height},
                        ref: "canvas"
                    })])
                },
                props: {
                    chartId: {default: t, type: String},
                    width: {default: 400, type: Number},
                    height: {default: 400, type: Number},
                    cssClasses: {type: String, default: ""},
                    styles: {type: Object},
                    plugins: {
                        type: Array, default: function () {
                            return []
                        }
                    }
                },
                data: function () {
                    return {_chart: null, _plugins: this.plugins}
                },
                methods: {
                    addPlugin: function (t) {
                        this.$data._plugins.push(t)
                    }, generateLegend: function () {
                        if (this.$data._chart) return this.$data._chart.generateLegend()
                    }, renderChart: function (t, a) {
                        this.$data._chart && this.$data._chart.destroy(), this.$data._chart = new r.a(this.$refs.canvas.getContext("2d"), {
                            type: e,
                            data: t,
                            options: a,
                            plugins: this.$data._plugins
                        })
                    }
                },
                beforeDestroy: function () {
                    this.$data._chart && this.$data._chart.destroy()
                }
            }
        }

        var s = o("bar-chart", "bar");
        o("horizontalbar-chart", "horizontalBar"), o("doughnut-chart", "doughnut"), o("line-chart", "line"), o("pie-chart", "pie"), o("polar-chart", "polarArea"), o("radar-chart", "radar"), o("bubble-chart", "bubble"), o("scatter-chart", "scatter");
        a.d(e, "a", function () {
            return s
        })
    }, "206b": function (t, e, a) {
        "use strict";
        var n = a("0ee7"), i = a.n(n);
        i.a
    }, "26ce": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-location-wrapper"}, [a("card-view", {
                    attrs: {
                        label: t.$t("DASHBOARD.card_title_location"),
                        className: t.className,
                        editable: t.editable
                    }
                }, [t._values ? a("impression-click-box", {
                    attrs: {
                        getImgUrl: t.getImgUrl,
                        getKey: t.getKey,
                        values: t._values
                    }
                }) : t._e(), t._values ? t._e() : a("not-available", {
                    attrs: {
                        text: t.$t("DASHBOARD.not_available"),
                        imgUrl: "/img/dashboard/time_no_data.png"
                    }
                })], 1)], 1)
            }, i = [], r = a("e1e5"), o = r["a"], s = (a("288a"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, "288a": function (t, e, a) {
        "use strict";
        var n = a("cab8"), i = a.n(n);
        i.a
    }, "2a65": function (t, e, a) {
        "use strict";
        (function (t) {
            a("c5f6");
            var n = a("2d1f"), i = a.n(n), r = a("768b"), o = a("cebc"), s = a("fa7d"), l = a("2f62");
            e["a"] = {
                mounted: function () {
                },
                data: function () {
                    return {}
                },
                props: {
                    summary: {type: Object, default: t.always({})},
                    showLastDay: {type: Boolean, default: !1},
                    lastDate: {type: String}
                },
                computed: Object(o["a"])({}, Object(l["mapGetters"])("dashboard", ["serviceFee"]), {
                    type: t.path(["summary", "type"]),
                    target: t.compose(t.defaultTo("allTime"), t.path(["summary", "target"])),
                    today: t.compose(t.defaultTo({}), t.path(["summary", "today"])),
                    allTime: t.compose(t.defaultTo({}), t.path(["summary", "allTime"])),
                    todayCost: function () {
                        return t.path(["today", this.type, "all"], this)
                    },
                    totalCost: function () {
                        return t.path([this.target, this.type, "all"], this)
                    },
                    totalBudget: t.path(["allTime", "budget", "all"]),
                    todayBudget: t.path(["today", "budget", "all"]),
                    channels: function () {
                        return t.compose(t.sortWith([t.descend(t.prop("value"))]), t.map(function (t) {
                            var e = Object(r["a"])(t, 2), a = e[0], n = e[1];
                            return {channel: a, value: n}
                        }), t.reject(t.propEq("0", "all")), i.a, t.reduce(function (e, a) {
                            var n = Object(r["a"])(a, 2), i = n[0], o = n[1];
                            return t.is(Number, o) ? t.set(t.lensProp(i), o, e) : e
                        }, {}), i.a, t.defaultTo({}), t.prop(this.type), t.prop(this.target))(this)
                    },
                    leftText: function () {
                        return "spend" === this.type ? this.$t("DASHBOARD.cost_platform") : this.$t("DASHBOARD.history_sub_title_budget")
                    },
                    rightText: function () {
                        return "spend" === this.type ? this.$t("DASHBOARD.cost_platform_include_fee") : this.$t("DASHBOARD.budget_platform_include_fee")
                    }
                }),
                methods: {
                    getLogo: function (t) {
                        var e = {facebook: "fb", instagram: "insta", google: "google", apple: "apple"};
                        return "/img/dashboard/history_".concat(e[t], "_logo.png")
                    }, _formatCurrency: function (e) {
                        return t.compose(this.formatCurrency, this._formatNumber)(e)
                    }, _formatNumber: function (e) {
                        return t.compose(t.curry(s["n"])(t.__, void 0, "N/A"), this._toFixed)(e)
                    }, _toFixed: t.compose(Number, function (t) {
                        return t.toFixed(2)
                    }, Number), includeFee: function (t) {
                        return t + t * this.serviceFee
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "2c51": function (t, e, a) {
    }, "2c6e": function (t, e, a) {
    }, "2f21": function (t, e, a) {
        "use strict";
        var n = a("79e5");
        t.exports = function (t, e) {
            return !!t && n(function () {
                e ? t.call(null, function () {
                }, 1) : t.call(null)
            })
        }
    }, "2fea": function (t, e, a) {
        "use strict";
        var n = a("8367"), i = a.n(n);
        i.a
    }, "30ef": function (t, e, a) {
        /*!
         * Chart.js v2.8.0
         * https://www.chartjs.org
         * (c) 2019 Chart.js Contributors
         * Released under the MIT License
         */
        (function (e, n) {
            t.exports = n(function () {
                try {
                    return a("c1df")
                } catch (t) {
                }
            }())
        })(0, function (t) {
            "use strict";
            t = t && t.hasOwnProperty("default") ? t["default"] : t;
            var e = {
                rgb2hsl: a,
                rgb2hsv: n,
                rgb2hwb: i,
                rgb2cmyk: o,
                rgb2keyword: s,
                rgb2xyz: l,
                rgb2lab: c,
                rgb2lch: d,
                hsl2rgb: u,
                hsl2hsv: h,
                hsl2hwb: f,
                hsl2cmyk: p,
                hsl2keyword: m,
                hsv2rgb: v,
                hsv2hsl: y,
                hsv2hwb: x,
                hsv2cmyk: _,
                hsv2keyword: C,
                hwb2rgb: k,
                hwb2hsl: w,
                hwb2hsv: A,
                hwb2cmyk: M,
                hwb2keyword: S,
                cmyk2rgb: D,
                cmyk2hsl: O,
                cmyk2hsv: P,
                cmyk2hwb: T,
                cmyk2keyword: I,
                keyword2rgb: z,
                keyword2hsl: V,
                keyword2hsv: W,
                keyword2hwb: U,
                keyword2cmyk: q,
                keyword2lab: Y,
                keyword2xyz: G,
                xyz2rgb: R,
                xyz2lab: F,
                xyz2lch: B,
                lab2xyz: L,
                lab2rgb: E,
                lab2lch: N,
                lch2lab: j,
                lch2xyz: H,
                lch2rgb: $
            };

            function a(t) {
                var e, a, n, i = t[0] / 255, r = t[1] / 255, o = t[2] / 255, s = Math.min(i, r, o),
                    l = Math.max(i, r, o), c = l - s;
                return l == s ? e = 0 : i == l ? e = (r - o) / c : r == l ? e = 2 + (o - i) / c : o == l && (e = 4 + (i - r) / c), e = Math.min(60 * e, 360), e < 0 && (e += 360), n = (s + l) / 2, a = l == s ? 0 : n <= .5 ? c / (l + s) : c / (2 - l - s), [e, 100 * a, 100 * n]
            }

            function n(t) {
                var e, a, n, i = t[0], r = t[1], o = t[2], s = Math.min(i, r, o), l = Math.max(i, r, o), c = l - s;
                return a = 0 == l ? 0 : c / l * 1e3 / 10, l == s ? e = 0 : i == l ? e = (r - o) / c : r == l ? e = 2 + (o - i) / c : o == l && (e = 4 + (i - r) / c), e = Math.min(60 * e, 360), e < 0 && (e += 360), n = l / 255 * 1e3 / 10, [e, a, n]
            }

            function i(t) {
                var e = t[0], n = t[1], i = t[2], r = a(t)[0], o = 1 / 255 * Math.min(e, Math.min(n, i));
                i = 1 - 1 / 255 * Math.max(e, Math.max(n, i));
                return [r, 100 * o, 100 * i]
            }

            function o(t) {
                var e, a, n, i, r = t[0] / 255, o = t[1] / 255, s = t[2] / 255;
                return i = Math.min(1 - r, 1 - o, 1 - s), e = (1 - r - i) / (1 - i) || 0, a = (1 - o - i) / (1 - i) || 0, n = (1 - s - i) / (1 - i) || 0, [100 * e, 100 * a, 100 * n, 100 * i]
            }

            function s(t) {
                return X[JSON.stringify(t)]
            }

            function l(t) {
                var e = t[0] / 255, a = t[1] / 255, n = t[2] / 255;
                e = e > .04045 ? Math.pow((e + .055) / 1.055, 2.4) : e / 12.92, a = a > .04045 ? Math.pow((a + .055) / 1.055, 2.4) : a / 12.92, n = n > .04045 ? Math.pow((n + .055) / 1.055, 2.4) : n / 12.92;
                var i = .4124 * e + .3576 * a + .1805 * n, r = .2126 * e + .7152 * a + .0722 * n,
                    o = .0193 * e + .1192 * a + .9505 * n;
                return [100 * i, 100 * r, 100 * o]
            }

            function c(t) {
                var e, a, n, i = l(t), r = i[0], o = i[1], s = i[2];
                return r /= 95.047, o /= 100, s /= 108.883, r = r > .008856 ? Math.pow(r, 1 / 3) : 7.787 * r + 16 / 116, o = o > .008856 ? Math.pow(o, 1 / 3) : 7.787 * o + 16 / 116, s = s > .008856 ? Math.pow(s, 1 / 3) : 7.787 * s + 16 / 116, e = 116 * o - 16, a = 500 * (r - o), n = 200 * (o - s), [e, a, n]
            }

            function d(t) {
                return N(c(t))
            }

            function u(t) {
                var e, a, n, i, r, o = t[0] / 360, s = t[1] / 100, l = t[2] / 100;
                if (0 == s) return r = 255 * l, [r, r, r];
                a = l < .5 ? l * (1 + s) : l + s - l * s, e = 2 * l - a, i = [0, 0, 0];
                for (var c = 0; c < 3; c++) n = o + 1 / 3 * -(c - 1), n < 0 && n++, n > 1 && n--, r = 6 * n < 1 ? e + 6 * (a - e) * n : 2 * n < 1 ? a : 3 * n < 2 ? e + (a - e) * (2 / 3 - n) * 6 : e, i[c] = 255 * r;
                return i
            }

            function h(t) {
                var e, a, n = t[0], i = t[1] / 100, r = t[2] / 100;
                return 0 === r ? [0, 0, 0] : (r *= 2, i *= r <= 1 ? r : 2 - r, a = (r + i) / 2, e = 2 * i / (r + i), [n, 100 * e, 100 * a])
            }

            function f(t) {
                return i(u(t))
            }

            function p(t) {
                return o(u(t))
            }

            function m(t) {
                return s(u(t))
            }

            function v(t) {
                var e = t[0] / 60, a = t[1] / 100, n = t[2] / 100, i = Math.floor(e) % 6, r = e - Math.floor(e),
                    o = 255 * n * (1 - a), s = 255 * n * (1 - a * r), l = 255 * n * (1 - a * (1 - r));
                n *= 255;
                switch (i) {
                    case 0:
                        return [n, l, o];
                    case 1:
                        return [s, n, o];
                    case 2:
                        return [o, n, l];
                    case 3:
                        return [o, s, n];
                    case 4:
                        return [l, o, n];
                    case 5:
                        return [n, o, s]
                }
            }

            function y(t) {
                var e, a, n = t[0], i = t[1] / 100, r = t[2] / 100;
                return a = (2 - i) * r, e = i * r, e /= a <= 1 ? a : 2 - a, e = e || 0, a /= 2, [n, 100 * e, 100 * a]
            }

            function x(t) {
                return i(v(t))
            }

            function _(t) {
                return o(v(t))
            }

            function C(t) {
                return s(v(t))
            }

            function k(t) {
                var e, a, n, i, o = t[0] / 360, s = t[1] / 100, l = t[2] / 100, c = s + l;
                switch (c > 1 && (s /= c, l /= c), e = Math.floor(6 * o), a = 1 - l, n = 6 * o - e, 0 != (1 & e) && (n = 1 - n), i = s + n * (a - s), e) {
                    default:
                    case 6:
                    case 0:
                        r = a, g = i, b = s;
                        break;
                    case 1:
                        r = i, g = a, b = s;
                        break;
                    case 2:
                        r = s, g = a, b = i;
                        break;
                    case 3:
                        r = s, g = i, b = a;
                        break;
                    case 4:
                        r = i, g = s, b = a;
                        break;
                    case 5:
                        r = a, g = s, b = i;
                        break
                }
                return [255 * r, 255 * g, 255 * b]
            }

            function w(t) {
                return a(k(t))
            }

            function A(t) {
                return n(k(t))
            }

            function M(t) {
                return o(k(t))
            }

            function S(t) {
                return s(k(t))
            }

            function D(t) {
                var e, a, n, i = t[0] / 100, r = t[1] / 100, o = t[2] / 100, s = t[3] / 100;
                return e = 1 - Math.min(1, i * (1 - s) + s), a = 1 - Math.min(1, r * (1 - s) + s), n = 1 - Math.min(1, o * (1 - s) + s), [255 * e, 255 * a, 255 * n]
            }

            function O(t) {
                return a(D(t))
            }

            function P(t) {
                return n(D(t))
            }

            function T(t) {
                return i(D(t))
            }

            function I(t) {
                return s(D(t))
            }

            function R(t) {
                var e, a, n, i = t[0] / 100, r = t[1] / 100, o = t[2] / 100;
                return e = 3.2406 * i + -1.5372 * r + -.4986 * o, a = -.9689 * i + 1.8758 * r + .0415 * o, n = .0557 * i + -.204 * r + 1.057 * o, e = e > .0031308 ? 1.055 * Math.pow(e, 1 / 2.4) - .055 : e *= 12.92, a = a > .0031308 ? 1.055 * Math.pow(a, 1 / 2.4) - .055 : a *= 12.92, n = n > .0031308 ? 1.055 * Math.pow(n, 1 / 2.4) - .055 : n *= 12.92, e = Math.min(Math.max(0, e), 1), a = Math.min(Math.max(0, a), 1), n = Math.min(Math.max(0, n), 1), [255 * e, 255 * a, 255 * n]
            }

            function F(t) {
                var e, a, n, i = t[0], r = t[1], o = t[2];
                return i /= 95.047, r /= 100, o /= 108.883, i = i > .008856 ? Math.pow(i, 1 / 3) : 7.787 * i + 16 / 116, r = r > .008856 ? Math.pow(r, 1 / 3) : 7.787 * r + 16 / 116, o = o > .008856 ? Math.pow(o, 1 / 3) : 7.787 * o + 16 / 116, e = 116 * r - 16, a = 500 * (i - r), n = 200 * (r - o), [e, a, n]
            }

            function B(t) {
                return N(F(t))
            }

            function L(t) {
                var e, a, n, i, r = t[0], o = t[1], s = t[2];
                return r <= 8 ? (a = 100 * r / 903.3, i = a / 100 * 7.787 + 16 / 116) : (a = 100 * Math.pow((r + 16) / 116, 3), i = Math.pow(a / 100, 1 / 3)), e = e / 95.047 <= .008856 ? e = 95.047 * (o / 500 + i - 16 / 116) / 7.787 : 95.047 * Math.pow(o / 500 + i, 3), n = n / 108.883 <= .008859 ? n = 108.883 * (i - s / 200 - 16 / 116) / 7.787 : 108.883 * Math.pow(i - s / 200, 3), [e, a, n]
            }

            function N(t) {
                var e, a, n, i = t[0], r = t[1], o = t[2];
                return e = Math.atan2(o, r), a = 360 * e / 2 / Math.PI, a < 0 && (a += 360), n = Math.sqrt(r * r + o * o), [i, n, a]
            }

            function E(t) {
                return R(L(t))
            }

            function j(t) {
                var e, a, n, i = t[0], r = t[1], o = t[2];
                return n = o / 360 * 2 * Math.PI, e = r * Math.cos(n), a = r * Math.sin(n), [i, e, a]
            }

            function H(t) {
                return L(j(t))
            }

            function $(t) {
                return E(j(t))
            }

            function z(t) {
                return K[t]
            }

            function V(t) {
                return a(z(t))
            }

            function W(t) {
                return n(z(t))
            }

            function U(t) {
                return i(z(t))
            }

            function q(t) {
                return o(z(t))
            }

            function Y(t) {
                return c(z(t))
            }

            function G(t) {
                return l(z(t))
            }

            var K = {
                aliceblue: [240, 248, 255],
                antiquewhite: [250, 235, 215],
                aqua: [0, 255, 255],
                aquamarine: [127, 255, 212],
                azure: [240, 255, 255],
                beige: [245, 245, 220],
                bisque: [255, 228, 196],
                black: [0, 0, 0],
                blanchedalmond: [255, 235, 205],
                blue: [0, 0, 255],
                blueviolet: [138, 43, 226],
                brown: [165, 42, 42],
                burlywood: [222, 184, 135],
                cadetblue: [95, 158, 160],
                chartreuse: [127, 255, 0],
                chocolate: [210, 105, 30],
                coral: [255, 127, 80],
                cornflowerblue: [100, 149, 237],
                cornsilk: [255, 248, 220],
                crimson: [220, 20, 60],
                cyan: [0, 255, 255],
                darkblue: [0, 0, 139],
                darkcyan: [0, 139, 139],
                darkgoldenrod: [184, 134, 11],
                darkgray: [169, 169, 169],
                darkgreen: [0, 100, 0],
                darkgrey: [169, 169, 169],
                darkkhaki: [189, 183, 107],
                darkmagenta: [139, 0, 139],
                darkolivegreen: [85, 107, 47],
                darkorange: [255, 140, 0],
                darkorchid: [153, 50, 204],
                darkred: [139, 0, 0],
                darksalmon: [233, 150, 122],
                darkseagreen: [143, 188, 143],
                darkslateblue: [72, 61, 139],
                darkslategray: [47, 79, 79],
                darkslategrey: [47, 79, 79],
                darkturquoise: [0, 206, 209],
                darkviolet: [148, 0, 211],
                deeppink: [255, 20, 147],
                deepskyblue: [0, 191, 255],
                dimgray: [105, 105, 105],
                dimgrey: [105, 105, 105],
                dodgerblue: [30, 144, 255],
                firebrick: [178, 34, 34],
                floralwhite: [255, 250, 240],
                forestgreen: [34, 139, 34],
                fuchsia: [255, 0, 255],
                gainsboro: [220, 220, 220],
                ghostwhite: [248, 248, 255],
                gold: [255, 215, 0],
                goldenrod: [218, 165, 32],
                gray: [128, 128, 128],
                green: [0, 128, 0],
                greenyellow: [173, 255, 47],
                grey: [128, 128, 128],
                honeydew: [240, 255, 240],
                hotpink: [255, 105, 180],
                indianred: [205, 92, 92],
                indigo: [75, 0, 130],
                ivory: [255, 255, 240],
                khaki: [240, 230, 140],
                lavender: [230, 230, 250],
                lavenderblush: [255, 240, 245],
                lawngreen: [124, 252, 0],
                lemonchiffon: [255, 250, 205],
                lightblue: [173, 216, 230],
                lightcoral: [240, 128, 128],
                lightcyan: [224, 255, 255],
                lightgoldenrodyellow: [250, 250, 210],
                lightgray: [211, 211, 211],
                lightgreen: [144, 238, 144],
                lightgrey: [211, 211, 211],
                lightpink: [255, 182, 193],
                lightsalmon: [255, 160, 122],
                lightseagreen: [32, 178, 170],
                lightskyblue: [135, 206, 250],
                lightslategray: [119, 136, 153],
                lightslategrey: [119, 136, 153],
                lightsteelblue: [176, 196, 222],
                lightyellow: [255, 255, 224],
                lime: [0, 255, 0],
                limegreen: [50, 205, 50],
                linen: [250, 240, 230],
                magenta: [255, 0, 255],
                maroon: [128, 0, 0],
                mediumaquamarine: [102, 205, 170],
                mediumblue: [0, 0, 205],
                mediumorchid: [186, 85, 211],
                mediumpurple: [147, 112, 219],
                mediumseagreen: [60, 179, 113],
                mediumslateblue: [123, 104, 238],
                mediumspringgreen: [0, 250, 154],
                mediumturquoise: [72, 209, 204],
                mediumvioletred: [199, 21, 133],
                midnightblue: [25, 25, 112],
                mintcream: [245, 255, 250],
                mistyrose: [255, 228, 225],
                moccasin: [255, 228, 181],
                navajowhite: [255, 222, 173],
                navy: [0, 0, 128],
                oldlace: [253, 245, 230],
                olive: [128, 128, 0],
                olivedrab: [107, 142, 35],
                orange: [255, 165, 0],
                orangered: [255, 69, 0],
                orchid: [218, 112, 214],
                palegoldenrod: [238, 232, 170],
                palegreen: [152, 251, 152],
                paleturquoise: [175, 238, 238],
                palevioletred: [219, 112, 147],
                papayawhip: [255, 239, 213],
                peachpuff: [255, 218, 185],
                peru: [205, 133, 63],
                pink: [255, 192, 203],
                plum: [221, 160, 221],
                powderblue: [176, 224, 230],
                purple: [128, 0, 128],
                rebeccapurple: [102, 51, 153],
                red: [255, 0, 0],
                rosybrown: [188, 143, 143],
                royalblue: [65, 105, 225],
                saddlebrown: [139, 69, 19],
                salmon: [250, 128, 114],
                sandybrown: [244, 164, 96],
                seagreen: [46, 139, 87],
                seashell: [255, 245, 238],
                sienna: [160, 82, 45],
                silver: [192, 192, 192],
                skyblue: [135, 206, 235],
                slateblue: [106, 90, 205],
                slategray: [112, 128, 144],
                slategrey: [112, 128, 144],
                snow: [255, 250, 250],
                springgreen: [0, 255, 127],
                steelblue: [70, 130, 180],
                tan: [210, 180, 140],
                teal: [0, 128, 128],
                thistle: [216, 191, 216],
                tomato: [255, 99, 71],
                turquoise: [64, 224, 208],
                violet: [238, 130, 238],
                wheat: [245, 222, 179],
                white: [255, 255, 255],
                whitesmoke: [245, 245, 245],
                yellow: [255, 255, 0],
                yellowgreen: [154, 205, 50]
            }, X = {};
            for (var J in K) X[JSON.stringify(K[J])] = J;
            var Z = function () {
                return new nt
            };
            for (var Q in e) {
                Z[Q + "Raw"] = function (t) {
                    return function (a) {
                        return "number" == typeof a && (a = Array.prototype.slice.call(arguments)), e[t](a)
                    }
                }(Q);
                var tt = /(\w+)2(\w+)/.exec(Q), et = tt[1], at = tt[2];
                Z[et] = Z[et] || {}, Z[et][at] = Z[Q] = function (t) {
                    return function (a) {
                        "number" == typeof a && (a = Array.prototype.slice.call(arguments));
                        var n = e[t](a);
                        if ("string" == typeof n || void 0 === n) return n;
                        for (var i = 0; i < n.length; i++) n[i] = Math.round(n[i]);
                        return n
                    }
                }(Q)
            }
            var nt = function () {
                this.convs = {}
            };
            nt.prototype.routeSpace = function (t, e) {
                var a = e[0];
                return void 0 === a ? this.getValues(t) : ("number" == typeof a && (a = Array.prototype.slice.call(e)), this.setValues(t, a))
            }, nt.prototype.setValues = function (t, e) {
                return this.space = t, this.convs = {}, this.convs[t] = e, this
            }, nt.prototype.getValues = function (t) {
                var e = this.convs[t];
                if (!e) {
                    var a = this.space, n = this.convs[a];
                    e = Z[a][t](n), this.convs[t] = e
                }
                return e
            }, ["rgb", "hsl", "hsv", "cmyk", "keyword"].forEach(function (t) {
                nt.prototype[t] = function (e) {
                    return this.routeSpace(t, arguments)
                }
            });
            var it = Z, rt = {
                aliceblue: [240, 248, 255],
                antiquewhite: [250, 235, 215],
                aqua: [0, 255, 255],
                aquamarine: [127, 255, 212],
                azure: [240, 255, 255],
                beige: [245, 245, 220],
                bisque: [255, 228, 196],
                black: [0, 0, 0],
                blanchedalmond: [255, 235, 205],
                blue: [0, 0, 255],
                blueviolet: [138, 43, 226],
                brown: [165, 42, 42],
                burlywood: [222, 184, 135],
                cadetblue: [95, 158, 160],
                chartreuse: [127, 255, 0],
                chocolate: [210, 105, 30],
                coral: [255, 127, 80],
                cornflowerblue: [100, 149, 237],
                cornsilk: [255, 248, 220],
                crimson: [220, 20, 60],
                cyan: [0, 255, 255],
                darkblue: [0, 0, 139],
                darkcyan: [0, 139, 139],
                darkgoldenrod: [184, 134, 11],
                darkgray: [169, 169, 169],
                darkgreen: [0, 100, 0],
                darkgrey: [169, 169, 169],
                darkkhaki: [189, 183, 107],
                darkmagenta: [139, 0, 139],
                darkolivegreen: [85, 107, 47],
                darkorange: [255, 140, 0],
                darkorchid: [153, 50, 204],
                darkred: [139, 0, 0],
                darksalmon: [233, 150, 122],
                darkseagreen: [143, 188, 143],
                darkslateblue: [72, 61, 139],
                darkslategray: [47, 79, 79],
                darkslategrey: [47, 79, 79],
                darkturquoise: [0, 206, 209],
                darkviolet: [148, 0, 211],
                deeppink: [255, 20, 147],
                deepskyblue: [0, 191, 255],
                dimgray: [105, 105, 105],
                dimgrey: [105, 105, 105],
                dodgerblue: [30, 144, 255],
                firebrick: [178, 34, 34],
                floralwhite: [255, 250, 240],
                forestgreen: [34, 139, 34],
                fuchsia: [255, 0, 255],
                gainsboro: [220, 220, 220],
                ghostwhite: [248, 248, 255],
                gold: [255, 215, 0],
                goldenrod: [218, 165, 32],
                gray: [128, 128, 128],
                green: [0, 128, 0],
                greenyellow: [173, 255, 47],
                grey: [128, 128, 128],
                honeydew: [240, 255, 240],
                hotpink: [255, 105, 180],
                indianred: [205, 92, 92],
                indigo: [75, 0, 130],
                ivory: [255, 255, 240],
                khaki: [240, 230, 140],
                lavender: [230, 230, 250],
                lavenderblush: [255, 240, 245],
                lawngreen: [124, 252, 0],
                lemonchiffon: [255, 250, 205],
                lightblue: [173, 216, 230],
                lightcoral: [240, 128, 128],
                lightcyan: [224, 255, 255],
                lightgoldenrodyellow: [250, 250, 210],
                lightgray: [211, 211, 211],
                lightgreen: [144, 238, 144],
                lightgrey: [211, 211, 211],
                lightpink: [255, 182, 193],
                lightsalmon: [255, 160, 122],
                lightseagreen: [32, 178, 170],
                lightskyblue: [135, 206, 250],
                lightslategray: [119, 136, 153],
                lightslategrey: [119, 136, 153],
                lightsteelblue: [176, 196, 222],
                lightyellow: [255, 255, 224],
                lime: [0, 255, 0],
                limegreen: [50, 205, 50],
                linen: [250, 240, 230],
                magenta: [255, 0, 255],
                maroon: [128, 0, 0],
                mediumaquamarine: [102, 205, 170],
                mediumblue: [0, 0, 205],
                mediumorchid: [186, 85, 211],
                mediumpurple: [147, 112, 219],
                mediumseagreen: [60, 179, 113],
                mediumslateblue: [123, 104, 238],
                mediumspringgreen: [0, 250, 154],
                mediumturquoise: [72, 209, 204],
                mediumvioletred: [199, 21, 133],
                midnightblue: [25, 25, 112],
                mintcream: [245, 255, 250],
                mistyrose: [255, 228, 225],
                moccasin: [255, 228, 181],
                navajowhite: [255, 222, 173],
                navy: [0, 0, 128],
                oldlace: [253, 245, 230],
                olive: [128, 128, 0],
                olivedrab: [107, 142, 35],
                orange: [255, 165, 0],
                orangered: [255, 69, 0],
                orchid: [218, 112, 214],
                palegoldenrod: [238, 232, 170],
                palegreen: [152, 251, 152],
                paleturquoise: [175, 238, 238],
                palevioletred: [219, 112, 147],
                papayawhip: [255, 239, 213],
                peachpuff: [255, 218, 185],
                peru: [205, 133, 63],
                pink: [255, 192, 203],
                plum: [221, 160, 221],
                powderblue: [176, 224, 230],
                purple: [128, 0, 128],
                rebeccapurple: [102, 51, 153],
                red: [255, 0, 0],
                rosybrown: [188, 143, 143],
                royalblue: [65, 105, 225],
                saddlebrown: [139, 69, 19],
                salmon: [250, 128, 114],
                sandybrown: [244, 164, 96],
                seagreen: [46, 139, 87],
                seashell: [255, 245, 238],
                sienna: [160, 82, 45],
                silver: [192, 192, 192],
                skyblue: [135, 206, 235],
                slateblue: [106, 90, 205],
                slategray: [112, 128, 144],
                slategrey: [112, 128, 144],
                snow: [255, 250, 250],
                springgreen: [0, 255, 127],
                steelblue: [70, 130, 180],
                tan: [210, 180, 140],
                teal: [0, 128, 128],
                thistle: [216, 191, 216],
                tomato: [255, 99, 71],
                turquoise: [64, 224, 208],
                violet: [238, 130, 238],
                wheat: [245, 222, 179],
                white: [255, 255, 255],
                whitesmoke: [245, 245, 245],
                yellow: [255, 255, 0],
                yellowgreen: [154, 205, 50]
            }, ot = {
                getRgba: st,
                getHsla: lt,
                getRgb: dt,
                getHsl: ut,
                getHwb: ct,
                getAlpha: ht,
                hexString: ft,
                rgbString: pt,
                rgbaString: gt,
                percentString: mt,
                percentaString: vt,
                hslString: bt,
                hslaString: yt,
                hwbString: xt,
                keyword: _t
            };

            function st(t) {
                if (t) {
                    var e = /^#([a-fA-F0-9]{3,4})$/i, a = /^#([a-fA-F0-9]{6}([a-fA-F0-9]{2})?)$/i,
                        n = /^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,
                        i = /^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,
                        r = /(\w+)/, o = [0, 0, 0], s = 1, l = t.match(e), c = "";
                    if (l) {
                        l = l[1], c = l[3];
                        for (var d = 0; d < o.length; d++) o[d] = parseInt(l[d] + l[d], 16);
                        c && (s = Math.round(parseInt(c + c, 16) / 255 * 100) / 100)
                    } else if (l = t.match(a)) {
                        c = l[2], l = l[1];
                        for (d = 0; d < o.length; d++) o[d] = parseInt(l.slice(2 * d, 2 * d + 2), 16);
                        c && (s = Math.round(parseInt(c, 16) / 255 * 100) / 100)
                    } else if (l = t.match(n)) {
                        for (d = 0; d < o.length; d++) o[d] = parseInt(l[d + 1]);
                        s = parseFloat(l[4])
                    } else if (l = t.match(i)) {
                        for (d = 0; d < o.length; d++) o[d] = Math.round(2.55 * parseFloat(l[d + 1]));
                        s = parseFloat(l[4])
                    } else if (l = t.match(r)) {
                        if ("transparent" == l[1]) return [0, 0, 0, 0];
                        if (o = rt[l[1]], !o) return
                    }
                    for (d = 0; d < o.length; d++) o[d] = Ct(o[d], 0, 255);
                    return s = s || 0 == s ? Ct(s, 0, 1) : 1, o[3] = s, o
                }
            }

            function lt(t) {
                if (t) {
                    var e = /^hsla?\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,
                        a = t.match(e);
                    if (a) {
                        var n = parseFloat(a[4]), i = Ct(parseInt(a[1]), 0, 360), r = Ct(parseFloat(a[2]), 0, 100),
                            o = Ct(parseFloat(a[3]), 0, 100), s = Ct(isNaN(n) ? 1 : n, 0, 1);
                        return [i, r, o, s]
                    }
                }
            }

            function ct(t) {
                if (t) {
                    var e = /^hwb\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,
                        a = t.match(e);
                    if (a) {
                        var n = parseFloat(a[4]), i = Ct(parseInt(a[1]), 0, 360), r = Ct(parseFloat(a[2]), 0, 100),
                            o = Ct(parseFloat(a[3]), 0, 100), s = Ct(isNaN(n) ? 1 : n, 0, 1);
                        return [i, r, o, s]
                    }
                }
            }

            function dt(t) {
                var e = st(t);
                return e && e.slice(0, 3)
            }

            function ut(t) {
                var e = lt(t);
                return e && e.slice(0, 3)
            }

            function ht(t) {
                var e = st(t);
                return e ? e[3] : (e = lt(t)) ? e[3] : (e = ct(t)) ? e[3] : void 0
            }

            function ft(t, e) {
                e = void 0 !== e && 3 === t.length ? e : t[3];
                return "#" + kt(t[0]) + kt(t[1]) + kt(t[2]) + (e >= 0 && e < 1 ? kt(Math.round(255 * e)) : "")
            }

            function pt(t, e) {
                return e < 1 || t[3] && t[3] < 1 ? gt(t, e) : "rgb(" + t[0] + ", " + t[1] + ", " + t[2] + ")"
            }

            function gt(t, e) {
                return void 0 === e && (e = void 0 !== t[3] ? t[3] : 1), "rgba(" + t[0] + ", " + t[1] + ", " + t[2] + ", " + e + ")"
            }

            function mt(t, e) {
                if (e < 1 || t[3] && t[3] < 1) return vt(t, e);
                var a = Math.round(t[0] / 255 * 100), n = Math.round(t[1] / 255 * 100),
                    i = Math.round(t[2] / 255 * 100);
                return "rgb(" + a + "%, " + n + "%, " + i + "%)"
            }

            function vt(t, e) {
                var a = Math.round(t[0] / 255 * 100), n = Math.round(t[1] / 255 * 100),
                    i = Math.round(t[2] / 255 * 100);
                return "rgba(" + a + "%, " + n + "%, " + i + "%, " + (e || t[3] || 1) + ")"
            }

            function bt(t, e) {
                return e < 1 || t[3] && t[3] < 1 ? yt(t, e) : "hsl(" + t[0] + ", " + t[1] + "%, " + t[2] + "%)"
            }

            function yt(t, e) {
                return void 0 === e && (e = void 0 !== t[3] ? t[3] : 1), "hsla(" + t[0] + ", " + t[1] + "%, " + t[2] + "%, " + e + ")"
            }

            function xt(t, e) {
                return void 0 === e && (e = void 0 !== t[3] ? t[3] : 1), "hwb(" + t[0] + ", " + t[1] + "%, " + t[2] + "%" + (void 0 !== e && 1 !== e ? ", " + e : "") + ")"
            }

            function _t(t) {
                return wt[t.slice(0, 3)]
            }

            function Ct(t, e, a) {
                return Math.min(Math.max(e, t), a)
            }

            function kt(t) {
                var e = t.toString(16).toUpperCase();
                return e.length < 2 ? "0" + e : e
            }

            var wt = {};
            for (var At in rt) wt[rt[At]] = At;
            var Mt = function (t) {
                return t instanceof Mt ? t : this instanceof Mt ? (this.valid = !1, this.values = {
                    rgb: [0, 0, 0],
                    hsl: [0, 0, 0],
                    hsv: [0, 0, 0],
                    hwb: [0, 0, 0],
                    cmyk: [0, 0, 0, 0],
                    alpha: 1
                }, void("string" === typeof t ? (e = ot.getRgba(t), e ? this.setValues("rgb", e) : (e = ot.getHsla(t)) ? this.setValues("hsl", e) : (e = ot.getHwb(t)) && this.setValues("hwb", e)) : "object" === typeof t && (e = t, void 0 !== e.r || void 0 !== e.red ? this.setValues("rgb", e) : void 0 !== e.l || void 0 !== e.lightness ? this.setValues("hsl", e) : void 0 !== e.v || void 0 !== e.value ? this.setValues("hsv", e) : void 0 !== e.w || void 0 !== e.whiteness ? this.setValues("hwb", e) : void 0 === e.c && void 0 === e.cyan || this.setValues("cmyk", e)))) : new Mt(t);
                var e
            };
            Mt.prototype = {
                isValid: function () {
                    return this.valid
                }, rgb: function () {
                    return this.setSpace("rgb", arguments)
                }, hsl: function () {
                    return this.setSpace("hsl", arguments)
                }, hsv: function () {
                    return this.setSpace("hsv", arguments)
                }, hwb: function () {
                    return this.setSpace("hwb", arguments)
                }, cmyk: function () {
                    return this.setSpace("cmyk", arguments)
                }, rgbArray: function () {
                    return this.values.rgb
                }, hslArray: function () {
                    return this.values.hsl
                }, hsvArray: function () {
                    return this.values.hsv
                }, hwbArray: function () {
                    var t = this.values;
                    return 1 !== t.alpha ? t.hwb.concat([t.alpha]) : t.hwb
                }, cmykArray: function () {
                    return this.values.cmyk
                }, rgbaArray: function () {
                    var t = this.values;
                    return t.rgb.concat([t.alpha])
                }, hslaArray: function () {
                    var t = this.values;
                    return t.hsl.concat([t.alpha])
                }, alpha: function (t) {
                    return void 0 === t ? this.values.alpha : (this.setValues("alpha", t), this)
                }, red: function (t) {
                    return this.setChannel("rgb", 0, t)
                }, green: function (t) {
                    return this.setChannel("rgb", 1, t)
                }, blue: function (t) {
                    return this.setChannel("rgb", 2, t)
                }, hue: function (t) {
                    return t && (t %= 360, t = t < 0 ? 360 + t : t), this.setChannel("hsl", 0, t)
                }, saturation: function (t) {
                    return this.setChannel("hsl", 1, t)
                }, lightness: function (t) {
                    return this.setChannel("hsl", 2, t)
                }, saturationv: function (t) {
                    return this.setChannel("hsv", 1, t)
                }, whiteness: function (t) {
                    return this.setChannel("hwb", 1, t)
                }, blackness: function (t) {
                    return this.setChannel("hwb", 2, t)
                }, value: function (t) {
                    return this.setChannel("hsv", 2, t)
                }, cyan: function (t) {
                    return this.setChannel("cmyk", 0, t)
                }, magenta: function (t) {
                    return this.setChannel("cmyk", 1, t)
                }, yellow: function (t) {
                    return this.setChannel("cmyk", 2, t)
                }, black: function (t) {
                    return this.setChannel("cmyk", 3, t)
                }, hexString: function () {
                    return ot.hexString(this.values.rgb)
                }, rgbString: function () {
                    return ot.rgbString(this.values.rgb, this.values.alpha)
                }, rgbaString: function () {
                    return ot.rgbaString(this.values.rgb, this.values.alpha)
                }, percentString: function () {
                    return ot.percentString(this.values.rgb, this.values.alpha)
                }, hslString: function () {
                    return ot.hslString(this.values.hsl, this.values.alpha)
                }, hslaString: function () {
                    return ot.hslaString(this.values.hsl, this.values.alpha)
                }, hwbString: function () {
                    return ot.hwbString(this.values.hwb, this.values.alpha)
                }, keyword: function () {
                    return ot.keyword(this.values.rgb, this.values.alpha)
                }, rgbNumber: function () {
                    var t = this.values.rgb;
                    return t[0] << 16 | t[1] << 8 | t[2]
                }, luminosity: function () {
                    for (var t = this.values.rgb, e = [], a = 0; a < t.length; a++) {
                        var n = t[a] / 255;
                        e[a] = n <= .03928 ? n / 12.92 : Math.pow((n + .055) / 1.055, 2.4)
                    }
                    return .2126 * e[0] + .7152 * e[1] + .0722 * e[2]
                }, contrast: function (t) {
                    var e = this.luminosity(), a = t.luminosity();
                    return e > a ? (e + .05) / (a + .05) : (a + .05) / (e + .05)
                }, level: function (t) {
                    var e = this.contrast(t);
                    return e >= 7.1 ? "AAA" : e >= 4.5 ? "AA" : ""
                }, dark: function () {
                    var t = this.values.rgb, e = (299 * t[0] + 587 * t[1] + 114 * t[2]) / 1e3;
                    return e < 128
                }, light: function () {
                    return !this.dark()
                }, negate: function () {
                    for (var t = [], e = 0; e < 3; e++) t[e] = 255 - this.values.rgb[e];
                    return this.setValues("rgb", t), this
                }, lighten: function (t) {
                    var e = this.values.hsl;
                    return e[2] += e[2] * t, this.setValues("hsl", e), this
                }, darken: function (t) {
                    var e = this.values.hsl;
                    return e[2] -= e[2] * t, this.setValues("hsl", e), this
                }, saturate: function (t) {
                    var e = this.values.hsl;
                    return e[1] += e[1] * t, this.setValues("hsl", e), this
                }, desaturate: function (t) {
                    var e = this.values.hsl;
                    return e[1] -= e[1] * t, this.setValues("hsl", e), this
                }, whiten: function (t) {
                    var e = this.values.hwb;
                    return e[1] += e[1] * t, this.setValues("hwb", e), this
                }, blacken: function (t) {
                    var e = this.values.hwb;
                    return e[2] += e[2] * t, this.setValues("hwb", e), this
                }, greyscale: function () {
                    var t = this.values.rgb, e = .3 * t[0] + .59 * t[1] + .11 * t[2];
                    return this.setValues("rgb", [e, e, e]), this
                }, clearer: function (t) {
                    var e = this.values.alpha;
                    return this.setValues("alpha", e - e * t), this
                }, opaquer: function (t) {
                    var e = this.values.alpha;
                    return this.setValues("alpha", e + e * t), this
                }, rotate: function (t) {
                    var e = this.values.hsl, a = (e[0] + t) % 360;
                    return e[0] = a < 0 ? 360 + a : a, this.setValues("hsl", e), this
                }, mix: function (t, e) {
                    var a = this, n = t, i = void 0 === e ? .5 : e, r = 2 * i - 1, o = a.alpha() - n.alpha(),
                        s = ((r * o === -1 ? r : (r + o) / (1 + r * o)) + 1) / 2, l = 1 - s;
                    return this.rgb(s * a.red() + l * n.red(), s * a.green() + l * n.green(), s * a.blue() + l * n.blue()).alpha(a.alpha() * i + n.alpha() * (1 - i))
                }, toJSON: function () {
                    return this.rgb()
                }, clone: function () {
                    var t, e, a = new Mt, n = this.values, i = a.values;
                    for (var r in n) n.hasOwnProperty(r) && (t = n[r], e = {}.toString.call(t), "[object Array]" === e ? i[r] = t.slice(0) : "[object Number]" === e ? i[r] = t : console.error("unexpected color value:", t));
                    return a
                }
            }, Mt.prototype.spaces = {
                rgb: ["red", "green", "blue"],
                hsl: ["hue", "saturation", "lightness"],
                hsv: ["hue", "saturation", "value"],
                hwb: ["hue", "whiteness", "blackness"],
                cmyk: ["cyan", "magenta", "yellow", "black"]
            }, Mt.prototype.maxes = {
                rgb: [255, 255, 255],
                hsl: [360, 100, 100],
                hsv: [360, 100, 100],
                hwb: [360, 100, 100],
                cmyk: [100, 100, 100, 100]
            }, Mt.prototype.getValues = function (t) {
                for (var e = this.values, a = {}, n = 0; n < t.length; n++) a[t.charAt(n)] = e[t][n];
                return 1 !== e.alpha && (a.a = e.alpha), a
            }, Mt.prototype.setValues = function (t, e) {
                var a, n, i = this.values, r = this.spaces, o = this.maxes, s = 1;
                if (this.valid = !0, "alpha" === t) s = e; else if (e.length) i[t] = e.slice(0, t.length), s = e[t.length]; else if (void 0 !== e[t.charAt(0)]) {
                    for (a = 0; a < t.length; a++) i[t][a] = e[t.charAt(a)];
                    s = e.a
                } else if (void 0 !== e[r[t][0]]) {
                    var l = r[t];
                    for (a = 0; a < t.length; a++) i[t][a] = e[l[a]];
                    s = e.alpha
                }
                if (i.alpha = Math.max(0, Math.min(1, void 0 === s ? i.alpha : s)), "alpha" === t) return !1;
                for (a = 0; a < t.length; a++) n = Math.max(0, Math.min(o[t][a], i[t][a])), i[t][a] = Math.round(n);
                for (var c in r) c !== t && (i[c] = it[t][c](i[t]));
                return !0
            }, Mt.prototype.setSpace = function (t, e) {
                var a = e[0];
                return void 0 === a ? this.getValues(t) : ("number" === typeof a && (a = Array.prototype.slice.call(e)), this.setValues(t, a), this)
            }, Mt.prototype.setChannel = function (t, e, a) {
                var n = this.values[t];
                return void 0 === a ? n[e] : a === n[e] ? this : (n[e] = a, this.setValues(t, n), this)
            }, "undefined" !== typeof window && (window.Color = Mt);
            var St = Mt, Dt = {
                noop: function () {
                }, uid: function () {
                    var t = 0;
                    return function () {
                        return t++
                    }
                }(), isNullOrUndef: function (t) {
                    return null === t || "undefined" === typeof t
                }, isArray: function (t) {
                    if (Array.isArray && Array.isArray(t)) return !0;
                    var e = Object.prototype.toString.call(t);
                    return "[object" === e.substr(0, 7) && "Array]" === e.substr(-6)
                }, isObject: function (t) {
                    return null !== t && "[object Object]" === Object.prototype.toString.call(t)
                }, isFinite: function (t) {
                    return ("number" === typeof t || t instanceof Number) && isFinite(t)
                }, valueOrDefault: function (t, e) {
                    return "undefined" === typeof t ? e : t
                }, valueAtIndexOrDefault: function (t, e, a) {
                    return Dt.valueOrDefault(Dt.isArray(t) ? t[e] : t, a)
                }, callback: function (t, e, a) {
                    if (t && "function" === typeof t.call) return t.apply(a, e)
                }, each: function (t, e, a, n) {
                    var i, r, o;
                    if (Dt.isArray(t)) if (r = t.length, n) for (i = r - 1; i >= 0; i--) e.call(a, t[i], i); else for (i = 0; i < r; i++) e.call(a, t[i], i); else if (Dt.isObject(t)) for (o = Object.keys(t), r = o.length, i = 0; i < r; i++) e.call(a, t[o[i]], o[i])
                }, arrayEquals: function (t, e) {
                    var a, n, i, r;
                    if (!t || !e || t.length !== e.length) return !1;
                    for (a = 0, n = t.length; a < n; ++a) if (i = t[a], r = e[a], i instanceof Array && r instanceof Array) {
                        if (!Dt.arrayEquals(i, r)) return !1
                    } else if (i !== r) return !1;
                    return !0
                }, clone: function (t) {
                    if (Dt.isArray(t)) return t.map(Dt.clone);
                    if (Dt.isObject(t)) {
                        for (var e = {}, a = Object.keys(t), n = a.length, i = 0; i < n; ++i) e[a[i]] = Dt.clone(t[a[i]]);
                        return e
                    }
                    return t
                }, _merger: function (t, e, a, n) {
                    var i = e[t], r = a[t];
                    Dt.isObject(i) && Dt.isObject(r) ? Dt.merge(i, r, n) : e[t] = Dt.clone(r)
                }, _mergerIf: function (t, e, a) {
                    var n = e[t], i = a[t];
                    Dt.isObject(n) && Dt.isObject(i) ? Dt.mergeIf(n, i) : e.hasOwnProperty(t) || (e[t] = Dt.clone(i))
                }, merge: function (t, e, a) {
                    var n, i, r, o, s, l = Dt.isArray(e) ? e : [e], c = l.length;
                    if (!Dt.isObject(t)) return t;
                    for (a = a || {}, n = a.merger || Dt._merger, i = 0; i < c; ++i) if (e = l[i], Dt.isObject(e)) for (r = Object.keys(e), s = 0, o = r.length; s < o; ++s) n(r[s], t, e, a);
                    return t
                }, mergeIf: function (t, e) {
                    return Dt.merge(t, e, {merger: Dt._mergerIf})
                }, extend: function (t) {
                    for (var e = function (e, a) {
                        t[a] = e
                    }, a = 1, n = arguments.length; a < n; ++a) Dt.each(arguments[a], e);
                    return t
                }, inherits: function (t) {
                    var e = this, a = t && t.hasOwnProperty("constructor") ? t.constructor : function () {
                        return e.apply(this, arguments)
                    }, n = function () {
                        this.constructor = a
                    };
                    return n.prototype = e.prototype, a.prototype = new n, a.extend = Dt.inherits, t && Dt.extend(a.prototype, t), a.__super__ = e.prototype, a
                }
            }, Ot = Dt;
            Dt.callCallback = Dt.callback, Dt.indexOf = function (t, e, a) {
                return Array.prototype.indexOf.call(t, e, a)
            }, Dt.getValueOrDefault = Dt.valueOrDefault, Dt.getValueAtIndexOrDefault = Dt.valueAtIndexOrDefault;
            var Pt = {
                linear: function (t) {
                    return t
                }, easeInQuad: function (t) {
                    return t * t
                }, easeOutQuad: function (t) {
                    return -t * (t - 2)
                }, easeInOutQuad: function (t) {
                    return (t /= .5) < 1 ? .5 * t * t : -.5 * (--t * (t - 2) - 1)
                }, easeInCubic: function (t) {
                    return t * t * t
                }, easeOutCubic: function (t) {
                    return (t -= 1) * t * t + 1
                }, easeInOutCubic: function (t) {
                    return (t /= .5) < 1 ? .5 * t * t * t : .5 * ((t -= 2) * t * t + 2)
                }, easeInQuart: function (t) {
                    return t * t * t * t
                }, easeOutQuart: function (t) {
                    return -((t -= 1) * t * t * t - 1)
                }, easeInOutQuart: function (t) {
                    return (t /= .5) < 1 ? .5 * t * t * t * t : -.5 * ((t -= 2) * t * t * t - 2)
                }, easeInQuint: function (t) {
                    return t * t * t * t * t
                }, easeOutQuint: function (t) {
                    return (t -= 1) * t * t * t * t + 1
                }, easeInOutQuint: function (t) {
                    return (t /= .5) < 1 ? .5 * t * t * t * t * t : .5 * ((t -= 2) * t * t * t * t + 2)
                }, easeInSine: function (t) {
                    return 1 - Math.cos(t * (Math.PI / 2))
                }, easeOutSine: function (t) {
                    return Math.sin(t * (Math.PI / 2))
                }, easeInOutSine: function (t) {
                    return -.5 * (Math.cos(Math.PI * t) - 1)
                }, easeInExpo: function (t) {
                    return 0 === t ? 0 : Math.pow(2, 10 * (t - 1))
                }, easeOutExpo: function (t) {
                    return 1 === t ? 1 : 1 - Math.pow(2, -10 * t)
                }, easeInOutExpo: function (t) {
                    return 0 === t ? 0 : 1 === t ? 1 : (t /= .5) < 1 ? .5 * Math.pow(2, 10 * (t - 1)) : .5 * (2 - Math.pow(2, -10 * --t))
                }, easeInCirc: function (t) {
                    return t >= 1 ? t : -(Math.sqrt(1 - t * t) - 1)
                }, easeOutCirc: function (t) {
                    return Math.sqrt(1 - (t -= 1) * t)
                }, easeInOutCirc: function (t) {
                    return (t /= .5) < 1 ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1)
                }, easeInElastic: function (t) {
                    var e = 1.70158, a = 0, n = 1;
                    return 0 === t ? 0 : 1 === t ? 1 : (a || (a = .3), n < 1 ? (n = 1, e = a / 4) : e = a / (2 * Math.PI) * Math.asin(1 / n), -n * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - e) * (2 * Math.PI) / a))
                }, easeOutElastic: function (t) {
                    var e = 1.70158, a = 0, n = 1;
                    return 0 === t ? 0 : 1 === t ? 1 : (a || (a = .3), n < 1 ? (n = 1, e = a / 4) : e = a / (2 * Math.PI) * Math.asin(1 / n), n * Math.pow(2, -10 * t) * Math.sin((t - e) * (2 * Math.PI) / a) + 1)
                }, easeInOutElastic: function (t) {
                    var e = 1.70158, a = 0, n = 1;
                    return 0 === t ? 0 : 2 === (t /= .5) ? 1 : (a || (a = .45), n < 1 ? (n = 1, e = a / 4) : e = a / (2 * Math.PI) * Math.asin(1 / n), t < 1 ? n * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - e) * (2 * Math.PI) / a) * -.5 : n * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - e) * (2 * Math.PI) / a) * .5 + 1)
                }, easeInBack: function (t) {
                    var e = 1.70158;
                    return t * t * ((e + 1) * t - e)
                }, easeOutBack: function (t) {
                    var e = 1.70158;
                    return (t -= 1) * t * ((e + 1) * t + e) + 1
                }, easeInOutBack: function (t) {
                    var e = 1.70158;
                    return (t /= .5) < 1 ? t * t * ((1 + (e *= 1.525)) * t - e) * .5 : .5 * ((t -= 2) * t * ((1 + (e *= 1.525)) * t + e) + 2)
                }, easeInBounce: function (t) {
                    return 1 - Pt.easeOutBounce(1 - t)
                }, easeOutBounce: function (t) {
                    return t < 1 / 2.75 ? 7.5625 * t * t : t < 2 / 2.75 ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : t < 2.5 / 2.75 ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375
                }, easeInOutBounce: function (t) {
                    return t < .5 ? .5 * Pt.easeInBounce(2 * t) : .5 * Pt.easeOutBounce(2 * t - 1) + .5
                }
            }, Tt = {effects: Pt};
            Ot.easingEffects = Pt;
            var It = Math.PI, Rt = It / 180, Ft = 2 * It, Bt = It / 2, Lt = It / 4, Nt = 2 * It / 3, Et = {
                clear: function (t) {
                    t.ctx.clearRect(0, 0, t.width, t.height)
                }, roundedRect: function (t, e, a, n, i, r) {
                    if (r) {
                        var o = Math.min(r, i / 2, n / 2), s = e + o, l = a + o, c = e + n - o, d = a + i - o;
                        t.moveTo(e, l), s < c && l < d ? (t.arc(s, l, o, -It, -Bt), t.arc(c, l, o, -Bt, 0), t.arc(c, d, o, 0, Bt), t.arc(s, d, o, Bt, It)) : s < c ? (t.moveTo(s, a), t.arc(c, l, o, -Bt, Bt), t.arc(s, l, o, Bt, It + Bt)) : l < d ? (t.arc(s, l, o, -It, 0), t.arc(s, d, o, 0, It)) : t.arc(s, l, o, -It, It), t.closePath(), t.moveTo(e, a)
                    } else t.rect(e, a, n, i)
                }, drawPoint: function (t, e, a, n, i, r) {
                    var o, s, l, c, d, u = (r || 0) * Rt;
                    if (e && "object" === typeof e && (o = e.toString(), "[object HTMLImageElement]" === o || "[object HTMLCanvasElement]" === o)) t.drawImage(e, n - e.width / 2, i - e.height / 2, e.width, e.height); else if (!(isNaN(a) || a <= 0)) {
                        switch (t.beginPath(), e) {
                            default:
                                t.arc(n, i, a, 0, Ft), t.closePath();
                                break;
                            case"triangle":
                                t.moveTo(n + Math.sin(u) * a, i - Math.cos(u) * a), u += Nt, t.lineTo(n + Math.sin(u) * a, i - Math.cos(u) * a), u += Nt, t.lineTo(n + Math.sin(u) * a, i - Math.cos(u) * a), t.closePath();
                                break;
                            case"rectRounded":
                                d = .516 * a, c = a - d, s = Math.cos(u + Lt) * c, l = Math.sin(u + Lt) * c, t.arc(n - s, i - l, d, u - It, u - Bt), t.arc(n + l, i - s, d, u - Bt, u), t.arc(n + s, i + l, d, u, u + Bt), t.arc(n - l, i + s, d, u + Bt, u + It), t.closePath();
                                break;
                            case"rect":
                                if (!r) {
                                    c = Math.SQRT1_2 * a, t.rect(n - c, i - c, 2 * c, 2 * c);
                                    break
                                }
                                u += Lt;
                            case"rectRot":
                                s = Math.cos(u) * a, l = Math.sin(u) * a, t.moveTo(n - s, i - l), t.lineTo(n + l, i - s), t.lineTo(n + s, i + l), t.lineTo(n - l, i + s), t.closePath();
                                break;
                            case"crossRot":
                                u += Lt;
                            case"cross":
                                s = Math.cos(u) * a, l = Math.sin(u) * a, t.moveTo(n - s, i - l), t.lineTo(n + s, i + l), t.moveTo(n + l, i - s), t.lineTo(n - l, i + s);
                                break;
                            case"star":
                                s = Math.cos(u) * a, l = Math.sin(u) * a, t.moveTo(n - s, i - l), t.lineTo(n + s, i + l), t.moveTo(n + l, i - s), t.lineTo(n - l, i + s), u += Lt, s = Math.cos(u) * a, l = Math.sin(u) * a, t.moveTo(n - s, i - l), t.lineTo(n + s, i + l), t.moveTo(n + l, i - s), t.lineTo(n - l, i + s);
                                break;
                            case"line":
                                s = Math.cos(u) * a, l = Math.sin(u) * a, t.moveTo(n - s, i - l), t.lineTo(n + s, i + l);
                                break;
                            case"dash":
                                t.moveTo(n, i), t.lineTo(n + Math.cos(u) * a, i + Math.sin(u) * a);
                                break
                        }
                        t.fill(), t.stroke()
                    }
                }, _isPointInArea: function (t, e) {
                    var a = 1e-6;
                    return t.x > e.left - a && t.x < e.right + a && t.y > e.top - a && t.y < e.bottom + a
                }, clipArea: function (t, e) {
                    t.save(), t.beginPath(), t.rect(e.left, e.top, e.right - e.left, e.bottom - e.top), t.clip()
                }, unclipArea: function (t) {
                    t.restore()
                }, lineTo: function (t, e, a, n) {
                    var i = a.steppedLine;
                    if (i) {
                        if ("middle" === i) {
                            var r = (e.x + a.x) / 2;
                            t.lineTo(r, n ? a.y : e.y), t.lineTo(r, n ? e.y : a.y)
                        } else "after" === i && !n || "after" !== i && n ? t.lineTo(e.x, a.y) : t.lineTo(a.x, e.y);
                        t.lineTo(a.x, a.y)
                    } else a.tension ? t.bezierCurveTo(n ? e.controlPointPreviousX : e.controlPointNextX, n ? e.controlPointPreviousY : e.controlPointNextY, n ? a.controlPointNextX : a.controlPointPreviousX, n ? a.controlPointNextY : a.controlPointPreviousY, a.x, a.y) : t.lineTo(a.x, a.y)
                }
            }, jt = Et;
            Ot.clear = Et.clear, Ot.drawRoundedRectangle = function (t) {
                t.beginPath(), Et.roundedRect.apply(Et, arguments)
            };
            var Ht = {
                _set: function (t, e) {
                    return Ot.merge(this[t] || (this[t] = {}), e)
                }
            };
            Ht._set("global", {
                defaultColor: "rgba(0,0,0,0.1)",
                defaultFontColor: "#666",
                defaultFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                defaultFontSize: 12,
                defaultFontStyle: "normal",
                defaultLineHeight: 1.2,
                showLines: !0
            });
            var $t = Ht, zt = Ot.valueOrDefault;

            function Vt(t) {
                return !t || Ot.isNullOrUndef(t.size) || Ot.isNullOrUndef(t.family) ? null : (t.style ? t.style + " " : "") + (t.weight ? t.weight + " " : "") + t.size + "px " + t.family
            }

            var Wt = {
                toLineHeight: function (t, e) {
                    var a = ("" + t).match(/^(normal|(\d+(?:\.\d+)?)(px|em|%)?)$/);
                    if (!a || "normal" === a[1]) return 1.2 * e;
                    switch (t = +a[2], a[3]) {
                        case"px":
                            return t;
                        case"%":
                            t /= 100;
                            break;
                        default:
                            break
                    }
                    return e * t
                }, toPadding: function (t) {
                    var e, a, n, i;
                    return Ot.isObject(t) ? (e = +t.top || 0, a = +t.right || 0, n = +t.bottom || 0, i = +t.left || 0) : e = a = n = i = +t || 0, {
                        top: e,
                        right: a,
                        bottom: n,
                        left: i,
                        height: e + n,
                        width: i + a
                    }
                }, _parseFont: function (t) {
                    var e = $t.global, a = zt(t.fontSize, e.defaultFontSize), n = {
                        family: zt(t.fontFamily, e.defaultFontFamily),
                        lineHeight: Ot.options.toLineHeight(zt(t.lineHeight, e.defaultLineHeight), a),
                        size: a,
                        style: zt(t.fontStyle, e.defaultFontStyle),
                        weight: null,
                        string: ""
                    };
                    return n.string = Vt(n), n
                }, resolve: function (t, e, a) {
                    var n, i, r;
                    for (n = 0, i = t.length; n < i; ++n) if (r = t[n], void 0 !== r && (void 0 !== e && "function" === typeof r && (r = r(e)), void 0 !== a && Ot.isArray(r) && (r = r[a]), void 0 !== r)) return r
                }
            }, Ut = Ot, qt = Tt, Yt = jt, Gt = Wt;

            function Kt(t, e, a, n) {
                var i, r, o, s, l, c, d, u, h, f = Object.keys(a);
                for (i = 0, r = f.length; i < r; ++i) if (o = f[i], c = a[o], e.hasOwnProperty(o) || (e[o] = c), s = e[o], s !== c && "_" !== o[0]) {
                    if (t.hasOwnProperty(o) || (t[o] = s), l = t[o], d = typeof c, d === typeof l) if ("string" === d) {
                        if (u = St(l), u.valid && (h = St(c), h.valid)) {
                            e[o] = h.mix(u, n).rgbString();
                            continue
                        }
                    } else if (Ut.isFinite(l) && Ut.isFinite(c)) {
                        e[o] = l + (c - l) * n;
                        continue
                    }
                    e[o] = c
                }
            }

            Ut.easing = qt, Ut.canvas = Yt, Ut.options = Gt;
            var Xt = function (t) {
                Ut.extend(this, t), this.initialize.apply(this, arguments)
            };
            Ut.extend(Xt.prototype, {
                initialize: function () {
                    this.hidden = !1
                }, pivot: function () {
                    var t = this;
                    return t._view || (t._view = Ut.clone(t._model)), t._start = {}, t
                }, transition: function (t) {
                    var e = this, a = e._model, n = e._start, i = e._view;
                    return a && 1 !== t ? (i || (i = e._view = {}), n || (n = e._start = {}), Kt(n, i, a, t), e) : (e._view = a, e._start = null, e)
                }, tooltipPosition: function () {
                    return {x: this._model.x, y: this._model.y}
                }, hasValue: function () {
                    return Ut.isNumber(this._model.x) && Ut.isNumber(this._model.y)
                }
            }), Xt.extend = Ut.inherits;
            var Jt = Xt, Zt = Jt.extend({
                chart: null,
                currentStep: 0,
                numSteps: 60,
                easing: "",
                render: null,
                onAnimationProgress: null,
                onAnimationComplete: null
            }), Qt = Zt;
            Object.defineProperty(Zt.prototype, "animationObject", {
                get: function () {
                    return this
                }
            }), Object.defineProperty(Zt.prototype, "chartInstance", {
                get: function () {
                    return this.chart
                }, set: function (t) {
                    this.chart = t
                }
            }), $t._set("global", {
                animation: {
                    duration: 1e3,
                    easing: "easeOutQuart",
                    onProgress: Ut.noop,
                    onComplete: Ut.noop
                }
            });
            var te = {
                animations: [], request: null, addAnimation: function (t, e, a, n) {
                    var i, r, o = this.animations;
                    for (e.chart = t, e.startTime = Date.now(), e.duration = a, n || (t.animating = !0), i = 0, r = o.length; i < r; ++i) if (o[i].chart === t) return void(o[i] = e);
                    o.push(e), 1 === o.length && this.requestAnimationFrame()
                }, cancelAnimation: function (t) {
                    var e = Ut.findIndex(this.animations, function (e) {
                        return e.chart === t
                    });
                    -1 !== e && (this.animations.splice(e, 1), t.animating = !1)
                }, requestAnimationFrame: function () {
                    var t = this;
                    null === t.request && (t.request = Ut.requestAnimFrame.call(window, function () {
                        t.request = null, t.startDigest()
                    }))
                }, startDigest: function () {
                    var t = this;
                    t.advance(), t.animations.length > 0 && t.requestAnimationFrame()
                }, advance: function () {
                    var t, e, a, n, i = this.animations, r = 0;
                    while (r < i.length) t = i[r], e = t.chart, a = t.numSteps, n = Math.floor((Date.now() - t.startTime) / t.duration * a) + 1, t.currentStep = Math.min(n, a), Ut.callback(t.render, [e, t], e), Ut.callback(t.onAnimationProgress, [t], e), t.currentStep >= a ? (Ut.callback(t.onAnimationComplete, [t], e), e.animating = !1, i.splice(r, 1)) : ++r
                }
            }, ee = Ut.options.resolve, ae = ["push", "pop", "shift", "splice", "unshift"];

            function ne(t, e) {
                t._chartjs ? t._chartjs.listeners.push(e) : (Object.defineProperty(t, "_chartjs", {
                    configurable: !0,
                    enumerable: !1,
                    value: {listeners: [e]}
                }), ae.forEach(function (e) {
                    var a = "onData" + e.charAt(0).toUpperCase() + e.slice(1), n = t[e];
                    Object.defineProperty(t, e, {
                        configurable: !0, enumerable: !1, value: function () {
                            var e = Array.prototype.slice.call(arguments), i = n.apply(this, e);
                            return Ut.each(t._chartjs.listeners, function (t) {
                                "function" === typeof t[a] && t[a].apply(t, e)
                            }), i
                        }
                    })
                }))
            }

            function ie(t, e) {
                var a = t._chartjs;
                if (a) {
                    var n = a.listeners, i = n.indexOf(e);
                    -1 !== i && n.splice(i, 1), n.length > 0 || (ae.forEach(function (e) {
                        delete t[e]
                    }), delete t._chartjs)
                }
            }

            var re = function (t, e) {
                this.initialize(t, e)
            };
            Ut.extend(re.prototype, {
                datasetElementType: null, dataElementType: null, initialize: function (t, e) {
                    var a = this;
                    a.chart = t, a.index = e, a.linkScales(), a.addElements()
                }, updateIndex: function (t) {
                    this.index = t
                }, linkScales: function () {
                    var t = this, e = t.getMeta(), a = t.getDataset();
                    null !== e.xAxisID && e.xAxisID in t.chart.scales || (e.xAxisID = a.xAxisID || t.chart.options.scales.xAxes[0].id), null !== e.yAxisID && e.yAxisID in t.chart.scales || (e.yAxisID = a.yAxisID || t.chart.options.scales.yAxes[0].id)
                }, getDataset: function () {
                    return this.chart.data.datasets[this.index]
                }, getMeta: function () {
                    return this.chart.getDatasetMeta(this.index)
                }, getScaleForId: function (t) {
                    return this.chart.scales[t]
                }, _getValueScaleId: function () {
                    return this.getMeta().yAxisID
                }, _getIndexScaleId: function () {
                    return this.getMeta().xAxisID
                }, _getValueScale: function () {
                    return this.getScaleForId(this._getValueScaleId())
                }, _getIndexScale: function () {
                    return this.getScaleForId(this._getIndexScaleId())
                }, reset: function () {
                    this.update(!0)
                }, destroy: function () {
                    this._data && ie(this._data, this)
                }, createMetaDataset: function () {
                    var t = this, e = t.datasetElementType;
                    return e && new e({_chart: t.chart, _datasetIndex: t.index})
                }, createMetaData: function (t) {
                    var e = this, a = e.dataElementType;
                    return a && new a({_chart: e.chart, _datasetIndex: e.index, _index: t})
                }, addElements: function () {
                    var t, e, a = this, n = a.getMeta(), i = a.getDataset().data || [], r = n.data;
                    for (t = 0, e = i.length; t < e; ++t) r[t] = r[t] || a.createMetaData(t);
                    n.dataset = n.dataset || a.createMetaDataset()
                }, addElementAndReset: function (t) {
                    var e = this.createMetaData(t);
                    this.getMeta().data.splice(t, 0, e), this.updateElement(e, t, !0)
                }, buildOrUpdateElements: function () {
                    var t = this, e = t.getDataset(), a = e.data || (e.data = []);
                    t._data !== a && (t._data && ie(t._data, t), a && Object.isExtensible(a) && ne(a, t), t._data = a), t.resyncElements()
                }, update: Ut.noop, transition: function (t) {
                    for (var e = this.getMeta(), a = e.data || [], n = a.length, i = 0; i < n; ++i) a[i].transition(t);
                    e.dataset && e.dataset.transition(t)
                }, draw: function () {
                    var t = this.getMeta(), e = t.data || [], a = e.length, n = 0;
                    for (t.dataset && t.dataset.draw(); n < a; ++n) e[n].draw()
                }, removeHoverStyle: function (t) {
                    Ut.merge(t._model, t.$previousStyle || {}), delete t.$previousStyle
                }, setHoverStyle: function (t) {
                    var e = this.chart.data.datasets[t._datasetIndex], a = t._index, n = t.custom || {}, i = t._model,
                        r = Ut.getHoverColor;
                    t.$previousStyle = {
                        backgroundColor: i.backgroundColor,
                        borderColor: i.borderColor,
                        borderWidth: i.borderWidth
                    }, i.backgroundColor = ee([n.hoverBackgroundColor, e.hoverBackgroundColor, r(i.backgroundColor)], void 0, a), i.borderColor = ee([n.hoverBorderColor, e.hoverBorderColor, r(i.borderColor)], void 0, a), i.borderWidth = ee([n.hoverBorderWidth, e.hoverBorderWidth, i.borderWidth], void 0, a)
                }, resyncElements: function () {
                    var t = this, e = t.getMeta(), a = t.getDataset().data, n = e.data.length, i = a.length;
                    i < n ? e.data.splice(i, n - i) : i > n && t.insertElements(n, i - n)
                }, insertElements: function (t, e) {
                    for (var a = 0; a < e; ++a) this.addElementAndReset(t + a)
                }, onDataPush: function () {
                    var t = arguments.length;
                    this.insertElements(this.getDataset().data.length - t, t)
                }, onDataPop: function () {
                    this.getMeta().data.pop()
                }, onDataShift: function () {
                    this.getMeta().data.shift()
                }, onDataSplice: function (t, e) {
                    this.getMeta().data.splice(t, e), this.insertElements(t, arguments.length - 2)
                }, onDataUnshift: function () {
                    this.insertElements(0, arguments.length)
                }
            }), re.extend = Ut.inherits;
            var oe = re;
            $t._set("global", {
                elements: {
                    arc: {
                        backgroundColor: $t.global.defaultColor,
                        borderColor: "#fff",
                        borderWidth: 2,
                        borderAlign: "center"
                    }
                }
            });
            var se = Jt.extend({
                inLabelRange: function (t) {
                    var e = this._view;
                    return !!e && Math.pow(t - e.x, 2) < Math.pow(e.radius + e.hoverRadius, 2)
                }, inRange: function (t, e) {
                    var a = this._view;
                    if (a) {
                        var n = Ut.getAngleFromPoint(a, {x: t, y: e}), i = n.angle, r = n.distance, o = a.startAngle,
                            s = a.endAngle;
                        while (s < o) s += 2 * Math.PI;
                        while (i > s) i -= 2 * Math.PI;
                        while (i < o) i += 2 * Math.PI;
                        var l = i >= o && i <= s, c = r >= a.innerRadius && r <= a.outerRadius;
                        return l && c
                    }
                    return !1
                }, getCenterPoint: function () {
                    var t = this._view, e = (t.startAngle + t.endAngle) / 2, a = (t.innerRadius + t.outerRadius) / 2;
                    return {x: t.x + Math.cos(e) * a, y: t.y + Math.sin(e) * a}
                }, getArea: function () {
                    var t = this._view;
                    return Math.PI * ((t.endAngle - t.startAngle) / (2 * Math.PI)) * (Math.pow(t.outerRadius, 2) - Math.pow(t.innerRadius, 2))
                }, tooltipPosition: function () {
                    var t = this._view, e = t.startAngle + (t.endAngle - t.startAngle) / 2,
                        a = (t.outerRadius - t.innerRadius) / 2 + t.innerRadius;
                    return {x: t.x + Math.cos(e) * a, y: t.y + Math.sin(e) * a}
                }, draw: function () {
                    var t, e = this._chart.ctx, a = this._view, n = a.startAngle, i = a.endAngle,
                        r = "inner" === a.borderAlign ? .33 : 0;
                    e.save(), e.beginPath(), e.arc(a.x, a.y, Math.max(a.outerRadius - r, 0), n, i), e.arc(a.x, a.y, a.innerRadius, i, n, !0), e.closePath(), e.fillStyle = a.backgroundColor, e.fill(), a.borderWidth && ("inner" === a.borderAlign ? (e.beginPath(), t = r / a.outerRadius, e.arc(a.x, a.y, a.outerRadius, n - t, i + t), a.innerRadius > r ? (t = r / a.innerRadius, e.arc(a.x, a.y, a.innerRadius - r, i + t, n - t, !0)) : e.arc(a.x, a.y, r, i + Math.PI / 2, n - Math.PI / 2), e.closePath(), e.clip(), e.beginPath(), e.arc(a.x, a.y, a.outerRadius, n, i), e.arc(a.x, a.y, a.innerRadius, i, n, !0), e.closePath(), e.lineWidth = 2 * a.borderWidth, e.lineJoin = "round") : (e.lineWidth = a.borderWidth, e.lineJoin = "bevel"), e.strokeStyle = a.borderColor, e.stroke()), e.restore()
                }
            }), le = Ut.valueOrDefault, ce = $t.global.defaultColor;
            $t._set("global", {
                elements: {
                    line: {
                        tension: .4,
                        backgroundColor: ce,
                        borderWidth: 3,
                        borderColor: ce,
                        borderCapStyle: "butt",
                        borderDash: [],
                        borderDashOffset: 0,
                        borderJoinStyle: "miter",
                        capBezierPoints: !0,
                        fill: !0
                    }
                }
            });
            var de = Jt.extend({
                draw: function () {
                    var t, e, a, n, i = this, r = i._view, o = i._chart.ctx, s = r.spanGaps, l = i._children.slice(),
                        c = $t.global, d = c.elements.line, u = -1;
                    for (i._loop && l.length && l.push(l[0]), o.save(), o.lineCap = r.borderCapStyle || d.borderCapStyle, o.setLineDash && o.setLineDash(r.borderDash || d.borderDash), o.lineDashOffset = le(r.borderDashOffset, d.borderDashOffset), o.lineJoin = r.borderJoinStyle || d.borderJoinStyle, o.lineWidth = le(r.borderWidth, d.borderWidth), o.strokeStyle = r.borderColor || c.defaultColor, o.beginPath(), u = -1, t = 0; t < l.length; ++t) e = l[t], a = Ut.previousItem(l, t), n = e._view, 0 === t ? n.skip || (o.moveTo(n.x, n.y), u = t) : (a = -1 === u ? a : l[u], n.skip || (u !== t - 1 && !s || -1 === u ? o.moveTo(n.x, n.y) : Ut.canvas.lineTo(o, a._view, e._view), u = t));
                    o.stroke(), o.restore()
                }
            }), ue = Ut.valueOrDefault, he = $t.global.defaultColor;

            function fe(t) {
                var e = this._view;
                return !!e && Math.abs(t - e.x) < e.radius + e.hitRadius
            }

            function pe(t) {
                var e = this._view;
                return !!e && Math.abs(t - e.y) < e.radius + e.hitRadius
            }

            $t._set("global", {
                elements: {
                    point: {
                        radius: 3,
                        pointStyle: "circle",
                        backgroundColor: he,
                        borderColor: he,
                        borderWidth: 1,
                        hitRadius: 1,
                        hoverRadius: 4,
                        hoverBorderWidth: 1
                    }
                }
            });
            var ge = Jt.extend({
                inRange: function (t, e) {
                    var a = this._view;
                    return !!a && Math.pow(t - a.x, 2) + Math.pow(e - a.y, 2) < Math.pow(a.hitRadius + a.radius, 2)
                }, inLabelRange: fe, inXRange: fe, inYRange: pe, getCenterPoint: function () {
                    var t = this._view;
                    return {x: t.x, y: t.y}
                }, getArea: function () {
                    return Math.PI * Math.pow(this._view.radius, 2)
                }, tooltipPosition: function () {
                    var t = this._view;
                    return {x: t.x, y: t.y, padding: t.radius + t.borderWidth}
                }, draw: function (t) {
                    var e = this._view, a = this._chart.ctx, n = e.pointStyle, i = e.rotation, r = e.radius, o = e.x,
                        s = e.y, l = $t.global, c = l.defaultColor;
                    e.skip || (void 0 === t || Ut.canvas._isPointInArea(e, t)) && (a.strokeStyle = e.borderColor || c, a.lineWidth = ue(e.borderWidth, l.elements.point.borderWidth), a.fillStyle = e.backgroundColor || c, Ut.canvas.drawPoint(a, n, r, o, s, i))
                }
            }), me = $t.global.defaultColor;

            function ve(t) {
                return t && void 0 !== t.width
            }

            function be(t) {
                var e, a, n, i, r;
                return ve(t) ? (r = t.width / 2, e = t.x - r, a = t.x + r, n = Math.min(t.y, t.base), i = Math.max(t.y, t.base)) : (r = t.height / 2, e = Math.min(t.x, t.base), a = Math.max(t.x, t.base), n = t.y - r, i = t.y + r), {
                    left: e,
                    top: n,
                    right: a,
                    bottom: i
                }
            }

            function ye(t, e, a) {
                return t === e ? a : t === a ? e : t
            }

            function xe(t) {
                var e = t.borderSkipped, a = {};
                return e ? (t.horizontal ? t.base > t.x && (e = ye(e, "left", "right")) : t.base < t.y && (e = ye(e, "bottom", "top")), a[e] = !0, a) : a
            }

            function _e(t, e, a) {
                var n, i, r, o, s = t.borderWidth, l = xe(t);
                return Ut.isObject(s) ? (n = +s.top || 0, i = +s.right || 0, r = +s.bottom || 0, o = +s.left || 0) : n = i = r = o = +s || 0, {
                    t: l.top || n < 0 ? 0 : n > a ? a : n,
                    r: l.right || i < 0 ? 0 : i > e ? e : i,
                    b: l.bottom || r < 0 ? 0 : r > a ? a : r,
                    l: l.left || o < 0 ? 0 : o > e ? e : o
                }
            }

            function Ce(t) {
                var e = be(t), a = e.right - e.left, n = e.bottom - e.top, i = _e(t, a / 2, n / 2);
                return {
                    outer: {x: e.left, y: e.top, w: a, h: n},
                    inner: {x: e.left + i.l, y: e.top + i.t, w: a - i.l - i.r, h: n - i.t - i.b}
                }
            }

            function ke(t, e, a) {
                var n = null === e, i = null === a, r = !(!t || n && i) && be(t);
                return r && (n || e >= r.left && e <= r.right) && (i || a >= r.top && a <= r.bottom)
            }

            $t._set("global", {
                elements: {
                    rectangle: {
                        backgroundColor: me,
                        borderColor: me,
                        borderSkipped: "bottom",
                        borderWidth: 0
                    }
                }
            });
            var we = Jt.extend({
                draw: function () {
                    var t = this._chart.ctx, e = this._view, a = Ce(e), n = a.outer, i = a.inner;
                    t.fillStyle = e.backgroundColor, t.fillRect(n.x, n.y, n.w, n.h), n.w === i.w && n.h === i.h || (t.save(), t.beginPath(), t.rect(n.x, n.y, n.w, n.h), t.clip(), t.fillStyle = e.borderColor, t.rect(i.x, i.y, i.w, i.h), t.fill("evenodd"), t.restore())
                }, height: function () {
                    var t = this._view;
                    return t.base - t.y
                }, inRange: function (t, e) {
                    return ke(this._view, t, e)
                }, inLabelRange: function (t, e) {
                    var a = this._view;
                    return ve(a) ? ke(a, t, null) : ke(a, null, e)
                }, inXRange: function (t) {
                    return ke(this._view, t, null)
                }, inYRange: function (t) {
                    return ke(this._view, null, t)
                }, getCenterPoint: function () {
                    var t, e, a = this._view;
                    return ve(a) ? (t = a.x, e = (a.y + a.base) / 2) : (t = (a.x + a.base) / 2, e = a.y), {x: t, y: e}
                }, getArea: function () {
                    var t = this._view;
                    return ve(t) ? t.width * Math.abs(t.y - t.base) : t.height * Math.abs(t.x - t.base)
                }, tooltipPosition: function () {
                    var t = this._view;
                    return {x: t.x, y: t.y}
                }
            }), Ae = {}, Me = se, Se = de, De = ge, Oe = we;
            Ae.Arc = Me, Ae.Line = Se, Ae.Point = De, Ae.Rectangle = Oe;
            var Pe = Ut.options.resolve;

            function Te(t, e) {
                var a, n, i, r, o = t.isHorizontal() ? t.width : t.height, s = t.getTicks();
                for (i = 1, r = e.length; i < r; ++i) o = Math.min(o, Math.abs(e[i] - e[i - 1]));
                for (i = 0, r = s.length; i < r; ++i) n = t.getPixelForTick(i), o = i > 0 ? Math.min(o, n - a) : o, a = n;
                return o
            }

            function Ie(t, e, a) {
                var n, i, r = a.barThickness, o = e.stackCount, s = e.pixels[t];
                return Ut.isNullOrUndef(r) ? (n = e.min * a.categoryPercentage, i = a.barPercentage) : (n = r * o, i = 1), {
                    chunk: n / o,
                    ratio: i,
                    start: s - n / 2
                }
            }

            function Re(t, e, a) {
                var n, i, r = e.pixels, o = r[t], s = t > 0 ? r[t - 1] : null, l = t < r.length - 1 ? r[t + 1] : null,
                    c = a.categoryPercentage;
                return null === s && (s = o - (null === l ? e.end - e.start : l - o)), null === l && (l = o + o - s), n = o - (o - Math.min(s, l)) / 2 * c, i = Math.abs(l - s) / 2 * c, {
                    chunk: i / e.stackCount,
                    ratio: a.barPercentage,
                    start: n
                }
            }

            $t._set("bar", {
                hover: {mode: "label"},
                scales: {
                    xAxes: [{
                        type: "category",
                        categoryPercentage: .8,
                        barPercentage: .9,
                        offset: !0,
                        gridLines: {offsetGridLines: !0}
                    }], yAxes: [{type: "linear"}]
                }
            });
            var Fe = oe.extend({
                dataElementType: Ae.Rectangle, initialize: function () {
                    var t, e = this;
                    oe.prototype.initialize.apply(e, arguments), t = e.getMeta(), t.stack = e.getDataset().stack, t.bar = !0
                }, update: function (t) {
                    var e, a, n = this, i = n.getMeta().data;
                    for (n._ruler = n.getRuler(), e = 0, a = i.length; e < a; ++e) n.updateElement(i[e], e, t)
                }, updateElement: function (t, e, a) {
                    var n = this, i = n.getMeta(), r = n.getDataset(), o = n._resolveElementOptions(t, e);
                    t._xScale = n.getScaleForId(i.xAxisID), t._yScale = n.getScaleForId(i.yAxisID), t._datasetIndex = n.index, t._index = e, t._model = {
                        backgroundColor: o.backgroundColor,
                        borderColor: o.borderColor,
                        borderSkipped: o.borderSkipped,
                        borderWidth: o.borderWidth,
                        datasetLabel: r.label,
                        label: n.chart.data.labels[e]
                    }, n._updateElementGeometry(t, e, a), t.pivot()
                }, _updateElementGeometry: function (t, e, a) {
                    var n = this, i = t._model, r = n._getValueScale(), o = r.getBasePixel(), s = r.isHorizontal(),
                        l = n._ruler || n.getRuler(), c = n.calculateBarValuePixels(n.index, e),
                        d = n.calculateBarIndexPixels(n.index, e, l);
                    i.horizontal = s, i.base = a ? o : c.base, i.x = s ? a ? o : c.head : d.center, i.y = s ? d.center : a ? o : c.head, i.height = s ? d.size : void 0, i.width = s ? void 0 : d.size
                }, _getStacks: function (t) {
                    var e, a, n = this, i = n.chart, r = n._getIndexScale(), o = r.options.stacked,
                        s = void 0 === t ? i.data.datasets.length : t + 1, l = [];
                    for (e = 0; e < s; ++e) a = i.getDatasetMeta(e), a.bar && i.isDatasetVisible(e) && (!1 === o || !0 === o && -1 === l.indexOf(a.stack) || void 0 === o && (void 0 === a.stack || -1 === l.indexOf(a.stack))) && l.push(a.stack);
                    return l
                }, getStackCount: function () {
                    return this._getStacks().length
                }, getStackIndex: function (t, e) {
                    var a = this._getStacks(t), n = void 0 !== e ? a.indexOf(e) : -1;
                    return -1 === n ? a.length - 1 : n
                }, getRuler: function () {
                    var t, e, a, n = this, i = n._getIndexScale(), r = n.getStackCount(), o = n.index,
                        s = i.isHorizontal(), l = s ? i.left : i.top, c = l + (s ? i.width : i.height), d = [];
                    for (t = 0, e = n.getMeta().data.length; t < e; ++t) d.push(i.getPixelForValue(null, t, o));
                    return a = Ut.isNullOrUndef(i.options.barThickness) ? Te(i, d) : -1, {
                        min: a,
                        pixels: d,
                        start: l,
                        end: c,
                        stackCount: r,
                        scale: i
                    }
                }, calculateBarValuePixels: function (t, e) {
                    var a, n, i, r, o, s, l = this, c = l.chart, d = l.getMeta(), u = l._getValueScale(),
                        h = u.isHorizontal(), f = c.data.datasets, p = +u.getRightValue(f[t].data[e]),
                        g = u.options.minBarLength, m = u.options.stacked, v = d.stack, b = 0;
                    if (m || void 0 === m && void 0 !== v) for (a = 0; a < t; ++a) n = c.getDatasetMeta(a), n.bar && n.stack === v && n.controller._getValueScaleId() === u.id && c.isDatasetVisible(a) && (i = +u.getRightValue(f[a].data[e]), (p < 0 && i < 0 || p >= 0 && i > 0) && (b += i));
                    return r = u.getPixelForValue(b), o = u.getPixelForValue(b + p), s = o - r, void 0 !== g && Math.abs(s) < g && (s = g, o = p >= 0 && !h || p < 0 && h ? r - g : r + g), {
                        size: s,
                        base: r,
                        head: o,
                        center: o + s / 2
                    }
                }, calculateBarIndexPixels: function (t, e, a) {
                    var n = this, i = a.scale.options, r = "flex" === i.barThickness ? Re(e, a, i) : Ie(e, a, i),
                        o = n.getStackIndex(t, n.getMeta().stack), s = r.start + r.chunk * o + r.chunk / 2,
                        l = Math.min(Ut.valueOrDefault(i.maxBarThickness, 1 / 0), r.chunk * r.ratio);
                    return {base: s - l / 2, head: s + l / 2, center: s, size: l}
                }, draw: function () {
                    var t = this, e = t.chart, a = t._getValueScale(), n = t.getMeta().data, i = t.getDataset(),
                        r = n.length, o = 0;
                    for (Ut.canvas.clipArea(e.ctx, e.chartArea); o < r; ++o) isNaN(a.getRightValue(i.data[o])) || n[o].draw();
                    Ut.canvas.unclipArea(e.ctx)
                }, _resolveElementOptions: function (t, e) {
                    var a, n, i, r = this, o = r.chart, s = o.data.datasets, l = s[r.index], c = t.custom || {},
                        d = o.options.elements.rectangle, u = {},
                        h = {chart: o, dataIndex: e, dataset: l, datasetIndex: r.index},
                        f = ["backgroundColor", "borderColor", "borderSkipped", "borderWidth"];
                    for (a = 0, n = f.length; a < n; ++a) i = f[a], u[i] = Pe([c[i], l[i], d[i]], h, e);
                    return u
                }
            }), Be = Ut.valueOrDefault, Le = Ut.options.resolve;
            $t._set("bubble", {
                hover: {mode: "single"},
                scales: {
                    xAxes: [{type: "linear", position: "bottom", id: "x-axis-0"}],
                    yAxes: [{type: "linear", position: "left", id: "y-axis-0"}]
                },
                tooltips: {
                    callbacks: {
                        title: function () {
                            return ""
                        }, label: function (t, e) {
                            var a = e.datasets[t.datasetIndex].label || "",
                                n = e.datasets[t.datasetIndex].data[t.index];
                            return a + ": (" + t.xLabel + ", " + t.yLabel + ", " + n.r + ")"
                        }
                    }
                }
            });
            var Ne = oe.extend({
                dataElementType: Ae.Point, update: function (t) {
                    var e = this, a = e.getMeta(), n = a.data;
                    Ut.each(n, function (a, n) {
                        e.updateElement(a, n, t)
                    })
                }, updateElement: function (t, e, a) {
                    var n = this, i = n.getMeta(), r = t.custom || {}, o = n.getScaleForId(i.xAxisID),
                        s = n.getScaleForId(i.yAxisID), l = n._resolveElementOptions(t, e), c = n.getDataset().data[e],
                        d = n.index,
                        u = a ? o.getPixelForDecimal(.5) : o.getPixelForValue("object" === typeof c ? c : NaN, e, d),
                        h = a ? s.getBasePixel() : s.getPixelForValue(c, e, d);
                    t._xScale = o, t._yScale = s, t._options = l, t._datasetIndex = d, t._index = e, t._model = {
                        backgroundColor: l.backgroundColor,
                        borderColor: l.borderColor,
                        borderWidth: l.borderWidth,
                        hitRadius: l.hitRadius,
                        pointStyle: l.pointStyle,
                        rotation: l.rotation,
                        radius: a ? 0 : l.radius,
                        skip: r.skip || isNaN(u) || isNaN(h),
                        x: u,
                        y: h
                    }, t.pivot()
                }, setHoverStyle: function (t) {
                    var e = t._model, a = t._options, n = Ut.getHoverColor;
                    t.$previousStyle = {
                        backgroundColor: e.backgroundColor,
                        borderColor: e.borderColor,
                        borderWidth: e.borderWidth,
                        radius: e.radius
                    }, e.backgroundColor = Be(a.hoverBackgroundColor, n(a.backgroundColor)), e.borderColor = Be(a.hoverBorderColor, n(a.borderColor)), e.borderWidth = Be(a.hoverBorderWidth, a.borderWidth), e.radius = a.radius + a.hoverRadius
                }, _resolveElementOptions: function (t, e) {
                    var a, n, i, r = this, o = r.chart, s = o.data.datasets, l = s[r.index], c = t.custom || {},
                        d = o.options.elements.point, u = l.data[e], h = {},
                        f = {chart: o, dataIndex: e, dataset: l, datasetIndex: r.index},
                        p = ["backgroundColor", "borderColor", "borderWidth", "hoverBackgroundColor", "hoverBorderColor", "hoverBorderWidth", "hoverRadius", "hitRadius", "pointStyle", "rotation"];
                    for (a = 0, n = p.length; a < n; ++a) i = p[a], h[i] = Le([c[i], l[i], d[i]], f, e);
                    return h.radius = Le([c.radius, u ? u.r : void 0, l.radius, d.radius], f, e), h
                }
            }), Ee = Ut.options.resolve, je = Ut.valueOrDefault;
            $t._set("doughnut", {
                animation: {animateRotate: !0, animateScale: !1},
                hover: {mode: "single"},
                legendCallback: function (t) {
                    var e = [];
                    e.push('<ul class="' + t.id + '-legend">');
                    var a = t.data, n = a.datasets, i = a.labels;
                    if (n.length) for (var r = 0; r < n[0].data.length; ++r) e.push('<li><span style="background-color:' + n[0].backgroundColor[r] + '"></span>'), i[r] && e.push(i[r]), e.push("</li>");
                    return e.push("</ul>"), e.join("")
                },
                legend: {
                    labels: {
                        generateLabels: function (t) {
                            var e = t.data;
                            return e.labels.length && e.datasets.length ? e.labels.map(function (a, n) {
                                var i = t.getDatasetMeta(0), r = e.datasets[0], o = i.data[n], s = o && o.custom || {},
                                    l = t.options.elements.arc,
                                    c = Ee([s.backgroundColor, r.backgroundColor, l.backgroundColor], void 0, n),
                                    d = Ee([s.borderColor, r.borderColor, l.borderColor], void 0, n),
                                    u = Ee([s.borderWidth, r.borderWidth, l.borderWidth], void 0, n);
                                return {
                                    text: a,
                                    fillStyle: c,
                                    strokeStyle: d,
                                    lineWidth: u,
                                    hidden: isNaN(r.data[n]) || i.data[n].hidden,
                                    index: n
                                }
                            }) : []
                        }
                    }, onClick: function (t, e) {
                        var a, n, i, r = e.index, o = this.chart;
                        for (a = 0, n = (o.data.datasets || []).length; a < n; ++a) i = o.getDatasetMeta(a), i.data[r] && (i.data[r].hidden = !i.data[r].hidden);
                        o.update()
                    }
                },
                cutoutPercentage: 50,
                rotation: -.5 * Math.PI,
                circumference: 2 * Math.PI,
                tooltips: {
                    callbacks: {
                        title: function () {
                            return ""
                        }, label: function (t, e) {
                            var a = e.labels[t.index], n = ": " + e.datasets[t.datasetIndex].data[t.index];
                            return Ut.isArray(a) ? (a = a.slice(), a[0] += n) : a += n, a
                        }
                    }
                }
            });
            var He = oe.extend({
                dataElementType: Ae.Arc, linkScales: Ut.noop, getRingIndex: function (t) {
                    for (var e = 0, a = 0; a < t; ++a) this.chart.isDatasetVisible(a) && ++e;
                    return e
                }, update: function (t) {
                    var e, a, n = this, i = n.chart, r = i.chartArea, o = i.options, s = r.right - r.left,
                        l = r.bottom - r.top, c = Math.min(s, l), d = {x: 0, y: 0}, u = n.getMeta(), h = u.data,
                        f = o.cutoutPercentage, p = o.circumference, g = n._getRingWeight(n.index);
                    if (p < 2 * Math.PI) {
                        var m = o.rotation % (2 * Math.PI);
                        m += 2 * Math.PI * (m >= Math.PI ? -1 : m < -Math.PI ? 1 : 0);
                        var v = m + p, b = {x: Math.cos(m), y: Math.sin(m)}, y = {x: Math.cos(v), y: Math.sin(v)},
                            x = m <= 0 && v >= 0 || m <= 2 * Math.PI && 2 * Math.PI <= v,
                            _ = m <= .5 * Math.PI && .5 * Math.PI <= v || m <= 2.5 * Math.PI && 2.5 * Math.PI <= v,
                            C = m <= -Math.PI && -Math.PI <= v || m <= Math.PI && Math.PI <= v,
                            k = m <= .5 * -Math.PI && .5 * -Math.PI <= v || m <= 1.5 * Math.PI && 1.5 * Math.PI <= v,
                            w = f / 100, A = {
                                x: C ? -1 : Math.min(b.x * (b.x < 0 ? 1 : w), y.x * (y.x < 0 ? 1 : w)),
                                y: k ? -1 : Math.min(b.y * (b.y < 0 ? 1 : w), y.y * (y.y < 0 ? 1 : w))
                            }, M = {
                                x: x ? 1 : Math.max(b.x * (b.x > 0 ? 1 : w), y.x * (y.x > 0 ? 1 : w)),
                                y: _ ? 1 : Math.max(b.y * (b.y > 0 ? 1 : w), y.y * (y.y > 0 ? 1 : w))
                            }, S = {width: .5 * (M.x - A.x), height: .5 * (M.y - A.y)};
                        c = Math.min(s / S.width, l / S.height), d = {x: -.5 * (M.x + A.x), y: -.5 * (M.y + A.y)}
                    }
                    for (e = 0, a = h.length; e < a; ++e) h[e]._options = n._resolveElementOptions(h[e], e);
                    for (i.borderWidth = n.getMaxBorderWidth(), i.outerRadius = Math.max((c - i.borderWidth) / 2, 0), i.innerRadius = Math.max(f ? i.outerRadius / 100 * f : 0, 0), i.radiusLength = (i.outerRadius - i.innerRadius) / (n._getVisibleDatasetWeightTotal() || 1), i.offsetX = d.x * i.outerRadius, i.offsetY = d.y * i.outerRadius, u.total = n.calculateTotal(), n.outerRadius = i.outerRadius - i.radiusLength * n._getRingWeightOffset(n.index), n.innerRadius = Math.max(n.outerRadius - i.radiusLength * g, 0), e = 0, a = h.length; e < a; ++e) n.updateElement(h[e], e, t)
                }, updateElement: function (t, e, a) {
                    var n = this, i = n.chart, r = i.chartArea, o = i.options, s = o.animation,
                        l = (r.left + r.right) / 2, c = (r.top + r.bottom) / 2, d = o.rotation, u = o.rotation,
                        h = n.getDataset(),
                        f = a && s.animateRotate ? 0 : t.hidden ? 0 : n.calculateCircumference(h.data[e]) * (o.circumference / (2 * Math.PI)),
                        p = a && s.animateScale ? 0 : n.innerRadius, g = a && s.animateScale ? 0 : n.outerRadius,
                        m = t._options || {};
                    Ut.extend(t, {
                        _datasetIndex: n.index,
                        _index: e,
                        _model: {
                            backgroundColor: m.backgroundColor,
                            borderColor: m.borderColor,
                            borderWidth: m.borderWidth,
                            borderAlign: m.borderAlign,
                            x: l + i.offsetX,
                            y: c + i.offsetY,
                            startAngle: d,
                            endAngle: u,
                            circumference: f,
                            outerRadius: g,
                            innerRadius: p,
                            label: Ut.valueAtIndexOrDefault(h.label, e, i.data.labels[e])
                        }
                    });
                    var v = t._model;
                    a && s.animateRotate || (v.startAngle = 0 === e ? o.rotation : n.getMeta().data[e - 1]._model.endAngle, v.endAngle = v.startAngle + v.circumference), t.pivot()
                }, calculateTotal: function () {
                    var t, e = this.getDataset(), a = this.getMeta(), n = 0;
                    return Ut.each(a.data, function (a, i) {
                        t = e.data[i], isNaN(t) || a.hidden || (n += Math.abs(t))
                    }), n
                }, calculateCircumference: function (t) {
                    var e = this.getMeta().total;
                    return e > 0 && !isNaN(t) ? 2 * Math.PI * (Math.abs(t) / e) : 0
                }, getMaxBorderWidth: function (t) {
                    var e, a, n, i, r, o, s, l, c = this, d = 0, u = c.chart;
                    if (!t) for (e = 0, a = u.data.datasets.length; e < a; ++e) if (u.isDatasetVisible(e)) {
                        n = u.getDatasetMeta(e), t = n.data, e !== c.index && (r = n.controller);
                        break
                    }
                    if (!t) return 0;
                    for (e = 0, a = t.length; e < a; ++e) i = t[e], o = r ? r._resolveElementOptions(i, e) : i._options, "inner" !== o.borderAlign && (s = o.borderWidth, l = o.hoverBorderWidth, d = s > d ? s : d, d = l > d ? l : d);
                    return d
                }, setHoverStyle: function (t) {
                    var e = t._model, a = t._options, n = Ut.getHoverColor;
                    t.$previousStyle = {
                        backgroundColor: e.backgroundColor,
                        borderColor: e.borderColor,
                        borderWidth: e.borderWidth
                    }, e.backgroundColor = je(a.hoverBackgroundColor, n(a.backgroundColor)), e.borderColor = je(a.hoverBorderColor, n(a.borderColor)), e.borderWidth = je(a.hoverBorderWidth, a.borderWidth)
                }, _resolveElementOptions: function (t, e) {
                    var a, n, i, r = this, o = r.chart, s = r.getDataset(), l = t.custom || {},
                        c = o.options.elements.arc, d = {},
                        u = {chart: o, dataIndex: e, dataset: s, datasetIndex: r.index},
                        h = ["backgroundColor", "borderColor", "borderWidth", "borderAlign", "hoverBackgroundColor", "hoverBorderColor", "hoverBorderWidth"];
                    for (a = 0, n = h.length; a < n; ++a) i = h[a], d[i] = Ee([l[i], s[i], c[i]], u, e);
                    return d
                }, _getRingWeightOffset: function (t) {
                    for (var e = 0, a = 0; a < t; ++a) this.chart.isDatasetVisible(a) && (e += this._getRingWeight(a));
                    return e
                }, _getRingWeight: function (t) {
                    return Math.max(je(this.chart.data.datasets[t].weight, 1), 0)
                }, _getVisibleDatasetWeightTotal: function () {
                    return this._getRingWeightOffset(this.chart.data.datasets.length)
                }
            });
            $t._set("horizontalBar", {
                hover: {mode: "index", axis: "y"},
                scales: {
                    xAxes: [{type: "linear", position: "bottom"}],
                    yAxes: [{
                        type: "category",
                        position: "left",
                        categoryPercentage: .8,
                        barPercentage: .9,
                        offset: !0,
                        gridLines: {offsetGridLines: !0}
                    }]
                },
                elements: {rectangle: {borderSkipped: "left"}},
                tooltips: {mode: "index", axis: "y"}
            });
            var $e = Fe.extend({
                _getValueScaleId: function () {
                    return this.getMeta().xAxisID
                }, _getIndexScaleId: function () {
                    return this.getMeta().yAxisID
                }
            }), ze = Ut.valueOrDefault, Ve = Ut.options.resolve, We = Ut.canvas._isPointInArea;

            function Ue(t, e) {
                return ze(t.showLine, e.showLines)
            }

            $t._set("line", {
                showLines: !0,
                spanGaps: !1,
                hover: {mode: "label"},
                scales: {xAxes: [{type: "category", id: "x-axis-0"}], yAxes: [{type: "linear", id: "y-axis-0"}]}
            });
            var qe = oe.extend({
                datasetElementType: Ae.Line, dataElementType: Ae.Point, update: function (t) {
                    var e, a, n = this, i = n.getMeta(), r = i.dataset, o = i.data || [],
                        s = n.getScaleForId(i.yAxisID), l = n.getDataset(), c = Ue(l, n.chart.options);
                    for (c && (void 0 !== l.tension && void 0 === l.lineTension && (l.lineTension = l.tension), r._scale = s, r._datasetIndex = n.index, r._children = o, r._model = n._resolveLineOptions(r), r.pivot()), e = 0, a = o.length; e < a; ++e) n.updateElement(o[e], e, t);
                    for (c && 0 !== r._model.tension && n.updateBezierControlPoints(), e = 0, a = o.length; e < a; ++e) o[e].pivot()
                }, updateElement: function (t, e, a) {
                    var n, i, r = this, o = r.getMeta(), s = t.custom || {}, l = r.getDataset(), c = r.index,
                        d = l.data[e], u = r.getScaleForId(o.yAxisID), h = r.getScaleForId(o.xAxisID),
                        f = o.dataset._model, p = r._resolvePointOptions(t, e);
                    n = h.getPixelForValue("object" === typeof d ? d : NaN, e, c), i = a ? u.getBasePixel() : r.calculatePointY(d, e, c), t._xScale = h, t._yScale = u, t._options = p, t._datasetIndex = c, t._index = e, t._model = {
                        x: n,
                        y: i,
                        skip: s.skip || isNaN(n) || isNaN(i),
                        radius: p.radius,
                        pointStyle: p.pointStyle,
                        rotation: p.rotation,
                        backgroundColor: p.backgroundColor,
                        borderColor: p.borderColor,
                        borderWidth: p.borderWidth,
                        tension: ze(s.tension, f ? f.tension : 0),
                        steppedLine: !!f && f.steppedLine,
                        hitRadius: p.hitRadius
                    }
                }, _resolvePointOptions: function (t, e) {
                    var a, n, i, r = this, o = r.chart, s = o.data.datasets[r.index], l = t.custom || {},
                        c = o.options.elements.point, d = {},
                        u = {chart: o, dataIndex: e, dataset: s, datasetIndex: r.index}, h = {
                            backgroundColor: "pointBackgroundColor",
                            borderColor: "pointBorderColor",
                            borderWidth: "pointBorderWidth",
                            hitRadius: "pointHitRadius",
                            hoverBackgroundColor: "pointHoverBackgroundColor",
                            hoverBorderColor: "pointHoverBorderColor",
                            hoverBorderWidth: "pointHoverBorderWidth",
                            hoverRadius: "pointHoverRadius",
                            pointStyle: "pointStyle",
                            radius: "pointRadius",
                            rotation: "pointRotation"
                        }, f = Object.keys(h);
                    for (a = 0, n = f.length; a < n; ++a) i = f[a], d[i] = Ve([l[i], s[h[i]], s[i], c[i]], u, e);
                    return d
                }, _resolveLineOptions: function (t) {
                    var e, a, n, i = this, r = i.chart, o = r.data.datasets[i.index], s = t.custom || {}, l = r.options,
                        c = l.elements.line, d = {},
                        u = ["backgroundColor", "borderWidth", "borderColor", "borderCapStyle", "borderDash", "borderDashOffset", "borderJoinStyle", "fill", "cubicInterpolationMode"];
                    for (e = 0, a = u.length; e < a; ++e) n = u[e], d[n] = Ve([s[n], o[n], c[n]]);
                    return d.spanGaps = ze(o.spanGaps, l.spanGaps), d.tension = ze(o.lineTension, c.tension), d.steppedLine = Ve([s.steppedLine, o.steppedLine, c.stepped]), d
                }, calculatePointY: function (t, e, a) {
                    var n, i, r, o = this, s = o.chart, l = o.getMeta(), c = o.getScaleForId(l.yAxisID), d = 0, u = 0;
                    if (c.options.stacked) {
                        for (n = 0; n < a; n++) if (i = s.data.datasets[n], r = s.getDatasetMeta(n), "line" === r.type && r.yAxisID === c.id && s.isDatasetVisible(n)) {
                            var h = Number(c.getRightValue(i.data[e]));
                            h < 0 ? u += h || 0 : d += h || 0
                        }
                        var f = Number(c.getRightValue(t));
                        return f < 0 ? c.getPixelForValue(u + f) : c.getPixelForValue(d + f)
                    }
                    return c.getPixelForValue(t)
                }, updateBezierControlPoints: function () {
                    var t, e, a, n, i = this, r = i.chart, o = i.getMeta(), s = o.dataset._model, l = r.chartArea,
                        c = o.data || [];

                    function d(t, e, a) {
                        return Math.max(Math.min(t, a), e)
                    }

                    if (s.spanGaps && (c = c.filter(function (t) {
                            return !t._model.skip
                        })), "monotone" === s.cubicInterpolationMode) Ut.splineCurveMonotone(c); else for (t = 0, e = c.length; t < e; ++t) a = c[t]._model, n = Ut.splineCurve(Ut.previousItem(c, t)._model, a, Ut.nextItem(c, t)._model, s.tension), a.controlPointPreviousX = n.previous.x, a.controlPointPreviousY = n.previous.y, a.controlPointNextX = n.next.x, a.controlPointNextY = n.next.y;
                    if (r.options.elements.line.capBezierPoints) for (t = 0, e = c.length; t < e; ++t) a = c[t]._model, We(a, l) && (t > 0 && We(c[t - 1]._model, l) && (a.controlPointPreviousX = d(a.controlPointPreviousX, l.left, l.right), a.controlPointPreviousY = d(a.controlPointPreviousY, l.top, l.bottom)), t < c.length - 1 && We(c[t + 1]._model, l) && (a.controlPointNextX = d(a.controlPointNextX, l.left, l.right), a.controlPointNextY = d(a.controlPointNextY, l.top, l.bottom)))
                }, draw: function () {
                    var t, e = this, a = e.chart, n = e.getMeta(), i = n.data || [], r = a.chartArea, o = i.length,
                        s = 0;
                    for (Ue(e.getDataset(), a.options) && (t = (n.dataset._model.borderWidth || 0) / 2, Ut.canvas.clipArea(a.ctx, {
                        left: r.left,
                        right: r.right,
                        top: r.top - t,
                        bottom: r.bottom + t
                    }), n.dataset.draw(), Ut.canvas.unclipArea(a.ctx)); s < o; ++s) i[s].draw(r)
                }, setHoverStyle: function (t) {
                    var e = t._model, a = t._options, n = Ut.getHoverColor;
                    t.$previousStyle = {
                        backgroundColor: e.backgroundColor,
                        borderColor: e.borderColor,
                        borderWidth: e.borderWidth,
                        radius: e.radius
                    }, e.backgroundColor = ze(a.hoverBackgroundColor, n(a.backgroundColor)), e.borderColor = ze(a.hoverBorderColor, n(a.borderColor)), e.borderWidth = ze(a.hoverBorderWidth, a.borderWidth), e.radius = ze(a.hoverRadius, a.radius)
                }
            }), Ye = Ut.options.resolve;
            $t._set("polarArea", {
                scale: {
                    type: "radialLinear",
                    angleLines: {display: !1},
                    gridLines: {circular: !0},
                    pointLabels: {display: !1},
                    ticks: {beginAtZero: !0}
                },
                animation: {animateRotate: !0, animateScale: !0},
                startAngle: -.5 * Math.PI,
                legendCallback: function (t) {
                    var e = [];
                    e.push('<ul class="' + t.id + '-legend">');
                    var a = t.data, n = a.datasets, i = a.labels;
                    if (n.length) for (var r = 0; r < n[0].data.length; ++r) e.push('<li><span style="background-color:' + n[0].backgroundColor[r] + '"></span>'), i[r] && e.push(i[r]), e.push("</li>");
                    return e.push("</ul>"), e.join("")
                },
                legend: {
                    labels: {
                        generateLabels: function (t) {
                            var e = t.data;
                            return e.labels.length && e.datasets.length ? e.labels.map(function (a, n) {
                                var i = t.getDatasetMeta(0), r = e.datasets[0], o = i.data[n], s = o.custom || {},
                                    l = t.options.elements.arc,
                                    c = Ye([s.backgroundColor, r.backgroundColor, l.backgroundColor], void 0, n),
                                    d = Ye([s.borderColor, r.borderColor, l.borderColor], void 0, n),
                                    u = Ye([s.borderWidth, r.borderWidth, l.borderWidth], void 0, n);
                                return {
                                    text: a,
                                    fillStyle: c,
                                    strokeStyle: d,
                                    lineWidth: u,
                                    hidden: isNaN(r.data[n]) || i.data[n].hidden,
                                    index: n
                                }
                            }) : []
                        }
                    }, onClick: function (t, e) {
                        var a, n, i, r = e.index, o = this.chart;
                        for (a = 0, n = (o.data.datasets || []).length; a < n; ++a) i = o.getDatasetMeta(a), i.data[r].hidden = !i.data[r].hidden;
                        o.update()
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function () {
                            return ""
                        }, label: function (t, e) {
                            return e.labels[t.index] + ": " + t.yLabel
                        }
                    }
                }
            });
            var Ge = oe.extend({
                dataElementType: Ae.Arc, linkScales: Ut.noop, update: function (t) {
                    var e, a, n, i = this, r = i.getDataset(), o = i.getMeta(), s = i.chart.options.startAngle || 0,
                        l = i._starts = [], c = i._angles = [], d = o.data;
                    for (i._updateRadius(), o.count = i.countVisibleElements(), e = 0, a = r.data.length; e < a; e++) l[e] = s, n = i._computeAngle(e), c[e] = n, s += n;
                    for (e = 0, a = d.length; e < a; ++e) d[e]._options = i._resolveElementOptions(d[e], e), i.updateElement(d[e], e, t)
                }, _updateRadius: function () {
                    var t = this, e = t.chart, a = e.chartArea, n = e.options,
                        i = Math.min(a.right - a.left, a.bottom - a.top);
                    e.outerRadius = Math.max(i / 2, 0), e.innerRadius = Math.max(n.cutoutPercentage ? e.outerRadius / 100 * n.cutoutPercentage : 1, 0), e.radiusLength = (e.outerRadius - e.innerRadius) / e.getVisibleDatasetCount(), t.outerRadius = e.outerRadius - e.radiusLength * t.index, t.innerRadius = t.outerRadius - e.radiusLength
                }, updateElement: function (t, e, a) {
                    var n = this, i = n.chart, r = n.getDataset(), o = i.options, s = o.animation, l = i.scale,
                        c = i.data.labels, d = l.xCenter, u = l.yCenter, h = o.startAngle,
                        f = t.hidden ? 0 : l.getDistanceFromCenterForValue(r.data[e]), p = n._starts[e],
                        g = p + (t.hidden ? 0 : n._angles[e]),
                        m = s.animateScale ? 0 : l.getDistanceFromCenterForValue(r.data[e]), v = t._options || {};
                    Ut.extend(t, {
                        _datasetIndex: n.index,
                        _index: e,
                        _scale: l,
                        _model: {
                            backgroundColor: v.backgroundColor,
                            borderColor: v.borderColor,
                            borderWidth: v.borderWidth,
                            borderAlign: v.borderAlign,
                            x: d,
                            y: u,
                            innerRadius: 0,
                            outerRadius: a ? m : f,
                            startAngle: a && s.animateRotate ? h : p,
                            endAngle: a && s.animateRotate ? h : g,
                            label: Ut.valueAtIndexOrDefault(c, e, c[e])
                        }
                    }), t.pivot()
                }, countVisibleElements: function () {
                    var t = this.getDataset(), e = this.getMeta(), a = 0;
                    return Ut.each(e.data, function (e, n) {
                        isNaN(t.data[n]) || e.hidden || a++
                    }), a
                }, setHoverStyle: function (t) {
                    var e = t._model, a = t._options, n = Ut.getHoverColor, i = Ut.valueOrDefault;
                    t.$previousStyle = {
                        backgroundColor: e.backgroundColor,
                        borderColor: e.borderColor,
                        borderWidth: e.borderWidth
                    }, e.backgroundColor = i(a.hoverBackgroundColor, n(a.backgroundColor)), e.borderColor = i(a.hoverBorderColor, n(a.borderColor)), e.borderWidth = i(a.hoverBorderWidth, a.borderWidth)
                }, _resolveElementOptions: function (t, e) {
                    var a, n, i, r = this, o = r.chart, s = r.getDataset(), l = t.custom || {},
                        c = o.options.elements.arc, d = {},
                        u = {chart: o, dataIndex: e, dataset: s, datasetIndex: r.index},
                        h = ["backgroundColor", "borderColor", "borderWidth", "borderAlign", "hoverBackgroundColor", "hoverBorderColor", "hoverBorderWidth"];
                    for (a = 0, n = h.length; a < n; ++a) i = h[a], d[i] = Ye([l[i], s[i], c[i]], u, e);
                    return d
                }, _computeAngle: function (t) {
                    var e = this, a = this.getMeta().count, n = e.getDataset(), i = e.getMeta();
                    if (isNaN(n.data[t]) || i.data[t].hidden) return 0;
                    var r = {chart: e.chart, dataIndex: t, dataset: n, datasetIndex: e.index};
                    return Ye([e.chart.options.elements.arc.angle, 2 * Math.PI / a], r, t)
                }
            });
            $t._set("pie", Ut.clone($t.doughnut)), $t._set("pie", {cutoutPercentage: 0});
            var Ke = He, Xe = Ut.valueOrDefault, Je = Ut.options.resolve;
            $t._set("radar", {scale: {type: "radialLinear"}, elements: {line: {tension: 0}}});
            var Ze = oe.extend({
                datasetElementType: Ae.Line,
                dataElementType: Ae.Point,
                linkScales: Ut.noop,
                update: function (t) {
                    var e, a, n = this, i = n.getMeta(), r = i.dataset, o = i.data || [], s = n.chart.scale,
                        l = n.getDataset();
                    for (void 0 !== l.tension && void 0 === l.lineTension && (l.lineTension = l.tension), r._scale = s, r._datasetIndex = n.index, r._children = o, r._loop = !0, r._model = n._resolveLineOptions(r), r.pivot(), e = 0, a = o.length; e < a; ++e) n.updateElement(o[e], e, t);
                    for (n.updateBezierControlPoints(), e = 0, a = o.length; e < a; ++e) o[e].pivot()
                },
                updateElement: function (t, e, a) {
                    var n = this, i = t.custom || {}, r = n.getDataset(), o = n.chart.scale,
                        s = o.getPointPositionForValue(e, r.data[e]), l = n._resolvePointOptions(t, e),
                        c = n.getMeta().dataset._model, d = a ? o.xCenter : s.x, u = a ? o.yCenter : s.y;
                    t._scale = o, t._options = l, t._datasetIndex = n.index, t._index = e, t._model = {
                        x: d,
                        y: u,
                        skip: i.skip || isNaN(d) || isNaN(u),
                        radius: l.radius,
                        pointStyle: l.pointStyle,
                        rotation: l.rotation,
                        backgroundColor: l.backgroundColor,
                        borderColor: l.borderColor,
                        borderWidth: l.borderWidth,
                        tension: Xe(i.tension, c ? c.tension : 0),
                        hitRadius: l.hitRadius
                    }
                },
                _resolvePointOptions: function (t, e) {
                    var a, n, i, r = this, o = r.chart, s = o.data.datasets[r.index], l = t.custom || {},
                        c = o.options.elements.point, d = {},
                        u = {chart: o, dataIndex: e, dataset: s, datasetIndex: r.index}, h = {
                            backgroundColor: "pointBackgroundColor",
                            borderColor: "pointBorderColor",
                            borderWidth: "pointBorderWidth",
                            hitRadius: "pointHitRadius",
                            hoverBackgroundColor: "pointHoverBackgroundColor",
                            hoverBorderColor: "pointHoverBorderColor",
                            hoverBorderWidth: "pointHoverBorderWidth",
                            hoverRadius: "pointHoverRadius",
                            pointStyle: "pointStyle",
                            radius: "pointRadius",
                            rotation: "pointRotation"
                        }, f = Object.keys(h);
                    for (a = 0, n = f.length; a < n; ++a) i = f[a], d[i] = Je([l[i], s[h[i]], s[i], c[i]], u, e);
                    return d
                },
                _resolveLineOptions: function (t) {
                    var e, a, n, i = this, r = i.chart, o = r.data.datasets[i.index], s = t.custom || {},
                        l = r.options.elements.line, c = {},
                        d = ["backgroundColor", "borderWidth", "borderColor", "borderCapStyle", "borderDash", "borderDashOffset", "borderJoinStyle", "fill"];
                    for (e = 0, a = d.length; e < a; ++e) n = d[e], c[n] = Je([s[n], o[n], l[n]]);
                    return c.tension = Xe(o.lineTension, l.tension), c
                },
                updateBezierControlPoints: function () {
                    var t, e, a, n, i = this, r = i.getMeta(), o = i.chart.chartArea, s = r.data || [];

                    function l(t, e, a) {
                        return Math.max(Math.min(t, a), e)
                    }

                    for (t = 0, e = s.length; t < e; ++t) a = s[t]._model, n = Ut.splineCurve(Ut.previousItem(s, t, !0)._model, a, Ut.nextItem(s, t, !0)._model, a.tension), a.controlPointPreviousX = l(n.previous.x, o.left, o.right), a.controlPointPreviousY = l(n.previous.y, o.top, o.bottom), a.controlPointNextX = l(n.next.x, o.left, o.right), a.controlPointNextY = l(n.next.y, o.top, o.bottom)
                },
                setHoverStyle: function (t) {
                    var e = t._model, a = t._options, n = Ut.getHoverColor;
                    t.$previousStyle = {
                        backgroundColor: e.backgroundColor,
                        borderColor: e.borderColor,
                        borderWidth: e.borderWidth,
                        radius: e.radius
                    }, e.backgroundColor = Xe(a.hoverBackgroundColor, n(a.backgroundColor)), e.borderColor = Xe(a.hoverBorderColor, n(a.borderColor)), e.borderWidth = Xe(a.hoverBorderWidth, a.borderWidth), e.radius = Xe(a.hoverRadius, a.radius)
                }
            });
            $t._set("scatter", {
                hover: {mode: "single"},
                scales: {
                    xAxes: [{id: "x-axis-1", type: "linear", position: "bottom"}],
                    yAxes: [{id: "y-axis-1", type: "linear", position: "left"}]
                },
                showLines: !1,
                tooltips: {
                    callbacks: {
                        title: function () {
                            return ""
                        }, label: function (t) {
                            return "(" + t.xLabel + ", " + t.yLabel + ")"
                        }
                    }
                }
            });
            var Qe = qe, ta = {
                bar: Fe,
                bubble: Ne,
                doughnut: He,
                horizontalBar: $e,
                line: qe,
                polarArea: Ge,
                pie: Ke,
                radar: Ze,
                scatter: Qe
            };

            function ea(t, e) {
                return t.native ? {x: t.x, y: t.y} : Ut.getRelativePosition(t, e)
            }

            function aa(t, e) {
                var a, n, i, r, o, s = t.data.datasets;
                for (n = 0, r = s.length; n < r; ++n) if (t.isDatasetVisible(n)) for (a = t.getDatasetMeta(n), i = 0, o = a.data.length; i < o; ++i) {
                    var l = a.data[i];
                    l._view.skip || e(l)
                }
            }

            function na(t, e) {
                var a = [];
                return aa(t, function (t) {
                    t.inRange(e.x, e.y) && a.push(t)
                }), a
            }

            function ia(t, e, a, n) {
                var i = Number.POSITIVE_INFINITY, r = [];
                return aa(t, function (t) {
                    if (!a || t.inRange(e.x, e.y)) {
                        var o = t.getCenterPoint(), s = n(e, o);
                        s < i ? (r = [t], i = s) : s === i && r.push(t)
                    }
                }), r
            }

            function ra(t) {
                var e = -1 !== t.indexOf("x"), a = -1 !== t.indexOf("y");
                return function (t, n) {
                    var i = e ? Math.abs(t.x - n.x) : 0, r = a ? Math.abs(t.y - n.y) : 0;
                    return Math.sqrt(Math.pow(i, 2) + Math.pow(r, 2))
                }
            }

            function oa(t, e, a) {
                var n = ea(e, t);
                a.axis = a.axis || "x";
                var i = ra(a.axis), r = a.intersect ? na(t, n) : ia(t, n, !1, i), o = [];
                return r.length ? (t.data.datasets.forEach(function (e, a) {
                    if (t.isDatasetVisible(a)) {
                        var n = t.getDatasetMeta(a), i = n.data[r[0]._index];
                        i && !i._view.skip && o.push(i)
                    }
                }), o) : []
            }

            var sa = {
                modes: {
                    single: function (t, e) {
                        var a = ea(e, t), n = [];
                        return aa(t, function (t) {
                            if (t.inRange(a.x, a.y)) return n.push(t), n
                        }), n.slice(0, 1)
                    }, label: oa, index: oa, dataset: function (t, e, a) {
                        var n = ea(e, t);
                        a.axis = a.axis || "xy";
                        var i = ra(a.axis), r = a.intersect ? na(t, n) : ia(t, n, !1, i);
                        return r.length > 0 && (r = t.getDatasetMeta(r[0]._datasetIndex).data), r
                    }, "x-axis": function (t, e) {
                        return oa(t, e, {intersect: !1})
                    }, point: function (t, e) {
                        var a = ea(e, t);
                        return na(t, a)
                    }, nearest: function (t, e, a) {
                        var n = ea(e, t);
                        a.axis = a.axis || "xy";
                        var i = ra(a.axis);
                        return ia(t, n, a.intersect, i)
                    }, x: function (t, e, a) {
                        var n = ea(e, t), i = [], r = !1;
                        return aa(t, function (t) {
                            t.inXRange(n.x) && i.push(t), t.inRange(n.x, n.y) && (r = !0)
                        }), a.intersect && !r && (i = []), i
                    }, y: function (t, e, a) {
                        var n = ea(e, t), i = [], r = !1;
                        return aa(t, function (t) {
                            t.inYRange(n.y) && i.push(t), t.inRange(n.x, n.y) && (r = !0)
                        }), a.intersect && !r && (i = []), i
                    }
                }
            };

            function la(t, e) {
                return Ut.where(t, function (t) {
                    return t.position === e
                })
            }

            function ca(t, e) {
                t.forEach(function (t, e) {
                    return t._tmpIndex_ = e, t
                }), t.sort(function (t, a) {
                    var n = e ? a : t, i = e ? t : a;
                    return n.weight === i.weight ? n._tmpIndex_ - i._tmpIndex_ : n.weight - i.weight
                }), t.forEach(function (t) {
                    delete t._tmpIndex_
                })
            }

            function da(t) {
                var e = 0, a = 0, n = 0, i = 0;
                return Ut.each(t, function (t) {
                    if (t.getPadding) {
                        var r = t.getPadding();
                        e = Math.max(e, r.top), a = Math.max(a, r.left), n = Math.max(n, r.bottom), i = Math.max(i, r.right)
                    }
                }), {top: e, left: a, bottom: n, right: i}
            }

            function ua(t, e) {
                Ut.each(t, function (t) {
                    e[t.position] += t.isHorizontal() ? t.height : t.width
                })
            }

            $t._set("global", {layout: {padding: {top: 0, right: 0, bottom: 0, left: 0}}});
            var ha = {
                    defaults: {}, addBox: function (t, e) {
                        t.boxes || (t.boxes = []), e.fullWidth = e.fullWidth || !1, e.position = e.position || "top", e.weight = e.weight || 0, t.boxes.push(e)
                    }, removeBox: function (t, e) {
                        var a = t.boxes ? t.boxes.indexOf(e) : -1;
                        -1 !== a && t.boxes.splice(a, 1)
                    }, configure: function (t, e, a) {
                        for (var n, i = ["fullWidth", "position", "weight"], r = i.length, o = 0; o < r; ++o) n = i[o], a.hasOwnProperty(n) && (e[n] = a[n])
                    }, update: function (t, e, a) {
                        if (t) {
                            var n = t.options.layout || {}, i = Ut.options.toPadding(n.padding), r = i.left, o = i.right,
                                s = i.top, l = i.bottom, c = la(t.boxes, "left"), d = la(t.boxes, "right"),
                                u = la(t.boxes, "top"), h = la(t.boxes, "bottom"), f = la(t.boxes, "chartArea");
                            ca(c, !0), ca(d, !1), ca(u, !0), ca(h, !1);
                            var p, g = c.concat(d), m = u.concat(h), v = g.concat(m), b = e - r - o, y = a - s - l,
                                x = b / 2, _ = (e - x) / g.length, C = b, k = y, w = {top: s, left: r, bottom: l, right: o},
                                A = [];
                            Ut.each(v, I), p = da(v), Ut.each(g, R), ua(g, w), Ut.each(m, R), ua(m, w), Ut.each(g, F), w = {
                                top: s,
                                left: r,
                                bottom: l,
                                right: o
                            }, ua(v, w);
                            var M = Math.max(p.left - w.left, 0);
                            w.left += M, w.right += Math.max(p.right - w.right, 0);
                            var S = Math.max(p.top - w.top, 0);
                            w.top += S, w.bottom += Math.max(p.bottom - w.bottom, 0);
                            var D = a - w.top - w.bottom, O = e - w.left - w.right;
                            O === C && D === k || (Ut.each(g, function (t) {
                                t.height = D
                            }), Ut.each(m, function (t) {
                                t.fullWidth || (t.width = O)
                            }), k = D, C = O);
                            var P = r + M, T = s + S;
                            Ut.each(c.concat(u), B), P += C, T += k, Ut.each(d, B), Ut.each(h, B), t.chartArea = {
                                left: w.left,
                                top: w.top,
                                right: w.left + C,
                                bottom: w.top + k
                            }, Ut.each(f, function (e) {
                                e.left = t.chartArea.left, e.top = t.chartArea.top, e.right = t.chartArea.right, e.bottom = t.chartArea.bottom, e.update(C, k)
                            })
                        }

                        function I(t) {
                            var e, a = t.isHorizontal();
                            a ? (e = t.update(t.fullWidth ? b : C, y / 2), k -= e.height) : (e = t.update(_, k), C -= e.width), A.push({
                                horizontal: a,
                                width: e.width,
                                box: t
                            })
                        }

                        function R(t) {
                            var e = Ut.findNextWhere(A, function (e) {
                                return e.box === t
                            });
                            if (e) if (e.horizontal) {
                                var a = {
                                    left: Math.max(w.left, p.left),
                                    right: Math.max(w.right, p.right),
                                    top: 0,
                                    bottom: 0
                                };
                                t.update(t.fullWidth ? b : C, y / 2, a)
                            } else t.update(e.width, k)
                        }

                        function F(t) {
                            var e = Ut.findNextWhere(A, function (e) {
                                return e.box === t
                            }), a = {left: 0, right: 0, top: w.top, bottom: w.bottom};
                            e && t.update(e.width, k, a)
                        }

                        function B(t) {
                            t.isHorizontal() ? (t.left = t.fullWidth ? r : w.left, t.right = t.fullWidth ? e - o : w.left + C, t.top = T, t.bottom = T + t.height, T = t.bottom) : (t.left = P, t.right = P + t.width, t.top = w.top, t.bottom = w.top + k, P = t.right)
                        }
                    }
                }, fa = {
                    acquireContext: function (t) {
                        return t && t.canvas && (t = t.canvas), t && t.getContext("2d") || null
                    }
                },
                pa = "/*\n * DOM element rendering detection\n * https://davidwalsh.name/detect-node-insertion\n */\n@keyframes chartjs-render-animation {\n\tfrom { opacity: 0.99; }\n\tto { opacity: 1; }\n}\n\n.chartjs-render-monitor {\n\tanimation: chartjs-render-animation 0.001s;\n}\n\n/*\n * DOM element resizing detection\n * https://github.com/marcj/css-element-queries\n */\n.chartjs-size-monitor,\n.chartjs-size-monitor-expand,\n.chartjs-size-monitor-shrink {\n\tposition: absolute;\n\tdirection: ltr;\n\tleft: 0;\n\ttop: 0;\n\tright: 0;\n\tbottom: 0;\n\toverflow: hidden;\n\tpointer-events: none;\n\tvisibility: hidden;\n\tz-index: -1;\n}\n\n.chartjs-size-monitor-expand > div {\n\tposition: absolute;\n\twidth: 1000000px;\n\theight: 1000000px;\n\tleft: 0;\n\ttop: 0;\n}\n\n.chartjs-size-monitor-shrink > div {\n\tposition: absolute;\n\twidth: 200%;\n\theight: 200%;\n\tleft: 0;\n\ttop: 0;\n}\n",
                ga = Object.freeze({default: pa});

            function ma(t) {
                return t && t.default || t
            }

            var va = ma(ga), ba = "$chartjs", ya = "chartjs-", xa = ya + "size-monitor", _a = ya + "render-monitor",
                Ca = ya + "render-animation", ka = ["animationstart", "webkitAnimationStart"], wa = {
                    touchstart: "mousedown",
                    touchmove: "mousemove",
                    touchend: "mouseup",
                    pointerenter: "mouseenter",
                    pointerdown: "mousedown",
                    pointermove: "mousemove",
                    pointerup: "mouseup",
                    pointerleave: "mouseout",
                    pointerout: "mouseout"
                };

            function Aa(t, e) {
                var a = Ut.getStyle(t, e), n = a && a.match(/^(\d+)(\.\d+)?px$/);
                return n ? Number(n[1]) : void 0
            }

            function Ma(t, e) {
                var a = t.style, n = t.getAttribute("height"), i = t.getAttribute("width");
                if (t[ba] = {
                        initial: {
                            height: n,
                            width: i,
                            style: {display: a.display, height: a.height, width: a.width}
                        }
                    }, a.display = a.display || "block", null === i || "" === i) {
                    var r = Aa(t, "width");
                    void 0 !== r && (t.width = r)
                }
                if (null === n || "" === n) if ("" === t.style.height) t.height = t.width / (e.options.aspectRatio || 2); else {
                    var o = Aa(t, "height");
                    void 0 !== r && (t.height = o)
                }
                return t
            }

            var Sa = function () {
                var t = !1;
                try {
                    var e = Object.defineProperty({}, "passive", {
                        get: function () {
                            t = !0
                        }
                    });
                    window.addEventListener("e", null, e)
                } catch (a) {
                }
                return t
            }(), Da = !!Sa && {passive: !0};

            function Oa(t, e, a) {
                t.addEventListener(e, a, Da)
            }

            function Pa(t, e, a) {
                t.removeEventListener(e, a, Da)
            }

            function Ta(t, e, a, n, i) {
                return {type: t, chart: e, native: i || null, x: void 0 !== a ? a : null, y: void 0 !== n ? n : null}
            }

            function Ia(t, e) {
                var a = wa[t.type] || t.type, n = Ut.getRelativePosition(t, e);
                return Ta(a, e, n.x, n.y, t)
            }

            function Ra(t, e) {
                var a = !1, n = [];
                return function () {
                    n = Array.prototype.slice.call(arguments), e = e || this, a || (a = !0, Ut.requestAnimFrame.call(window, function () {
                        a = !1, t.apply(e, n)
                    }))
                }
            }

            function Fa(t) {
                var e = document.createElement("div");
                return e.className = t || "", e
            }

            function Ba(t) {
                var e = 1e6, a = Fa(xa), n = Fa(xa + "-expand"), i = Fa(xa + "-shrink");
                n.appendChild(Fa()), i.appendChild(Fa()), a.appendChild(n), a.appendChild(i), a._reset = function () {
                    n.scrollLeft = e, n.scrollTop = e, i.scrollLeft = e, i.scrollTop = e
                };
                var r = function () {
                    a._reset(), t()
                };
                return Oa(n, "scroll", r.bind(n, "expand")), Oa(i, "scroll", r.bind(i, "shrink")), a
            }

            function La(t, e) {
                var a = t[ba] || (t[ba] = {}), n = a.renderProxy = function (t) {
                    t.animationName === Ca && e()
                };
                Ut.each(ka, function (e) {
                    Oa(t, e, n)
                }), a.reflow = !!t.offsetParent, t.classList.add(_a)
            }

            function Na(t) {
                var e = t[ba] || {}, a = e.renderProxy;
                a && (Ut.each(ka, function (e) {
                    Pa(t, e, a)
                }), delete e.renderProxy), t.classList.remove(_a)
            }

            function Ea(t, e, a) {
                var n = t[ba] || (t[ba] = {}), i = n.resizer = Ba(Ra(function () {
                    if (n.resizer) {
                        var i = a.options.maintainAspectRatio && t.parentNode, r = i ? i.clientWidth : 0;
                        e(Ta("resize", a)), i && i.clientWidth < r && a.canvas && e(Ta("resize", a))
                    }
                }));
                La(t, function () {
                    if (n.resizer) {
                        var e = t.parentNode;
                        e && e !== i.parentNode && e.insertBefore(i, e.firstChild), i._reset()
                    }
                })
            }

            function ja(t) {
                var e = t[ba] || {}, a = e.resizer;
                delete e.resizer, Na(t), a && a.parentNode && a.parentNode.removeChild(a)
            }

            function Ha(t, e) {
                var a = t._style || document.createElement("style");
                t._style || (t._style = a, e = "/* Chart.js */\n" + e, a.setAttribute("type", "text/css"), document.getElementsByTagName("head")[0].appendChild(a)), a.appendChild(document.createTextNode(e))
            }

            var $a = {
                disableCSSInjection: !1,
                _enabled: "undefined" !== typeof window && "undefined" !== typeof document,
                _ensureLoaded: function () {
                    this._loaded || (this._loaded = !0, this.disableCSSInjection || Ha(this, va))
                },
                acquireContext: function (t, e) {
                    "string" === typeof t ? t = document.getElementById(t) : t.length && (t = t[0]), t && t.canvas && (t = t.canvas);
                    var a = t && t.getContext && t.getContext("2d");
                    return this._ensureLoaded(), a && a.canvas === t ? (Ma(t, e), a) : null
                },
                releaseContext: function (t) {
                    var e = t.canvas;
                    if (e[ba]) {
                        var a = e[ba].initial;
                        ["height", "width"].forEach(function (t) {
                            var n = a[t];
                            Ut.isNullOrUndef(n) ? e.removeAttribute(t) : e.setAttribute(t, n)
                        }), Ut.each(a.style || {}, function (t, a) {
                            e.style[a] = t
                        }), e.width = e.width, delete e[ba]
                    }
                },
                addEventListener: function (t, e, a) {
                    var n = t.canvas;
                    if ("resize" !== e) {
                        var i = a[ba] || (a[ba] = {}), r = i.proxies || (i.proxies = {}),
                            o = r[t.id + "_" + e] = function (e) {
                                a(Ia(e, t))
                            };
                        Oa(n, e, o)
                    } else Ea(n, a, t)
                },
                removeEventListener: function (t, e, a) {
                    var n = t.canvas;
                    if ("resize" !== e) {
                        var i = a[ba] || {}, r = i.proxies || {}, o = r[t.id + "_" + e];
                        o && Pa(n, e, o)
                    } else ja(n)
                }
            };
            Ut.addEvent = Oa, Ut.removeEvent = Pa;
            var za = $a._enabled ? $a : fa, Va = Ut.extend({
                initialize: function () {
                }, acquireContext: function () {
                }, releaseContext: function () {
                }, addEventListener: function () {
                }, removeEventListener: function () {
                }
            }, za);
            $t._set("global", {plugins: {}});
            var Wa = {
                _plugins: [], _cacheId: 0, register: function (t) {
                    var e = this._plugins;
                    [].concat(t).forEach(function (t) {
                        -1 === e.indexOf(t) && e.push(t)
                    }), this._cacheId++
                }, unregister: function (t) {
                    var e = this._plugins;
                    [].concat(t).forEach(function (t) {
                        var a = e.indexOf(t);
                        -1 !== a && e.splice(a, 1)
                    }), this._cacheId++
                }, clear: function () {
                    this._plugins = [], this._cacheId++
                }, count: function () {
                    return this._plugins.length
                }, getAll: function () {
                    return this._plugins
                }, notify: function (t, e, a) {
                    var n, i, r, o, s, l = this.descriptors(t), c = l.length;
                    for (n = 0; n < c; ++n) if (i = l[n], r = i.plugin, s = r[e], "function" === typeof s && (o = [t].concat(a || []), o.push(i.options), !1 === s.apply(r, o))) return !1;
                    return !0
                }, descriptors: function (t) {
                    var e = t.$plugins || (t.$plugins = {});
                    if (e.id === this._cacheId) return e.descriptors;
                    var a = [], n = [], i = t && t.config || {}, r = i.options && i.options.plugins || {};
                    return this._plugins.concat(i.plugins || []).forEach(function (t) {
                        var e = a.indexOf(t);
                        if (-1 === e) {
                            var i = t.id, o = r[i];
                            !1 !== o && (!0 === o && (o = Ut.clone($t.global.plugins[i])), a.push(t), n.push({
                                plugin: t,
                                options: o || {}
                            }))
                        }
                    }), e.descriptors = n, e.id = this._cacheId, n
                }, _invalidate: function (t) {
                    delete t.$plugins
                }
            }, Ua = {
                constructors: {}, defaults: {}, registerScaleType: function (t, e, a) {
                    this.constructors[t] = e, this.defaults[t] = Ut.clone(a)
                }, getScaleConstructor: function (t) {
                    return this.constructors.hasOwnProperty(t) ? this.constructors[t] : void 0
                }, getScaleDefaults: function (t) {
                    return this.defaults.hasOwnProperty(t) ? Ut.merge({}, [$t.scale, this.defaults[t]]) : {}
                }, updateScaleDefaults: function (t, e) {
                    var a = this;
                    a.defaults.hasOwnProperty(t) && (a.defaults[t] = Ut.extend(a.defaults[t], e))
                }, addScalesToLayout: function (t) {
                    Ut.each(t.scales, function (e) {
                        e.fullWidth = e.options.fullWidth, e.position = e.options.position, e.weight = e.options.weight, ha.addBox(t, e)
                    })
                }
            }, qa = Ut.valueOrDefault;
            $t._set("global", {
                tooltips: {
                    enabled: !0,
                    custom: null,
                    mode: "nearest",
                    position: "average",
                    intersect: !0,
                    backgroundColor: "rgba(0,0,0,0.8)",
                    titleFontStyle: "bold",
                    titleSpacing: 2,
                    titleMarginBottom: 6,
                    titleFontColor: "#fff",
                    titleAlign: "left",
                    bodySpacing: 2,
                    bodyFontColor: "#fff",
                    bodyAlign: "left",
                    footerFontStyle: "bold",
                    footerSpacing: 2,
                    footerMarginTop: 6,
                    footerFontColor: "#fff",
                    footerAlign: "left",
                    yPadding: 6,
                    xPadding: 6,
                    caretPadding: 2,
                    caretSize: 5,
                    cornerRadius: 6,
                    multiKeyBackground: "#fff",
                    displayColors: !0,
                    borderColor: "rgba(0,0,0,0)",
                    borderWidth: 0,
                    callbacks: {
                        beforeTitle: Ut.noop,
                        title: function (t, e) {
                            var a = "", n = e.labels, i = n ? n.length : 0;
                            if (t.length > 0) {
                                var r = t[0];
                                r.label ? a = r.label : r.xLabel ? a = r.xLabel : i > 0 && r.index < i && (a = n[r.index])
                            }
                            return a
                        },
                        afterTitle: Ut.noop,
                        beforeBody: Ut.noop,
                        beforeLabel: Ut.noop,
                        label: function (t, e) {
                            var a = e.datasets[t.datasetIndex].label || "";
                            return a && (a += ": "), Ut.isNullOrUndef(t.value) ? a += t.yLabel : a += t.value, a
                        },
                        labelColor: function (t, e) {
                            var a = e.getDatasetMeta(t.datasetIndex), n = a.data[t.index], i = n._view;
                            return {borderColor: i.borderColor, backgroundColor: i.backgroundColor}
                        },
                        labelTextColor: function () {
                            return this._options.bodyFontColor
                        },
                        afterLabel: Ut.noop,
                        afterBody: Ut.noop,
                        beforeFooter: Ut.noop,
                        footer: Ut.noop,
                        afterFooter: Ut.noop
                    }
                }
            });
            var Ya = {
                average: function (t) {
                    if (!t.length) return !1;
                    var e, a, n = 0, i = 0, r = 0;
                    for (e = 0, a = t.length; e < a; ++e) {
                        var o = t[e];
                        if (o && o.hasValue()) {
                            var s = o.tooltipPosition();
                            n += s.x, i += s.y, ++r
                        }
                    }
                    return {x: n / r, y: i / r}
                }, nearest: function (t, e) {
                    var a, n, i, r = e.x, o = e.y, s = Number.POSITIVE_INFINITY;
                    for (a = 0, n = t.length; a < n; ++a) {
                        var l = t[a];
                        if (l && l.hasValue()) {
                            var c = l.getCenterPoint(), d = Ut.distanceBetweenPoints(e, c);
                            d < s && (s = d, i = l)
                        }
                    }
                    if (i) {
                        var u = i.tooltipPosition();
                        r = u.x, o = u.y
                    }
                    return {x: r, y: o}
                }
            };

            function Ga(t, e) {
                return e && (Ut.isArray(e) ? Array.prototype.push.apply(t, e) : t.push(e)), t
            }

            function Ka(t) {
                return ("string" === typeof t || t instanceof String) && t.indexOf("\n") > -1 ? t.split("\n") : t
            }

            function Xa(t) {
                var e = t._xScale, a = t._yScale || t._scale, n = t._index, i = t._datasetIndex,
                    r = t._chart.getDatasetMeta(i).controller, o = r._getIndexScale(), s = r._getValueScale();
                return {
                    xLabel: e ? e.getLabelForIndex(n, i) : "",
                    yLabel: a ? a.getLabelForIndex(n, i) : "",
                    label: o ? "" + o.getLabelForIndex(n, i) : "",
                    value: s ? "" + s.getLabelForIndex(n, i) : "",
                    index: n,
                    datasetIndex: i,
                    x: t._model.x,
                    y: t._model.y
                }
            }

            function Ja(t) {
                var e = $t.global;
                return {
                    xPadding: t.xPadding,
                    yPadding: t.yPadding,
                    xAlign: t.xAlign,
                    yAlign: t.yAlign,
                    bodyFontColor: t.bodyFontColor,
                    _bodyFontFamily: qa(t.bodyFontFamily, e.defaultFontFamily),
                    _bodyFontStyle: qa(t.bodyFontStyle, e.defaultFontStyle),
                    _bodyAlign: t.bodyAlign,
                    bodyFontSize: qa(t.bodyFontSize, e.defaultFontSize),
                    bodySpacing: t.bodySpacing,
                    titleFontColor: t.titleFontColor,
                    _titleFontFamily: qa(t.titleFontFamily, e.defaultFontFamily),
                    _titleFontStyle: qa(t.titleFontStyle, e.defaultFontStyle),
                    titleFontSize: qa(t.titleFontSize, e.defaultFontSize),
                    _titleAlign: t.titleAlign,
                    titleSpacing: t.titleSpacing,
                    titleMarginBottom: t.titleMarginBottom,
                    footerFontColor: t.footerFontColor,
                    _footerFontFamily: qa(t.footerFontFamily, e.defaultFontFamily),
                    _footerFontStyle: qa(t.footerFontStyle, e.defaultFontStyle),
                    footerFontSize: qa(t.footerFontSize, e.defaultFontSize),
                    _footerAlign: t.footerAlign,
                    footerSpacing: t.footerSpacing,
                    footerMarginTop: t.footerMarginTop,
                    caretSize: t.caretSize,
                    cornerRadius: t.cornerRadius,
                    backgroundColor: t.backgroundColor,
                    opacity: 0,
                    legendColorBackground: t.multiKeyBackground,
                    displayColors: t.displayColors,
                    borderColor: t.borderColor,
                    borderWidth: t.borderWidth
                }
            }

            function Za(t, e) {
                var a = t._chart.ctx, n = 2 * e.yPadding, i = 0, r = e.body, o = r.reduce(function (t, e) {
                    return t + e.before.length + e.lines.length + e.after.length
                }, 0);
                o += e.beforeBody.length + e.afterBody.length;
                var s = e.title.length, l = e.footer.length, c = e.titleFontSize, d = e.bodyFontSize,
                    u = e.footerFontSize;
                n += s * c, n += s ? (s - 1) * e.titleSpacing : 0, n += s ? e.titleMarginBottom : 0, n += o * d, n += o ? (o - 1) * e.bodySpacing : 0, n += l ? e.footerMarginTop : 0, n += l * u, n += l ? (l - 1) * e.footerSpacing : 0;
                var h = 0, f = function (t) {
                    i = Math.max(i, a.measureText(t).width + h)
                };
                return a.font = Ut.fontString(c, e._titleFontStyle, e._titleFontFamily), Ut.each(e.title, f), a.font = Ut.fontString(d, e._bodyFontStyle, e._bodyFontFamily), Ut.each(e.beforeBody.concat(e.afterBody), f), h = e.displayColors ? d + 2 : 0, Ut.each(r, function (t) {
                    Ut.each(t.before, f), Ut.each(t.lines, f), Ut.each(t.after, f)
                }), h = 0, a.font = Ut.fontString(u, e._footerFontStyle, e._footerFontFamily), Ut.each(e.footer, f), i += 2 * e.xPadding, {
                    width: i,
                    height: n
                }
            }

            function Qa(t, e) {
                var a, n, i, r, o, s = t._model, l = t._chart, c = t._chart.chartArea, d = "center", u = "center";
                s.y < e.height ? u = "top" : s.y > l.height - e.height && (u = "bottom");
                var h = (c.left + c.right) / 2, f = (c.top + c.bottom) / 2;
                "center" === u ? (a = function (t) {
                    return t <= h
                }, n = function (t) {
                    return t > h
                }) : (a = function (t) {
                    return t <= e.width / 2
                }, n = function (t) {
                    return t >= l.width - e.width / 2
                }), i = function (t) {
                    return t + e.width + s.caretSize + s.caretPadding > l.width
                }, r = function (t) {
                    return t - e.width - s.caretSize - s.caretPadding < 0
                }, o = function (t) {
                    return t <= f ? "top" : "bottom"
                }, a(s.x) ? (d = "left", i(s.x) && (d = "center", u = o(s.y))) : n(s.x) && (d = "right", r(s.x) && (d = "center", u = o(s.y)));
                var p = t._options;
                return {xAlign: p.xAlign ? p.xAlign : d, yAlign: p.yAlign ? p.yAlign : u}
            }

            function tn(t, e, a, n) {
                var i = t.x, r = t.y, o = t.caretSize, s = t.caretPadding, l = t.cornerRadius, c = a.xAlign,
                    d = a.yAlign, u = o + s, h = l + s;
                return "right" === c ? i -= e.width : "center" === c && (i -= e.width / 2, i + e.width > n.width && (i = n.width - e.width), i < 0 && (i = 0)), "top" === d ? r += u : r -= "bottom" === d ? e.height + u : e.height / 2, "center" === d ? "left" === c ? i += u : "right" === c && (i -= u) : "left" === c ? i -= h : "right" === c && (i += h), {
                    x: i,
                    y: r
                }
            }

            function en(t, e) {
                return "center" === e ? t.x + t.width / 2 : "right" === e ? t.x + t.width - t.xPadding : t.x + t.xPadding
            }

            function an(t) {
                return Ga([], Ka(t))
            }

            var nn = Jt.extend({
                initialize: function () {
                    this._model = Ja(this._options), this._lastActive = []
                }, getTitle: function () {
                    var t = this, e = t._options, a = e.callbacks, n = a.beforeTitle.apply(t, arguments),
                        i = a.title.apply(t, arguments), r = a.afterTitle.apply(t, arguments), o = [];
                    return o = Ga(o, Ka(n)), o = Ga(o, Ka(i)), o = Ga(o, Ka(r)), o
                }, getBeforeBody: function () {
                    return an(this._options.callbacks.beforeBody.apply(this, arguments))
                }, getBody: function (t, e) {
                    var a = this, n = a._options.callbacks, i = [];
                    return Ut.each(t, function (t) {
                        var r = {before: [], lines: [], after: []};
                        Ga(r.before, Ka(n.beforeLabel.call(a, t, e))), Ga(r.lines, n.label.call(a, t, e)), Ga(r.after, Ka(n.afterLabel.call(a, t, e))), i.push(r)
                    }), i
                }, getAfterBody: function () {
                    return an(this._options.callbacks.afterBody.apply(this, arguments))
                }, getFooter: function () {
                    var t = this, e = t._options.callbacks, a = e.beforeFooter.apply(t, arguments),
                        n = e.footer.apply(t, arguments), i = e.afterFooter.apply(t, arguments), r = [];
                    return r = Ga(r, Ka(a)), r = Ga(r, Ka(n)), r = Ga(r, Ka(i)), r
                }, update: function (t) {
                    var e, a, n = this, i = n._options, r = n._model, o = n._model = Ja(i), s = n._active, l = n._data,
                        c = {xAlign: r.xAlign, yAlign: r.yAlign}, d = {x: r.x, y: r.y},
                        u = {width: r.width, height: r.height}, h = {x: r.caretX, y: r.caretY};
                    if (s.length) {
                        o.opacity = 1;
                        var f = [], p = [];
                        h = Ya[i.position].call(n, s, n._eventPosition);
                        var g = [];
                        for (e = 0, a = s.length; e < a; ++e) g.push(Xa(s[e]));
                        i.filter && (g = g.filter(function (t) {
                            return i.filter(t, l)
                        })), i.itemSort && (g = g.sort(function (t, e) {
                            return i.itemSort(t, e, l)
                        })), Ut.each(g, function (t) {
                            f.push(i.callbacks.labelColor.call(n, t, n._chart)), p.push(i.callbacks.labelTextColor.call(n, t, n._chart))
                        }), o.title = n.getTitle(g, l), o.beforeBody = n.getBeforeBody(g, l), o.body = n.getBody(g, l), o.afterBody = n.getAfterBody(g, l), o.footer = n.getFooter(g, l), o.x = h.x, o.y = h.y, o.caretPadding = i.caretPadding, o.labelColors = f, o.labelTextColors = p, o.dataPoints = g, u = Za(this, o), c = Qa(this, u), d = tn(o, u, c, n._chart)
                    } else o.opacity = 0;
                    return o.xAlign = c.xAlign, o.yAlign = c.yAlign, o.x = d.x, o.y = d.y, o.width = u.width, o.height = u.height, o.caretX = h.x, o.caretY = h.y, n._model = o, t && i.custom && i.custom.call(n, o), n
                }, drawCaret: function (t, e) {
                    var a = this._chart.ctx, n = this._view, i = this.getCaretPosition(t, e, n);
                    a.lineTo(i.x1, i.y1), a.lineTo(i.x2, i.y2), a.lineTo(i.x3, i.y3)
                }, getCaretPosition: function (t, e, a) {
                    var n, i, r, o, s, l, c = a.caretSize, d = a.cornerRadius, u = a.xAlign, h = a.yAlign, f = t.x,
                        p = t.y, g = e.width, m = e.height;
                    if ("center" === h) s = p + m / 2, "left" === u ? (n = f, i = n - c, r = n, o = s + c, l = s - c) : (n = f + g, i = n + c, r = n, o = s - c, l = s + c); else if ("left" === u ? (i = f + d + c, n = i - c, r = i + c) : "right" === u ? (i = f + g - d - c, n = i - c, r = i + c) : (i = a.caretX, n = i - c, r = i + c), "top" === h) o = p, s = o - c, l = o; else {
                        o = p + m, s = o + c, l = o;
                        var v = r;
                        r = n, n = v
                    }
                    return {x1: n, x2: i, x3: r, y1: o, y2: s, y3: l}
                }, drawTitle: function (t, e, a) {
                    var n = e.title;
                    if (n.length) {
                        t.x = en(e, e._titleAlign), a.textAlign = e._titleAlign, a.textBaseline = "top";
                        var i, r, o = e.titleFontSize, s = e.titleSpacing;
                        for (a.fillStyle = e.titleFontColor, a.font = Ut.fontString(o, e._titleFontStyle, e._titleFontFamily), i = 0, r = n.length; i < r; ++i) a.fillText(n[i], t.x, t.y), t.y += o + s, i + 1 === n.length && (t.y += e.titleMarginBottom - s)
                    }
                }, drawBody: function (t, e, a) {
                    var n, i = e.bodyFontSize, r = e.bodySpacing, o = e._bodyAlign, s = e.body, l = e.displayColors,
                        c = e.labelColors, d = 0, u = l ? en(e, "left") : 0;
                    a.textAlign = o, a.textBaseline = "top", a.font = Ut.fontString(i, e._bodyFontStyle, e._bodyFontFamily), t.x = en(e, o);
                    var h = function (e) {
                        a.fillText(e, t.x + d, t.y), t.y += i + r
                    };
                    a.fillStyle = e.bodyFontColor, Ut.each(e.beforeBody, h), d = l && "right" !== o ? "center" === o ? i / 2 + 1 : i + 2 : 0, Ut.each(s, function (r, o) {
                        n = e.labelTextColors[o], a.fillStyle = n, Ut.each(r.before, h), Ut.each(r.lines, function (r) {
                            l && (a.fillStyle = e.legendColorBackground, a.fillRect(u, t.y, i, i), a.lineWidth = 1, a.strokeStyle = c[o].borderColor, a.strokeRect(u, t.y, i, i), a.fillStyle = c[o].backgroundColor, a.fillRect(u + 1, t.y + 1, i - 2, i - 2), a.fillStyle = n), h(r)
                        }), Ut.each(r.after, h)
                    }), d = 0, Ut.each(e.afterBody, h), t.y -= r
                }, drawFooter: function (t, e, a) {
                    var n = e.footer;
                    n.length && (t.x = en(e, e._footerAlign), t.y += e.footerMarginTop, a.textAlign = e._footerAlign, a.textBaseline = "top", a.fillStyle = e.footerFontColor, a.font = Ut.fontString(e.footerFontSize, e._footerFontStyle, e._footerFontFamily), Ut.each(n, function (n) {
                        a.fillText(n, t.x, t.y), t.y += e.footerFontSize + e.footerSpacing
                    }))
                }, drawBackground: function (t, e, a, n) {
                    a.fillStyle = e.backgroundColor, a.strokeStyle = e.borderColor, a.lineWidth = e.borderWidth;
                    var i = e.xAlign, r = e.yAlign, o = t.x, s = t.y, l = n.width, c = n.height, d = e.cornerRadius;
                    a.beginPath(), a.moveTo(o + d, s), "top" === r && this.drawCaret(t, n), a.lineTo(o + l - d, s), a.quadraticCurveTo(o + l, s, o + l, s + d), "center" === r && "right" === i && this.drawCaret(t, n), a.lineTo(o + l, s + c - d), a.quadraticCurveTo(o + l, s + c, o + l - d, s + c), "bottom" === r && this.drawCaret(t, n), a.lineTo(o + d, s + c), a.quadraticCurveTo(o, s + c, o, s + c - d), "center" === r && "left" === i && this.drawCaret(t, n), a.lineTo(o, s + d), a.quadraticCurveTo(o, s, o + d, s), a.closePath(), a.fill(), e.borderWidth > 0 && a.stroke()
                }, draw: function () {
                    var t = this._chart.ctx, e = this._view;
                    if (0 !== e.opacity) {
                        var a = {width: e.width, height: e.height}, n = {x: e.x, y: e.y},
                            i = Math.abs(e.opacity < .001) ? 0 : e.opacity,
                            r = e.title.length || e.beforeBody.length || e.body.length || e.afterBody.length || e.footer.length;
                        this._options.enabled && r && (t.save(), t.globalAlpha = i, this.drawBackground(n, e, t, a), n.y += e.yPadding, this.drawTitle(n, e, t), this.drawBody(n, e, t), this.drawFooter(n, e, t), t.restore())
                    }
                }, handleEvent: function (t) {
                    var e = this, a = e._options, n = !1;
                    return e._lastActive = e._lastActive || [], "mouseout" === t.type ? e._active = [] : e._active = e._chart.getElementsAtEventForMode(t, a.mode, a), n = !Ut.arrayEquals(e._active, e._lastActive), n && (e._lastActive = e._active, (a.enabled || a.custom) && (e._eventPosition = {
                        x: t.x,
                        y: t.y
                    }, e.update(!0), e.pivot())), n
                }
            }), rn = Ya, on = nn;
            on.positioners = rn;
            var sn = Ut.valueOrDefault;

            function ln() {
                return Ut.merge({}, [].slice.call(arguments), {
                    merger: function (t, e, a, n) {
                        if ("xAxes" === t || "yAxes" === t) {
                            var i, r, o, s = a[t].length;
                            for (e[t] || (e[t] = []), i = 0; i < s; ++i) o = a[t][i], r = sn(o.type, "xAxes" === t ? "category" : "linear"), i >= e[t].length && e[t].push({}), !e[t][i].type || o.type && o.type !== e[t][i].type ? Ut.merge(e[t][i], [Ua.getScaleDefaults(r), o]) : Ut.merge(e[t][i], o)
                        } else Ut._merger(t, e, a, n)
                    }
                })
            }

            function cn() {
                return Ut.merge({}, [].slice.call(arguments), {
                    merger: function (t, e, a, n) {
                        var i = e[t] || {}, r = a[t];
                        "scales" === t ? e[t] = ln(i, r) : "scale" === t ? e[t] = Ut.merge(i, [Ua.getScaleDefaults(r.type), r]) : Ut._merger(t, e, a, n)
                    }
                })
            }

            function dn(t) {
                t = t || {};
                var e = t.data = t.data || {};
                return e.datasets = e.datasets || [], e.labels = e.labels || [], t.options = cn($t.global, $t[t.type], t.options || {}), t
            }

            function un(t) {
                var e = t.options;
                Ut.each(t.scales, function (e) {
                    ha.removeBox(t, e)
                }), e = cn($t.global, $t[t.config.type], e), t.options = t.config.options = e, t.ensureScalesHaveIDs(), t.buildOrUpdateScales(), t.tooltip._options = e.tooltips, t.tooltip.initialize()
            }

            function hn(t) {
                return "top" === t || "bottom" === t
            }

            $t._set("global", {
                elements: {},
                events: ["mousemove", "mouseout", "click", "touchstart", "touchmove"],
                hover: {onHover: null, mode: "nearest", intersect: !0, animationDuration: 400},
                onClick: null,
                maintainAspectRatio: !0,
                responsive: !0,
                responsiveAnimationDuration: 0
            });
            var fn = function (t, e) {
                return this.construct(t, e), this
            };
            Ut.extend(fn.prototype, {
                construct: function (t, e) {
                    var a = this;
                    e = dn(e);
                    var n = Va.acquireContext(t, e), i = n && n.canvas, r = i && i.height, o = i && i.width;
                    a.id = Ut.uid(), a.ctx = n, a.canvas = i, a.config = e, a.width = o, a.height = r, a.aspectRatio = r ? o / r : null, a.options = e.options, a._bufferedRender = !1, a.chart = a, a.controller = a, fn.instances[a.id] = a, Object.defineProperty(a, "data", {
                        get: function () {
                            return a.config.data
                        }, set: function (t) {
                            a.config.data = t
                        }
                    }), n && i ? (a.initialize(), a.update()) : console.error("Failed to create chart: can't acquire context from the given item")
                }, initialize: function () {
                    var t = this;
                    return Wa.notify(t, "beforeInit"), Ut.retinaScale(t, t.options.devicePixelRatio), t.bindEvents(), t.options.responsive && t.resize(!0), t.ensureScalesHaveIDs(), t.buildOrUpdateScales(), t.initToolTip(), Wa.notify(t, "afterInit"), t
                }, clear: function () {
                    return Ut.canvas.clear(this), this
                }, stop: function () {
                    return te.cancelAnimation(this), this
                }, resize: function (t) {
                    var e = this, a = e.options, n = e.canvas, i = a.maintainAspectRatio && e.aspectRatio || null,
                        r = Math.max(0, Math.floor(Ut.getMaximumWidth(n))),
                        o = Math.max(0, Math.floor(i ? r / i : Ut.getMaximumHeight(n)));
                    if ((e.width !== r || e.height !== o) && (n.width = e.width = r, n.height = e.height = o, n.style.width = r + "px", n.style.height = o + "px", Ut.retinaScale(e, a.devicePixelRatio), !t)) {
                        var s = {width: r, height: o};
                        Wa.notify(e, "resize", [s]), a.onResize && a.onResize(e, s), e.stop(), e.update({duration: a.responsiveAnimationDuration})
                    }
                }, ensureScalesHaveIDs: function () {
                    var t = this.options, e = t.scales || {}, a = t.scale;
                    Ut.each(e.xAxes, function (t, e) {
                        t.id = t.id || "x-axis-" + e
                    }), Ut.each(e.yAxes, function (t, e) {
                        t.id = t.id || "y-axis-" + e
                    }), a && (a.id = a.id || "scale")
                }, buildOrUpdateScales: function () {
                    var t = this, e = t.options, a = t.scales || {}, n = [], i = Object.keys(a).reduce(function (t, e) {
                        return t[e] = !1, t
                    }, {});
                    e.scales && (n = n.concat((e.scales.xAxes || []).map(function (t) {
                        return {options: t, dtype: "category", dposition: "bottom"}
                    }), (e.scales.yAxes || []).map(function (t) {
                        return {options: t, dtype: "linear", dposition: "left"}
                    }))), e.scale && n.push({
                        options: e.scale,
                        dtype: "radialLinear",
                        isDefault: !0,
                        dposition: "chartArea"
                    }), Ut.each(n, function (e) {
                        var n = e.options, r = n.id, o = sn(n.type, e.dtype);
                        hn(n.position) !== hn(e.dposition) && (n.position = e.dposition), i[r] = !0;
                        var s = null;
                        if (r in a && a[r].type === o) s = a[r], s.options = n, s.ctx = t.ctx, s.chart = t; else {
                            var l = Ua.getScaleConstructor(o);
                            if (!l) return;
                            s = new l({id: r, type: o, options: n, ctx: t.ctx, chart: t}), a[s.id] = s
                        }
                        s.mergeTicksOptions(), e.isDefault && (t.scale = s)
                    }), Ut.each(i, function (t, e) {
                        t || delete a[e]
                    }), t.scales = a, Ua.addScalesToLayout(this)
                }, buildOrUpdateControllers: function () {
                    var t = this, e = [];
                    return Ut.each(t.data.datasets, function (a, n) {
                        var i = t.getDatasetMeta(n), r = a.type || t.config.type;
                        if (i.type && i.type !== r && (t.destroyDatasetMeta(n), i = t.getDatasetMeta(n)), i.type = r, i.controller) i.controller.updateIndex(n), i.controller.linkScales(); else {
                            var o = ta[i.type];
                            if (void 0 === o) throw new Error('"' + i.type + '" is not a chart type.');
                            i.controller = new o(t, n), e.push(i.controller)
                        }
                    }, t), e
                }, resetElements: function () {
                    var t = this;
                    Ut.each(t.data.datasets, function (e, a) {
                        t.getDatasetMeta(a).controller.reset()
                    }, t)
                }, reset: function () {
                    this.resetElements(), this.tooltip.initialize()
                }, update: function (t) {
                    var e = this;
                    if (t && "object" === typeof t || (t = {
                            duration: t,
                            lazy: arguments[1]
                        }), un(e), Wa._invalidate(e), !1 !== Wa.notify(e, "beforeUpdate")) {
                        e.tooltip._data = e.data;
                        var a = e.buildOrUpdateControllers();
                        Ut.each(e.data.datasets, function (t, a) {
                            e.getDatasetMeta(a).controller.buildOrUpdateElements()
                        }, e), e.updateLayout(), e.options.animation && e.options.animation.duration && Ut.each(a, function (t) {
                            t.reset()
                        }), e.updateDatasets(), e.tooltip.initialize(), e.lastActive = [], Wa.notify(e, "afterUpdate"), e._bufferedRender ? e._bufferedRequest = {
                            duration: t.duration,
                            easing: t.easing,
                            lazy: t.lazy
                        } : e.render(t)
                    }
                }, updateLayout: function () {
                    var t = this;
                    !1 !== Wa.notify(t, "beforeLayout") && (ha.update(this, this.width, this.height), Wa.notify(t, "afterScaleUpdate"), Wa.notify(t, "afterLayout"))
                }, updateDatasets: function () {
                    var t = this;
                    if (!1 !== Wa.notify(t, "beforeDatasetsUpdate")) {
                        for (var e = 0, a = t.data.datasets.length; e < a; ++e) t.updateDataset(e);
                        Wa.notify(t, "afterDatasetsUpdate")
                    }
                }, updateDataset: function (t) {
                    var e = this, a = e.getDatasetMeta(t), n = {meta: a, index: t};
                    !1 !== Wa.notify(e, "beforeDatasetUpdate", [n]) && (a.controller.update(), Wa.notify(e, "afterDatasetUpdate", [n]))
                }, render: function (t) {
                    var e = this;
                    t && "object" === typeof t || (t = {duration: t, lazy: arguments[1]});
                    var a = e.options.animation, n = sn(t.duration, a && a.duration), i = t.lazy;
                    if (!1 !== Wa.notify(e, "beforeRender")) {
                        var r = function (t) {
                            Wa.notify(e, "afterRender"), Ut.callback(a && a.onComplete, [t], e)
                        };
                        if (a && n) {
                            var o = new Qt({
                                numSteps: n / 16.66, easing: t.easing || a.easing, render: function (t, e) {
                                    var a = Ut.easing.effects[e.easing], n = e.currentStep, i = n / e.numSteps;
                                    t.draw(a(i), i, n)
                                }, onAnimationProgress: a.onProgress, onAnimationComplete: r
                            });
                            te.addAnimation(e, o, n, i)
                        } else e.draw(), r(new Qt({numSteps: 0, chart: e}));
                        return e
                    }
                }, draw: function (t) {
                    var e = this;
                    e.clear(), Ut.isNullOrUndef(t) && (t = 1), e.transition(t), e.width <= 0 || e.height <= 0 || !1 !== Wa.notify(e, "beforeDraw", [t]) && (Ut.each(e.boxes, function (t) {
                        t.draw(e.chartArea)
                    }, e), e.drawDatasets(t), e._drawTooltip(t), Wa.notify(e, "afterDraw", [t]))
                }, transition: function (t) {
                    for (var e = this, a = 0, n = (e.data.datasets || []).length; a < n; ++a) e.isDatasetVisible(a) && e.getDatasetMeta(a).controller.transition(t);
                    e.tooltip.transition(t)
                }, drawDatasets: function (t) {
                    var e = this;
                    if (!1 !== Wa.notify(e, "beforeDatasetsDraw", [t])) {
                        for (var a = (e.data.datasets || []).length - 1; a >= 0; --a) e.isDatasetVisible(a) && e.drawDataset(a, t);
                        Wa.notify(e, "afterDatasetsDraw", [t])
                    }
                }, drawDataset: function (t, e) {
                    var a = this, n = a.getDatasetMeta(t), i = {meta: n, index: t, easingValue: e};
                    !1 !== Wa.notify(a, "beforeDatasetDraw", [i]) && (n.controller.draw(e), Wa.notify(a, "afterDatasetDraw", [i]))
                }, _drawTooltip: function (t) {
                    var e = this, a = e.tooltip, n = {tooltip: a, easingValue: t};
                    !1 !== Wa.notify(e, "beforeTooltipDraw", [n]) && (a.draw(), Wa.notify(e, "afterTooltipDraw", [n]))
                }, getElementAtEvent: function (t) {
                    return sa.modes.single(this, t)
                }, getElementsAtEvent: function (t) {
                    return sa.modes.label(this, t, {intersect: !0})
                }, getElementsAtXAxis: function (t) {
                    return sa.modes["x-axis"](this, t, {intersect: !0})
                }, getElementsAtEventForMode: function (t, e, a) {
                    var n = sa.modes[e];
                    return "function" === typeof n ? n(this, t, a) : []
                }, getDatasetAtEvent: function (t) {
                    return sa.modes.dataset(this, t, {intersect: !0})
                }, getDatasetMeta: function (t) {
                    var e = this, a = e.data.datasets[t];
                    a._meta || (a._meta = {});
                    var n = a._meta[e.id];
                    return n || (n = a._meta[e.id] = {
                        type: null,
                        data: [],
                        dataset: null,
                        controller: null,
                        hidden: null,
                        xAxisID: null,
                        yAxisID: null
                    }), n
                }, getVisibleDatasetCount: function () {
                    for (var t = 0, e = 0, a = this.data.datasets.length; e < a; ++e) this.isDatasetVisible(e) && t++;
                    return t
                }, isDatasetVisible: function (t) {
                    var e = this.getDatasetMeta(t);
                    return "boolean" === typeof e.hidden ? !e.hidden : !this.data.datasets[t].hidden
                }, generateLegend: function () {
                    return this.options.legendCallback(this)
                }, destroyDatasetMeta: function (t) {
                    var e = this.id, a = this.data.datasets[t], n = a._meta && a._meta[e];
                    n && (n.controller.destroy(), delete a._meta[e])
                }, destroy: function () {
                    var t, e, a = this, n = a.canvas;
                    for (a.stop(), t = 0, e = a.data.datasets.length; t < e; ++t) a.destroyDatasetMeta(t);
                    n && (a.unbindEvents(), Ut.canvas.clear(a), Va.releaseContext(a.ctx), a.canvas = null, a.ctx = null), Wa.notify(a, "destroy"), delete fn.instances[a.id]
                }, toBase64Image: function () {
                    return this.canvas.toDataURL.apply(this.canvas, arguments)
                }, initToolTip: function () {
                    var t = this;
                    t.tooltip = new on({_chart: t, _chartInstance: t, _data: t.data, _options: t.options.tooltips}, t)
                }, bindEvents: function () {
                    var t = this, e = t._listeners = {}, a = function () {
                        t.eventHandler.apply(t, arguments)
                    };
                    Ut.each(t.options.events, function (n) {
                        Va.addEventListener(t, n, a), e[n] = a
                    }), t.options.responsive && (a = function () {
                        t.resize()
                    }, Va.addEventListener(t, "resize", a), e.resize = a)
                }, unbindEvents: function () {
                    var t = this, e = t._listeners;
                    e && (delete t._listeners, Ut.each(e, function (e, a) {
                        Va.removeEventListener(t, a, e)
                    }))
                }, updateHoverStyle: function (t, e, a) {
                    var n, i, r, o = a ? "setHoverStyle" : "removeHoverStyle";
                    for (i = 0, r = t.length; i < r; ++i) n = t[i], n && this.getDatasetMeta(n._datasetIndex).controller[o](n)
                }, eventHandler: function (t) {
                    var e = this, a = e.tooltip;
                    if (!1 !== Wa.notify(e, "beforeEvent", [t])) {
                        e._bufferedRender = !0, e._bufferedRequest = null;
                        var n = e.handleEvent(t);
                        a && (n = a._start ? a.handleEvent(t) : n | a.handleEvent(t)), Wa.notify(e, "afterEvent", [t]);
                        var i = e._bufferedRequest;
                        return i ? e.render(i) : n && !e.animating && (e.stop(), e.render({
                            duration: e.options.hover.animationDuration,
                            lazy: !0
                        })), e._bufferedRender = !1, e._bufferedRequest = null, e
                    }
                }, handleEvent: function (t) {
                    var e = this, a = e.options || {}, n = a.hover, i = !1;
                    return e.lastActive = e.lastActive || [], "mouseout" === t.type ? e.active = [] : e.active = e.getElementsAtEventForMode(t, n.mode, n), Ut.callback(a.onHover || a.hover.onHover, [t.native, e.active], e), "mouseup" !== t.type && "click" !== t.type || a.onClick && a.onClick.call(e, t.native, e.active), e.lastActive.length && e.updateHoverStyle(e.lastActive, n.mode, !1), e.active.length && n.mode && e.updateHoverStyle(e.active, n.mode, !0), i = !Ut.arrayEquals(e.active, e.lastActive), e.lastActive = e.active, i
                }
            }), fn.instances = {};
            var pn = fn;
            fn.Controller = fn, fn.types = {}, Ut.configMerge = cn, Ut.scaleMerge = ln;
            var gn = function () {
                function t(t, e, a) {
                    var n;
                    return "string" === typeof t ? (n = parseInt(t, 10), -1 !== t.indexOf("%") && (n = n / 100 * e.parentNode[a])) : n = t, n
                }

                function e(t) {
                    return void 0 !== t && null !== t && "none" !== t
                }

                function a(a, n, i) {
                    var r = document.defaultView, o = Ut._getParentNode(a), s = r.getComputedStyle(a)[n],
                        l = r.getComputedStyle(o)[n], c = e(s), d = e(l), u = Number.POSITIVE_INFINITY;
                    return c || d ? Math.min(c ? t(s, a, i) : u, d ? t(l, o, i) : u) : "none"
                }

                Ut.where = function (t, e) {
                    if (Ut.isArray(t) && Array.prototype.filter) return t.filter(e);
                    var a = [];
                    return Ut.each(t, function (t) {
                        e(t) && a.push(t)
                    }), a
                }, Ut.findIndex = Array.prototype.findIndex ? function (t, e, a) {
                    return t.findIndex(e, a)
                } : function (t, e, a) {
                    a = void 0 === a ? t : a;
                    for (var n = 0, i = t.length; n < i; ++n) if (e.call(a, t[n], n, t)) return n;
                    return -1
                }, Ut.findNextWhere = function (t, e, a) {
                    Ut.isNullOrUndef(a) && (a = -1);
                    for (var n = a + 1; n < t.length; n++) {
                        var i = t[n];
                        if (e(i)) return i
                    }
                }, Ut.findPreviousWhere = function (t, e, a) {
                    Ut.isNullOrUndef(a) && (a = t.length);
                    for (var n = a - 1; n >= 0; n--) {
                        var i = t[n];
                        if (e(i)) return i
                    }
                }, Ut.isNumber = function (t) {
                    return !isNaN(parseFloat(t)) && isFinite(t)
                }, Ut.almostEquals = function (t, e, a) {
                    return Math.abs(t - e) < a
                }, Ut.almostWhole = function (t, e) {
                    var a = Math.round(t);
                    return a - e < t && a + e > t
                }, Ut.max = function (t) {
                    return t.reduce(function (t, e) {
                        return isNaN(e) ? t : Math.max(t, e)
                    }, Number.NEGATIVE_INFINITY)
                }, Ut.min = function (t) {
                    return t.reduce(function (t, e) {
                        return isNaN(e) ? t : Math.min(t, e)
                    }, Number.POSITIVE_INFINITY)
                }, Ut.sign = Math.sign ? function (t) {
                    return Math.sign(t)
                } : function (t) {
                    return t = +t, 0 === t || isNaN(t) ? t : t > 0 ? 1 : -1
                }, Ut.log10 = Math.log10 ? function (t) {
                    return Math.log10(t)
                } : function (t) {
                    var e = Math.log(t) * Math.LOG10E, a = Math.round(e), n = t === Math.pow(10, a);
                    return n ? a : e
                }, Ut.toRadians = function (t) {
                    return t * (Math.PI / 180)
                }, Ut.toDegrees = function (t) {
                    return t * (180 / Math.PI)
                }, Ut._decimalPlaces = function (t) {
                    if (Ut.isFinite(t)) {
                        var e = 1, a = 0;
                        while (Math.round(t * e) / e !== t) e *= 10, a++;
                        return a
                    }
                }, Ut.getAngleFromPoint = function (t, e) {
                    var a = e.x - t.x, n = e.y - t.y, i = Math.sqrt(a * a + n * n), r = Math.atan2(n, a);
                    return r < -.5 * Math.PI && (r += 2 * Math.PI), {angle: r, distance: i}
                }, Ut.distanceBetweenPoints = function (t, e) {
                    return Math.sqrt(Math.pow(e.x - t.x, 2) + Math.pow(e.y - t.y, 2))
                }, Ut.aliasPixel = function (t) {
                    return t % 2 === 0 ? 0 : .5
                }, Ut._alignPixel = function (t, e, a) {
                    var n = t.currentDevicePixelRatio, i = a / 2;
                    return Math.round((e - i) * n) / n + i
                }, Ut.splineCurve = function (t, e, a, n) {
                    var i = t.skip ? e : t, r = e, o = a.skip ? e : a,
                        s = Math.sqrt(Math.pow(r.x - i.x, 2) + Math.pow(r.y - i.y, 2)),
                        l = Math.sqrt(Math.pow(o.x - r.x, 2) + Math.pow(o.y - r.y, 2)), c = s / (s + l),
                        d = l / (s + l);
                    c = isNaN(c) ? 0 : c, d = isNaN(d) ? 0 : d;
                    var u = n * c, h = n * d;
                    return {
                        previous: {x: r.x - u * (o.x - i.x), y: r.y - u * (o.y - i.y)},
                        next: {x: r.x + h * (o.x - i.x), y: r.y + h * (o.y - i.y)}
                    }
                }, Ut.EPSILON = Number.EPSILON || 1e-14, Ut.splineCurveMonotone = function (t) {
                    var e, a, n, i, r, o, s, l, c, d = (t || []).map(function (t) {
                        return {model: t._model, deltaK: 0, mK: 0}
                    }), u = d.length;
                    for (e = 0; e < u; ++e) if (n = d[e], !n.model.skip) {
                        if (a = e > 0 ? d[e - 1] : null, i = e < u - 1 ? d[e + 1] : null, i && !i.model.skip) {
                            var h = i.model.x - n.model.x;
                            n.deltaK = 0 !== h ? (i.model.y - n.model.y) / h : 0
                        }
                        !a || a.model.skip ? n.mK = n.deltaK : !i || i.model.skip ? n.mK = a.deltaK : this.sign(a.deltaK) !== this.sign(n.deltaK) ? n.mK = 0 : n.mK = (a.deltaK + n.deltaK) / 2
                    }
                    for (e = 0; e < u - 1; ++e) n = d[e], i = d[e + 1], n.model.skip || i.model.skip || (Ut.almostEquals(n.deltaK, 0, this.EPSILON) ? n.mK = i.mK = 0 : (r = n.mK / n.deltaK, o = i.mK / n.deltaK, l = Math.pow(r, 2) + Math.pow(o, 2), l <= 9 || (s = 3 / Math.sqrt(l), n.mK = r * s * n.deltaK, i.mK = o * s * n.deltaK)));
                    for (e = 0; e < u; ++e) n = d[e], n.model.skip || (a = e > 0 ? d[e - 1] : null, i = e < u - 1 ? d[e + 1] : null, a && !a.model.skip && (c = (n.model.x - a.model.x) / 3, n.model.controlPointPreviousX = n.model.x - c, n.model.controlPointPreviousY = n.model.y - c * n.mK), i && !i.model.skip && (c = (i.model.x - n.model.x) / 3, n.model.controlPointNextX = n.model.x + c, n.model.controlPointNextY = n.model.y + c * n.mK))
                }, Ut.nextItem = function (t, e, a) {
                    return a ? e >= t.length - 1 ? t[0] : t[e + 1] : e >= t.length - 1 ? t[t.length - 1] : t[e + 1]
                }, Ut.previousItem = function (t, e, a) {
                    return a ? e <= 0 ? t[t.length - 1] : t[e - 1] : e <= 0 ? t[0] : t[e - 1]
                }, Ut.niceNum = function (t, e) {
                    var a, n = Math.floor(Ut.log10(t)), i = t / Math.pow(10, n);
                    return a = e ? i < 1.5 ? 1 : i < 3 ? 2 : i < 7 ? 5 : 10 : i <= 1 ? 1 : i <= 2 ? 2 : i <= 5 ? 5 : 10, a * Math.pow(10, n)
                }, Ut.requestAnimFrame = function () {
                    return "undefined" === typeof window ? function (t) {
                        t()
                    } : window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (t) {
                        return window.setTimeout(t, 1e3 / 60)
                    }
                }(), Ut.getRelativePosition = function (t, e) {
                    var a, n, i = t.originalEvent || t, r = t.target || t.srcElement, o = r.getBoundingClientRect(),
                        s = i.touches;
                    s && s.length > 0 ? (a = s[0].clientX, n = s[0].clientY) : (a = i.clientX, n = i.clientY);
                    var l = parseFloat(Ut.getStyle(r, "padding-left")), c = parseFloat(Ut.getStyle(r, "padding-top")),
                        d = parseFloat(Ut.getStyle(r, "padding-right")),
                        u = parseFloat(Ut.getStyle(r, "padding-bottom")), h = o.right - o.left - l - d,
                        f = o.bottom - o.top - c - u;
                    return a = Math.round((a - o.left - l) / h * r.width / e.currentDevicePixelRatio), n = Math.round((n - o.top - c) / f * r.height / e.currentDevicePixelRatio), {
                        x: a,
                        y: n
                    }
                }, Ut.getConstraintWidth = function (t) {
                    return a(t, "max-width", "clientWidth")
                }, Ut.getConstraintHeight = function (t) {
                    return a(t, "max-height", "clientHeight")
                }, Ut._calculatePadding = function (t, e, a) {
                    return e = Ut.getStyle(t, e), e.indexOf("%") > -1 ? a * parseInt(e, 10) / 100 : parseInt(e, 10)
                }, Ut._getParentNode = function (t) {
                    var e = t.parentNode;
                    return e && "[object ShadowRoot]" === e.toString() && (e = e.host), e
                }, Ut.getMaximumWidth = function (t) {
                    var e = Ut._getParentNode(t);
                    if (!e) return t.clientWidth;
                    var a = e.clientWidth, n = Ut._calculatePadding(e, "padding-left", a),
                        i = Ut._calculatePadding(e, "padding-right", a), r = a - n - i, o = Ut.getConstraintWidth(t);
                    return isNaN(o) ? r : Math.min(r, o)
                }, Ut.getMaximumHeight = function (t) {
                    var e = Ut._getParentNode(t);
                    if (!e) return t.clientHeight;
                    var a = e.clientHeight, n = Ut._calculatePadding(e, "padding-top", a),
                        i = Ut._calculatePadding(e, "padding-bottom", a), r = a - n - i, o = Ut.getConstraintHeight(t);
                    return isNaN(o) ? r : Math.min(r, o)
                }, Ut.getStyle = function (t, e) {
                    return t.currentStyle ? t.currentStyle[e] : document.defaultView.getComputedStyle(t, null).getPropertyValue(e)
                }, Ut.retinaScale = function (t, e) {
                    var a = t.currentDevicePixelRatio = e || "undefined" !== typeof window && window.devicePixelRatio || 1;
                    if (1 !== a) {
                        var n = t.canvas, i = t.height, r = t.width;
                        n.height = i * a, n.width = r * a, t.ctx.scale(a, a), n.style.height || n.style.width || (n.style.height = i + "px", n.style.width = r + "px")
                    }
                }, Ut.fontString = function (t, e, a) {
                    return e + " " + t + "px " + a
                }, Ut.longestText = function (t, e, a, n) {
                    n = n || {};
                    var i = n.data = n.data || {}, r = n.garbageCollect = n.garbageCollect || [];
                    n.font !== e && (i = n.data = {}, r = n.garbageCollect = [], n.font = e), t.font = e;
                    var o = 0;
                    Ut.each(a, function (e) {
                        void 0 !== e && null !== e && !0 !== Ut.isArray(e) ? o = Ut.measureText(t, i, r, o, e) : Ut.isArray(e) && Ut.each(e, function (e) {
                            void 0 === e || null === e || Ut.isArray(e) || (o = Ut.measureText(t, i, r, o, e))
                        })
                    });
                    var s = r.length / 2;
                    if (s > a.length) {
                        for (var l = 0; l < s; l++) delete i[r[l]];
                        r.splice(0, s)
                    }
                    return o
                }, Ut.measureText = function (t, e, a, n, i) {
                    var r = e[i];
                    return r || (r = e[i] = t.measureText(i).width, a.push(i)), r > n && (n = r), n
                }, Ut.numberOfLabelLines = function (t) {
                    var e = 1;
                    return Ut.each(t, function (t) {
                        Ut.isArray(t) && t.length > e && (e = t.length)
                    }), e
                }, Ut.color = St ? function (t) {
                    return t instanceof CanvasGradient && (t = $t.global.defaultColor), St(t)
                } : function (t) {
                    return console.error("Color.js not found!"), t
                }, Ut.getHoverColor = function (t) {
                    return t instanceof CanvasPattern || t instanceof CanvasGradient ? t : Ut.color(t).saturate(.5).darken(.1).rgbString()
                }
            };

            function mn() {
                throw new Error("This method is not implemented: either no adapter can be found or an incomplete integration was provided.")
            }

            function vn(t) {
                this.options = t || {}
            }

            Ut.extend(vn.prototype, {
                formats: mn,
                parse: mn,
                format: mn,
                add: mn,
                diff: mn,
                startOf: mn,
                endOf: mn,
                _create: function (t) {
                    return t
                }
            }), vn.override = function (t) {
                Ut.extend(vn.prototype, t)
            };
            var bn = vn, yn = {_date: bn}, xn = {
                formatters: {
                    values: function (t) {
                        return Ut.isArray(t) ? t : "" + t
                    }, linear: function (t, e, a) {
                        var n = a.length > 3 ? a[2] - a[1] : a[1] - a[0];
                        Math.abs(n) > 1 && t !== Math.floor(t) && (n = t - Math.floor(t));
                        var i = Ut.log10(Math.abs(n)), r = "";
                        if (0 !== t) {
                            var o = Math.max(Math.abs(a[0]), Math.abs(a[a.length - 1]));
                            if (o < 1e-4) {
                                var s = Ut.log10(Math.abs(t));
                                r = t.toExponential(Math.floor(s) - Math.floor(i))
                            } else {
                                var l = -1 * Math.floor(i);
                                l = Math.max(Math.min(l, 20), 0), r = t.toFixed(l)
                            }
                        } else r = "0";
                        return r
                    }, logarithmic: function (t, e, a) {
                        var n = t / Math.pow(10, Math.floor(Ut.log10(t)));
                        return 0 === t ? "0" : 1 === n || 2 === n || 5 === n || 0 === e || e === a.length - 1 ? t.toExponential() : ""
                    }
                }
            }, _n = Ut.valueOrDefault, Cn = Ut.valueAtIndexOrDefault;

            function kn(t) {
                var e, a, n = [];
                for (e = 0, a = t.length; e < a; ++e) n.push(t[e].label);
                return n
            }

            function wn(t, e, a) {
                var n = t.getPixelForTick(e);
                return a && (1 === t.getTicks().length ? n -= t.isHorizontal() ? Math.max(n - t.left, t.right - n) : Math.max(n - t.top, t.bottom - n) : n -= 0 === e ? (t.getPixelForTick(1) - n) / 2 : (n - t.getPixelForTick(e - 1)) / 2), n
            }

            function An(t, e, a) {
                return Ut.isArray(e) ? Ut.longestText(t, a, e) : t.measureText(e).width
            }

            $t._set("scale", {
                display: !0,
                position: "left",
                offset: !1,
                gridLines: {
                    display: !0,
                    color: "rgba(0, 0, 0, 0.1)",
                    lineWidth: 1,
                    drawBorder: !0,
                    drawOnChartArea: !0,
                    drawTicks: !0,
                    tickMarkLength: 10,
                    zeroLineWidth: 1,
                    zeroLineColor: "rgba(0,0,0,0.25)",
                    zeroLineBorderDash: [],
                    zeroLineBorderDashOffset: 0,
                    offsetGridLines: !1,
                    borderDash: [],
                    borderDashOffset: 0
                },
                scaleLabel: {display: !1, labelString: "", padding: {top: 4, bottom: 4}},
                ticks: {
                    beginAtZero: !1,
                    minRotation: 0,
                    maxRotation: 50,
                    mirror: !1,
                    padding: 0,
                    reverse: !1,
                    display: !0,
                    autoSkip: !0,
                    autoSkipPadding: 0,
                    labelOffset: 0,
                    callback: xn.formatters.values,
                    minor: {},
                    major: {}
                }
            });
            var Mn = Jt.extend({
                getPadding: function () {
                    var t = this;
                    return {
                        left: t.paddingLeft || 0,
                        top: t.paddingTop || 0,
                        right: t.paddingRight || 0,
                        bottom: t.paddingBottom || 0
                    }
                },
                getTicks: function () {
                    return this._ticks
                },
                mergeTicksOptions: function () {
                    var t = this.options.ticks;
                    for (var e in!1 === t.minor && (t.minor = {display: !1}), !1 === t.major && (t.major = {display: !1}), t) "major" !== e && "minor" !== e && ("undefined" === typeof t.minor[e] && (t.minor[e] = t[e]), "undefined" === typeof t.major[e] && (t.major[e] = t[e]))
                },
                beforeUpdate: function () {
                    Ut.callback(this.options.beforeUpdate, [this])
                },
                update: function (t, e, a) {
                    var n, i, r, o, s, l, c = this;
                    for (c.beforeUpdate(), c.maxWidth = t, c.maxHeight = e, c.margins = Ut.extend({
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }, a), c._maxLabelLines = 0, c.longestLabelWidth = 0, c.longestTextCache = c.longestTextCache || {}, c.beforeSetDimensions(), c.setDimensions(), c.afterSetDimensions(), c.beforeDataLimits(), c.determineDataLimits(), c.afterDataLimits(), c.beforeBuildTicks(), s = c.buildTicks() || [], s = c.afterBuildTicks(s) || s, c.beforeTickToLabelConversion(), r = c.convertTicksToLabels(s) || c.ticks, c.afterTickToLabelConversion(), c.ticks = r, n = 0, i = r.length; n < i; ++n) o = r[n], l = s[n], l ? l.label = o : s.push(l = {
                        label: o,
                        major: !1
                    });
                    return c._ticks = s, c.beforeCalculateTickRotation(), c.calculateTickRotation(), c.afterCalculateTickRotation(), c.beforeFit(), c.fit(), c.afterFit(), c.afterUpdate(), c.minSize
                },
                afterUpdate: function () {
                    Ut.callback(this.options.afterUpdate, [this])
                },
                beforeSetDimensions: function () {
                    Ut.callback(this.options.beforeSetDimensions, [this])
                },
                setDimensions: function () {
                    var t = this;
                    t.isHorizontal() ? (t.width = t.maxWidth, t.left = 0, t.right = t.width) : (t.height = t.maxHeight, t.top = 0, t.bottom = t.height), t.paddingLeft = 0, t.paddingTop = 0, t.paddingRight = 0, t.paddingBottom = 0
                },
                afterSetDimensions: function () {
                    Ut.callback(this.options.afterSetDimensions, [this])
                },
                beforeDataLimits: function () {
                    Ut.callback(this.options.beforeDataLimits, [this])
                },
                determineDataLimits: Ut.noop,
                afterDataLimits: function () {
                    Ut.callback(this.options.afterDataLimits, [this])
                },
                beforeBuildTicks: function () {
                    Ut.callback(this.options.beforeBuildTicks, [this])
                },
                buildTicks: Ut.noop,
                afterBuildTicks: function (t) {
                    var e = this;
                    return Ut.isArray(t) && t.length ? Ut.callback(e.options.afterBuildTicks, [e, t]) : (e.ticks = Ut.callback(e.options.afterBuildTicks, [e, e.ticks]) || e.ticks, t)
                },
                beforeTickToLabelConversion: function () {
                    Ut.callback(this.options.beforeTickToLabelConversion, [this])
                },
                convertTicksToLabels: function () {
                    var t = this, e = t.options.ticks;
                    t.ticks = t.ticks.map(e.userCallback || e.callback, this)
                },
                afterTickToLabelConversion: function () {
                    Ut.callback(this.options.afterTickToLabelConversion, [this])
                },
                beforeCalculateTickRotation: function () {
                    Ut.callback(this.options.beforeCalculateTickRotation, [this])
                },
                calculateTickRotation: function () {
                    var t = this, e = t.ctx, a = t.options.ticks, n = kn(t._ticks), i = Ut.options._parseFont(a);
                    e.font = i.string;
                    var r = a.minRotation || 0;
                    if (n.length && t.options.display && t.isHorizontal()) {
                        var o, s, l = Ut.longestText(e, i.string, n, t.longestTextCache), c = l,
                            d = t.getPixelForTick(1) - t.getPixelForTick(0) - 6;
                        while (c > d && r < a.maxRotation) {
                            var u = Ut.toRadians(r);
                            if (o = Math.cos(u), s = Math.sin(u), s * l > t.maxHeight) {
                                r--;
                                break
                            }
                            r++, c = o * l
                        }
                    }
                    t.labelRotation = r
                },
                afterCalculateTickRotation: function () {
                    Ut.callback(this.options.afterCalculateTickRotation, [this])
                },
                beforeFit: function () {
                    Ut.callback(this.options.beforeFit, [this])
                },
                fit: function () {
                    var t = this, e = t.minSize = {width: 0, height: 0}, a = kn(t._ticks), n = t.options, i = n.ticks,
                        r = n.scaleLabel, o = n.gridLines, s = t._isVisible(), l = n.position, c = t.isHorizontal(),
                        d = Ut.options._parseFont, u = d(i), h = n.gridLines.tickMarkLength;
                    if (e.width = c ? t.isFullWidth() ? t.maxWidth - t.margins.left - t.margins.right : t.maxWidth : s && o.drawTicks ? h : 0, e.height = c ? s && o.drawTicks ? h : 0 : t.maxHeight, r.display && s) {
                        var f = d(r), p = Ut.options.toPadding(r.padding), g = f.lineHeight + p.height;
                        c ? e.height += g : e.width += g
                    }
                    if (i.display && s) {
                        var m = Ut.longestText(t.ctx, u.string, a, t.longestTextCache), v = Ut.numberOfLabelLines(a),
                            b = .5 * u.size, y = t.options.ticks.padding;
                        if (t._maxLabelLines = v, t.longestLabelWidth = m, c) {
                            var x = Ut.toRadians(t.labelRotation), _ = Math.cos(x), C = Math.sin(x),
                                k = C * m + u.lineHeight * v + b;
                            e.height = Math.min(t.maxHeight, e.height + k + y), t.ctx.font = u.string;
                            var w, A, M = An(t.ctx, a[0], u.string), S = An(t.ctx, a[a.length - 1], u.string),
                                D = t.getPixelForTick(0) - t.left, O = t.right - t.getPixelForTick(a.length - 1);
                            0 !== t.labelRotation ? (w = "bottom" === l ? _ * M : _ * b, A = "bottom" === l ? _ * b : _ * S) : (w = M / 2, A = S / 2), t.paddingLeft = Math.max(w - D, 0) + 3, t.paddingRight = Math.max(A - O, 0) + 3
                        } else i.mirror ? m = 0 : m += y + b, e.width = Math.min(t.maxWidth, e.width + m), t.paddingTop = u.size / 2, t.paddingBottom = u.size / 2
                    }
                    t.handleMargins(), t.width = e.width, t.height = e.height
                },
                handleMargins: function () {
                    var t = this;
                    t.margins && (t.paddingLeft = Math.max(t.paddingLeft - t.margins.left, 0), t.paddingTop = Math.max(t.paddingTop - t.margins.top, 0), t.paddingRight = Math.max(t.paddingRight - t.margins.right, 0), t.paddingBottom = Math.max(t.paddingBottom - t.margins.bottom, 0))
                },
                afterFit: function () {
                    Ut.callback(this.options.afterFit, [this])
                },
                isHorizontal: function () {
                    return "top" === this.options.position || "bottom" === this.options.position
                },
                isFullWidth: function () {
                    return this.options.fullWidth
                },
                getRightValue: function (t) {
                    if (Ut.isNullOrUndef(t)) return NaN;
                    if (("number" === typeof t || t instanceof Number) && !isFinite(t)) return NaN;
                    if (t) if (this.isHorizontal()) {
                        if (void 0 !== t.x) return this.getRightValue(t.x)
                    } else if (void 0 !== t.y) return this.getRightValue(t.y);
                    return t
                },
                getLabelForIndex: Ut.noop,
                getPixelForValue: Ut.noop,
                getValueForPixel: Ut.noop,
                getPixelForTick: function (t) {
                    var e = this, a = e.options.offset;
                    if (e.isHorizontal()) {
                        var n = e.width - (e.paddingLeft + e.paddingRight),
                            i = n / Math.max(e._ticks.length - (a ? 0 : 1), 1), r = i * t + e.paddingLeft;
                        a && (r += i / 2);
                        var o = e.left + r;
                        return o += e.isFullWidth() ? e.margins.left : 0, o
                    }
                    var s = e.height - (e.paddingTop + e.paddingBottom);
                    return e.top + t * (s / (e._ticks.length - 1))
                },
                getPixelForDecimal: function (t) {
                    var e = this;
                    if (e.isHorizontal()) {
                        var a = e.width - (e.paddingLeft + e.paddingRight), n = a * t + e.paddingLeft, i = e.left + n;
                        return i += e.isFullWidth() ? e.margins.left : 0, i
                    }
                    return e.top + t * e.height
                },
                getBasePixel: function () {
                    return this.getPixelForValue(this.getBaseValue())
                },
                getBaseValue: function () {
                    var t = this, e = t.min, a = t.max;
                    return t.beginAtZero ? 0 : e < 0 && a < 0 ? a : e > 0 && a > 0 ? e : 0
                },
                _autoSkip: function (t) {
                    var e, a, n = this, i = n.isHorizontal(), r = n.options.ticks.minor, o = t.length, s = !1,
                        l = r.maxTicksLimit, c = n._tickSize() * (o - 1),
                        d = i ? n.width - (n.paddingLeft + n.paddingRight) : n.height - (n.paddingTop + n.PaddingBottom),
                        u = [];
                    for (c > d && (s = 1 + Math.floor(c / d)), o > l && (s = Math.max(s, 1 + Math.floor(o / l))), e = 0; e < o; e++) a = t[e], s > 1 && e % s > 0 && delete a.label, u.push(a);
                    return u
                },
                _tickSize: function () {
                    var t = this, e = t.isHorizontal(), a = t.options.ticks.minor, n = Ut.toRadians(t.labelRotation),
                        i = Math.abs(Math.cos(n)), r = Math.abs(Math.sin(n)), o = a.autoSkipPadding || 0,
                        s = t.longestLabelWidth + o || 0, l = Ut.options._parseFont(a),
                        c = t._maxLabelLines * l.lineHeight + o || 0;
                    return e ? c * i > s * r ? s / i : c / r : c * r < s * i ? c / i : s / r
                },
                _isVisible: function () {
                    var t, e, a, n = this, i = n.chart, r = n.options.display;
                    if ("auto" !== r) return !!r;
                    for (t = 0, e = i.data.datasets.length; t < e; ++t) if (i.isDatasetVisible(t) && (a = i.getDatasetMeta(t), a.xAxisID === n.id || a.yAxisID === n.id)) return !0;
                    return !1
                },
                draw: function (t) {
                    var e = this, a = e.options;
                    if (e._isVisible()) {
                        var n, i, r, o = e.chart, s = e.ctx, l = $t.global, c = l.defaultFontColor, d = a.ticks.minor,
                            u = a.ticks.major || d, h = a.gridLines, f = a.scaleLabel, p = a.position,
                            g = 0 !== e.labelRotation, m = d.mirror, v = e.isHorizontal(), b = Ut.options._parseFont,
                            y = d.display && d.autoSkip ? e._autoSkip(e.getTicks()) : e.getTicks(),
                            x = _n(d.fontColor, c), _ = b(d), C = _.lineHeight, k = _n(u.fontColor, c), w = b(u),
                            A = d.padding, M = d.labelOffset, S = h.drawTicks ? h.tickMarkLength : 0,
                            D = _n(f.fontColor, c), O = b(f), P = Ut.options.toPadding(f.padding),
                            T = Ut.toRadians(e.labelRotation), I = [], R = h.drawBorder ? Cn(h.lineWidth, 0, 0) : 0,
                            F = Ut._alignPixel;
                        "top" === p ? (n = F(o, e.bottom, R), i = e.bottom - S, r = n - R / 2) : "bottom" === p ? (n = F(o, e.top, R), i = n + R / 2, r = e.top + S) : "left" === p ? (n = F(o, e.right, R), i = e.right - S, r = n - R / 2) : (n = F(o, e.left, R), i = n + R / 2, r = e.left + S);
                        var B = 1e-7;
                        if (Ut.each(y, function (n, s) {
                                if (!Ut.isNullOrUndef(n.label)) {
                                    var l, c, d, u, f, b, y, x, _, k, w, D, O, P, L, N, E = n.label;
                                    s === e.zeroLineIndex && a.offset === h.offsetGridLines ? (l = h.zeroLineWidth, c = h.zeroLineColor, d = h.zeroLineBorderDash || [], u = h.zeroLineBorderDashOffset || 0) : (l = Cn(h.lineWidth, s), c = Cn(h.color, s), d = h.borderDash || [], u = h.borderDashOffset || 0);
                                    var j = Ut.isArray(E) ? E.length : 1, H = wn(e, s, h.offsetGridLines);
                                    if (v) {
                                        var $ = S + A;
                                        H < e.left - B && (c = "rgba(0,0,0,0)"), f = y = _ = w = F(o, H, l), b = i, x = r, O = e.getPixelForTick(s) + M, "top" === p ? (k = F(o, t.top, R) + R / 2, D = t.bottom, L = ((g ? 1 : .5) - j) * C, N = g ? "left" : "center", P = e.bottom - $) : (k = t.top, D = F(o, t.bottom, R) - R / 2, L = (g ? 0 : .5) * C, N = g ? "right" : "center", P = e.top + $)
                                    } else {
                                        var z = (m ? 0 : S) + A;
                                        H < e.top - B && (c = "rgba(0,0,0,0)"), f = i, y = r, b = x = k = D = F(o, H, l), P = e.getPixelForTick(s) + M, L = (1 - j) * C / 2, "left" === p ? (_ = F(o, t.left, R) + R / 2, w = t.right, N = m ? "left" : "right", O = e.right - z) : (_ = t.left, w = F(o, t.right, R) - R / 2, N = m ? "right" : "left", O = e.left + z)
                                    }
                                    I.push({
                                        tx1: f,
                                        ty1: b,
                                        tx2: y,
                                        ty2: x,
                                        x1: _,
                                        y1: k,
                                        x2: w,
                                        y2: D,
                                        labelX: O,
                                        labelY: P,
                                        glWidth: l,
                                        glColor: c,
                                        glBorderDash: d,
                                        glBorderDashOffset: u,
                                        rotation: -1 * T,
                                        label: E,
                                        major: n.major,
                                        textOffset: L,
                                        textAlign: N
                                    })
                                }
                            }), Ut.each(I, function (t) {
                                var e = t.glWidth, a = t.glColor;
                                if (h.display && e && a && (s.save(), s.lineWidth = e, s.strokeStyle = a, s.setLineDash && (s.setLineDash(t.glBorderDash), s.lineDashOffset = t.glBorderDashOffset), s.beginPath(), h.drawTicks && (s.moveTo(t.tx1, t.ty1), s.lineTo(t.tx2, t.ty2)), h.drawOnChartArea && (s.moveTo(t.x1, t.y1), s.lineTo(t.x2, t.y2)), s.stroke(), s.restore()), d.display) {
                                    s.save(), s.translate(t.labelX, t.labelY), s.rotate(t.rotation), s.font = t.major ? w.string : _.string, s.fillStyle = t.major ? k : x, s.textBaseline = "middle", s.textAlign = t.textAlign;
                                    var n = t.label, i = t.textOffset;
                                    if (Ut.isArray(n)) for (var r = 0; r < n.length; ++r) s.fillText("" + n[r], 0, i), i += C; else s.fillText(n, 0, i);
                                    s.restore()
                                }
                            }), f.display) {
                            var L, N, E = 0, j = O.lineHeight / 2;
                            if (v) L = e.left + (e.right - e.left) / 2, N = "bottom" === p ? e.bottom - j - P.bottom : e.top + j + P.top; else {
                                var H = "left" === p;
                                L = H ? e.left + j + P.top : e.right - j - P.top, N = e.top + (e.bottom - e.top) / 2, E = H ? -.5 * Math.PI : .5 * Math.PI
                            }
                            s.save(), s.translate(L, N), s.rotate(E), s.textAlign = "center", s.textBaseline = "middle", s.fillStyle = D, s.font = O.string, s.fillText(f.labelString, 0, 0), s.restore()
                        }
                        if (R) {
                            var $, z, V, W, U = R, q = Cn(h.lineWidth, y.length - 1, 0);
                            v ? ($ = F(o, e.left, U) - U / 2, z = F(o, e.right, q) + q / 2, V = W = n) : (V = F(o, e.top, U) - U / 2, W = F(o, e.bottom, q) + q / 2, $ = z = n), s.lineWidth = R, s.strokeStyle = Cn(h.color, 0), s.beginPath(), s.moveTo($, V), s.lineTo(z, W), s.stroke()
                        }
                    }
                }
            }), Sn = {position: "bottom"}, Dn = Mn.extend({
                getLabels: function () {
                    var t = this.chart.data;
                    return this.options.labels || (this.isHorizontal() ? t.xLabels : t.yLabels) || t.labels
                }, determineDataLimits: function () {
                    var t, e = this, a = e.getLabels();
                    e.minIndex = 0, e.maxIndex = a.length - 1, void 0 !== e.options.ticks.min && (t = a.indexOf(e.options.ticks.min), e.minIndex = -1 !== t ? t : e.minIndex), void 0 !== e.options.ticks.max && (t = a.indexOf(e.options.ticks.max), e.maxIndex = -1 !== t ? t : e.maxIndex), e.min = a[e.minIndex], e.max = a[e.maxIndex]
                }, buildTicks: function () {
                    var t = this, e = t.getLabels();
                    t.ticks = 0 === t.minIndex && t.maxIndex === e.length - 1 ? e : e.slice(t.minIndex, t.maxIndex + 1)
                }, getLabelForIndex: function (t, e) {
                    var a = this, n = a.chart;
                    return n.getDatasetMeta(e).controller._getValueScaleId() === a.id ? a.getRightValue(n.data.datasets[e].data[t]) : a.ticks[t - a.minIndex]
                }, getPixelForValue: function (t, e) {
                    var a, n = this, i = n.options.offset, r = Math.max(n.maxIndex + 1 - n.minIndex - (i ? 0 : 1), 1);
                    if (void 0 !== t && null !== t && (a = n.isHorizontal() ? t.x : t.y), void 0 !== a || void 0 !== t && isNaN(e)) {
                        var o = n.getLabels();
                        t = a || t;
                        var s = o.indexOf(t);
                        e = -1 !== s ? s : e
                    }
                    if (n.isHorizontal()) {
                        var l = n.width / r, c = l * (e - n.minIndex);
                        return i && (c += l / 2), n.left + c
                    }
                    var d = n.height / r, u = d * (e - n.minIndex);
                    return i && (u += d / 2), n.top + u
                }, getPixelForTick: function (t) {
                    return this.getPixelForValue(this.ticks[t], t + this.minIndex, null)
                }, getValueForPixel: function (t) {
                    var e, a = this, n = a.options.offset, i = Math.max(a._ticks.length - (n ? 0 : 1), 1),
                        r = a.isHorizontal(), o = (r ? a.width : a.height) / i;
                    return t -= r ? a.left : a.top, n && (t -= o / 2), e = t <= 0 ? 0 : Math.round(t / o), e + a.minIndex
                }, getBasePixel: function () {
                    return this.bottom
                }
            }), On = Sn;
            Dn._defaults = On;
            var Pn = Ut.noop, Tn = Ut.isNullOrUndef;

            function In(t, e) {
                var a, n, i, r, o = [], s = 1e-14, l = t.stepSize, c = l || 1, d = t.maxTicks - 1, u = t.min, h = t.max,
                    f = t.precision, p = e.min, g = e.max, m = Ut.niceNum((g - p) / d / c) * c;
                if (m < s && Tn(u) && Tn(h)) return [p, g];
                r = Math.ceil(g / m) - Math.floor(p / m), r > d && (m = Ut.niceNum(r * m / d / c) * c), l || Tn(f) ? a = Math.pow(10, Ut._decimalPlaces(m)) : (a = Math.pow(10, f), m = Math.ceil(m * a) / a), n = Math.floor(p / m) * m, i = Math.ceil(g / m) * m, l && (!Tn(u) && Ut.almostWhole(u / m, m / 1e3) && (n = u), !Tn(h) && Ut.almostWhole(h / m, m / 1e3) && (i = h)), r = (i - n) / m, r = Ut.almostEquals(r, Math.round(r), m / 1e3) ? Math.round(r) : Math.ceil(r), n = Math.round(n * a) / a, i = Math.round(i * a) / a, o.push(Tn(u) ? n : u);
                for (var v = 1; v < r; ++v) o.push(Math.round((n + v * m) * a) / a);
                return o.push(Tn(h) ? i : h), o
            }

            var Rn = Mn.extend({
                getRightValue: function (t) {
                    return "string" === typeof t ? +t : Mn.prototype.getRightValue.call(this, t)
                }, handleTickRangeOptions: function () {
                    var t = this, e = t.options, a = e.ticks;
                    if (a.beginAtZero) {
                        var n = Ut.sign(t.min), i = Ut.sign(t.max);
                        n < 0 && i < 0 ? t.max = 0 : n > 0 && i > 0 && (t.min = 0)
                    }
                    var r = void 0 !== a.min || void 0 !== a.suggestedMin,
                        o = void 0 !== a.max || void 0 !== a.suggestedMax;
                    void 0 !== a.min ? t.min = a.min : void 0 !== a.suggestedMin && (null === t.min ? t.min = a.suggestedMin : t.min = Math.min(t.min, a.suggestedMin)), void 0 !== a.max ? t.max = a.max : void 0 !== a.suggestedMax && (null === t.max ? t.max = a.suggestedMax : t.max = Math.max(t.max, a.suggestedMax)), r !== o && t.min >= t.max && (r ? t.max = t.min + 1 : t.min = t.max - 1), t.min === t.max && (t.max++, a.beginAtZero || t.min--)
                }, getTickLimit: function () {
                    var t, e = this, a = e.options.ticks, n = a.stepSize, i = a.maxTicksLimit;
                    return n ? t = Math.ceil(e.max / n) - Math.floor(e.min / n) + 1 : (t = e._computeTickLimit(), i = i || 11), i && (t = Math.min(i, t)), t
                }, _computeTickLimit: function () {
                    return Number.POSITIVE_INFINITY
                }, handleDirectionalChanges: Pn, buildTicks: function () {
                    var t = this, e = t.options, a = e.ticks, n = t.getTickLimit();
                    n = Math.max(2, n);
                    var i = {
                        maxTicks: n,
                        min: a.min,
                        max: a.max,
                        precision: a.precision,
                        stepSize: Ut.valueOrDefault(a.fixedStepSize, a.stepSize)
                    }, r = t.ticks = In(i, t);
                    t.handleDirectionalChanges(), t.max = Ut.max(r), t.min = Ut.min(r), a.reverse ? (r.reverse(), t.start = t.max, t.end = t.min) : (t.start = t.min, t.end = t.max)
                }, convertTicksToLabels: function () {
                    var t = this;
                    t.ticksAsNumbers = t.ticks.slice(), t.zeroLineIndex = t.ticks.indexOf(0), Mn.prototype.convertTicksToLabels.call(t)
                }
            }), Fn = {position: "left", ticks: {callback: xn.formatters.linear}}, Bn = Rn.extend({
                determineDataLimits: function () {
                    var t = this, e = t.options, a = t.chart, n = a.data, i = n.datasets, r = t.isHorizontal(), o = 0,
                        s = 1;

                    function l(e) {
                        return r ? e.xAxisID === t.id : e.yAxisID === t.id
                    }

                    t.min = null, t.max = null;
                    var c = e.stacked;
                    if (void 0 === c && Ut.each(i, function (t, e) {
                            if (!c) {
                                var n = a.getDatasetMeta(e);
                                a.isDatasetVisible(e) && l(n) && void 0 !== n.stack && (c = !0)
                            }
                        }), e.stacked || c) {
                        var d = {};
                        Ut.each(i, function (n, i) {
                            var r = a.getDatasetMeta(i),
                                o = [r.type, void 0 === e.stacked && void 0 === r.stack ? i : "", r.stack].join(".");
                            void 0 === d[o] && (d[o] = {positiveValues: [], negativeValues: []});
                            var s = d[o].positiveValues, c = d[o].negativeValues;
                            a.isDatasetVisible(i) && l(r) && Ut.each(n.data, function (a, n) {
                                var i = +t.getRightValue(a);
                                isNaN(i) || r.data[n].hidden || (s[n] = s[n] || 0, c[n] = c[n] || 0, e.relativePoints ? s[n] = 100 : i < 0 ? c[n] += i : s[n] += i)
                            })
                        }), Ut.each(d, function (e) {
                            var a = e.positiveValues.concat(e.negativeValues), n = Ut.min(a), i = Ut.max(a);
                            t.min = null === t.min ? n : Math.min(t.min, n), t.max = null === t.max ? i : Math.max(t.max, i)
                        })
                    } else Ut.each(i, function (e, n) {
                        var i = a.getDatasetMeta(n);
                        a.isDatasetVisible(n) && l(i) && Ut.each(e.data, function (e, a) {
                            var n = +t.getRightValue(e);
                            isNaN(n) || i.data[a].hidden || (null === t.min ? t.min = n : n < t.min && (t.min = n), null === t.max ? t.max = n : n > t.max && (t.max = n))
                        })
                    });
                    t.min = isFinite(t.min) && !isNaN(t.min) ? t.min : o, t.max = isFinite(t.max) && !isNaN(t.max) ? t.max : s, this.handleTickRangeOptions()
                }, _computeTickLimit: function () {
                    var t, e = this;
                    return e.isHorizontal() ? Math.ceil(e.width / 40) : (t = Ut.options._parseFont(e.options.ticks), Math.ceil(e.height / t.lineHeight))
                }, handleDirectionalChanges: function () {
                    this.isHorizontal() || this.ticks.reverse()
                }, getLabelForIndex: function (t, e) {
                    return +this.getRightValue(this.chart.data.datasets[e].data[t])
                }, getPixelForValue: function (t) {
                    var e, a = this, n = a.start, i = +a.getRightValue(t), r = a.end - n;
                    return e = a.isHorizontal() ? a.left + a.width / r * (i - n) : a.bottom - a.height / r * (i - n), e
                }, getValueForPixel: function (t) {
                    var e = this, a = e.isHorizontal(), n = a ? e.width : e.height,
                        i = (a ? t - e.left : e.bottom - t) / n;
                    return e.start + (e.end - e.start) * i
                }, getPixelForTick: function (t) {
                    return this.getPixelForValue(this.ticksAsNumbers[t])
                }
            }), Ln = Fn;
            Bn._defaults = Ln;
            var Nn = Ut.valueOrDefault;

            function En(t, e) {
                var a, n, i = [], r = Nn(t.min, Math.pow(10, Math.floor(Ut.log10(e.min)))),
                    o = Math.floor(Ut.log10(e.max)), s = Math.ceil(e.max / Math.pow(10, o));
                0 === r ? (a = Math.floor(Ut.log10(e.minNotZero)), n = Math.floor(e.minNotZero / Math.pow(10, a)), i.push(r), r = n * Math.pow(10, a)) : (a = Math.floor(Ut.log10(r)), n = Math.floor(r / Math.pow(10, a)));
                var l = a < 0 ? Math.pow(10, Math.abs(a)) : 1;
                do {
                    i.push(r), ++n, 10 === n && (n = 1, ++a, l = a >= 0 ? 1 : l), r = Math.round(n * Math.pow(10, a) * l) / l
                } while (a < o || a === o && n < s);
                var c = Nn(t.max, r);
                return i.push(c), i
            }

            var jn = {position: "left", ticks: {callback: xn.formatters.logarithmic}};

            function Hn(t, e) {
                return Ut.isFinite(t) && t >= 0 ? t : e
            }

            var $n = Mn.extend({
                determineDataLimits: function () {
                    var t = this, e = t.options, a = t.chart, n = a.data, i = n.datasets, r = t.isHorizontal();

                    function o(e) {
                        return r ? e.xAxisID === t.id : e.yAxisID === t.id
                    }

                    t.min = null, t.max = null, t.minNotZero = null;
                    var s = e.stacked;
                    if (void 0 === s && Ut.each(i, function (t, e) {
                            if (!s) {
                                var n = a.getDatasetMeta(e);
                                a.isDatasetVisible(e) && o(n) && void 0 !== n.stack && (s = !0)
                            }
                        }), e.stacked || s) {
                        var l = {};
                        Ut.each(i, function (n, i) {
                            var r = a.getDatasetMeta(i),
                                s = [r.type, void 0 === e.stacked && void 0 === r.stack ? i : "", r.stack].join(".");
                            a.isDatasetVisible(i) && o(r) && (void 0 === l[s] && (l[s] = []), Ut.each(n.data, function (e, a) {
                                var n = l[s], i = +t.getRightValue(e);
                                isNaN(i) || r.data[a].hidden || i < 0 || (n[a] = n[a] || 0, n[a] += i)
                            }))
                        }), Ut.each(l, function (e) {
                            if (e.length > 0) {
                                var a = Ut.min(e), n = Ut.max(e);
                                t.min = null === t.min ? a : Math.min(t.min, a), t.max = null === t.max ? n : Math.max(t.max, n)
                            }
                        })
                    } else Ut.each(i, function (e, n) {
                        var i = a.getDatasetMeta(n);
                        a.isDatasetVisible(n) && o(i) && Ut.each(e.data, function (e, a) {
                            var n = +t.getRightValue(e);
                            isNaN(n) || i.data[a].hidden || n < 0 || (null === t.min ? t.min = n : n < t.min && (t.min = n), null === t.max ? t.max = n : n > t.max && (t.max = n), 0 !== n && (null === t.minNotZero || n < t.minNotZero) && (t.minNotZero = n))
                        })
                    });
                    this.handleTickRangeOptions()
                }, handleTickRangeOptions: function () {
                    var t = this, e = t.options.ticks, a = 1, n = 10;
                    t.min = Hn(e.min, t.min), t.max = Hn(e.max, t.max), t.min === t.max && (0 !== t.min && null !== t.min ? (t.min = Math.pow(10, Math.floor(Ut.log10(t.min)) - 1), t.max = Math.pow(10, Math.floor(Ut.log10(t.max)) + 1)) : (t.min = a, t.max = n)), null === t.min && (t.min = Math.pow(10, Math.floor(Ut.log10(t.max)) - 1)), null === t.max && (t.max = 0 !== t.min ? Math.pow(10, Math.floor(Ut.log10(t.min)) + 1) : n), null === t.minNotZero && (t.min > 0 ? t.minNotZero = t.min : t.max < 1 ? t.minNotZero = Math.pow(10, Math.floor(Ut.log10(t.max))) : t.minNotZero = a)
                }, buildTicks: function () {
                    var t = this, e = t.options.ticks, a = !t.isHorizontal(), n = {min: Hn(e.min), max: Hn(e.max)},
                        i = t.ticks = En(n, t);
                    t.max = Ut.max(i), t.min = Ut.min(i), e.reverse ? (a = !a, t.start = t.max, t.end = t.min) : (t.start = t.min, t.end = t.max), a && i.reverse()
                }, convertTicksToLabels: function () {
                    this.tickValues = this.ticks.slice(), Mn.prototype.convertTicksToLabels.call(this)
                }, getLabelForIndex: function (t, e) {
                    return +this.getRightValue(this.chart.data.datasets[e].data[t])
                }, getPixelForTick: function (t) {
                    return this.getPixelForValue(this.tickValues[t])
                }, _getFirstTickValue: function (t) {
                    var e = Math.floor(Ut.log10(t)), a = Math.floor(t / Math.pow(10, e));
                    return a * Math.pow(10, e)
                }, getPixelForValue: function (t) {
                    var e, a, n, i, r, o = this, s = o.options.ticks, l = s.reverse, c = Ut.log10,
                        d = o._getFirstTickValue(o.minNotZero), u = 0;
                    return t = +o.getRightValue(t), l ? (n = o.end, i = o.start, r = -1) : (n = o.start, i = o.end, r = 1), o.isHorizontal() ? (e = o.width, a = l ? o.right : o.left) : (e = o.height, r *= -1, a = l ? o.top : o.bottom), t !== n && (0 === n && (u = Nn(s.fontSize, $t.global.defaultFontSize), e -= u, n = d), 0 !== t && (u += e / (c(i) - c(n)) * (c(t) - c(n))), a += r * u), a
                }, getValueForPixel: function (t) {
                    var e, a, n, i, r = this, o = r.options.ticks, s = o.reverse, l = Ut.log10,
                        c = r._getFirstTickValue(r.minNotZero);
                    if (s ? (a = r.end, n = r.start) : (a = r.start, n = r.end), r.isHorizontal() ? (e = r.width, i = s ? r.right - t : t - r.left) : (e = r.height, i = s ? t - r.top : r.bottom - t), i !== a) {
                        if (0 === a) {
                            var d = Nn(o.fontSize, $t.global.defaultFontSize);
                            i -= d, e -= d, a = c
                        }
                        i *= l(n) - l(a), i /= e, i = Math.pow(10, l(a) + i)
                    }
                    return i
                }
            }), zn = jn;
            $n._defaults = zn;
            var Vn = Ut.valueOrDefault, Wn = Ut.valueAtIndexOrDefault, Un = Ut.options.resolve, qn = {
                display: !0,
                animate: !0,
                position: "chartArea",
                angleLines: {
                    display: !0,
                    color: "rgba(0, 0, 0, 0.1)",
                    lineWidth: 1,
                    borderDash: [],
                    borderDashOffset: 0
                },
                gridLines: {circular: !1},
                ticks: {
                    showLabelBackdrop: !0,
                    backdropColor: "rgba(255,255,255,0.75)",
                    backdropPaddingY: 2,
                    backdropPaddingX: 2,
                    callback: xn.formatters.linear
                },
                pointLabels: {
                    display: !0, fontSize: 10, callback: function (t) {
                        return t
                    }
                }
            };

            function Yn(t) {
                var e = t.options;
                return e.angleLines.display || e.pointLabels.display ? t.chart.data.labels.length : 0
            }

            function Gn(t) {
                var e = t.ticks;
                return e.display && t.display ? Vn(e.fontSize, $t.global.defaultFontSize) + 2 * e.backdropPaddingY : 0
            }

            function Kn(t, e, a) {
                return Ut.isArray(a) ? {w: Ut.longestText(t, t.font, a), h: a.length * e} : {
                    w: t.measureText(a).width,
                    h: e
                }
            }

            function Xn(t, e, a, n, i) {
                return t === n || t === i ? {start: e - a / 2, end: e + a / 2} : t < n || t > i ? {
                    start: e - a,
                    end: e
                } : {start: e, end: e + a}
            }

            function Jn(t) {
                var e, a, n, i = Ut.options._parseFont(t.options.pointLabels),
                    r = {l: 0, r: t.width, t: 0, b: t.height - t.paddingTop}, o = {};
                t.ctx.font = i.string, t._pointLabelSizes = [];
                var s = Yn(t);
                for (e = 0; e < s; e++) {
                    n = t.getPointPosition(e, t.drawingArea + 5), a = Kn(t.ctx, i.lineHeight, t.pointLabels[e] || ""), t._pointLabelSizes[e] = a;
                    var l = t.getIndexAngle(e), c = Ut.toDegrees(l) % 360, d = Xn(c, n.x, a.w, 0, 180),
                        u = Xn(c, n.y, a.h, 90, 270);
                    d.start < r.l && (r.l = d.start, o.l = l), d.end > r.r && (r.r = d.end, o.r = l), u.start < r.t && (r.t = u.start, o.t = l), u.end > r.b && (r.b = u.end, o.b = l)
                }
                t.setReductions(t.drawingArea, r, o)
            }

            function Zn(t) {
                return 0 === t || 180 === t ? "center" : t < 180 ? "left" : "right"
            }

            function Qn(t, e, a, n) {
                var i, r, o = a.y + n / 2;
                if (Ut.isArray(e)) for (i = 0, r = e.length; i < r; ++i) t.fillText(e[i], a.x, o), o += n; else t.fillText(e, a.x, o)
            }

            function ti(t, e, a) {
                90 === t || 270 === t ? a.y -= e.h / 2 : (t > 270 || t < 90) && (a.y -= e.h)
            }

            function ei(t) {
                var e = t.ctx, a = t.options, n = a.angleLines, i = a.gridLines, r = a.pointLabels,
                    o = Vn(n.lineWidth, i.lineWidth), s = Vn(n.color, i.color), l = Gn(a);
                e.save(), e.lineWidth = o, e.strokeStyle = s, e.setLineDash && (e.setLineDash(Un([n.borderDash, i.borderDash, []])), e.lineDashOffset = Un([n.borderDashOffset, i.borderDashOffset, 0]));
                var c = t.getDistanceFromCenterForValue(a.ticks.reverse ? t.min : t.max), d = Ut.options._parseFont(r);
                e.font = d.string, e.textBaseline = "middle";
                for (var u = Yn(t) - 1; u >= 0; u--) {
                    if (n.display && o && s) {
                        var h = t.getPointPosition(u, c);
                        e.beginPath(), e.moveTo(t.xCenter, t.yCenter), e.lineTo(h.x, h.y), e.stroke()
                    }
                    if (r.display) {
                        var f = 0 === u ? l / 2 : 0, p = t.getPointPosition(u, c + f + 5),
                            g = Wn(r.fontColor, u, $t.global.defaultFontColor);
                        e.fillStyle = g;
                        var m = t.getIndexAngle(u), v = Ut.toDegrees(m);
                        e.textAlign = Zn(v), ti(v, t._pointLabelSizes[u], p), Qn(e, t.pointLabels[u] || "", p, d.lineHeight)
                    }
                }
                e.restore()
            }

            function ai(t, e, a, n) {
                var i, r = t.ctx, o = e.circular, s = Yn(t), l = Wn(e.color, n - 1), c = Wn(e.lineWidth, n - 1);
                if ((o || s) && l && c) {
                    if (r.save(), r.strokeStyle = l, r.lineWidth = c, r.setLineDash && (r.setLineDash(e.borderDash || []), r.lineDashOffset = e.borderDashOffset || 0), r.beginPath(), o) r.arc(t.xCenter, t.yCenter, a, 0, 2 * Math.PI); else {
                        i = t.getPointPosition(0, a), r.moveTo(i.x, i.y);
                        for (var d = 1; d < s; d++) i = t.getPointPosition(d, a), r.lineTo(i.x, i.y)
                    }
                    r.closePath(), r.stroke(), r.restore()
                }
            }

            function ni(t) {
                return Ut.isNumber(t) ? t : 0
            }

            var ii = Rn.extend({
                setDimensions: function () {
                    var t = this;
                    t.width = t.maxWidth, t.height = t.maxHeight, t.paddingTop = Gn(t.options) / 2, t.xCenter = Math.floor(t.width / 2), t.yCenter = Math.floor((t.height - t.paddingTop) / 2), t.drawingArea = Math.min(t.height - t.paddingTop, t.width) / 2
                }, determineDataLimits: function () {
                    var t = this, e = t.chart, a = Number.POSITIVE_INFINITY, n = Number.NEGATIVE_INFINITY;
                    Ut.each(e.data.datasets, function (i, r) {
                        if (e.isDatasetVisible(r)) {
                            var o = e.getDatasetMeta(r);
                            Ut.each(i.data, function (e, i) {
                                var r = +t.getRightValue(e);
                                isNaN(r) || o.data[i].hidden || (a = Math.min(r, a), n = Math.max(r, n))
                            })
                        }
                    }), t.min = a === Number.POSITIVE_INFINITY ? 0 : a, t.max = n === Number.NEGATIVE_INFINITY ? 0 : n, t.handleTickRangeOptions()
                }, _computeTickLimit: function () {
                    return Math.ceil(this.drawingArea / Gn(this.options))
                }, convertTicksToLabels: function () {
                    var t = this;
                    Rn.prototype.convertTicksToLabels.call(t), t.pointLabels = t.chart.data.labels.map(t.options.pointLabels.callback, t)
                }, getLabelForIndex: function (t, e) {
                    return +this.getRightValue(this.chart.data.datasets[e].data[t])
                }, fit: function () {
                    var t = this, e = t.options;
                    e.display && e.pointLabels.display ? Jn(t) : t.setCenterPoint(0, 0, 0, 0)
                }, setReductions: function (t, e, a) {
                    var n = this, i = e.l / Math.sin(a.l), r = Math.max(e.r - n.width, 0) / Math.sin(a.r),
                        o = -e.t / Math.cos(a.t), s = -Math.max(e.b - (n.height - n.paddingTop), 0) / Math.cos(a.b);
                    i = ni(i), r = ni(r), o = ni(o), s = ni(s), n.drawingArea = Math.min(Math.floor(t - (i + r) / 2), Math.floor(t - (o + s) / 2)), n.setCenterPoint(i, r, o, s)
                }, setCenterPoint: function (t, e, a, n) {
                    var i = this, r = i.width - e - i.drawingArea, o = t + i.drawingArea, s = a + i.drawingArea,
                        l = i.height - i.paddingTop - n - i.drawingArea;
                    i.xCenter = Math.floor((o + r) / 2 + i.left), i.yCenter = Math.floor((s + l) / 2 + i.top + i.paddingTop)
                }, getIndexAngle: function (t) {
                    var e = 2 * Math.PI / Yn(this),
                        a = this.chart.options && this.chart.options.startAngle ? this.chart.options.startAngle : 0,
                        n = a * Math.PI * 2 / 360;
                    return t * e + n
                }, getDistanceFromCenterForValue: function (t) {
                    var e = this;
                    if (null === t) return 0;
                    var a = e.drawingArea / (e.max - e.min);
                    return e.options.ticks.reverse ? (e.max - t) * a : (t - e.min) * a
                }, getPointPosition: function (t, e) {
                    var a = this, n = a.getIndexAngle(t) - Math.PI / 2;
                    return {x: Math.cos(n) * e + a.xCenter, y: Math.sin(n) * e + a.yCenter}
                }, getPointPositionForValue: function (t, e) {
                    return this.getPointPosition(t, this.getDistanceFromCenterForValue(e))
                }, getBasePosition: function () {
                    var t = this, e = t.min, a = t.max;
                    return t.getPointPositionForValue(0, t.beginAtZero ? 0 : e < 0 && a < 0 ? a : e > 0 && a > 0 ? e : 0)
                }, draw: function () {
                    var t = this, e = t.options, a = e.gridLines, n = e.ticks;
                    if (e.display) {
                        var i = t.ctx, r = this.getIndexAngle(0), o = Ut.options._parseFont(n);
                        (e.angleLines.display || e.pointLabels.display) && ei(t), Ut.each(t.ticks, function (e, s) {
                            if (s > 0 || n.reverse) {
                                var l = t.getDistanceFromCenterForValue(t.ticksAsNumbers[s]);
                                if (a.display && 0 !== s && ai(t, a, l, s), n.display) {
                                    var c = Vn(n.fontColor, $t.global.defaultFontColor);
                                    if (i.font = o.string, i.save(), i.translate(t.xCenter, t.yCenter), i.rotate(r), n.showLabelBackdrop) {
                                        var d = i.measureText(e).width;
                                        i.fillStyle = n.backdropColor, i.fillRect(-d / 2 - n.backdropPaddingX, -l - o.size / 2 - n.backdropPaddingY, d + 2 * n.backdropPaddingX, o.size + 2 * n.backdropPaddingY)
                                    }
                                    i.textAlign = "center", i.textBaseline = "middle", i.fillStyle = c, i.fillText(e, 0, -l), i.restore()
                                }
                            }
                        })
                    }
                }
            }), ri = qn;
            ii._defaults = ri;
            var oi = Ut.valueOrDefault, si = Number.MIN_SAFE_INTEGER || -9007199254740991,
                li = Number.MAX_SAFE_INTEGER || 9007199254740991, ci = {
                    millisecond: {common: !0, size: 1, steps: [1, 2, 5, 10, 20, 50, 100, 250, 500]},
                    second: {common: !0, size: 1e3, steps: [1, 2, 5, 10, 15, 30]},
                    minute: {common: !0, size: 6e4, steps: [1, 2, 5, 10, 15, 30]},
                    hour: {common: !0, size: 36e5, steps: [1, 2, 3, 6, 12]},
                    day: {common: !0, size: 864e5, steps: [1, 2, 5]},
                    week: {common: !1, size: 6048e5, steps: [1, 2, 3, 4]},
                    month: {common: !0, size: 2628e6, steps: [1, 2, 3]},
                    quarter: {common: !1, size: 7884e6, steps: [1, 2, 3, 4]},
                    year: {common: !0, size: 3154e7}
                }, di = Object.keys(ci);

            function ui(t, e) {
                return t - e
            }

            function hi(t) {
                var e, a, n, i = {}, r = [];
                for (e = 0, a = t.length; e < a; ++e) n = t[e], i[n] || (i[n] = !0, r.push(n));
                return r
            }

            function fi(t, e, a, n) {
                if ("linear" === n || !t.length) return [{time: e, pos: 0}, {time: a, pos: 1}];
                var i, r, o, s, l, c = [], d = [e];
                for (i = 0, r = t.length; i < r; ++i) s = t[i], s > e && s < a && d.push(s);
                for (d.push(a), i = 0, r = d.length; i < r; ++i) l = d[i + 1], o = d[i - 1], s = d[i], void 0 !== o && void 0 !== l && Math.round((l + o) / 2) === s || c.push({
                    time: s,
                    pos: i / (r - 1)
                });
                return c
            }

            function pi(t, e, a) {
                var n, i, r, o = 0, s = t.length - 1;
                while (o >= 0 && o <= s) {
                    if (n = o + s >> 1, i = t[n - 1] || null, r = t[n], !i) return {lo: null, hi: r};
                    if (r[e] < a) o = n + 1; else {
                        if (!(i[e] > a)) return {lo: i, hi: r};
                        s = n - 1
                    }
                }
                return {lo: r, hi: null}
            }

            function gi(t, e, a, n) {
                var i = pi(t, e, a), r = i.lo ? i.hi ? i.lo : t[t.length - 2] : t[0],
                    o = i.lo ? i.hi ? i.hi : t[t.length - 1] : t[1], s = o[e] - r[e], l = s ? (a - r[e]) / s : 0,
                    c = (o[n] - r[n]) * l;
                return r[n] + c
            }

            function mi(t, e) {
                var a = t._adapter, n = t.options.time, i = n.parser, r = i || n.format, o = e;
                return "function" === typeof i && (o = i(o)), Ut.isFinite(o) || (o = "string" === typeof r ? a.parse(o, r) : a.parse(o)), null !== o ? +o : (i || "function" !== typeof r || (o = r(e), Ut.isFinite(o) || (o = a.parse(o))), o)
            }

            function vi(t, e) {
                if (Ut.isNullOrUndef(e)) return null;
                var a = t.options.time, n = mi(t, t.getRightValue(e));
                return null === n ? n : (a.round && (n = +t._adapter.startOf(n, a.round)), n)
            }

            function bi(t, e, a, n) {
                var i, r, o, s = e - t, l = ci[a], c = l.size, d = l.steps;
                if (!d) return Math.ceil(s / (n * c));
                for (i = 0, r = d.length; i < r; ++i) if (o = d[i], Math.ceil(s / (c * o)) <= n) break;
                return o
            }

            function yi(t, e, a, n) {
                var i, r, o, s = di.length;
                for (i = di.indexOf(t); i < s - 1; ++i) if (r = ci[di[i]], o = r.steps ? r.steps[r.steps.length - 1] : li, r.common && Math.ceil((a - e) / (o * r.size)) <= n) return di[i];
                return di[s - 1]
            }

            function xi(t, e, a, n, i) {
                var r, o, s = di.length;
                for (r = s - 1; r >= di.indexOf(a); r--) if (o = di[r], ci[o].common && t._adapter.diff(i, n, o) >= e.length) return o;
                return di[a ? di.indexOf(a) : 0]
            }

            function _i(t) {
                for (var e = di.indexOf(t) + 1, a = di.length; e < a; ++e) if (ci[di[e]].common) return di[e]
            }

            function Ci(t, e, a, n) {
                var i, r = t._adapter, o = t.options, s = o.time, l = s.unit || yi(s.minUnit, e, a, n), c = _i(l),
                    d = oi(s.stepSize, s.unitStepSize), u = "week" === l && s.isoWeekday, h = o.ticks.major.enabled,
                    f = ci[l], p = e, g = a, m = [];
                for (d || (d = bi(e, a, l, n)), u && (p = +r.startOf(p, "isoWeek", u), g = +r.startOf(g, "isoWeek", u)), p = +r.startOf(p, u ? "day" : l), g = +r.startOf(g, u ? "day" : l), g < a && (g = +r.add(g, 1, l)), i = p, h && c && !u && !s.round && (i = +r.startOf(i, c), i = +r.add(i, ~~((p - i) / (f.size * d)) * d, l)); i < g; i = +r.add(i, d, l)) m.push(+i);
                return m.push(+i), m
            }

            function ki(t, e, a, n, i) {
                var r, o, s = 0, l = 0;
                return i.offset && e.length && (i.time.min || (r = gi(t, "time", e[0], "pos"), s = 1 === e.length ? 1 - r : (gi(t, "time", e[1], "pos") - r) / 2), i.time.max || (o = gi(t, "time", e[e.length - 1], "pos"), l = 1 === e.length ? o : (o - gi(t, "time", e[e.length - 2], "pos")) / 2)), {
                    start: s,
                    end: l
                }
            }

            function wi(t, e, a) {
                var n, i, r, o, s = [];
                for (n = 0, i = e.length; n < i; ++n) r = e[n], o = !!a && r === +t._adapter.startOf(r, a), s.push({
                    value: r,
                    major: o
                });
                return s
            }

            var Ai = {
                position: "bottom",
                distribution: "linear",
                bounds: "data",
                adapters: {},
                time: {
                    parser: !1,
                    format: !1,
                    unit: !1,
                    round: !1,
                    displayFormat: !1,
                    isoWeekday: !1,
                    minUnit: "millisecond",
                    displayFormats: {}
                },
                ticks: {autoSkip: !1, source: "auto", major: {enabled: !1}}
            }, Mi = Mn.extend({
                initialize: function () {
                    this.mergeTicksOptions(), Mn.prototype.initialize.call(this)
                }, update: function () {
                    var t = this, e = t.options, a = e.time || (e.time = {}),
                        n = t._adapter = new yn._date(e.adapters.date);
                    return a.format && console.warn("options.time.format is deprecated and replaced by options.time.parser."), Ut.mergeIf(a.displayFormats, n.formats()), Mn.prototype.update.apply(t, arguments)
                }, getRightValue: function (t) {
                    return t && void 0 !== t.t && (t = t.t), Mn.prototype.getRightValue.call(this, t)
                }, determineDataLimits: function () {
                    var t, e, a, n, i, r, o = this, s = o.chart, l = o._adapter, c = o.options.time,
                        d = c.unit || "day", u = li, h = si, f = [], p = [], g = [], m = s.data.labels || [];
                    for (t = 0, a = m.length; t < a; ++t) g.push(vi(o, m[t]));
                    for (t = 0, a = (s.data.datasets || []).length; t < a; ++t) if (s.isDatasetVisible(t)) if (i = s.data.datasets[t].data, Ut.isObject(i[0])) for (p[t] = [], e = 0, n = i.length; e < n; ++e) r = vi(o, i[e]), f.push(r), p[t][e] = r; else {
                        for (e = 0, n = g.length; e < n; ++e) f.push(g[e]);
                        p[t] = g.slice(0)
                    } else p[t] = [];
                    g.length && (g = hi(g).sort(ui), u = Math.min(u, g[0]), h = Math.max(h, g[g.length - 1])), f.length && (f = hi(f).sort(ui), u = Math.min(u, f[0]), h = Math.max(h, f[f.length - 1])), u = vi(o, c.min) || u, h = vi(o, c.max) || h, u = u === li ? +l.startOf(Date.now(), d) : u, h = h === si ? +l.endOf(Date.now(), d) + 1 : h, o.min = Math.min(u, h), o.max = Math.max(u + 1, h), o._horizontal = o.isHorizontal(), o._table = [], o._timestamps = {
                        data: f,
                        datasets: p,
                        labels: g
                    }
                }, buildTicks: function () {
                    var t, e, a, n = this, i = n.min, r = n.max, o = n.options, s = o.time, l = [], c = [];
                    switch (o.ticks.source) {
                        case"data":
                            l = n._timestamps.data;
                            break;
                        case"labels":
                            l = n._timestamps.labels;
                            break;
                        case"auto":
                        default:
                            l = Ci(n, i, r, n.getLabelCapacity(i), o)
                    }
                    for ("ticks" === o.bounds && l.length && (i = l[0], r = l[l.length - 1]), i = vi(n, s.min) || i, r = vi(n, s.max) || r, t = 0, e = l.length; t < e; ++t) a = l[t], a >= i && a <= r && c.push(a);
                    return n.min = i, n.max = r, n._unit = s.unit || xi(n, c, s.minUnit, n.min, n.max), n._majorUnit = _i(n._unit), n._table = fi(n._timestamps.data, i, r, o.distribution), n._offsets = ki(n._table, c, i, r, o), o.ticks.reverse && c.reverse(), wi(n, c, n._majorUnit)
                }, getLabelForIndex: function (t, e) {
                    var a = this, n = a._adapter, i = a.chart.data, r = a.options.time,
                        o = i.labels && t < i.labels.length ? i.labels[t] : "", s = i.datasets[e].data[t];
                    return Ut.isObject(s) && (o = a.getRightValue(s)), r.tooltipFormat ? n.format(mi(a, o), r.tooltipFormat) : "string" === typeof o ? o : n.format(mi(a, o), r.displayFormats.datetime)
                }, tickFormatFunction: function (t, e, a, n) {
                    var i = this, r = i._adapter, o = i.options, s = o.time.displayFormats, l = s[i._unit],
                        c = i._majorUnit, d = s[c], u = +r.startOf(t, c), h = o.ticks.major,
                        f = h.enabled && c && d && t === u, p = r.format(t, n || (f ? d : l)),
                        g = f ? h : o.ticks.minor, m = oi(g.callback, g.userCallback);
                    return m ? m(p, e, a) : p
                }, convertTicksToLabels: function (t) {
                    var e, a, n = [];
                    for (e = 0, a = t.length; e < a; ++e) n.push(this.tickFormatFunction(t[e].value, e, t));
                    return n
                }, getPixelForOffset: function (t) {
                    var e = this, a = e.options.ticks.reverse, n = e._horizontal ? e.width : e.height,
                        i = e._horizontal ? a ? e.right : e.left : a ? e.bottom : e.top,
                        r = gi(e._table, "time", t, "pos"),
                        o = n * (e._offsets.start + r) / (e._offsets.start + 1 + e._offsets.end);
                    return a ? i - o : i + o
                }, getPixelForValue: function (t, e, a) {
                    var n = this, i = null;
                    if (void 0 !== e && void 0 !== a && (i = n._timestamps.datasets[a][e]), null === i && (i = vi(n, t)), null !== i) return n.getPixelForOffset(i)
                }, getPixelForTick: function (t) {
                    var e = this.getTicks();
                    return t >= 0 && t < e.length ? this.getPixelForOffset(e[t].value) : null
                }, getValueForPixel: function (t) {
                    var e = this, a = e._horizontal ? e.width : e.height, n = e._horizontal ? e.left : e.top,
                        i = (a ? (t - n) / a : 0) * (e._offsets.start + 1 + e._offsets.start) - e._offsets.end,
                        r = gi(e._table, "pos", i, "time");
                    return e._adapter._create(r)
                }, getLabelWidth: function (t) {
                    var e = this, a = e.options.ticks, n = e.ctx.measureText(t).width, i = Ut.toRadians(a.maxRotation),
                        r = Math.cos(i), o = Math.sin(i), s = oi(a.fontSize, $t.global.defaultFontSize);
                    return n * r + s * o
                }, getLabelCapacity: function (t) {
                    var e = this, a = e.options.time.displayFormats.millisecond, n = e.tickFormatFunction(t, 0, [], a),
                        i = e.getLabelWidth(n), r = e.isHorizontal() ? e.width : e.height, o = Math.floor(r / i);
                    return o > 0 ? o : 1
                }
            }), Si = Ai;
            Mi._defaults = Si;
            var Di = {category: Dn, linear: Bn, logarithmic: $n, radialLinear: ii, time: Mi}, Oi = {
                datetime: "MMM D, YYYY, h:mm:ss a",
                millisecond: "h:mm:ss.SSS a",
                second: "h:mm:ss a",
                minute: "h:mm a",
                hour: "hA",
                day: "MMM D",
                week: "ll",
                month: "MMM YYYY",
                quarter: "[Q]Q - YYYY",
                year: "YYYY"
            };
            yn._date.override("function" === typeof t ? {
                _id: "moment", formats: function () {
                    return Oi
                }, parse: function (e, a) {
                    return "string" === typeof e && "string" === typeof a ? e = t(e, a) : e instanceof t || (e = t(e)), e.isValid() ? e.valueOf() : null
                }, format: function (e, a) {
                    return t(e).format(a)
                }, add: function (e, a, n) {
                    return t(e).add(a, n).valueOf()
                }, diff: function (e, a, n) {
                    return t.duration(t(e).diff(t(a))).as(n)
                }, startOf: function (e, a, n) {
                    return e = t(e), "isoWeek" === a ? e.isoWeekday(n).valueOf() : e.startOf(a).valueOf()
                }, endOf: function (e, a) {
                    return t(e).endOf(a).valueOf()
                }, _create: function (e) {
                    return t(e)
                }
            } : {}), $t._set("global", {plugins: {filler: {propagate: !0}}});
            var Pi = {
                dataset: function (t) {
                    var e = t.fill, a = t.chart, n = a.getDatasetMeta(e), i = n && a.isDatasetVisible(e),
                        r = i && n.dataset._children || [], o = r.length || 0;
                    return o ? function (t, e) {
                        return e < o && r[e]._view || null
                    } : null
                }, boundary: function (t) {
                    var e = t.boundary, a = e ? e.x : null, n = e ? e.y : null;
                    return function (t) {
                        return {x: null === a ? t.x : a, y: null === n ? t.y : n}
                    }
                }
            };

            function Ti(t, e, a) {
                var n, i = t._model || {}, r = i.fill;
                if (void 0 === r && (r = !!i.backgroundColor), !1 === r || null === r) return !1;
                if (!0 === r) return "origin";
                if (n = parseFloat(r, 10), isFinite(n) && Math.floor(n) === n) return "-" !== r[0] && "+" !== r[0] || (n = e + n), !(n === e || n < 0 || n >= a) && n;
                switch (r) {
                    case"bottom":
                        return "start";
                    case"top":
                        return "end";
                    case"zero":
                        return "origin";
                    case"origin":
                    case"start":
                    case"end":
                        return r;
                    default:
                        return !1
                }
            }

            function Ii(t) {
                var e, a = t.el._model || {}, n = t.el._scale || {}, i = t.fill, r = null;
                if (isFinite(i)) return null;
                if ("start" === i ? r = void 0 === a.scaleBottom ? n.bottom : a.scaleBottom : "end" === i ? r = void 0 === a.scaleTop ? n.top : a.scaleTop : void 0 !== a.scaleZero ? r = a.scaleZero : n.getBasePosition ? r = n.getBasePosition() : n.getBasePixel && (r = n.getBasePixel()), void 0 !== r && null !== r) {
                    if (void 0 !== r.x && void 0 !== r.y) return r;
                    if (Ut.isFinite(r)) return e = n.isHorizontal(), {x: e ? r : null, y: e ? null : r}
                }
                return null
            }

            function Ri(t, e, a) {
                var n, i = t[e], r = i.fill, o = [e];
                if (!a) return r;
                while (!1 !== r && -1 === o.indexOf(r)) {
                    if (!isFinite(r)) return r;
                    if (n = t[r], !n) return !1;
                    if (n.visible) return r;
                    o.push(r), r = n.fill
                }
                return !1
            }

            function Fi(t) {
                var e = t.fill, a = "dataset";
                return !1 === e ? null : (isFinite(e) || (a = "boundary"), Pi[a](t))
            }

            function Bi(t) {
                return t && !t.skip
            }

            function Li(t, e, a, n, i) {
                var r;
                if (n && i) {
                    for (t.moveTo(e[0].x, e[0].y), r = 1; r < n; ++r) Ut.canvas.lineTo(t, e[r - 1], e[r]);
                    for (t.lineTo(a[i - 1].x, a[i - 1].y), r = i - 1; r > 0; --r) Ut.canvas.lineTo(t, a[r], a[r - 1], !0)
                }
            }

            function Ni(t, e, a, n, i, r) {
                var o, s, l, c, d, u, h, f = e.length, p = n.spanGaps, g = [], m = [], v = 0, b = 0;
                for (t.beginPath(), o = 0, s = f + !!r; o < s; ++o) l = o % f, c = e[l]._view, d = a(c, l, n), u = Bi(c), h = Bi(d), u && h ? (v = g.push(c), b = m.push(d)) : v && b && (p ? (u && g.push(c), h && m.push(d)) : (Li(t, g, m, v, b), v = b = 0, g = [], m = []));
                Li(t, g, m, v, b), t.closePath(), t.fillStyle = i, t.fill()
            }

            var Ei = {
                id: "filler", afterDatasetsUpdate: function (t, e) {
                    var a, n, i, r, o = (t.data.datasets || []).length, s = e.propagate, l = [];
                    for (n = 0; n < o; ++n) a = t.getDatasetMeta(n), i = a.dataset, r = null, i && i._model && i instanceof Ae.Line && (r = {
                        visible: t.isDatasetVisible(n),
                        fill: Ti(i, n, o),
                        chart: t,
                        el: i
                    }), a.$filler = r, l.push(r);
                    for (n = 0; n < o; ++n) r = l[n], r && (r.fill = Ri(l, n, s), r.boundary = Ii(r), r.mapper = Fi(r))
                }, beforeDatasetDraw: function (t, e) {
                    var a = e.meta.$filler;
                    if (a) {
                        var n = t.ctx, i = a.el, r = i._view, o = i._children || [], s = a.mapper,
                            l = r.backgroundColor || $t.global.defaultColor;
                        s && l && o.length && (Ut.canvas.clipArea(n, t.chartArea), Ni(n, o, s, r, l, i._loop), Ut.canvas.unclipArea(n))
                    }
                }
            }, ji = Ut.noop, Hi = Ut.valueOrDefault;

            function $i(t, e) {
                return t.usePointStyle && t.boxWidth > e ? e : t.boxWidth
            }

            $t._set("global", {
                legend: {
                    display: !0,
                    position: "top",
                    fullWidth: !0,
                    reverse: !1,
                    weight: 1e3,
                    onClick: function (t, e) {
                        var a = e.datasetIndex, n = this.chart, i = n.getDatasetMeta(a);
                        i.hidden = null === i.hidden ? !n.data.datasets[a].hidden : null, n.update()
                    },
                    onHover: null,
                    onLeave: null,
                    labels: {
                        boxWidth: 40, padding: 10, generateLabels: function (t) {
                            var e = t.data;
                            return Ut.isArray(e.datasets) ? e.datasets.map(function (e, a) {
                                return {
                                    text: e.label,
                                    fillStyle: Ut.isArray(e.backgroundColor) ? e.backgroundColor[0] : e.backgroundColor,
                                    hidden: !t.isDatasetVisible(a),
                                    lineCap: e.borderCapStyle,
                                    lineDash: e.borderDash,
                                    lineDashOffset: e.borderDashOffset,
                                    lineJoin: e.borderJoinStyle,
                                    lineWidth: e.borderWidth,
                                    strokeStyle: e.borderColor,
                                    pointStyle: e.pointStyle,
                                    datasetIndex: a
                                }
                            }, this) : []
                        }
                    }
                }, legendCallback: function (t) {
                    var e = [];
                    e.push('<ul class="' + t.id + '-legend">');
                    for (var a = 0; a < t.data.datasets.length; a++) e.push('<li><span style="background-color:' + t.data.datasets[a].backgroundColor + '"></span>'), t.data.datasets[a].label && e.push(t.data.datasets[a].label), e.push("</li>");
                    return e.push("</ul>"), e.join("")
                }
            });
            var zi = Jt.extend({
                initialize: function (t) {
                    Ut.extend(this, t), this.legendHitBoxes = [], this._hoveredItem = null, this.doughnutMode = !1
                }, beforeUpdate: ji, update: function (t, e, a) {
                    var n = this;
                    return n.beforeUpdate(), n.maxWidth = t, n.maxHeight = e, n.margins = a, n.beforeSetDimensions(), n.setDimensions(), n.afterSetDimensions(), n.beforeBuildLabels(), n.buildLabels(), n.afterBuildLabels(), n.beforeFit(), n.fit(), n.afterFit(), n.afterUpdate(), n.minSize
                }, afterUpdate: ji, beforeSetDimensions: ji, setDimensions: function () {
                    var t = this;
                    t.isHorizontal() ? (t.width = t.maxWidth, t.left = 0, t.right = t.width) : (t.height = t.maxHeight, t.top = 0, t.bottom = t.height), t.paddingLeft = 0, t.paddingTop = 0, t.paddingRight = 0, t.paddingBottom = 0, t.minSize = {
                        width: 0,
                        height: 0
                    }
                }, afterSetDimensions: ji, beforeBuildLabels: ji, buildLabels: function () {
                    var t = this, e = t.options.labels || {}, a = Ut.callback(e.generateLabels, [t.chart], t) || [];
                    e.filter && (a = a.filter(function (a) {
                        return e.filter(a, t.chart.data)
                    })), t.options.reverse && a.reverse(), t.legendItems = a
                }, afterBuildLabels: ji, beforeFit: ji, fit: function () {
                    var t = this, e = t.options, a = e.labels, n = e.display, i = t.ctx, r = Ut.options._parseFont(a),
                        o = r.size, s = t.legendHitBoxes = [], l = t.minSize, c = t.isHorizontal();
                    if (c ? (l.width = t.maxWidth, l.height = n ? 10 : 0) : (l.width = n ? 10 : 0, l.height = t.maxHeight), n) if (i.font = r.string, c) {
                        var d = t.lineWidths = [0], u = 0;
                        i.textAlign = "left", i.textBaseline = "top", Ut.each(t.legendItems, function (t, e) {
                            var n = $i(a, o), r = n + o / 2 + i.measureText(t.text).width;
                            (0 === e || d[d.length - 1] + r + a.padding > l.width) && (u += o + a.padding, d[d.length - (e > 0 ? 0 : 1)] = a.padding), s[e] = {
                                left: 0,
                                top: 0,
                                width: r,
                                height: o
                            }, d[d.length - 1] += r + a.padding
                        }), l.height += u
                    } else {
                        var h = a.padding, f = t.columnWidths = [], p = a.padding, g = 0, m = 0, v = o + h;
                        Ut.each(t.legendItems, function (t, e) {
                            var n = $i(a, o), r = n + o / 2 + i.measureText(t.text).width;
                            e > 0 && m + v > l.height - h && (p += g + a.padding, f.push(g), g = 0, m = 0), g = Math.max(g, r), m += v, s[e] = {
                                left: 0,
                                top: 0,
                                width: r,
                                height: o
                            }
                        }), p += g, f.push(g), l.width += p
                    }
                    t.width = l.width, t.height = l.height
                }, afterFit: ji, isHorizontal: function () {
                    return "top" === this.options.position || "bottom" === this.options.position
                }, draw: function () {
                    var t = this, e = t.options, a = e.labels, n = $t.global, i = n.defaultColor, r = n.elements.line,
                        o = t.width, s = t.lineWidths;
                    if (e.display) {
                        var l, c = t.ctx, d = Hi(a.fontColor, n.defaultFontColor), u = Ut.options._parseFont(a),
                            h = u.size;
                        c.textAlign = "left", c.textBaseline = "middle", c.lineWidth = .5, c.strokeStyle = d, c.fillStyle = d, c.font = u.string;
                        var f = $i(a, h), p = t.legendHitBoxes, g = function (t, a, n) {
                            if (!(isNaN(f) || f <= 0)) {
                                c.save();
                                var o = Hi(n.lineWidth, r.borderWidth);
                                if (c.fillStyle = Hi(n.fillStyle, i), c.lineCap = Hi(n.lineCap, r.borderCapStyle), c.lineDashOffset = Hi(n.lineDashOffset, r.borderDashOffset), c.lineJoin = Hi(n.lineJoin, r.borderJoinStyle), c.lineWidth = o, c.strokeStyle = Hi(n.strokeStyle, i), c.setLineDash && c.setLineDash(Hi(n.lineDash, r.borderDash)), e.labels && e.labels.usePointStyle) {
                                    var s = f * Math.SQRT2 / 2, l = t + f / 2, d = a + h / 2;
                                    Ut.canvas.drawPoint(c, n.pointStyle, s, l, d)
                                } else 0 !== o && c.strokeRect(t, a, f, h), c.fillRect(t, a, f, h);
                                c.restore()
                            }
                        }, m = function (t, e, a, n) {
                            var i = h / 2, r = f + i + t, o = e + i;
                            c.fillText(a.text, r, o), a.hidden && (c.beginPath(), c.lineWidth = 2, c.moveTo(r, o), c.lineTo(r + n, o), c.stroke())
                        }, v = t.isHorizontal();
                        l = v ? {
                            x: t.left + (o - s[0]) / 2 + a.padding,
                            y: t.top + a.padding,
                            line: 0
                        } : {x: t.left + a.padding, y: t.top + a.padding, line: 0};
                        var b = h + a.padding;
                        Ut.each(t.legendItems, function (e, n) {
                            var i = c.measureText(e.text).width, r = f + h / 2 + i, d = l.x, u = l.y;
                            v ? n > 0 && d + r + a.padding > t.left + t.minSize.width && (u = l.y += b, l.line++, d = l.x = t.left + (o - s[l.line]) / 2 + a.padding) : n > 0 && u + b > t.top + t.minSize.height && (d = l.x = d + t.columnWidths[l.line] + a.padding, u = l.y = t.top + a.padding, l.line++), g(d, u, e), p[n].left = d, p[n].top = u, m(d, u, e, i), v ? l.x += r + a.padding : l.y += b
                        })
                    }
                }, _getLegendItemAt: function (t, e) {
                    var a, n, i, r = this;
                    if (t >= r.left && t <= r.right && e >= r.top && e <= r.bottom) for (i = r.legendHitBoxes, a = 0; a < i.length; ++a) if (n = i[a], t >= n.left && t <= n.left + n.width && e >= n.top && e <= n.top + n.height) return r.legendItems[a];
                    return null
                }, handleEvent: function (t) {
                    var e, a = this, n = a.options, i = "mouseup" === t.type ? "click" : t.type;
                    if ("mousemove" === i) {
                        if (!n.onHover && !n.onLeave) return
                    } else {
                        if ("click" !== i) return;
                        if (!n.onClick) return
                    }
                    e = a._getLegendItemAt(t.x, t.y), "click" === i ? e && n.onClick && n.onClick.call(a, t.native, e) : (n.onLeave && e !== a._hoveredItem && (a._hoveredItem && n.onLeave.call(a, t.native, a._hoveredItem), a._hoveredItem = e), n.onHover && e && n.onHover.call(a, t.native, e))
                }
            });

            function Vi(t, e) {
                var a = new zi({ctx: t.ctx, options: e, chart: t});
                ha.configure(t, a, e), ha.addBox(t, a), t.legend = a
            }

            var Wi = {
                id: "legend", _element: zi, beforeInit: function (t) {
                    var e = t.options.legend;
                    e && Vi(t, e)
                }, beforeUpdate: function (t) {
                    var e = t.options.legend, a = t.legend;
                    e ? (Ut.mergeIf(e, $t.global.legend), a ? (ha.configure(t, a, e), a.options = e) : Vi(t, e)) : a && (ha.removeBox(t, a), delete t.legend)
                }, afterEvent: function (t, e) {
                    var a = t.legend;
                    a && a.handleEvent(e)
                }
            }, Ui = Ut.noop;
            $t._set("global", {
                title: {
                    display: !1,
                    fontStyle: "bold",
                    fullWidth: !0,
                    padding: 10,
                    position: "top",
                    text: "",
                    weight: 2e3
                }
            });
            var qi = Jt.extend({
                initialize: function (t) {
                    var e = this;
                    Ut.extend(e, t), e.legendHitBoxes = []
                },
                beforeUpdate: Ui,
                update: function (t, e, a) {
                    var n = this;
                    return n.beforeUpdate(), n.maxWidth = t, n.maxHeight = e, n.margins = a, n.beforeSetDimensions(), n.setDimensions(), n.afterSetDimensions(), n.beforeBuildLabels(), n.buildLabels(), n.afterBuildLabels(), n.beforeFit(), n.fit(), n.afterFit(), n.afterUpdate(), n.minSize
                },
                afterUpdate: Ui,
                beforeSetDimensions: Ui,
                setDimensions: function () {
                    var t = this;
                    t.isHorizontal() ? (t.width = t.maxWidth, t.left = 0, t.right = t.width) : (t.height = t.maxHeight, t.top = 0, t.bottom = t.height), t.paddingLeft = 0, t.paddingTop = 0, t.paddingRight = 0, t.paddingBottom = 0, t.minSize = {
                        width: 0,
                        height: 0
                    }
                },
                afterSetDimensions: Ui,
                beforeBuildLabels: Ui,
                buildLabels: Ui,
                afterBuildLabels: Ui,
                beforeFit: Ui,
                fit: function () {
                    var t = this, e = t.options, a = e.display, n = t.minSize,
                        i = Ut.isArray(e.text) ? e.text.length : 1, r = Ut.options._parseFont(e),
                        o = a ? i * r.lineHeight + 2 * e.padding : 0;
                    t.isHorizontal() ? (n.width = t.maxWidth, n.height = o) : (n.width = o, n.height = t.maxHeight), t.width = n.width, t.height = n.height
                },
                afterFit: Ui,
                isHorizontal: function () {
                    var t = this.options.position;
                    return "top" === t || "bottom" === t
                },
                draw: function () {
                    var t = this, e = t.ctx, a = t.options;
                    if (a.display) {
                        var n, i, r, o = Ut.options._parseFont(a), s = o.lineHeight, l = s / 2 + a.padding, c = 0,
                            d = t.top, u = t.left, h = t.bottom, f = t.right;
                        e.fillStyle = Ut.valueOrDefault(a.fontColor, $t.global.defaultFontColor), e.font = o.string, t.isHorizontal() ? (i = u + (f - u) / 2, r = d + l, n = f - u) : (i = "left" === a.position ? u + l : f - l, r = d + (h - d) / 2, n = h - d, c = Math.PI * ("left" === a.position ? -.5 : .5)), e.save(), e.translate(i, r), e.rotate(c), e.textAlign = "center", e.textBaseline = "middle";
                        var p = a.text;
                        if (Ut.isArray(p)) for (var g = 0, m = 0; m < p.length; ++m) e.fillText(p[m], 0, g, n), g += s; else e.fillText(p, 0, 0, n);
                        e.restore()
                    }
                }
            });

            function Yi(t, e) {
                var a = new qi({ctx: t.ctx, options: e, chart: t});
                ha.configure(t, a, e), ha.addBox(t, a), t.titleBlock = a
            }

            var Gi = {
                id: "title", _element: qi, beforeInit: function (t) {
                    var e = t.options.title;
                    e && Yi(t, e)
                }, beforeUpdate: function (t) {
                    var e = t.options.title, a = t.titleBlock;
                    e ? (Ut.mergeIf(e, $t.global.title), a ? (ha.configure(t, a, e), a.options = e) : Yi(t, e)) : a && (ha.removeBox(t, a), delete t.titleBlock)
                }
            }, Ki = {}, Xi = Ei, Ji = Wi, Zi = Gi;
            for (var Qi in Ki.filler = Xi, Ki.legend = Ji, Ki.title = Zi, pn.helpers = Ut, gn(pn), pn._adapters = yn, pn.Animation = Qt, pn.animationService = te, pn.controllers = ta, pn.DatasetController = oe, pn.defaults = $t, pn.Element = Jt, pn.elements = Ae, pn.Interaction = sa, pn.layouts = ha, pn.platform = Va, pn.plugins = Wa, pn.Scale = Mn, pn.scaleService = Ua, pn.Ticks = xn, pn.Tooltip = on, pn.helpers.each(Di, function (t, e) {
                pn.scaleService.registerScaleType(e, t, t._defaults)
            }), Ki) Ki.hasOwnProperty(Qi) && pn.plugins.register(Ki[Qi]);
            pn.platform.initialize();
            var tr = pn;
            return "undefined" !== typeof window && (window.Chart = pn), pn.Chart = pn, pn.Legend = Ki.legend._element, pn.Title = Ki.title._element, pn.pluginService = pn.plugins, pn.PluginBase = pn.Element.extend({}), pn.canvasHelpers = pn.helpers.canvas, pn.layoutService = pn.layouts, pn.LinearScaleBase = Rn, pn.helpers.each(["Bar", "Bubble", "Doughnut", "Line", "PolarArea", "Radar", "Scatter"], function (t) {
                pn[t] = function (e, a) {
                    return new pn(e, pn.helpers.merge(a || {}, {type: t.charAt(0).toLowerCase() + t.slice(1)}))
                }
            }), tr
        })
    }, "367f": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("b-modal", {
                    staticClass: "dashboard-bestAd-modal dashboard-best-ad-modal",
                    class: {first: 1 === t.currentPage, last: t.currentPage === t.ads.length},
                    attrs: {active: t._active, canCancel: ["escape", "outside"], width: ""},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [t.ad ? a("div", {staticClass: "dashboard-bestAd-modal-contents"}, [a("div", {staticClass: "bestAd-container"}, [a("div", {
                    staticClass: "bestAd-container--navigation left",
                    on: {
                        click: function (e) {
                            return t.setPage(t.currentPage - 1)
                        }
                    }
                }, [a("b-icon", {
                    attrs: {
                        pack: "fal",
                        icon: "chevron-left"
                    }
                })], 1), a("div", {staticClass: "bestAd-container--contents"}, [a("div", {staticClass: "dashboard-bestAd-modal-header"}, [a("span", {staticClass: "header-buttons"}, [a("img", {
                    staticClass: "close-btn",
                    attrs: {src: "/img/icons/modal_close_btn.png"},
                    on: {
                        click: function (e) {
                            t._active = !1
                        }
                    }
                })])]), a("div", {
                    staticClass: "best-ad-modal-comp",
                    class: [t.ad.channel, this.launchedClass]
                }, [a("div", {staticClass: "best-ad-modal-comp-left"}, [a("div", {staticClass: "preview-wrapper"}, [t.isSample && ["facebook", "instagram"].includes(t.channel) ? a("div", [t.creativeMedia ? [a("img", {
                    staticStyle: {"max-width": "400px"},
                    attrs: {src: t.creativeMedia}
                })] : [t._v(t._s(t.$t("DASHBOARD.not_available_for_sample")))]], 2) : a("best-preview", {
                    staticClass: "preview",
                    attrs: {values: t.propsToModal()},
                    on: {"update:channelLink": t.handleDirectUrl}
                })], 1)]), a("div", {staticClass: "best-ad-modal-comp-right"}, ["PAUSE" === t.ad.status ? a("span", {staticClass: "deactivated-text"}, [t._v(t._s(t.$t("DASHBOARD.deactivated_ad")))]) : t._e(), a("div", {staticClass: "header"}, [a("img", {attrs: {src: t.logoUrl}}), a("span", {staticClass: "type"}, [t._v(t._s(t.adTypeText))]), "facebook" === t.ad.channel || "instagram" === t.ad.channel ? a("b-icon", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: t.$t("DASHBOARD.placement_tooltip"),
                        expression: "$t('DASHBOARD.placement_tooltip')"
                    }], attrs: {pack: "far", icon: "question-circle"}
                }) : t._e(), t.ad.id ? a("span", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: t.$t("DASHBOARD.ad_id_tooltip"),
                        expression: "$t('DASHBOARD.ad_id_tooltip')"
                    }], staticClass: "ad-id"
                }, [t._v(t._s(t.ad.id))]) : t._e(), a("span", {staticClass: "target"}, [t._v(t._s(t.targetStr))])], 1), a("div", {staticClass: "header-objective"}, [t._v("\n                            " + t._s(t.getObjective(t.ad)) + "\n                            "), "APP_INSTALLS" === t.ad.objective && t.ad.creative.url ? a("a", {
                    staticClass: "app-link",
                    attrs: {target: "_blank", href: t.ad.creative.url}
                }, [t._v(t._s(t.getUrl(t.ad)))]) : t._e()]), a("div", {staticClass: "summary"}, [a("span", {staticClass: "first"}, [a("span", {staticClass: "numbers"}, [t._v(t._s(t._formatNumber(t.ad.clicks)))]), a("span", {staticClass: "text"}, [t._v(t._s(t.$t("DASHBOARD.clicks")))])]), a("span", {staticClass: "second"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t._formatNumber(t.ad.impressions)))]), a("span", {staticClass: "text"}, [t._v(t._s(t.$t("DASHBOARD.history_imps")))])]), a("span", {staticClass: "third"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t.toPercent(t.ad.clicks / t.ad.impressions)))])])]), a("div", {staticClass: "divider"}), a("div", {staticClass: "box-container cost"}, [a("div", {staticClass: "box-container--title"}, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "usd-circle"
                    }
                }), a("span", {staticClass: "box-title"}, [t._v(t._s(t.$t("DASHBOARD.cost")))])], 1), a("div", {staticClass: "box-container--contents"}, [a("div", {staticClass: "cost-each"}, [a("span", {staticClass: "top"}, [t._v(t._s(t._formatCurrency(t.ad.cpc)))]), a("span", {staticClass: "bottom"}, [t._v(t._s(t.$t("DASHBOARD.cost_per_click")))])]), a("div", {staticClass: "cost-each"}, [a("span", {staticClass: "top"}, [t._v(t._s(t._formatCurrency(t.ad.spend)))]), a("span", {staticClass: "bottom"}, [t._v(t._s(t.$t("DASHBOARD.history_tab_title_totalCost")))])])])]), "google-text" === t.adType ? a("div", {staticClass: "box-container keywords"}, [a("div", {staticClass: "box-container--title"}, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "hashtag"
                    }
                }), a("span", {staticClass: "box-title"}, [t._v(t._s(t.$t("DASHBOARD.keywords")))])], 1), a("div", {staticClass: "box-container--contents"}, [a("tags", {
                    staticClass: "google-ad-keywords",
                    attrs: {removable: !1, values: t.extractValues(t.ad.audience.KEYWORD.value), "hash-tag": !0}
                })], 1)]) : t._e(), "google-display" === t.adType ? a("div", {staticClass: "box-container websites"}, [a("div", {staticClass: "box-container--title"}, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "browser"
                    }
                }), a("span", {staticClass: "box-title"}, [t._v(t._s(t.$t("DASHBOARD.best_modal_best_website")))])], 1), a("div", {staticClass: "box-container--contents"}, [a("tags", {
                    staticClass: "google-ad-websites",
                    attrs: {removable: !1, values: t.bestWebsites}
                })], 1)]) : t._e(), t.ad.directUrl ? a("div", {staticClass: "box-container url"}, [a("div", {staticClass: "box-container--title"}, [a("a", {
                    attrs: {
                        target: "_blank",
                        href: t.ad.directUrl
                    }
                }, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "external-link-square"
                    }
                }), a("span", {staticClass: "box-title"}, [t._v(t._s(t.$t("DASHBOARD.directUrl")))])], 1)])]) : t._e(), t.launched ? t._e() : a("div", {staticClass: "not-launched-info on-dashboard"}, [a("img", {attrs: {src: "/img/dashboard/dashboard_info_icon.png"}}), a("span", [t._v(t._s(t.$t("DASHBOARD.in_external_review")))])])])])]), a("div", {
                    staticClass: "bestAd-container--navigation right",
                    on: {
                        click: function (e) {
                            return t.setPage(t.currentPage + 1)
                        }
                    }
                }, [a("b-icon", {attrs: {pack: "fal", icon: "chevron-right"}})], 1)])]) : t._e()])
            }, i = [], r = a("931d"), o = r["a"], s = (a("88e7"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, "36bd": function (t, e, a) {
        "use strict";
        var n = a("4bf8"), i = a("77f1"), r = a("9def");
        t.exports = function (t) {
            var e = n(this), a = r(e.length), o = arguments.length, s = i(o > 1 ? arguments[1] : void 0, a),
                l = o > 2 ? arguments[2] : void 0, c = void 0 === l ? a : i(l, a);
            while (c > s) e[s++] = t;
            return e
        }
    }, "381f": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "budget-status"}, [a("div", {staticClass: "budget-left"}, [a("img", {attrs: {src: "/img/dashboard/spent.png"}}), a("div", {staticClass: "budget-contents"}, [a("span", {staticClass: "budget-title"}, [t._v("\n                " + t._s(t.$t("DASHBOARD.cost_budget_spent")) + "\n            ")]), a("span", [a("span", {staticClass: "budget-text"}, [t._v(t._s(t.formatCurrency(t.spent)))]), a("span", {staticClass: "budget-number"}, [t._v("\n                    " + t._s(t._f("addComma")(t.ratio)) + "%\n                    "), a("b-tooltip", {
                    attrs: {
                        label: t.$t("DASHBOARD.percent_of_spent"),
                        type: "is-light",
                        position: "is-bottom"
                    }
                }, [a("img", {attrs: {src: "/img/proposal/journey_info_icon.png"}})])], 1)])])]), a("div", {staticClass: "budget-right"}, [a("img", {attrs: {src: "/img/dashboard/cost_per_click_icon_big.png"}}), a("div", {staticClass: "budget-contents"}, [a("span", {staticClass: "budget-title"}, [t._v(t._s(t.$t("DASHBOARD.cost_per_click")))]), a("span", {staticClass: "budget-text"}, [t._v(t._s(t.formatCurrency(t.cpc)))])])])])
            }, i = [], r = a("1b61"), o = r["a"], s = (a("f8c6"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "54d8699e", null);
        e["a"] = l.exports
    }, "386b": function (t, e, a) {
        var n = a("5ca1"), i = a("79e5"), r = a("be13"), o = /"/g, s = function (t, e, a, n) {
            var i = String(r(t)), s = "<" + e;
            return "" !== a && (s += " " + a + '="' + String(n).replace(o, "&quot;") + '"'), s + ">" + i + "</" + e + ">"
        };
        t.exports = function (t, e) {
            var a = {};
            a[t] = e(s), n(n.P + n.F * i(function () {
                var e = ""[t]('"');
                return e !== e.toLowerCase() || e.split('"').length > 3
            }), "String", a)
        }
    }, "38a9": function (t, e, a) {
        "use strict";
        var n = a("911a"), i = a.n(n);
        i.a
    }, "3a9a": function (t, e, a) {
    }, "3be3": function (t, e, a) {
        "use strict";
        var n = function () {
            var t = this, e = t.$createElement;
            t._self._c;
            return t._m(0)
        }, i = [function () {
            var t = this, e = t.$createElement, a = t._self._c || e;
            return a("div", {staticClass: "common-separator"}, [a("div", {staticClass: "left-line"}), a("div", {staticClass: "right-line"})])
        }], r = (a("38a9"), a("2877")), o = {}, s = Object(r["a"])(o, n, i, !1, null, "0703f8b2", null);
        e["a"] = s.exports
    }, "3dd5": function (t, e, a) {
    }, "3f84": function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("2d1f"), i = a.n(n), r = a("768b"), o = a("cebc"), s = a("fa7d"), l = a("2f62");
            e["a"] = {
                mounted: function () {
                },
                props: {
                    summary: {type: Object, default: t.always({})},
                    showLastDay: {type: Boolean, default: !1},
                    lastDate: {type: String}
                },
                data: function () {
                    return {mainFormat: s["n"], subFormat: s["n"], percentFormat: s["db"]}
                },
                methods: {
                    getLogo: function (t) {
                        var e = {facebook: "fb", instagram: "insta", google: "google", apple: "apple"};
                        return "/img/dashboard/history_".concat(e[t], "_logo.png")
                    }, toPercent: s["db"], _formatNumber: function (t) {
                        return Object(s["n"])(t, void 0, "N/A")
                    }
                },
                computed: Object(o["a"])({}, Object(l["mapGetters"])("dashboard", ["campaignId"]), {
                    today: t.path(["summary", "today"]),
                    allTime: t.path(["summary", "allTime"]),
                    todayMain: function () {
                        return t.path(["today", "all", this.displayConversion, "goalCompletion"], this)
                    },
                    todaySub: function () {
                        return t.path(["today", "all", this.displayConversion, "clicks"], this)
                    },
                    todayPercent: function () {
                        return t.path(["today", "all", this.displayConversion, "conversionRate"], this)
                    },
                    allTimePercent: function () {
                        return t.path(["allTime", "all", this.displayConversion, "conversionRate"], this)
                    },
                    allTimeMain: function () {
                        return t.path(["allTime", "all", this.displayConversion, "goalCompletion"], this)
                    },
                    allTimeSub: function () {
                        return t.path(["allTime", "all", this.displayConversion, "clicks"], this)
                    },
                    channels: function () {
                        var e = this;
                        return t.compose(t.sortWith([t.descend(t.prop("main")), t.descend(t.prop("ratio"))]), s["k"], t.map(function (t) {
                            var a = Object(r["a"])(t, 2), n = a[0], i = a[1];
                            if (i = i[e.displayConversion], i) {
                                var o = i, s = o.clicks, l = o.goalCompletion, c = o.conversionRate;
                                return {channel: n, main: l, sub: s, ratio: c}
                            }
                        }), t.reject(t.propEq(0, "all")), i.a, t.prop("allTime"))(this)
                    },
                    types: t.path(["summary", "types"]),
                    displayConversion: {
                        get: t.path(["summary", "displayConversion"]), set: function (t) {
                            this.$emit("update:displayConversion", t)
                        }
                    },
                    conversionTypes: t.path(["summary", "conversionTypes"])
                })
            }
        }).call(this, a("b17e"))
    }, "43c9": function (t, e, a) {
        "use strict";
        (function (t) {
            a("ac6a"), a("1584");
            var n = a("fa7d");
            e["a"] = {
                render: function () {
                    var t = arguments[0];
                    return t("div", {class: "impression-click-box"}, [t("div", {class: "head"}, [t("span", {class: "head--title"}), t("span", {class: "head--clicks bullet-before"}, [this.$t("DASHBOARD.imp_click_clicks")]), t("span", {class: "head--impressions bullet-before"}, [this.$t("DASHBOARD.imp_click_imps")]), t("span", {class: "head--ctr bullet-before"}, [this.$t("DASHBOARD.imp_click_ctr")])]), t("div", {class: "row--container"}, [this.list()])])
                },
                props: {
                    getImgUrl: Function,
                    getKey: Function,
                    getTooltip: Function,
                    getParentClass: {type: Function, default: t.always("")},
                    values: {type: [Array, Object], required: !0}
                },
                methods: {
                    title: function (t, e) {
                        var a = this.$createElement, n = this.getKey && this.getKey(t, e);
                        return a("span", {
                            class: "row--title adriel-ellipsis",
                            directives: [{name: "tooltip", value: n}]
                        }, [this.getImgUrl && a("img", {attrs: {src: this.getImgUrl(t, e)}}), a("span", {class: "adriel-ellipsis"}, [n]), this.getTooltip && a("info-tooltip", {attrs: {label: this.getTooltip(t, e)}})])
                    }, list: function () {
                        var t = this, e = this.$createElement;
                        return this.values.map(function (a, i) {
                            var r = a.clicks, o = a.impressions,
                                s = ["row--each", "row--each-".concat(i), t.getParentClass(a)];
                            return e("div", {class: s.join(" ")}, [t.title(a, i), e("span", {class: "row--clicks"}, [Object(n["n"])(r)]), e("span", {class: "row--impressions bullet-before"}, [Object(n["n"])(o)]), e("span", {class: "row--ctr bullet-before"}, [Object(n["db"])(r / o)])])
                        })
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "4e70": function (t, e, a) {
        "use strict";
        var n = a("5a94"), i = a.n(n);
        i.a
    }, 5474: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("1fca"), i = a("a748"), r = a("1a2d"), o = a("1585"), s = a("ebb6"), l = a("69dd");
            e["a"] = {
                extends: n["a"], props: ["chartData", "options"], mounted: function () {
                    this.watchData$()
                }, methods: {
                    render: function () {
                        this.renderChart(t.clone(this.chartData), t.clone(this.options))
                    }, watchData$: function () {
                        var e = Object(i["a"])(this.$watchAsObservable("chartData", {
                            immediate: !0,
                            deep: !0
                        }), this.$watchAsObservable("options", {
                            immediate: !0,
                            deep: !0
                        })).pipe(Object(r["a"])(300), Object(o["a"])("newValue"), Object(s["a"])(t.clone), Object(l["a"])(t.equals));
                        this.$subscribeTo(e, this.render)
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "54eb4": function (t, e, a) {
    }, 5583: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("5176"), i = a.n(n), r = (a("ac6a"), a("a4bb")), o = a.n(r),
                s = (a("55dd"), a("c5f6"), a("c15c")), l = a("43c9"), c = t.compose(Number, t.prop("clicks")),
                d = function (t, e) {
                    return c(e) - c(t)
                };
            e["a"] = {
                mixins: [s["a"]], computed: {
                    _values: function () {
                        var e = this;
                        return t.sort(d, o()(this.values).reduce(function (t, a) {
                            return t.push(i()({key: a}, e.values[a])), t
                        }, []))
                    }
                }, methods: {
                    getImgUrl: function (t) {
                        var e = t.key;
                        return "/img/dashboard/".concat(e, ".png")
                    }, getKey: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        return this.$t("DASHBOARD.gender_".concat(t.key))
                    }
                }, components: {ImpressionClickBox: l["a"]}
            }
        }).call(this, a("b17e"))
    }, "55dd": function (t, e, a) {
        "use strict";
        var n = a("5ca1"), i = a("d8e8"), r = a("4bf8"), o = a("79e5"), s = [].sort, l = [1, 2, 3];
        n(n.P + n.F * (o(function () {
            l.sort(void 0)
        }) || !o(function () {
            l.sort(null)
        }) || !a("2f21")(s)), "Array", {
            sort: function (t) {
                return void 0 === t ? s.call(r(this)) : s.call(r(this), i(t))
            }
        })
    }, 5633: function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-timeSpent-wrapper"}, [a("card-view", {
                    attrs: {
                        label: t.$t("DASHBOARD.card_title_time_spent"),
                        className: t.className,
                        editable: t.editable
                    }
                }, [t.isGaConnected ? a("div", {staticClass: "timeSpent-contents"}, [t.leftValue ? a("div", {staticClass: "times"}, [a("span", [t._v(t._s(t.leftValue))]), a("span", [t._v(t._s(t.$t("DASHBOARD.timespent_fomula")))])]) : a("div", {staticClass: "times empty"}, [a("span", [t._v("N/A")])]), t.bounceRate && t.leftValue ? a("div", {staticClass: "bounce"}, [a("span", [t._v(t._s(t.$t("DASHBOARD.timespent_bounce_rate")))]), a("info-tooltip", {attrs: {label: "Percentage of people who clicked the ad and left your site without navigating further."}}), a("span", [t._v(t._s(t.bounceRate) + "%")])], 1) : t._e()]) : t._e(), t.isGaConnected ? t._e() : a("not-available", {
                    staticClass: "not-available-time-spent",
                    attrs: {imgUrl: "/img/dashboard/time_no_data.png"}
                }, [a("span", {
                    staticClass: "not-available-bottom-spent",
                    attrs: {slot: "not-available-bottom"},
                    slot: "not-available-bottom"
                }, [t._v("\n                " + t._s(t.$t("DASHBOARD.need_ga")) + "\n            ")])])], 1)], 1)
            }, i = [], r = a("f79f"), o = r["a"], s = (a("0ad0"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, "568f": function (t, e, a) {
    }, 5789: function (t, e, a) {
        "use strict";
        (function (t) {
            a("7514");
            var n = a("db0c"), i = a.n(n), r = a("768b"), o = a("2d1f"), s = a.n(o), l = (a("c5f6"), a("cebc")),
                c = (a("6c7b"), a("2f62")), d = a("d3c0"), u = a("65aa"), h = a("7ee8"), f = a("85c2"), p = a("ef42"),
                g = a("fa7d"), m = a("c1df"), v = a.n(m), b = a("1585"), y = a("4677"), x = a("69dd"),
                _ = function (e) {
                    return t.assocPath(["tooltips", "callbacks"], {
                        label: function (t, a) {
                            var n = a.datasets[t.datasetIndex].label || "";
                            return n && (n += ": "), n += e(t.yLabel), n
                        }
                    })
                }, C = function (e) {
                    return t.compose(_(e), t.assocPath(["scales", "yAxes", 0, "ticks", "callback"], e))
                }, k = function (e) {
                    return t.compose(C(e), t.assocPath(["scales", "yAxes", 0, "stacked"], !1))
                }, w = {
                    responsive: !0,
                    maintainAspectRatio: !1,
                    legend: {display: !1},
                    scales: {
                        xAxes: [{
                            stacked: !0,
                            maxBarThickness: 30,
                            gridLines: {
                                drawTicks: !1,
                                drawOnChartArea: !1,
                                fontFamily: "'Rubik', 'sans-serif', 'Noto Sans'",
                                zeroLineBorderDashOffset: 10
                            },
                            ticks: {padding: 8, min: 0, fontFamily: "'Rubik', 'sans-serif', 'Noto Sans'"}
                        }],
                        yAxes: [{
                            stacked: !0,
                            gridLines: {
                                borderDash: [10, 3],
                                drawTicks: !1,
                                drawBorder: !1,
                                zeroLineBorderDash: [5, 5],
                                zeroLineColor: "#cccccc",
                                color: "#cccccc",
                                fontFamily: "'Rubik', 'sans-serif', 'Noto Sans'"
                            },
                            ticks: {
                                padding: 15,
                                min: 0,
                                maxTicksLimit: 5,
                                fontFamily: "'Rubik', 'sans-serif', 'Noto Sans'",
                                callback: t.unary(g["n"])
                            }
                        }]
                    }
                }, A = function (e, a) {
                    var n = t.takeLast(a);
                    return t.pipe(t.over(t.lensProp("labels"), function (i) {
                        if (a < e) i = n(i); else if (a > e) {
                            t.path(["labels", "0"]);
                            while (i.length < a) i.unshift(v()(t.head(i)).subtract(1, "day"))
                        }
                        return i
                    }), t.over(t.lensProp("datasets"), t.pipe(t.map(function (e) {
                        var i = e.data, r = a - i.length;
                        r > 0 ? e.data = t.concat(new Array(r).fill(0), i) : r < 0 && (e.data = n(i));
                        var o = {};
                        return "line" === e.type && (o.pointHitRadius = 20), Object(l["a"])({}, e, o)
                    }))))
                };
            e["a"] = {
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        var e = t.$watchAsObservable("windowDimension", {
                            immediate: !0,
                            deep: !0
                        }).pipe(Object(b["a"])("newValue", "width"), Object(y["a"])(200), Object(x["a"])());
                        t.$subscribeTo(e, function (e) {
                            var a = {height: "220px", position: "relative"}, n = {};
                            n = e > 800 ? {width: "600px"} : {width: .9 * e - 30 + "px"}, t.graphStyle = Object(l["a"])({}, a, n)
                        })
                    })
                },
                data: function () {
                    return {periods: ["7", "15", "30"], graphStyle: {width: "600px", height: "220px"}}
                },
                props: {
                    tabs: {type: Array, deafult: t.always([])},
                    selected: {type: Object, deafult: t.always({})},
                    period: [String, Number],
                    displayConversion: String
                },
                components: {
                    HistoryMixedChart: d["a"],
                    HistoryLeftTypeA: u["a"],
                    HistoryLeftTypeB: h["a"],
                    HistoryLeftTypeC: f["a"],
                    GaHistoryCard: p["a"]
                },
                methods: {
                    getTabName: function (t) {
                        return this.$t("DASHBOARD.history_tab_title_".concat(t))
                    }, getPeriod: function (t) {
                        return this.$t("DASHBOARD.history_period", {days: t})
                    }, getTab: function (t) {
                        return this.$t("DASHBOARD.history_sub_title_".concat(t))
                    }, subTabClass: function (t) {
                        switch (t) {
                            case"clicks":
                            case"platform":
                            case"video10s":
                            case"conversions":
                                return "subTab--blue";
                            case"impressions":
                            case"budget":
                            case"conversionRate":
                                return "subTab--orange";
                            case"cpc":
                            case"ctr":
                            case"videoViewRatio":
                                return "subTab--red"
                        }
                    }, subTabs: function (t) {
                        switch (t || this._parent) {
                            case"clicks":
                                return ["clicks", "impressions", "ctr"];
                            case"totalCost":
                                return ["platform"];
                            case"cpc":
                                return ["cpc"];
                            case"dailyBudget":
                                return ["budget"];
                            case"video10s":
                                return ["video10s", "impressions", "videoViewRatio"];
                            case"conversions":
                                return ["conversions", "conversionRate"]
                        }
                    }, updateParent: function (e) {
                        e !== this._parent && (this.$emit("update:tab:parent", e), this.$emit("update:tab:child", t.compose(t.head, t.defaultTo([]), this.subTabs)(e)))
                    }, getSummary: function (e, a, n, i, o) {
                        return i ? {
                            types: {main: e, sub: a, ratio: n},
                            formatter: o || {main: this.formatNumber, sub: this.formatNumber},
                            today: {main: t.path(["today", e, "all"], i), sub: t.path(["today", a, "all"], i)},
                            allTime: {main: t.path(["allTime", e, "all"], i), sub: t.path(["allTime", a, "all"], i)},
                            channels: t.compose(t.sortWith([t.descend(t.prop(e)), t.descend(t.prop(n))]), t.map(t.apply(t.assoc("channel"))), t.reject(t.propEq("0", "all")), s.a, t.reduce(function (e, a) {
                                var n = Object(r["a"])(a, 2), i = n[0], o = n[1];
                                return s()(o).reduce(function (e, a) {
                                    var n = Object(r["a"])(a, 2), o = n[0], s = n[1];
                                    return t.is(Number, s) ? t.assocPath([o, i], s, e) : e
                                }, e)
                            }, {}), s.a, t.prop("allTime"))(i)
                        } : {}
                    }, handleConversionChange: function (t) {
                        this.$emit("update:displayConversion", t)
                    }
                },
                computed: Object(l["a"])({}, Object(c["mapGetters"])("view", ["windowDimension"]), Object(c["mapGetters"])("dashboard", ["graphClicks", "graphImpressions", "graphCtr", "graphTotalCost", "graphTotalBudget", "graphCpc", "graphVideo10s", "graphVideoImpressions", "graphVideoViewRatio", "historyClickSummary", "historySpendSummary", "historyVideoSummary", "historyCpcSummary", "status", "isGaConnected", "selectedConversion", "graphConversions", "historyConversionsSummary", "conversionTypes", "graphConversionRate"]), {
                    graphKey: t.converge(t.compose(t.join("::"), function () {
                        for (var e = arguments.length, a = new Array(e), n = 0; n < e; n++) a[n] = arguments[n];
                        return a.reduce(t.concat, [], a)
                    }), [t.compose(t.of, t.defaultTo(""), t.prop("displayConversion")), t.compose(i.a, t.defaultTo({}), t.prop("selected")), t.compose(t.of, t.prop("period"))]),
                    _period: {
                        get: t.prop("period"), set: function (t) {
                            this.$emit("update:period", t)
                        }
                    },
                    isDeactivated: t.propEq("status", "deactivated"),
                    summary: function () {
                        switch (this._parent) {
                            case"clicks":
                                return this.getSummary("click", "impression", "ctr", this.historyClickSummary);
                            case"cpc":
                                return this.getSummary("cpc", "click", void 0, this.historyCpcSummary, {
                                    main: this.formatCurrency,
                                    sub: this.formatNumber
                                });
                            case"video10s":
                                return this.getSummary("videoView", "impression", "ratioViewsImpressions", this.historyVideoSummary);
                            case"dailyBudget":
                                return Object(l["a"])({}, this.historySpendSummary, {type: "budget", target: "today"});
                            case"totalCost":
                                return Object(l["a"])({}, this.historySpendSummary, {type: "spend", target: "allTime"});
                            case"conversions":
                                return Object(l["a"])({}, this.historyConversionsSummary, {
                                    conversionTypes: this.conversionTypes,
                                    displayConversion: this.displayConversion
                                })
                        }
                    },
                    graphData: function () {
                        if ("clicks" === this._parent) {
                            if ("clicks" === this._child) return this.graphClicks;
                            if ("impressions" === this._child) return this.graphImpressions;
                            if ("ctr" === this._child) return this.graphCtr
                        } else {
                            if ("totalCost" === this._parent) return this.graphTotalCost;
                            if ("cpc" === this._parent) return this.graphCpc;
                            if ("dailyBudget" === this._parent) return this.graphTotalBudget;
                            if ("video10s" === this._parent) {
                                if ("video10s" === this._child) return this.graphVideo10s;
                                if ("impressions" === this._child) return this.graphVideoImpressions;
                                if ("videoViewRatio" === this._child) return this.graphVideoViewRatio
                            } else if ("conversions" === this._parent) {
                                if ("conversions" === this._child) return this.graphConversions[this.displayConversion];
                                if ("conversionRate" === this._child) return this.graphConversionRate[this.displayConversion]
                            }
                        }
                    },
                    _parent: t.path(["selected", "parent"]),
                    _child: t.path(["selected", "child"]),
                    mainColor: function () {
                        return t.path(["graphData", "_options", "mainColor"], this) || t.compose(t.head, t.props(["backgroundColor", "borderColor"]), t.defaultTo({}), t.find(t.propEq("label", "all")), t.defaultTo([]), t.path(["graphData", "datasets"]))(this)
                    },
                    lastDate: function () {
                        var e = t.path(["formatted", "labels"], this);
                        return e ? t.last(e) : ""
                    },
                    formatted: function () {
                        var e = this, a = Number(this.period), n = t.clone(this.graphData || {});
                        return Object(g["N"])(n) ? t.pipe(A(n.labels.length, a), t.over(t.lensProp("labels"), t.addIndex(t.map)(function (t, n) {
                            return n !== a - 1 || e.isDeactivated ? v()(t).format("MM[-]DD") : e.$t("DASHBOARD.today")
                        })))(n) : n
                    },
                    _options: function () {
                        var e = C(t.unary(g["n"]));
                        switch (this._child) {
                            case"ctr":
                            case"videoViewRatio":
                            case"conversionRate":
                                e = k(g["db"]);
                                break;
                            case"cpc":
                                e = k(t.unary(this.formatCurrency));
                                break;
                            case"platform":
                            case"budget":
                                e = C(t.unary(this.formatCurrency));
                                break
                        }
                        return t.compose(e, t.assocPath(["scales", "yAxes", 0, "ticks", "fontColor"], this.mainColor))(w)
                    },
                    leftType: function () {
                        switch (this._parent) {
                            case"clicks":
                            case"cpc":
                            case"video10s":
                                return "HistoryLeftTypeA";
                            case"totalCost":
                            case"dailyBudget":
                                return "HistoryLeftTypeB";
                            case"conversions":
                                return "HistoryLeftTypeC"
                        }
                    },
                    isSingleTab: function () {
                        return t.pipe(this.subTabs, t.defaultTo([]), t.propSatisfies(t.lte(t.__, 1), "length"))(this._parent)
                    },
                    gaCardVisible: t.propEq("_parent", "conversions")
                })
            }
        }).call(this, a("b17e"))
    }, "5a94": function (t, e, a) {
    }, "5d0e": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "ads-facebook-preveiw"}, [!t.isSample && t.url ? a("iframe", {
                    style: {
                        width: "540px",
                        height: "720px"
                    }, attrs: {src: t.url, scrolling: "yes"}
                }) : a("div", [a("img", {attrs: {src: t.creativeMedia}})])])
            }, i = [], r = a("b422"), o = r["a"], s = (a("1d83"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "f9bde094", null);
        e["a"] = l.exports
    }, "60ab": function (t, e, a) {
        "use strict";
        var n = a("9ead"), i = a.n(n);
        i.a
    }, "65aa": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "left-box"}, [t.showLastDay ? a("div", {
                    staticClass: "left-box--today-title",
                    domProps: {textContent: t._s(t.lastDate)}
                }) : a("div", {
                    staticClass: "left-box--today-title",
                    domProps: {textContent: t._s(t.$t("DASHBOARD.today"))}
                }), a("div", {staticClass: "left-box--today-contents"}, [a("span", {staticClass: "first"}, [a("span", {staticClass: "numbers"}, [t._v(t._s(t.mainFormat(t.todayMain)))])]), a("span", {staticClass: "second"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t.subFormat(t.todaySub)))]), a("span", {staticClass: "text"}, [t._v(t._s(t.subText))])])]), a("div", {staticClass: "left-box--divider"}), a("div", {
                    staticClass: "left-box--all-title",
                    domProps: {textContent: t._s(t.$t("DASHBOARD.all_time"))}
                }), a("div", {staticClass: "left-box--all-contents"}, [a("span", {staticClass: "first"}, [a("span", {staticClass: "numbers"}, [t._v(t._s(t.mainFormat(t.allTimeMain)))])]), a("span", {staticClass: "second"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t.subFormat(t.allTimeSub)))]), a("span", {staticClass: "text"}, [t._v(t._s(t.subText))])])]), a("div", {staticClass: "left-box--channels"}, [a("div", {staticClass: "channel"}, t._l(t.channels, function (e) {
                    return a("span", {key: e.channel}, [a("img", {attrs: {src: t.getLogo(e.channel)}})])
                }), 0), a("div", {staticClass: "clicks"}, t._l(t.channels, function (e) {
                    return a("span", {key: e.channel, domProps: {textContent: t._s(t.mainFormat(e[t.types.main]))}})
                }), 0), a("div", {staticClass: "views"}, t._l(t.channels, function (e) {
                    return a("span", {
                        key: e.channel,
                        staticClass: "bullet-before",
                        domProps: {textContent: t._s(t.subFormat(e[t.types.sub]))}
                    })
                }), 0), void 0 !== t.types.ratio ? a("div", {staticClass: "ctrs"}, t._l(t.channels, function (e) {
                    return a("span", {
                        key: e.channel,
                        staticClass: "bullet-before",
                        domProps: {textContent: t._s(t.toPercent(e[t.types.ratio] || 0))}
                    })
                }), 0) : t._e()])])
            }, i = [], r = a("6efc"), o = r["a"], s = (a("d247"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "a477e78c", null);
        e["a"] = l.exports
    }, "660a": function (t, e, a) {
        "use strict";
        var n = a("0fd8"), i = a.n(n);
        i.a
    }, "673e": function (t, e, a) {
        "use strict";
        a("386b")("sub", function (t) {
            return function () {
                return t(this, "sub", "", "")
            }
        })
    }, "6aed": function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("7618"), i = a("ce4b"), r = a("fa7d");
            e["a"] = {
                created: function () {
                }, name: "adsTemplate_callToAction", mixins: [i["a"]], computed: {
                    phoneNumber: function () {
                        var e = t.path(["creative", "phoneNumber"], this);
                        return "object" == Object(n["a"])(e) ? e.phoneNumber : e
                    },
                    businessName: t.path(["creative", "businessName"]),
                    url: t.compose(r["j"], t.path(["creative", "url"])),
                    description: t.path(["creative", "description"])
                }
            }
        }).call(this, a("b17e"))
    }, "6b7e": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-wrapper"}, [t.editingCampaign ? a("update-title", {
                    attrs: {campaign: t.editingCampaign},
                    on: {complete: t.handleComplete}
                }) : t._e(), a("left-nav", {staticClass: "left-nav"}), a("mobile-nav", {staticClass: "mobile-nav"}), a("div", {staticClass: "dashboard-contents"}, [a("top-title", {attrs: {sub: t.$t("DASHBOARD.dashboard_title")}}, [a("div", {
                    staticClass: "dashboard-main-title",
                    attrs: {slot: "next-main"},
                    on: {click: t.setEditingCampaign},
                    slot: "next-main"
                }, [a("span", {
                    directives: [{name: "tooltip", rawName: "v-tooltip", value: t.title, expression: "title"}],
                    staticClass: "adriel-ellipsis word-break"
                }, [t._v(t._s(t.title))]), t.loaded ? a("b-icon", {
                    style: {"font-size": "10px", color: "#666666"},
                    attrs: {pack: "fal", icon: "pen"}
                }) : t._e()], 1), t.loaded ? a("div", {
                    staticClass: "dashboard-top",
                    attrs: {slot: "title-slot"},
                    slot: "title-slot"
                }, [a("running-status", t._b({
                    nativeOn: {
                        click: function (e) {
                            return t.runLonger(e)
                        }
                    }
                }, "running-status", t.propsToRunningStatus, !1), [t.isRunLongerVisible ? a("span", {
                    staticClass: "run-longer-btn",
                    attrs: {slot: "bottom"},
                    slot: "bottom"
                }, [a("span", {staticClass: "notch"}), t._v("\n                        + " + t._s(t.extendText) + "\n                    ")]) : t._e()])], 1) : t._e()]), t.isLoading ? [t._m(0)] : [t.loaded ? [a("div", {staticClass: "dashboard-summary"}, [a("div", {staticClass: "summary-elem dashboard-clicks"}, [t._m(1), a("span", {staticClass: "summary-elem-number"}, [t._v(t._s(t._f("addComma")(t.clicks)))]), a("span", {staticClass: "summary-elem-title"}, [t._v(t._s(t.deactivated ? t.$t("DASHBOARD.clicks_total") : t.$t("DASHBOARD.clicks_of_today")))])]), t.conversionTopValue ? a("div", {staticClass: "summary-elem dashboard-sales summary-elem-left"}, [t._m(2), a("span", {staticClass: "summary-elem-number"}, [t._v(t._s(t.formatNumber(t.conversionTopValue.value)))]), a("span", {staticClass: "summary-elem-title"}, [t._v(t._s("normalImpression" === t.conversionTopValue.key ? this.$t("DASHBOARD.summary_elem_impression") : t.conversionTopValue.key))])]) : t._e(), a("div", {staticClass: "summary-elem dashboard-cpc"}, [t._m(3), a("span", {staticClass: "summary-elem-number"}, [t._v(t._s(t.formatCurrency(t.cpc)))]), a("span", {staticClass: "summary-elem-title"}, [t._v(t._s(t.$t("DASHBOARD.cost_per_click")))])]), a("div", {staticClass: "summary-elem dashboard-channel summary-elem-left"}, [t._m(4), a("span", {staticClass: "summary-elem-channel"}, [a("img", {attrs: {src: t.channelImg}}), a("span", [t._v(t._s(t._f("upperHead")(t.bestChannel)))])]), a("span", {
                    staticClass: "summary-elem-title",
                    domProps: {textContent: t._s(t.$t("DASHBOARD.best_channel"))}
                })]), a("div", {staticClass: "additional-elem"}, [a("span", {
                    staticClass: "dashboard-btn budget",
                    class: t.userLang,
                    on: {
                        click: function (e) {
                            return t.$emit("addBudget")
                        }
                    }
                }, [a("img", {attrs: {src: "/img/dashboard/add_more_budget_icon.png"}}), a("span", [t._v(t._s(t.$t("DASHBOARD.add_budget")))])]), a("span", {
                    staticClass: "dashboard-btn",
                    class: t.userLang,
                    on: {
                        click: function (e) {
                            return t.pushToRoute("/proposal/" + t.campaignId + "/ads")
                        }
                    }
                }, [a("span", {staticClass: "icon-wrapper"}, [a("b-icon", {
                    style: {"font-size": "12px", color: "#5aabe3"},
                    attrs: {pack: "far", icon: "pen-square"}
                })], 1), a("span", {staticClass: "dropdown-span"}, [t._v(t._s(t.$t("DASHBOARD.ad_settings")))])]), a("span", {
                    staticClass: "dashboard-btn",
                    class: t.userLang,
                    on: {
                        click: function (e) {
                            return t.pushToRoute("/billingHistory")
                        }
                    }
                }, [a("span", {staticClass: "icon-wrapper"}, [a("b-icon", {
                    style: {"font-size": "12px", color: "#635897"},
                    attrs: {pack: "far", icon: "file-invoice-dollar"}
                })], 1), a("span", {staticClass: "dropdown-span"}, [t._v(t._s(t.$t("DASHBOARD.billing_history")))])]), a("span", {
                    staticClass: "dashboard-btn",
                    class: t.userLang,
                    on: {click: t.downloadExcel}
                }, [t._m(5), a("span", {staticClass: "dropdown-span"}, [t._v(t._s(t.$t("DASHBOARD.download_excel")))])])])]), t.form ? a("div", {staticClass: "lead_form"}, [a("div", {staticClass: "lead_form--title"}, [a("b-icon", {
                    attrs: {
                        pack: "fal",
                        icon: "list-alt"
                    }
                }), a("span", {staticClass: "lead_form--text"}, [a("span", {staticClass: "normal"}, [t._v(t._s(t.$t("DASHBOARD.form_title")))])])], 1), a("div", {staticStyle: {position: "relative"}}, [a("div", {staticClass: "dashboard-form-container"}, [a("div", {staticClass: "dashboard-form-count-area"}, [a("span", [t._v(t._s(t.$t("FORM.leads_count")))]), a("span", {staticClass: "highlight"}, [t._v(t._s(this.form.answers || 0))])]), a("div", {staticClass: "dashboard-form-img-area"}, [a("img", {
                    attrs: {
                        src: t.formEdit.img,
                        alt: ""
                    }
                })]), a("div", {staticClass: "dashboard-form-texts-area"}, [a("div", {staticClass: "form--title"}, [t._v(t._s(this.deleteTag(t.formEdit.header[0].title)))]), a("div", {staticClass: "form--desc"}, [t._v(t._s(this.deleteTag(t.formEdit.header[1].title)))])]), a("div", {staticClass: "dashboard-form-btns-area"}, [a("div", {staticClass: "dashboard-form-btns-wrapper"}, [a("button", {
                    class: {
                        "lang-ko": "ko" == t.lang,
                        "lang-en": "en" == t.lang
                    }, on: {click: t.goFormPage}
                }, [t._v(t._s(t.$t("DASHBOARD.form_btn1")))]), a("button", {
                    class: {
                        "lang-ko": "ko" == t.lang,
                        "lang-en": "en" == t.lang
                    }, on: {click: t.handleClickForm}
                }, [t._v(t._s(t.$t("DASHBOARD.form_btn2")))]), a("button", {on: {click: t.downloadForm}}, [a("i", {staticClass: "fas fa-arrow-alt-to-bottom"})])])])])])]) : t._e(), a("div", {staticClass: "runnings"}, [a("div", {staticClass: "runnings--title"}, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "ad"
                    }
                }), a("span", {staticClass: "runnings--text"}, [a("span", {staticClass: "normal"}, [t._v(t._s(t.$t("DASHBOARD.number_of_running_ads")))]), a("span", {staticClass: "highlight"}, [t._v(t._s(t.adsLength))])])], 1), a("ads-slide", {
                    attrs: {ads: t.ads},
                    on: {"click:ad": t.handleClickAd}
                })], 1), t.graph ? [a("history-card", {
                    staticClass: "history-top",
                    attrs: {
                        tabs: t.dataTabs,
                        selected: t.selectedData,
                        period: t.selectedPeriod,
                        displayConversion: t._displayConversion
                    },
                    on: {
                        "update:displayConversion": function (e) {
                            t._displayConversion = e
                        }, "update:display-conversion": function (e) {
                            t._displayConversion = e
                        }, "update:tab:child": function (e) {
                            t.selectedData.child = e
                        }, "update:tab:parent": function (e) {
                            t.selectedData.parent = e
                        }, "update:period": function (e) {
                            t.selectedPeriod = e
                        }
                    }
                }), a("history-card", {
                    staticClass: "history-bottom",
                    attrs: {tabs: ["cpc", "totalCost", "dailyBudget"], selected: t.selectedCost, period: t.costPeriod},
                    on: {
                        "update:tab:child": function (e) {
                            t.selectedCost.child = e
                        }, "update:tab:parent": function (e) {
                            t.selectedCost.parent = e
                        }, "update:period": function (e) {
                            t.costPeriod = e
                        }
                    }
                })] : t._e(), a("div", {staticClass: "dashboard-boxes"}, t._l(t.types, function (e) {
                    return a("a-comp", {
                        key: e, attrs: {type: t.getType(e)}, on: {
                            "show:modal": function (e) {
                                return t.$emit("show:modal", e)
                            }
                        }
                    })
                }), 1)] : t._e(), t.loaded ? t._e() : a("no-result")]], 2)], 1)
            }, i = [function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "loading-wrapper"}, [a("div", {staticClass: "animated-logo"})])
            }, function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("span", {staticClass: "img-container"}, [a("img", {attrs: {src: "/img/dashboard/top_clicks_icon.png"}})])
            }, function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("span", {staticClass: "img-container"}, [a("img", {attrs: {src: "/img/dashboard/top_conversions_icon.png"}})])
            }, function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("span", {staticClass: "img-container"}, [a("img", {attrs: {src: "/img/dashboard/top_cost_icon.png"}})])
            }, function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("span", {staticClass: "img-container"}, [a("img", {attrs: {src: "/img/dashboard/top_channel_icon.png"}})])
            }, function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("span", {staticClass: "icon-wrapper execl-span"}, [a("img", {attrs: {src: "/img/icons/dashboard_excel_icon.png"}})])
            }], r = a("914f"), o = r["a"], s = (a("e465"), a("c505"), a("a6cf"), a("660a"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "48244ef3", null);
        e["a"] = l.exports
    }, "6c7b": function (t, e, a) {
        var n = a("5ca1");
        n(n.P, "Array", {fill: a("36bd")}), a("9c6c")("fill")
    }, "6deb": function (t, e, a) {
    }, "6efc": function (t, e, a) {
        "use strict";
        (function (t) {
            a("673e");
            var n = a("fa7d");
            e["a"] = {
                props: {
                    summary: {
                        type: Object, default: function () {
                            return {}
                        }
                    }, showLastDay: {type: Boolean, default: !1}, lastDate: {type: String}
                },
                data: function () {
                    return {}
                },
                methods: {
                    getLogo: function (t) {
                        var e = {facebook: "fb", instagram: "insta", google: "google", apple: "apple"};
                        return "/img/dashboard/history_".concat(e[t], "_logo.png")
                    }, toPercent: n["db"], _formatNumber: function (t) {
                        return Object(n["n"])(t, void 0, "N/A")
                    }
                },
                computed: {
                    today: t.path(["summary", "today"]),
                    allTime: t.path(["summary", "allTime"]),
                    todayMain: t.path(["today", "main"]),
                    todaySub: t.path(["today", "sub"]),
                    allTimeMain: t.path(["allTime", "main"]),
                    allTimeSub: t.path(["allTime", "sub"]),
                    channels: t.path(["summary", "channels"]),
                    types: t.path(["summary", "types"]),
                    subText: function () {
                        var t = this.types.sub;
                        return "impression" === t ? this.$t("DASHBOARD.history_imps") : "click" === t ? this.$t("DASHBOARD.history_clicks") : void 0
                    },
                    mainFormat: t.pathOr(t.identity, ["summary", "formatter", "main"]),
                    subFormat: t.pathOr(t.identity, ["summary", "formatter", "sub"])
                }
            }
        }).call(this, a("b17e"))
    }, "6f93": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {
                    directives: [{
                        name: "opacity-mask",
                        rawName: "v-opacity-mask",
                        value: !t.enabled,
                        expression: "!enabled"
                    }], staticClass: "main"
                }, [a("div", {staticClass: "mobile-image _ngcontent-pnp-76"}, [a("div", {staticClass: "preview-content _ngcontent-pnp-75"}, [a("div", {staticClass: "top-section-with-action _ngcontent-pnp-75"}, [t._m(0), a("div", {staticClass: "top-section _ngcontent-pnp-75"}, [a("div", {staticClass: "headline-group _ngcontent-pnp-75"}, [a("div", {staticClass: "_ngcontent-pnp-74"}, [a("a", {staticClass: "link headline _ngcontent-pnp-74"}, [t._v("\n                                Call " + t._s(t.phoneNumber) + "�� - " + t._s(t.businessName))])])]), a("div", {staticClass: "visurl _ngcontent-pnp-75"}, [a("span", {
                    staticClass: "badge _ngcontent-pnp-75",
                    attrs: {"aria-hidden": "true"}
                }, [t._v("Ad")]), a("span", {staticClass: "cite _ngcontent-pnp-75"}, [a("div", {staticClass: "_ngcontent-pnp-74"}, [t._v(t._s(t.url) + "��")])])])])]), a("hr", {staticClass: "line _ngcontent-pnp-75"}), a("div", {staticClass: "_ngcontent-pnp-74"}, [a("div", {staticClass: "description _ngcontent-pnp-74 adriel-ellipsis"}, [a("span", {staticClass: "_ngcontent-pnp-74 "}, [t._v("\n                        " + t._s(t.description) + "\n                    ")])])])])])])
            }, i = [function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "action _ngcontent-pnp-75 _nghost-pnp-33"}, [a("img", {attrs: {src: "/img/dashboard/phone.png"}})])
            }], r = a("6aed"), o = r["a"], s = (a("b173"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "3aa55f3d", null);
        e["a"] = l.exports
    }, 7268: function (t, e, a) {
        "use strict";
        var n = a("2c51"), i = a.n(n);
        i.a
    }, 7277: function (t, e, a) {
        "use strict";
        a.r(e);
        var n = function () {
            var t = this, e = t.$createElement, a = t._self._c || e;
            return a("div", {staticClass: "dashboard-parent-wrapper"}, [a("extend-days-modal", t._b({
                attrs: {active: t.isExtendDaysModalActive},
                on: {
                    "update:active": function (e) {
                        t.isExtendDaysModalActive = e
                    }, submit: t.extendCampaignDays
                }
            }, "extend-days-modal", t.propsToRunningStatus, !1)), a("extend-budget-modal", {
                attrs: {active: t.isExtendBudgetModalActive},
                on: {
                    "update:active": function (e) {
                        t.isExtendBudgetModalActive = e
                    }, submit: t.extendCampaignBudget
                }
            }), a("best-ad-modal", {
                attrs: {active: t.isBestAdModalActive, outerIndex: t.bestModalIndex},
                on: {
                    "update:active": function (e) {
                        t.isBestAdModalActive = e
                    }
                }
            }), a("form-modal", {
                staticClass: "form-edit-modal",
                attrs: {type: "formEdit", active: t.isFormEditActive, campaignId: t.campaignId},
                on: {
                    "update:active": function (e) {
                        t.isFormEditActive = e
                    }
                }
            }), a("dashboard-body", {
                attrs: {isLoading: t.isLoading},
                on: {runLonger: t.checkRunLonger, addBudget: t.checkAddBudget, "show:modal": t.showModal}
            })], 1)
        }, i = [], r = a("985c"), o = r["a"], s = a("2877"), l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["default"] = l.exports
    }, 7782: function (t, e, a) {
        "use strict";
        var n = a("8b05"), i = a.n(n);
        i.a
    }, "77a3": function (t, e, a) {
    }, "783a": function (t, e, a) {
    }, "799e": function (t, e, a) {
        "use strict";
        var n = a("ad00"), i = a.n(n);
        i.a
    }, "7abf": function (t, e, a) {
        "use strict";
        (function (t) {
            a("7514");
            var n = a("5176"), i = a.n(n), r = (a("ac6a"), a("a4bb")), o = a.n(r), s = a("cebc"),
                l = (a("c5f6"), a("c15c")), c = a("2f62"), d = a("43c9"), u = t.concat("#"),
                h = t.compose(Number, t.prop("clicks")), f = t.compose(Number, t.prop("impressions")),
                p = t.converge(t.divide, [h, f]), g = t.sortWith([t.descend(h), t.descend(f), t.descend(p)]);
            e["a"] = {
                mixins: [l["a"]],
                created: function () {
                },
                data: function () {
                    return {}
                },
                props: {},
                computed: Object(s["a"])({}, Object(c["mapGetters"])("dashboard", ["ads"]), {
                    _values: function () {
                        var t = this;
                        return g(o()(this.values).reduce(function (e, a) {
                            return e.push(i()({key: a}, t.values[a])), e
                        }, []))
                    }, available: function () {
                        return !!t.find(t.propEq("creativeType", "ExpandedTextAd"), this.ads)
                    }
                }),
                methods: {
                    getKey: function (t) {
                        var e = t.key;
                        return u(e)
                    }
                },
                components: {ImpressionClickBox: d["a"]}
            }
        }).call(this, a("b17e"))
    }, "7bb9": function (t, e, a) {
        "use strict";
        var n = a("783a"), i = a.n(n);
        i.a
    }, "7ee8": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "left-box"}, [a("div", {staticClass: "left-box--costs"}, [a("div", {staticClass: "left-box--costs--content"}, [a("div", {staticClass: "costs--title"}, [t._v(t._s(t.leftText))]), a("div", {staticClass: "costs--number"}, [t._v(t._s(t._formatCurrency(t.totalCost)))])]), a("div", {staticClass: "left-box--vertical-divider"}), a("div", {staticClass: "left-box--costs--content"}, [a("div", {staticClass: "costs--title"}, [t._v(t._s(t.rightText))]), a("div", {staticClass: "costs--number"}, [t._v(t._s(t._formatCurrency(t.includeFee(t.totalCost))))])])]), a("div", {staticClass: "left-box--divider"}), "spend" === t.type ? a("div", {staticClass: "left-box--bottom"}, [a("div", {staticClass: "bottom-box"}, [t.showLastDay ? a("div", {
                    staticClass: "bottom-box--title",
                    domProps: {textContent: t._s(t.lastDate)}
                }) : a("div", {staticClass: "bottom-box--title"}, [t._v(t._s(t.$t("DASHBOARD.today")))]), a("div", {staticClass: "bottom-box--number"}, [t._v(t._s(t._formatCurrency(t.todayCost)) + " (" + t._s(t._formatCurrency(t.includeFee(t.todayCost))) + ")")])]), a("div", {staticClass: "bottom-box"}, [a("div", {staticClass: "bottom-box--title"}, [t._v(t._s(t.$t("DASHBOARD.history_sub_title_budget")))]), a("div", {staticClass: "bottom-box--number"}, [t._v(t._s(t._formatCurrency(t.todayBudget)) + " (" + t._s(t._formatCurrency(t.includeFee(t.todayBudget))) + ")")])])]) : t._e(), a("div", {staticClass: "left-box--channels"}, [a("div", {staticClass: "channel"}, t._l(t.channels, function (e) {
                    return a("span", {key: e.channel}, [a("img", {attrs: {src: t.getLogo(e.channel)}})])
                }), 0), a("div", {staticClass: "amount"}, t._l(t.channels, function (e) {
                    return a("span", {key: e.channel}, [t._v(t._s(t._formatCurrency(e.value)))])
                }), 0)]), "budget" === t.type ? a("div", {staticClass: "left-box--comments"}, [a("span", {domProps: {innerHTML: t._s(t.$t("DASHBOARD.daily_budget_comment"))}})]) : t._e()])
            }, i = [], r = a("2a65"), o = r["a"], s = (a("9a60"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "3f0ea428", null);
        e["a"] = l.exports
    }, 8024: function (t, e, a) {
    }, "82e4": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-ads-slide", attrs: {tabindex: "0"}}, [a("div", {
                    ref: "container",
                    staticClass: "swiper",
                    class: t.idxClass
                }, [a("button", {ref: "prev", staticClass: "prev"}, [a("span", [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "chevron-left"
                    }
                })], 1)]), a("button", {ref: "next", staticClass: "next"}, [a("span", [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "chevron-right"
                    }
                })], 1)]), a("ul", {
                    ref: "list",
                    staticClass: "swiper__list",
                    style: t.listStyle
                }, t._l(t._ads, function (e, n) {
                    return a("li", {
                        key: t.adKey(e),
                        staticClass: "swiper__item",
                        class: [e.channel, e.creativeType, e.status],
                        on: {
                            click: function (e) {
                                return t.handleClick(n)
                            }
                        }
                    }, [a("div", {staticClass: "each-card"}, [a("div", {
                        staticClass: "each-card--head",
                        class: e.channel
                    }, [a("img", {attrs: {src: t.icon(e)}}), e.id ? a("span", {
                        directives: [{
                            name: "tooltip",
                            rawName: "v-tooltip",
                            value: t.$t("DASHBOARD.ad_id_tooltip"),
                            expression: "$t('DASHBOARD.ad_id_tooltip')"
                        }], staticClass: "ad-id"
                    }, [t._v(t._s(e.id))]) : t._e(), a("span", {
                        staticClass: "target",
                        domProps: {textContent: t._s(t.targetStr(e))}
                    })]), a("div", {staticClass: "each-card--body"}, [a("div", {staticClass: "pause-cover"}, [a("span", [t._v(t._s(t.$t("DASHBOARD.deactivated_ad")))])]), a("div", {staticClass: "dummy-space-top"}), a("best-preview", {
                        staticClass: "preview",
                        attrs: {values: t.getPreviewValue(e)},
                        on: {
                            "update:channelLink": function (t) {
                                e.channelLink = t
                            }
                        }
                    }), a("div", {staticClass: "dummy-space-bottom"})], 1), a("div", {staticClass: "each-card--bottom"}, [a("div", [a("span", {
                        directives: [{
                            name: "tooltip",
                            rawName: "v-tooltip",
                            value: t.$t("DASHBOARD.ads_slide_bottom_tooltip_click"),
                            expression: "$t('DASHBOARD.ads_slide_bottom_tooltip_click')"
                        }], staticClass: "each-card--bottom--each"
                    }, [a("b-icon", {
                        style: {color: "#999999"},
                        attrs: {pack: "fas", icon: "mouse-pointer"}
                    }), a("span", {staticClass: "value"}, [t._v(t._s(t.formatNumber(e.clicks)))])], 1), a("span", {
                        directives: [{
                            name: "tooltip",
                            rawName: "v-tooltip",
                            value: t.$t("DASHBOARD.ads_slide_bottom_tooltip_ctr"),
                            expression: "$t('DASHBOARD.ads_slide_bottom_tooltip_ctr')"
                        }], staticClass: "each-card--bottom--each ctr"
                    }, [a("img", {attrs: {src: "/img/icons/dashboard_ctr_icon.png"}}), a("span", {staticClass: "value"}, [t._v(t._s(t.toPercent(e.clicks / e.impressions)))])]), a("span", {
                        directives: [{
                            name: "tooltip",
                            rawName: "v-tooltip",
                            value: t.$t("DASHBOARD.ads_slide_bottom_tooltip_totalCost"),
                            expression: "$t('DASHBOARD.ads_slide_bottom_tooltip_totalCost')"
                        }], staticClass: "each-card--bottom--each"
                    }, [a("b-icon", {
                        style: {color: "#999999"},
                        attrs: {pack: "far", icon: "usd-circle"}
                    }), a("span", {staticClass: "value"}, [t._v(t._s(t._formatCurrency(e.spend)))])], 1), a("span", {
                        directives: [{
                            name: "tooltip",
                            rawName: "v-tooltip",
                            value: t.$t("DASHBOARD.ads_slide_bottom_tooltip_cpc"),
                            expression: "$t('DASHBOARD.ads_slide_bottom_tooltip_cpc')"
                        }], staticClass: "each-card--bottom--each cpc"
                    }, [a("img", {attrs: {src: "/img/icons/dashboard_cpc_icon.png"}}), a("span", {staticClass: "value"}, [t._v(t._s(t._formatCurrency(e.cpc)))])])])])])])
                }), 0)])])
            }, i = [], r = a("d588"), o = r["a"], s = (a("7bb9"), a("799e"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, 8367: function (t, e, a) {
    }, "85c2": function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "left-box"}, [a("div", {staticClass: "top-space"}, [a("b-select", {
                    staticClass: "conversion-selector",
                    attrs: {expanded: ""},
                    model: {
                        value: t.displayConversion, callback: function (e) {
                            t.displayConversion = e
                        }, expression: "displayConversion"
                    }
                }, t._l(t.conversionTypes, function (e) {
                    return a("option", {key: e, domProps: {value: e}}, [t._v(t._s(e))])
                }), 0)], 1), t.showLastDay ? a("div", {
                    staticClass: "left-box--today-title",
                    domProps: {textContent: t._s(t.lastDate)}
                }) : a("div", {
                    staticClass: "left-box--today-title",
                    domProps: {textContent: t._s(t.$t("DASHBOARD.today"))}
                }), a("div", {staticClass: "left-box--today-contents"}, [a("span", {staticClass: "first"}, [a("span", {staticClass: "numbers"}, [t._v(t._s(t.mainFormat(t.todayMain)))])]), a("span", {staticClass: "second"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t.subFormat(t.todaySub)))]), a("span", {staticClass: "text"}, [t._v(t._s(t.$t("DASHBOARD.clicks")))])]), a("span", {staticClass: "third"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t.percentFormat(t.todayPercent / 100)))])])]), a("div", {staticClass: "left-box--divider"}), a("div", {
                    staticClass: "left-box--all-title",
                    domProps: {textContent: t._s(t.$t("DASHBOARD.all_time"))}
                }), a("div", {staticClass: "left-box--all-contents"}, [a("span", {staticClass: "first"}, [a("span", {staticClass: "numbers"}, [t._v(t._s(t.mainFormat(t.allTimeMain)))])]), a("span", {staticClass: "second"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t.subFormat(t.allTimeSub)))]), a("span", {staticClass: "text"}, [t._v(t._s(t.$t("DASHBOARD.clicks")))])]), a("span", {staticClass: "thrid"}, [a("span", {staticClass: "numbers bullet-before"}, [t._v(t._s(t.percentFormat(t.allTimePercent / 100)))])])]), a("div", {staticClass: "left-box--channels"}, [a("div", {staticClass: "channel"}, t._l(t.channels, function (e) {
                    return a("span", {key: e.channel}, [a("img", {attrs: {src: t.getLogo(e.channel)}})])
                }), 0), a("div", {staticClass: "clicks"}, t._l(t.channels, function (e) {
                    return a("span", {key: e.channel, domProps: {textContent: t._s(t.mainFormat(e.main))}})
                }), 0), a("div", {staticClass: "views"}, t._l(t.channels, function (e) {
                    return a("span", {
                        key: e.channel,
                        staticClass: "bullet-before",
                        domProps: {textContent: t._s(t.subFormat(e.sub))}
                    })
                }), 0), a("div", {staticClass: "ctrs"}, t._l(t.channels, function (e) {
                    return a("span", {
                        key: e.channel,
                        staticClass: "bullet-before",
                        domProps: {textContent: t._s(t.toPercent((e.ratio || 0) / 100))}
                    })
                }), 0)])])
            }, i = [], r = a("3f84"), o = r["a"], s = (a("206b"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, "75bcccab", null);
        e["a"] = l.exports
    }, "88e7": function (t, e, a) {
        "use strict";
        var n = a("3dd5"), i = a.n(n);
        i.a
    }, "8b05": function (t, e, a) {
    }, "911a": function (t, e, a) {
    }, "914f": function (t, e, a) {
        "use strict";
        (function (t) {
            a("a481"), a("28a5"), a("b54a");
            var n = a("cebc"), i = a("795b"), r = a.n(i), o = a("fa7d"), s = a("dde5"), l = a("4e4d"), c = a("fe51"),
                d = a("2f62"), u = a("608d"), h = a("1187"), f = a("ed3a"), p = a("82e4"), g = a("6f9a"), m = a("3be3"),
                v = a("046b"), b = a("b183"), y = a("9225");
            e["a"] = {
                name: "Dashboard",
                created: function () {
                    var t = this;
                    return new r.a(function (e, a) {
                        t.$store.dispatch("form/restApi", ["index", [t.campaignId, "C"], e, a])
                    }).then(function (e) {
                        t.form = e
                    }).catch(function (t) {
                        console.log(t)
                    })
                },
                props: {isLoading: {type: Boolean, default: !1}},
                components: {
                    "top-title": u["a"],
                    "left-nav": l["a"],
                    "a-comp": h["a"],
                    separator: m["a"],
                    "no-result": v["a"],
                    "update-title": c["a"],
                    "running-status": g["a"],
                    "mobile-nav": b["a"],
                    HistoryCard: f["a"],
                    AdsSlide: p["a"]
                },
                methods: Object(n["a"])({}, Object(d["mapMutations"])("dashboard", ["setCampaignTitle"]), {
                    getType: function (t) {
                        return this.channel + Object(o["fb"])(t)
                    }, setEditingCampaign: function () {
                        this.editingCampaign = {title: this.title, id: this.campaignId}
                    }, handleComplete: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                        this.editingCampaign = null, t && this.setCampaignTitle(t)
                    }, runLonger: function () {
                        this.$emit("runLonger")
                    }, handleClickAd: function (t) {
                        this.$emit("show:modal", {type: "best", value: t})
                    }, handleClickForm: function (t) {
                        this.$emit("show:modal", {type: "form", value: t})
                    }, goFormPage: function () {
                        window.open(this.form.link, "_blank")
                    }, downloadForm: function () {
                        var t = this;
                        return axios({
                            url: "forms/downloadExcel/".concat(this.form.id),
                            method: "GET",
                            responseType: "blob"
                        }).then(function (t) {
                            var e = t.data;
                            Object(o["h"])({
                                blob: new Blob([e], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}),
                                name: "Form.xlsx"
                            })
                        }).catch(function (e) {
                            alert(t.$t("MY_CAMPAIGNS.fail_to_export_excel")), console.log(e)
                        })
                    }, downloadExcel: function () {
                        var e = this;
                        s["d"].downloadExcel(this.campaignId).then(function (a) {
                            a.excel ? Object(o["h"])({
                                uri: Object(o["T"])(a.excel),
                                name: t.last(a.excel.split("/"))
                            }) : alert(e.$t("MY_CAMPAIGNS.fail_to_export_excel"))
                        }).catch(function (t) {
                            alert(e.$t("MY_CAMPAIGNS.fail_to_export_excel")), console.error(t)
                        })
                    }, deleteTag: function (t) {
                        return t.replace(/<br>/g, "")
                    }
                }),
                data: function () {
                    return {
                        channel: "all",
                        editingCampaign: null,
                        selectedData: {parent: "clicks", child: "clicks"},
                        selectedCost: {parent: "cpc", child: "cpc"},
                        selectedPeriod: 7,
                        costPeriod: 7,
                        displayConversion: "",
                        form: "",
                        lang: y["a"]._vm.$root.locale
                    }
                },
                computed: Object(n["a"])({}, Object(d["mapState"])("form", ["formEdit"]), Object(d["mapGetters"])("dashboard", ["loaded", "title", "allDetails", "bestChannel", "clicks", "cpc", "daysLeft", "asap", "startDate", "endDate", "sales", "status", "conversionAvailable", "selectedConversion", "conversionValue", "conversionTopValue", "continuously", "deactivatedAt", "isSample", "graph", "startedAt", "ads", "graphDataVideoAvailable", "conversionTypes"]), {
                    campaignId: t.path(["$route", "params", "id"]),
                    types: function () {
                        return ["timeSpentOnSite", "keywords", "newAndReturning", "location", "age", "gender"]
                    },
                    channelImg: function () {
                        var t = this.bestChannel;
                        if (t) return "/img/dashboard/".concat(t, "_logo.png")
                    },
                    deactivated: t.propEq("status", "deactivated"),
                    propsToRunningStatus: t.pick(["asap", "deactivated", "daysLeft", "startDate", "endDate", "continuously", "startedAt", "deactivatedAt"]),
                    extendText: function () {
                        switch (this.status) {
                            case"running":
                                return this.$t("DASHBOARD.run_longer");
                            case"deactivated":
                                return this.$t("DASHBOARD.run_again")
                        }
                    },
                    isRunLongerVisible: function () {
                        switch (this.status) {
                            case"running":
                                return !this.continuously;
                            case"deactivated":
                                return !0
                        }
                        return !1
                    },
                    dataTabs: function () {
                        var e = ["clicks", "conversions"];
                        return Object(o["N"])(this.conversionTypes) || (e = t.without(["conversions"], e)), e
                    },
                    adsLength: function () {
                        return (this.ads || []).length
                    },
                    _displayConversion: {
                        get: function () {
                            return this.displayConversion || t.head(this.conversionTypes || [])
                        }, set: function (t) {
                            this.displayConversion = t
                        }
                    }
                })
            }
        }).call(this, a("b17e"))
    }, "931d": function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("5176"), i = a.n(n), r = a("cebc"), o = (a("c5f6"), a("2d2d")), s = a("381f"), l = a("a88a"),
                c = a("fa7d"), d = a("2f62"), u = a("edad"), h = a("6e77"), f = a("1585"), p = a("ebb6"), g = a("5670"),
                m = a("a772"),
                v = t.cond([[m["o"], t.compose(c["R"], t.path(["ad", "bestPosition"]))], [m["q"], t.always("Display Ad")], [m["r"], t.always("Search Ad")], [m["t"], t.always("Google universalApp")], [m["m"], t.always("Apple appstore")], [t.T, t.always("")]]);
            e["a"] = {
                created: function () {
                    this.watchOuterIndex$(), this.watchKeyboard$()
                },
                props: {active: {type: Boolean, default: !1}, outerIndex: {type: [Number, String], default: 0}},
                data: function () {
                    return {currentPage: 1}
                },
                components: {tags: o["a"], cost: s["a"], result: l["a"], "best-preview": u["a"]},
                computed: Object(r["a"])({}, Object(d["mapGetters"])("dashboard", ["isSample", "ads"]), {
                    adsLength: t.path(["ads", "length"]),
                    _active: {
                        get: t.prop("active"), set: function (t) {
                            this.$emit("update:active", t)
                        }
                    },
                    ad: function () {
                        return t.path(["_ads", this.currentPage - 1], this)
                    },
                    channel: t.path(["ad", "channel"]),
                    adType: function () {
                        switch (this.ad.creativeType) {
                            case"ResponsiveDisplayAd":
                                return "google-display";
                            case"ExpandedTextAd":
                                return "google-text"
                        }
                    },
                    logoUrl: function () {
                        var t = this.channel;
                        if (t) return "/img/dashboard/".concat(t, "_logo.png")
                    },
                    adTypeText: v,
                    bestWebsites: t.pathOr([], ["ad", "bestWebsites"]),
                    target: function () {
                        t.pathOr(!1, ["ad", "remarketing"], this);
                        return t.ifElse(t.compose(t.equals(!0), t.pathOr(!1, ["ad", "remarketing"])), t.always("retargeting"), t.always("newCustomer"))(this)
                    },
                    targetStr: function () {
                        return this.$t("retargeting" === this.target ? "DASHBOARD.re_target" : "DASHBOARD.new_target")
                    },
                    launchedClass: function () {
                        return this.launched ? "campaign-launched" : "campaign-not-launched"
                    },
                    launched: t.either(t.pathSatisfies(t.gt(t.__, 0), ["ad", "impressions"]), t.pathSatisfies(t.either(t.equals("RUNNING"), t.equals("PAUSE")), ["ad", "status"])),
                    creativeMedia: function () {
                        var e = t.path(["ad", "creative"], this);
                        if (!e) return null;
                        var a = e.media || e.image;
                        return a ? Object(c["U"])(a) : null
                    },
                    _ads: t.pipe(t.prop("ads"), t.defaultTo([]), t.partition(function (t) {
                        var e = t.status;
                        return "PAUSE" !== e
                    }), t.flatten)
                }),
                methods: {
                    propsToModal: function () {
                        return Object(r["a"])({}, this.ad.creative, t.pick(["id", "channel", "creativeType", "bestPosition"], this.ad), {creative: this.ad.creative})
                    },
                    setPage: function (t) {
                        var e = Math.min(this.adsLength, t);
                        this.currentPage = Math.max(1, e)
                    },
                    _formatNumber: function (t) {
                        return Object(c["n"])(t, void 0, "N/A")
                    },
                    toPercent: c["db"],
                    _formatCurrency: function (e) {
                        return t.compose(this.formatCurrency, this._formatNumber)(e)
                    },
                    watchOuterIndex$: function () {
                        var e = this,
                            a = this.$watchAsObservable("outerIndex", {immediate: !0}).pipe(Object(f["a"])("newValue"), Object(p["a"])(t.max(0)), Object(p["a"])(function (a) {
                                return t.min(a, e.ads.length - 1)
                            }), Object(g["a"])(function (t) {
                                return !isNaN(t) && t > -1
                            }));
                        this.$subscribeTo(a, function (a) {
                            return e.currentPage = t.inc(a)
                        })
                    },
                    watchKeyboard$: function () {
                        var e = this,
                            a = Object(h["a"])(document, "keydown").pipe(Object(f["a"])("keyCode"), Object(g["a"])(t.contains(t.__, [37, 39])));
                        this.$subscribeTo(a, function (a) {
                            37 == a ? e.setPage(t.dec(e.currentPage)) : e.setPage(t.inc(e.currentPage))
                        })
                    },
                    handleDirectUrl: function (t) {
                        i()(this.ad, {directUrl: t}), this.$forceUpdate()
                    },
                    getObjective: function (t) {
                        var e = t.channel, a = t.objective;
                        return "google" === e ? "Link Clicks" : "APP_INSTALLS" === a ? "App Installs" : "Link Clicks"
                    },
                    getUrl: function (t) {
                        return decodeURI(t.creative.url)
                    },
                    extractValues: t.compose(t.map(t.ifElse(t.is(Object), t.prop("text"), t.identity)), t.defaultTo([]))
                }
            }
        }).call(this, a("b17e"))
    }, "938d": function (t, e, a) {
        "use strict";
        var n = a("d313"), i = a.n(n);
        i.a
    }, "985c": function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("cebc"), i = (a("c5f6"), a("6b7e")), r = a("4b4c"), o = a("3d62"), s = a("367f"), l = a("a5dd"),
                c = a("4c6b"), d = a("2f62"), u = a("54da"), h = a("808d"), f = a("d3fb"), p = a("d96a"),
                g = t.is(Number);
            e["a"] = {
                watch: {
                    campaignId: {
                        handler: function (t) {
                            var e = this;
                            this.isLoading = !0, Object(u["a"])(Object(h["a"])(1500), Object(f["a"])(this.getReport(this.campaignId))).pipe(Object(p["a"])(function () {
                                return e.isLoading = !1
                            })).subscribe(), Object(u["a"])(Object(h["a"])(1500), Object(f["a"])(this.getReportGraph(this.campaignId))).subscribe()
                        }, immediate: !0
                    }
                },
                data: function () {
                    return {
                        isExtendDaysModalActive: !1,
                        isExtendBudgetModalActive: !1,
                        isBestAdModalActive: !1,
                        isFormEditActive: !1,
                        bestModalIndex: 0,
                        isLoading: !1
                    }
                },
                methods: Object(n["a"])({}, Object(d["mapActions"])("dashboard", ["extendCampaign", "getReport", "getReportGraph"]), Object(d["mapMutations"])("dashboard", ["setReport"]), {
                    checkRunLonger: function () {
                        if (this.isRunLongerVisible) switch (this.status) {
                            case c["d"].RUNNING:
                                return void(this.isExtendDaysModalActive = !0);
                            case c["d"].DEACTIVATED:
                            case c["d"].SAMPLE:
                                return this.showMessageModal()
                        }
                    }, checkAddBudget: function () {
                        switch (this.status) {
                            case c["d"].DEACTIVATED:
                            case c["d"].IN_PROGRESS:
                            case c["d"].SAMPLE:
                                return this.showMessageModal();
                            default:
                                return void(this.isExtendBudgetModalActive = !0)
                        }
                    }, extendCampaignDays: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        t = Number(t), t > 0 && g(t) && this.extendCampaign({days: t})
                    }, extendCampaignBudget: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        t = Number(t), t > 0 && g(t) && this.extendCampaign({budget: t})
                    }, showModal: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, e = t.type,
                            a = t.value;
                        "best" === e ? (this.isBestAdModalActive = !0, this.bestModalIndex = a || 0) : "form" === e && (this.isFormEditActive = !0)
                    }, showMessageModal: function () {
                        this.messageBefore({
                            text: this.$t("MY_CAMPAIGNS.check_plan_confirm"),
                            btnText: this.$t("MY_CAMPAIGNS.go_to_snb"),
                            cb: function () {
                                this.pushToRoute({name: "Proposal/Plan", params: {id: this.campaignId}})
                            }.bind(this)
                        })
                    }
                }),
                beforeDestroy: function () {
                    this.setReport({})
                },
                components: {
                    DashboardBody: i["a"],
                    ExtendDaysModal: r["a"],
                    ExtendBudgetModal: o["a"],
                    BestAdModal: s["a"],
                    FormModal: l["a"]
                },
                computed: Object(n["a"])({}, Object(d["mapGetters"])("dashboard", ["status", "continuously", "schedule", "deactivatedAt", "daysLeft", "deactivatedAt", "startedAt"]), {
                    propsToRunningStatus: function () {
                        return Object(n["a"])({}, t.pick(["deactivated", "daysLeft", "deactivatedAt", "startedAt"], this), t.pick(["startDate", "endDate", "continuously", "asap"], this.schedule))
                    }, campaignId: t.path(["$route", "params", "id"]), isRunLongerVisible: function () {
                        switch (this.status) {
                            case"running":
                                return !this.continuously;
                            case"deactivated":
                                return !0
                        }
                        return !1
                    }
                })
            }
        }).call(this, a("b17e"))
    }, "9a60": function (t, e, a) {
        "use strict";
        var n = a("2c6e"), i = a.n(n);
        i.a
    }, "9ead": function (t, e, a) {
    }, "9fa2": function (t, e, a) {
    }, a5dd: function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("b-modal", {
                    staticClass: "dashboard-bestAd-modal",
                    attrs: {active: t._active, canCancel: ["escape", "outside"]},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }, close: function (e) {
                            return t.$emit("close")
                        }
                    }
                }, [a("div", {staticClass: "form-edit-container"}, [a("div", {staticClass: "form-edit--top-wrapper"}, [t._v("\n            " + t._s(t.$t("FORM.edit_title")) + "\n            "), a("img", {
                    staticClass: "close-btn",
                    attrs: {src: "/img/icons/modal_close_btn.png"},
                    on: {click: t.close}
                })]), a("Form", {
                    staticClass: "form-edit--body",
                    attrs: {
                        type: "formEdit" == t.type ? "edit" : "",
                        page: "formEdit",
                        src: t.formEdit && t.formEdit.img,
                        edit_CampaignId: t.campaignId
                    }
                })], 1), a("footer", {staticClass: "modal-card-foot"}, [a("div", {staticClass: "form-edit--bottom-wrapper"}, [a("button", {
                    staticClass: "button left",
                    attrs: {type: "button"},
                    on: {click: t.close}
                }, [t._v(t._s(t.$t("FORM.edit_btn1")))]), a("button", {
                    staticClass: "button is-primary right",
                    on: {click: t.clickSave}
                }, [t._v(t._s(t.$t("FORM.edit_btn2")))])])])])
            }, i = [], r = a("071c"), o = r["a"], s = (a("7782"), a("2fea"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, a6cf: function (t, e, a) {
        "use strict";
        var n = a("af5e"), i = a.n(n);
        i.a
    }, a88a: function (t, e, a) {
        "use strict";
        var n = function () {
            var t = this, e = t.$createElement, a = t._self._c || e;
            return a("div", {staticClass: "bar-box-wrapper"}, [a("div", {staticClass: "bar-box-element"}, [a("div", {staticClass: "bar-graph-wrapper"}, [a("div", {staticClass: "bar-graph-bg"}, [a("div", {
                staticClass: "bar-graph-elem",
                class: {"add-min": t.getRatio() < 25},
                style: t.getWidthStyle()
            }, [a("div", {staticClass: "bar-graph-text"}, [a("span", [t._v(t._s(t.values.clicks))]), a("span", [t._v(t._s(t.$t("DASHBOARD.clicks")))])])])])]), a("div", {staticClass: "bar-box-right-text"}, [a("span", [t._v(t._s(t.values.impressions || 0))]), a("span", [t._v(t._s(t.$t("DASHBOARD.views")))])])])])
        }, i = [], r = (a("ac6a"), {
            created: function () {
            },
            data: function () {
                return {}
            },
            methods: {
                getWidthStyle: function () {
                    return {width: "".concat(this.getRatio(), "%"), "background-color": this.mainColor}
                }, getRatio: function () {
                    var t = this.values, e = t.clicks, a = t.impressions;
                    if (!e) return 0;
                    var n = e / a / .05 * 100;
                    return Math.min(100, n)
                }
            },
            props: {values: {type: [Array, Object], required: !0}, mainColor: {type: String, default: "red"}},
            computed: {}
        }), o = r, s = (a("eba2"), a("2877")), l = Object(s["a"])(o, n, i, !1, null, "09ad9577", null);
        e["a"] = l.exports
    }, ad00: function (t, e, a) {
    }, af5e: function (t, e, a) {
    }, b173: function (t, e, a) {
        "use strict";
        var n = a("f354"), i = a.n(n);
        i.a
    }, b422: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("75fc"), i = a("cebc"), r = a("dde5"), o = a("2f62"), s = a("fa7d");
            e["a"] = {
                props: ["id", "channel", "bestPosition", "creative"],
                created: function () {
                },
                data: function () {
                    return {url: "", failedPreview: !1}
                },
                watch: {
                    id: {
                        handler: function (t) {
                            this.url = "", this.failedPreview = !1, this.fetchPreview()
                        }, immediate: !0
                    }
                },
                computed: Object(i["a"])({}, Object(o["mapGetters"])("dashboard", ["campaignId", "isSample"]), {
                    position: function () {
                        if (!this.failedPreview && this.bestPosition) return this.bestPosition;
                        switch (this.channel) {
                            case"facebook":
                                return "desktop_feed_standard";
                            case"instagram":
                                return "instagram_standard"
                        }
                    }, creativeMedia: function () {
                        var t = Object(s["V"])(this.creative), e = t.media, a = t.image;
                        return e || a
                    }
                }),
                methods: {
                    fetchPreview: function () {
                        var e = this;
                        r["d"].getFacebookPreview.apply(r["d"], Object(n["a"])(t.props(["campaignId", "id", "position"], this))).then(function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            e.url = t.previewLink || t.iframeSrc, t.channelLink && e.$emit("update:channelLink", t.channelLink)
                        }).catch(function (t) {
                            var a = t.response;
                            a && 400 == a.status && (e.failedPreview = !0, e.fetchPreview())
                        })
                    }
                }
            }
        }).call(this, a("b17e"))
    }, b54a: function (t, e, a) {
        "use strict";
        a("386b")("link", function (t) {
            return function (e) {
                return t(this, "a", "href", e)
            }
        })
    }, bf3b: function (t, e, a) {
    }, c15c: function (t, e, a) {
        "use strict";
        var n = a("cebc"), i = a("19e8"), r = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "not-available-wrapper"}, [a("span", {staticClass: "not-available-slot"}, [t._t("default")], 2), t.imgUrl ? a("img", {
                    staticClass: "not-available-img",
                    attrs: {src: t.imgUrl}
                }) : t._e(), a("span", {staticClass: "not-available"}, [t._v(t._s(t.text || t.$t("DASHBOARD.service_in_prepare")))]), t._t("not-available-bottom", [a("span", {
                    staticClass: "ment-no-help",
                    on: {click: t.openHelp}
                }, [t._v("\n            " + t._s(t.$t("DASHBOARD.adriel_help")) + "\n        ")])])], 2)
            }, o = [], s = {props: ["imgUrl", "text"]}, l = s, c = (a("938d"), a("2877")),
            d = Object(c["a"])(l, r, o, !1, null, "621abb7c", null), u = d.exports, h = a("2f62");
        e["a"] = {
            props: {
                editable: {type: Boolean, default: !1},
                className: {type: String, default: ""},
                values: {type: [Array, Object]},
                resetable: {type: Boolean, default: !1}
            },
            methods: {
                reset: function () {
                    this.$emit("edit:end"), this.$emit("edit:reset")
                }, updateValue: function (t) {
                    this.$emit("update:value", t)
                }
            },
            computed: Object(n["a"])({}, Object(h["mapGetters"])("dashboard", ["isSample"])),
            components: {"card-view": i["a"], "not-available": u}
        }
    }, c4a5: function (t, e, a) {
    }, c505: function (t, e, a) {
        "use strict";
        var n = a("bf3b"), i = a.n(n);
        i.a
    }, c582: function (t, e, a) {
    }, cab8: function (t, e, a) {
    }, cc91: function (t, e, a) {
    }, ce4b: function (t, e, a) {
        "use strict";
        (function (t) {
            e["a"] = {
                props: {
                    creative: {type: Object, default: t.always({})},
                    enabled: {type: Boolean, default: !0},
                    isLoading: {type: Boolean, default: !1}
                }
            }
        }).call(this, a("b17e"))
    }, d247: function (t, e, a) {
        "use strict";
        var n = a("c582"), i = a.n(n);
        i.a
    }, d313: function (t, e, a) {
    }, d3c0: function (t, e, a) {
        "use strict";
        var n, i, r = a("5474"), o = r["a"], s = a("2877"), l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, d588: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("cebc"), i = a("768b"), r = a("edad"), o = a("fa7d"), s = a("6e77"), l = a("a748"), c = a("c4cc"),
                d = a("ebb6"), u = a("742d"), h = a("a744");
            e["a"] = {
                name: "dashboard-ads-slide",
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        t.init()
                    })
                },
                components: {BestPreview: r["a"]},
                props: {ads: {type: Array, default: t.always([])}},
                data: t.always({index: 0, move: 0}),
                computed: {
                    length: t.path(["ads", "length"]), idxClass: function () {
                        var t = this.index, e = [];
                        return 0 === t && e.push("first"), t === this.lastIndex && e.push("last"), e.join(" ")
                    }, listStyle: function () {
                        return {transform: "translateX(".concat(this.move, "px)")}
                    }, lastIndex: function () {
                        return Math.floor(t.dec(this.ads.length) / 3)
                    }, _ads: t.pipe(t.prop("ads"), t.defaultTo([]), t.partition(function (t) {
                        var e = t.status;
                        return "PAUSE" !== e
                    }), t.flatten)
                },
                methods: {
                    init: function () {
                        var e = this, a = t.props(["prev", "next"], this.$refs), n = Object(i["a"])(a, 2), r = n[0],
                            o = n[1];
                        if (o && r) {
                            var f = Object(s["a"])(r, "click").pipe(Object(c["a"])(function (t) {
                                return t.stopPropagation()
                            }), Object(d["a"])(function () {
                                return t.ifElse(t.gt(t.__, 0), t.dec, t.identity)
                            })), p = Object(s["a"])(o, "click").pipe(Object(c["a"])(function (t) {
                                return t.stopPropagation()
                            }), Object(d["a"])(function () {
                                return t.ifElse(t.lt(t.__, e.lastIndex), t.inc, t.identity)
                            })), g = Object(l["a"])(f, p).pipe(Object(u["a"])(function (t, e) {
                                return e(t)
                            }, 0), Object(h["a"])(0), Object(c["a"])(function (t) {
                                return e.index = t
                            }), Object(d["a"])(function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                                return t ? 75 - 290 * t * 3 : 0
                            }));
                            this.$subscribeTo(g, function (t) {
                                return e.move = t
                            }, function (t) {
                                return console.log(t)
                            })
                        }
                    },
                    adKey: t.pipe(t.props(["channel", "creativeType", "id", "remarketing"]), o["k"], t.join("::")),
                    icon: function (t) {
                        var e = t.channel;
                        return "/img/dashboard/header_".concat(e, "_logo.png")
                    },
                    targetStr: function (e) {
                        return this.$t(t.propEq("remarketing", !0, e) ? "DASHBOARD.re_target" : "DASHBOARD.new_target")
                    },
                    getPreviewValue: function (e) {
                        return e ? Object(n["a"])({}, e.creative, t.pick(["creative", "id", "channel", "creativeType", "bestPosition"], e)) : {}
                    },
                    handleClick: function (t) {
                        this.$emit("click:ad", t)
                    },
                    toPercent: o["db"],
                    _formatNumber: function (t) {
                        return Object(o["n"])(t, void 0, "N/A")
                    },
                    _formatCurrency: function (e) {
                        return t.compose(this.formatCurrency, this._formatNumber)(e)
                    }
                }
            }
        }).call(this, a("b17e"))
    }, d5f9: function (t, e, a) {
        "use strict";
        var n = a("cc91"), i = a.n(n);
        i.a
    }, ddae: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("cebc"), i = (a("ac6a"), a("fa7d")), r = a("621e"), o = a("65d1"), s = a("5d0e"), l = a("9dd8"),
                c = a("6f93"), d = a("6911"), u = a("a772");
            e["a"] = {
                components: {
                    GoogleSearch: r["a"],
                    GoogleDisplay: o["a"],
                    GoogleUniversalApp: l["a"],
                    "facebook-ad": s["a"],
                    "call-only": c["a"],
                    AppleSearch: d["a"]
                },
                props: {values: {type: Object, required: !0}},
                data: function () {
                    return {componentEditable: !1}
                },
                computed: {
                    adCompType: t.compose(t.cond([[u["o"], t.always("facebook-ad")], [u["q"], t.always("google-display")], [u["r"], t.always("google-search")], [u["t"], t.always("google-universal-app")], [u["m"], t.always("apple-search")]]), t.prop("values")),
                    _values: function () {
                        var t = Object(i["V"])(this.values, ["image", "logoImage"]);
                        return Object(n["a"])({}, t, {meta: {isEditing: !1}})
                    }
                }
            }
        }).call(this, a("b17e"))
    }, dec3: function (t, e, a) {
        "use strict";
        var n = a("3a9a"), i = a.n(n);
        i.a
    }, dffe: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("cebc"), i = (a("20d6"), a("ac6a"), a("a4bb")), r = a.n(i), o = (a("55dd"), a("e814")),
                s = a.n(o), l = a("c15c"), c = a("43c9"), d = function (t, e) {
                    return s()(t.key) - s()(e.key)
                };
            e["a"] = {
                mixins: [l["a"]], created: function () {
                }, data: function () {
                    return {}
                }, props: {}, computed: {
                    _values: function () {
                        var e = this, a = t.sort(d, r()(this.values).reduce(function (a, i) {
                            var r, o = s()(i);
                            r = o >= 55 ? "55-" : i;
                            var l = t.findIndex(t.propEq("key", r), a);
                            if (l > -1) {
                                var c = t.mergeWithKey(function (t, e, a) {
                                    return e + a
                                }, a[l], e.values[i]);
                                a.splice(l, 1, c)
                            } else a.push(Object(n["a"])({key: r}, e.values[i]));
                            return a
                        }, [])), i = t.findIndex(t.propEq("key", "unknown"), a);
                        if (i > -1) {
                            var o = a.splice(i, 1);
                            a.push(o[0])
                        }
                        return a
                    }
                }, methods: {
                    getImgUrl: function (t, e) {
                        return "/img/dashboard/age_icon".concat(e + 1, ".png")
                    }, getKey: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        return t.key
                    }
                }, components: {ImpressionClickBox: c["a"]}
            }
        }).call(this, a("b17e"))
    }, e1e5: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("5176"), i = a.n(n), r = a("a4bb"), o = a.n(r), s = (a("ac6a"), a("c5f6"), a("c15c")),
                l = a("43c9"), c = t.compose(Number, t.prop("clicks")), d = t.compose(Number, t.prop("impressions")),
                u = t.converge(t.divide, [c, d]), h = t.sortWith([t.descend(c), t.descend(u), t.descend(d)]);
            e["a"] = {
                mixins: [s["a"]],
                created: function () {
                },
                data: function () {
                    return {}
                },
                props: {},
                computed: {
                    _values: function () {
                        var t = this.values || {}, e = o()(t);
                        if (e.length) return h(e.reduce(function (e, a) {
                            return e.push(i()({key: a}, t[a])), e
                        }, []))
                    }
                },
                methods: {
                    getImgUrl: t.always("/img/dashboard/location_icon.png"), getKey: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        return t.key
                    }
                },
                components: {ImpressionClickBox: l["a"]}
            }
        }).call(this, a("b17e"))
    }, e26e: function (t, e, a) {
        "use strict";
        var n = a("9fa2"), i = a.n(n);
        i.a
    }, e3ff: function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {staticClass: "dashboard-age-wrapper"}, [a("card-view", {
                    attrs: {
                        label: t.$t("DASHBOARD.card_title_age"),
                        className: t.className,
                        editable: t.editable
                    }
                }, [a("impression-click-box", {
                    attrs: {
                        getImgUrl: t.getImgUrl,
                        getKey: t.getKey,
                        values: t._values
                    }
                })], 1)], 1)
            }, i = [], r = a("dffe"), o = r["a"], s = (a("7268"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, e465: function (t, e, a) {
        "use strict";
        var n = a("54eb4"), i = a.n(n);
        i.a
    }, eba2: function (t, e, a) {
        "use strict";
        var n = a("c4a5"), i = a.n(n);
        i.a
    }, ebe7: function (t, e, a) {
        "use strict";
        var n = a("6deb"), i = a.n(n);
        i.a
    }, ed3a: function (t, e, a) {
        "use strict";
        var n = function () {
                var t = this, e = t.$createElement, a = t._self._c || e;
                return a("div", {
                    staticClass: "dashboard-history-card",
                    class: []
                }, [a("div", {staticClass: "dashboard-history-card--header"}, t._l(t.tabs, function (e) {
                    return a("span", {
                        key: e,
                        staticClass: "card-view-header-title",
                        class: {selected: e === t.selected.parent},
                        on: {
                            click: function (a) {
                                return t.updateParent(e)
                            }
                        }
                    }, [t._v("\n            " + t._s(t.getTabName(e)) + "\n        ")])
                }), 0), a("div", {
                    ref: "body",
                    staticClass: "dashboard-history-card--body"
                }, [t.displayConversion || !t.gaCardVisible || t.selectedConversion ? [a("div", {staticClass: "history-clicks-container"}, [a(t.leftType, {
                    tag: "component",
                    attrs: {summary: t.summary, "show-last-day": t.isDeactivated, "last-date": t.lastDate},
                    on: {"update:displayConversion": t.handleConversionChange}
                }), a("div", {staticClass: "right-box"}, [a("div", {
                    staticClass: "right-box--buttons",
                    class: {"single-btn": t.isSingleTab}
                }, [t._l(t.subTabs(t._parent), function (e) {
                    return a("button", {
                        key: e,
                        staticClass: "subTab",
                        class: [t.subTabClass(e), {selected: e === t.selected.child}],
                        on: {
                            click: function (a) {
                                return t.$emit("update:tab:child", e)
                            }
                        }
                    }, [a("span", {staticClass: "bullet"}), a("span", {domProps: {textContent: t._s(t.getTab(e))}})])
                }), a("b-select", {
                    staticClass: "period-selector",
                    attrs: {expanded: ""},
                    model: {
                        value: t._period, callback: function (e) {
                            t._period = e
                        }, expression: "_period"
                    }
                }, t._l(t.periods, function (e) {
                    return a("option", {
                        key: e,
                        domProps: {value: e}
                    }, [t._v("\n                            " + t._s(t.getPeriod(e)) + "\n                            ")])
                }), 0)], 2), a("history-mixed-chart", {
                    key: t.graphKey,
                    style: t.graphStyle,
                    attrs: {"chart-data": t.formatted, options: t._options}
                })], 1)], 1)] : [a("ga-history-card")]], 2)])
            }, i = [], r = a("5789"), o = r["a"], s = (a("0fd8d"), a("e26e"), a("2877")),
            l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, edad: function (t, e, a) {
        "use strict";
        var n = function () {
            var t = this, e = t.$createElement, a = t._self._c || e;
            return a("div", [a(t.adCompType, t._b({
                tag: "component",
                attrs: {editable: t.componentEditable, value: t._values},
                on: {
                    "update:channelLink": function (e) {
                        return t.$emit("update:channelLink", e)
                    }
                }
            }, "component", t._values, !1))], 1)
        }, i = [], r = a("ddae"), o = r["a"], s = a("2877"), l = Object(s["a"])(o, n, i, !1, null, null, null);
        e["a"] = l.exports
    }, ef42: function (t, e, a) {
        "use strict";
        var n = function () {
            var t = this, e = t.$createElement, a = t._self._c || e;
            return a("div", {staticClass: "history-ga"}, [a("div", {staticClass: "ga-connect"}, [a("img", {attrs: {src: "/img/dashboard/history_conversion.png"}}), a("span", {staticClass: "ga-text word-break"}, [t._v("\n            " + t._s(t.$t("DASHBOARD.connect_ga_on_proposal")) + "\n        ")]), a("router-link", {
                staticClass: "auth-btn google",
                attrs: {tag: "button", to: "/proposal/" + t.campaignId + "/audienceProfile"}
            }, [a("span", [t._v("\n                " + t._s(t.$t("DASHBOARD.go_to_set_goal")) + "\n            ")]), a("b-icon", {
                style: {
                    "font-size": "12px",
                    color: "white"
                }, attrs: {pack: "far", icon: "chevron-right"}
            })], 1)], 1)])
        }, i = [], r = a("cebc"), o = a("2f62"), s = (a("fa7d"), {
            created: function () {
            },
            props: {},
            computed: Object(r["a"])({}, Object(o["mapGetters"])("dashboard", ["isGaConnected", "campaignId", "selectedConversion"])),
            methods: {}
        }), l = s, c = (a("ebe7"), a("2877")), d = Object(c["a"])(l, n, i, !1, null, "591dca38", null);
        e["a"] = d.exports
    }, f354: function (t, e, a) {
    }, f79f: function (t, e, a) {
        "use strict";
        (function (t) {
            var n = a("cebc"), i = a("c15c"), r = a("2f62"), o = a("fa7d"), s = t.ifElse(isNaN, t.identity, Math.round);
            e["a"] = {
                mixins: [i["a"]],
                created: function () {
                },
                data: function () {
                    return {}
                },
                props: {},
                computed: Object(n["a"])({}, Object(r["mapGetters"])("dashboard", ["googleAnalytics"]), {
                    leftValue: function () {
                        return t.compose(s, t.path(["values", "timeSpentOnSite"]))(this)
                    }, bounceRate: function () {
                        return t.compose(s, t.path(["values", "bounceRate"]))(this)
                    }, isGaConnected: t.compose(o["N"], t.prop("googleAnalytics"))
                }),
                methods: {},
                components: {}
            }
        }).call(this, a("b17e"))
    }, f8c6: function (t, e, a) {
        "use strict";
        var n = a("8024"), i = a.n(n);
        i.a
    }
}]);
//# sourceMappingURL=dashboard.ce2e430a.js.map