(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sign"], {
    "0151": function (e, t, s) {
        "use strict";
        var a = s("2090"), i = s.n(a);
        i.a
    }, "02af": function (e, t, s) {
    }, "090f": function (e, t, s) {
    }, 1926: function (e, t, s) {
        "use strict";
        s.r(t);
        var a = function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("div", {staticClass: "reset-adriel-wrapper"}, [s("message-modal", {
                attrs: {
                    text: "Password has been changed",
                    active: e.isModalOpen,
                    btnText: "Close"
                }, on: {
                    "update:active": function (t) {
                        e.isModalOpen = t
                    }, close: e.redirect, btnClicked: e.redirect
                }
            }), s("div", {staticClass: "reset-adriel"}, [s("div", {staticClass: "forgot-info-wrapper"}, [s("span", {staticClass: "forgot-info-title"}, [e._v("\n                " + e._s(e.$t("RESET.title")) + "\n            ")])]), s("form", {
                staticClass: "sign-inputs auth",
                attrs: {autocomplete: "off"},
                on: {
                    submit: function (t) {
                        return t.preventDefault(), e.send(t)
                    }
                }
            }, [s("span", [s("span", [e._v(e._s(e.$t("RESET.new_password")))]), s("b-input", {
                attrs: {
                    placeholder: e.$t("RESET.password"),
                    type: "password",
                    autocomplete: "new-password"
                }, nativeOn: {
                    keypress: function (t) {
                        return !t.type.indexOf("key") && e._k(t.keyCode, "enter", 13, t.key, "Enter") ? null : e.reset(t)
                    }
                }, model: {
                    value: e.password, callback: function (t) {
                        e.password = t
                    }, expression: "password"
                }
            })], 1), s("span", [s("span", [e._v(e._s(e.$t("RESET.new_password_again")))]), s("b-input", {
                attrs: {
                    placeholder: e.$t("RESET.password"),
                    type: "password",
                    autocomplete: "new-password"
                }, nativeOn: {
                    keypress: function (t) {
                        return !t.type.indexOf("key") && e._k(t.keyCode, "enter", 13, t.key, "Enter") ? null : e.reset(t)
                    }
                }, model: {
                    value: e.passwordConfirm, callback: function (t) {
                        e.passwordConfirm = t
                    }, expression: "passwordConfirm"
                }
            })], 1)]), s("button", {on: {click: e.reset}}, [e._v(e._s(e.$t("RESET.next")))])])], 1)
        }, i = [], n = s("dde5"), r = (s("fa7d"), s("2f62"), {
            created: function () {
            }, data: function () {
                return {password: "", passwordConfirm: "", isModalOpen: !1}
            }, methods: {
                reset: function () {
                    var e = this;
                    if (this.password !== this.passwordConfirm) return alert("plase type same password");
                    n["c"].reset(this.$route.query.token, this.password).then(function (t) {
                        e.isModalOpen = !0
                    }).catch(function (e) {
                        console.log(e)
                    })
                }, redirect: function () {
                    this.$router.push({name: "Sign/Login"})
                }
            }
        }), o = r, c = (s("6a15"), s("2877")), l = Object(c["a"])(o, a, i, !1, null, null, null);
        t["default"] = l.exports
    }, "1c4f": function (e, t, s) {
        "use strict";
        s.r(t);
        var a = function () {
                var e = this, t = e.$createElement, s = e._self._c || t;
                return s("div", {staticClass: "sign-wrapper"}, [s("div", {staticClass: "sign-wrapper-top-container"}, [s("div", {staticClass: "sign-wrapper-top"}, [e._m(0), s("div", {staticClass: "sign-lang-wrapper"}, [s("span", {
                    class: {active: e.isLang("en")},
                    on: {
                        click: function (t) {
                            return e._setLang("en")
                        }
                    }
                }, [e._v("ENG")]), s("span", {
                    class: {active: e.isLang("ko")}, on: {
                        click: function (t) {
                            return e._setLang("ko")
                        }
                    }
                }, [e._v("KOR")]), s("span", {
                    class: {active: e.isLang("jp")}, on: {
                        click: function (t) {
                            return e._setLang("jp")
                        }
                    }
                }, [e._v("�ζ쑍沃�")])])])]), s("router-view"), s("adriel-footer")], 1)
            }, i = [function () {
                var e = this, t = e.$createElement, s = e._self._c || t;
                return s("a", {attrs: {href: "https://adriel.ai"}}, [s("img", {attrs: {src: "/img/logos/logo_n_f.svg"}})])
            }], n = (s("7f7f"), s("cebc")), r = function () {
                var e = this, t = e.$createElement, s = e._self._c || t;
                return s("div", {staticClass: "footer-wrapper"}, [s("div", {staticClass: "footer-body"}, [s("div", {staticClass: "footer-left"}, [s("div", {staticClass: "footer-address"}, [e._v("\n                " + e._s(e.$t("FOOTER.company_name")) + "\n            ")]), s("div", {staticClass: "footer-detail"}, [s("a", {attrs: {href: "mailto:contact@adriel.ai"}}, [e._v("contact@adriel.ai")]), s("br"), e._v("\n                " + e._s(e.$t("FOOTER.company_address")) + "\n                "), s("br"), e._v("\n                " + e._s(e.$t("FOOTER.company_info")) + "\n            ")])]), s("div", {staticClass: "footer-right"}, [s("div", {staticClass: "footer-sns"}, e._l(e.sns, function (t) {
                    return s("img", {
                        key: t,
                        attrs: {src: "/img/footer_" + t + ".png"},
                        on: {
                            click: function (s) {
                                return e.openSns(t)
                            }
                        }
                    })
                }), 0)])])])
            }, o = [], c = {
                data: function () {
                    return {sns: ["facebook", "instagram", "linkedin", "twitter"]}
                }, methods: {
                    openSns: function (e) {
                        var t;
                        switch (e) {
                            case"facebook":
                                t = "https://www.facebook.com/adrielai/";
                                break;
                            case"instagram":
                                t = "https://www.instagram.com/adriel_advertising/";
                                break;
                            case"linkedin":
                                t = "https://www.linkedin.com/company/adrielai/";
                                break;
                            case"twitter":
                                t = "https://twitter.com/AdrielAI1";
                                break
                        }
                        t && window.open(t, "_blank")
                    }
                }
            }, l = c, d = (s("38b7"), s("2877")), p = Object(d["a"])(l, r, o, !1, null, "ea397264", null), u = p.exports,
            v = s("fa7d"), f = s("2f62"), m = {
                components: {"adriel-footer": u}, created: function () {
                    if (!this.user) {
                        var e = Object(v["w"])();
                        this.$i18n.locale !== e && this._setLang(e)
                    }
                }, computed: Object(n["a"])({}, Object(f["mapGetters"])("user", ["user"])), methods: {
                    isLang: function (e) {
                        return Object(v["w"])() === e
                    }, _setLang: function (e) {
                        this.setLang(e, this.$route.name)
                    }
                }
            }, _ = m, g = (s("6c46"), Object(d["a"])(_, a, i, !1, null, "4b06e3d4", null));
        t["default"] = g.exports
    }, 2090: function (e, t, s) {
    }, "2fa9": function (e, t, s) {
        "use strict";
        (function (e) {
            s("386d"), s("6762"), s("2fdb");
            var a = s("5176"), i = s.n(a), n = s("db0c"), r = s.n(n), o = s("cebc"), c = s("dde5"), l = s("146f"),
                d = s("b271"), p = s("af2e"), u = s("fa7d"), v = s("2f62");
            t["a"] = {
                mounted: function () {
                    "uk_promotion_10" === this.queryStringThenRemove("from") ? this.additionalText = "with free 짙10 voucher" : this.isLangType("ko") || (this.additionalText = "Claim free $10 voucher"), this.$nextTick(function () {
                        var e = document.querySelector("input");
                        e && e.focus()
                    })
                },
                data: function () {
                    return {
                        email: "",
                        password: "",
                        passwordConfirm: "",
                        websiteUrl: "",
                        referral: "",
                        isModalOpen: !1,
                        agreeTerms: !1,
                        isMessageModalActive: !1,
                        isTemplateModalActive: !1,
                        messageModalText: "",
                        referralDisabled: !1,
                        modalType: "",
                        lang: "en",
                        additionalText: "",
                        notValids: {},
                        agreesKo: {
                            privacy: {value: !1, expanded: !1},
                            service: {value: !1, expanded: !1},
                            marketing: {value: !1, expanded: !1},
                            personalized: {value: !1, expanded: !1}
                        },
                        phCoupon: !1
                    }
                },
                computed: {
                    serviceImg: function () {
                        var e = "/img/ko_service_";
                        return window.innerWidth > 600 ? e + "big.png" : e + "small.png"
                    }, slides: function () {
                        var t = this;
                        return e.times(function (s) {
                            return {
                                title: t.$t("JOIN.adriel_offer_title_".concat(s)),
                                mainImage: t.$t("JOIN.adriel_offer_image_".concat(s)),
                                contents: e.defaultTo([], t.$t("JOIN.adriel_offer_contents_".concat(s)))
                            }
                        }, 4)
                    }, dest: function () {
                        var t = e.path(["$route", "query", "dest"])(this);
                        return "Express" != t && (t = "MyCampaigns"), t
                    }
                },
                components: {TermsAndPolicy: l["default"], ServicePreview: d["a"], OauthBtn: p["a"]},
                methods: Object(o["a"])({}, Object(v["mapMutations"])("user", ["login"]), Object(v["mapActions"])("user", ["checkSession"]), {
                    signUp: function () {
                        var t = this, s = this.email, a = this.password, i = this.passwordConfirm, n = this.websiteUrl;
                        if (!Object(u["K"])(s)) return this.toast(this.$t("JOIN.invalid_email_format"));
                        if (a !== i) return this.toast(this.$t("JOIN.password_not_match"));
                        if ("ko" === this.lang) {
                            if (!this.agreesKo.privacy.value) return this.toast(this.$t("JOIN.should_check_terms"));
                            if (!this.agreesKo.service.value) return this.toast(this.$t("JOIN.should_check_terms"))
                        } else if (!this.agreeTerms) return this.notValids = Object(o["a"])({}, this.notValids, {terms: !0}), this.toast(this.$t("JOIN.should_check_terms"));
                        var r = "Asia/Seoul";
                        try {
                            r = Intl.DateTimeFormat().resolvedOptions().timeZone
                        } catch (l) {
                            console.error("cannot get timezone")
                        }
                        c["c"].register({
                            email: s,
                            password: a,
                            websiteUrl: n,
                            token: this.$route.query.token,
                            referral: this.referral,
                            language: Object(u["w"])(),
                            timezone: r
                        }).then(function (s) {
                            t.$ga.event({
                                eventCategory: "App",
                                eventAction: "Sign",
                                eventLabel: "Register",
                                eventValue: 1
                            }), fbq("track", "CompleteRegistration"), t.$intercom.trackEvent("Register"), t.caulyTrackerEvent("CA_signup"), t.checkSession().then(function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                "GB" === e.country && ire("trackConversion", 17087, {
                                    orderId: "signUp::email::".concat(e.id),
                                    customerId: e.id
                                })
                            }), t.login(s), t.messageModalText = t.$t("JOIN.welcome_content", {email: e.propOr("-", "email", s)}), t.isMessageModalActive = !0
                        }, function (e) {
                            if (e.response) {
                                var s = e.response.data;
                                if (s) {
                                    var a = s.code;
                                    return "PASSWORD_TOO_SHORT" === a ? t.toast(t.$t("JOIN.password_too_short")) : "EMAIL_ALREADY_EXIST" === a ? t.toast(t.$t("JOIN.email_exist")) : t.toast(t.$t("JOIN.failed_to_create_account"))
                                }
                            }
                        })
                    }, caulyTrackerEvent: function (e) {
                        window._paq = window._paq || [], _paq.push(["track_code", "07016bef-e1a2-4279-880f-1f9ed598ea04"]), _paq.push(["event_name", e]), _paq.push(["send_event"]), function () {
                            var e = "//image.cauly.co.kr/script/", t = document, s = t.createElement("script"),
                                a = t.getElementsByTagName("script")[0];
                            s.type = "text/javascript", s.async = !0, s.defer = !0, s.src = e + "caulytracker_async.js", a.parentNode.insertBefore(s, a)
                        }()
                    }, handleTerms: function () {
                        this.agreeTerms ? this.agreeTerms = !1 : (this.notValids = Object(o["a"])({}, this.notValids, {terms: !1}), this.agreeTerms = !0)
                    }, redirect: function () {
                        this.$router.push({name: "Onboarding", query: {c: "selection"}})
                    }, showModal: function (e) {
                        this.modalType = e, this.isModalOpen = !0
                    }, checked: function (t) {
                        switch (t) {
                            case"total":
                                return e.compose(e.all(e.identity), e.map(e.prop("value")), r.a)(this.agreesKo)
                        }
                        return this.agreesKo[t].value
                    }, setAgreeKo: function (t) {
                        "total" !== t ? this.agreesKo[t].value = !this.agreesKo[t].value : this.agreesKo = e.map(function (e) {
                            return i()(e, {value: !0})
                        }, this.agreesKo)
                    }, toggleExpanded: function (e) {
                        this.agreesKo[e].expanded = !this.agreesKo[e].expanded
                    }, checkQueryParams: function () {
                        this.phCoupon = !!window.location.search.includes("phpromopost");
                        var t = e.path(["$route", "query", "noEmail"], this);
                        if (t) return this.messageModalText = this.$t("JOIN.fb_no_email"), void(this.isMessageModalActive = !0);
                        var s = e.path(["$route", "query", "emailExist"], this);
                        return s ? (this.messageModalText = this.$t("JOIN.social_email_exist", {email: s}), void(this.isTemplateModalActive = !0)) : void 0
                    }
                }),
                created: function () {
                    this.referral = this.$route.query.referral || window.localStorage.getItem("referral"), this.referralDisabled = !!this.referral, this.referral && (this.$route.query.referral = this.referral), this.checkQueryParams()
                }
            }
        }).call(this, s("b17e"))
    }, 3621: function (e, t, s) {
    }, "386d": function (e, t, s) {
        "use strict";
        var a = s("cb7c"), i = s("83a1"), n = s("5f1b");
        s("214f")("search", 1, function (e, t, s, r) {
            return [function (s) {
                var a = e(this), i = void 0 == s ? void 0 : s[t];
                return void 0 !== i ? i.call(s, a) : new RegExp(s)[t](String(a))
            }, function (e) {
                var t = r(s, e, this);
                if (t.done) return t.value;
                var o = a(e), c = String(this), l = o.lastIndex;
                i(l, 0) || (o.lastIndex = 0);
                var d = n(o, c);
                return i(o.lastIndex, l) || (o.lastIndex = l), null === d ? -1 : d.index
            }]
        })
    }, "38b7": function (e, t, s) {
        "use strict";
        var a = s("8a0f"), i = s.n(a);
        i.a
    }, "41b8": function (e, t, s) {
        "use strict";
        var a = s("090f"), i = s.n(a);
        i.a
    }, "50e7": function (e, t, s) {
        "use strict";
        (function (e) {
            var a = s("cebc"), i = s("0a63"), n = s("2f62"), r = s("fa7d");
            t["a"] = {
                created: function () {
                },
                props: {
                    slides: {type: Array, default: e.always([])},
                    paginationActiveColor: {type: String, default: "#18649b"}
                },
                components: {Carousel: i["Carousel"], Slide: i["Slide"]},
                computed: Object(a["a"])({}, Object(n["mapGetters"])("user", ["user"])),
                methods: {
                    isLang: function (e) {
                        return Object(r["w"])(this.user) === e
                    }
                }
            }
        }).call(this, s("b17e"))
    }, "60d9": function (e, t, s) {
        "use strict";
        var a = s("3621"), i = s.n(a);
        i.a
    }, "6a15": function (e, t, s) {
        "use strict";
        var a = s("f645"), i = s.n(a);
        i.a
    }, "6c46": function (e, t, s) {
        "use strict";
        var a = s("7643"), i = s.n(a);
        i.a
    }, "6f98": function (e, t, s) {
    }, 7643: function (e, t, s) {
    }, "77a0": function (e, t, s) {
        "use strict";
        (function (e) {
            var a = s("cebc"), i = s("dde5"), n = s("2f62"), r = s("af2e");
            t["a"] = {
                created: function () {
                    this.checkQueryParams()
                },
                mounted: function () {
                    this.$nextTick(function () {
                        var e = document.querySelector("input");
                        e && e.focus()
                    })
                },
                data: function () {
                    return {
                        email: "",
                        password: "",
                        rememberMe: !1,
                        isMessageModalActive: !1,
                        isTemplateModalActive: !1,
                        messageModalText: ""
                    }
                },
                components: {OauthBtn: r["a"]},
                methods: Object(a["a"])({}, Object(n["mapActions"])("user", {loginAction: "login"}), {
                    redirect: function () {
                        this.$router.push({name: this.dest})
                    }, login: function () {
                        var e = this, t = this.email, s = this.password;
                        i["c"].login(t, s).then(function (t) {
                            e.loginAction(t);
                            var s = e.$route.query.dest || "MyCampaigns", a = {};
                            "AccountSetting" === s && (a.businessCert = 1), e.$router.push({
                                name: s,
                                query: a
                            }), e.$ga.event({
                                eventCategory: "Users",
                                eventAction: "Login",
                                eventLabel: "Login",
                                eventValue: 1
                            })
                        }).catch(function (t) {
                            return console.error(t), e.toast(e.$t("LOGIN.check_email_password"))
                        })
                    }, checkQueryParams: function () {
                        var t = e.path(["$route", "query", "noEmail"], this);
                        if (t) return this.messageModalText = this.$t("JOIN.fb_no_email"), void(this.isMessageModalActive = !0);
                        var s = e.path(["$route", "query", "emailExist"], this);
                        return s ? (this.messageModalText = this.$t("JOIN.social_email_exist", {email: s}), void(this.isTemplateModalActive = !0)) : void 0
                    }
                }),
                computed: {
                    dest: function () {
                        var t = e.path(["$route", "query", "dest"])(this);
                        return "Express" != t && (t = "MyCampaigns"), t
                    }
                }
            }
        }).call(this, s("b17e"))
    }, "83a1": function (e, t) {
        e.exports = Object.is || function (e, t) {
            return e === t ? 0 !== e || 1 / e === 1 / t : e != e && t != t
        }
    }, "8a0f": function (e, t, s) {
    }, "996d": function (e, t, s) {
        "use strict";
        s.r(t);
        var a = function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("div", {staticClass: "verify-adriel-wrapper"}, [s("img", {
                attrs: {
                    src: "/img/email_check_icon.png",
                    alt: ""
                }
            }), s("span", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: e.emailVerified,
                    expression: "emailVerified"
                }], staticClass: "verify-info-title"
            }, [e._v("\n        " + e._s(e.$t("EMAIL_VERIFY.verified")) + "\n    ")]), s("div", {staticClass: "verify-adriel"}, [s("span", [e._v(e._s(e.$t("EMAIL_VERIFY.thank_you_verifying")))]), s("span", [e._v(e._s(e.$t("EMAIL_VERIFY.what_we_will_send")))]), s("span", {staticClass: "verify-adriel-info"}, [s("span", [e._v("- " + e._s(e.$t("EMAIL_VERIFY.will_send_1")))]), s("span", [e._v("- " + e._s(e.$t("EMAIL_VERIFY.will_send_2")))])]), s("span", [e._v(e._s(e.$t("EMAIL_VERIFY.click_my_campaign")))])]), s("button", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: e.emailVerified,
                    expression: "emailVerified"
                }], on: {click: e.redirect}
            }, [e._v(e._s(e.$t("EMAIL_VERIFY.btn_text")))])])
        }, i = [], n = (s("a481"), s("dde5")), r = {
            name: "VerifyEmail", data: function () {
                return {emailVerified: !1, error: !1}
            }, methods: {
                verify: function () {
                    var e = this;
                    n["c"].verifyEmail(this.$route.query.token).then(function (t) {
                        e.emailVerified = !0
                    }).catch(function (t) {
                        e.emailVerified = !0, console.log(t)
                    })
                }, redirect: function () {
                    this.$router.replace({name: "MyCampaigns"})
                }
            }, mounted: function () {
                this.verify()
            }
        }, o = r, c = (s("41b8"), s("2877")), l = Object(c["a"])(o, a, i, !1, null, null, null);
        t["default"] = l.exports
    }, "9cd4": function (e, t, s) {
        "use strict";
        s.r(t);
        var a = function () {
                var e = this, t = e.$createElement, s = e._self._c || t;
                return s("div", {staticClass: "auth-adriel-wrapper"}, [s("message-modal", {
                    attrs: {
                        text: e.messageModalText,
                        active: e.isMessageModalActive
                    }, on: {
                        "update:active": function (t) {
                            e.isMessageModalActive = t
                        }
                    }
                }), s("message-modal", {
                    attrs: {active: e.isTemplateModalActive}, on: {
                        "update:active": function (t) {
                            e.isTemplateModalActive = t
                        }
                    }
                }, [s("span", {domProps: {innerHTML: e._s(e.messageModalText)}})]), s("div", {staticClass: "auth-adriel"}, [s("span", {staticClass: "join-adriel-title"}, [e._v(e._s(e.$t("LOGIN.auth")))]), s("form", {
                    staticClass: "sign-inputs auth",
                    attrs: {autocomplete: "off"},
                    on: {
                        submit: function (t) {
                            return t.preventDefault(), e.login(t)
                        }
                    }
                }, [s("span", [s("adriel-input", {
                    attrs: {placeholder: e.$t("LOGIN.email"), type: "email"},
                    model: {
                        value: e.email, callback: function (t) {
                            e.email = t
                        }, expression: "email"
                    }
                })], 1), s("span", [s("adriel-input", {
                    attrs: {placeholder: e.$t("LOGIN.password"), type: "password"},
                    nativeOn: {
                        keypress: function (t) {
                            return !t.type.indexOf("key") && e._k(t.keyCode, "enter", 13, t.key, "Enter") ? null : e.login(t)
                        }
                    },
                    model: {
                        value: e.password, callback: function (t) {
                            e.password = t
                        }, expression: "password"
                    }
                })], 1)]), s("div", {staticClass: "auth-middle"}, [s("div", {
                    staticClass: "remember-me",
                    on: {
                        click: function (t) {
                            e.rememberMe = !e.rememberMe
                        }
                    }
                }, [s("check-box", {attrs: {checked: e.rememberMe}}), s("span", [e._v(e._s(e.$t("LOGIN.remember")))])], 1), s("router-link", {
                    attrs: {
                        to: {name: "Sign/Forgot"},
                        tag: "span"
                    }
                }, [e._v(e._s(e.$t("LOGIN.forgot_password")))])], 1), s("button", {on: {click: e.login}}, [e._v(e._s(e.$t("LOGIN.auth")))]), s("hr"), s("oauth-btn", {
                    attrs: {
                        provider: "facebook",
                        join: !1
                    }
                }), s("oauth-btn", {
                    style: {"margin-top": "10px"},
                    attrs: {provider: "google", join: !1}
                })], 1), s("div", {staticClass: "auth-additionals"}, [s("router-link", {
                    attrs: {
                        to: {name: "Sign/Join"},
                        tag: "span"
                    }
                }, [e._v("\n            " + e._s(e.$t("LOGIN.not_signed")) + "\n            "), s("span", [e._v(e._s(e.$t("LOGIN.sign_up")))])])], 1)], 1)
            }, i = [], n = s("77a0"), r = n["a"], o = (s("eb35"), s("2877")),
            c = Object(o["a"])(r, a, i, !1, null, null, null);
        t["default"] = c.exports
    }, a9af: function (e, t, s) {
        "use strict";
        var a = s("6f98"), i = s.n(a);
        i.a
    }, af2e: function (e, t, s) {
        "use strict";
        var a = function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("a", {
                staticClass: "oauth-btn", class: e.provider, on: {
                    click: function (t) {
                        return t.preventDefault(), e.openLink(t)
                    }
                }
            }, [s("span", [e._v(e._s(e.text))])])
        }, i = [], n = s("fa7d"), r = {
            name: "OauthBtn",
            props: {
                provider: {type: String, default: "facebook"},
                join: {type: Boolean, default: !0},
                dest: {type: String, default: "MyCampaigns"}
            },
            data: function () {
                return {}
            },
            computed: {
                text: function () {
                    return this.join ? this.$t("JOIN.sign_up_with", {provider: Object(n["d"])(this.provider)}) : this.$t("LOGIN.login_with", {provider: Object(n["d"])(this.provider)})
                }
            },
            methods: {
                openLink: function () {
                    this.join ? (this.$ga.event({
                        eventCategory: "App",
                        eventAction: "Sign",
                        eventLabel: "Register",
                        eventValue: 1
                    }), fbq("track", "CompleteRegistration"), this.$intercom.trackEvent("Register")) : this.$ga.event({
                        eventCategory: "Users",
                        eventAction: "Login",
                        eventLabel: "Login",
                        eventValue: 1
                    }), window.location.href = this.authLink()
                }, authLink: function () {
                    var e = "Asia/Seoul";
                    try {
                        e = Intl.DateTimeFormat().resolvedOptions().timeZone
                    } catch (s) {
                        console.error("cannot get timezone")
                    }
                    var t = "https://app.adriel.ai/api";
                    return "".concat(t, "/auth/").concat(this.provider, "?lang=").concat(Object(n["w"])(), "&timezone='").concat(e, "'&dest=").concat(this.dest)
                }
            }
        }, o = r, c = (s("a9af"), s("2877")), l = Object(c["a"])(o, a, i, !1, null, "43bf83a0", null);
        t["a"] = l.exports
    }, b271: function (e, t, s) {
        "use strict";
        var a = function () {
                var e = this, t = e.$createElement, s = e._self._c || t;
                return s("carousel", {
                    staticClass: "preview-carousel",
                    class: {ko: e.isLang("ko")},
                    attrs: {
                        autoplay: !0,
                        autoplayTimeout: 3e3,
                        autoplayHoverPause: !0,
                        perPage: 1,
                        paginationEnabled: !0,
                        paginationActiveColor: e.paginationActiveColor,
                        paginationColor: "#dddddd",
                        paginationPadding: 5
                    }
                }, e._l(e.slides, function (t, a) {
                    return s("slide", {key: a}, [s("img", {attrs: {src: t.mainImage}}), s("div", {staticClass: "slide-bottom"}, [s("span", {staticClass: "slide-title"}, [e._v("\n                " + e._s(t.title) + "\n            ")]), t.contents ? s("div", {staticClass: "slide-contents"}, e._l(t.contents || [], function (t, a) {
                        return s("div", {key: a}, [s("span", {staticClass: "slide-contents--head"}, [e._v(" 쨌 ")]), s("span", {staticClass: "slide-contents--each"}, [e._v(e._s(t))])])
                    }), 0) : e._e()])])
                }), 1)
            }, i = [], n = s("50e7"), r = n["a"], o = (s("f560"), s("2877")),
            c = Object(o["a"])(r, a, i, !1, null, null, null);
        t["a"] = c.exports
    }, c94f: function (e, t, s) {
        "use strict";
        s.r(t);
        var a = function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("div", {staticClass: "join-adriel-container"}, [s("div", {staticClass: "join-adriel-preview"}, [s("service-preview", {attrs: {slides: e.slides}})], 1), s("div", {staticClass: "join-adriel-wrapper"}, [s("terms-and-policy", {
                attrs: {
                    active: e.isModalOpen,
                    type: e.modalType,
                    confirmCb: function () {
                        e.agreeTerms = !0
                    }
                }, on: {
                    "update:active": function (t) {
                        e.isModalOpen = t
                    }
                }
            }), s("message-modal", {
                attrs: {text: e.messageModalText, active: e.isMessageModalActive, cb: e.redirect},
                on: {
                    "update:active": function (t) {
                        e.isMessageModalActive = t
                    }
                }
            }), s("message-modal", {
                attrs: {active: e.isTemplateModalActive}, on: {
                    "update:active": function (t) {
                        e.isTemplateModalActive = t
                    }
                }
            }, [s("span", {domProps: {innerHTML: e._s(e.messageModalText)}})]), s("div", {staticClass: "join-adriel"}, [s("span", {staticClass: "join-adriel-title"}, [s("div", {staticClass: "join-adriel-title__top"}, [e._v("\n                    " + e._s(e.$t("JOIN.sign_up")) + "\n                    "), e.phCoupon ? s("img", {attrs: {src: "/img/icons/ph_coupon.png"}}) : e._e()]), e.referral ? s("span", [e._v("with referral code " + e._s(e.referral))]) : e._e(), e.additionalText && !e.phCoupon ? s("span", {
                staticClass: "additional-text",
                domProps: {textContent: e._s(e.additionalText)}
            }) : e._e()]), s("form", {
                staticClass: "sign-inputs signUp",
                attrs: {autocomplete: "off"},
                on: {
                    submit: function (t) {
                        return t.preventDefault(), e.signUp(t)
                    }
                }
            }, [s("span", [s("span", [e._v(e._s(e.$t("JOIN.email")))]), s("adriel-input", {
                attrs: {
                    placeholder: e.$t("LOGIN.email"),
                    type: "email"
                }, model: {
                    value: e.email, callback: function (t) {
                        e.email = t
                    }, expression: "email"
                }
            })], 1), s("span", [s("span", [e._v(e._s(e.$t("JOIN.password")))]), s("adriel-input", {
                attrs: {
                    placeholder: e.$t("JOIN.password_placeholder"),
                    type: "password",
                    autocomplete: "new-password"
                }, model: {
                    value: e.password, callback: function (t) {
                        e.password = t
                    }, expression: "password"
                }
            })], 1), s("span", [s("span", [e._v(e._s(e.$t("JOIN.confirm_password")))]), s("adriel-input", {
                attrs: {
                    placeholder: e.$t("JOIN.confirm_password"),
                    type: "password",
                    autocomplete: "new-password"
                }, model: {
                    value: e.passwordConfirm, callback: function (t) {
                        e.passwordConfirm = t
                    }, expression: "passwordConfirm"
                }
            })], 1), "ko" !== e.lang ? s("div", {
                staticClass: "join-agree-terms-en",
                class: {hasError: !0 === e.notValids.terms},
                on: {click: e.handleTerms}
            }, [s("check-box", {attrs: {checked: e.agreeTerms}}), s("span", [e._v("\n                        " + e._s(e.$t("JOIN.terms_services_1")) + "\n                        "), s("span", {
                on: {
                    click: function (t) {
                        return t.stopPropagation(), e.showModal("terms")
                    }
                }
            }, [e._v(e._s(e.$t("JOIN.terms_services_terms")))]), e._v("\n                        " + e._s(e.$t("JOIN.terms_services_2")) + "\n                        "), s("span", {
                on: {
                    click: function (t) {
                        return t.stopPropagation(), e.showModal("policy")
                    }
                }
            }, [e._v(e._s(e.$t("JOIN.terms_services_policy")))]), e._v("\n                        " + e._s(e.$t("JOIN.terms_services_3")) + "\n                    ")])], 1) : s("div", {staticClass: "join-agree-terms-ko"}, [s("div", {
                staticClass: "terms-row total",
                on: {
                    click: function (t) {
                        return e.setAgreeKo("total")
                    }
                }
            }, [s("div", [s("img", {attrs: {src: e.checked("total") ? "/img/terms_pressed.png" : "/img/proposal/check_box.png"}}), s("span", [e._v("�꾩껜 �쎄��� �숈쓽�⑸땲��")])])]), s("div", {staticClass: "terms-row"}, [s("div", {
                on: {
                    click: function (t) {
                        return e.setAgreeKo("privacy")
                    }
                }
            }, [s("img", {attrs: {src: e.checked("privacy") ? "/img/terms_pressed.png" : "/img/proposal/check_box.png"}}), e._m(0), s("img", {
                attrs: {src: e.agreesKo["privacy"].expanded ? "/img/icons/join_up_icon.png" : "/img/icons/join_down_icon.png"},
                on: {
                    click: function (t) {
                        return t.stopPropagation(), e.toggleExpanded("privacy")
                    }
                }
            })]), e.agreesKo["privacy"].expanded ? s("div", {staticClass: "details"}, [s("span", {staticClass: "details__title"}, [e._v("�꾨뱶由ъ뿕 �쒕퉬�� �댁슜�먯쓽 媛쒖씤�뺣낫瑜� �섏쭛�섎뒗 紐⑹쟻�� �ㅼ쓬怨� 媛숈뒿�덈떎.")]), e._m(1), s("span", {staticClass: "details__content"}, [e._v("- �숈쓽瑜� 嫄곕��� 沅뚮━媛� �덉쑝��, �숈쓽嫄곕��� �곕Ⅸ �쒕퉬�� �댁슜�� �쒗븳�� �덉쓣 �� �덉뒿�덈떎.")]), s("span", {staticClass: "details__content"}, [e._v("- �뚯궗�� 怨꾩빟 諛� �쒕퉬�� �댄뻾�� �꾪빐 媛쒖씤�뺣낫 泥섎━�낅Т瑜� �꾪긽�� �� �덉쑝硫�, 媛쒖씤�뺣낫泥섎━諛⑹묠�� 洹� �댁슜�� 怨좎��⑸땲��.")])]) : e._e()]), s("div", {staticClass: "terms-row"}, [s("div", {
                on: {
                    click: function (t) {
                        return e.setAgreeKo("service")
                    }
                }
            }, [s("img", {attrs: {src: e.checked("service") ? "/img/terms_pressed.png" : "/img/proposal/check_box.png"}}), e._m(2), s("img", {
                attrs: {src: e.agreesKo["service"].expanded ? "/img/icons/join_up_icon.png" : "/img/icons/join_down_icon.png"},
                on: {
                    click: function (t) {
                        return t.stopPropagation(), e.toggleExpanded("service")
                    }
                }
            })]), e.agreesKo.service.expanded ? s("div", {staticClass: "details"}, [s("img", {attrs: {src: e.serviceImg}})]) : e._e()]), s("div", {staticClass: "terms-row"}, [s("div", {
                on: {
                    click: function (t) {
                        return e.setAgreeKo("marketing")
                    }
                }
            }, [s("img", {attrs: {src: e.checked("marketing") ? "/img/terms_pressed.png" : "/img/proposal/check_box.png"}}), e._m(3), s("img", {
                attrs: {src: e.agreesKo["marketing"].expanded ? "/img/icons/join_up_icon.png" : "/img/icons/join_down_icon.png"},
                on: {
                    click: function (t) {
                        return t.stopPropagation(), e.toggleExpanded("marketing")
                    }
                }
            })]), e.agreesKo.marketing.expanded ? s("div", {staticClass: "details"}, [s("span", {staticClass: "details__title"}, [e._v("�꾨뱶由ъ뿕 �쒕퉬�� �댁슜�먯쓽 媛쒖씤�뺣낫瑜� �섏쭛�섎뒗 紐⑹쟻�� �ㅼ쓬怨� 媛숈뒿�덈떎.")]), e._m(4), s("span", {staticClass: "details__content"}, [e._v("- �숈쓽瑜� 嫄곕��� 沅뚮━媛� �덉쑝硫�, �숈쓽 嫄곕� �� �뚯썝媛��낆쓣 �듯븳 湲곕낯�쒕퉬�� �댁슜�� 媛��ν븯��, �댁뒪�덊꽣 諛쒖넚 �깆쓽 �쒕퉬�ㅺ� �쒗븳�⑸땲��.")])]) : e._e()]), s("div", {staticClass: "terms-row"}, [s("div", {
                on: {
                    click: function (t) {
                        return e.setAgreeKo("personalized")
                    }
                }
            }, [s("img", {attrs: {src: e.checked("personalized") ? "/img/terms_pressed.png" : "/img/proposal/check_box.png"}}), e._m(5), s("img", {
                attrs: {src: e.agreesKo["personalized"].expanded ? "/img/icons/join_up_icon.png" : "/img/icons/join_down_icon.png"},
                on: {
                    click: function (t) {
                        return t.stopPropagation(), e.toggleExpanded("personalized")
                    }
                }
            })]), e.agreesKo["personalized"].expanded ? s("div", {staticClass: "details"}, [s("span", {staticClass: "details__title"}, [e._v("�꾨뱶由ъ뿕 �쒕퉬�� �댁슜�먯쓽 媛쒖씤�뺣낫瑜� �섏쭛�섎뒗 紐⑹쟻�� �ㅼ쓬怨� 媛숈뒿�덈떎.")]), e._m(6), s("span", {staticClass: "details__content"}, [e._v("- �숈쓽瑜� 嫄곕��� 沅뚮━媛� �덉쑝硫�, �숈쓽 嫄곕� �� �뚯썝媛��낆쓣 �듯븳 湲곕낯�쒕퉬�� �댁슜�� 媛��ν븯��, 留욎땄 �쒕퉬�� �덈궡 �깆쓽 �쒕퉬�ㅺ� �쒗븳�⑸땲��.")])]) : e._e()])])]), s("button", {on: {click: e.signUp}}, [e._v(e._s(e.$t("JOIN.sign_up")))]), s("hr"), s("oauth-btn", {
                attrs: {
                    dest: e.dest,
                    provider: "facebook"
                }
            }), s("oauth-btn", {
                style: {"margin-top": "10px"},
                attrs: {provider: "google", dest: e.dest}
            })], 1), s("router-link", {
                staticClass: "mb-10",
                attrs: {to: {name: "Sign/Login"}, tag: "span"}
            }, [e._v(e._s(e.$t("JOIN.already_signed")))])], 1)])
        }, i = [function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("span", [e._v("\n                                媛쒖씤�뺣낫 �섏쭛��씠�� �숈쓽\n                                "), s("span", {staticClass: "required"}, [e._v("(�꾩닔)")])])
        }, function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("span", {staticClass: "details__table"}, [s("div", [s("span", [e._v("�섏쭛紐⑹쟻")]), s("span", [e._v("�뚯썝�� �쒕퉬�� �쒓났 諛� �좎����由�")])]), s("div", [s("span", [e._v("�섏쭛��ぉ")]), s("span", [e._v("�대찓��, 鍮꾨�踰덊샇")])]), s("div", [s("span", [e._v("蹂댁쑀/�댁슜湲곌컙")]), s("span", [e._v("�뚯썝�덊눜 ��")])])])
        }, function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("span", [e._v("\n                                �쒕퉬�� �댁슜�쎄�\n                                "), s("span", {staticClass: "required"}, [e._v("(�꾩닔)")])])
        }, function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("span", [e._v("\n                                留덉��� �뺣낫 �섏떊 �숈쓽\n                                "), s("span", {staticClass: "optional"}, [e._v("(�좏깮)")])])
        }, function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("span", {staticClass: "details__table"}, [s("div", [s("span", [e._v("�섏쭛紐⑹쟻")]), s("span", [e._v("�댁뒪�덊꽣 諛쒖넚")])]), s("div", [s("span", [e._v("�섏쭛��ぉ")]), s("span", [e._v("�대찓��")])]), s("div", [s("span", [e._v("蹂댁쑀/�댁슜湲곌컙")]), s("span", [e._v("�숈쓽泥좏쉶 �먮뒗 �뚯썝 �덊눜 ��")])])])
        }, function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("span", [e._v("\n                                留욎땄 �쒕퉬�� �뺣낫 �섏떊 �숈쓽\n                                "), s("span", {staticClass: "optional"}, [e._v("(�좏깮)")])])
        }, function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("span", {staticClass: "details__table"}, [s("div", [s("span", [e._v("�섏쭛紐⑹쟻")]), s("span", [e._v("留욎땄 �쒕퉬�� �덈궡")])]), s("div", [s("span", [e._v("�섏쭛��ぉ")]), s("span", [e._v("�대찓��, �몃뱶�곕쾲��, �먮룞�섏쭛�뺣낫 (荑좏궎)")])]), s("div", [s("span", [e._v("蹂댁쑀/�댁슜湲곌컙")]), s("span", [e._v("�숈쓽泥좏쉶 �먮뒗 �뚯썝 �덊눜 ��")])])])
        }], n = s("2fa9"), r = n["a"], o = (s("60d9"), s("2877")), c = Object(o["a"])(r, a, i, !1, null, null, null);
        t["default"] = c.exports
    }, cc85: function (e, t, s) {
        "use strict";
        s.r(t);
        var a = function () {
            var e = this, t = e.$createElement, s = e._self._c || t;
            return s("div", {staticClass: "forgot-adriel-wrapper"}, [s("message-modal", {
                attrs: {
                    text: e.modalText,
                    active: e.isModalOpen,
                    btnText: e.$t("FORGOT.btn_done")
                }, on: {
                    "update:active": function (t) {
                        e.isModalOpen = t
                    }, close: e.redirect, btnClicked: e.redirect
                }
            }, [s("span", {staticClass: "forgot-modal-title"}, [e._v("\n            " + e._s(e.$t("FORGOT.sent_title")) + "\n        ")])]), s("div", {staticClass: "forgot-adriel"}, [s("div", {staticClass: "forgot-info-wrapper"}, [s("span", {staticClass: "forgot-info-title"}, [e._v("\n                " + e._s(e.$t("FORGOT.title")) + "\n            ")]), s("span", {staticClass: "forgot-info"}, [e._v("\n                " + e._s(e.$t("FORGOT.detail")) + "\n            ")])]), s("form", {
                staticClass: "sign-inputs auth",
                attrs: {autocomplete: "off"},
                on: {
                    submit: function (t) {
                        return t.preventDefault(), e.send(t)
                    }
                }
            }, [s("span", [s("img", {attrs: {src: "/img/email_icon.png"}}), s("b-input", {
                attrs: {
                    placeholder: e.$t("FORGOT.email"),
                    type: "email"
                }, nativeOn: {
                    keypress: function (t) {
                        return !t.type.indexOf("key") && e._k(t.keyCode, "enter", 13, t.key, "Enter") ? null : e.send(t)
                    }
                }, model: {
                    value: e.email, callback: function (t) {
                        e.email = t
                    }, expression: "email"
                }
            })], 1)]), s("button", {on: {click: e.send}}, [e._v(e._s(e.$t("COMMON.submit")))])])], 1)
        }, i = [], n = s("dde5"), r = s("fa7d"), o = (s("2f62"), {
            data: function () {
                return {email: "", isModalOpen: !1, sended: !1}
            }, computed: {
                modalText: function () {
                    return this.$t("FORGOT.sent_content", {email: this.email})
                }
            }, methods: {
                send: function () {
                    var e = this;
                    Object(r["K"])(this.email) ? !0 !== this.sended && (this.sended = !0, this.$ga.event({
                        eventCategory: "Users",
                        eventAction: "Forgot password",
                        eventLabel: "Forgot password",
                        eventValue: this.email
                    }), n["c"].forgot(this.email).then(function (t) {
                        e.isModalOpen = !0
                    }).catch(function (t) {
                        e.sended = !1, console.log(t), alert("please check email")
                    })) : alert("please check email")
                }, redirect: function () {
                    this.$router.push({name: "Sign/Login"})
                }
            }
        }), c = o, l = (s("0151"), s("2877")), d = Object(l["a"])(c, a, i, !1, null, null, null);
        t["default"] = d.exports
    }, eb35: function (e, t, s) {
        "use strict";
        var a = s("02af"), i = s.n(a);
        i.a
    }, f560: function (e, t, s) {
        "use strict";
        var a = s("feea"), i = s.n(a);
        i.a
    }, f645: function (e, t, s) {
    }, feea: function (e, t, s) {
    }
}]);
//# sourceMappingURL=sign.ed303b4b.js.map