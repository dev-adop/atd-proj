$(document).ready(function(){
    // $('.sign-wrapper-top-container').load('/views/public/header.html');
    /*
    login.html
     */

    /*
    __campaignStatus__ 캠페인 등록 진행상태
    __campaignName__ 캠페인 이름
    __campaignDate__ 캠페인 생성일
    __campaignRunning__ 캠페인 진행상태
    __campaignExtension__ 캠페인 기간연장(캠페인 진행 상태가 종료일땐 '', 아니면 disabled)
    __campaignResult__ 캠페인 기간연장(캠페인 진행 상태가 종료일땐 '', 아니면 disabled)
    __campaignImage__ 캠페인 광고 이미지 주
    __campaignColor__ 캠페인 진행 상태에 따라 값 변경(캠페인 진행 상태가 종료일땐 deactivated, 등록중이면 in_progress, 진행중이면 '')

     */
    var campaignTemplate =
        '<div class="campaign-box-element __campaignColor__">\n' +
        '<div class="campaign-box-top">\n' +
        '<div class="campaign-box-cover-wrapper">\n' +
        '<div class="bg-img-wrapper campaign-box-cover-img bg-success" style="width: 325px; height: 150px; background-image: url('+'"__campaignImage__"'+'); background-size: cover; border-top-left-radius: 5px; border-top-right-radius: 5px; background-color: rgb(77, 80, 88);">\n' +
        '</div>\n' +
        '<div class="campaign-box-cover"></div>\n' +
        '</div>\n' +
        '<div class="campaign-box-top-info-wrapper">\n' +
        '<span class="campaign-box-top-info-status">__campaignStatus__</span>\n' +
        '<span class="campaign-box-icons">\n' +
        '<img class="hover-opacity has-tooltip" data-original-title="null" src="/img/campaign_delete_icon.png">\n' +
        '<img class="hover-opacity has-tooltip" data-original-title="null" src="/img/copy_campaign.png">\n' +
        '</span>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div class="campaign-box-bottom">\n' +
        '<div class="title-wrapper">\n' +
        '<span class="title-area">\n' +
        '<span class="campaign-box-title adriel-ellipsis">__campaignName__</span>\n' +
        '<span class="icon" style="font-size: 10px; color: rgb(102, 102, 102);">\n' +
        '<i class="fal fa-pen fa-lg"></i>\n' +
        '</span>\n' +
        '</span>\n' +
        '</div>\n' +
        '<span class="campaign-box-created">__campaignDate__에 제작</span>\n' +
        '<div class="campaign-box-status-info">\n' +
        '<span class="adriel-switch-wrapper">\n' +
        '<span class="switch-value-label switch-off">\n' +
        '__campaignRunning__\n' +
        '</span>\n' +
        '<label class="switch" tabindex="0">\n' +
        '<input tabindex="-1" true-value="true" type="checkbox" value="false">\n' +
        '<span class="check"></span>\n' +
        '<span class="control-label"></span>\n' +
        '</label>\n' +
        '</span>\n' +
        '</div>\n' +
        '<hr>\n' +
        '<div class="campaign-box-buttons">\n' +
        '<span class="each setting">\n' +
        '<span class="icon">\n' +
        '<i class="far fa-pen-square fa-lg"></i>\n' +
        '</span>\n' +
        '<span class="each--text adriel-ellipsis">광고 설정 보기</span>\n' +
        '</span>\n' +
        '<span class="each run __campaignExtension__">\n' +
        '<span class="icon">\n' +
        '<i class="far fa-calendar-plus fa-lg"></i>\n' +
        '</span>\n' +
        '<span class="each--text adriel-ellipsis">광고 기간 연장</span>\n' +
        '</span>\n' +
        '<span class="each results __campaignResult__">\n' +
        '<span class="icon">\n' +
        '<i class="fas fa-chart-bar fa-lg"></i>\n' +
        '</span>\n' +
        '<span class="each--text adriel-ellipsis">결과 현황 보기</span>\n' +
        '</span>\n' +
        '<span class="each budget">\n' +
        '<span class="icon">\n' +
        '<i class="far fa-usd-circle fa-lg"></i>\n' +
        '</span>\n' +
        '<span class="each--text adriel-ellipsis">일일 예산 추가</span>\n' +
        '</span>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div class="campaign-box-indicator"></div>\n' +
        '</div>';


    $(".my-campaigns-contents").append(campaignTemplate);

    $('.sign-inputs').validate();
    $('#loginBtn').on('click', function(){
       login();
    });

    $('#loginPw').on('keydown', function (key) {
        if(key.keyCode == 13) {
            $('#loginBtn').click();
        }
    });

    $('#rememberMe').on('click', function () {
        var img = '/img/';
        if($(this).hasClass('nonChk')){
            $(this).find('img').attr('src', img + 'checkbox_active.png');
            $(this).removeClass('nonChk');
            localStorage.auth = true;
        }else{
            $(this).find('img').attr('src', img + 'checkbox_default.png');
            $(this).addClass('nonChk');
            localStorage.auth = false;
        }
    });

    /*
    forgot.html
     */
    $('#forgotBtn').on('click', function () {
        forgot();
    });

    /*
    join.html
     */
    $('#joinBtn').on('click', function (){
        join();
    });
});

function login() {
    if($('#loginId').val() == ""){
        alert("Please check your Email.")
    }else if($('#loginPw').val() == ""){
        alert("Please check your Password.")
    }else{
        var param = new Object();
        param.email = $('#loginId').val();
        param.password = $('#loginPw').val();

        ajaxRequest('/api/login', param,'POST',function (result) {
            // console.log(result);
            if (result.success === true){
                // console.log(result.token);
                localStorage.token = result.token;
                // $.ajaxSetup({
                //    headers: {
                //        'x-access-token' : token
                //    }
                // });
                location.href = '/myCampaigns';
            }else if(result.success === false){
                alert(result.message);
                // alert('Please check your Email or Password.');
                localStorage.removeItem('token');
            }
        },function (error) {
            alert('Please try again.');
            console.log(error);
            localStorage.clear();
        });
    }
}

function forgot() {
    if($('#forgotId').val() == ""){
        alert("Please input your Email.");
    } else {
        alert("Please check your Email");
        //email 발송 추가하기
    }
}

function join() {
    if($('#joinId').val() == ""){
        alert("Please input your Email.");
    }else if($('#joinPw').val() == ""){
        alert("Please input your Password.");
    }else if($('#joinPwChk').val() == "" || $('#joinPw').val() !== $('#joinPwChk').val()){
        alert("Password does not match with confirm password");
    }else{
        var param = new Object();
        param.email = $('#joinId').val();
        param.password = $('#joinPw').val();

        ajaxRequest('/api/register', param,'POST',function (result) {
            // console.log(result);
            if (result.success === true){
                // console.log(result.token);
                location.href = '/';
            }else if(result.success === false){
                alert(result.message);
            }
        },function (error) {
            alert('Please try again.');
            console.log(error);
        });
    }
}