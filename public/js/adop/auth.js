$(document).ready(function(){
    // $('.sign-wrapper-top-container').load('/views/public/header.html');
    /*
    login.html
     */
    $('.sign-inputs').validate();
    $('#loginBtn').on('click', function(){
       login();
    });

    $('#loginPw').on('keydown', function (key) {
        if(key.keyCode == 13) {
            $('#loginBtn').click();
        }
    });

    $('#rememberMe').on('click', function () {
        var img = '/img/';
        if($(this).hasClass('nonChk')){
            $(this).find('img').attr('src', img + 'checkbox_active.png');
            $(this).removeClass('nonChk');
            localStorage.auth = true;
        }else{
            $(this).find('img').attr('src', img + 'checkbox_default.png');
            $(this).addClass('nonChk');
            localStorage.auth = false;
        }
    });

    /*
    forgot.html
     */
    $('#forgotBtn').on('click', function () {
        forgot();
    });

    /*
    join.html
     */
    $('#joinBtn').on('click', function (){
        join();
    });
});

function login() {
    if($('#loginId').val() == ""){
        alert("Please check your Email.")
    }else if($('#loginPw').val() == ""){
        alert("Please check your Password.")
    }else{
        var param = new Object();
        param.email = $('#loginId').val();
        param.password = $('#loginPw').val();

        ajaxRequest('/api/login', param,'POST',function (result) {
            // console.log(result);
            if (result.success === true){
                // console.log(result.token);
                localStorage.token = result.token;
                // $.ajaxSetup({
                //    headers: {
                //        'x-access-token' : token
                //    }
                // });
                location.href = '/myCampaigns';
            }else if(result.success === false){
                alert(result.message);
                // alert('Please check your Email or Password.');
                localStorage.removeItem('token');
            }
        },function (error) {
            alert('Please try again.');
            console.log(error);
            localStorage.clear();
        });
    }
}

function forgot() {
    if($('#forgotId').val() == ""){
        alert("Please input your Email.");
    } else {
        alert("Please check your Email");
        //email 발송 추가하기
    }
}

function join() {
    if($('#joinId').val() == ""){
        alert("Please input your Email.");
    }else if($('#joinPw').val() == ""){
        alert("Please input your Password.");
    }else if($('#joinPwChk').val() == "" || $('#joinPw').val() !== $('#joinPwChk').val()){
        alert("Password does not match with confirm password");
    }else{
        var param = new Object();
        param.email = $('#joinId').val();
        param.password = $('#joinPw').val();

        ajaxRequest('/api/register', param,'POST',function (result) {
            // console.log(result);
            if (result.success === true){
                // console.log(result.token);
                location.href = '/';
            }else if(result.success === false){
                alert(result.message);
            }
        },function (error) {
            alert('Please try again.');
            console.log(error);
        });
    }
}