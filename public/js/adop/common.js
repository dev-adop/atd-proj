function ajaxRequest(url, data, method, successMsg, errorMsg) {
    //validate method : Only "POST","GET"," PUT","DELETE"
    var methodUpper = method.toUpperCase();
    $.LoadingOverlay("show");
    if(methodUpper == "POST" || methodUpper == "GET" || methodUpper == "PUT" || methodUpper == "DELETE"){

        var ajaxResult = null;

        $.ajax({

            type        : methodUpper,
            url         : url,
            data        : JSON.stringify(data),
            // data        : data,
            contentType : "application/json; charset=utf-8",
            cache       : false,
            dataType    : 'json',
            async       : false

        })
        .done(function( result ) {
            if(typeof successMsg == "string")
                alert( successMsg );
            if(typeof successMsg == "function")
                successMsg(result);

            $.LoadingOverlay("hide");
        })
        .fail(function(jqXHR, textStatus) {
            if(typeof errorMsg == "string")
                alert( errorMsg );
            if(typeof errorMsg == "function")
                errorMsg(jqXHR, textStatus);
            $.LoadingOverlay("hide");
        });
        console.log(ajaxResult);
        return ajaxResult;

    }else {
        $.LoadingOverlay("hide");
        alert("This 'Method' is wrong.");
        return false;
    }
}

function ajaxRequestAuth(url, data, method, successMsg, errorMsg) {
    //validate method : Only "POST","GET"," PUT","DELETE"
    $.LoadingOverlay("show");
    var methodUpper = method.toUpperCase();

    if(methodUpper == "POST" || methodUpper == "GET" || methodUpper == "PUT" || methodUpper == "DELETE"){

        var ajaxResult = null;

        $.ajax({

            type        : methodUpper,
            url         : url,
            data        : data,
            // contentType : "application/json; charset=utf-8",
            cache       : false,
            dataType    : 'json',
            async       : false,
            beforeSend: function(xhr) {
                if (localStorage.token) {
                    //https://github.com/chaofz/jquery-jwt-auth/blob/master/index.html
                    xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.token);
                }
            }
        })
        .done(function( result ) {
            $.LoadingOverlay("hide");
            if(typeof successMsg == "string")
                alert( successMsg );
            if(typeof successMsg == "function")
                successMsg(result);

            if (result == 'logout'){
                location.href = '/auth';
            } else{
                ajaxResult = result;
            }
        })
        .fail(function(jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            if(typeof errorMsg == "string")
                alert( errorMsg );
            if(typeof errorMsg == "function")
                errorMsg(jqXHR, textStatus);
        });

        return ajaxResult;

    }else {
        $.LoadingOverlay("hide");
        alert("This 'Method' is wrong.");
        return false;
    }
}