(function (e) {
    function t(t) {
        for (var n, o, s = t[0], c = t[1], l = t[2], d = 0, u = []; d < s.length; d++) o = s[d], i[o] && u.push(i[o][0]), i[o] = 0;
        for (n in c) Object.prototype.hasOwnProperty.call(c, n) && (e[n] = c[n]);
        p && p(t);
        while (u.length) u.shift()();
        return r.push.apply(r, l || []), a()
    }

    function a() {
        for (var e, t = 0; t < r.length; t++) {
            for (var a = r[t], n = !0, o = 1; o < a.length; o++) {
                var s = a[o];
                0 !== i[s] && (n = !1)
            }
            n && (r.splice(t--, 1), e = c(c.s = a[0]))
        }
        return e
    }

    var n = {}, o = {app: 0}, i = {app: 0}, r = [];

    function s(e) {
        return c.p + "js/" + ({
            "accountSetting~onboarding~proposal": "accountSetting~onboarding~proposal",
            accountSetting: "accountSetting",
            "dashboard~onboarding~proposal~sign": "dashboard~onboarding~proposal~sign",
            sign: "sign",
            "dashboard~onboarding~proposal": "dashboard~onboarding~proposal",
            "onboarding~proposal": "onboarding~proposal",
            onboarding: "onboarding",
            proposal: "proposal",
            dashboard: "dashboard",
            express: "express",
            manager: "manager"
        }[e] || e) + "." + {
            "accountSetting~onboarding~proposal": "ceccae85",
            accountSetting: "0af65889",
            "dashboard~onboarding~proposal~sign": "a0c118cb",
            "chunk-b1925690": "180c6280",
            sign: "ed303b4b",
            "dashboard~onboarding~proposal": "960a4533",
            "onboarding~proposal": "5804c6bf",
            onboarding: "8131026d",
            proposal: "6c98880c",
            dashboard: "ce2e430a",
            express: "52becb25",
            manager: "bbd287c8"
        }[e] + ".js"
    }

    function c(t) {
        if (n[t]) return n[t].exports;
        var a = n[t] = {i: t, l: !1, exports: {}};
        return e[t].call(a.exports, a, a.exports, c), a.l = !0, a.exports
    }

    c.e = function (e) {
        var t = [], a = {
            "accountSetting~onboarding~proposal": 1,
            accountSetting: 1,
            "chunk-b1925690": 1,
            sign: 1,
            "dashboard~onboarding~proposal": 1,
            "onboarding~proposal": 1,
            onboarding: 1,
            proposal: 1,
            dashboard: 1,
            express: 1,
            manager: 1
        };
        o[e] ? t.push(o[e]) : 0 !== o[e] && a[e] && t.push(o[e] = new Promise(function (t, a) {
            for (var n = "css/" + ({
                "accountSetting~onboarding~proposal": "accountSetting~onboarding~proposal",
                accountSetting: "accountSetting",
                "dashboard~onboarding~proposal~sign": "dashboard~onboarding~proposal~sign",
                sign: "sign",
                "dashboard~onboarding~proposal": "dashboard~onboarding~proposal",
                "onboarding~proposal": "onboarding~proposal",
                onboarding: "onboarding",
                proposal: "proposal",
                dashboard: "dashboard",
                express: "express",
                manager: "manager"
            }[e] || e) + "." + {
                "accountSetting~onboarding~proposal": "f707c87e",
                accountSetting: "e49d57de",
                "dashboard~onboarding~proposal~sign": "31d6cfe0",
                "chunk-b1925690": "f9f56860",
                sign: "1af9e006",
                "dashboard~onboarding~proposal": "b23afb89",
                "onboarding~proposal": "60254190",
                onboarding: "cff5bd25",
                proposal: "97b13948",
                dashboard: "d1bcdd95",
                express: "4aaffcd7",
                manager: "d94fe465"
            }[e] + ".css", i = c.p + n, r = document.getElementsByTagName("link"), s = 0; s < r.length; s++) {
                var l = r[s], d = l.getAttribute("data-href") || l.getAttribute("href");
                if ("stylesheet" === l.rel && (d === n || d === i)) return t()
            }
            var u = document.getElementsByTagName("style");
            for (s = 0; s < u.length; s++) {
                l = u[s], d = l.getAttribute("data-href");
                if (d === n || d === i) return t()
            }
            var p = document.createElement("link");
            p.rel = "stylesheet", p.type = "text/css", p.onload = t, p.onerror = function (t) {
                var n = t && t.target && t.target.src || i,
                    r = new Error("Loading CSS chunk " + e + " failed.\n(" + n + ")");
                r.code = "CSS_CHUNK_LOAD_FAILED", r.request = n, delete o[e], p.parentNode.removeChild(p), a(r)
            }, p.href = i;
            var _ = document.getElementsByTagName("head")[0];
            _.appendChild(p)
        }).then(function () {
            o[e] = 0
        }));
        var n = i[e];
        if (0 !== n) if (n) t.push(n[2]); else {
            var r = new Promise(function (t, a) {
                n = i[e] = [t, a]
            });
            t.push(n[2] = r);
            var l, d = document.createElement("script");
            d.charset = "utf-8", d.timeout = 120, c.nc && d.setAttribute("nonce", c.nc), d.src = s(e), l = function (t) {
                d.onerror = d.onload = null, clearTimeout(u);
                var a = i[e];
                if (0 !== a) {
                    if (a) {
                        var n = t && ("load" === t.type ? "missing" : t.type), o = t && t.target && t.target.src,
                            r = new Error("Loading chunk " + e + " failed.\n(" + n + ": " + o + ")");
                        r.type = n, r.request = o, a[1](r)
                    }
                    i[e] = void 0
                }
            };
            var u = setTimeout(function () {
                l({type: "timeout", target: d})
            }, 12e4);
            d.onerror = d.onload = l, document.head.appendChild(d)
        }
        return Promise.all(t)
    }, c.m = e, c.c = n, c.d = function (e, t, a) {
        c.o(e, t) || Object.defineProperty(e, t, {enumerable: !0, get: a})
    }, c.r = function (e) {
        "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(e, "__esModule", {value: !0})
    }, c.t = function (e, t) {
        if (1 & t && (e = c(e)), 8 & t) return e;
        if (4 & t && "object" === typeof e && e && e.__esModule) return e;
        var a = Object.create(null);
        if (c.r(a), Object.defineProperty(a, "default", {
                enumerable: !0,
                value: e
            }), 2 & t && "string" != typeof e) for (var n in e) c.d(a, n, function (t) {
            return e[t]
        }.bind(null, n));
        return a
    }, c.n = function (e) {
        var t = e && e.__esModule ? function () {
            return e["default"]
        } : function () {
            return e
        };
        return c.d(t, "a", t), t
    }, c.o = function (e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, c.p = "/", c.oe = function (e) {
        throw console.error(e), e
    };
    var l = window["webpackJsonp"] = window["webpackJsonp"] || [], d = l.push.bind(l);
    l.push = t, l = l.slice();
    for (var u = 0; u < l.length; u++) t(l[u]);
    var p = d;
    r.push([0, "chunk-vendors"]), a()
})({
    0: function (e, t, a) {
        e.exports = a("56d7")
    }, "001c": function (e, t, a) {
    }, "0041": function (e, t, a) {
    }, "0230": function (e, t, a) {
    }, "024b": function (e, t, a) {
        "use strict";
        (function (e) {
            a("96cf");
            var n = a("795b"), o = a.n(n), i = a("f499"), r = a.n(i), s = a("3b8d"), c = a("e380"), l = a.n(c),
                d = e.prop("data"), u = "/utils";

            function p(e) {
                return _.apply(this, arguments)
            }

            function _() {
                return _ = Object(s["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.get("".concat(u, "/fbInfo"), {params: {userToken: t}});
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), _.apply(this, arguments)
            }

            var m = l()(function (e) {
                return axios.get("/utils/userInterest?q=".concat(e)).then(d)
            }), f = l()(function (e) {
                return axios.post("/utils/interestSuggestions", e).then(d)
            }), g = l()(function (e) {
                return axios.get("/utils/suggestions", {params: {website: e}}).then(d)
            }), h = function () {
                var e = {};
                return function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = r()(t);
                    return e[a] ? o.a.resolve(e[a]) : axios.post("/utils/appSearch", t).then(d).then(function (t) {
                        return e[a] = t, t
                    })
                }
            }(), b = function () {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return axios.post("/utils/completeMobileAppUrls", e).then(d)
            };
            t["a"] = {
                fbInfo: p,
                getInterests: m,
                getSuggestions: g,
                interestSuggestions: f,
                searchApp: h,
                completeMobile: b
            }
        }).call(this, a("b17e"))
    }, "028c": function (e, t, a) {
    }, "02e4": function (e, t, a) {
        "use strict";
        var n = a("55cc"), o = a.n(n);
        o.a
    }, "0300": function (e, t, a) {
        "use strict";
        var n = a("ea2e"), o = a.n(n);
        o.a
    }, "08c6": function (e, t, a) {
        "use strict";
        var n = a("d805"), o = a.n(n);
        o.a
    }, "08df": function (e, t, a) {
        "use strict";
        (function (e) {
            a("ac6a"), a("2f62"), a("e380"), a("b047"), e.contains(e.__, ["url", "contact", "booking"]), e.max(0);
            t["a"] = {
                props: {
                    active: {type: Boolean, default: !1},
                    headerText: {type: String, default: ""},
                    confirmText: {type: String, default: "OK"},
                    cancelText: {type: String, default: "CANCEL"},
                    hasButtons: {type: Boolean, default: !0},
                    hasCancelBtn: {type: Boolean, default: !0},
                    width: {type: String, default: "450px"}
                }, data: function () {
                    return {top: void 0, left: "auto"}
                }, computed: {
                    wrapperStyle: function () {
                        return {top: this.top, left: this.left}
                    }, _active: {
                        get: function () {
                            return this.active
                        }, set: function (e) {
                            this.$emit("update:active", e)
                        }
                    }
                }, components: {}, methods: {
                    confirm: function () {
                        this.$emit("confirm", this.values)
                    }, cancel: function () {
                        this._active = !1, this.$emit("cancel")
                    }
                }, beforeDestroy: function () {
                }, mounted: function () {
                    var e = this;
                    this.$nextTick(function () {
                        var t = 100;
                        e.top = "".concat(t, "px")
                    })
                }
            }
        }).call(this, a("b17e"))
    }, "0a50": function (e, t, a) {
    }, "0a52": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2d1f"), o = a.n(n), i = a("795b"), r = a.n(i), s = (a("96cf"), a("3b8d")), c = a("a8db"),
                l = a("cebc"), d = a("75fc"), u = a("b6d0"), p = a.n(u), _ = a("dde5"), m = a("b047"), f = a.n(m),
                g = a("fa7d"), h = new p.a(["running", "deactivated", "in_review", "proposal_review"]);
            t["a"] = {
                namespaced: !0,
                state: {
                    campaigns: [],
                    campaign: {},
                    cachedCampaigns: {},
                    isCardLoading: !1,
                    estimations: void 0,
                    lastUpdated: void 0,
                    googleAnalyticsBackup: void 0,
                    facebookPixelBackup: void 0,
                    fbAppBackup: void 0,
                    deactivatingSet: new p.a,
                    gaConversions: [],
                    pixelConversions: [],
                    coupons: [],
                    totalData: void 0,
                    countByTabs: void 0
                },
                mutations: {
                    setCampaigns: function (e, t) {
                        var a = t.data, n = void 0 === a ? [] : a, o = t.isNewData, i = t.type;
                        "CARD" === i ? (e.campaigns = o ? n.results : [].concat(Object(d["a"])(e.campaigns), Object(d["a"])(n.results)), e.countByTabs = n.totalByStatus) : "TABLE" === i ? (e.campaigns = n.results, e.totalData = n.total, e.countByTabs = n.totalByStatus) : e.campaigns = n, e.deactivatingSet.clear()
                    }, setCachedCampaigns: function (e, t) {
                        var a = t.data, n = t.key;
                        n ? e.cachedCampaigns[n] = a : (e.cachedCampaigns = {}, e.countByTabs = void 0)
                    }, setCardLoading: function (e, t) {
                        e.isCardLoading = t
                    }, setCampaign: function (t, a) {
                        t.campaign = a;
                        var n = e.path(e.__, a);
                        t.googleAnalyticsBackup = n(["intel", "googleAnalytics"]), t.facebookPixelBackup = n(["intel", "facebookPixel"]), t.fbAppBackup = n(["intel", "fbApp"])
                    }, setBillingPlan: function (e, t) {
                        e.campaign.billing_plan = t
                    }, setAutoOptimization: function (e, t) {
                        e.campaign.auto_optimization = t
                    }, setEstimations: function (e, t) {
                        e.estimations = t
                    }, updateProposal: function (t, a) {
                        t.lastUpdated = e.clone(a)
                    }, setGa: function (e, t) {
                        e.campaign.intel = Object(l["a"])({}, e.campaign.intel, {googleAnalytics: t})
                    }, setPixel: function (e, t) {
                        e.campaign.intel = Object(l["a"])({}, e.campaign.intel, {facebookPixel: t})
                    }, setFbApp: function (e, t) {
                        e.campaign.intel = Object(l["a"])({}, e.campaign.intel, {fbApp: t})
                    }, setAppleAppId: function (e, t) {
                        e.campaign.intel = Object(l["a"])({}, e.campaign.intel, {appleAppId: t})
                    }, setDeactivatingSet: function (e, t) {
                        var a = t.value, n = t.id, o = e.deactivatingSet;
                        a ? o.add(n) : o.delete(n)
                    }, setCampaignImage: function (e, t) {
                        t && (e.campaign.image = t)
                    }, setGAConversionList: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        e.gaConversions = t
                    }, setPixelConversionList: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        e.pixelConversions = t
                    }, setCoupons: function (e, t) {
                        e.coupons = t
                    }
                },
                actions: {
                    updateProposal: function (t, a) {
                        var n = t.state, o = t.dispatch;
                        if (a) {
                            var i = e.omit(["schedule"]);
                            e.equals(i(a), i(n.campaign.proposal)) || o("proposal/updateData", a, {root: !0})
                        }
                    }, getCampaignsByList: function (e, t) {
                        var a = e.commit, n = e.state, o = b(t), i = n.cachedCampaigns;
                        if (!i[o]) return _["d"].query(t).then(function (e) {
                            a("setCampaigns", {data: e, type: "TABLE"}), a("setCachedCampaigns", {data: e, key: o})
                        }).catch(function (e) {
                            return console.log(e)
                        });
                        a("setCampaigns", {data: i[o], type: "TABLE"})
                    }, getCampaignsByCard: function (e, t) {
                        var a = e.commit, n = e.state, o = t.isNewData, i = Object(c["a"])(t, ["isNewData"]), r = b(i),
                            s = n.cachedCampaigns;
                        if (!s[r]) return a("setCardLoading", !0), _["d"].query(i).then(function (e) {
                            a("setCampaigns", {data: e, isNewData: o, type: "CARD"}), a("setCachedCampaigns", {
                                data: e,
                                key: r
                            }), a("setCardLoading", !1)
                        }).catch(function (e) {
                            console.log(e), a("setCardLoading", !1)
                        });
                        a("setCampaigns", {data: s[r], isNewData: o, type: "CARD"})
                    }, getCampaigns: function (e) {
                        var t = e.commit;
                        return _["d"].query().then(function (e) {
                            return t("setCampaigns", {data: e, type: "ALL"})
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, getCampaign: function (t, a) {
                        var n = t.commit;
                        _["d"].get(a).then(function (t) {
                            n("setCampaign", t), n("proposal/setData", e.prop("proposal", t), {root: !0}), e.pathEq(["proposal", "meta", "webOrApp", "type"], "form", t) && (t.form = t.proposal.form, n("form/SET_DATA", ["formCreate", e.prop("form", t)], {root: !0}))
                        }).catch(function (e) {
                            return console.log("err", e)
                        })
                    }, copy: function (e, t) {
                        var a = e.commit, n = e.dispatch, o = t.id, i = t.params, r = t.cb;
                        _["d"].copy(o).then(Object(s["a"])(regeneratorRuntime.mark(function e() {
                            return regeneratorRuntime.wrap(function (e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        if (a("setCachedCampaigns", {data: {}}), "TABLE" !== i.layout) {
                                            e.next = 6;
                                            break
                                        }
                                        return e.next = 4, n("getCampaignsByList", i);
                                    case 4:
                                        e.next = 8;
                                        break;
                                    case 6:
                                        return e.next = 8, n("getCampaignsByCard", i);
                                    case 8:
                                        r();
                                    case 9:
                                    case"end":
                                        return e.stop()
                                }
                            }, e)
                        }))).catch(function (e) {
                            return console.log("err", e)
                        })
                    }, delete: function (e, t) {
                        var a = e.commit, n = e.dispatch, o = t.ids, i = t.params, r = t.cb;
                        _["d"].deleteCampaign(o).then(Object(s["a"])(regeneratorRuntime.mark(function e() {
                            return regeneratorRuntime.wrap(function (e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        if (a("setCachedCampaigns", {data: {}}), "TABLE" !== i.layout) {
                                            e.next = 6;
                                            break
                                        }
                                        return e.next = 4, n("getCampaignsByList", i);
                                    case 4:
                                        e.next = 8;
                                        break;
                                    case 6:
                                        return e.next = 8, n("getCampaignsByCard", i);
                                    case 8:
                                        r();
                                    case 9:
                                    case"end":
                                        return e.stop()
                                }
                            }, e)
                        }))).catch(function (e) {
                            return console.log("err", e)
                        })
                    }, updateCampaign: function (e, t) {
                        var a = e.dispatch, n = t.id, o = t.payload;
                        return _["d"].update(n, o).then(function () {
                            a("getCampaigns")
                        }).catch(function (e) {
                            return console.log("err", e)
                        })
                    }, deactivateCampaign: function (e, t) {
                        var a = e.commit, n = e.dispatch, o = e.state, i = t.id, r = t.params;
                        o.deactivatingSet.has(i) || (a("setDeactivatingSet", {
                            value: !0,
                            id: i
                        }), _["d"].deactivate(i).then(Object(s["a"])(regeneratorRuntime.mark(function e() {
                            return regeneratorRuntime.wrap(function (e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        if (a("setCachedCampaigns", {data: {}}), "TABLE" !== r.layout) {
                                            e.next = 6;
                                            break
                                        }
                                        return e.next = 4, n("getCampaignsByList", r);
                                    case 4:
                                        e.next = 8;
                                        break;
                                    case 6:
                                        return e.next = 8, n("getCampaignsByCard", r);
                                    case 8:
                                    case"end":
                                        return e.stop()
                                }
                            }, e)
                        }))).catch(function (e) {
                            return console.log("err", e)
                        }).finally(function () {
                            return a("setDeactivatingSet", {value: !1, id: i})
                        }))
                    }, requestEstimations: f()(function (e) {
                        var t = e.commit, a = e.getters;
                        _["d"].getEstimations(a.campaignId, a.proposal).then(function (e) {
                            return t("setEstimations", e)
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, 250), updateTitle: function (e, t) {
                        var a = t.newValue, n = t.oldValue;
                        return a.title.trim() && a.title !== n.title ? _["d"].updateTitle(a.id, a.title).then(function (e) {
                            var t = e.title;
                            return n.title = t, t
                        }) : r.a.resolve()
                    }, updateGa: function (e) {
                        var t = e.commit, a = e.getters, n = e.dispatch,
                            o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                        _["d"].updateGa(a.campaignId, o).then(function () {
                            t("setGa", o), n("getGAConversionList")
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, getGAConversionList: function (e) {
                        var t = e.commit, a = e.getters;
                        _["d"].listConversions({campaignId: a.campaignId}).then(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            t("setGAConversionList", e)
                        }, function () {
                            return t("setGAConversionList", [])
                        })
                    }, getPixelConversionList: function (e) {
                        var t = e.commit, a = e.getters;
                        _["d"].listConversions({campaignId: a.campaignId, type: "pixel"}).then(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            t("setPixelConversionList", e)
                        }, function () {
                            return t("setPixelConversionList", [])
                        })
                    }, resetGa: function (e) {
                        var t = e.commit, a = e.getters;
                        _["d"].updateGa(a.campaignId, a.googleAnalyticsBackup).then(function () {
                            return t("setGa", a.googleAnalyticsBackup)
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, resetFacebookPixel: function (e) {
                        var t = e.commit, a = e.getters;
                        _["d"].updatePixel(a.campaignId, a.facebookPixelBackup).then(function () {
                            return t("setPixel", a.facebookPixelBackup)
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, resetFbApp: function (e) {
                        var t = e.commit, a = e.getters;
                        _["d"].updateFbApp(a.campaignId, a.fbAppBackup).then(function () {
                            return t("setFbApp", a.fbAppBackup)
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, refreshCampaignImage: function (e, t) {
                        var a = e.commit;
                        _["d"].get(t).then(function (e) {
                            var t = e.image;
                            return a("setCampaignImage", t)
                        }).catch(function (e) {
                            return console.log("err", e)
                        })
                    }, updateFacebookPixel: function (e) {
                        var t = e.commit, a = e.getters,
                            n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                        _["d"].updatePixel(a.campaignId, n).then(function () {
                            return t("setPixel", n)
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, updateFbApp: function (e) {
                        var t = e.commit, a = e.getters,
                            n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                        _["d"].updateFbApp(a.campaignId, n).then(function () {
                            return t("setFbApp", n)
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, extendCampaign: function (e, t) {
                        var a = e.dispatch, n = t.id, o = t.value;
                        return _["d"].extend(n, o).then(function () {
                            a("getCampaigns")
                        })
                    }, getAvailableCoupons: function (t) {
                        var a = t.commit, n = t.getters;
                        return _["d"].coupons(n.campaignId).then(function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            a("setCoupons", e.compose(e.reverse, e.sortBy(e.prop("created_at")))(t))
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }
                },
                getters: {
                    campaigns: e.prop("campaigns"),
                    totalData: e.prop("totalData"),
                    countByTabs: e.prop("countByTabs"),
                    isCardLoading: e.prop("isCardLoading"),
                    hasCampaigns: e.pathSatisfies(e.gt(e.__, 0), ["campaigns", "length"]),
                    campaign: e.prop("campaign"),
                    autoOptimization: e.path(["campaign", "auto_optimization"]),
                    proposal: e.path(["campaign", "proposal"]),
                    estimations: e.prop("estimations"),
                    campaignId: e.path(["campaign", "id"]),
                    intel: e.path(["campaign", "intel"]),
                    intelFacebookPage: e.path(["campaign", "intel", "facebookPage"]),
                    lastUpdated: e.prop("lastUpdated"),
                    billingPlan: e.path(["campaign", "billing_plan"]),
                    googleAnalytics: e.path(["campaign", "intel", "googleAnalytics"]),
                    googleAnalyticsBackup: e.prop(["googleAnalyticsBackup"]),
                    facebookPixelBackup: e.prop(["facebookPixelBackup"]),
                    isUpdate: function (t) {
                        return h.has(e.path(["campaign", "status"], t))
                    },
                    title: e.path(["campaign", "title"]),
                    campaignImage: e.path(["campaign", "image"]),
                    serviceFee: e.pathOr(.13, ["campaign", "service_fee"]),
                    selectedCoupons: function (t, a) {
                        var n = t.coupons, o = void 0 === n ? [] : n, i = a.campaignId;
                        return o.filter(e.propEq("campaign_id", i))
                    },
                    couponsWithouCreditCard: function (e, t) {
                        var a = t.selectedCoupons, n = void 0 === a ? [] : a;
                        return n.filter(function (e) {
                            return !e.need_credit_card
                        })
                    },
                    needCreditCard: function (t, a) {
                        var n = e.propOr([], "selectedCoupons", a);
                        return !Object(g["N"])(n) || n.every(e.prop("need_credit_card"))
                    },
                    appleAppIdConnected: e.compose(g["N"], e.path(["campaign", "intel", "appleAppId"]))
                }
            };
            var b = e.compose(e.join("-"), e.map(e.join(":")), o.a)
        }).call(this, a("b17e"))
    }, "0a81": function (e, t, a) {
    }, "0d21": function (e, t, a) {
    }, "0e1c": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("31a2"), o = a("c1df"), i = a.n(o), r = a("1229");
            t["a"] = {
                props: {data: {type: Object, required: !0}}, data: function () {
                    return {bannerLoaded: !0}
                }, components: {"switch-view": n["a"]}, mixins: [r["a"]], methods: {
                    isTurnedOff: function (t) {
                        var a = e.equals(t);
                        if (a("feed")) {
                            if (this.isStatus("in_progress") || this.isStatus("in_review_express")) return !0
                        } else if (a("board")) {
                            if (this.isStatus("in_progress") || this.isStatus("in_review_express")) return !0
                        } else if (a("proposal")) {
                            if (this.isStatus("in_review_express")) return !0;
                            if (this.isStatus("running")) return !1
                        }
                    }, isStatus: function (t) {
                        return e.propEq("status", t)(this)
                    }
                }, computed: {
                    bannerExist: function () {
                        return this.bannerLoaded
                    }, isStarted: function () {
                        return e.path(["data", "started_at"], this)
                    }, statusString: function () {
                        return this.isStatus("running") ? "MY_CAMPAIGNS.status_running" : this.isStatus("in_progress") || this.isStatus("onboarding") ? "MY_CAMPAIGNS.status_in_progress" : this.isStatus("in_review") || this.isStatus("proposal_review") ? "MY_CAMPAIGNS.status_in_review" : this.isStatus("in_review_express") ? "MY_CAMPAIGNS.status_in_review_express" : this.isStatus("sample") ? "MY_CAMPAIGNS.status_sample" : "MY_CAMPAIGNS.status_deactivated"
                    }, settingTooltip: function () {
                        return this.isStatus("in_progress") ? "COMMON.ads" : "COMMON.edit"
                    }, reviewModalMesage: function () {
                        return this.$t("MY_CAMPAIGNS.review_warn")
                    }, extendText: function () {
                        return this.$t("DASHBOARD.run_longer")
                    }, isStatusMentVisible: function () {
                        var e = this.schedule || {};
                        return e && !e.continuously && "running" === this.status && this.daysLeft <= 3
                    }, daysLeft: function () {
                        var e = this.schedule;
                        if (e) {
                            var t = i()(e.endDate).endOf("day").toDate() - new Date;
                            return Math.floor(t / 864e5)
                        }
                    }, schedule: e.path(["data", "proposal", "schedule"])
                }
            }
        }).call(this, a("b17e"))
    }, "0ecb": function (e, t, a) {
        "use strict";
        (function (e) {
            t["a"] = {
                props: {
                    text: {type: String},
                    html: {type: String},
                    active: {type: Boolean, default: !1},
                    btnText: {type: String},
                    cb: {type: Function, default: e.always(void 0)}
                }, computed: {
                    _active: {
                        get: e.prop("active"), set: function (e) {
                            this.$emit("update:active", e)
                        }
                    }, _btnText: function () {
                        return this.btnText || this.$t("COMMON.close")
                    }
                }, methods: {
                    btnClicked: function () {
                        this._active = !1, this.$emit("btnClicked"), this.cb()
                    }
                }
            }
        }).call(this, a("b17e"))
    }, 1046: function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("2638"), o = a.n(n), i = (a("7f7f"), a("cebc")), r = a("35ec"), s = a("2f62"), c = a("fa7d");
            t["a"] = {
                data: function () {
                    return {bottomVisible: !1}
                },
                computed: Object(i["a"])({}, Object(s["mapGetters"])("user", ["user", "accountId", "account", "accounts", "defaultAccountId"]), {routeMeta: e.pathOr({}, ["$route", "meta"])}),
                render: function () {
                    var e = arguments[0], t = this.routeMeta.floatingAccountVisible;
                    if (!1 === t || !Object(c["N"])(this.accounts)) return null;
                    var a = this.bottomVisible, n = this.setBottomVisible, i = this.renderBottom, s = this.account,
                        l = void 0 === s ? {} : s, d = Object(c["z"])(l.name) || "d", u = {
                            class: "head-account-icon",
                            attrs: {color: l.color},
                            nativeOn: {click: this.handleIconClick},
                            directives: [{name: "tooltip", value: l.name}]
                        };
                    return e(r["s"], [a && e(r["f"], {
                        nativeOn: {
                            click: function () {
                                return n(!1)
                            }
                        }
                    }), e(r["k"], {attrs: {routeName: this.$route.name}}, [e(r["c"], o()([{}, u]), [d]), i()])])
                },
                methods: Object(i["a"])({}, Object(s["mapActions"])("user", ["setDefaultAccount"]), {
                    handleElemClick: function (e) {
                        void 0 !== e && this.accountId !== e && (this.changeAccount(e, this.routeMeta.canChangeAccount), this.setBottomVisible(!1))
                    }, renderBottom: function () {
                        var e = this, t = this.$createElement, a = this.account, n = void 0 === a ? {} : a,
                            i = this.user, s = void 0 === i ? {} : i, l = this.defaultAccountId;
                        if (!this.bottomVisible) return null;
                        var d = {directives: [{name: "line-clamp", value: 2}]}, u = n.name, p = l === n.id;
                        return t(r["h"], [t(r["i"], [t(r["r"], [s.name]), t(r["q"], [s.email])]), t(r["d"], [t(r["m"], [t(r["c"], {
                            style: {cursor: "default"},
                            attrs: {color: n.color}
                        }, [Object(c["z"])(u)]), t(r["n"], [t(r["o"], o()([{}, d]), [u]), p ? t(r["a"], [this.$t("ACCOUNT_SETTING.default_account")]) : t(r["p"], {
                            nativeOn: {
                                click: function () {
                                    e.setDefaultAccount(n.id), e.setBottomVisible(!1)
                                }
                            }
                        }, [this.$t("ACCOUNT_SETTING.set_as_default_account")])])]), this.renderAccounts()]), t(r["j"], {nativeOn: {click: this.handleAddClick}}, [t(r["e"], ["+ Add Account"])])])
                    }, setBottomVisible: function (e) {
                        this.bottomVisible = e
                    }, renderAccounts: function () {
                        var t = this, a = this.$createElement, n = Number(this.accountId);
                        return e.compose(e.map(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, n = e.name,
                                o = e.is_default, i = e.color, s = e.id;
                            return a(r["b"], {
                                attrs: {"data-account-id": s}, nativeOn: {
                                    click: function () {
                                        return t.handleElemClick(s)
                                    }
                                }
                            }, [a(r["g"], {attrs: {color: i}}), a(r["l"], {class: "adriel-ellipsis"}, [n]), o && a(r["a"], [t.$t("ACCOUNT_SETTING.default_account")])])
                        }), e.reject(e.propEq("id", n)), e.propOr([], "accounts"))(this)
                    }, handleAddClick: function () {
                        this.$emit("showAccountModal", {editMode: !1}), this.setBottomVisible(!1)
                    }, handleIconClick: function () {
                        this.setBottomVisible(!this.bottomVisible)
                    }
                })
            }
        }).call(this, a("b17e"))
    }, 1097: function (e, t, a) {
    }, 1118: function (e, t, a) {
    }, 1160: function (e, t, a) {
        "use strict";
        var n = a("537c"), o = a.n(n);
        o.a
    }, 1229: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("4c6b");
            t["a"] = {
                props: {data: {type: Object, required: !1}},
                computed: {
                    campaignId: e.path(["data", "id"]),
                    status: e.path(["data", "status"]),
                    campaignType: e.pathOr("", ["data", "proposal", "meta", "webOrApp", "type"])
                },
                methods: {
                    switchValueChanged: function (e) {
                        var t = e.value, a = e.id, n = e.status, o = n || this.status, i = a || this.campaignId;
                        this.$emit("active", {id: i, value: t, status: o})
                    }, boxClicked: function (e) {
                        var t = e.status, a = e.id, n = e.campaignType, o = t || this.status, i = a || this.campaignId,
                            r = n || this.campaignType;
                        switch (o) {
                            case"running":
                            case"deactivated":
                                this.goTo({where: "board", id: i});
                                break;
                            case"onboarding":
                                this.goTo({where: "onboarding", id: i});
                                break;
                            default:
                                this.goTo({where: "proposal", id: i, campaignType: r});
                                break
                        }
                    }, goTo: function (e) {
                        var t = e.where, a = e.id, n = e.disabled, o = e.campaignType, i = a || this.campaignId;
                        if (!0 !== n) {
                            var r;
                            switch (t) {
                                case"proposal":
                                    r = {name: "form" === o ? "Proposal/Form" : "Proposal/Ads", params: {id: i}};
                                    break;
                                case"board":
                                    r = {name: "Dashboard", params: {id: i}};
                                    break;
                                case"feed":
                                    break;
                                case"onboarding":
                                    r = {name: "Onboarding", params: {id: i}};
                                    break
                            }
                            r && this.$router.push(r)
                        }
                    }, deletable: function (e) {
                        var t = e.status;
                        return t === n["d"].DEACTIVATED || t === n["d"].IN_PROGRESS || t === n["d"].ONBOARDING
                    }, duplicable: function (e) {
                        var t = e.status;
                        return t !== n["d"].IN_REVIEW_EXPRESS && t !== n["d"].SAMPLE
                    }, runLongerPossible: function (t) {
                        var a = t.data;
                        switch (a.status) {
                            case n["d"].RUNNING:
                                return !e.path(["proposal", "schedule", "continuously"], a);
                            case n["d"].DEACTIVATED:
                                return !0
                        }
                    }, resultsPossible: function (t) {
                        var a = t.status;
                        return e.contains(a, [n["d"].RUNNING, n["d"].DEACTIVATED, n["d"].SAMPLE])
                    }, extendBudgetPossible: function (e) {
                        var t = e.status;
                        return t !== n["d"].IN_REVIEW_EXPRESS && t !== n["d"].SAMPLE
                    }, changeSettingPossible: function (e) {
                        var t = e.status;
                        return t !== n["d"].IN_REVIEW_EXPRESS
                    }, changeEnablePossible: function (e) {
                        var t = e.status;
                        return t !== n["d"].IN_REVIEW_EXPRESS
                    }, checkEdit: function (e) {
                        var t = e.status, a = e.id, n = e.campaignType, o = t || this.status, i = a || this.campaignId,
                            r = n || this.campaignType;
                        this.changeSettingPossible({status: o}) && ("onboarding" === o ? this.goTo({
                            where: "onboarding",
                            id: i
                        }) : this.goTo({where: "proposal", id: i, campaignType: r}))
                    }, checkRunLonger: function (e) {
                        var t = e.data, a = t || this.data;
                        if (this.runLongerPossible({data: a})) switch (a.status) {
                            case n["d"].RUNNING:
                                return this.$emit("modal:days", a.id);
                            case n["d"].DEACTIVATED:
                                return this.$emit("modal:shedule", a.id)
                        }
                    }, checkResult: function (e) {
                        var t = e.status, a = e.id, n = t || this.status, o = a || this.campaignId;
                        this.resultsPossible({status: n}) && this.goTo({where: "board", id: o})
                    }, checkBudget: function (e) {
                        var t = e.status, a = e.id, o = t || this.status, i = a || this.campaignId;
                        if (this.extendBudgetPossible({status: o})) switch (o) {
                            case n["d"].DEACTIVATED:
                            case n["d"].IN_PROGRESS:
                                return this.$emit("modal:shedule", i);
                            default:
                                return this.$emit("modal:budget", i)
                        }
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "163e": function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("fa7d");
            t["a"] = {
                props: {max: {type: [String, Number], required: !0}, str: {type: String, default: ""}},
                computed: {current: e.compose(n["e"], e.prop("str"))}
            }
        }).call(this, a("b17e"))
    }, "16d0": function (e, t, a) {
        "use strict";
        var n = a("0230"), o = a.n(n);
        o.a
    }, "170a": function (e, t, a) {
        "use strict";
        t["a"] = {
            render: function () {
                var e = arguments[0], t = this.value ? "#5aabe3" : "#ccc";
                return e("b-icon", {attrs: {pack: "fas", icon: "check-square"}, style: {color: t}})
            }, props: {value: {type: Boolean, default: !1}}
        }
    }, 1836: function (e, t, a) {
        "use strict";
        (function (e) {
            a("6762"), a("2fdb"), a("c5f6"), a("2f62");
            var n = a("c1df"), o = a.n(n), i = a("fa7d"),
                r = e.compose(e.ifElse(isNaN, e.always("-"), e.max(0)), Object(i["Q"])(1), e.divide(e.__, 864e5));

            function s(e) {
                return Number(e) > 1 ? "days" : "day"
            }

            t["a"] = {
                props: ["deactivated", "daysLeft", "startDate", "endDate", "asap", "continuously", "startedAt", "deactivatedAt"],
                computed: {
                    _startDate: function () {
                        return this.asap || this.continuously ? Object(i["cb"])(this.startedAt || this.startDate) || new Date : Object(i["cb"])(this.startDate || this.startedAt) || new Date
                    }, _endDate: function () {
                        return Object(i["cb"])(this.endDate || this.deactivatedAt) || new Date
                    }, startDateStr: function () {
                        return this.asap || this.continuously ? o()(this.startedAt || this.startDate).format("MMM D") : o()(this.startDate).format("MMM D")
                    }, endDateStr: function () {
                        return o()(this.deactivatedAt || this.endDate).format("MMM D")
                    }, daysRan: function () {
                        return r(Object(i["cb"])(this.deactivatedAt || new Date) - this._startDate)
                    }, daysStr: function () {
                        return s(this.daysRan)
                    }, scheduledStr: function () {
                        return s(this.daysLeftBeforeStart)
                    }, daysLeftBeforeStart: function () {
                        return this._startDate ? o()(this._startDate).diff(o()(), "days") + 1 : 0
                    }, status: function () {
                        return this.deactivated ? "deactivated" : this.continuously ? "continuously" : o()(this._endDate).isSame(o()(), "day") ? "finalDay" : !this.continuously && o()(this._startDate).isAfter(o()()) ? "scheduled" : "startEnd"
                    }, leftStyle: function () {
                        var t = this;
                        return e.ifElse(e.contains(e.__, ["deactivated", "continuously", "finalDay"]), e.always({width: "100%"}), function () {
                            var a = e.clamp(0, 100, 100 * (1 - t.daysLeft / r(t._endDate - t._startDate))) + "%";
                            return {width: a}
                        })(this.status)
                    }, remainsLittle: function () {
                        return ["finalDay", "startEnd"].includes(this.status) && void 0 !== this.daysLeft && this.daysLeft <= 3
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "199c": function (e, t, a) {
        "use strict";
        (function (e) {
            var n, o = a("bd86"), i = (a("a481"), a("7f7f"), a("cebc")), r = a("2f62"), s = a("fb1d"), c = a("38ed"),
                l = a("396e"), d = a("51c6"), u = a("739d"), p = a("1046"), _ = a("cab9"), m = a("fa7d"), f = a("6e77"),
                g = a("4677"), h = a("a744"), b = a("ebb6"), v = a("69dd"), y = Object(_["a"])(p["a"]);
            t["a"] = {
                name: "App",
                components: {
                    Logout: c["a"],
                    LoadingAdriel: l["a"],
                    ModalBox: d["a"],
                    DemoFAB: u["a"],
                    NewCampaign: s["a"],
                    FloatingAccount: y
                },
                data: function () {
                    return {ready: !1, showNotSupported: !1}
                },
                methods: Object(i["a"])({}, Object(r["mapMutations"])("view", ["setWindowDimension", "setIsNewCampaignModalActive"]), Object(r["mapMutations"])("user", ["setAccountId"]), Object(r["mapActions"])("user", ["logout"]), {
                    bootIntercom: function () {
                        this.user && !this.isDemo ? this.$intercom.boot({
                            user_id: this.user.id,
                            name: this.user.name,
                            email: this.user.email,
                            user_hash: this.user.intercom_hash
                        }) : this.$intercom.boot()
                    }, updateIntercom: function () {
                        var e = this.user;
                        e && !this.isDemo ? this.$intercom.update({
                            user_id: e.id,
                            name: e.name,
                            email: e.email,
                            user_hash: e.intercom_hash
                        }) : this.$intercom.update()
                    }, checkBrowser: function () {
                        this.showNotSupported = !Object(m["J"])()
                    }, clickSignUpFromDemo: function () {
                        this.setDemoAccountModalActive(!1), this.logout(), this.$router.push({name: "Sign/Join"})
                    }
                }),
                computed: Object(i["a"])({}, Object(r["mapGetters"])("user", ["user"]), Object(r["mapState"])("view", ["isDemoAccountModalActive", "isNewCampaignModalActive"]), Object(r["mapGetters"])("campaign", ["hasCampaigns"])),
                created: function () {
                    this.ready = !0, this.checkBrowser()
                },
                watch: (n = {
                    user: function () {
                        arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                        this.updateIntercom()
                    }
                }, Object(o["a"])(n, "user.language", {
                    handler: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "en";
                        this.$i18n.locale = e, this.$validator.localize(e)
                    }, immediate: !0, deep: !0
                }), Object(o["a"])(n, "$route.query.accountId", {
                    handler: function (e) {
                        axios.defaults.headers.common.AccountId = e, this.setAccountId(e)
                    }, immediate: !0
                }), Object(o["a"])(n, "cAccountId", {
                    handler: function (t) {
                        if (t) {
                            var a = this.$route;
                            this.$router.replace({path: a.path, query: e.assoc("accountId", t, a.query)})
                        }
                    }, immediate: !0
                }), n),
                mounted: function () {
                    this.bootIntercom();
                    var t = Object(f["a"])(window, "resize").pipe(Object(g["a"])(150), Object(h["a"])("init"), Object(b["a"])(function () {
                        return {width: window.innerWidth, height: window.innerHeight}
                    }), Object(v["a"])(e.equals));
                    this.$subscribeTo(t, this.setWindowDimension)
                }
            }
        }).call(this, a("b17e"))
    }, "19e8": function (e, t, a) {
        "use strict";
        var n = function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return a("div", {
                staticClass: "card-view",
                class: [{dimmed: e.dimmed, "card-view-editing": e.isEditing}, e.className]
            }, [a("div", {staticClass: "card-view-header"}, [a("span", {staticClass: "card-view-header-title"}, [e._v(e._s(e.label))]), e._t("slot-header"), a("span", {staticClass: "card-view-setting"}, [a("b-tooltip", {
                attrs: {
                    label: e.resetLabel,
                    type: "is-light",
                    position: "is-bottom"
                }
            }, [e.resetable ? a("img", {
                attrs: {src: "/img/proposal/retrieve_icon.png"},
                on: {
                    click: function (t) {
                        return e.$emit("edit:reset")
                    }
                }
            }) : e._e()]), e._t("setting", [e.editable && !e.isEditing ? a("img", {
                directives: [{
                    name: "tooltip",
                    rawName: "v-tooltip",
                    value: {content: e.settingLabel, visible: !!e.settingLabel},
                    expression: "{content: settingLabel, visible: !!settingLabel}"
                }], staticClass: "card-view-setting-icon", attrs: {src: e.settingImgSrc}, on: {
                    click: function (t) {
                        return e.$emit("edit:start")
                    }
                }
            }) : e._e()]), a("b-tooltip", {
                attrs: {
                    label: e.confirmLabel,
                    type: "is-light",
                    position: "is-bottom"
                }
            }, [e.editable && e.isEditing ? a("img", {
                attrs: {src: "/img/proposal/edit_ok_icon.png"},
                on: {
                    click: function (t) {
                        return e.$emit("edit:end")
                    }
                }
            }) : e._e()])], 2)], 2), a("div", {staticClass: "card-view-body"}, [e._t("default")], 2)])
        }, o = [], i = {
            created: function () {
            },
            props: {
                isEditableAlways: {type: Boolean, default: !1},
                isEditing: {type: Boolean, default: !1},
                editable: {type: Boolean, default: !1},
                label: {type: String, required: !0},
                dimmed: {type: Boolean, required: !1},
                resetLabel: {type: String},
                confirmLabel: {type: String},
                settingLabel: {type: String},
                className: {type: [String, Array], default: ""},
                resetable: {type: Boolean, default: !1},
                settingImgSrc: {type: String, default: "/img/proposal/edit_icon.png"}
            }
        }, r = i, s = (a("9de3"), a("f59a"), a("2877")), c = Object(s["a"])(r, n, o, !1, null, null, null);
        t["a"] = c.exports
    }, "1ac1": function (e, t, a) {
    }, "1bc2": function (e, t, a) {
        "use strict";
        a("96cf");
        var n = a("3b8d"), o = "/payment";

        function i() {
            return r.apply(this, arguments)
        }

        function r() {
            return r = Object(n["a"])(regeneratorRuntime.mark(function e() {
                var t;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.get("".concat(o, "/client_token"));
                        case 2:
                            return t = e.sent, e.abrupt("return", t.data.clientToken);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), r.apply(this, arguments)
        }

        function s() {
            return c.apply(this, arguments)
        }

        function c() {
            return c = Object(n["a"])(regeneratorRuntime.mark(function e() {
                var t;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.get("".concat(o, "/methods"));
                        case 2:
                            return t = e.sent, e.abrupt("return", t.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), c.apply(this, arguments)
        }

        function l(e) {
            return d.apply(this, arguments)
        }

        function d() {
            return d = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.post("".concat(o, "/methods"), {cardToken: t});
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), d.apply(this, arguments)
        }

        function u(e) {
            return p.apply(this, arguments)
        }

        function p() {
            return p = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a, n, i, r, s, c;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return a = t.amount, n = t.nonce, i = void 0 === n ? null : n, r = t.paymentMethodToken, s = void 0 === r ? null : r, e.next = 3, axios.post("".concat(o, "/checkout"), {
                                amount: a,
                                nonce: i,
                                paymentMethodToken: s
                            });
                        case 3:
                            return c = e.sent, e.abrupt("return", c.data);
                        case 5:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), p.apply(this, arguments)
        }

        function _(e) {
            return m.apply(this, arguments)
        }

        function m() {
            return m = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.post("".concat(o, "/default_method"), {cardId: t});
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), m.apply(this, arguments)
        }

        function f(e, t) {
            return g.apply(this, arguments)
        }

        function g() {
            return g = Object(n["a"])(regeneratorRuntime.mark(function e(t, a) {
                var n;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.delete("".concat(o, "/methods"), {
                                params: {
                                    cardId: t,
                                    stopCampaigns: a ? 1 : 0
                                }
                            });
                        case 2:
                            return n = e.sent, e.abrupt("return", n.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), g.apply(this, arguments)
        }

        function h(e) {
            return b.apply(this, arguments)
        }

        function b() {
            return b = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.post("".concat(o), {amount: t});
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), b.apply(this, arguments)
        }

        function v(e) {
            return y.apply(this, arguments)
        }

        function y() {
            return y = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a, n, i, r, s;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return a = t.payerId, n = t.paymentId, i = t.amount, r = t.campaignId, e.next = 3, axios.post("".concat(o, "/execute"), {
                                payerId: a,
                                paymentId: n,
                                amount: i,
                                campaignId: r
                            });
                        case 3:
                            return s = e.sent, e.abrupt("return", s.data);
                        case 5:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), y.apply(this, arguments)
        }

        t["a"] = {
            clientToken: i,
            getPaymentMethods: s,
            addPaymentMethod: l,
            checkout: u,
            makeDefault: _,
            deletePaymentMethod: f,
            createPayment: h,
            executePayment: v
        }
    }, "1f11": function (e, t, a) {
        "use strict";
        var n = a("6130"), o = a.n(n);
        o.a
    }, "1f5d": function (e, t, a) {
        "use strict";
        var n = a("8b5f"), o = a.n(n);
        o.a
    }, "1f73": function (e, t) {
        e.exports = {
            en: {
                header_modal: "You're on a demo site",
                need_real_account: "If you'd like to use the full version, please sign up.",
                sign_up_and_start: "Sign up and start right away",
                want_to_signup: "I want to signup",
                want_to_use_full_version: "Want to use the full version?"
            },
            jp: {
                header_modal: "�볝굦��깈�㏂궢�ㅳ깉�㎯걲",
                need_real_account: "�뺛꺂�먦꺖�멥깾�녈굮�붷닶�ⓦ걮�잆걚�닷릦���곭쇉�꿔걮�╉걦�졼걬�꾠��",
                sign_up_and_start: "�삯뙯�쀣겍餓듽걲�먨닶�ⓦ걲��",
                want_to_signup: "�삯뙯�쀣걼��",
                want_to_use_full_version: "�뺛꺂�먦꺖�멥깾�녈굮鵝욍걚�잆걚�㎯걲�뗰폕"
            },
            ko: {
                header_modal: "�곕え �섏씠吏��먯꽌�� �대떦 湲곕뒫�� �댁슜�섏떎 �� �놁뒿�덈떎",
                need_real_account: "�뚯썝 媛��� �� �꾨뱶由ъ뿕�� 紐⑤뱺 �쒕퉬�ㅻ� �쒗븳 �놁씠 �댁슜�섏떎 �� �덉뒿�덈떎.",
                sign_up_and_start: "�뚯썝媛��� �섎윭媛�湲�",
                want_to_signup: "媛��낇븯湲�",
                want_to_use_full_version: "愿묎퀬瑜� 留뚮뱾�대낫�몄슂!"
            }
        }
    }, "1fee": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return "checkbox" === e.type ? a("input", {
                    directives: [{
                        name: "model",
                        rawName: "v-model",
                        value: e._value,
                        expression: "_value"
                    }],
                    ref: "elem",
                    staticClass: "adriel-common-input",
                    attrs: {placeholder: e.placeholder, autofocus: e.autofocus, type: "checkbox"},
                    domProps: {checked: Array.isArray(e._value) ? e._i(e._value, null) > -1 : e._value},
                    on: {
                        change: function (t) {
                            var a = e._value, n = t.target, o = !!n.checked;
                            if (Array.isArray(a)) {
                                var i = null, r = e._i(a, i);
                                n.checked ? r < 0 && (e._value = a.concat([i])) : r > -1 && (e._value = a.slice(0, r).concat(a.slice(r + 1)))
                            } else e._value = o
                        }
                    }
                }) : "radio" === e.type ? a("input", {
                    directives: [{
                        name: "model",
                        rawName: "v-model",
                        value: e._value,
                        expression: "_value"
                    }],
                    ref: "elem",
                    staticClass: "adriel-common-input",
                    attrs: {placeholder: e.placeholder, autofocus: e.autofocus, type: "radio"},
                    domProps: {checked: e._q(e._value, null)},
                    on: {
                        change: function (t) {
                            e._value = null
                        }
                    }
                }) : a("input", {
                    directives: [{name: "model", rawName: "v-model", value: e._value, expression: "_value"}],
                    ref: "elem",
                    staticClass: "adriel-common-input",
                    attrs: {placeholder: e.placeholder, autofocus: e.autofocus, type: e.type},
                    domProps: {value: e._value},
                    on: {
                        input: function (t) {
                            t.target.composing || (e._value = t.target.value)
                        }
                    }
                })
            }, o = [], i = a("4206"), r = i["a"], s = (a("92ae"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, null, null);
        t["a"] = c.exports
    }, 2147: function (e, t, a) {
        "use strict";
        (function (e) {
            a("7514");
            var n = a("cebc"), o = (a("20d6"), a("5176")), i = a.n(o), r = a("dde5"), s = a("fa7d"), c = a("d3fb"),
                l = a("9586"), d = a("1b92"), u = a("66aa"), p = a("f59d");

            function _() {
                return {
                    user: null,
                    accountId: void 0,
                    accounts: [],
                    authFailed: !1,
                    fbInfo: {},
                    paymentMethods: [],
                    referralShown: !1,
                    gaInfo: {},
                    loadingPaymentMethods: !1,
                    permissionsMissing: !1
                }
            }

            t["a"] = {
                namespaced: !0, state: _(), mutations: {
                    login: function (e, t) {
                        t && (e.user = t)
                    }, authFail: function (e) {
                        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                        e.authFailed = t
                    }, logout: function (e) {
                        e.user = null
                    }, setFbInfo: function (e, t) {
                        e.fbInfo = t
                    }, setAccounts: function (e, t) {
                        e.accounts = t
                    }, setPaymentMethods: function (e, t) {
                        e.loadingPaymentMethods = !1, e.paymentMethods = t
                    }, setReferralShown: function (e, t) {
                        e.referralShown = t
                    }, setGaInfo: function (e, t) {
                        e.gaInfo = t
                    }, clearUser: function (e) {
                        i()(e, _())
                    }, setPixelInfo: function (e, t) {
                        e.fbInfo = t
                    }, setPermissionsMissing: function (e, t) {
                        e.permissionsMissing = t
                    }, setAccountId: function (e, t) {
                        e.accountId = t
                    }, deleteAccount: function (t, a) {
                        t.accounts = e.reject(e.propSatisfies(function (e) {
                            return e == a
                        }, "id"), t.accounts)
                    }, updateAccount: function (t, a) {
                        var n = a.id, o = a.account, i = e.findIndex(e.propEq("id", n), t.accounts);
                        i > -1 && (t.accounts = e.over(e.lensIndex(i), e.mergeLeft(o), t.accounts))
                    }
                }, actions: {
                    checkSession: function (e) {
                        var t = e.commit, a = e.dispatch,
                            n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                        return r["c"].session().then(function (e) {
                            return t("authFail", !1), t("login", e), a("fetchAccountList"), e
                        }).catch(function () {
                            return !0 !== n && t("authFail", !0)
                        })
                    }, login: function (e, t) {
                        var a = e.commit;
                        a("login", t)
                    }, logout: function (e) {
                        var t = e.commit;
                        return r["c"].logout().then(function () {
                            t("clearUser")
                        })
                    }, fetchFacebookProfile: function (e, t) {
                        var a, n = e.commit, o = e.state;
                        o.permissionsMissing && (a = "rerequest"), Object(c["a"])(Object(s["t"])({
                            token: t,
                            authType: a
                        })).subscribe(function (e) {
                            n("setPermissionsMissing", !1), n("setFbInfo", e)
                        }, function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {code: "UNKNOWN_ERROR"};
                            "PERMISSIONS_MISSING" === e.code && n("setPermissionsMissing", !0)
                        })
                    }, fetchPaymentMethods: function (e) {
                        var t = e.commit;
                        return this.loadingPaymentMethods = !0, r["k"].getPaymentMethods().then(function (e) {
                            t("setPaymentMethods", e)
                        })
                    }, fetchAccountList: function (t) {
                        var a = t.commit;
                        Object(l["a"])(function () {
                            return r["a"].list()
                        }).pipe(Object(u["a"])(Object(s["p"])()), Object(p["a"])(e.always(Object(d["b"])()))).subscribe(function (e) {
                            return a("setAccounts", e)
                        }, function (e) {
                            return console.log(e)
                        })
                    }, updateBusinessNumber: function (e, t) {
                        var a = e.commit, o = (e.state, e.getters.user), i = e.dispatch;
                        a("login", Object(n["a"])({}, o, {business_no: t})), r["o"].updateBizNo(t).then(function () {
                            return i("checkSession")
                        })
                    }, createAccount: function (e, t) {
                        e.commit;
                        var a = e.dispatch;
                        return r["o"].createAccount(t).then(function (e) {
                            return a("fetchAccountList"), e
                        })
                    }, deleteAccount: function (e, t) {
                        var a = e.commit;
                        r["o"].deleteAccount(t).then(function (e) {
                            a("deleteAccount", t)
                        })
                    }, setDefaultAccount: function (e, t) {
                        e.commit;
                        var a = e.dispatch;
                        r["o"].setDefaultAccount(t).then(function () {
                            a("fetchAccountList")
                        })
                    }, updateAccount: function (t, a) {
                        var n = t.commit, o = a.id, i = a.account;
                        r["o"].updateAccount(o, i).then(function () {
                            n("updateAccount", {id: o, account: e.pick(["color", "name"], i)})
                        })
                    }
                }, getters: {
                    user: e.prop("user"),
                    accountId: function (e, t) {
                        var a = t.defaultAccountId;
                        return 0 == e.accountId ? a : e.accountId
                    },
                    accounts: e.prop("accounts"),
                    account: function (t, a) {
                        var n = a.accounts, o = void 0 === n ? [] : n, i = a.accountId, r = void 0 === i ? -1 : i,
                            s = a.defaultAccount;
                        return o.find(e.propSatisfies(function (e) {
                            return e == r
                        }, "id")) || s
                    },
                    authFailed: e.prop("authFailed"),
                    credits: e.path(["user", "credits"]),
                    fbInfo: e.prop("fbInfo"),
                    gaInfo: e.compose(e.defaultTo({}), e.prop("gaInfo")),
                    referralShown: e.prop("referralShown"),
                    paymentMethods: e.prop("paymentMethods"),
                    hasPaymentMethod: function (e) {
                        return e.paymentMethods.length > 0
                    },
                    isAdmin: e.compose(e.prop("backoffice_admin"), e.propOr({}, "user")),
                    userLang: e.compose(s["w"], e.prop("user")),
                    defaultAccount: function (t, a) {
                        var n = a.accounts, o = void 0 === n ? [] : n;
                        return e.compose(e.defaultTo({}), e.find(e.prop("is_default")))(o)
                    },
                    defaultAccountId: function (e, t) {
                        var a = t.defaultAccount, n = void 0 === a ? {} : a;
                        return n.id
                    },
                    userId: e.path(["user", "id"])
                }
            }
        }).call(this, a("b17e"))
    }, "237c": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("768b"), o = (a("7f7f"), a("cebc")), i = a("2f62"), r = a("1585"), s = a("87ee"), c = a("c4cc"),
                l = a("9f2d"), d = a("6e77"), u = a("a748"), p = "ACTION_LOGOUT", _ = "ACTION_NEW_CAMPAIGN",
                m = "ACTION_EXPRESS", f = [{name: "manager", to: "/manager", label: "LEFT_NAV.manager"}, {
                    name: "campaign",
                    to: "/myCampaigns",
                    label: "LEFT_NAV.my_campaigns"
                }, {name: "account", to: "/accountSetting", label: "LEFT_NAV.account_setting"}, {
                    name: "help",
                    to: "/helpCampaign",
                    label: "LEFT_NAV.help"
                }];
            t["a"] = {
                props: ["isMobileMenuShown"],
                data: function () {
                    return {menus: f}
                },
                methods: Object(o["a"])({}, Object(i["mapActions"])("user", ["logout"]), Object(i["mapMutations"])("view", ["setLogoutModalActive", "setIsNewCampaignModalActive"]), {
                    onClickHandler: function (e) {
                        this.$emit("onMobileMenuClose"), this.isDemo && e !== p ? this.setDemoAccountModalActive(!0) : e === _ ? this.setIsNewCampaignModalActive(!0) : e === p ? this.setLogoutModalActive(!0) : e === m && this.$router.push({name: "Express"})
                    }, getIcon: function (e) {
                        var t = e.name;
                        return "manager" === t ? "handshake-alt" : "campaign" === t ? "folder" : "account" === t ? "user" : "help" === t ? "envelope" : void 0
                    }, watchShow$: function () {
                        var t = this,
                            a = this.$watchAsObservable("isMobileMenuShown", {immediate: !0}).pipe(Object(r["a"])("newValue"), Object(s["a"])(e.identity)),
                            o = Object(n["a"])(a, 2), i = o[0], p = o[1];
                        this.$subscribeTo(i, function () {
                            window.scrollTo(0, 0), Object(d["a"])(window, "scroll").pipe(Object(c["a"])(function (e) {
                                return window.scrollTo(0, 0)
                            }), Object(l["a"])(Object(u["a"])(t.getBeforeDestroy$(), p))).subscribe()
                        })
                    }
                }),
                created: function () {
                    var e = this;
                    this.$nextTick(function () {
                        e.watchShow$()
                    })
                }
            }
        }).call(this, a("b17e"))
    }, "23a2": function (e, t) {
        e.exports = {
            en: {
                adriel_credit: "Adriel credit",
                amount_other_time: "Amount paid other times",
                amount_this_time: "Amount paid this time",
                amount_total: "Total amount paid for this campaign",
                credit_card: "Credit card",
                credit_deducted: "Credit deducted",
                invoice_for: "Invoices for <span>{email}</span>",
                invoice_info_details: "Billing details",
                invoice_info_details_content: 'If you�셝 like to receive your invoice by email, please send us a request at <a href="mailto:support@adriel.ai">support@adriel.ai</a>.',
                invoice_info_email: "Billing email",
                no_invoices: "There are no invoices yet.",
                paid: "Paid",
                payment_status: "Status",
                refunded: "Refunded",
                service_fee: "Service Fee {fee}%",
                service_fee_include_exclude: "incl. fee",
                top_title: "Invoice History",
                transfer: "Transfer"
            },
            jp: {
                adriel_credit: "�㏂깋�ゃ궓�ャ꺕��꺃�멥긿��",
                amount_other_time: "�띶썮�딀뵱�뺛걚�뺛굦�잓뇫窈�",
                amount_this_time: "餓듿썮�딀뵱�뺛걚�뺛굦�잓뇫窈�",
                amount_total: "�볝겗��깵�녈깪�쇈꺍�ャ걡��돂�꾠걬�뚣걼�덅쮫��뇫窈�",
                credit_card: "��꺃�멥긿�덀궖�쇈깋",
                credit_deducted: "��꺃�멥긿�덃렒�ㅶ툑��",
                invoice_for: "<span>{email}</span>��쳦黎귝쎑",
                invoice_info_details: "獄뗦콆�멱㈂榮�",
                invoice_info_details_content: '獄뗦콆�멥굮�▲꺖�ャ겎�쀣걨�뽧굤�잆걚�닷릦����<a href="mailto:support@adriel.ai">support@adriel.ai</a>�ャ꺁��궓�밤깉�믧�곥겂�╉걦�졼걬�꾠��',
                invoice_info_email: "獄뗦콆�멥깳�쇈꺂",
                no_invoices: "獄뗦콆�멥겘�얇걽�귙굤�얇걵��",
                paid: "�딀뵱�뺛걚歷덀겳",
                payment_status: "�밤깇�쇈궭��",
                refunded: "瓦붼뇫歷덀겳",
                service_fee: "�듐꺖�볝궧縕사뵪{fee}%",
                service_fee_include_exclude: "�듐꺖�볝궧縕사뵪渦쇈겳",
                top_title: "獄뗦콆�멨괘閭�",
                transfer: "�곲뇫"
            },
            ko: {
                adriel_credit: "�꾨뱶由ъ뿕 荑좏룿",
                amount_other_time: "�대쾲 寃곗젣瑜� �쒖쇅�� �꾩쟻 寃곗젣 湲덉븸�낅땲��.",
                amount_this_time: "�대쾲 嫄댁뿉 寃곗젣�� 湲덉븸�낅땲��.",
                amount_total: "�대쾲 寃곗젣瑜� �ы븿�� �꾩쟻 寃곗젣 湲덉븸�낅땲��.",
                credit_card: "�좎슜移대뱶",
                credit_deducted: "荑좏룿 �ъ슜",
                invoice_for: "<span>{email}</span>�섏쓽 �몃낫�댁뒪",
                invoice_info_details: "�몃낫�댁뒪 �붿껌",
                invoice_info_details_content: '�몃낫�댁뒪瑜� �대찓�쇰줈 �〓�諛쏄린瑜� �먰븯�� 寃쎌슦, <a href="mailto:support@adriel.ai">support@adriel.ai</a>濡� 蹂꾨룄 �붿껌�� 二쇱떗�쒖삤.',
                invoice_info_email: "寃곗젣 �곸닔利� �섏떊 �대찓�� ",
                no_invoices: "寃곗젣�� �댁뿭�� �놁뒿�덈떎.",
                paid: "吏�遺� �꾨즺",
                payment_status: "吏�遺� �곹깭",
                refunded: "�섎텋 �꾨즺",
                service_fee: "�섏닔猷� {fee}%",
                service_fee_include_exclude: "�섏닔猷� �ы븿",
                top_title: "寃곗젣 �댁뿭",
                transfer: "怨꾩쥖�댁껜"
            }
        }
    }, "24c4": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("cebc"), o = a("db0c"), i = a.n(o), r = a("2f62"), s = a("8fac"), c = {
                ALL: "all",
                RUNNING: "running",
                IN_REVIEW: "in_review",
                IN_PROGRESS: "in_progress",
                DEACTIVATED: "deactivated"
            }, l = (e.tail(i()(c)), {
                all: "MY_CAMPAIGNS.tab_all",
                running: "MY_CAMPAIGNS.tab_running",
                in_review: "MY_CAMPAIGNS.tab_in_review",
                in_progress: "MY_CAMPAIGNS.tab_in_progress",
                deactivated: "MY_CAMPAIGNS.tab_deactivated"
            });
            t["a"] = {
                components: {CampaignBox: s["a"]},
                props: ["campaigns", "startChat", "goToExpress", "ensureCopy", "ensureDelete", "ensureToggleActive", "showTitleModal", "showScheduleModal", "showDaysModal", "showBudgetModal", "CARD_DEFAULT_PARAMS", "isLoading", "isRefresh", "validTabs"],
                mounted: function () {
                    this.scrollObserver = new IntersectionObserver(this.handleScrollObserver), this.scrollObserver.observe(this.$refs.spinner), this.isRefresh || this.setStatusByParmas(this.query)
                },
                beforeDestroy: function () {
                    this.scrollObserver.unobserve(this.$refs.spinner)
                },
                data: function () {
                    return {campaignId: null, selectedTab: c.ALL, offset: 0, limit: 8}
                },
                methods: Object(n["a"])({}, Object(r["mapMutations"])("campaign", ["setCampaigns", "setCachedCampaigns"]), {
                    getTabClass: function (t) {
                        return e.compose(e.path([t, "0"]), e.invert)(c)
                    }, getTabLang: function (e) {
                        return l[e]
                    }, handleScrollObserver: function (e) {
                        var t = e[0].intersectionRatio;
                        t > 0 && window.scrollY > 0 && (this.offset = 0 === this.offset ? 8 : this.offset + 9, this.offset >= 8 && (this.limit = 9), this.$emit("getCampaignsByParams", Object(n["a"])({}, this.passedParams, {
                            offset: this.offset,
                            limit: this.limit,
                            isNewData: !1
                        })))
                    }, onClickSelectTab: function (e) {
                        this.selectedTab !== e && (this.selectedTab = e, this.offset = 0, this.limit = 8, this.$emit("getCampaignsByParams", Object(n["a"])({}, this.passedParams, {isNewData: !0})))
                    }, setStatusByParmas: function (e) {
                        this.selectedTab = e.filters ? e.filters : c.ALL
                    }
                }),
                computed: {
                    query: e.path(["$route", "query"]), filterParams: function () {
                        return this.selectedTab === c.ALL ? {} : {filters: [this.selectedTab]}
                    }, passedParams: function () {
                        return this.filterParams.filters ? Object(n["a"])({}, this.CARD_DEFAULT_PARAMS, this.filterParams) : this.CARD_DEFAULT_PARAMS
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "25a5": function (e, t, a) {
    }, "25eb": function (e, t, a) {
        "use strict";
        (function (e) {
            a.d(t, "l", function () {
                return y
            }), a.d(t, "m", function () {
                return w
            }), a.d(t, "i", function () {
                return k
            }), a.d(t, "j", function () {
                return x
            }), a.d(t, "d", function () {
                return O
            }), a.d(t, "a", function () {
                return A
            }), a.d(t, "k", function () {
                return C
            }), a.d(t, "f", function () {
                return S
            }), a.d(t, "g", function () {
                return E
            }), a.d(t, "e", function () {
                return T
            }), a.d(t, "b", function () {
                return P
            }), a.d(t, "h", function () {
                return I
            }), a.d(t, "c", function () {
                return L
            });
            var n = a("aede"), o = a("2b0e"), i = a("9c56"), r = a("bd2c"), s = a("c082");

            function c() {
                var e = Object(n["a"])(["\n    background-color: #999999;\n    &:hover {\n        background-color: #bbbbbb;\n    }\n"]);
                return c = function () {
                    return e
                }, e
            }

            function l() {
                var e = Object(n["a"])(["\n    display: flex;\n    margin-top: 30px;\n    justify-content: space-between;\n    button {\n        width: 205px;\n        height: 40px;\n        @media screen and (max-width: 800px) {\n            width: 50%;\n            max-width: 190px;\n            &:last-child {\n                margin-left: 10px;\n            }\n        }\n    }\n    @media screen and (max-width: 800px) {\n        margin-top: auto;\n    }\n"]);
                return l = function () {
                    return e
                }, e
            }

            function d() {
                var e = Object(n["a"])(["\n    width: 40px;\n    height: 40px;\n    cursor: pointer;\n    background-color: ", ";\n    @media screen and (max-width: 800px) {\n        width: 12.5%;\n    }\n"]);
                return d = function () {
                    return e
                }, e
            }

            function u() {
                var e = Object(n["a"])(["\n    display: flex;\n    flex-wrap: wrap;\n    width: 320px;\n    margin: 20px auto 0 auto;\n    @media screen and (max-width: 800px) {\n        width: 100%;\n    }\n"]);
                return u = function () {
                    return e
                }, e
            }

            function p() {
                var e = Object(n["a"])(["\n    width: 20px;\n    height: 20px;\n    border-radius: 10px;\n    ", "\n    left: 6px;\n    transition: background-color 0.2s;\n    background-color: ", ";\n"]);
                return p = function () {
                    return e
                }, e
            }

            function _() {
                var e = Object(n["a"])(["\n    input {\n        width: 100%;\n        padding-left: 30px;\n    }\n"]);
                return _ = function () {
                    return e
                }, e
            }

            function m() {
                var e = Object(n["a"])(["\n    ", "\n    width: 100%;\n    padding: 20px 15px 15px 15px;\n    background-color: white;\n    border-bottom-left-radius: 5px;\n    border-bottom-right-radius: 5px;\n    @media screen and (max-width: 800px) {\n        height: calc(100% - 45px);\n    }\n"]);
                return m = function () {
                    return e
                }, e
            }

            function f() {
                var e = Object(n["a"])(["\n    position: absolute;\n    right: 15px;\n    top: 15px;\n    cursor: pointer;\n"]);
                return f = function () {
                    return e
                }, e
            }

            function g() {
                var e = Object(n["a"])(["\n    ", "\n    font-size: 14px;\n    font-weight: 600;\n    color: #4a4a4a;\n"]);
                return g = function () {
                    return e
                }, e
            }

            function h() {
                var e = Object(n["a"])(["\n    width: 100%;\n    height: 45px;\n    background-color: #f7f7f7;\n    position: relative;\n    ", "\n    border-top-left-radius: 5px;\n    border-top-right-radius: 5px;\n"]);
                return h = function () {
                    return e
                }, e
            }

            function b() {
                var e = Object(n["a"])(["\n    ", "\n    margin: 0 auto;\n    border-radius: 5px;\n    background-color: white;\n    width: 450px;\n    @media screen and (max-width: 800px) {\n        width: 100vw;\n        height: 100%;\n        padding: 0;\n        overflow-y: auto;\n        display: flex;\n        flex-direction: column;\n    }\n"]);
                return b = function () {
                    return e
                }, e
            }

            function v() {
                var e = Object(n["a"])(["\n    .modal-content {\n        @media screen and (max-width: 800px) {\n            max-height: initial;\n            height: 100vh;\n        }\n    }\n"]);
                return v = function () {
                    return e
                }, e
            }

            var y = Object(i["b"])(o["default"].options.components.BModal)(v()), w = i["b"].div(b(), r["g"]),
                k = i["b"].div(h(), r["f"]), x = i["b"].span(g(), r["g"]), O = i["b"].img(f()),
                A = i["b"].div(m(), r["h"]),
                C = Object(i["b"])(o["default"].component("account-input-field", s["a"]))(_()),
                S = Object(i["b"])("span", {bgColor: String})(p(), r["b"], e.propOr("#b72b2b", "bgColor")),
                E = i["b"].div(u()), T = Object(i["b"])("span", {bgColor: String})(d(), e.prop("bgColor")),
                P = i["b"].div(l()), I = r["e"], L = r["d"].extend(c())
        }).call(this, a("b17e"))
    }, "288f": function (e, t, a) {
        "use strict";
        var n = a("aede"), o = a("9c56");

        function i() {
            var e = Object(n["a"])(["\n    color: #5aabe3;\n    position: absolute;\n    font-size: 18px;\n    top: 5px;\n    right: 5px;\n"]);
            return i = function () {
                return e
            }, e
        }

        function r() {
            var e = Object(n["a"])(["\n            box-shadow: none;\n            border: solid 2px #5aabe3;\n            background-color: #f2fcff;\n            position: relative;\n        "]);
            return r = function () {
                return e
            }, e
        }

        function s() {
            var e = Object(n["a"])(["\n    ", "\n    display: inline-flex;\n    align-items: center;\n    justify-content: center;\n    border-radius: 3px;\n    cursor: pointer;\n    transition: opacity 0.4s, background-color 0.2s, transform 0.3s;\n    border: solid 1px #999999;\n    background-color: #f7f7f7;\n    font-size: 20px;\n    color: #333333;\n    text-align: center;\n    padding: 0 20px;\n    word-break: keep-all;\n    word-wrap: break-word;\n    white-space: pre-line;\n    margin: 0;\n\n    ", "\n"]);
            return s = function () {
                return e
            }, e
        }

        function c() {
            var e = Object(n["a"])(["\n    font-family: 'Rubik', 'sans-serif', 'Noto Sans';\n"]);
            return c = function () {
                return e
            }, e
        }

        var l = Object(o["a"])(c()), d = Object(o["b"])("div", {selected: Boolean})(s(), l, function (e) {
            var t = e.selected;
            return t && Object(o["a"])(r())
        }), u = o["b"].i(i());
        t["a"] = {
            name: "BasicBox",
            props: {
                selected: {type: Boolean, default: !1},
                className: {type: String, default: ""},
                showCheckIcon: {type: Boolean, default: !0}
            },
            render: function (e) {
                var t = this, a = this.selected, n = this.showCheckIcon;
                return e(d, {
                    attrs: {selected: a}, nativeOn: {
                        click: function () {
                            return t.$emit("click")
                        }
                    }
                }, [this.$slots.default || e("span", ["slot"]), n && a && e(u, {class: "fas fa-check-circle"})])
            }
        }
    }, "2a79": function (e, t, a) {
        "use strict";
        var n = a("c177"), o = a.n(n);
        o.a
    }, "2c40": function (e, t, a) {
        "use strict";
        var n = a("895d"), o = a.n(n);
        o.a
    }, "2d65": function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("6f9a"), o = (a("fa7d"), ["5", "10", "50", "custom"]);
            t["a"] = {
                created: function () {
                },
                props: ["deactivated", "daysLeft", "startDate", "endDate", "continuously", "startedAt", "deactivatedAt", "active"],
                components: {"running-status": n["a"]},
                data: function () {
                    return {options: o, selected: 10, inputValue: ""}
                },
                computed: {
                    _active: {
                        get: e.prop("active"), set: function (e) {
                            this.$emit("update:active", e)
                        }
                    }, _btnText: function () {
                        return this.$t("COMMON.publish")
                    }
                },
                methods: {
                    confirm: function () {
                        this._active = !1;
                        var e = "custom" === this.selected ? this.inputValue : this.selected;
                        e = Math.max(0, Number(e)), this.$emit("submit", e)
                    }, close: function () {
                        this._active = !1, this.$emit("close")
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "2ee0": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("b-modal", {
                    attrs: {
                        "has-modal-card": "",
                        active: e._active,
                        canCancel: ["escape", "outside", "x"]
                    }, on: {
                        "update:active": function (t) {
                            e._active = t
                        }, close: function (t) {
                            return e.$emit("close")
                        }
                    }
                }, [a("div", {staticClass: "message-modal-wrapper"}, [e._t("default"), a("span", {domProps: {textContent: e._s(e.text)}}), a("span", {domProps: {innerHTML: e._s(e.html)}}), a("button", {on: {click: e.btnClicked}}, [e._v("\n            " + e._s(e._btnText) + "\n        ")])], 2)])
            }, o = [], i = a("0ecb"), r = i["a"], s = (a("cb6b"), a("330a"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "50f0c19c", null);
        t["a"] = c.exports
    }, "31a2": function (e, t, a) {
        "use strict";
        var n = function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return a("span", {staticClass: "adriel-switch-wrapper"}, [e.showText ? a("span", {
                staticClass: "switch-value-label",
                class: {"switch-on": e.value, "switch-off": !e.value}
            }, [e._v("\n        " + e._s(e.valueText) + "\n    ")]) : e._e(), a("b-switch", {
                ref: "switch",
                class: e.switchClassNames,
                attrs: {disabled: e.disabled},
                nativeOn: {
                    click: function (t) {
                        return t.preventDefault(), t.stopPropagation(), e.clicked(t)
                    }
                },
                model: {
                    value: e.value, callback: function (t) {
                        e.value = t
                    }, expression: "value"
                }
            })], 1)
        }, o = [], i = {
            props: {
                value: {type: Boolean},
                className: {type: String},
                showText: {type: Boolean, default: !0},
                disabled: {type: Boolean, default: !1},
                emphasize: {type: Boolean, default: !1}
            }, data: function () {
                return {switchClassNames: []}
            }, mounted: function () {
                this.className && this.switchClassNames.push(this.className), this.emphasize && !this.value && this.switchClassNames.push("blink_me")
            }, watch: {}, methods: {
                clicked: function (e) {
                    var t = this.switchClassNames.indexOf("blink_me");
                    t >= 0 && this.switchClassNames.splice(t, 1), this.disabled || this.$emit("update:value", !this.value)
                }
            }, computed: {
                valueText: function () {
                    return this.value ? "ON" : "OFF"
                }
            }
        }, r = i, s = (a("9ced"), a("b815"), a("2877")), c = Object(s["a"])(r, n, o, !1, null, "66a0b134", null);
        t["a"] = c.exports
    }, "330a": function (e, t, a) {
        "use strict";
        var n = a("1118"), o = a.n(n);
        o.a
    }, "342e": function (e, t, a) {
    }, "34f3": function (e, t, a) {
        "use strict";
        var n = a("f9e7"), o = a.n(n);
        o.a
    }, 3537: function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("section", [a("div", {staticClass: "my-campaigns-tab-container"}, e._l(e.validTabs, function (t) {
                    return a("button", {
                        key: t,
                        staticClass: "my-campaigns-tab",
                        class: [{active: t === e.selectedTab}, e.getTabClass(t)],
                        on: {
                            click: function () {
                                return e.onClickSelectTab(t)
                            }
                        }
                    }, [e._v(e._s(e.$t(e.getTabLang(t))))])
                }), 0), a("div", {staticClass: "my-campaigns-contents"}, [a("div", {staticClass: "box-new-express"}, [a("div", {
                    staticClass: "box-new",
                    on: {click: e.startChat}
                }, [a("i", {staticClass: "fal fa-plus"}), a("span", [e._v(e._s(e.$t("MY_CAMPAIGNS.new_campaign")))])]), a("div", {
                    staticClass: "box-express",
                    on: {click: e.goToExpress}
                }, [a("b-icon", {
                    style: {
                        "font-size": "23px",
                        width: "auto",
                        height: "auto",
                        position: "relative",
                        top: "7px"
                    }, attrs: {pack: "fas", icon: "magic"}
                }), a("span", {staticClass: "express-title"}, [e._v(e._s(e.$t("MY_CAMPAIGNS.new_campaign")))]), a("span", {staticClass: "express-content word-break"}, [e._v(e._s(e.$t("EXPRESS.by_adriel")))]), a("span", {staticClass: "express-info"}, [a("span", {staticClass: "origin"}, [e._v(e._s(e.formatCurrency(19)))]), a("span", {staticClass: "discount"}, [e._v(e._s(e.formatCurrency(9)))])])], 1)]), a("div", {
                    staticClass: "box-new-mobile",
                    on: {click: e.startChat}
                }, [a("i", {staticClass: "fal fa-plus"}), a("span", {staticClass: "box-new-mobile__text"}, [e._v(e._s(e.$t("MY_CAMPAIGNS.new_campaign")))])]), a("div", {
                    staticClass: "box-new-mobile express mt-10",
                    on: {click: e.goToExpress}
                }, [a("i", {
                    staticClass: "fal fa-magic",
                    style: {color: "white"}
                }), a("span", {staticClass: "box-new-mobile__text"}, [e._v(e._s(e.$t("LEFT_NAV.express")))])]), e._l(e.campaigns, function (t) {
                    return a("campaign-box", {
                        key: t.id,
                        attrs: {data: t},
                        on: {
                            copy: e.ensureCopy,
                            delete: e.ensureDelete,
                            active: e.ensureToggleActive,
                            "click:title": e.showTitleModal,
                            "modal:shedule": e.showScheduleModal,
                            "modal:days": e.showDaysModal,
                            "modal:budget": e.showBudgetModal
                        }
                    })
                })], 2), a("div", {
                    ref: "spinner",
                    staticClass: "my-campaigns-card-loading"
                }, [e.isLoading ? a("b-icon", {
                    staticStyle: {width: "100%", "margin-top": "20px"},
                    attrs: {pack: "fas", icon: "spinner", size: "is-large", "custom-class": "fa-spin"}
                }) : e._e()], 1)])
            }, o = [], i = a("24c4"), r = i["a"], s = (a("9ec2"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "1f0abe1b", null);
        t["a"] = c.exports
    }, 3593: function (e, t) {
        e.exports = {
            ko: {
                sign_up: "�뚯썝媛���",
                remember: "濡쒓렇�� �곹깭 �좎�",
                password: "鍮꾨�踰덊샇",
                not_signed: "�꾩쭅 �ъ슜�먭� �꾨땲�좉���?",
                login_with: "{provider}濡� 濡쒓렇��",
                login: "濡쒓렇��",
                forgot_password: "鍮꾨�踰덊샇 李얘린",
                email: "�대찓��",
                check_email_password: "�대찓�� 二쇱냼�� 鍮꾨�踰덊샇瑜� �ㅼ떆 �뺤씤�댁＜�몄슂."
            },
            jp: {
                sign_up: "�삯뙯�쇻굥",
                remember: "鼇섌뙯�쇻굥",
                password: "�묆궧��꺖��",
                not_signed: "�㏂궖�╉꺍�덀굮�귙겂�╉걚�얇걵�볝걢竊�",
                login_with: " {provider}�㎯꺆�겹궎�녈걲��",
                login: "��궛�ㅳ꺍",
                forgot_password: "�묆궧��꺖�됥굮恙섅굦��",
                email: "E-mail",
                check_email_password: "�▲꺖�ャ궋�됥꺃�밤걢�묆궧��꺖�됥굮�귙걝訝�佯�▶沃띲걮�╉걦�졼걬�꾠��"
            },
            en: {
                sign_up: "Sign up",
                remember: "Remember me",
                password: "Password",
                not_signed: "Not a user yet?",
                login_with: "Login with {provider}",
                login: "Login",
                forgot_password: "Forgot password",
                email: "E-mail",
                check_email_password: "Please check your email or password again."
            }
        }
    }, "35c3": function (e, t, a) {
    }, "35ca": function (e, t, a) {
        "use strict";
        (function (e) {
            a("96cf");
            var n = a("3b8d"), o = "/user";

            function i(e, t) {
                return r.apply(this, arguments)
            }

            function r() {
                return r = Object(n["a"])(regeneratorRuntime.mark(function e(t, a) {
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.prev = 0, e.next = 3, axios.post("".concat(o, "/linkFacebook"), {
                                    facebookPageId: t,
                                    facebookUserToken: a
                                });
                            case 3:
                                return e.sent, e.abrupt("return", !0);
                            case 7:
                                throw e.prev = 7, e.t0 = e["catch"](0), e.t0;
                            case 10:
                            case"end":
                                return e.stop()
                        }
                    }, e, null, [[0, 7]])
                })), r.apply(this, arguments)
            }

            function s() {
                return c.apply(this, arguments)
            }

            function c() {
                return c = Object(n["a"])(regeneratorRuntime.mark(function e() {
                    var t;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.get(o);
                            case 2:
                                return t = e.sent, e.abrupt("return", t.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), c.apply(this, arguments)
            }

            function l(e) {
                return d.apply(this, arguments)
            }

            function d() {
                return d = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.put(o, t);
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), d.apply(this, arguments)
            }

            function u(e) {
                return p.apply(this, arguments)
            }

            function p() {
                return p = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.put("".concat(o, "/bizNo"), {business_no: t});
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), p.apply(this, arguments)
            }

            function _() {
                return m.apply(this, arguments)
            }

            function m() {
                return m = Object(n["a"])(regeneratorRuntime.mark(function e() {
                    var t;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.get("".concat(o, "/rank"));
                            case 2:
                                return t = e.sent, e.abrupt("return", t.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), m.apply(this, arguments)
            }

            function f(e) {
                return g.apply(this, arguments)
            }

            function g() {
                return g = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.delete(o, {data: {password: t}});
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), g.apply(this, arguments)
            }

            function h() {
                return b.apply(this, arguments)
            }

            function b() {
                return b = Object(n["a"])(regeneratorRuntime.mark(function e() {
                    var t;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.get("".concat(o, "/deletable"));
                            case 2:
                                return t = e.sent, e.abrupt("return", t.data.deletable);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), b.apply(this, arguments)
            }

            function v(e) {
                return y.apply(this, arguments)
            }

            function y() {
                return y = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return a = new FormData, a.append("file", t), e.abrupt("return", axios.post("".concat(o, "/uploadBusinessCertification"), a, {headers: {"Content-Type": "multipart/form-data"}}).then(function (e) {
                                    return e.data
                                }));
                            case 3:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), y.apply(this, arguments)
            }

            function w(e) {
                return axios.delete("/accounts/".concat(e))
            }

            function k(t) {
                return axios.post("/accounts", t).then(e.prop("data"))
            }

            function x(t) {
                return axios.post("/accounts/".concat(t, "/setDefault")).then(e.prop("data"))
            }

            function O(t, a) {
                return axios.put("/accounts/".concat(t), a).then(e.prop("data"))
            }

            function A(e) {
                return C.apply(this, arguments)
            }

            function C() {
                return C = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.get("".concat(o, "/").concat(t, "/nbFeeds"));
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), C.apply(this, arguments)
            }

            t["a"] = {
                get: s,
                update: l,
                updateBizNo: u,
                linkFacebook: i,
                rank: _,
                destroy: f,
                deletable: h,
                submitBusinessCertification: v,
                deleteAccount: w,
                createAccount: k,
                setDefaultAccount: x,
                updateAccount: O,
                fetchNbFeeds: A
            }
        }).call(this, a("b17e"))
    }, "35cd": function (e, t, a) {
        "use strict";
        var n = a("d44f"), o = a.n(n);
        o.a
    }, "35ec": function (e, t, a) {
        "use strict";
        a.d(t, "s", function () {
            return O
        }), a.d(t, "f", function () {
            return A
        }), a.d(t, "k", function () {
            return C
        }), a.d(t, "c", function () {
            return S
        }), a.d(t, "h", function () {
            return E
        }), a.d(t, "i", function () {
            return T
        }), a.d(t, "r", function () {
            return P
        }), a.d(t, "q", function () {
            return I
        }), a.d(t, "d", function () {
            return L
        }), a.d(t, "m", function () {
            return j
        }), a.d(t, "n", function () {
            return M
        }), a.d(t, "o", function () {
            return R
        }), a.d(t, "a", function () {
            return N
        }), a.d(t, "j", function () {
            return D
        }), a.d(t, "e", function () {
            return $
        }), a.d(t, "b", function () {
            return B
        }), a.d(t, "g", function () {
            return F
        }), a.d(t, "l", function () {
            return G
        }), a.d(t, "p", function () {
            return U
        });
        var n = a("aede"), o = a("9c56"), i = a("bd2c"), r = a("b8cc");

        function s() {
            var e = Object(n["a"])(["\n    margin-right: 10px;\n"]);
            return s = function () {
                return e
            }, e
        }

        function c() {
            var e = Object(n["a"])(["\n    width: 100%;\n    padding: 15px 10px;\n    display: flex;\n    align-items: center;\n    cursor: pointer;\n    transition: background-color 0.2s;\n\n    &:hover {\n        background-color: #f5f5f5;\n    }\n"]);
            return c = function () {
                return e
            }, e
        }

        function l() {
            var e = Object(n["a"])(["\n    ", "\n    font-size: 13px;\n    font-weight: 600;\n    color: white;\n"]);
            return l = function () {
                return e
            }, e
        }

        function d() {
            var e = Object(n["a"])(["\n    margin-left: auto;\n    margin-right: 10px;\n    margin-top: 7px;\n"]);
            return d = function () {
                return e
            }, e
        }

        function u() {
            var e = Object(n["a"])(["\n    ", "\n    font-size: 13px;\n    font-weight: 600;\n    color: #4d5058;\n    margin-bottom: 5px;\n"]);
            return u = function () {
                return e
            }, e
        }

        function p() {
            var e = Object(n["a"])(["\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    margin-left: 10px;\n    word-break: break-word;\n    flex: 1;\n"]);
            return p = function () {
                return e
            }, e
        }

        function _() {
            var e = Object(n["a"])(["\n    padding: 15px 10px;\n    display: flex;\n    border-bottom: solid 1px #eeeeee;\n"]);
            return _ = function () {
                return e
            }, e
        }

        function m() {
            var e = Object(n["a"])(["\n    display: flex;\n    flex-direction: column;\n    max-height: 200px;\n    overflow-y: auto;\n"]);
            return m = function () {
                return e
            }, e
        }

        function f() {
            var e = Object(n["a"])(["\n    ", "\n    font-size: 12px;\n    color: white;\n    margin-top: 5px;\n"]);
            return f = function () {
                return e
            }, e
        }

        function g() {
            var e = Object(n["a"])(["\n    ", "\n    font-size: 14px;\n    font-weight: bold;\n    color: white;\n"]);
            return g = function () {
                return e
            }, e
        }

        function h() {
            var e = Object(n["a"])(["\n    width: 100%;\n    padding: 10px;\n    background-color: #2b80ba;\n    border-top-left-radius: 5px;\n    border-top-right-radius: 5px;\n    display: flex;\n    flex-direction: column;\n"]);
            return h = function () {
                return e
            }, e
        }

        function b() {
            var e = Object(n["a"])(["\n    width: 250px;\n    border-radius: 5px;\n    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);\n    background-color: white;\n    position: absolute;\n    top: 40px;\n    right: 0;\n    animation-duration: 0.2s;\n    padding-bottom: 10px;\n"]);
            return b = function () {
                return e
            }, e
        }

        function v() {
            var e = Object(n["a"])(["\n    cursor:pointer;\n    flex-shrink: 0;\n"]);
            return v = function () {
                return e
            }, e
        }

        function y() {
            var e = Object(n["a"])(["\n            top: 47px;\n            right: 80px;\n            @media screen and (max-width: 800px) {\n                right: 45px;\n                top: 21px;\n            }\n        "]);
            return y = function () {
                return e
            }, e
        }

        function w() {
            var e = Object(n["a"])(["\n    position: fixed;\n    right: 25px;\n    top: 25px;\n    @media screen and (max-width: 800px) {\n        right: 45px;\n        top: 25px;\n    }\n    ", "\n"]);
            return w = function () {
                return e
            }, e
        }

        function k() {
            var e = Object(n["a"])(["\n    position: fixed;\n    left: 0;\n    right: 0;\n    top: 0;\n    bottom: 0;\n    @media screen and (max-width: 800px) {\n        background-color: rgba(0, 0, 0, 0.65);\n    }\n"]);
            return k = function () {
                return e
            }, e
        }

        function x() {
            var e = Object(n["a"])(["\n    position: fixed;\n    z-index: 20;\n"]);
            return x = function () {
                return e
            }, e
        }

        var O = Object(o["b"])("div")(x()), A = o["b"].div(k()),
            C = Object(o["b"])("div", {routeName: String})(w(), function (e) {
                var t = e.routeName;
                return "Onboarding" === t && Object(o["a"])(y())
            }), S = r["c"].extend(v()), E = o["b"].div(b()), T = o["b"].div(h()), P = o["b"].span(g(), i["g"]),
            I = o["b"].span(f(), i["g"]), L = o["b"].div(m()), j = o["b"].div(_()), M = o["b"].div(p()),
            R = o["b"].span(u(), i["g"]), N = r["a"], D = i["e"].extend(d()), $ = o["b"].span(l(), i["g"]),
            B = o["b"].div(c()), F = r["e"], G = r["g"].extend(s()), U = r["j"]
    }, "38ed": function (e, t, a) {
        "use strict";
        var n = function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return a("confirm-modal", {
                attrs: {active: e.isVisible, text: e.$t("COMMON.logout_confirm")},
                on: {
                    "update:active": function (t) {
                        e.isVisible = t
                    }, confirm: e.confirm
                }
            })
        }, o = [], i = (a("96cf"), a("3b8d")), r = a("cebc"), s = a("2f62"), c = a("61f4"), l = a("fa7d"), d = {
            components: {"confirm-modal": c["a"]},
            computed: Object(r["a"])({}, Object(s["mapGetters"])("view", ["isLogoutModalActive"]), Object(s["mapGetters"])("user", ["user"]), {
                isVisible: {
                    get: function () {
                        return this.isLogoutModalActive
                    }, set: function (e) {
                        this.setLogoutModalActive(e)
                    }
                }
            }),
            methods: Object(r["a"])({}, Object(s["mapActions"])("user", ["logout"]), Object(s["mapMutations"])("view", ["setLogoutModalActive"]), {
                confirm: function () {
                    var e = Object(i["a"])(regeneratorRuntime.mark(function e() {
                        var t;
                        return regeneratorRuntime.wrap(function (e) {
                            while (1) switch (e.prev = e.next) {
                                case 0:
                                    return t = "ko" === Object(l["w"])(this.user) ? "ko" : "en", e.next = 3, this.logout();
                                case 3:
                                    this.$router.push("/sign/auth?lang=".concat(t));
                                case 4:
                                case"end":
                                    return e.stop()
                            }
                        }, e, this)
                    }));

                    function t() {
                        return e.apply(this, arguments)
                    }

                    return t
                }()
            })
        }, u = d, p = a("2877"), _ = Object(p["a"])(u, n, o, !1, null, null, null);
        t["a"] = _.exports
    }, "396e": function (e, t, a) {
        "use strict";
        var n = function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return e.isLoadingAdriel ? a("div", {staticClass: "loading-adriel-wrapper"}, [a("div", {staticClass: "loading-adriel-content"}, [a("div", {staticClass: "animated-logo"}), a("span", {
                staticClass: "loading-msg",
                domProps: {textContent: e._s(e._loadingMsg)}
            })])]) : e._e()
        }, o = [], i = a("cebc"), r = a("2f62"), s = {
            computed: Object(i["a"])({}, Object(r["mapGetters"])("view", ["isLoadingAdriel", "loadingMsg"]), {
                _loadingMsg: function () {
                    return this.loadingMsg || "Well done! Your part is done...\n It seems that Adriel has enough information to design the best campaign strategy for you.\nPlease wait while we come up with a nice proposal!"
                }
            })
        }, c = s, l = (a("f87c"), a("2877")), d = Object(l["a"])(c, n, o, !1, null, "2f281e04", null);
        t["a"] = d.exports
    }, "3d62": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("b-modal", {
                    attrs: {"has-modal-card": "", active: e._active, canCancel: ["escape", "outside"]},
                    on: {
                        "update:active": function (t) {
                            e._active = t
                        }, close: function (t) {
                            return e.$emit("close")
                        }
                    }
                }, [a("div", {staticClass: "extend-budget-modal-wrapper"}, [a("img", {
                    staticClass: "close-btn",
                    attrs: {src: "/img/icons/modal_close_btn.png"},
                    on: {click: e.close}
                }), a("span", {
                    staticClass: "extend-budget-text word-break",
                    domProps: {textContent: e._s(e.$t("COMMON.add_budget_text"))}
                }), a("div", {staticClass: "select-options"}, e._l(e.options, function (t) {
                    return a("span", {key: t}, [a("b-radio", {
                        attrs: {"native-value": t},
                        model: {
                            value: e.selected, callback: function (t) {
                                e.selected = t
                            }, expression: "selected"
                        }
                    }, ["custom" != t ? a("span", [e._v("\n                        " + e._s(e.$t("COMMON.extend_budget_campaign", {budget: e.formatCurrency(t)})) + "\n                    ")]) : a("span", [e.$t("COMMON.extend_budget_campaign_" + t + "_1") ? a("span", [e._v("\n                            " + e._s(e.$t("COMMON.extend_budget_campaign_" + t + "_1", {symbol: e.getCurrencySymbol()})) + "\n                        ")]) : e._e(), a("adriel-input", {
                        ref: "input",
                        refInFor: !0,
                        attrs: {placeholder: "0", type: "number"},
                        model: {
                            value: e.inputValue, callback: function (t) {
                                e.inputValue = t
                            }, expression: "inputValue"
                        }
                    }), a("span", [e._v("\n                            " + e._s(e.$t("COMMON.extend_budget_campaign_" + t + "_2")) + "\n                        ")])], 1)])], 1)
                }), 0), a("button", {on: {click: e.confirm}}, [e._v("\n            " + e._s(e._btnText) + "\n        ")])])])
            }, o = [], i = a("2d65"), r = i["a"], s = (a("16d0"), a("6eab"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "6b65c7de", null);
        t["a"] = c.exports
    }, "41cb": function (e, t, a) {
        "use strict";
        (function (e) {
            a("6762"), a("2fdb"), a("7f7f");
            var n = a("cebc"), o = a("2b0e"), i = a("8c4f"), r = a("4360"), s = a("abd2"), c = a("9bc9"), l = a("d3fb"),
                d = a("d337"), u = a("fa7d"), p = a("4c6b"), _ = {floatingAccountVisible: !1},
                m = {canChangeAccount: !1};
            o["default"].use(i["a"]);
            var f = new i["a"]({
                routes: [{path: "/", redirect: "MyCampaigns"}, {
                    path: "/form",
                    name: "Form",
                    component: c["a"]
                }, {
                    path: "/onboarding/:id?", name: "Onboarding", meta: Object(n["a"])({}, _), component: function () {
                        return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("dashboard~onboarding~proposal"), a.e("accountSetting~onboarding~proposal"), a.e("onboarding~proposal"), a.e("onboarding")]).then(a.bind(null, "c392"))
                    }
                }, {
                    path: "/sign", component: function () {
                        return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("chunk-b1925690"), a.e("sign")]).then(a.bind(null, "1c4f"))
                    }, meta: _, children: [{
                        path: "join", meta: _, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("chunk-b1925690"), a.e("sign")]).then(a.bind(null, "c94f"))
                        }, name: "Sign/Join"
                    }, {
                        path: "login", meta: _, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("chunk-b1925690"), a.e("sign")]).then(a.bind(null, "9cd4"))
                        }, name: "Sign/Login"
                    }, {
                        path: "forgot", meta: _, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("chunk-b1925690"), a.e("sign")]).then(a.bind(null, "cc85"))
                        }, name: "Sign/Forgot"
                    }, {
                        path: "reset", meta: _, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("chunk-b1925690"), a.e("sign")]).then(a.bind(null, "1926"))
                        }, name: "Sign/Reset"
                    }, {
                        path: "verify", meta: _, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("chunk-b1925690"), a.e("sign")]).then(a.bind(null, "996d"))
                        }, name: "Sign/Verify"
                    }, {path: "*", redirect: "Sign/Join"}, {path: "", redirect: "Sign/Join"}]
                }, {
                    path: "/proposal/:id", component: function () {
                        return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("dashboard~onboarding~proposal"), a.e("accountSetting~onboarding~proposal"), a.e("onboarding~proposal"), a.e("proposal")]).then(a.bind(null, "2f56"))
                    }, children: [{
                        path: "form", component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("dashboard~onboarding~proposal"), a.e("accountSetting~onboarding~proposal"), a.e("onboarding~proposal"), a.e("proposal")]).then(a.bind(null, "8bd7"))
                        }, name: "Proposal/Form"
                    }, {
                        path: "ads", meta: m, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("dashboard~onboarding~proposal"), a.e("accountSetting~onboarding~proposal"), a.e("onboarding~proposal"), a.e("proposal")]).then(a.bind(null, "a79b"))
                        }, name: "Proposal/Ads"
                    }, {
                        path: "plan", meta: m, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("dashboard~onboarding~proposal"), a.e("accountSetting~onboarding~proposal"), a.e("onboarding~proposal"), a.e("proposal")]).then(a.bind(null, "a1d5"))
                        }, name: "Proposal/Plan"
                    }, {
                        path: "audienceProfile", meta: m, component: function () {
                            return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("dashboard~onboarding~proposal"), a.e("accountSetting~onboarding~proposal"), a.e("onboarding~proposal"), a.e("proposal")]).then(a.bind(null, "6e9e"))
                        }, name: "Proposal/AudienceProfile"
                    }, {path: "*", redirect: "ads"}, {path: "", redirect: "ads"}]
                }, {path: "/launched", component: s["a"], name: "CampaignLaunched"}, {
                    path: "/inReview",
                    component: s["f"],
                    name: "InReview"
                }, {
                    path: "/myCampaigns",
                    name: "MyCampaigns",
                    component: s["g"],
                    meta: {isRefresh: !1},
                    beforeEnter: function (e, t, a) {
                        e.meta.isRefresh = null === t.name, a()
                    }
                }, {
                    path: "/accountSetting", name: "AccountSetting", component: function () {
                        return Promise.all([a.e("accountSetting~onboarding~proposal"), a.e("accountSetting")]).then(a.bind(null, "99c1"))
                    }
                }, {path: "/billingHistory", name: "BillingHistory", component: s["b"]}, {
                    path: "/helpCampaign",
                    name: "HelpCampaign",
                    component: s["e"]
                }, {
                    path: "/dashboard/:id", name: "Dashboard", component: function () {
                        return Promise.all([a.e("dashboard~onboarding~proposal~sign"), a.e("dashboard~onboarding~proposal"), a.e("dashboard")]).then(a.bind(null, "7277"))
                    }
                }, {
                    path: "/manager/:id?", name: "Manager", component: function () {
                        return a.e("manager").then(a.bind(null, "274e"))
                    }
                }, {
                    path: "/express", name: "Express", component: function () {
                        return a.e("express").then(a.bind(null, "6432"))
                    }
                }, {path: "/404", name: "NotFound", component: s["h"]}, {
                    path: "/empty",
                    component: s["d"]
                }, {path: "/demoRedirection", component: s["c"]}, {path: "*", redirect: "/404"}]
            }), g = "user/user";
            f.beforeEach(function (e, t, a) {
                var n = r["a"].getters, o = n[g];
                if (o || !0 === n["user/authFailed"]) return o && !Object(u["N"])(n["user/accounts"]) && r["a"].dispatch("user/fetchAccountList"), a();
                Object(l["a"])(r["a"].dispatch("user/checkSession")).pipe(Object(d["a"])(5e3)).subscribe(function () {
                    ["Sign/Login", "Sign/Join"].includes(e.name) && n[g] ? a({name: "MyCampaigns"}) : a()
                }, function (e) {
                    console.log(e), a()
                })
            });
            var h = e.contains(e.__, ["Proposal", "Proposal/Ads", "Proposal/Boost", "Proposal/Plan", "Proposal/AudienceProfile", "Proposal/AudienceJourney", "Dashboard", "MyCampaigns", "AccountSetting", "Express", "Onboarding"]);
            f.beforeEach(function () {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = e.name,
                    a = (arguments.length > 1 && arguments[1], arguments.length > 2 ? arguments[2] : void 0);
                !h(t) || r["a"].getters[g] ? a() : a("/sign/auth?dest=".concat(t))
            }), f.beforeEach(function () {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                    t = (e.name, arguments.length > 1 && arguments[1], arguments.length > 2 ? arguments[2] : void 0);
                r["a"].getters["user/userId"];
                t()
            });
            var b = function () {
                var e = r["a"].getters["user/user"]["email"], t = r["a"].getters["user/user"]["backoffice_admin"];
                return Object(p["r"])(e.toLowerCase()) || t
            };
            f.beforeEach(function (e, t, a) {
                if ("Manager" === e.name && !b()) return a(t.fullPath);
                a()
            }), f.beforeEach(function (t, a, n) {
                var o = "accountId", i = e.path(["query", o]), r = i(t), s = i(a) || 0;
                void 0 === r ? (axios.defaults.headers.common.AccountId = s, n({
                    path: t.path,
                    query: e.assoc(o, s, t.query)
                })) : n()
            }), t["a"] = f
        }).call(this, a("b17e"))
    }, 4206: function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            t["a"] = {
                mounted: function () {
                    if (!0 === this.autofocus) {
                        var e = this.$refs.elem;
                        e && e.focus()
                    }
                },
                props: {
                    placeholder: {type: String, default: ""},
                    type: {type: String, default: "text"},
                    value: {type: [String, Number], default: ""},
                    autocomplete: {type: String, default: ""},
                    autofocus: {type: Boolean, default: !1},
                    name: {type: String}
                },
                computed: {
                    _value: {
                        get: e.prop("value"), set: function (e) {
                            this.$emit("input", e)
                        }
                    }
                }
            }
        }).call(this, a("b17e"))
    }, 4355: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), o = a.n(n), i = a("48a6"), r = a("fa7d"), s = a("a772"), c = a("4c6b"), l = a("6e77"),
                d = a("5670");
            t["a"] = {
                name: "CampaignGoalModal", mounted: function () {
                    var e = this;
                    this.$nextTick(function () {
                        e.watchEnter$()
                    })
                }, props: {active: {type: Boolean, default: !1}}, data: function () {
                    return {
                        dirty: !1,
                        value: {appStoreUrl: "", developerSiteUrl: "", playStoreUrl: "", type: "web", websiteUrl: ""}
                    }
                }, render: function () {
                    var e = this, t = arguments[0];
                    if (this.active) {
                        var a = {
                            attrs: {active: !0, canCancel: ["escape"], "max-width": "auto", width: "800px"},
                            on: {
                                "update:active": function (t) {
                                    return e.updateActive(t)
                                }
                            },
                            style: {}
                        };
                        return t(i["g"], o()([{}, a]), [t(i["i"], [t(i["d"], {
                            attrs: {src: "/img/icons/modal_close_btn.png"},
                            nativeOn: {
                                click: function () {
                                    return e.updateActive(!1)
                                }
                            }
                        }), t(i["a"], [t(i["e"], [t(i["f"], [this.$t("MY_CAMPAIGNS.campaign_goal_text")])]), t(i["h"], {
                            attrs: {
                                value: this.value,
                                showCheckIcon: !0,
                                dirty: this.dirty
                            }, on: {
                                input: function (t) {
                                    return e.value = t
                                }
                            }
                        })]), t(i["c"], [t(i["b"], {
                            nativeOn: {
                                click: function () {
                                    return e.updateActive(!1)
                                }
                            }
                        }, [this.$t("COMMON.cancel")]), t(i["b"], {
                            attrs: {isConfirm: !0},
                            nativeOn: {click: this.handleConfirm}
                        }, [this.$t("COMMON.create")])])])])
                    }
                }, methods: {
                    updateActive: function (e) {
                        this.$emit("update:active", e)
                    }, handleConfirm: function () {
                        Object(s["x"])(this.value) ? (this.$emit("confirm", this.value), this.updateActive(!1)) : this.dirty = !0
                    }, watchEnter$: function () {
                        var t = Object(l["a"])(document, "keyup").pipe(Object(d["a"])(e.both(e.propEq("keyCode", c["q"].enter), Object(r["bb"])("input"))));
                        this.$subscribeTo(t, this.handleConfirm)
                    }
                }
            }
        }).call(this, a("b17e"))
    }, 4360: function (e, t, a) {
        "use strict";
        var n, o = a("2b0e"), i = a("2f62"), r = (a("96cf"), a("3b8d")), s = "SAVE_CHAT_ANSWERS", c = "SAVE_PROPOSAL",
            l = {
                launchCampaign: function () {
                    var e = Object(r["a"])(regeneratorRuntime.mark(function e(t, a) {
                        var n, o;
                        return regeneratorRuntime.wrap(function (e) {
                            while (1) switch (e.prev = e.next) {
                                case 0:
                                    return n = t.commit, e.prev = 1, e.next = 4, axios.post("/campaigns", {
                                        updated_by: "user",
                                        chat_answers: a
                                    });
                                case 4:
                                    o = e.sent, n(s, a), n(c, o.data.proposal), e.next = 12;
                                    break;
                                case 9:
                                    e.prev = 9, e.t0 = e["catch"](1), console.error(e.t0);
                                case 12:
                                case"end":
                                    return e.stop()
                            }
                        }, e, null, [[1, 9]])
                    }));

                    function t(t, a) {
                        return e.apply(this, arguments)
                    }

                    return t
                }()
            }, d = a("bd86"), u = (n = {}, Object(d["a"])(n, s, function (e, t) {
                e.answers = t
            }), Object(d["a"])(n, c, function (e, t) {
                e.proposal = t
            }), n), p = a("d296"), _ = a("2147"), m = a("0a52"), f = a("bf53"), g = a("79c3"), h = a("5f78"), b = a("4ec4"),
            v = a("c50d");
        o["default"].use(i["default"]);
        var y = {count: 0, isLogoutModalActive: !1}, w = new i["default"].Store({
            strict: !1,
            state: y,
            actions: l,
            mutations: u,
            modules: {
                proposal: p["a"],
                user: _["a"],
                campaign: m["a"],
                view: f["a"],
                dashboard: g["a"],
                form: h["a"],
                manager: b["a"],
                file: v["a"]
            },
            plugins: []
        });
        t["a"] = w
    }, 4381: function (e, t) {
        e.exports = {
            en: {
                company_address: "167 Fleet Street, London EC4A 2EA",
                company_info: "",
                company_name: "Adriel AI Ltd.",
                company_tel: ""
            },
            jp: {
                company_address: "2035 Sunset Lake Road, Suite B-2, Newark, Delaware 19702, USA",
                company_info: "",
                company_name: "Adriel Inc.",
                company_tel: ""
            },
            ko: {
                company_address: "�쒖슱�밸퀎�� 以묎뎄 �꾩�濡� 66 13痢�",
                company_info: "Biz No. 838-87-00916 | 2018-�쒖슱媛뺣궓-03748 | CEO �꾩닔��",
                company_name: "Adriel Inc. (二�)�꾨뱶由ъ뿕",
                company_tel: "T. +82-1522-7359 | E."
            }
        }
    }, "43de": function (e, t, a) {
        "use strict";
        var n = a("d53c"), o = a.n(n);
        o.a
    }, 4537: function (e, t) {
        e.exports = {
            en: {
                btn_done: "Done",
                detail: "Enter your email address to reset your password.\n Please check your spam folder if you cannot find the email.",
                email: "E-mail",
                sent_content: "An email has been sent to your email address, {email}. Follow the instructions in the email to reset your password.",
                sent_title: "Password Reset email sent",
                title: "Forgot your password?"
            },
            jp: {
                btn_done: "若뚥틙",
                detail: "�묆궧��꺖�됥굮�ゃ궩�껁깉�쇻굥�잆굙�ャ깳�쇈꺂�㏂깋�с궧�믦쮼�γ걮�╉걦�졼걬�꾠��\n �㏂깋�ゃ궓�ャ걢�됥깳�쇈꺂�뚦콎�뗣겒�꾢졃�덀겘�밤깙�졼깢�⒲꺂���믥▶沃띲걮�╉걦�졼걬�꾠��",
                email: "E-mail",
                sent_content: "鼇�츣�쀣걼�▲꺖�ャ궋�됥꺃�밧츥�ャ깳�쇈꺂�뚪�곥굢�뚣겲�쀣걼��, {email}. �▲꺖�ャ겗�뉒ㅊ�ュ풏�ｃ겍�묆궧��꺖�됥굮�ゃ궩�껁깉�쀣겍�뤵걽�뺛걚��",
                sent_title: "�묆궧��꺖�됥꺁�삠긿�덄뵪�▲꺖�ャ걣�곥굢�뚣겲�쀣걼��",
                title: "�묆궧��꺖�됥굮�듿퓲�뚣겎�쇻걢?"
            },
            ko: {
                btn_done: "�뺤씤",
                detail: "鍮꾨�踰덊샇 �ъ꽕�뺤쓣 �꾪빐 �대찓�� 二쇱냼瑜� �낅젰�댁＜�몄슂. \n�대찓�쇱씠 �꾩넚�섏� �딅뒗�ㅻ㈃ �ㅽ뙵 �대뜑瑜� �뺤씤�섏꽭��.",
                email: "�대찓��",
                sent_content: "鍮꾨�踰덊샇 蹂�寃쎌쓣 �꾪븳 �대찓�쇱씠 諛쒖넚�섏뿀�듬땲��. {email}濡� �대찓�쇱씠 諛쒖넚�섏뿀�듬땲��. �뺤씤 �� �대찓�쇱뿉 �ㅻ챸�� �댁슜�� �곕씪 鍮꾨�踰덊샇瑜� 蹂�寃쏀빐二쇱꽭��.",
                sent_title: "鍮꾨�踰덊샇 蹂�寃쎌쓣 �꾪븳 �대찓�쇱씠 諛쒖넚�섏뿀�듬땲��.",
                title: "鍮꾨�踰덊샇媛� 湲곗뼲�섏� �딆쑝�몄슂?"
            }
        }
    }, 4657: function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var t = a("2b0e"), n = a("e37d"), o = a("fa7d"), i = a("6e77"), r = a("a744"), s = a("ebb6"), c = a("5670");
            t["default"].directive("img-prepend", {
                bind: function (e, t, a) {
                    e.src = Object(o["U"])(t.value)
                }
            }), t["default"].directive("tooltip", n["a"].VTooltip), t["default"].directive("maxchars", function () {
                var t = {}, a = e.compose(e.curryN(2), e.flip)(o["eb"]), n = e.flip(e.gt);
                return {
                    bind: function (l, d, u) {
                        var p = d.value || Number.POSITIVE_INFINITY, _ = Object(o["o"])(),
                            m = Object(i["a"])(l, "input").pipe(Object(r["a"])("init"), Object(s["a"])(function () {
                                return l.value
                            }), Object(c["a"])(e.compose(n(p), o["e"])), Object(s["a"])(a(p))).subscribe(function (e) {
                                l.value = e, u.elm.dispatchEvent(new CustomEvent("input"))
                            });
                        t = e.assoc(_, m, t), l.dataset.handlerId = _
                    }, unbind: function (a) {
                        var n = a.dataset.handlerId;
                        Object(o["Z"])("unsubscribe", e.propOr({}, n, t)), t = e.dissoc(n, t), delete a.dataset.handlerId
                    }
                }
            }());
            var l = function (e, t) {
                var a = e.classList, n = t.value;
                n ? a.add("opacity-mask") : a.remove("opacity-mask")
            };
            t["default"].directive("opacity-mask", {bind: l, update: l})
        }).call(this, a("b17e"))
    }, "473f": function (e, t, a) {
        "use strict";
        (function (e) {
            var a = "/accounts";

            function n() {
                return axios.get("".concat(a)).then(e.prop("data"))
            }

            function o() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {name: "account"};
                return axios.post("".concat(a), t).then(e.prop("data"))
            }

            function i(t) {
                var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {name: "account"};
                return axios.post("".concat(a, "/").concat(t), n).then(e.prop("data"))
            }

            function r(e) {
                return axios.delete("".concat(a, "/").concat(e))
            }

            t["a"] = {list: n, create: o, update: i, destroy: r}
        }).call(this, a("b17e"))
    }, "48a6": function (e, t, a) {
        "use strict";
        a.d(t, "i", function () {
            return b
        }), a.d(t, "h", function () {
            return v
        }), a.d(t, "a", function () {
            return y
        }), a.d(t, "e", function () {
            return w
        }), a.d(t, "f", function () {
            return k
        }), a.d(t, "c", function () {
            return x
        }), a.d(t, "b", function () {
            return O
        }), a.d(t, "d", function () {
            return A
        }), a.d(t, "g", function () {
            return C
        });
        var n = a("aede"), o = a("2b0e"), i = a("9c56"), r = a("97a6");

        function s() {
            var e = Object(n["a"])(["\n    @media screen and (max-width: 800px) {\n        .modal-content {\n            max-height: initial;\n            height: 100vh;\n        }\n    }\n"]);
            return s = function () {
                return e
            }, e
        }

        function c() {
            var e = Object(n["a"])(["\n    position: absolute;\n    right: 15px;\n    top: 15px;\n    cursor: pointer;\n"]);
            return c = function () {
                return e
            }, e
        }

        function l() {
            var e = Object(n["a"])(["\n    ", "\n    height: 50px;\n    border-radius: 3px;\n    font-size: 16px;\n    font-weight: bold;\n    text-align: center;\n    width: 50%;\n    color: #fff7f7;\n    margin: 0 7px;\n    background-color: ", ";\n"]);
            return l = function () {
                return e
            }, e
        }

        function d() {
            var e = Object(n["a"])(["\n    display: flex;\n    justify-content: space-between;\n    margin: 0 -7px;\n    margin-top: 55px;\n"]);
            return d = function () {
                return e
            }, e
        }

        function u() {
            var e = Object(n["a"])(["\n    ", "\n    font-size: 20px;\n    text-align: center;\n    color: #333333;\n    white-space: pre;\n"]);
            return u = function () {
                return e
            }, e
        }

        function p() {
            var e = Object(n["a"])(["\n    display: flex;\n    align-items: center;\n    justify-content: center;\n"]);
            return p = function () {
                return e
            }, e
        }

        function _() {
            var e = Object(n["a"])([""]);
            return _ = function () {
                return e
            }, e
        }

        function m() {
            var e = Object(n["a"])(["\n    margin-top: 30px;\n    .onboarding-app-url--boxes {\n        .onboarding-basic-box {\n            width: 31%;\n            height: 150px;\n            margin: 0;\n        }\n    }\n"]);
            return m = function () {
                return e
            }, e
        }

        function f() {
            var e = Object(n["a"])(["\n    ", "\n    width: 800px;\n    margin: 0 auto;\n    border-radius: 5px;\n    background-color: white;\n    padding: 50px 75px 30px 75px;\n    @media screen and (max-width: 800px) {\n        width: 100vw;\n        height: 100%;\n        padding: 50px 15px 15px 15px;\n        overflow-y: auto;\n    }\n"]);
            return f = function () {
                return e
            }, e
        }

        function g() {
            var e = Object(n["a"])(["\n    font-family: 'Rubik', 'sans-serif', 'Noto Sans';\n"]);
            return g = function () {
                return e
            }, e
        }

        var h = Object(i["a"])(g()), b = i["b"].div(f(), h),
            v = Object(i["b"])(o["default"].component("campaign-goal-web-or-app", r["a"]))(m()), y = i["b"].div(_()),
            w = i["b"].div(p()), k = i["b"].div(u(), h), x = i["b"].div(d()),
            O = Object(i["b"])("button", {isConfirm: Boolean})(l(), h, function (e) {
                var t = e.isConfirm;
                return !0 === t ? "#5aabe3" : "#999999"
            }), A = i["b"].img(c()), C = Object(i["b"])(o["default"].options.components.BModal)(s())
    }, "4a03": function (e, t, a) {
        "use strict";
        var n = a("6953"), o = a.n(n);
        o.a
    }, "4a1d": function (e, t, a) {
        "use strict";
        (function (e) {
            a("96cf");
            var n = a("795b"), o = a.n(n), i = (a("ac6a"), a("3b8d")), r = a("e380"), s = a.n(r), c = a("c1df"),
                l = a.n(c), d = a("4328"), u = a.n(d), p = a("fa7d"), _ = a("a772"), m = e.prop("data"),
                f = "/campaigns";

            function g(e) {
                return h.apply(this, arguments)
            }

            function h() {
                return h = Object(i["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.get("".concat(f), {
                                    params: t, paramsSerializer: function (e) {
                                        return u.a.stringify(e, {arrayforamt: "brackets"})
                                    }
                                });
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), h.apply(this, arguments)
            }

            function b(e) {
                return e.proposal ? e.proposal = v(e) : e.onboarding, e
            }

            function v(t) {
                var a = t.proposal, n = e.either(e.isNil, isNaN);
                a.schedule || (a.schedule = {});
                var o = a.budget.daily;
                n(o) && (a.budget.daily = 0), (a.budget.channels || []).forEach(function (e) {
                    n(e.value) && (e.value = 0)
                }), a.channelAds = e.compose(_["a"], e.map(_["i"]), e.propOr([], "channelAds"))(a), a.meta = a.meta || {webOrApp: {type: Object(_["d"])(a.channelAds) ? "app" : "web"}};
                var i = a.schedule;
                if ("in_progress" === t.status) {
                    var r = l()(), s = r.weekday(), c = 1;
                    s > 4 && (c = 2);
                    var d = r.add(c, "day");
                    i.startDate && !l()(i.startDate).isBefore(d) || (i.startDate = d.add(1, "hour").toDate());
                    var u = l()(i.startDate).add(2, "day");
                    i.endDate && !l()(i.endDate).isBefore(u) || (i.endDate = u.add(1, "hour").toDate())
                }
                return void 0 === i.asap && (i.asap = !0), a
            }

            function y(e) {
                return axios.get("".concat(f, "/").concat(e)).then(m).then(b).catch(function (e) {
                    console.log(e)
                })
            }

            function w(e) {
                return k.apply(this, arguments)
            }

            function k() {
                return k = Object(i["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.post("".concat(f, "/").concat(t, "/copy"));
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), k.apply(this, arguments)
            }

            function x(e) {
                return O.apply(this, arguments)
            }

            function O() {
                return O = Object(i["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.post(f, {chat_answers: t});
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), O.apply(this, arguments)
            }

            function A() {
                return C.apply(this, arguments)
            }

            function C() {
                return C = Object(i["a"])(regeneratorRuntime.mark(function e() {
                    var t, a, n, o, i = arguments;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return t = i.length > 0 && void 0 !== i[0] ? i[0] : {}, a = t.webOrApp, n = void 0 === a ? {} : a, e.next = 3, axios.post("".concat(f, "/empty"), {data: {webOrApp: n}});
                            case 3:
                                return o = e.sent, e.abrupt("return", o.data);
                            case 5:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), C.apply(this, arguments)
            }

            function S(t, a, n) {
                var o = e.join("/", [f, t]);
                return void 0 !== n && (o += "?".concat(n)), axios.put(o, e.omit(["id", "imageURL"], a)).then(m).then(v).then(function (e) {
                    var t = e.schedule;
                    return t && (t.startDate && (t.startDate = Object(p["cb"])(t.startDate)), t.endDate && (t.endDate = Object(p["cb"])(t.endDate))), e
                })
            }

            function E(e) {
                return axios.post("".concat(f, "/").concat(e, "/deactivate")).then(m)
            }

            function T(e, t) {
                return P.apply(this, arguments)
            }

            function P() {
                return P = Object(i["a"])(regeneratorRuntime.mark(function e(t, a) {
                    var n;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.post("".concat(f, "/").concat(t, "/estimate"), {proposal: a});
                            case 2:
                                return n = e.sent, e.abrupt("return", n.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), P.apply(this, arguments)
            }

            function I(e, t, a) {
                return L.apply(this, arguments)
            }

            function L() {
                return L = Object(i["a"])(regeneratorRuntime.mark(function e(t, a, n) {
                    var o;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.post("".concat(f, "/").concat(t, "/creatives/generate"), {
                                    channel: a,
                                    creative: n
                                });
                            case 2:
                                return o = e.sent, e.abrupt("return", o.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), L.apply(this, arguments)
            }

            function j(e) {
                return axios.get("".concat(f, "/").concat(e, "/reports")).then(m)
            }

            function M(e) {
                return axios.get("".concat(f, "/").concat(e, "/facebookPage")).then(m).then(function (e) {
                    return e || void 0
                })
            }

            var R = s()(function (e, t, a) {
                if (t < 0) return new o.a(function (e) {
                    return e()
                });
                var n = "".concat(f, "/").concat(e, "/previewFacebook/").concat(t);
                return void 0 !== a && (n += "?position=".concat(a)), axios.get(n).then(m)
            }, function () {
                for (var e = arguments.length, t = new Array(e), a = 0; a < e; a++) t[a] = arguments[a];
                return t.join("-")
            });

            function N(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return axios.post("".concat(f, "/").concat(e, "/execute"), t)
            }

            function D(e, t, a, n) {
                return axios.put("".concat(f, "/").concat(e, "/updateAccounts"), {
                    facebookPageId: t,
                    facebookUserToken: a,
                    instagram: n
                }).then(function (e) {
                    return console.log(e)
                }).catch(function (e) {
                    return console.log(e)
                })
            }

            function $(e, t) {
                return axios.put("".concat(f, "/").concat(e, "/updateAccounts"), {googleAnalytics: t}).then(function (e) {
                    return console.log(e)
                }).catch(function (e) {
                    return console.log(e)
                })
            }

            function B(e, t, a) {
                return axios.put("".concat(f, "/").concat(e, "/updateAccounts"), {
                    facebookPixel: t,
                    facebookUserToken: a
                })
            }

            function F(e, t) {
                return axios.put("".concat(f, "/").concat(e, "/updateAccounts"), {fbApp: t})
            }

            function G(e, t) {
                return axios.put("".concat(f, "/").concat(e, "/title"), {title: t}).then(m)
            }

            function U(e) {
                return axios.delete("".concat(f), {data: {ids: e}}).then(m)
            }

            function q(e) {
                return axios.post("".concat(f, "/").concat(e, "/debounceExecute"))
            }

            function Y(e, t) {
                return axios.post("".concat(f, "/").concat(e, "/coupons"), {couponIds: t})
            }

            function V(t, a) {
                return axios.get("".concat(f, "/").concat(t, "/coupons")).then(e.prop("data"))
            }

            function z(t) {
                return axios.get("".concat(f, "/").concat(t, "/availableCouponAmount")).then(e.prop("data"))
            }

            function W(t) {
                var a = t.campaignId, n = t.type, o = void 0 === n ? "ga" : n;
                return axios.get("".concat(f, "/").concat(a, "/conversions"), {params: {type: o}}).then(e.prop("data"))
            }

            function H(e, t) {
                return axios.post("".concat(f, "/").concat(e, "/conversions/select"), {conversion: t})
            }

            function K() {
                return J.apply(this, arguments)
            }

            function J() {
                return J = Object(i["a"])(regeneratorRuntime.mark(function t() {
                    var a;
                    return regeneratorRuntime.wrap(function (t) {
                        while (1) switch (t.prev = t.next) {
                            case 0:
                                return t.next = 2, axios.get("".concat(f, "/nbLaunchedCampaigns"));
                            case 2:
                                return a = t.sent, t.abrupt("return", e.path(["data", "nb"], a));
                            case 4:
                            case"end":
                                return t.stop()
                        }
                    }, t)
                })), J.apply(this, arguments)
            }

            function Q(t, a) {
                return Object(p["N"])(a) ? axios.put("".concat(f, "/").concat(t, "/extend"), e.pick(["budget", "days"], a)).then(e.prop("data")) : o.a.resolve()
            }

            function X(e) {
                return axios.get("".concat(f, "/").concat(e, "/reports/graph")).then(m)
            }

            function Z(e) {
                return axios.get("".concat(f, "/").concat(e, "/reports/excel")).then(m)
            }

            function ee() {
                return axios.get("".concat(f, "/excel"), {responseType: "blob"}).then(m)
            }

            function te() {
                return axios.get("".concat(f, "/onboarding")).then(m)
            }

            function ae(e, t) {
                return axios.post("".concat(f, "/").concat(e, "/onboarding"), {onboarding: t}).then(m)
            }

            function ne(e) {
                return axios.post("".concat(f, "/").concat(e, "/startOnBoardingCampaign")).then(m)
            }

            function oe(e, t) {
                return axios.put("".concat(f, "/").concat(e, "/updateAccounts"), {appleAppId: t})
            }

            t["a"] = {
                query: g,
                copy: w,
                create: x,
                createEmpty: A,
                update: S,
                deactivate: E,
                get: y,
                getEstimations: T,
                getReport: j,
                getFacebookPreview: R,
                getFacebookProfile: M,
                execute: N,
                updateFacebook: D,
                updateTitle: G,
                deleteCampaign: U,
                updateGa: $,
                generateCreativeImages: I,
                debounceExecute: q,
                coupons: V,
                bindCoupons: Y,
                availableCouponAmount: z,
                updatePixel: B,
                listConversions: W,
                addConversion: H,
                nbLaunchedCampaigns: K,
                extend: Q,
                getReportGraph: X,
                downloadExcel: Z,
                getOnBoardingCampaign: te,
                updateOnBoarding: ae,
                startOnBoardingCampaign: ne,
                updateFbApp: F,
                downloadCampaignsExcel: ee,
                checkAppstoreConnected: oe
            }
        }).call(this, a("b17e"))
    }, "4b4c": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("b-modal", {
                    attrs: {"has-modal-card": "", active: e._active, canCancel: ["escape", "outside"]},
                    on: {
                        "update:active": function (t) {
                            e._active = t
                        }, close: function (t) {
                            return e.$emit("close")
                        }
                    }
                }, [a("div", {staticClass: "extend-modal-wrapper"}, [a("img", {
                    staticClass: "close-btn",
                    attrs: {src: "/img/icons/modal_close_btn.png"},
                    on: {click: e.close}
                }), a("running-status", e._b({}, "running-status", e.$props, !1)), a("div", {staticClass: "select-options"}, e._l(e.options, function (t) {
                    return a("span", {key: t}, [a("b-radio", {
                        attrs: {"native-value": t},
                        model: {
                            value: e.selected, callback: function (t) {
                                e.selected = t
                            }, expression: "selected"
                        }
                    }, ["custom" != t ? a("span", [e._v(e._s(e.$t("COMMON.extend_campaign", {days: t})))]) : a("span", [e.$t("COMMON.extend_campaign_" + t + "_1") ? a("span", [e._v(e._s(e.$t("COMMON.extend_campaign_" + t + "_1")))]) : e._e(), a("adriel-input", {
                        ref: "input",
                        refInFor: !0,
                        attrs: {placeholder: "0", type: "number"},
                        model: {
                            value: e.inputValue, callback: function (t) {
                                e.inputValue = t
                            }, expression: "inputValue"
                        }
                    }), a("span", [e._v(e._s(e.$t("COMMON.extend_campaign_" + t + "_2")))])], 1)])], 1)
                }), 0), a("button", {on: {click: e.confirm}}, [e._v(e._s(e._btnText))])], 1)])
            }, o = [], i = a("b1db"), r = i["a"], s = (a("35cd"), a("5d4b"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "5e0a46c8", null);
        t["a"] = c.exports
    }, "4bdc": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("fa7d"), o = a("da7a"), i = (a("10c7"), a("56d7"));
            t["a"] = {
                name: "ImageOrVideo",
                data: function () {
                    return {success: !0}
                },
                components: {ClipLoader: o["a"]},
                created: function () {
                    var e = this;
                    this.$nextTick(function () {
                        e.watchResize && e.watchSize()
                    })
                },
                methods: {
                    handleLoad: function (e) {
                        var t = e.target;
                        this.success = !0, i["EventBus"].$emit("mediaLoaded"), this.$emit("load:success", t)
                    }, handleError: function (e) {
                        this.success = !1, this.$emit("load:fail", e)
                    }, watchSize: function () {
                        var e = this, t = function () {
                            i["EventBus"].$emit("mediaLoaded"), e.$emit("mediaLoaded")
                        };
                        this.elem && addResizeListener(this.elem, t), this.getBeforeDestroy$().subscribe(function () {
                            e.elem && removeResizeListener(e.elem, t)
                        })
                    }, renderValidation: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                        return e.is(String, t) ? this.$t(t) : this.$t(t.key, t.value)
                    }
                },
                watch: {
                    src: function () {
                        this.success = !0
                    }
                },
                props: {
                    src: {type: String, default: ""},
                    fallback: {type: String, default: "/img/proposal/no_image_dark_icon.png"},
                    fallbackTooltip: {type: String},
                    isLoading: {type: Boolean, default: !1},
                    controls: {type: Boolean, default: !0},
                    validations: {type: Array, default: e.always([])},
                    meta: {type: Object, default: e.always({})},
                    placement: {type: String, default: "right-end"},
                    error: {type: Boolean, default: !0},
                    watchResize: {type: Boolean, default: !0}
                },
                computed: {
                    successUrl: function () {
                        return this.success && this._src
                    },
                    type: function () {
                        return Object(n["O"])(this.src) ? "video" : "image"
                    },
                    _src: e.pipe(e.prop("src"), n["U"]),
                    elem: e.path(["$refs", "elem"]),
                    previewNotAvailable: function () {
                        return "video" === this.type && !e.pathEq(["meta", "codec_name"], "h264", this)
                    },
                    notValid: e.path(["validations", "length"])
                }
            }
        }).call(this, a("b17e"))
    }, "4c6b": function (e, t, a) {
        "use strict";
        (function (e) {
            a.d(t, "e", function () {
                return i
            }), a.d(t, "b", function () {
                return r
            }), a.d(t, "a", function () {
                return s
            }), a.d(t, "m", function () {
                return c
            }), a.d(t, "g", function () {
                return u
            }), a.d(t, "p", function () {
                return p
            }), a.d(t, "s", function () {
                return l
            }), a.d(t, "t", function () {
                return d
            }), a.d(t, "d", function () {
                return _
            }), a.d(t, "f", function () {
                return m
            }), a.d(t, "j", function () {
                return f
            }), a.d(t, "q", function () {
                return g
            }), a.d(t, "h", function () {
                return h
            }), a.d(t, "o", function () {
                return b
            }), a.d(t, "k", function () {
                return v
            }), a.d(t, "i", function () {
                return y
            }), a.d(t, "l", function () {
                return w
            }), a.d(t, "c", function () {
                return k
            }), a.d(t, "r", function () {
                return x
            }), a.d(t, "n", function () {
                return O
            });
            a("a481");
            var n = a("15b8"), o = a.n(n), i = ["txt", "csv"], r = "There's already the same one",
                s = ["all", "facebook", "instagram", "google", "apple"], c = ["visa", "mastercard", "amex", "jcb"],
                l = "image/*", d = "image/*, video/*",
                u = {scope: "email,manage_pages,business_management,ads_management,instagram_basic", return_scopes: !0},
                p = /^[\u00BF-\u1FFF\u2C00-\uD7FF\w#$&_ "+.,\/:\-\[\]\']+$/, _ = {
                    RUNNING: "running",
                    IN_REVIEW: "in_review",
                    IN_PROGRESS: "in_progress",
                    PROPOSAL_REVIEW: "proposal_review",
                    IN_REVIEW_EXPRESS: "in_review_express",
                    DEACTIVATED: "deactivated",
                    ONBOARDING: "onboarding",
                    SAMPLE: "sample"
                }, m = [{text: "COMMON.cta_fb_LEARN_MORE", value: "LEARN_MORE"}, {
                    text: "COMMON.cta_fb_SHOP_NOW",
                    value: "SHOP_NOW"
                }, {text: "COMMON.cta_fb_BOOK_TRAVEL", value: "BOOK_TRAVEL"}, {
                    text: "COMMON.cta_fb_LISTEN_NOW",
                    value: "LISTEN_NOW"
                }, {text: "COMMON.cta_fb_WATCH_VIDEO", value: "WATCH_VIDEO"}, {
                    text: "COMMON.cta_fb_SIGN_UP",
                    value: "SIGN_UP"
                }, {text: "COMMON.cta_fb_DOWNLOAD", value: "DOWNLOAD"}, {
                    text: "COMMON.cta_fb_WATCH_MORE",
                    value: "WATCH_MORE"
                }, {text: "COMMON.cta_fb_NO_BUTTON", value: "NO_BUTTON"}, {
                    text: "COMMON.cta_fb_APPLY_NOW",
                    value: "APPLY_NOW"
                }, {text: "COMMON.cta_fb_GET_OFFER", value: "GET_OFFER"}, {
                    text: "COMMON.cta_fb_SUBSCRIBE",
                    value: "SUBSCRIBE"
                }, {text: "COMMON.cta_fb_CONTACT_US", value: "CONTACT_US"}, {
                    text: "COMMON.cta_fb_ORDER_NOW",
                    value: "ORDER_NOW"
                }, {text: "COMMON.cta_fb_GET_SHOWTIMES", value: "GET_SHOWTIMES"}, {
                    text: "COMMON.cta_fb_PLAY_GAME",
                    value: "PLAY_GAME"
                }, {text: "COMMON.cta_fb_INSTALL_APP", value: "INSTALL_APP"}, {
                    text: "COMMON.cta_fb_USE_APP",
                    value: "USE_APP"
                }, {text: "COMMON.cta_fb_GET_QUOTE", value: "GET_QUOTE"}],
                f = ["APPLY_NOW", "BOOK_NOW", "CONTACT_US", "DOWNLOAD", "LEARN_MORE", "INSTALL", "VISIT_SITE", "SHOP_NOW", "SIGN_UP", "GET_QUOTE", "SUBSCRIBE", "SEE_MORE"],
                g = {
                    enter: 13,
                    backspace: 8,
                    tab: 9,
                    shift: 16,
                    ctrl: 17,
                    alt: 18,
                    capsLock: 20,
                    esc: 27,
                    up: 38,
                    down: 40
                }, h = {channel: "facebook", creativeType: "photo"}, b = {channel: "instagram", creativeType: "photo"},
                v = {channel: "google", creativeType: "ExpandedTextAd"},
                y = {channel: "google", creativeType: "ResponsiveDisplayAd"},
                w = {channel: "google", creativeType: "universalApp"}, k = {channel: "apple", creativeType: "search"},
                x = (o()([h, b, v]), o()([h, b, w, k]), e.contains(e.__, ["testuk4@adriel.ai", "backoffice@adriel.ai", "beom911@hanmail.net", "alicia.kim@adriel.ai", "beomyeon.kim@adriel.ai", "eric.kim@adriel.ai", "gayoung.hwang@adriel.ai", "hyunmin.oh@adriel.ai", "irene.ahn@adriel.ai", "jane.lee@adriel.ai", "junghoo.park@adriel.ai", "juok.ki@adriel.ai", "kevin.eum@adriel.ai", "maxime.chariere@adriel.ai", "michelle.lee@adriel.ai", "olivier.duchenne@adriel.ai", "seonmyung.lim@adriel.ai", "sophie.eom@adriel.ai", "sue.chung@adriel.ai", "thibaud.detandt@adriel.ai", "camilla.maia@adriel.ai"])),
                O = {attrs: {type: "textarea", isLimitLine: !0, cleaner: e.replace(/\n/gi, ""), resizable: !1}}
        }).call(this, a("b17e"))
    }, "4e4d": function (e, t, a) {
        "use strict";
        var n = function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return a("div", {staticClass: "left-nav-wrapper"}, [a("ul", [a("li", {staticClass: "top-logo"}, [a("a", {
                attrs: {
                    href: e.landingPage,
                    target: "_blank"
                }
            }, [a("img", {attrs: {src: "/img/logos/logo_n_f.svg"}})])]), this.isManagerAllowed ? a("router-link", {
                staticClass: "left-menu manager",
                attrs: {tag: "li", to: "/manager"}
            }, [a("span", {staticClass: "img-holder"}, [a("b-icon", {
                attrs: {
                    pack: "far",
                    icon: "handshake-alt"
                }
            })], 1), a("span", {staticClass: "left-title"}, [e._v(e._s(e.$t("LEFT_NAV.manager")))])]) : e._e(), a("router-link", {
                staticClass: "left-menu campaign",
                attrs: {tag: "li", to: "/myCampaigns", event: "MyCampaigns" === this.$route.name ? "" : "click"}
            }, [a("span", {staticClass: "img-holder"}, [a("b-icon", {
                attrs: {
                    pack: "far",
                    icon: "folder"
                }
            })], 1), a("span", {staticClass: "left-title"}, [e._v(e._s(e.$t("LEFT_NAV.my_campaigns")))])]), a("router-link", {
                staticClass: "left-menu setting",
                attrs: {tag: "li", to: "/accountSetting"}
            }, [a("span", {staticClass: "img-holder"}, [a("b-icon", {
                attrs: {
                    pack: "far",
                    icon: "user"
                }
            })], 1), a("span", {staticClass: "left-title"}, [e._v(e._s(e.$t("LEFT_NAV.account_setting")))])]), a("router-link", {
                staticClass: "left-menu help",
                attrs: {tag: "li", to: "/helpCampaign"}
            }, [a("span", {staticClass: "img-holder"}, [a("b-icon", {
                attrs: {
                    pack: "far",
                    icon: "envelope"
                }
            })], 1), a("span", {staticClass: "left-title"}, [e._v(e._s(e.$t("LEFT_NAV.help")))])]), a("li", {staticClass: "left-menu newCamp"}, [a("span", {on: {click: e.startChat}}, [a("i", {staticClass: "fal fa-plus"}), e._v("\n                " + e._s(e.$t("LEFT_NAV.new_campaign")) + "\n            ")])]), a("li", {staticClass: "left-menu expressCamp express-button"}, [a("span", {
                on: {
                    click: function (t) {
                        return e.goToExpress()
                    }
                }
            }, [a("b-icon", {
                attrs: {
                    pack: "far",
                    icon: "magic"
                }
            }), e._v("\n                " + e._s(e.$t("LEFT_NAV.express")) + "\n            ")], 1)]), e.isLangType("ko") ? a("li", {staticClass: "left-menu blog"}, [e._m(0)]) : e._e()], 1), a("div", {staticClass: "bottomWrapper"}, [a("span", {
                staticClass: "logout",
                on: {
                    click: function (t) {
                        return e.setLogoutModalActive(!0)
                    }
                }
            }, [a("b-icon", {
                style: {color: "#666666"},
                attrs: {pack: "far", icon: "sign-out"}
            }), a("span", {staticClass: "logout-title"}, [e._v(e._s(e.$t("LEFT_NAV.logout_tooltip")))])], 1)])])
        }, o = [function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return a("a", {
                attrs: {
                    href: "https://blog.naver.com/adrielai",
                    target: "_blank"
                }
            }, [a("img", {attrs: {src: "/img/icons/left_nav_blog.png"}}), e._v("\n                �꾨뱶由ъ뿕 釉붾줈洹� 蹂닿린\n            ")])
        }], i = a("cebc"), r = a("2f62"), s = a("fa7d"), c = a("4c6b"), l = {
            components: {},
            methods: Object(i["a"])({}, Object(r["mapActions"])("user", ["logout"]), Object(r["mapMutations"])("view", ["setLogoutModalActive", "setIsNewCampaignModalActive"]), {
                startChat: function () {
                    this.isDemo ? this.setDemoAccountModalActive(!0) : this.setIsNewCampaignModalActive(!0)
                }, goToExpress: function () {
                    this.isDemo ? this.setDemoAccountModalActive(!0) : this.$router.push({name: "Express"})
                }
            }),
            computed: Object(i["a"])({}, Object(r["mapGetters"])("user", ["user"]), {
                landingPage: function () {
                    return "ko" === Object(s["w"])(this.user) ? "https://adriel.ai/kr" : "https://adriel.ai/en"
                }, isManagerAllowed: function () {
                    return Object(c["r"])(this.user.email.toLowerCase())
                }
            })
        }, d = l, u = (a("34f3"), a("eaf5"), a("2877")), p = Object(u["a"])(d, n, o, !1, null, null, null);
        t["a"] = p.exports
    }, "4ec4": function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("cebc"), o = a("5176"), i = a.n(o), r = a("75fc"), s = a("dde5"), c = a("d3fb"), l = a("1585");

            function d() {
                return {
                    feeds: [],
                    user: {},
                    unreads: 0,
                    totalFeedsCnt: 0,
                    actionList: [],
                    isLoading: !1,
                    isActionBtnLoading: !1,
                    campaigns: {},
                    campaignSummaries: [],
                    actionRequiredCount: 0
                }
            }

            t["a"] = {
                namespaced: !0,
                state: d(),
                mutations: {
                    setFeeds: function (e, t) {
                        var a = t.newsfeed, n = t.queryType;
                        e.feeds = e.feeds.length && "UPDATE" !== n ? [].concat(Object(r["a"])(e.feeds), Object(r["a"])(a)) : a
                    }, setUser: function (e, t) {
                        e.user = t
                    }, setUnreads: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
                        e.unreads = t
                    }, setIsLoading: function (e, t) {
                        e.isLoading = t
                    }, setIsBtnLoading: function (e, t) {
                        e.isActionBtnLoading = t
                    }, setActionList: function (e, t) {
                        var a = t.recommendations, n = void 0 === a ? [] : a, o = t.campaigns,
                            s = void 0 === o ? {} : o, c = t.page;
                        e.actionList = c ? [].concat(Object(r["a"])(e.actionList), Object(r["a"])(n)) : n, e.campaigns = i()(e.campaigns, s)
                    }, updateAction: function (e, t) {
                        var a = t.actionId;
                        e.actionList = e.actionList.map(function (e) {
                            return e.id === a ? Object(n["a"])({}, e, {status: "PENDING"}) : e
                        })
                    }, setCampaignSummaries: function (e, t) {
                        e.campaignSummaries = t
                    }, setActionRequiredCount: function (e, t) {
                        e.actionRequiredCount = t
                    }, setDeletingActoinRequired: function (e, t) {
                        e.actionList = e.actionList.filter(function (e) {
                            return e.Id !== t
                        })
                    }
                },
                actions: {
                    fetchUnreads: function (e, t) {
                        var a = e.commit;
                        Object(c["a"])(s["o"].fetchNbFeeds(t)).pipe(Object(l["a"])("new")).subscribe(function (e) {
                            return a("setUnreads", e)
                        }, function () {
                            return a("setUnreads", 0)
                        })
                    }, getActionList: function (e, t) {
                        var a = e.commit;
                        return a("setIsLoading", !0), s["j"].getActionList(t).then(function (e) {
                            a("setActionList", Object(n["a"])({}, e, {page: t.page})), a("setIsLoading", !1)
                        }).catch(function (e) {
                            a("setIsLoading", !1), console.log(e)
                        })
                    }, getQueryActionList: function (e, t) {
                        var a = e.commit;
                        return a("setIsLoading", !0), s["j"].getActionItem(t).then(function (e) {
                            a("setActionList", e), a("setIsLoading", !1)
                        }).catch(function (e) {
                            a("setIsLoading", !1), console.log(e)
                        })
                    }, getCampaignSummaries: function (e, t) {
                        var a = e.commit, n = t.type, o = t.userId;
                        return s["j"].getCampaignSummaries({type: n, userId: o}).then(function (e) {
                            return a("setCampaignSummaries", e)
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, postActionState: function (e, t) {
                        var a = e.commit, n = e.dispatch, o = t.actionId, i = t.userId;
                        return a("isActionBtnLoading", !1), s["j"].postActionState({actionId: o}).then(function () {
                            return a("updateAction", {actionId: o}), a("isActionBtnLoading", !1), n("getActionRequiredCount", {userId: i})
                        }).catch(function (e) {
                            console.log(e), a("isActionBtnLoading", !1)
                        })
                    }, getActionRequiredCount: function (e, t) {
                        var a = e.commit, n = t.userId;
                        return s["j"].getActionRequiredCount({userId: n}).then(function (e) {
                            var t = e.count;
                            return a("setActionRequiredCount", Number(t))
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, deleteActionRequired: function (e, t) {
                        var a = e.commit;
                        return s["j"].deleteActionRequired(t).then(function () {
                            return a("setDeletingActoinRequired", t)
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, getNewsfeed: function (e, t) {
                        var a = e.commit;
                        return s["j"].getNewsfeed(t).then(function (e) {
                            var n = e.feeds;
                            a("setFeeds", {newsfeed: n, queryType: t.queryType}), a("setIsLoading", !1)
                        }).catch(function (e) {
                            a("setIsLoading", !1), console.log(e)
                        })
                    }
                },
                getters: {
                    actionList: e.prop("actionList"),
                    campaigns: e.prop("campaigns"),
                    campaignSummaries: e.prop("campaignSummaries"),
                    feeds: e.prop("feeds"),
                    isLoading: e.prop("isLoading"),
                    actionRequiredCount: e.prop("actionRequiredCount"),
                    isActionBtnLoading: e.prop("isActionBtnLoading")
                }
            }
        }).call(this, a("b17e"))
    }, "51c6": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("b-modal", {
                    staticClass: "modal-box",
                    attrs: {active: e._active, canCancel: ["escape", "x"], width: e.width},
                    on: {
                        "update:active": function (t) {
                            e._active = t
                        }, close: e.cancel
                    }
                }, [a("div", {
                    ref: "wrapper",
                    staticClass: "modal-box-wrapper",
                    style: e.wrapperStyle
                }, [a("div", {
                    ref: "header",
                    staticClass: "modal-box-header"
                }, [e._v(e._s(e.headerText))]), a("div", {staticClass: "modal-box-adder-wrapper"}, [e._t("default")], 2), e.hasButtons && e.hasCancelBtn ? a("div", {staticClass: "buttons-wrapper"}, [e.hasCancelBtn ? a("button", {
                    on: {
                        mousedown: function (e) {
                            e.stopPropagation()
                        }, click: e.cancel
                    }
                }, [e._v(e._s(e.cancelText))]) : e._e(), a("button", {
                    on: {
                        mousedown: function (e) {
                            e.stopPropagation()
                        }, click: e.confirm
                    }
                }, [e._v(e._s(e.confirmText))])]) : e._e(), e.hasButtons && !e.hasCancelBtn ? a("div", {staticClass: "confirm-wrapper"}, [a("button", {
                    on: {
                        mousedown: function (e) {
                            e.stopPropagation()
                        }, click: e.confirm
                    }
                }, [e._v(e._s(e.confirmText))])]) : e._e()])])
            }, o = [], i = a("08df"), r = i["a"], s = (a("5b7f"), a("08c6"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "65b61bda", null);
        t["a"] = c.exports
    }, "537c": function (e, t, a) {
    }, 5477: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), o = a.n(n), i = (a("7f7f"), a("cebc")), r = a("25eb"), s = a("fa7d"),
                c = ["#B72B2B", "#E24141", "#FF8F8F", "#B90B57", "#EC3E8A", "#D862EA", "#A322B7", "#783F9F", "#957DF6", "#471198", "#1D4492", "#3266CD", "#42A3DB", "#295785", "#053E77", "#30B6BF", "#9CDBC0", "#50B68A", "#389955", "#2F7B14", "#9ECE3C", "#E1E652", "#E3B73C", "#DF8218"],
                l = function () {
                    return c[Object(s["X"])(0, c.length - 1)]
                };
            t["a"] = {
                name: "AccountModal",
                props: {
                    editMode: {type: Boolean, default: !1}, account: {
                        type: Object, default: function () {
                            return {color: l()}
                        }
                    }
                },
                created: function () {
                    this.value = Object(i["a"])({}, this.account)
                },
                data: function () {
                    return {value: {}, dirty: !1}
                },
                render: function () {
                    var e = this, t = arguments[0], a = this.value, n = a.color, i = a.name, s = {
                        attrs: {active: !0, canCancel: ["escape"], "max-width": "auto", width: "750px"},
                        on: {"update:active": this.updateActive}
                    }, l = {
                        attrs: {
                            placeholder: "Account name",
                            value: i,
                            resizable: !1,
                            error: this.dirty ? this.$t("COMMON.required") : ""
                        }, on: {
                            input: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                                t.trim() && (e.dirty = !1), e.updateValue("name", t)
                            }
                        }
                    };
                    return t(r["l"], o()([{}, s]), [t(r["m"], [t(r["i"], [t(r["j"], [this.headerText]), t(r["d"], {
                        attrs: {src: "/img/icons/modal_close_btn.png"},
                        nativeOn: {
                            click: function () {
                                return e.updateActive(!1)
                            }
                        }
                    })]), t(r["a"], [t(r["k"], o()([{}, l]), [t(r["f"], {
                        slot: "icon",
                        attrs: {bgColor: n}
                    })]), t(r["g"], {nativeOn: {click: this.handleClick}}, [c.map(function (e) {
                        return t(r["e"], {attrs: {bgColor: e, "data-color": e}})
                    })]), t(r["b"], [t(r["c"], {
                        nativeOn: {
                            click: function () {
                                return e.updateActive(!1)
                            }
                        }
                    }, [this.$t("COMMON.cancel")]), t(r["h"], {nativeOn: {click: this.handleConfirm}}, [this.confirmText])])])])])
                },
                methods: {
                    updateActive: function (e) {
                        this.$emit("update:active", e)
                    }, handleClick: function (t) {
                        var a = e.path(["target", "dataset", "color"], t);
                        a && this.updateValue("color", a)
                    }, handleConfirm: function () {
                        var e = this.value.name || "";
                        e.trim() ? (this.$emit("confirm", this.value), this.updateActive(!1)) : this.dirty = !0
                    }, updateValue: e.curry(function (t, a) {
                        this.value = e.assoc(t, a, this.value)
                    })
                },
                computed: {
                    headerText: function () {
                        return this.editMode ? this.$t("ACCOUNT_SETTING.account_modal_header_edit") : this.$t("ACCOUNT_SETTING.account_modal_header_add")
                    }, confirmText: function () {
                        return this.editMode ? this.$t("ACCOUNT_SETTING.account_modal_confirm_edit") : this.$t("ACCOUNT_SETTING.account_modal_confirm_add")
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "55cc": function (e, t, a) {
    }, "55cd": function (e, t) {
        e.exports = {
            en: {
                congratulations: "Congratulations!",
                contents: "Your ads are now in review by external platforms.<br> As soon as they're approved and live, your ad results will <br> appear on the Campaign Results page.",
                go_to_my: "Go to My campaigns",
                in_review: "In review"
            },
            jp: {
                congratulations: "�듽굙�㎯겏�녴걫�뽧걚�얇걲竊�",
                contents: "�잆걽�꾠겲�곩쨼�ⓦ깤�⒲긿�덀깢�⒲꺖�졼걣�귙겒�잆겗佯껃몜�믣��삡릎�㎯걲��<br> 佯껃몜�뚧돽沃띲걬�뚧А寧с�곥궘�ｃ꺍�싥꺖�녕탳�쒌깪�쇈궦�ュ룏�졼걬�뚣겲�쇻��",
                go_to_my: "�욁궎��깵�녈깪�쇈꺍�ヨ죱��",
                in_review: "野⒵읅訝�"
            },
            ko: {
                congratulations: "異뺥븯�⑸땲��!",
                contents: "愿묎퀬 �뚮옯�쇱씠 愿묎퀬二쇰떂�� 愿묎퀬瑜� 寃��좏븯怨� �덉뒿�덈떎. <br>愿묎퀬媛� �뱀씤�섎㈃ 吏묓뻾 �꾪솴�� 罹좏럹�� 寃곌낵 �섏씠吏��먯꽌 �뺤씤�섏떎 �� �덉뒿�덈떎.",
                go_to_my: "�� 愿묎퀬 蹂닿린",
                in_review: "寃��� 以�"
            }
        }
    }, "56ba": function (e, t, a) {
    }, "56d7": function (e, t, a) {
        "use strict";
        a.r(t);
        a("04f3");
        var n = a("ed3b"), o = (a("b6e5"), a("55f1")), i = (a("73d0"), a("a600")), r = (a("a71a"), a("b558")),
            s = a("cebc"), c = (a("cadf"), a("551c"), a("f751"), a("097d"), a("2b0e")), l = a("ce19"), d = a("7bb1"),
            u = a("0284"), p = a.n(u), _ = a("31fa"), m = a.n(_), f = a("b6d0d"), g = a("e37d"), h = a("7890"),
            b = a.n(h), v = a("05f6"), y = a.n(v), w = a("252c"), k = a("d847"), x = a.n(k), O = a("795b"), A = a.n(O),
            C = a("bc3a"), S = a.n(C), E = {baseURL: "https://app.adriel.ai/api", withCredentials: !0},
            T = S.a.create(E);
        T.interceptors.request.use(function (e) {
            return e
        }, function (e) {
            return A.a.reject(e)
        }), T.interceptors.response.use(function (e) {
            return e
        }, function (e) {
            return A.a.reject(e)
        }), T.setAccountId = function (e) {
            T.defaults.headers.common["Adriel-Account"] = e
        }, Plugin.install = function (e, t) {
            e.axios = T, window.axios = T, x()(e.prototype, {
                axios: {
                    get: function () {
                        return T
                    }
                }, $axios: {
                    get: function () {
                        return T
                    }
                }
            })
        }, c["default"].use(Plugin);
        Plugin;
        var P = a("8a03"), I = a.n(P), L = a("0a0d"), j = a.n(L), M = (a("c5f6"), {
                name: "Delay",
                props: {wait: {type: Number, default: 0}, from: {type: Number, default: j()()}},
                data: function () {
                    return {waiting: !0}
                },
                created: function () {
                    var e = this;
                    this.timer = setTimeout(function () {
                        e.waiting = !1
                    }, this.from - j()() + this.wait)
                },
                destroyed: function () {
                    clearTimeout(this.timer)
                },
                render: function (e) {
                    return e("div", this.waiting ? this.$slots.loading || null : this.$slots.default)
                }
            }), R = a("2ee0"), N = a("61f4"), D = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("span", {staticClass: "character-counter-wrapper"}, [a("span", {staticClass: "character-counter-current"}, [e._v(e._s(e.current))]), a("span", [e._v("/")]), a("span", {staticClass: "character-counter-max"}, [e._v(e._s(e.max))])])
            }, $ = [], B = a("163e"), F = B["a"], G = (a("57a0"), a("2877")),
            U = Object(G["a"])(F, D, $, !1, null, "0173edb9", null), q = U.exports, Y = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return e.asImage && e.success ? a("div", {staticClass: "bg-as-image"}, [a("img", {attrs: {src: e.src}})]) : a("div", {
                    staticClass: "bg-img-wrapper",
                    class: [{"bg-success": e.success, "bg-fallback": !e.success}, e.className],
                    style: e.imgStyle
                })
            }, V = [], z = a("58bd"), W = z["a"], H = (a("0300"), Object(G["a"])(W, Y, V, !1, null, "96bdc266", null)),
            K = H.exports, J = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("span", {staticClass: "info-tooltip"}, [a("img", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: e.label,
                        expression: "label"
                    }], attrs: {src: "/img/icons/tooltip_icon.png"}
                })])
            }, Q = [], X = {props: {label: {type: String, default: ""}}}, Z = X,
            ee = Object(G["a"])(Z, J, Q, !1, null, null, null), te = ee.exports, ae = a("1fee"), ne = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("textarea", {
                    directives: [{
                        name: "model",
                        rawName: "v-model",
                        value: e._value,
                        expression: "_value"
                    }],
                    ref: "input",
                    staticClass: "adriel-common-textarea",
                    attrs: {type: "text", placeholder: e.placeholder, autofocus: e.autofocus, contenteditable: ""},
                    domProps: {value: e._value},
                    on: {
                        focus: function (t) {
                            return e.focusFn()
                        }, input: function (t) {
                            t.target.composing || (e._value = t.target.value)
                        }
                    }
                })
            }, oe = [], ie = a("57d6"), re = ie["a"], se = (a("ac9e"), Object(G["a"])(re, ne, oe, !1, null, null, null)),
            ce = se.exports, le = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("img", {attrs: {src: e.src}})
            }, de = [], ue = a("f9be"), pe = ue["a"], _e = Object(G["a"])(pe, le, de, !1, null, null, null),
            me = _e.exports;
        c["default"].use(I.a), c["default"].component("Delay", M), c["default"].component("message-modal", R["a"]), c["default"].component("confirm-modal", N["a"]), c["default"].component("bg-img", K), c["default"].component("info-tooltip", te), c["default"].component("adriel-input", ae["a"]), c["default"].component("adriel-textarea", ce), c["default"].component("v-popover", g["a"].VPopover), c["default"].component("character-counter", q), c["default"].component("check-box", me);
        var fe = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {
                    class: e.language,
                    attrs: {id: "app"}
                }, [e.ready ? a("div", [e.showNotSupported ? a("div", {staticClass: "browser-warning notices is-top"}, [a("div", {staticClass: "toast adriel-warning is-top"}, [a("div", [e._v("�꾨뱶由ъ뿕�� �щ＼ 釉뚮씪�곗��먯꽌 媛��� �� �묐룞�⑸땲��. �꾩옱 �붾㈃�� �� 蹂댁씠吏� �딄굅��, 理쒖쟻�붾맂 �섍꼍�먯꽌 �쒕퉬�ㅻ� �댁슜�섏떆�ㅻ㈃ �щ＼ 釉뚮씪�곗�瑜� �ъ슜�댁＜�몄슂.")]), a("img", {
                    attrs: {src: "/img/icons/toast_close_btn.png"},
                    on: {
                        click: function (t) {
                            e.showNotSupported = !1
                        }
                    }
                })])]) : e._e(), a("loading-adriel"), a("logout"), a("new-campaign", {
                    attrs: {active: e.isNewCampaignModalActive},
                    on: {"update:active": e.setIsNewCampaignModalActive}
                }), e.isDemo ? a("demo-FAB", {on: {confirm: e.clickSignUpFromDemo}}) : e._e(), a("floating-account"), a("modal-box", {
                    attrs: {
                        active: e.isDemoAccountModalActive,
                        "header-text": e.$t("DEMO.header_modal"),
                        cancelText: e.$t("COMMON.cancel"),
                        "confirm-text": e.$t("DEMO.want_to_signup")
                    }, on: {
                        confirm: e.clickSignUpFromDemo, cancel: function (t) {
                            return e.setDemoAccountModalActive(!1)
                        }
                    }
                }, [a("p", {staticClass: "text-center"}, [e._v(e._s(e.$t("DEMO.need_real_account")))])]), a("div", {attrs: {id: "main-route-view"}}, [a("router-view")], 1)], 1) : [e._v("Loading ...")]], 2)
            }, ge = [], he = a("199c"), be = he["a"], ve = (a("5c0b"), Object(G["a"])(be, fe, ge, !1, null, null, null)),
            ye = ve.exports, we = a("41cb"), ke = a("4360"), xe = a("9225"),
            Oe = (a("8975"), a("4657"), a("5abe"), a("6834")), Ae = a("fa7d"), Ce = a("243b"), Se = a.n(Ce),
            Ee = a("3100"), Te = a.n(Ee);
        a.d(t, "EventBus", function () {
            return Ie
        }), a.d(t, "i18n", function () {
            return xe["a"]
        }), a.d(t, "store", function () {
            return ke["a"]
        }), c["default"].use(l["a"]), c["default"].use(w["b"]), c["default"].use(b.a, {importCss: !0}), c["default"].use(d["a"], {
            locale: Object(Ae["w"])(),
            dictionary: {
                en: Object(s["a"])({}, Se.a, {messages: Object(s["a"])({}, Se.a.messages, {url: "This field is not a valid URL."})}),
                ko: Object(s["a"])({}, Te.a, {
                    messages: Object(s["a"])({}, Te.a.messages, {
                        url: "�섎せ�� URL�� �낅젰�섏뿀�듬땲��.",
                        required: "�꾩닔 �낅젰�ы빆 �낅땲��."
                    })
                })
            }
        }), c["default"].use(p.a, {
            id: "UA-121121420-1",
            router: we["a"],
            debug: {sendHitTask: !0},
            linkers: ["adriel.ai"]
        }), c["default"].use(m.a, {appId: "i11ny0xx"}), c["default"].use(r["a"]), c["default"].use(i["a"]), c["default"].use(o["a"]), c["default"].use(n["a"]), c["default"].use(y.a), c["default"].use(f["a"]), c["default"].use(g["a"], {
            defaultPlacement: "bottom",
            defaultClass: "adriel-tooltip",
            defaultHideOnTargetClick: !1,
            popover: {
                defaultPlacement: "bottom",
                defaultClass: "adriel-tooltip adriel-tooltip-popover",
                defaultWrapperClass: "adriel-tooltip-wrapper",
                defaultInnerClass: "adriel-tooltip-inner",
                defaultTrigger: "hover",
                defaultContainer: "body"
            }
        }), c["default"].config.comma = 188, c["default"].config.productionTip = !1, c["default"].mixin(Oe["a"]);
        var Pe = new c["default"]({
            router: we["a"], store: ke["a"], i18n: xe["a"], render: function (e) {
                return e(ye)
            }
        }).$mount("#app");
        window.app = Pe;
        var Ie = new c["default"]
    }, "57a0": function (e, t, a) {
        "use strict";
        var n = a("8279"), o = a.n(n);
        o.a
    }, "57d6": function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("6e77"), o = a("a748"), i = a("1a2d"), r = a("4677"), s = a("ebb6"), c = a("5670"), l = a("fa7d");
            t["a"] = {
                mounted: function () {
                    if (!0 === this.autofocus) {
                        var e = this.$refs.elem;
                        e && e.focus()
                    }
                },
                created: function () {
                    var t = this;
                    if (this.autoResize) {
                        var a = this.$watchAsObservable("_value", {immediate: !0}),
                            d = Object(n["a"])(window, "resize").pipe(Object(i["a"])(300)),
                            u = Object(o["a"])(a, d).pipe(Object(r["a"])(150), Object(s["a"])(function () {
                                return t.$refs.input
                            }), Object(c["a"])(e.identity));
                        this.$subscribeTo(u, l["Y"])
                    }
                },
                props: {
                    placeholder: {type: String, default: ""},
                    value: {type: [String, Number], default: ""},
                    name: {type: String, default: ""},
                    autoResize: {type: Boolean, default: !1},
                    autofocus: {type: Boolean, default: !1},
                    focusFn: {
                        type: Function, default: function () {
                        }
                    }
                },
                computed: {
                    _value: {
                        get: e.prop("value"), set: function (e) {
                            this.$emit("input", e)
                        }
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "58bd": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("5176"), o = a.n(n);
            t["a"] = {
                data: function () {
                    return {success: !0}
                },
                created: function () {
                    this.load()
                },
                methods: {
                    load: function () {
                        var e = new Image, t = this;
                        e.onload = function () {
                            e.onload = null, e.onerror = null, t.success = !0, t.$emit("load:success", e)
                        }, e.onerror = function (a) {
                            e.onload = null, e.onerror = null, t.success = !1, t.$emit("load:fail", a)
                        }, e.src = this.src
                    }
                },
                props: {
                    src: {type: String, default: ""},
                    fallback: {type: String, default: "/img/fallback_img.png"},
                    dimension: {type: Array, default: e.always([])},
                    fallbackStyle: {type: Object, default: e.always({})},
                    asImage: {type: Boolean, default: !1},
                    className: String
                },
                watch: {
                    src: function () {
                        this.load()
                    }
                },
                computed: {
                    imgURL: function () {
                        return this.success && this.src
                    }, imgStyle: function () {
                        var t, a = this.imgURL;
                        return t = this.dimension.length ? e.merge({
                            width: this.dimension[0] + "px",
                            height: this.dimension[1] + "px"
                        }) : e.merge({
                            width: "100%",
                            height: "100%"
                        }), t(a ? {
                            "background-image": "url('".concat(a, "')"),
                            "background-size": "cover"
                        } : o()(this.fallbackStyle, {"background-image": "url('".concat(this.fallback, "')")}))
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "5a7a": function (e, t, a) {
        "use strict";
        var n = a("b6a0"), o = a.n(n);
        o.a
    }, "5b7f": function (e, t, a) {
        "use strict";
        var n = a("0d21"), o = a.n(n);
        o.a
    }, "5c0b": function (e, t, a) {
        "use strict";
        var n = a("1ac1"), o = a.n(n);
        o.a
    }, "5d4b": function (e, t, a) {
        "use strict";
        var n = a("342e"), o = a.n(n);
        o.a
    }, "5e64": function (e, t, a) {
    }, "5f78": function (e, t, a) {
        "use strict";
        (function (e) {
            a("6b54");
            var n = a("7618"), o = a("a745"), i = a.n(o), r = (a("a481"), a("768b")), s = (a("ac6a"), a("dde5")),
                c = a("9225"), l = [{
                    type: "title",
                    title: "",
                    isEditing: !1,
                    isFocusing: !1,
                    default: "FORM.title"
                }, {type: "description", title: "", isEditing: !1, isFocusing: !1, default: "FORM.desc"}], d = [{
                    type: "short",
                    title: "FORM.phoneDefault",
                    isEditing: !1,
                    isFocusing: !1,
                    isRequired: !0,
                    isDrag: !1,
                    default: "FORM.quesTitle"
                }, {
                    type: "short",
                    title: "FORM.emailDefault",
                    isEditing: !1,
                    isFocusing: !1,
                    isRequired: !0,
                    isDrag: !1,
                    default: "FORM.quesTitle"
                }, {
                    type: "checkbox",
                    title: "",
                    isEditing: !1,
                    isFocusing: !1,
                    isRequired: !0,
                    isDrag: !0,
                    default: "FORM.quesTitle",
                    choices: [{type: "choice", choice: "", isEditing: !1, default: "FORM.multipleItem"}, {
                        type: "choice",
                        choice: "",
                        isEditing: !1,
                        default: "FORM.multipleItem"
                    }]
                }, {
                    type: "multiple",
                    title: "",
                    isEditing: !1,
                    isFocusing: !1,
                    isRequired: !0,
                    isDrag: !0,
                    default: "FORM.quesTitle",
                    choices: [{type: "choice", choice: "", isEditing: !1, default: "FORM.multipleItem"}, {
                        type: "choice",
                        choice: "",
                        isEditing: !1,
                        default: "FORM.multipleItem"
                    }]
                }], u = [{type: "thanks", isEditing: !1, isFocusing: !1, title: "", default: "FORM.thanks_placeholder"}],
                p = ["isFocusing", "isEditing", "isDrag", "default"], _ = function (e) {
                    var t = function (e) {
                        return [["isEditing", !1], ["isFocusing", !1], ["default", "FORM.".concat(e)]]
                    }, a = function (e) {
                        return [["isEditing", !1], ["default", "FORM.".concat(e)]]
                    }, n = function (e, t, a) {
                        return a[e] = t
                    }, o = function (e, a) {
                        return t(a).forEach(function (t) {
                            var a = Object(r["a"])(t, 2), o = a[0], i = a[1];
                            return n(o, i, e)
                        })
                    }, i = function (e, t) {
                        return a(t).forEach(function (t) {
                            var a = Object(r["a"])(t, 2), o = a[0], i = a[1];
                            return n(o, i, e)
                        })
                    };
                    e.header.forEach(function (e) {
                        "title" === e.type ? o(e, "title") : o(e, "desc")
                    }), e.body.forEach(function (e) {
                        e.choices && e.choices.forEach(function (e) {
                            return i(e, "multipleItem")
                        }), e.isRequired = void 0 == e.isRequired || e.isRequired, o(e, "quesTitle")
                    }), e.footer.forEach(function (e) {
                        o(e, "thanks")
                    })
                }, m = {
                    index: function (e, t, a, n, o) {
                        var i = Object(r["a"])(a, 3), c = i[0], l = i[1], d = i[2];
                        return s["g"].callApi("forms", t, {path: [c], params: {type: l, campaignId: c}}).then(function (t) {
                            _(t.questions), e.commit("CHANGE_DATA", [d || "formEdit", function (e, a) {
                                e[a] = t.questions
                            }]), e.commit("CHANGE_DATA", [[d || "formEdit", "linkId"], function (e, a) {
                                var n = Object(r["a"])(a, 2), o = n[0], i = n[1];
                                e[o][i] = t.campaign_id
                            }]), n(t)
                        }).catch(function (e) {
                            console.log(e), o(e)
                        })
                    }, create: function (e, t, a, n, o) {
                        var i = function e(t, a) {
                            var n = f(t);
                            return n.choices && (n.choices = n.choices.map(e)), p.forEach(function (e) {
                                return n[e] = void 0
                            }), n.order = a, "choice" == n.type ? (n.choice = t.choice || t.default, n.choice = n.choice.replace(/\n/g, "<br>")) : (n.title = t.title || t.default, n.title = n.title.replace(/\n/g, "<br>")), n
                        };
                        return s["g"].callApi("forms", t, {
                            path: [a],
                            data: {
                                img: e.state.formCreate.img,
                                header: e.state.formCreate.header.map(i),
                                body: e.state.formCreate.body.map(i),
                                footer: e.state.formCreate.footer.map(i),
                                lang: c["a"].locale
                            }
                        }).then(function (t) {
                            e.commit("CHANGE_DATA", [["formCreate", "linkId"], function (e, a) {
                                var n = Object(r["a"])(a, 2), o = n[0], i = n[1];
                                e[o][i] = t.campaign_id
                            }]), n(t)
                        }).catch(function (e) {
                            console.log(e), o(e)
                        })
                    }, update: function (e, t, a, n, o) {
                        var i = function e(t, a) {
                            var n = f(t);
                            return n.choices && (n.choices = n.choices.map(e)), p.forEach(function (e) {
                                return n[e] = void 0
                            }), n.order = a, n
                        };
                        return s["g"].callApi("forms", t, {
                            path: [a],
                            data: {
                                img: e.state.formEdit.img,
                                header: e.state.formEdit.header.map(i),
                                body: e.state.formEdit.body.map(i),
                                footer: e.state.formEdit.footer.map(i),
                                lang: c["a"].locale
                            }
                        }).then(function (e) {
                            n(e)
                        }).catch(function (e) {
                            console.log(e), o(e)
                        })
                    }
                };

            function f(e) {
                var t = i()(e) ? [] : {};
                if ("object" === Object(n["a"])(e) && null !== e) for (var a in e) e.hasOwnProperty(a) && (t[a] = f(e[a])); else t = e;
                return t
            }

            function g(e, t, a) {
                if ("[object Object]" !== Object.prototype.toString.call(t) || null != a && !e.hasOwnProperty(a)) ; else {
                    var n = null == a ? e : e[a];
                    for (var o in t) g(n, t[o], o)
                }
            }

            t["a"] = {
                namespaced: !0,
                state: {
                    nowEditPath: [],
                    editables: [],
                    headerData: l,
                    formData: d,
                    footerData: u,
                    title: "Title",
                    isEditingTitle: !1,
                    isFocusingTitle: !1,
                    desc: "Description",
                    isEditingDes: !1,
                    isFocusingDes: !1,
                    thanks: "",
                    imgSrc: "",
                    formCreate: {linkId: "", img: "", header: f(l), body: f(d), footer: f(u)},
                    formEdit: {linkId: "", img: "", header: f(l), body: f(d), footer: f(u)},
                    formResultData: {header: [], body: [], footer: []}
                },
                mutations: {
                    SET_DATA: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        _(o), e[n] = o
                    }, CHANGE_DATA: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        o(e, n)
                    }, REMOVE_DATA: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        o(e, n)
                    }, MERGE_DATA: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        g(e, o, n)
                    }
                },
                actions: {
                    setData: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        e.commit("SET_DATA", [n, o])
                    }, clearData: function (e, t) {
                        var a = Object(r["a"])(t, 1), n = a[0];
                        e.commit("CHANGE_DATA", [n, function (e, t) {
                            e[t] = {linkId: "", img: "", header: f(l), body: f(d), footer: f(u)}
                        }])
                    }, changeData: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        e.commit("CHANGE_DATA", [n, o])
                    }, removeData: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        e.commit("REMOVE_DATA", [n, o])
                    }, mergeData: function (e, t) {
                        var a = Object(r["a"])(t, 2), n = a[0], o = a[1];
                        e.commit("MERGE_DATA", [n, o])
                    }, restApi: function (e, t) {
                        var a = Object(r["a"])(t, 4), n = a[0], o = a[1], i = a[2], s = a[3];
                        return m[n](e, n, o, i, s)
                    }
                },
                getters: {
                    nowEditRef: e.prop("nowEditRef"),
                    editables: e.prop("editables"),
                    formData: e.compose(f, e.prop("formData")),
                    formUsersHeader: e.compose(e.map(function (e) {
                        return {type: e.type, title: e.title}
                    }), e.prop("headerData")),
                    headerData: e.prop("headerData"),
                    thanksMessage: e.compose(String, e.map(e.prop("title")), e.path(["formCreate", "footer"])),
                    title: e.prop("title"),
                    desc: e.prop("desc"),
                    thanks: e.prop("thanks"),
                    imgSrc: e.prop("imgSrc"),
                    nowEditPath: e.prop("nowEditPath"),
                    formCreate: e.prop("formCreate"),
                    formEdit: e.prop("formEdit"),
                    basicQues: e.always({
                        short: {
                            type: "short",
                            title: "",
                            isEditing: !1,
                            isFocusing: !1,
                            isDrag: !0,
                            isRequired: !0,
                            default: "FORM.quesTitle"
                        },
                        multiple: {
                            type: "multiple",
                            title: "",
                            isEditing: !1,
                            isFocusing: !1,
                            isDrag: !0,
                            isRequired: !0,
                            default: "FORM.quesTitle",
                            choices: [{
                                type: "choice",
                                choice: "",
                                isEditing: !1,
                                default: "FORM.multipleItem"
                            }, {type: "choice", choice: "", isEditing: !1, default: "FORM.multipleItem"}]
                        },
                        checkbox: {
                            type: "checkbox",
                            title: "",
                            isEditing: !1,
                            isFocusing: !1,
                            isDrag: !0,
                            isRequired: !0,
                            default: "FORM.quesTitle",
                            choices: [{
                                type: "choice",
                                choice: "",
                                isEditing: !1,
                                default: "FORM.multipleItem"
                            }, {type: "choice", choice: "", isEditing: !1, default: "FORM.multipleItem"}]
                        }
                    }),
                    emptyChoice: e.always({type: "choice", choice: "", isEditing: !1, default: "FORM.multipleItem"})
                }
            }
        }).call(this, a("b17e"))
    }, "608d": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("span", {staticClass: "title-wrapper"}, [e._t("icon"), a("span", {
                    staticClass: "proposal-title",
                    class: {"main-sub-title": !!e.sub, "main-only-title": !e.sub}
                }, [e.sub ? [a("span", {staticClass: "sub"}, [e._v(e._s(e.sub))]), a("span", {
                    staticClass: "main",
                    on: {
                        click: function (t) {
                            return e.$emit("click:main")
                        }
                    }
                }, [e._v(e._s(e.main))]), e._t("next-main"), e.subLabel ? a("span", {staticClass: "sub-label"}, [e._v(e._s(e.subLabel))]) : e._e()] : [a("span", {staticClass: "main"}, [e._v(e._s(e.main))])]], 2), e._t("title-slot")], 2)
            }, o = [], i = {props: ["main", "sub", "subLabel"]}, r = i, s = (a("8e48"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "5c4d9a3e", null);
        t["a"] = c.exports
    }, 6130: function (e, t, a) {
    }, "61f4": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("b-modal", {
                    class: e.className,
                    attrs: {"has-modal-card": "", active: e._active, canCancel: !1},
                    on: {
                        "update:active": function (t) {
                            e._active = t
                        }
                    }
                }, [a("div", {staticClass: "confirm-modal-wrapper"}, [e._t("body", [a("span", {domProps: {innerHTML: e._s(e.text)}})]), a("div", {staticClass: "modal-button-wrapper"}, [a("button", {on: {click: e.cancel}}, [e._v("\n                " + e._s(e._cancelText) + "\n            ")]), a("button", {on: {click: e.confirm}}, [e._v("\n                " + e._s(e._confirmText) + "\n            ")])])], 2)])
            }, o = [], i = a("7781"), r = i["a"], s = (a("1160"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "498829aa", null);
        t["a"] = c.exports
    }, "632d": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("e380"), o = a.n(n), i = "/locations", r = o()(function (t, a) {
                return axios.get("".concat(i, "/autocomplete"), {params: {q: t, address: a}}).then(e.prop("data"))
            });
            t["a"] = {autocomplete: r}
        }).call(this, a("b17e"))
    }, "648b": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("cebc"), o = a("2f62"), i = a("808d"), r = a("d3fb"), s = a("ebb6"), c = a("550d"), l = a("3fab");
            t["a"] = {
                created: function () {
                    var e = this;
                    Object(i["a"])(0, 500).pipe(Object(s["a"])(function () {
                        return e.$refs.input
                    }), Object(c["a"])(function (e) {
                        return !e
                    }), Object(l["a"])()).subscribe(function (e) {
                        return e.focus()
                    })
                },
                props: {campaign: {type: Object, required: !0}, callback: {type: Function, default: e.identity}},
                data: function () {
                    return {editingCampaign: e.clone(this.campaign)}
                },
                methods: Object(n["a"])({}, Object(o["mapActions"])("campaign", ["updateTitle"]), {
                    confirm: function () {
                        var e = this;
                        Object(r["a"])(this.updateTitle({
                            newValue: this.editingCampaign,
                            oldValue: this.campaign
                        })).subscribe(function (t) {
                            e.callback(t), e.$emit("complete", t)
                        }, function (t) {
                            console.log(t), e.$emit("complete")
                        })
                    }
                })
            }
        }).call(this, a("b17e"))
    }, 6815: function (e, t, a) {
        var n = {
            "./en-SG": "cdab",
            "./en-SG.js": "cdab",
            "./en-au": "0e6b",
            "./en-au.js": "0e6b",
            "./en-ca": "3886",
            "./en-ca.js": "3886",
            "./en-gb": "39a6",
            "./en-gb.js": "39a6",
            "./en-ie": "e1d3",
            "./en-ie.js": "e1d3",
            "./en-il": "7333",
            "./en-il.js": "7333",
            "./en-nz": "6f50",
            "./en-nz.js": "6f50",
            "./ko": "22f8",
            "./ko.js": "22f8"
        };

        function o(e) {
            var t = i(e);
            return a(t)
        }

        function i(e) {
            var t = n[e];
            if (!(t + 1)) {
                var a = new Error("Cannot find module '" + e + "'");
                throw a.code = "MODULE_NOT_FOUND", a
            }
            return t
        }

        o.keys = function () {
            return Object.keys(n)
        }, o.resolve = i, e.exports = o, o.id = "6815"
    }, 6834: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("e814"), o = a.n(n), i = a("3be2"), r = a.n(i), s = (a("c5f6"), a("a481"), a("cebc")),
                c = a("2b0e"), l = a("6612"), d = a.n(l), u = a("61f4"), p = a("2ee0"), _ = a("1585"), m = a("5670"),
                f = a("3fab"), g = a("fa7d"), h = a("2f62"), b = a("56d7"), v = a("6e77");
            t["a"] = {
                computed: Object(s["a"])({}, Object(h["mapGetters"])("user", {
                    globalUser: "user",
                    userLang: "userLang",
                    cAccountId: "accountId"
                }), Object(h["mapGetters"])("view", ["windowDimension"]), {
                    language: e.compose(g["w"], e.prop("globalUser")),
                    currencySymbol: function () {
                        return this.globalUser ? "" : this.globalUser.currency
                    },
                    isDemo: function () {
                        return this.globalUser && "DEMO" === this.globalUser.type
                    },
                    maybeKorean: function () {
                        var t = e.compose(e.toUpper, e.pathOr("", ["globalUser", "country"]))(this),
                            a = e.compose(e.toUpper, e.defaultTo(""), e.prop("userLang"))(this);
                        return "KR" === t || "KO" === a
                    }
                }),
                methods: Object(s["a"])({}, Object(h["mapMutations"])("view", ["setDemoAccountModalActive"]), {
                    isMobile: g["F"],
                    showDemoAccountModal: function () {
                        this.setDemoAccountModalActive(!0)
                    },
                    confirmBefore: function (e) {
                        var t, a = this, n = e.text, o = e.confirmText, i = e.cancelText, r = e.confirmCb,
                            s = void 0 === r ? function () {
                            } : r, l = e.cancelCb, d = void 0 === l ? function () {
                            } : l, p = e.className, _ = c["default"].extend(u["a"]), m = function () {
                                t.$destroy(!0), a.$el.removeChild(t.$el)
                            }, f = function () {
                                s(), m()
                            }, g = function () {
                                d(), m()
                            };
                        t = new _({
                            propsData: {
                                active: !0,
                                text: n,
                                confirmText: o,
                                cancelText: i,
                                className: p,
                                confirmCb: f,
                                cancelCb: g
                            }, i18n: b["i18n"]
                        }), t.$mount(), this.$el.appendChild(t.$el)
                    },
                    messageBefore: function (t) {
                        var a = c["default"].extend(p["a"]),
                            n = new a({propsData: e.merge({active: !0}, t), i18n: b["i18n"]}), o = function () {
                                n.$destroy(!0), n.$el.remove()
                            };
                        return n.$on("btnClicked", function () {
                            o(), Object(g["Z"])("cb", t)
                        }), n.$on("close", o), n.$mount(), this.$el.appendChild(n.$el), n
                    },
                    toast: function () {
                        var t;
                        return Object(v["a"])(document.body, "click").pipe(Object(_["a"])("target"), Object(m["a"])(function (a) {
                            var n = e.prop("$el", t || {});
                            do {
                                if (n === a) return !0;
                                a = a.parentNode
                            } while (a && n);
                            return !1
                        })).subscribe(function () {
                            return Object(g["Z"])("$el.remove", t)
                        }), function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "message",
                                a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "is-top",
                                n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "adriel-warning",
                                o = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : 2e3;
                            Object(g["Z"])("$el.remove", t), t = this.$toast.open({
                                duration: o,
                                message: e,
                                position: a,
                                type: "adriel-toast " + n,
                                queue: !1
                            })
                        }
                    }(),
                    openHelp: function () {
                        window.open("/#/helpCampaign", "_blank")
                    },
                    blurElement: function () {
                        Object(g["Z"])("activeElement.blur", document)
                    },
                    getBeforeDestroy$: function () {
                        return this.$eventToObservable("hook:beforeDestroy").pipe(Object(f["a"])())
                    },
                    setLang: function (t, a) {
                        if (a) {
                            var n = this.$route.query;
                            this.$router.replace({name: a, query: e.assoc("lang", t, n)})
                        }
                        Object(g["ab"])("lang", t), this.$i18n.locale = t, this.$validator.localize(t)
                    },
                    isLangType: function (e) {
                        return this.userLang === e
                    },
                    pushToRoute: function (e) {
                        e && this.$router.push(e)
                    },
                    toFixed: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
                        return Number(e).toFixed(t)
                    },
                    formatCurrency: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                        if (t.toFixed && (e = this.toFixed(e, t.toFixed)), !this.globalUser) return e;
                        switch (e = d()(e).format("(0.00a)"), e = r()(Number(e)) ? o()(e) : e, this.globalUser.currency) {
                            case"USD":
                                return "$ " + e;
                            case"GBP":
                                return "짙 " + e;
                            case"EUR":
                                return e + " ��";
                            default:
                                return e
                        }
                    },
                    getCurrencySymbol: function () {
                        switch (e.pathOr("USD", ["globalUser", "currency"], this)) {
                            case"USD":
                                return "$";
                            case"GBP":
                                return "짙";
                            case"EUR":
                                return "��";
                            default:
                                return "$"
                        }
                    },
                    formatNumber: g["n"],
                    queryStringThenRemove: function (t) {
                        var a = e.path(["$route", "query"], this);
                        return this.$router.replace({query: e.omit([t], a)}), e.prop(t, a)
                    },
                    inputEmitter: function (e) {
                        return this.$emit("input", e)
                    },
                    changeAccount: function (t) {
                        var a = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], n = this.$route;
                        if (t != this.cAccountId) {
                            var o = this.$router.resolve({
                                path: a ? n.path : "/myCampaigns",
                                query: e.assoc("accountId", t, n.query)
                            }), i = o.href;
                            window.open(i, "_blank")
                        }
                    }
                })
            }
        }).call(this, a("b17e"))
    }, 6953: function (e, t, a) {
    }, "6eab": function (e, t, a) {
        "use strict";
        var n = a("0a50"), o = a.n(n);
        o.a
    }, "6ebf": function (e, t) {
        e.exports = {
            en: {
                billing_faq: "Billing FAQ",
                when_will_i_first_be_billed: "When will I first be billed?",
                when_will_i_first_be_billed_answer: "\n            You only pay when someone clicks on your ad. We'll charge your card automatically when your ads receive clicks.\n            <br>Upon launching a\n        "
            }, ko: {}
        }
    }, "6f9a": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {
                    staticClass: "campaign-left",
                    class: [e.status, {highlighted: e.remainsLittle}]
                }, [a("span", {staticClass: "campaign-left-top"}, [a("img", {attrs: {src: "/img/dashboard/day_icon.png"}}), e.deactivated ? [a("div", {
                    domProps: {
                        innerHTML: e._s(e.$t("DASHBOARD.ran_for", {
                            day: e.daysRan,
                            str: e.daysStr
                        }))
                    }
                })] : ["startEnd" === e.status ? a("div", {domProps: {innerHTML: e._s(e.$t("DASHBOARD.remain_for", {day: e.daysLeft}))}}) : e._e(), "finalDay" === e.status ? a("span", {
                    staticClass: "campaign-left-top-text",
                    domProps: {textContent: e._s(e.$t("DASHBOARD.last_day"))}
                }) : e._e(), "continuously" === e.status ? a("div", {
                    domProps: {
                        innerHTML: e._s(e.$t("DASHBOARD.running_for", {
                            day: e.daysRan,
                            str: e.daysStr
                        }))
                    }
                }) : e._e(), "scheduled" === e.status ? a("div", {
                    domProps: {
                        innerHTML: e._s(e.$t("DASHBOARD.scheduled_for", {
                            day: e.daysLeftBeforeStart,
                            str: e.scheduledStr
                        }))
                    }
                }) : e._e()]], 2), a("span", {staticClass: "campaign-left-bottom"}, [a("span", {
                    staticClass: "bar-graph",
                    style: e.leftStyle
                })]), a("span", {staticClass: "start-end"}, [a("span", [e._v(e._s(e.startDateStr))]), a("span", [e._v(e._s(e.endDateStr))])]), e._t("bottom")], 2)
            }, o = [], i = a("1836"), r = i["a"], s = (a("739e"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, null, null);
        t["a"] = c.exports
    }, "707a": function (e, t, a) {
    }, "708a": function (e, t, a) {
        "use strict";
        var n = a("8cf7"), o = a.n(n);
        o.a
    }, 7143: function (e, t) {
        e.exports = {
            en: {
                ad_landing_url_placeholder: "Enter Landing URL",
                ad_types_type_appleSearch: "App Store",
                ad_types_type_facebookFeed: "Facebook",
                ad_types_type_googleDisplay: "Google display",
                ad_types_type_googleSearch: "Google search",
                ad_types_type_googleUniversalApp: "Google Universal App",
                ad_types_type_instagramFeed: "Instagram",
                ad_validation_invalid: "Invalid URL",
                ad_validation_missing: "Required",
                add_budget_text: "Get more customers by increasing your daily budget. Adriel AI will automatically allocate the budget on your best-performing ad(s).",
                add_payment_method: "Add a payment method",
                ads: "Ads",
                ads_add_slide: "Add slides",
                ads_crop_image: "Crop image",
                ads_delete_slide: "Delete slide",
                ads_selection_text: "Where do you want your ads to appear?",
                amount: "Amount",
                and: "and",
                appstore_search_failed: "We couldn�셳 find this app.&nbsp;<a>Please insert the app URL</a>",
                appstore_search_placeholder: "Search for an app on the Appstore or enter the app URL",
                audience_interests: "Audience interests",
                call_to_action_title: "Call to action",
                campaign_id: "Campaign ID",
                campaign_title: "Campaign Title",
                campaign_update_title: "Change the title of your campaign",
                cancel: "Cancel",
                channel_facebook: "Facebook",
                channel_google: "Google",
                channel_instagram: "Instagram",
                close: "Close",
                confirm_remove_ad: "Do you want to delete the {channel} ad?",
                connect_apple_app: "Connect your app from the Apple App Store",
                connect_apple_app_start: "Start Connecting",
                copied: "Copied",
                copy: "Copy",
                copy_from_facebook: "Copy Facebook ad",
                create: "Create",
                cta_fb_APPLY_NOW: "apply now",
                cta_fb_BOOK_TRAVEL: "book now",
                cta_fb_CONTACT_US: "contact us",
                cta_fb_DONATE: "donate",
                cta_fb_DOWNLOAD: "download",
                cta_fb_GET_OFFER: "get offer",
                cta_fb_GET_QUOTE: "get quote",
                cta_fb_GET_SHOWTIMES: "get showtimes",
                cta_fb_INSTALL_APP: "install app",
                cta_fb_LEARN_MORE: "learn more",
                cta_fb_LISTEN_NOW: "listen now",
                cta_fb_NO_BUTTON: "no button",
                cta_fb_ORDER_NOW: "order now",
                cta_fb_PLAY_GAME: "play game",
                cta_fb_SHOP_NOW: "shop now",
                cta_fb_SIGN_UP: "sign up",
                cta_fb_SUBSCRIBE: "subscribe",
                cta_fb_USE_APP: "use app",
                cta_fb_WATCH_MORE: "watch more",
                cta_fb_WATCH_VIDEO: "watch video",
                cta_google_APPLY_NOW: "Apply Now",
                cta_google_BOOK_NOW: "Book Now",
                cta_google_CONTACT_US: "Contact Us",
                cta_google_DOWNLOAD: "Download",
                cta_google_GET_QUOTE: "Get Quote",
                cta_google_INSTALL: "Install",
                cta_google_LEARN_MORE: "Learn More",
                cta_google_SEE_MORE: "See More",
                cta_google_SHOP_NOW: "Shop Now",
                cta_google_SIGN_UP: "Sign Up",
                cta_google_SUBSCRIBE: "Subscribe",
                cta_google_VISIT_SITE: "Visit Site",
                date: "Date",
                delete: "Delete",
                do_later: "Later",
                done: "Done",
                duplicate: "Duplicate ad",
                edit: "Edit",
                enter_url: "Enter URL",
                extend_budget_campaign: "Add {budget}/days",
                extend_budget_campaign_custom_1: "Add {symbol}",
                extend_budget_campaign_custom_2: "day",
                extend_campaign: "Add {days} days",
                extend_campaign_custom_1: "Add",
                extend_campaign_custom_2: "day",
                facebook_carousel_description_placeholder: "Enter description (Optional)",
                facebook_feed_description_placeholder: "Enter text message",
                facebook_feed_headline_placeholder: "Enter headline (Optional)",
                facebook_feed_link_description_placeholder: "Enter description (Optional)",
                fb_app_box_content: "We recommend connecting your Facebook App to <b>maximize the number of app installs.</b> With this information, Adriel can optimize your campaign for app installation and track the results. ",
                fb_app_how_to: [{
                    image: "/img/common/fb_app_img_1_en.png",
                    title: "COMMON.fb_app_how_to_title_0"
                }, {image: "/img/common/fb_app_img_2_en.png", title: "COMMON.fb_app_how_to_title_1"}],
                fb_app_how_to_title_0: 'When you click the following link, <a href="{link}" target="_blank">{link}</a> you will be redirected to the Facebook page where you can share your app with Adriel. (Please make sure you are logged in to Facebook first.) Click the app you would like share, then click this button.',
                fb_app_how_to_title_1: 'When a pop-up appears, put in <span class="copy-target"><b>525463574501100</b><span class="copy">COPY</span></span> to Business ID. Turn on the App analytics user and click next. Now that you\'re finished, Adriel can optimize your ads for app installation and track the performance.',
                fb_app_modal_confirm_sharing: "You haven't shared your Facebook app with us. Are you sure you want to leave this page without sharing Facebook app?",
                fb_app_modal_header: "Share Facebook App",
                fb_app_modal_need_auth: "To run Facebook App download ads, Adriel needs to have information about your app. Please log in to Facebook by checking the button below.",
                fb_app_modal_no_apps_found: "We haven't detected any App associated with your Facebook account.",
                fb_app_modal_reason_no_auth: "We haven't detected any App associated with your Facebook account. Please follow the link to share your app with Adriel.",
                fb_app_modal_reason_no_auth_url: "https://intercom.help/adriel/en/articles/3121950-",
                fb_app_modal_reason_no_tracker: "This app does not satisfy the condition(s) required for app ads. Please check the following link to install Facebook app events and add your Facebook app to the ad platforms.",
                fb_app_modal_reason_no_tracker_url: "https://developers.facebook.com/docs/app-ads/",
                fb_app_modal_select_desc: "Please select the App that you want to share",
                fb_share_app: "Share App",
                file_format: "File format",
                gender_men: "Men",
                gender_women: "Women",
                generate_empty_campaign: "We're trying our best to make your campaign perfect! \n Please give us a moment.",
                google_display_business_name_placeholder: "Enter a business name",
                google_display_description_placeholder: "Enter a description",
                google_display_title_placeholder: "Enter a title",
                instagram_carousel_title_placeholder: "Enter headline (Optional)",
                invalid_app_store_url: "Invalid App Store URL",
                invalid_play_store_url: "Invalid Play Store URL",
                invalid_url: "Invalid URL",
                log_in_to_fb: "Connect Facebook Page",
                logout_confirm: "Are you sure you want to log out?",
                min_size: "Min. size",
                newfeed: "News Feed",
                next: "Next",
                ok: "OK",
                optional: "Optional",
                payment_method: "Payment method",
                playstore_search_placeholder: "Search for an app on the Playstore or enter the app URL",
                proposal: "Proposal",
                publish: "Publish",
                remove: "Remove",
                required: "Required",
                save: "Save",
                search: "Search",
                search_keywords: "Search keywords",
                select: "Select",
                status: "Status",
                submit: "Submit",
                yes: "Yes",
                no_url: "This field is not a valid URL."
            },
            jp: {
                ad_landing_url_placeholder: "URL",
                ad_types_type_appleSearch: "App Store",
                ad_types_type_facebookFeed: "�뺛궒�ㅳ궧�뽧긿��",
                ad_types_type_googleDisplay: "�겹꺖�겹꺂�뉎궍�밤깤�с궎",
                ad_types_type_googleSearch: "�겹꺖�겹꺂濾쒐뇨",
                ad_types_type_googleUniversalApp: "Google Universal App",
                ad_types_type_instagramFeed: "�ㅳ꺍�밤궭�겹꺀��",
                ad_validation_invalid: "�▼듅�ゃ궋�됥꺃�밤겎��",
                ad_validation_missing: "恙낂젅�끿쎅�㎯걲",
                add_budget_text: "訝��ε퐪�잆굤��틛嶸쀣굮罌쀣굜�쀣겍�곥궖�밤궭�욁꺖�믡굚�ｃ겏�꿨풓�쀣겲�쀣굟�녴�귙궋�됥꺁�ⓦ꺂�췆I�뚣궘�ｃ꺍�싥꺖�녈굮���⒴뙑�쇻굥�잆굙�ヨ눎�뺟쉪�ヨ옙�졽틛嶸쀣굮�꿔굤壤볝겍�얇걲��",
                add_payment_method: "�딀뵱�뺛걚�방퀡�믣뒥�덀굥",
                ads: "佯껃몜",
                ads_add_slide: "�밤꺀�ㅳ깋瓦썲뒥",
                ads_crop_image: "�ㅳ깳�쇈궦�ャ긿��",
                ads_delete_slide: "�밤꺀�ㅳ깋�딃솮",
                amount: "�묌죲t",
                and: "�㏂꺍��",
                audience_interests: "�욍꺖�꿔긿�덆¨若�뼟恙껂틟",
                campaign_id: "��깵�녈깪�쇈꺍 ID",
                campaign_title: "��깵�녈깪�쇈꺍�욍궎�덀꺂",
                campaign_update_title: "��깵�녈깪�쇈꺍�욍궎�덀꺂�믣쨯�덀굥",
                cancel: "��깵�녈궩��",
                close: "�됥걯��",
                copied: "�녈깞��",
                copy_from_facebook: "Facebook佯껃몜�녈깞��",
                date: "�δ퍡",
                done: "永귚틙",
                edit: "渶③썓",
                extend_budget_campaign: "瓦썲뒥 {budget}/訝���",
                extend_budget_campaign_custom_1: "瓦썲뒥 {symbol}",
                extend_budget_campaign_custom_2: "��",
                extend_campaign: "Add {days} days",
                extend_campaign_custom_1: "�졼걟��",
                extend_campaign_custom_2: "��",
                facebook_carousel_description_placeholder: "沃ф삇�ε뒟(�멩뒢)",
                facebook_feed_description_placeholder: "佯껃몜�녴궘�밤깉�ε뒟(�멩뒢)",
                facebook_feed_headline_placeholder: "�욍궎�덀꺂�ε뒟(�멩뒢)",
                facebook_feed_link_description_placeholder: "沃ф삇�ε뒟(�멩뒢)",
                fb_app_box_content: "佯껃몜�먩옖璵드ㄷ�뽧겗�잆굙�ュ떑也ⓦ걮�얇걲�� �㏂깤�ゃ궞�쇈궥�㎯꺍佯껃몜�믦Þ營�걲�뗣겗�ユ��⒴뙑�쀣걼��,�㏂깤�よÞ營�굮歷у츣�듽굠�녘옙瓮▲걮�잆굤�쇻굥�볝겏�뚣겎�띲겲�쇻��",
                fb_app_how_to: [{
                    image: "/img/common/fb_app_img_1_ko.png",
                    title: "COMMON.fb_app_how_to_title_0"
                }, {image: "/img/common/fb_app_img_2_ko.png", title: "COMMON.fb_app_how_to_title_1"}],
                fb_app_how_to_title_0: '轝▲겗�ゃ꺍��굮��꺁�껁궚�쇻굥��,Adriel�ャ궋�쀣꺁�믣뀻�됥겎�띲굥Facebook�삯씊�ャ겇�ゃ걣�듽겲�쇻�� (Facebook�ャ꺆�겹궎�녈걬�뚣겍�꾠굥�뗧▶沃띲걮�╉걦�졼걬�꾠��) 轝▲겗�싥꺖�멥겎�길쐣�쀣굠�녴겏�쇻굥�㏂깤�ゃ굮�멩뒢�쀣걼孃�,"�묆꺖�덀깏�쇔돯�듿퐪��"�쒌궭�녈굮�쇈걮�╉걦�졼걬�꾠�� ',
                fb_app_how_to_title_1: '�앫긿�쀣궋�껁깤�뚨뤎�뤵굦�뚣겙�곥깛�멥깓�툶D�� <span class="copy-target"><b>525463574501100</b><span class="copy">�녈깞�쇈걲��</span></span>�볝겏�믣뀯�쎼걮�╉�곥궋�쀣꺁�녷옄�╉꺖�뜰굮ON�ヨÞ若싥걮�╉걦�졼걬�ｃ겍轝▲겗�쒌궭�녈굮�쇈걮�╉걦�졼걬�뚣겙�쇻겧�╉겗�뗧텥�띲걣若뚥틙�뺛굦�얇걲�� �볝겗�롧쮮��,�㏂깤�ゅ틕�딂Þ營�겓���⒴뙑�쀣걼��,�㏂깤�よÞ營�굮歷у츣�듽굠�녘옙瓮▲걮�잆굤�쇻굥�볝겏�뚣겎�띲겲�쇻��',
                fb_app_modal_confirm_sharing: "Facebook�㏂깤�ゃ겗�길쐣�뚦츑雅녴걮�╉걚�얇걵�볝�� �앫굦�㎯굚�볝겗囹볝굮�됥굙�얇걲��",
                fb_app_modal_header: "Facebook�㏂깤�ゃ겗�길쐣",
                fb_app_modal_need_auth: "Facebook�㏂깤�ゃ��╉꺍��꺖�됧틕�듽굮�룩죱�쇻굥�잆굙�ャ겘Adriel�ャ궋�쀣꺁�ュ��쇻굥�끻젿�뚦퓚誤곥겎�쇻�� 訝뗣겗�쒌궭�녈굮��꺁�껁궚�쀣겍Facebook�ャ꺆�겹궎�녈걮�╉걦�졼걬�꾠��",
                fb_app_modal_no_apps_found: "Facebook�㏂궖�╉꺍�덀겏�ㅳ겒�뚣겂�╉걚�뗣궋�쀣꺁�믥쇇誤뗣겎�띲겲�쎼굯�㎯걮�잆��",
                fb_app_modal_reason_no_auth: "Facebook�㏂궖�╉꺍�덀겏�ㅳ겒�뚣겂�╉걚�뗣궋�쀣꺁�뚨▶沃띲걬�뚣겲�쎼굯�� �㏂깤�ゃ궋��궩�방Ł�먦겗�길쐣��걼��,轝▲겗�ゃ꺍��굮閻븃첀�쀣겍�뤵걽�뺛걚��",
                fb_app_modal_reason_no_auth_url: "https://intercom.help/adriel/en/articles/3121954-",
                fb_app_modal_reason_no_tracker: "�㏂깤�よÞ營�틕�듽겗�잆굙��궋�쀣꺁鼇�츣�뚦츑雅녴걮�╉걚�얇걵�볝�� 訝뗨쮼��꺁�녈궚�믥▶沃띲걮�╉걚�잆걽��,Facebook �㏂깤�ゃ궎�쇻꺍�덀굮鼇�쉰�뺛굦�잆걢,佯껃몜�쀣꺀�껁깉�뺛궔�쇈깲�첛acebook �㏂깤�ゃ걣�삯뙯�뺛굦�╉걚�뗣걢閻븃첀�쀣겍�뤵걽�뺛걚��",
                fb_app_modal_reason_no_tracker_url: "https://developers.facebook.com/docs/app-ads/",
                fb_app_modal_select_desc: "�길쐣�쇻굥�㏂깤�ゃ굮�멥굯�㎯걦�졼걬�꾠��",
                fb_share_app: "�㏂깤�ゅ뀻��",
                file_format: "�뺛궊�ㅳ꺂�뺛궔�쇈깯�껁깉",
                gender_men: "�룡��",
                gender_women: "也녔��",
                google_display_business_name_placeholder: "�볝궦�띲궧�띶뀯��",
                google_display_description_placeholder: "佯껃몜�녴궘�밤깉�ε뒟",
                google_display_title_placeholder: "佯껃몜�녴궘�밤깉�ε뒟",
                instagram_carousel_title_placeholder: "佯껃몜�녴궘�밤깉�ε뒟 (�ゃ깤�룔깾��)",
                invalid_app_store_url: "�됧듅�㎯겒�꾠궋�쀣꺁�밤깉�㏂꺁�녈궚�㎯걲��",
                invalid_play_store_url: "�됧듅�㎯겒�꾠깤�с궎�밤깉�㏂꺁�녈궚�㎯걲��",
                invalid_url: "�됧듅�㎯겒�꾡퐦���㎯걲",
                logout_confirm: "��궛�㏂궑�덀걮�╉굚�덀굧�쀣걚�㎯걲��?",
                min_size: "�잆깑�욁깲�듐궎��",
                newfeed: "�뗣깷�쇈궧�뺛궍�쇈깋",
                next: "轝�",
                ok: "OK",
                optional: "餓삥꼷��",
                payment_method: "�딀뵱�뺛걚�방퀡",
                proposal: "�녈걮渦쇈겳",
                publish: "�븃죱�쇻굥",
                remove: "�뽧굤�ㅳ걦",
                required: "恙낂젅��",
                save: "岳앭춼",
                search_keywords: "濾쒐뇨��꺖��꺖�� ",
                status: "�밤깇�쇈궭��",
                submit: "�뺟㉮",
                yes: "��걚"
            },
            ko: {
                ad_landing_url_placeholder: "�쒕뵫 URL",
                ad_types_type_appleSearch: "�깆뒪�좎뼱",
                ad_types_type_facebookFeed: "�섏씠�ㅻ턿",
                ad_types_type_googleDisplay: "援ш� �붿뒪�뚮젅��",
                ad_types_type_googleSearch: "援ш� 寃���",
                ad_types_type_googleUniversalApp: "援ш� �좊땲踰꾩꽕 ��",
                ad_types_type_instagramFeed: "�몄뒪��洹몃옩",
                ad_validation_invalid: "�좏슚�섏� �딆� 二쇱냼�낅땲��",
                ad_validation_missing: "�꾩닔��ぉ�낅땲��",
                add_budget_text: "珥� �쇱씪 �덉궛�� 湲덉븸�� 異붽��섏뿬 �� 留롮� 怨좉컼�� 紐⑥쭛�� 蹂댁꽭��. �멸났吏��μ씠 �먮룞�쇰줈 �깃낵媛� �믪� 愿묎퀬�뚮옯�쇱뿉 諛곕텇�⑸땲��.\n (諛섏쁺�� �쒖젏遺��� �곸슜�⑸땲��)",
                add_payment_method: "寃곗젣 �섎떒 異붽��섍린",
                ads: "愿묎퀬",
                ads_add_slide: "�щ씪�대뱶 異붽�",
                ads_crop_image: "�대�吏� �먮Ⅴ湲�",
                ads_delete_slide: "�щ씪�대뱶 ��젣",
                ads_selection_text: "�먰븯�쒕뒗 愿묎퀬 �뚮옯�쇱쓣 �좏깮�댁＜�몄슂.",
                amount: "湲덉븸",
                and: "洹몃━怨�",
                appstore_search_failed: "寃��됲븯�� �깆쓣 李얠쓣 �� �놁뒿�덈떎.&nbsp<a>�� �ㅼ슫濡쒕뱶 URL�� 吏곸젒 �낅젰�댁＜�몄슂.</a>",
                appstore_search_placeholder: "�깆뒪�좎뼱 �� 寃��� �뱀� �� URL �낅젰",
                audience_interests: "��寃� 怨좉컼 愿��ъ궗",
                call_to_action_title: "�됰룞 �좊룄",
                campaign_id: "罹좏럹�� �꾩씠��",
                campaign_title: "愿묎퀬 �쒕ぉ",
                campaign_update_title: "愿묎퀬 �쒕ぉ 諛붽씀湲�",
                cancel: "痍⑥냼",
                channel_facebook: "�섏씠�ㅻ턿",
                channel_google: "援ш�",
                channel_instagram: "�몄뒪��洹몃옩",
                close: "�リ린",
                confirm_remove_ad: "{channel} 愿묎퀬瑜� ��젣�섏떆寃좎뒿�덇퉴?",
                connect_apple_app: "�깆뒪�좎뼱 �깆쓣 �곌껐�댁빞 �⑸땲��.",
                connect_apple_app_start: "�쒖옉�섍린",
                copied: "蹂듭궗�섏뿀�듬땲��",
                copy: "蹂듭궗",
                copy_from_facebook: "�섏씠�ㅻ턿 愿묎퀬 蹂듭궗",
                create: "留뚮뱾湲�",
                cta_fb_APPLY_NOW: "吏�湲� �좎껌�섍린",
                cta_fb_BOOK_TRAVEL: "�덉빟�섍린",
                cta_fb_CONTACT_US: "臾몄쓽�섍린",
                cta_fb_DONATE: "湲곕��섍린",
                cta_fb_DOWNLOAD: "�ㅼ슫濡쒕뱶",
                cta_fb_GET_OFFER: "荑좏룿 諛쏄린",
                cta_fb_GET_QUOTE: "寃ъ쟻 諛쏄린",
                cta_fb_GET_SHOWTIMES: "�곸쁺 �쒓컙 蹂닿린",
                cta_fb_INSTALL_APP: "�� �ㅼ튂�섍린",
                cta_fb_LEARN_MORE: "�� �뚯븘蹂닿린",
                cta_fb_LISTEN_NOW: "吏�湲� �ｊ린",
                cta_fb_NO_BUTTON: "踰꾪듉 �놁쓬",
                cta_fb_ORDER_NOW: "�덉빟 �붿껌�섍린",
                cta_fb_PLAY_GAME: "寃뚯엫�섍린",
                cta_fb_SHOP_NOW: "吏�湲� 援щℓ�섍린",
                cta_fb_SIGN_UP: "媛��낇븯湲�",
                cta_fb_SUBSCRIBE: "援щ룆�섍린",
                cta_fb_USE_APP: "�� �ъ슜�섍린",
                cta_fb_WATCH_MORE: "�숈쁺�� �� 蹂닿린",
                cta_fb_WATCH_VIDEO: "�숈쁺�� 蹂닿린",
                cta_google_APPLY_NOW: "�곸슜�섍린",
                cta_google_BOOK_NOW: "�덉빟�섍린",
                cta_google_CONTACT_US: "臾몄쓽�섍린",
                cta_google_DOWNLOAD: "�ㅼ슫濡쒕뱶�섍린",
                cta_google_GET_QUOTE: "寃ъ쟻 諛쏄린",
                cta_google_INSTALL: "�ㅼ튂�섍린",
                cta_google_LEARN_MORE: "�먯꽭�� �뚯븘蹂닿린",
                cta_google_SEE_MORE: "�붾낫湲�",
                cta_google_SHOP_NOW: "援щℓ�섍린",
                cta_google_SIGN_UP: "媛��낇븯湲�",
                cta_google_SUBSCRIBE: "援щ룆�섍린",
                cta_google_VISIT_SITE: "�ъ씠�� 諛⑸Ц�섍린",
                date: "�좎쭨",
                delete: "��젣",
                do_later: "�섏쨷�� �섍린",
                done: "�꾨즺",
                duplicate: "蹂듭젣",
                edit: "�섏젙",
                enter_url: "URL 吏곸젒 �낅젰",
                extend_budget_campaign: "{budget}/�� 異붽�",
                extend_budget_campaign_custom_1: "{symbol}",
                extend_budget_campaign_custom_2: "/�� 異붽�",
                extend_campaign: "{days}�� 異붽�",
                extend_campaign_custom_1: "",
                extend_campaign_custom_2: "�� 異붽�",
                facebook_carousel_description_placeholder: "�ㅻ챸 �낅젰 (�좏깮)",
                facebook_feed_description_placeholder: "愿묎퀬 �띿뒪�� �낅젰 (�좏깮)",
                facebook_feed_headline_placeholder: "�쒕ぉ �낅젰 (�좏깮)",
                facebook_feed_link_description_placeholder: "�ㅻ챸 �낅젰 (�좏깮)",
                fb_app_box_content: " <b>愿묎퀬 �깃낵 洹밸���</b>瑜� �꾪빐 沅뚯옣 �쒕┰�덈떎. <br />�� 愿묎퀬瑜� �ㅼ튂�� 理쒖쟻�뷀븯嫄곕굹 �� �ㅼ튂瑜� 痢≪젙 諛� 異붿쟻�� �� �덉뒿�덈떎.",
                fb_app_how_to: [{
                    image: "/img/common/fb_app_img_1_ko.png",
                    title: "COMMON.fb_app_how_to_title_0"
                }, {image: "/img/common/fb_app_img_2_ko.png", title: "COMMON.fb_app_how_to_title_1"}],
                fb_app_how_to_title_0: '�ㅼ쓬 留곹겕 <a href="{link}" target="_blank">{link}</a>瑜� �대┃�섎㈃ �꾨뱶由ъ뿕�먭쾶 �깆쓣 怨듭쑀�� �� �덈뒗 �섏씠�ㅻ턿 �붾㈃ �쇰줈 �곌껐�⑸땲��. (�섏씠�ㅻ턿�� 濡쒓렇�몃릺�� �덈뒗吏� �뺤씤�댁＜�몄슂) �꾨옒 �섏씠吏��먯꽌 怨듭쑀�섍퀬�� �섎뒗 �깆쓣 �좏깮�섏떊 ��, "�뚰듃�� �좊떦" 踰꾪듉�� �뚮윭二쇱꽭��. ',
                fb_app_how_to_title_1: '�앹뾽 李쎌씠 �섑��섎㈃ 鍮꾩쫰�덉뒪 ID�� <span class="copy-target"><b>525463574501100</b><span class="copy">蹂듭궗�섍린</span></span>瑜� �낅젰�섏떆怨� �� 遺꾩꽍 �ъ슜�먮� ON�쇰줈 �ㅼ젙�댁＜�쒓퀬 �ㅼ쓬 踰꾪듉�� �뚮윭二쇱떆硫� 紐⑤뱺 �덉감媛� �꾨즺�⑸땲��. �� 怨쇱젙�� �� 愿묎퀬 �ㅼ튂�� 理쒖쟻�뷀븯嫄곕굹 �� �ㅼ튂瑜� 痢≪젙 諛� 異붿쟻�� �� �덉뒿�덈떎.',
                fb_app_modal_confirm_sharing: "�섏씠�ㅻ턿 �� 怨듭쑀媛� �꾨즺�섏� �딆븯�듬땲��. 洹몃옒�� �� 李쎌쓣 �レ쑝�쒓쿋�듬땲源�?",
                fb_app_modal_header: " Facekbook SDK �듯빀�섍린",
                fb_app_modal_need_auth: "�섏씠�ㅻ턿 �� �ㅼ슫濡쒕뱶 愿묎퀬瑜� 吏묓뻾�섍린 �꾪빐�쒕뒗 Adriel�� �깆뿉 ���� �뺣낫媛� �덉뼱�� �⑸땲��. �꾨옒 踰꾪듉�� �대┃�댁꽌 �섏씠�ㅻ턿�� 濡쒓렇�명빐二쇱꽭��.",
                fb_app_modal_no_apps_found: "Facebook 怨꾩젙怨� �곌껐�� �깆쓣 諛쒓껄�섏� 紐삵뻽�듬땲��.",
                fb_app_modal_reason_no_auth: "�섏씠�ㅻ턿 怨꾩젙怨� �곌껐�� �깆씠 �뺤씤�섏� �딆뒿�덈떎. �� �묎렐 沅뚰븳 怨듭쑀瑜� �꾪빐 �ㅼ쓬 留곹겕瑜� �뺤씤�댁＜�몄슂.",
                fb_app_modal_reason_no_auth_url: "https://intercom.help/adriel/en/articles/3121954-",
                fb_app_modal_reason_no_tracker: "�깆꽕移� 愿묎퀬瑜� �꾪븳 �� �ㅼ젙�� �꾨즺�섏� �딆븯�듬땲��. �꾨옒 留곹겕瑜� �뺤씤�섏떆�� �섏씠�ㅻ턿 �� �대깽�� �ㅼ튂媛� �섏뿀�붿�, 愿묎퀬 �뚮옯�쇱뿉 �섏씠�ㅻ턿 �깆씠 �깅줉�섏뼱 �덈뒗吏� �뺤씤�댁＜�몄슂.",
                fb_app_modal_reason_no_tracker_url: "https://developers.facebook.com/docs/app-ads/",
                fb_app_modal_select_desc: "怨듭쑀 �� �깆쓣 �좏깮�섏떗�쒖삤.",
                fb_share_app: "�� 怨듭쑀",
                file_format: "�뚯씪 �뺤떇",
                gender_men: "�⑥꽦",
                gender_women: "�ъ꽦",
                generate_empty_campaign: "�붿슧 硫뗭쭊 愿묎퀬瑜� 留뚮뱾湲� �꾪빐 �곗씠�곕� �섏쭛 以묒엯�덈떎. \n議곌툑留� 湲곕떎�� 二쇱꽭��.",
                google_display_business_name_placeholder: "�곹샇紐� �낅젰",
                google_display_description_placeholder: "愿묎퀬 �띿뒪�� �낅젰",
                google_display_title_placeholder: "愿묎퀬 �쒕ぉ �낅젰",
                instagram_carousel_title_placeholder: "愿묎퀬 �쒕ぉ �낅젰 (�듭뀡)",
                invalid_app_store_url: "�좏슚�섏� �딆� �� �ㅽ넗�� 留곹겕�낅땲��",
                invalid_play_store_url: "�좏슚�섏� �딆� �뚮젅�� �ㅽ넗�� 留곹겕�낅땲��",
                invalid_url: "�좏슚�섏� �딆� 二쇱냼�낅땲��",
                log_in_to_fb: "�섏씠�ㅻ턿 �섏씠吏� �곌껐",
                logout_confirm: "濡쒓렇�꾩썐 �섏떆寃좎뒿�덇퉴?",
                min_size: "理쒖냼 �ш린",
                newfeed: "�댁뒪�쇰뱶",
                next: "�ㅼ쓬",
                ok: "�뺤씤",
                optional: "�좏깮",
                payment_method: "寃곗젣 �섎떒",
                playstore_search_placeholder: "�뚮젅�댁뒪�좎뼱 �� 寃��� �뱀� �� URL �낅젰",
                proposal: "愿묎퀬 �쒖븞",
                publish: "諛섏쁺",
                remove: "��젣",
                required: "�꾩닔",
                save: "����",
                search: "寃���",
                search_keywords: "寃��� �ㅼ썙��",
                select: "�좏깮",
                status: "�곹깭",
                submit: "�쒖텧",
                yes: "��",
                no_url: "�섎せ�� URL�� �낅젰�섏뿀�듬땲��."
            }
        }
    }, "739d": function (e, t, a) {
        "use strict";
        var n = function () {
            var e = this, t = e.$createElement, a = e._self._c || t;
            return a("div", {
                staticClass: "demo-fab", on: {
                    click: function (t) {
                        return e.$emit("confirm")
                    }
                }
            }, [a("img", {
                staticClass: "gif",
                attrs: {src: "/img/demo/ads_ani.gif", alt: ""}
            }), a("span", {staticClass: "demo-fab-text"}, [a("p", [e._v(e._s(e.$t("DEMO.want_to_use_full_version")))]), a("a", {attrs: {href: "#"}}, [e._v("\n            " + e._s(e.$t("DEMO.sign_up_and_start")) + "\n            "), a("img", {
                attrs: {
                    src: "/img/demo/next_btn.png",
                    alt: "",
                    width: "12px"
                }
            })])])])
        }, o = [], i = {
            name: "DemoFAB", methods: {
                clickSignUpFromDemo: function () {
                    this.setDemoAccountModalActive(!1), this.logout(), this.$router.push({name: "Sign/Join"})
                }
            }
        }, r = i, s = (a("b965"), a("2877")), c = Object(s["a"])(r, n, o, !1, null, null, null);
        t["a"] = c.exports
    }, "739e": function (e, t, a) {
        "use strict";
        var n = a("d807"), o = a.n(n);
        o.a
    }, 7549: function (e, t, a) {
    }, "75c1": function (e, t, a) {
        "use strict";
        (function (e) {
            a("ac6a"), a("96cf");
            var n = a("3b8d"), o = (a("7514"), a("cebc")), i = (a("6762"), a("2fdb"), a("a481"), a("5176")), r = a.n(i),
                s = (a("c5f6"), a("2f62")), c = a("4e4d"), l = a("b183"), d = a("608d"), u = a("fe51"), p = a("3537"),
                _ = a("ef2e"), m = a("9153"), f = a("c384"), g = a("4b4c"), h = a("3d62"), b = a("dde5"), v = a("c1df"),
                y = a.n(v), w = a("fa7d"), k = e.is(Number),
                x = e.compose(e.ifElse(isNaN, e.always("-"), e.max(0)), Object(w["Q"])(1), e.divide(e.__, 864e5)),
                O = function (t) {
                    var a = e.path(["proposal", "schedule", "endDate"])(t);
                    if (a) return x(y()(a).endOf("day").toDate() - new Date)
                }, A = {TABLE: "TABLE", CARD: "CARD"}, C = {layout: A.TABLE, size: 10, page: 0},
                S = {layout: A.CARD, limit: 8, offset: 0, isNewData: !0};
            t["a"] = {
                name: "MyCampaigns",
                mounted: function () {
                    var t = this.$route.query.signup, a = r()({}, this.$route.query);
                    delete a.signup, this.$router.replace({query: a}), ["google", "facebook"].includes(t) && e.pathEq(["globalUser", "country"], "GB", this) && ire("trackConversion", 17087, {
                        orderId: "signUp::".concat(t, "::").concat(this.globalUser.id),
                        customerId: this.globalUser.id
                    })
                },
                components: {
                    "left-nav": c["a"],
                    "top-title": d["a"],
                    "update-title": u["a"],
                    "mobile-nav": l["a"],
                    ExtendDaysModal: g["a"],
                    ExtendBudgetModal: h["a"],
                    StopSurvey: f["a"],
                    CampaignList: _["a"],
                    CampaignBoxWrapper: p["a"],
                    EmptyCampaign: m["a"]
                },
                data: function () {
                    return {
                        confirmModaltext: "",
                        confirmModalConfirmText: null,
                        confirmModalType: "ensureCopy",
                        isConfirmModalVisible: !1,
                        isExtendDaysModalActive: !1,
                        isExtendBudgetModalActive: !1,
                        isSurveyModalActive: !1,
                        messageModal: null,
                        editingCampaign: null,
                        campaignId: null,
                        campaignsIds: [],
                        messageModalType: "goToSnB",
                        extendTarget: null,
                        campaignListType: A.CARD,
                        listDefaultParams: C,
                        cardDefaultParams: S,
                        listType: A
                    }
                },
                methods: Object(o["a"])({}, Object(s["mapActions"])("campaign", ["getCampaignsByList", "getCampaignsByCard", "copy", "delete", "deactivateCampaign", "extendCampaign"]), Object(s["mapActions"])("view", ["setLoadingAdriel"]), Object(s["mapMutations"])("view", ["setIsNewCampaignModalActive"]), Object(s["mapMutations"])("campaign", ["setCampaigns", "setCachedCampaigns"]), {
                    checkMessageResult: function () {
                        var t = e.equals(this.messageModalType);
                        t("goToSnB") && this.$router.push({name: "Proposal/Plan", params: {id: this.campaignId}})
                    }, showConfirmModal: function (e) {
                        var t = e.id, a = e.ids, n = e.text, o = e.type, i = e.confirmText;
                        this.campaignId = t, this.campaignIds = a, this.confirmModaltext = n, this.confirmModalType = o, this.confirmModalConfirmText = i, this.isConfirmModalVisible = !0
                    }, clearModal: function () {
                        var e = this,
                            t = ["isConfirmModalVisible", "isExtendDaysModalActive", "isExtendBudgetModalActive", "isSurveyModalActive", "editingCampaign", "messageModal"];
                        t.every(function (t) {
                            if (e.$data[t]) switch (t) {
                                case"messageModal":
                                    return e.messageModal.$emit("close"), !1;
                                case"editingCampaign":
                                    return e.$data[t] = null, !1;
                                default:
                                    return e.$data[t] = !1, !1
                            }
                            return !0
                        }), this.isNewCampaignModalActive && this.setIsNewCampaignModalActive(!1)
                    }, startChat: function () {
                        this.isDemo ? this.setDemoAccountModalActive(!0) : this.setIsNewCampaignModalActive(!0)
                    }, goToExpress: function () {
                        this.isDemo ? this.setDemoAccountModalActive(!0) : this.$router.push({name: "Express"})
                    }, ensureCopy: function (e) {
                        this.showConfirmModal({id: e, text: this.$t("MY_CAMPAIGNS.copy_confirm"), type: "ensureCopy"})
                    }, ensureDelete: function (e) {
                        var t = e.ids;
                        this.isDemo ? this.showDemoAccountModal() : this.showConfirmModal({
                            ids: t,
                            text: this.$t("MY_CAMPAIGNS.delete_confirm"),
                            type: "ensureDelete"
                        })
                    }, ensureToggleActive: function (e) {
                        var t = this, a = e.id, n = e.value, o = e.status;
                        if (n) this.showScheduleModal(a); else if ("running" === o) {
                            var i, r = this.$t("MY_CAMPAIGNS.deactivate_confirm"),
                                s = this.campaigns.find(function (e) {
                                    return e.id === a
                                });
                            if (s && s.started_at) {
                                var c = y()(s.started_at).add(3, "days").isAfter(y()());
                                c && (r = this.$t("MY_CAMPAIGNS.deactivate_confirm_warning"), i = this.$t("MY_CAMPAIGNS.stop_button"))
                            }
                            this.showConfirmModal({id: a, text: r, type: "ensureDeactivate", confirmText: i})
                        } else this.deactivateCampaign({
                            id: a, params: this.query, cb: function () {
                                return t.setDefaultPageAfterAction(t.query)
                            }
                        })
                    }, showTitleModal: function (t) {
                        this.editingCampaign = e.find(e.propEq("id", t), this.campaigns)
                    }, extendCampaignDays: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        e = Number(e), e > 0 && k(e) && this.extendCampaign({id: this.extendTargetId, value: {days: e}})
                    }, extendCampaignBudget: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        e = Number(e), e > 0 && k(e) && this.extendCampaign({
                            id: this.extendTargetId,
                            value: {budget: e}
                        })
                    }, showScheduleModal: function (e) {
                        this.messageModal = this.messageBefore({
                            text: this.$t("MY_CAMPAIGNS.check_plan_confirm"),
                            btnText: this.$t("MY_CAMPAIGNS.go_to_snb"),
                            cb: function () {
                                this.pushToRoute({name: "Proposal/Plan", params: {id: e}})
                            }.bind(this)
                        })
                    }, showDaysModal: function (t) {
                        this.extendTarget = e.find(e.propEq("id", t), this.campaigns), this.isExtendDaysModalActive = !0
                    }, showBudgetModal: function (t) {
                        this.extendTarget = e.find(e.propEq("id", t), this.campaigns), this.isExtendBudgetModalActive = !0
                    }, submitStopSurvey: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        this.isSurveyModalActive = !1, b["l"].save(Object(o["a"])({campaign_id: this.campaignId}, e))
                    }, onClickListType: function (e) {
                        if (e !== this.campaignListType) {
                            this.campaignListType = e;
                            var t = this.currentDefaultParams(e);
                            this.getCampaignsByParams(t)
                        }
                    }, getCampaignsByParams: function () {
                        var e = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                            return regeneratorRuntime.wrap(function (e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        if (this.campaignListType !== A.TABLE) {
                                            e.next = 7;
                                            break
                                        }
                                        return e.next = 3, this.getCampaignsByList(t);
                                    case 3:
                                        this.$router.push({query: t}), this.setLoadingAdriel(!1), e.next = 11;
                                        break;
                                    case 7:
                                        return e.next = 9, this.getCampaignsByCard(t);
                                    case 9:
                                        this.$router.replace({query: t}), this.setLoadingAdriel(!1);
                                    case 11:
                                    case"end":
                                        return e.stop()
                                }
                            }, e, this)
                        }));

                        function t(t) {
                            return e.apply(this, arguments)
                        }

                        return t
                    }(), setDefaultPageAfterAction: function (e) {
                        this.campaignListType === A.TABLE ? (this.currentCheckedRows && this.$refs.list.setCheckedRows([]), this.$router.push({query: e}), this.$refs.list.setStatusByParmas(e)) : document.documentElement.scrollTop = 0
                    }, currentDefaultParams: function (e) {
                        return e === A.TABLE ? C : S
                    }, setPopStateEvent: function () {
                        this.campaignListType = this.queryListType, this.queryListType === A.TABLE ? (this.getCampaignsByParams(this.query), this.$refs.list.setStatusByParmas(this.query)) : (this.getCampaignsByParams(this.currentDefaultParams(A.CARD)), this.$refs.card.setStatusByParmas(this.query)), this.clearModal()
                    }
                }),
                computed: Object(o["a"])({}, Object(s["mapGetters"])("campaign", ["campaigns", "totalData", "isCardLoading", "countByTabs"]), Object(s["mapGetters"])("view", ["isNewCampaignModalActive"]), {
                    query: e.path(["$route", "query"]),
                    queryListType: e.path(["$route", "query", "layout"]),
                    currentListPage: e.path(["$refs", "list", "page"]),
                    currentCheckedRows: e.path(["$refs", "list", "checkedRows"]),
                    isRefresh: e.path(["$route", "meta", "isRefresh"]),
                    hasCampaigns: function () {
                        return this.campaigns.length > 0
                    },
                    confirmCb: function () {
                        var t = this;
                        return e.cond([[e.equals("ensureCopy"), e.always(function () {
                            var e = t.currentDefaultParams(t.campaignListType);
                            t.copy({
                                id: t.campaignId, params: e, cb: function () {
                                    return t.setDefaultPageAfterAction(e)
                                }
                            })
                        })], [e.equals("ensureDelete"), e.always(function () {
                            t.delete({
                                ids: t.campaignIds, params: t.query, cb: function () {
                                    return t.setDefaultPageAfterAction(t.query)
                                }
                            })
                        })], [e.equals("ensureDeactivate"), e.always(function () {
                            t.isSurveyModalActive = !0, t.deactivateCampaign({
                                id: t.campaignId,
                                params: t.query,
                                cb: function () {
                                    return t.setDefaultPageAfterAction(t.query)
                                }
                            })
                        })]])(this.confirmModalType)
                    },
                    btnText: function () {
                        return "review_message" === this.messageModalType ? this.$t("COMMON.ok") : this.$t("MY_CAMPAIGNS.go_to_snb")
                    },
                    propsToRunningStatus: e.compose(function (t) {
                        return Object(o["a"])({}, e.path(["proposal", "schedule"], t) || {}, {
                            startedAt: t.startedAt,
                            deactivated: !1,
                            deactivatedAt: t.deactivatedAt,
                            daysLeft: O(t)
                        })
                    }, e.defaultTo({}), e.prop("extendTarget")),
                    extendTargetId: e.compose(e.prop("id"), e.defaultTo({}), e.prop("extendTarget")),
                    currentSortingFilterStatus: function () {
                        return this.campaignListType === A.TABLE ? e.path(["$refs", "list", "sortAndPagingParams"], this) : e.path(["$refs", "card", "filterParams"], this)
                    },
                    paramOptionAfterAction: function () {
                        return this.campaignListType === A.TABLE ? Object(o["a"])({}, this.currentSortingFilterStatus, {page: this.currentListPage - 1}) : this.currentSortingFilterStatus
                    },
                    validTabs: e.compose(e.keys(), e.pickBy(function (e) {
                        return Number(e) > 0
                    }), e.prop("countByTabs")),
                    validTabsForTable: e.compose(e.append("all"), e.prop("validTabs")),
                    validTabsForCard: e.compose(e.prepend("all"), e.prop("validTabs"))
                }),
                created: function () {
                    this.setLoadingAdriel({
                        active: !0,
                        message: " "
                    }), (this.queryListType !== A.TABLE || this.isRefresh) && this.$router.replace({query: this.currentDefaultParams(this.queryListType || this.campaignListType)}), this.campaignListType = this.queryListType, this.getCampaignsByParams(this.query), window.addEventListener("popstate", this.setPopStateEvent)
                },
                destroyed: function () {
                    this.setCampaigns({data: []}), this.setCachedCampaigns({data: {}}), window.removeEventListener("popstate", this.setPopStateEvent)
                }
            }
        }).call(this, a("b17e"))
    }, "760d": function (e, t) {
        e.exports = {
            en: {
                account_setting: "User Setting",
                express: "Make Adriel do it",
                help: "Help",
                logout_tooltip: "Log out",
                my_campaigns: "My Campaigns",
                new_campaign: "New Campaign",
                newsfeed: "News Feed",
                manager: "My Manager"
            },
            jp: {
                account_setting: "�㏂궖�╉꺍�덅Þ若�",
                express: "鵝쒏닇�쇻굥",
                help: "�섅꺂��",
                logout_tooltip: "��궛�㏂궑��",
                my_campaigns: "�욁궎��깵�녈깪�쇈꺍",
                new_campaign: "�겹걮�꾠궘�ｃ꺍�싥꺖��",
                newsfeed: "�겹걮�꾠깢�ｃ꺖��"
            },
            ko: {
                account_setting: "�ъ슜�� �ㅼ젙",
                express: "�꾨뱶由ъ뿕�먭쾶 遺��곹븯湲�",
                help: "臾몄쓽�섍린",
                logout_tooltip: "濡쒓렇�꾩썐",
                my_campaigns: "�� 愿묎퀬",
                new_campaign: "�� 愿묎퀬 留뚮뱾湲�",
                newsfeed: "�댁뒪�쇰뱶",
                manager: "�� 留ㅻ땲��"
            }
        }
    }, 7781: function (e, t, a) {
        "use strict";
        (function (e) {
            var a = e.always(void 0);
            t["a"] = {
                props: {
                    text: {type: String},
                    active: {type: Boolean, default: !1},
                    confirmCb: {type: Function, default: a},
                    cancelCb: {type: Function, default: a},
                    confirmText: {type: String},
                    cancelText: {type: String},
                    className: String
                }, computed: {
                    _active: {
                        get: function () {
                            return this.active
                        }, set: function (e) {
                            this.$emit("update:active", e)
                        }
                    }, _confirmText: function () {
                        return this.confirmText || this.$t("COMMON.ok")
                    }, _cancelText: function () {
                        return this.cancelText || this.$t("COMMON.cancel")
                    }
                }, methods: {
                    confirm: function () {
                        this._active = !1, this.$emit("confirm"), this.confirmCb()
                    }, cancel: function () {
                        this._active = !1, this.$emit("cancel"), this.cancelCb()
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "778f": function (e, t, a) {
    }, "79c3": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("bd86"), o = a("a4bb"), i = a.n(o), r = a("5176"), s = a.n(r), c = a("2d1f"), l = a.n(c),
                d = a("768b"), u = (a("7f7f"), a("c5f6"), a("ac6a"), a("20d6"), a("cebc")), p = (a("7514"), a("dde5")),
                _ = a("fa7d"), m = a("c1df"), f = a.n(m),
                g = e.compose(e.max(0), Object(_["Q"])(1), e.divide(e.__, 864e5)), h = e.path(["data", "details"]),
                b = e.curry(function (t, a, n) {
                    return e.compose(e.prop("values"), e.find(e.propEq("key", t)), function (e) {
                        return e || []
                    }, a)(n)
                }), v = e.pathOr([], ["data", "dataAds"]), y = e.ifElse(isNaN, e.identity, Math.round),
                w = function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
                        t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], a = {
                            labels: e,
                            datasets: [{label: "facebook", backgroundColor: "#385c8e", data: []}, {
                                label: "instagram",
                                backgroundColor: "#7c64db",
                                data: []
                            }, {label: "google", backgroundColor: "#518ef8", data: []}, {
                                label: "apple",
                                backgroundColor: "#cdd0d1",
                                data: []
                            }]
                        };
                    return !0 === t && a.datasets.push({
                        label: "all",
                        type: "line",
                        fill: !1,
                        backgroundColor: "blue",
                        borderColor: "blue",
                        data: []
                    }), a
                }, k = e.propEq("label"), x = e.curry(function (t, a) {
                    return e.pathOr(0, [t, a])
                }), O = function (t, a, n) {
                    var o = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3], i = e.prop(a, t),
                        r = e.map(e.__, i), s = x(n);
                    return e.over(e.lensProp("datasets"), e.map(function (e) {
                        return Object(u["a"])({}, e, {data: r(s(e.label))})
                    }), w(e.pluck("date", i), o))
                }, A = function (t, a) {
                    var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "goalCompletion",
                        o = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3],
                        i = e.prop("conversions", t), r = e.map(e.__, i);
                    return e.over(e.lensProp("datasets"), e.map(function (t) {
                        return Object(u["a"])({}, t, {data: r(e.pathOr(0, ["conversion", t.label, a, n]))})
                    }), w(e.pluck("date", i), o))
                }, C = {BLUE: "#3087c3", ORANGE: "#f8a233", RED: "#e16464"}, S = function () {
                    return {data: {}, campaignId: null, conversionList: !1, selectedConversion: void 0, graph: null}
                };
            t["a"] = {
                namespaced: !0, state: S(), mutations: {
                    setReport: function (e, t) {
                        e.data = t
                    }, setReportGraph: function (e, t) {
                        e.graph = t
                    }, setCampaignId: function (e, t) {
                        e.campaignId = t
                    }, setCampaignTitle: function (e, t) {
                        t && (e.data.title = t)
                    }, setConversionList: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        e.conversionList = t
                    }, setSelectedConversion: function (e, t) {
                        e.selectedConversion = t
                    }
                }, actions: {
                    getReport: function (e, t) {
                        var a = e.commit, n = e.dispatch;
                        return a("setCampaignId", t), n("getConversionList", t), p["d"].getReport(t).then(function (e) {
                            return a("setReport", e)
                        }).catch(function () {
                            return a("setReport", {})
                        })
                    }, getReportGraph: function (e, t) {
                        var a = e.commit;
                        return p["d"].getReportGraph(t).then(function (e) {
                            return a("setReportGraph", e)
                        }).catch(function () {
                            return a("setReportGraph", null)
                        })
                    }, getConversionList: function (t, a) {
                        var n = t.commit;
                        p["d"].listConversions({campaignId: a}).then(function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            n("setConversionList", t), n("setSelectedConversion", e.find(e.propEq("selected", !0), t))
                        }, function () {
                            return n("setConversionList")
                        })
                    }, updateGa: function (t, a) {
                        t.commit;
                        var n = t.getters, o = t.dispatch, i = n.campaignId;
                        return p["d"].updateGa(i, a).then(function () {
                            return p["d"].get(i)
                        }).then(function (t) {
                            if (!t) throw new Error;
                            var n = e.compose(e.defaultTo([]), e.find(e.propEq("name", "websiteVisitors")), e.path(["proposal", "audienceProfile"]))(t),
                                o = e.findIndex(e.propEq("type", "googleAnalytics"), n.values);
                            return o > -1 && n.values.splice(o, 1), n.values.push(Object(u["a"])({type: "googleAnalytics"}, a.properties[0])), p["d"].update(i, t, "save=1")
                        }).then(function () {
                            return o("getReport", i)
                        }).catch(function (e) {
                            console.log(e)
                        })
                    }, extendCampaign: function (e, t) {
                        e.commit;
                        var a = e.getters, n = e.dispatch, o = a.campaignId;
                        return p["d"].extend(o, t).then(function () {
                            n("getReport", o)
                        })
                    }
                }, getters: {
                    loaded: function (t) {
                        return !e.isEmpty(t.data)
                    },
                    title: e.path(["data", "title"]),
                    isSample: e.path(["data", "isSample"]),
                    conversionAvailable: e.path(["data", "conversionAvailable"]),
                    bestChannel: e.path(["data", "bestChannel"]),
                    clicks: e.path(["data", "clicks"]),
                    cpc: e.path(["data", "cpc"]),
                    asap: e.path(["data", "schedule", "asap"]),
                    startDate: e.path(["data", "schedule", "startDate"]),
                    endDate: e.path(["data", "schedule", "endDate"]),
                    continuously: e.path(["data", "schedule", "continuously"]),
                    startedAt: e.path(["data", "startedAt"]),
                    deactivatedAt: e.path(["data", "deactivatedAt"]),
                    sales: e.path(["data", "sales"]),
                    campaignId: e.prop("campaignId"),
                    impressions: e.path(["data", "impressions"]),
                    ads: v,
                    serviceFee: e.path(["data", "serviceFee"]),
                    daysLeft: function (t) {
                        var a = e.path(["data", "schedule", "endDate"])(t);
                        if (a) return g(f()(a).endOf("day").toDate() - new Date)
                    },
                    totalSpent: function (t) {
                        var a = e.compose(Number, e.propOr(0, "spend"));
                        return e.compose(e.reduce(function (e, t) {
                            return e + a(t)
                        }, 0), v)(t)
                    },
                    allDetails: h,
                    allAge: b("age", h),
                    allGender: b("gender", h),
                    allLocation: b("location", h),
                    allKeywords: b("keywords", h),
                    allNewAndReturning: b("newAndReturning", h),
                    allTimeSpentOnSite: b("timeSpentOnSite", h),
                    allConversion: b("conversion", h),
                    status: e.path(["data", "status"]),
                    googleAnalytics: e.path(["data", "intel", "googleAnalytics"]),
                    conversionList: e.prop("conversionList"),
                    selectedConversion: e.prop("selectedConversion"),
                    conversionValue: function (t, a) {
                        var n = a.allConversion;
                        if (e.path(["data", "conversions"])(t)) return e.path(["data", "conversions"])(t);
                        var o = t.selectedConversion;
                        if (o) {
                            var i = e.path([o.name, "goalCompletion"], n);
                            return void 0 === i ? null : y(i)
                        }
                    },
                    conversionTopValue: function (t, a) {
                        var n = a.historyConversionsSummary, o = (a.status, a.impressions), i = a.objective;
                        if (!n || "LINK_CLICKS" === i) return s();
                        var r = n["allTime"].all;
                        return Object(_["N"])(r) ? e.compose(function (e) {
                            return Object(_["N"])(e) && e.value ? e : s()
                        }, e.reduce(function (e, t) {
                            var a = Object(d["a"])(t, 2), n = a[0], o = a[1].goalCompletion, i = void 0 === o ? 0 : o;
                            return (e.value || 0) < i ? {key: n, value: i} : e
                        }, {}), l.a)(r) : s();

                        function s() {
                            return {key: "normalImpression", value: o}
                        }
                    },
                    graph: e.prop("graph"),
                    graphClicks: function (e, t) {
                        var a = t.graph;
                        if (Object(_["N"])(a)) {
                            var n = O(a, "clicks", "click");
                            return s()(n.datasets.find(k("all")), {
                                backgroundColor: C.BLUE,
                                borderColor: C.BLUE
                            }), Object(u["a"])({}, n, {_options: {mainColor: C.BLUE}})
                        }
                    },
                    graphImpressions: function (e, t) {
                        var a = t.graph;
                        if (Object(_["N"])(a)) {
                            var n = O(a, "clicks", "impression");
                            return s()(n.datasets.find(k("all")), {
                                backgroundColor: C.ORANGE,
                                borderColor: C.ORANGE
                            }), Object(u["a"])({}, n, {_options: {mainColor: C.ORANGE}})
                        }
                    },
                    graphCtr: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(n)) {
                            var o = e.compose(e.without(["all"]), i.a, e.pathOr({}, ["summary", "allTime", "clicks", "click"]))(n),
                                r = O(n, "clicks", "ctr", !1);
                            return r.datasets = r.datasets.filter(e.propSatisfies(e.contains(e.__, o), "label")).map(function (e) {
                                return Object(u["a"])({}, e, {type: "line", fill: !1, borderColor: e.backgroundColor})
                            }), Object(u["a"])({}, r, {_options: {mainColor: C.RED}})
                        }
                    },
                    graphTotalCost: function (e, t) {
                        var a = t.graph;
                        if (Object(_["N"])(a)) {
                            var n = O(a, "spends", "spend");
                            return s()(n.datasets.find(k("all")), {
                                backgroundColor: C.BLUE,
                                borderColor: C.BLUE
                            }), Object(u["a"])({}, n, {_options: {mainColor: C.BLUE}})
                        }
                    },
                    graphTotalBudget: function (e, t) {
                        var a = t.graph;
                        if (Object(_["N"])(a)) {
                            var n = O(a, "spends", "budget");
                            return s()(n.datasets.find(k("all")), {
                                backgroundColor: C.ORANGE,
                                borderColor: C.ORANGE
                            }), Object(u["a"])({}, n, {_options: {mainColor: C.ORANGE}})
                        }
                    },
                    graphConversions: function (t, a) {
                        var o = a.graph;
                        if (Object(_["N"])(o)) {
                            var r = e.path(["summary", "allTime", "conversions", "conversion", "all"], o);
                            if (r) {
                                var c = e.merge({_options: {mainColor: C.BLUE}});
                                return e.compose(e.mapObjIndexed(function (e, t, a) {
                                    var n = c(e);
                                    return s()(n.datasets.find(k("all")), {
                                        backgroundColor: C.BLUE,
                                        borderColor: C.BLUE
                                    }), n
                                }), e.reduce(function (e, t) {
                                    return Object(u["a"])({}, e, Object(n["a"])({}, t, A(o, t)))
                                }, {}), i.a)(r)
                            }
                        }
                    },
                    graphConversionRate: function (t, a) {
                        var o = a.graph;
                        if (Object(_["N"])(o)) {
                            var r = e.path(["summary", "allTime", "conversions", "conversion", "all"], o);
                            if (r) {
                                var s = e.merge({_options: {mainColor: C.ORANGE}}),
                                    c = e.path(["summary", "allTime", "conversions", "conversion"], o);
                                return e.compose(e.mapObjIndexed(e.compose(s, e.over(e.lensProp("datasets"), e.map(function (t) {
                                    return Object(u["a"])({}, t, {
                                        type: "line",
                                        fill: !1,
                                        borderColor: t.backgroundColor,
                                        data: t.data.map(e.divide(e.__, 100))
                                    })
                                })))), e.reduce(function (t, a) {
                                    return Object(u["a"])({}, t, Object(n["a"])({}, a, e.compose(e.over(e.lensProp("datasets"), e.filter(function (t) {
                                        var n = t.label;
                                        return e.path([n, a], c)
                                    })), e.apply(A))([o, a, "conversionRate", !1])))
                                }, {}), i.a)(r)
                            }
                        }
                    },
                    historyConversionsSummary: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(n)) return {
                            today: e.path(["summary", "today", "conversions", "conversion"], n),
                            allTime: e.path(["summary", "allTime", "conversions", "conversion"], n)
                        }
                    },
                    conversionTypes: function (t, a) {
                        var n = a.graph;
                        return i()(e.pathOr({}, ["summary", "allTime", "conversions", "conversion", "all"], n))
                    },
                    historyClickSummary: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(n)) return {
                            today: e.path(["summary", "today", "clicks"], n),
                            allTime: e.path(["summary", "allTime", "clicks"], n)
                        }
                    },
                    historySpendSummary: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(n)) return {
                            today: e.path(["summary", "today", "spends"], n),
                            allTime: e.path(["summary", "allTime", "spends"], n)
                        }
                    },
                    historyBudgetSummary: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(n)) return {
                            today: e.path(["summary", "today", "budget"], n),
                            allTime: e.path(["summary", "allTime", "spends"], n)
                        }
                    },
                    historyVideoSummary: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(n)) return {
                            today: e.path(["summary", "today", "videoViews"], n),
                            allTime: e.path(["summary", "allTime", "videoViews"], n)
                        }
                    },
                    historyCpcSummary: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(n)) return {
                            today: {
                                cpc: e.path(["summary", "today", "cpc", "cpc"], n),
                                click: e.path(["summary", "today", "clicks", "click"], n)
                            },
                            allTime: {
                                cpc: e.path(["summary", "allTime", "cpc", "cpc"], n),
                                click: e.path(["summary", "allTime", "clicks", "click"], n)
                            }
                        }
                    },
                    graphCpc: function (t, a) {
                        var n = a.graph, o = a.historyCpcSummary;
                        if (Object(_["N"])(n) && Object(_["N"])(o)) {
                            var r = e.pathOr({}, ["allTime", "cpc"], o) || e.compose(e.defaultTo({}), e.prop("cpc"), e.last)(e.prop("cpc", n)),
                                c = O(n, "cpc", "cpc");
                            return c.datasets = c.datasets.filter(e.propSatisfies(e.contains(e.__, i()(r || {})), "label")).map(function (e) {
                                return Object(u["a"])({}, e, {type: "line", fill: !1, borderColor: e.backgroundColor})
                            }), s()(c.datasets.find(k("all")), {
                                backgroundColor: C.RED,
                                borderColor: C.RED,
                                label: "average"
                            }), Object(u["a"])({}, c, {_options: {mainColor: C.RED}})
                        }
                    },
                    graphDataVideoAvailable: function (t, a) {
                        var n = a.graph;
                        return Object(_["N"])(e.prop("videoViews", n))
                    },
                    graphVideo10s: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(e.prop("videoViews", n))) {
                            var o = O(n, "videoViews", "videoView");
                            return s()(o.datasets.find(k("all")), {
                                backgroundColor: C.BLUE,
                                borderColor: C.BLUE
                            }), Object(u["a"])({}, o, {_options: {mainColor: C.BLUE}})
                        }
                    },
                    graphVideoImpressions: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(e.prop("videoViews", n))) {
                            var o = O(n, "videoViews", "impression");
                            return s()(o.datasets.find(k("all")), {
                                backgroundColor: C.ORANGE,
                                borderColor: C.ORANGE
                            }), Object(u["a"])({}, o, {_options: {mainColor: C.ORANGE}})
                        }
                    },
                    graphVideoViewRatio: function (t, a) {
                        var n = a.graph;
                        if (Object(_["N"])(e.prop("videoViews", n))) {
                            var o = e.compose(i.a, e.defaultTo({}), e.path([0, "ratioViewsImpressions"]))(e.prop("videoViews", n)),
                                r = O(n, "videoViews", "ratioViewsImpressions");
                            return r.datasets = r.datasets.filter(e.propSatisfies(e.contains(e.__, o), "label")).map(function (e) {
                                return Object(u["a"])({}, e, {type: "line", fill: !1, borderColor: e.backgroundColor})
                            }), s()(r.datasets.find(k("all")), {
                                backgroundColor: C.RED,
                                borderColor: C.RED
                            }), Object(u["a"])({}, r, {_options: {mainColor: C.RED}})
                        }
                    },
                    isGaConnected: function (e, t) {
                        var a = t.googleAnalytics;
                        return Object(_["N"])(a)
                    },
                    objective: e.path(["data", "objective"]),
                    schedule: e.pathOr({}, ["data", "schedule"])
                }
            }
        }).call(this, a("b17e"))
    }, "79f5": function (e, t) {
        e.exports = {
            en: {
                add_option: "+ Add Option",
                add_question: "+ Add Question",
                checkboxTitle: "Enter your question.",
                choiceTitle: "Option",
                desc: "Briefly explain what will happen after they subscribe, or simply what your customers can get from you. Will you contact them? Do you have any goodies or promotional items for them? How exciting are the products you offer?",
                descriptionDefault: "We will contact you shortly with promotional code.",
                dropdown_ques1: "Short-answer Question",
                dropdown_ques2: "Multiple Choice",
                dropdown_ques3: "Check Box",
                edit_btn1: "Cancel",
                edit_btn2: "OK",
                edit_title: "Edit Customer leads",
                emailDefault: "What is your email address?",
                img_upload: "Image Upload",
                leads_count: "No. of leads",
                multipleItem: "Option",
                multipleTitle: "Enter your question.",
                not_required: "Optional",
                phoneDefault: "What is your contact number?",
                plus_question2: "(Choose all that apply)",
                plus_question3: "(Only choose the ones that apply to you)",
                policy: "By clicking Submit, you agree to send your information to the merchant who can use it according to their privacy policy. Adriel, their advertising partner, might also use it subject to its Data Policy, including auto-fill forms for ads.",
                policy_underline: "View Adriel Data Policy",
                question: "Question",
                quesTitle: "Enter your question.",
                required: "Required",
                shortAnswer: "Short answer",
                shortTitle: "Ask your potential customer for their contact information.",
                submit_btn: "Submit",
                summary_url_title: "Customer Leads",
                thanks: "Enter a thank you message",
                thanks_placeholder: "Thank you so much. We hope to see you soon.",
                title: "Enter a captivating title to engage your audience.",
                titleDefault: "Welcome! Please fill in the form below.",
                submitted: "Successfully submitted"
            },
            jp: {
                add_option: "+ �멩뒢��옙��",
                add_question: "+ 蘊ゅ븦瓦썲뒥",
                checkboxTitle: "蘊ゅ븦�믤쎑�꾠겍�뤵걽�뺛걚��",
                choiceTitle: "�멩뒢�� ",
                desc: "佯껃몜訝삥쭣��＝�곥겲�잆겘�듐꺖�볝궧�ラ뼟�쇻굥怜▼뜕�よが�롢굜,礪섇폀�먨눣餓ε풄�⒲굯�ゃ걪�ⓦ걣�꿱죱�뺛굦�뗣걢怜▼뜕�ユ쎑�꾠겍�뤵걽�뺛걚�� �볝걪�ャ걡若€쭣�뚧깄�긱굮�먨눣�쇻굥�볝겏�㎩풓�됥굦�뗦겑�듐겓�ㅳ걚��섭沃욍걲�뗣걪�ⓦ걣訝��ら뇥誤곥겎�쇻��",
                descDefault: "�꿨폊�ㅳ깧�녈깉�ゃ겑�꾤Ŧ�됬뵪�ゆ깄�긱굮�딃�곥굤�꾠걼�쀣겲�쇻��",
                dropdown_ques1: "��걚蘊ゅ븦",
                dropdown_ques2: "鸚싨뙁渶싧쩀壤�나��",
                dropdown_ques3: "�곥궒�껁궚�쒌긿��궧",
                edit_btn1: "�뽪텋",
                edit_btn2: "若뚥틙",
                edit_title: "逆쒎쑉窈㎩��잓썓佯껃몜�믥랬��",
                img_upload: "�ㅳ깳�쇈궦�㏂긿�쀣꺆�쇈깋",
                multipleItem: "�멩뒢�� ",
                multipleTitle: "蘊ゅ븦�믤쎑�꾠겍�뤵걽�뺛걚��",
                plus_question2: "(筽뉑빊�곥궒�껁궚��꺗)",
                plus_question3: "(1�뗣겗�멩뒢��꺗)",
                policy: "�먨눣�믡궚�ゃ긿��걲�뗣겏鴉싧뱻礪섅겘鴉싧뱻礪섅겗�끻젿�믦㈁壤볠��끹겓邕��곥걮,壤볢㈁�싥꺖�멥겘�뗤볶�끻젿��릤�백뇺�ュ풏�ｃ겍�볝겗�끻젿�믢슴�ⓦ걲�뗣걪�ⓦ겓�뚧꼷�뺛굦�얇걲�� �얇걼,�㏂깋�ゃ궓�ャ겎�괔driel���뗤볶�끻젿��릤�백뇺�ャ굠��,�볝겗�끻젿�믣틕�듽겗�ゅ땿鵝쒏닇礪섇폀�ゃ겑�ヤ슴�ⓦ걲�뗣걪�ⓦ겓�ゃ굤�얇걲��",
                policy_underline: "�뗤볶�끻젿��릤�백뇺",
                question: "蘊ゅ븦",
                quesTitle: "蘊ゅ븦�믤쎑�꾠겍�뤵걽�뺛걚��",
                shortTitle: "蘊ゅ븦�믤쎑�꾠겍�뤵걽�뺛걚��",
                summary_url_title: "逆쒎쑉窈㎩�閻뷰퓷��쭣凉�",
                thanks: "�듿�礪섅걣礪섇폀�믤룓�뷩걮�잌풄,烏①ㅊ�뺛굦�뗧뵽�㏂겓�γ굦�뗧썵�삡볶雅뗣굮鵝쒏닇�쀣겍�뤵걽�뺛걚��",
                thanks_placeholder: "�귙굤�뚣겏�녴걫�뽧걚�얇걲�귞▶沃띶풄�쇻걧�ｇ덧�쀣겲�쇻��",
                title: "逆쒎쑉窈㎩���쎅�믣폊�뤵궭�ㅳ깉�ャ굮�멥걚�╉걦�졼걬�꾠��",
                titleDefault: "�볝굯�ャ걾��,訝뗨쮼�ョ가�섅겓�끻젿�믡걫鼇섇뀯�뤵걽�뺛걚��"
            },
            ko: {
                add_option: "+ �좏깮吏� 異붽�",
                add_question: "+ 吏덈Ц 異붽�",
                checkboxTitle: "吏덈Ц�� �곸뼱二쇱꽭��.",
                choiceTitle: "�좏깮吏� ",
                desc: "愿묎퀬二쇰떂�� �ъ뾽�� ���� 媛꾨떒�� �ㅻ챸, �먮뒗 紐⑥쑝怨좎옄 �섎뒗 怨좉컼 �뺣낫瑜� �뚮젮二쇱꽭��. 醫뗭� 怨좉컼DB �섏쭛 愿묎퀬�� �뺣낫瑜� �④��쇰줈�� �쒓났�섎뒗 �쒗깮�� �� 媛뺤“�⑸땲��.",
                descriptionDefault: "�좎씤 �대깽�� �� 媛곸쥌 �좎슜�� �뺣낫瑜� 蹂대궡 �쒕━寃좎뒿�덈떎.",
                dropdown_ques1: "吏㏃� 吏덈Ц",
                dropdown_ques2: "�ㅼ��좊떎�� 吏덈Ц",
                dropdown_ques3: "泥댄겕諛뺤뒪",
                edit_btn1: "痍⑥냼",
                edit_btn2: "�꾨즺",
                edit_title: "DB 愿묎퀬 �몄쭛�섍린",
                emailDefault: "�대찓�� 二쇱냼瑜� �④꺼二쇱꽭��.",
                img_upload: "�대�吏� �낅줈��",
                leads_count: "怨좉컼DB ��",
                multipleItem: "�좏깮吏� ",
                multipleTitle: "吏덈Ц�� �곸뼱二쇱꽭��.",
                not_required: "�좏깮",
                phoneDefault: "�곕씫泥섎� �④꺼二쇱꽭��.",
                plus_question2: "(蹂듭닔 泥댄겕 媛���)",
                plus_question3: "(1媛� �좏깮 媛���)",
                policy: "�쒖텧�� �대┃�섎㈃ �뚯썝�섏� �뚯썝�섏쓽 �뺣낫瑜� �대떦 �낆껜�먭쾶 �꾩넚�섎ŉ �대떦 �섏씠吏��� 媛쒖씤�뺣낫泥섎━諛⑹묠�� �곕씪 �� �뺣낫瑜� �ъ슜�섎뒗 \n寃껋뿉 �숈쓽�섍쾶 �⑸땲��.�먰븳 �꾨뱶由ъ뿕�먯꽌�� �꾨뱶由ъ뿕 媛쒖씤�뺣낫泥섎━諛⑹묠�� �곕씪 �� �뺣낫瑜� 愿묎퀬�� �먮룞 �묒꽦 �묒떇 �깆뿉 �ъ슜�섍쾶 \n�⑸땲��.",
                policy_underline: "媛쒖씤�뺣낫泥섎━諛⑹묠",
                question: "吏덈Ц",
                quesTitle: "吏덈Ц�� �곸뼱二쇱꽭��.",
                required: "�꾩닔",
                shortAnswer: "吏㏃� �듬�",
                shortTitle: "怨좉컼�쇰줈遺��� �섏쭛�섍퀬�� �섎뒗 �뺣낫瑜� 臾쇱뼱蹂댁꽭��.",
                submit_btn: "�쒖텧�섍린",
                summary_url_title: "DB 愿묎퀬",
                thanks: "怨좉컼�� �묒떇�� �쒖텧�� �� �쒖떆�섎뒗 �붾㈃�� �ｌ쓣 媛먯궗 �몄궗瑜� �묒꽦�댁＜�몄슂.",
                thanks_placeholder: "媛먯궗�⑸땲��. �뺤씤 �� 怨� �곕씫 �쒕━寃좎뒿�덈떎.",
                title: "�좎옱 怨좉컼�� �덇만�� �꾨뒗 �쒕ぉ�� �곸뼱二쇱꽭��.",
                titleDefault: "�덈뀞�섏꽭��, �꾨옒�� 媛꾨떒�� �뺣낫瑜� 湲곗엯�댁＜�몄슂.",
                submitted: "�쒖텧�� �깃났�덉뒿�덈떎."
            }
        }
    }, "79f7": function (e, t) {
        e.exports = {
            en: {
                ad_id_tooltip: "This is the ad's ID. You can refer to this number when you check each ad's performance on your Google Analytics.",
                ad_settings: "Ad settings",
                add_app_url: "Add Appstore URL",
                add_budget: "Add budget",
                adriel_help: "We're here to help",
                ads_slide_bottom_tooltip_click: "Clicks",
                ads_slide_bottom_tooltip_cpc: "CPC",
                ads_slide_bottom_tooltip_ctr: "CTR",
                ads_slide_bottom_tooltip_totalCost: "Total cost",
                all_time: "All time",
                best_channel: "Best channel",
                best_modal_audience: "Audience",
                best_modal_best_website: "Best Website",
                best_modal_cost: "Cost",
                best_modal_keywords: "Keywords",
                best_modal_result: "Result",
                billing_history: "Billing history",
                budget_left: "Budget Left",
                budget_platform_include_fee: "Total budget incl. fees",
                budget_spent: "Spent",
                card_title_age: "Age",
                card_title_best_ad: "Best Ad",
                card_title_best_channel: "Best channel",
                card_title_budget: "Budget spent",
                card_title_clicks: "Clicks",
                card_title_cpc: "Cost per click",
                card_title_gender: "Gender",
                card_title_keywords: "Best keywords",
                card_title_location: "Location",
                card_title_new_returning: "New & Returning",
                card_title_sales: "Sales",
                card_title_time_spent: "Time spent on Site",
                clicks: "clicks",
                clicks_of_today: "Last 24h's clicks",
                clicks_total: "Clicks",
                connect_ga: "If your website is using Google Analytics, please click the button below to log in to your GA.",
                connect_ga_on_proposal: "You haven't shared your Google Anlaytics or Pixel and/or the respective conversion objective.\n You can do so in Ad Settings, or click the button below.",
                conversion_input_placeholder: "or Enter specific url as conversion",
                conversion_placeholder: "Choose your conversion",
                conversions: "Conversions",
                cost: "Cost",
                cost_budget_spent: "Budget spent",
                cost_per_click: "Cost per click",
                cost_platform: "Cost spent on platforms",
                cost_platform_include_fee: "Total cost incl. fees",
                customer_new_tooltip: "New visitors similar to your past and current customers will be included. This list is created based on Pixel or Google Analytics.",
                customer_return_tooltip: "Your past and current customers based on your website visitors or list of past & current customers. Not available if there is no Pixel, Google Analytics or list of past & current customers.",
                daily_budget_comment: "During the AI's optimization process, the actual spending on each platform may exceed or fall below the daily budget. (Budget rebalancing)",
                dashboard_title: "Campaign results",
                deactivated_ad: "Deactivated",
                directUrl: "See this ad on Facebook",
                download_excel: "Download data",
                form_btn1: "View",
                form_btn2: "Edit",
                form_title: "Customer leads",
                gender_female: "female",
                gender_male: "male",
                gender_unknown: "unknown",
                go_to_set_goal: "Go to Ad Settings",
                history_clicks: "clicks",
                history_imps: "imps",
                history_period: "Past {days} days",
                history_sub_title_budget: "Daily budget",
                history_sub_title_clicks: "Clicks",
                history_sub_title_conversionRate: "Conversion rate",
                history_sub_title_conversions: "Conversions",
                history_sub_title_cpc: "Cost per click",
                history_sub_title_ctr: "CTR",
                history_sub_title_impressions: "Impressions",
                history_sub_title_platform: "Ad platform cost",
                history_sub_title_video10s: "10s view",
                history_sub_title_videoViewRatio: "10s view rate",
                history_tab_title_clicks: "Clicks",
                history_tab_title_conversions: "Conversions",
                history_tab_title_cpc: "Cost per click",
                history_tab_title_dailyBudget: "Daily budget",
                history_tab_title_totalCost: "Total cost",
                history_tab_title_video10s: "10s views",
                imp_click_clicks: "Clicks",
                imp_click_ctr: "CTR",
                imp_click_imps: "Imps",
                in_external_review: "This ad is still being reviewed by the ad platform and will show up as soon as it's approved.",
                keywords: "Keywords",
                last_day: "Today is the last day!",
                need_ga: "Not available",
                new_returning_new: "new",
                new_returning_returning: "returning",
                new_target: "New customer",
                not_available: "Not Available",
                not_available_for_sample: "Not available for sample",
                number_of_running_ads: "Number of running ads ",
                percent_of_spent: "% of budget spent on this ad",
                placement_tooltip: "Your ads are placed in all available placements and only the best performing placement is shown here.",
                ran_for: '<span class="campaign-left-top-text first">Ran for</span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">{str}</span>',
                re_target: "Retargeting",
                remain_for: '<span class="campaign-left-top-text">Live in </span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">days</span>',
                run_again: "Run again",
                run_longer: "Run longer",
                run_search_ad: "You need to run Google Search ads to see this information.",
                running_for: '<span class="campaign-left-top-text first">It\'s been running for</span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">{str}</span>',
                sales_of_today: "Last 24h's sales",
                sales_total: "Sales",
                scheduled_for: '<span class="campaign-left-top-text">Live in</span> <span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">{str}.</span>',
                see_more_ads: "See more ads",
                service_in_prepare: "Not Available",
                summary_elem_impression: "Impression",
                timespent_bounce_rate: "Bounce rate",
                timespent_fomula: "seconds / visitor",
                today: "Last 24h",
                views: "views"
            },
            jp: {
                ad_id_tooltip: "�꾢틕�듽겗訝��뤵겗ID�㎯걲�괛oogle�㏂깏�ゃ깇�ｃ궚�밤겎榮졿쓲�γ겗�먩옖�믥▶沃띲걲�뗩슋�ヤ씩�⒲겎�쇻��",
                ad_settings: "佯껃몜鼇�츣",
                add_app_url: "Appstore�췜RL�믣뒥�덀겍�뤵걽�뺛걚",
                add_budget: "雅덄츞�믣쥥�꾠걲",
                adriel_help: "鵝뺛걢�듿쎇�듽겎�쇻걢竊�",
                ads_slide_bottom_tooltip_click: "��꺁�껁궚��",
                ads_slide_bottom_tooltip_cpc: "CPC",
                ads_slide_bottom_tooltip_ctr: "CTR",
                ads_slide_bottom_tooltip_totalCost: "�녈궧�덂릦鼇�",
                all_time: "�꾠겇�㎯굚",
                best_channel: "�쇻궧�덀긽�ｃ깓��",
                best_modal_audience: "�ゃ꺖�뉎궍�ⓦ꺍��",
                best_modal_best_website: "�쇻궧�덀궑�㎯깣�듐궎��",
                best_modal_cost: "縕사뵪",
                best_modal_keywords: "��꺖��꺖��",
                best_modal_result: "永먩옖",
                billing_history: "蘊쇔뀯掠ζ�",
                budget_left: "餘뗣굤雅덄츞",
                budget_platform_include_fee: "�뗦빊�쇻굮�ャ�訝��δ틛嶸�",
                budget_spent: "雅덄츞易덂뙑",
                card_title_age: "亮닻숱",
                card_title_best_ad: "�쇻궧�덂틕��",
                card_title_best_channel: "�쇻궧�덀긽�ｃ깓��",
                card_title_budget: "易덂뙑雅덄츞",
                card_title_clicks: "��꺁�껁궚��",
                card_title_cpc: "��꺁�껁궚�섆쐴",
                card_title_gender: "�㎩닪",
                card_title_keywords: "�쇻궧�덀궘�쇈꺈�쇈깋",
                card_title_location: "��궞�쇈궥�㎯꺍",
                card_title_new_returning: "�계쫸&�℡춼",
                card_title_sales: "鶯꿔굤訝듽걩",
                card_title_time_spent: "�듐궎�덃퍧�ⓩ셽��",
                clicks: "��꺁�껁궚��",
                clicks_of_today: "24�귡뼋餓ε냵��궚�ゃ긿��빊",
                clicks_total: "��꺁�껁궚��",
                connect_ga: "Google Analytics�믢슴�ｃ겍�꾠굥�닷릦�곦툔��깭�욍꺍�믡궚�ゃ긿��걮�╉꺆�겹궎�녈걮�╊툔�뺛걚��",
                connect_ga_on_proposal: "�╉궒�뽧궢�ㅳ깉�덀꺀�껁궖�쇈겗與⑶솏�뚦뀻�됥걬�뚣겍�꾠겒�꾠걢�곩쨯�쏁쎅與쇻걣鼇�츣�뺛굦�╉걚�얇걵�볝�귛쨯�쏁쎅與쇻굮瓦썲뒥�쇻굥�잆굙�ャ�곦빳訝뗣겗�쒌궭�녈굮�쇈걮�╉�곮Þ若싥깪�쇈궦�ョ㎉�뺛걮�╉걦�졼걬��",
                conversion_input_placeholder: "�얇걼��돶若싥겗URL�믡궠�녈깘�쇈궦�㎯꺍�ⓦ걮��쇉�꿔걮�╉걦�졼걬�꾠��",
                conversion_placeholder: "�녈꺍�먦꺖�멥깾�녈굮�멥굯�㎯걦�졼걬�꾠��",
                conversions: "�녈꺍�먦꺖�멥깾�녔빊",
                cost: "�녈궧��",
                cost_budget_spent: "易덂뙑雅덄츞",
                cost_per_click: "��꺁�껁궚�섆쐴",
                cost_platform: "�쀣꺀�껁깉�쇈깢�⒲꺖�졼겎易덂뙑�뺛굦�잆궠�밤깉",
                cost_platform_include_fee: "�듐꺖�볝궧縕사뵪渦쇈겗�덅쮫",
                customer_new_tooltip: "�℡춼�ャ궧�욍깯�쇈겓鴉쇈겍�꾠굥�계쫸�╉꺖�뜰꺖�뚦맜�얇굦�얇걲�귙걪��꺁�밤깉�츿ixel�곥겲�잆겘Google Analytics�믣뀇�ヤ퐳�먦걬�뚣겍�꾠겲�쇻��",
                customer_return_tooltip: "�듐궎�덅Ø�뤵깺�쇈궣�쇈걡�덀겞�ゃ궧�덀겓�뷩겈�꾠걼�℡춼�ャ궧�욍깯�쇈겎�쇻�괦ixel�곥겲�잆겘Google Analytics�뚪�ｅ땿�뺛굦�╉걚�ゃ걚�닷릦烏①ㅊ�뺛굦�얇걵��",
                daily_budget_comment: "雅뷴램�θ꺗����⒴뙑��걥葉뗣겎�곩릢�쀣꺀�껁깉�쎼꺖�졼겗若잓슋��윿烏뚩꼇�ⓦ겘�곦��γ겗雅덄츞�믦텈�롢겲�잆겘�ら걫�ャ겒�뗣걪�ⓦ걣�㎯걤�얇걲�귨펷雅덄츞�ゃ깘�⒲꺍�룔꺍�곤펹",
                dashboard_title: "��깵�녈깪�쇈꺍永먩옖",
                deactivated_ad: "�띴에�쒏�",
                directUrl: "Facebook訝듽겎�볝겗佯껃몜�믦쫳��",
                download_excel: "�뉎꺖�욍겗���╉꺍��꺖��",
                form_btn1: "�ゃ꺍��굮�뗣걦",
                form_btn2: "渶③썓�쇻굥",
                form_title: "逆쒎쑉�꾠겒�잓썓佯껃몜",
                gender_female: "也녔��",
                gender_male: "�룡��",
                gender_unknown: "�ゅ츣",
                go_to_set_goal: "�녈꺍�먦꺖�멥깾�녕쎅與쇻겗鼇�츣",
                history_clicks: "��꺁�껁궚��",
                history_imps: "�ㅳ꺍�쀣꺃�껁궥�㎯꺍��",
                history_period: "recent {days} days",
                history_sub_title_budget: "�ε닪雅덄츞",
                history_sub_title_clicks: "��꺁�껁궚��",
                history_sub_title_conversionRate: "�녈꺍�먦꺖�멥깾�녕럤",
                history_sub_title_conversions: "�녈꺍�먦꺖�멥깾�녔빊 ",
                history_sub_title_cpc: "��꺁�껁궚�섆쐴",
                history_sub_title_ctr: "��꺁�껁궚��",
                history_sub_title_impressions: "�ㅳ꺍�쀣꺃�껁궥�㎯꺍��",
                history_sub_title_platform: "佯껃몜�쀣꺀�껁깉�뺛궔�쇈깲縕사뵪",
                history_sub_title_video10s: "10s view",
                history_sub_title_videoViewRatio: "10s view rate",
                history_tab_title_clicks: "��꺁�껁궚��",
                history_tab_title_conversions: "�녈꺍�먦꺖�멥깾�녔빊",
                history_tab_title_cpc: "��꺁�껁궚�섆쐴",
                history_tab_title_dailyBudget: "訝��δ틛嶸�",
                history_tab_title_totalCost: "渶뤺꼇��",
                history_tab_title_video10s: "10s views",
                imp_click_clicks: "��꺁�껁궚��",
                imp_click_ctr: "CTR",
                imp_click_imps: "�ㅳ꺍�쀣꺃�껁궥�㎯꺍��",
                in_external_review: "�볝겗佯껃몜��깳�뉎궍�㏂겓�덀굥野⒵읅訝�겎�쇻�귝돽沃띲걬�뚧А寧ц〃鹽뷩걬�뚣겲�쇻��",
                keywords: "��꺖��꺖��",
                last_day: "餓딀뿥�뚧�永귝뿥�㎯걲竊�",
                need_ga: "屋꿨퐪�ゃ걮",
                new_returning_new: "�계쫸",
                new_returning_returning: "�℡춼",
                new_target: "�계쫸�ャ궧�욍깯��",
                not_available: "屋꿨퐪�ゃ걮",
                not_available_for_sample: "�듐꺍�쀣꺂��걗�듽겲�쎼굯",
                number_of_runnig_ads: "�띴에訝�겗佯껃몜��: ",
                number_of_running_ads: "�띴에訝�겗佯껃몜�� : ",
                percent_of_spent: "�볝겗佯껃몜�㎪텋�뽧걮�잋틛嶸쀯펳",
                placement_tooltip: "�귙겒�잆겗佯껃몜��닶�ⓨ룾�썬겒�쇻겧�╉겗佯껃몜�졼겓鼇�쉰�뺛굦�╉걚�얇걲�귙걪�볝겓���곥깙�뺛궔�쇈깯�녈궧�뚥��よ돬�꾠깴�롢겗�욤〃鹽뷩걬�뚣겍�꾠겲�쇻��",
                ran_for: '<span class="campaign-left-top-text first">Ran for</span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">{str}</span>',
                re_target: "�ゃ궭�쇈궟�녴궍�녈궛",
                remain_for: '<span class="campaign-left-top-text">Live in </span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">days</span>',
                run_again: "�귙걝訝�佯�뀓岳▲걲��",
                run_longer: "�띴에�믢섯�겹걲",
                run_search_ad: "�볝겗�끻젿�믦쫳�뗣겓��쩂榮℡틕�듽굮�띴에�쇻굥恙낁쫨�뚣걗�듽겲�쇻��",
                running_for: '<span class="campaign-left-top-text first">It\'s been running for</span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">{str}</span>',
                sales_of_today: "24�귡뼋餓ε냵��２�듾툓��",
                sales_total: "鶯꿔굤訝듽걩",
                scheduled_for: '<span class="campaign-left-top-text">愿묎퀬 吏묓뻾源뚯�</span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">��</span>',
                see_more_ads: "�귙겂�ⓨ틕�듽굮誤뗣굥",
                service_in_prepare: "屋꿨퐪�ゃ걮",
                summary_elem_impression: "�ㅳ꺍�쀣꺃�껁궥�㎯꺍��",
                timespent_bounce_rate: "�먦궑�녈궧��",
                timespent_fomula: "燁� / 鼇ゅ븦��",
                today: "24�귡뼋餓ε냵",
                views: "�볝깷�쇗빊"
            },
            ko: {
                ad_id_tooltip: "媛� 愿묎퀬�� 怨좎쑀 ID�낅땲��. 援ш� �먮꼸由ы떛�ㅼ뿉�� �뚯옱蹂� �깃낵瑜� �뺤씤�섏떎 �� �좎슜�⑸땲��.",
                ad_settings: "愿묎퀬 �ㅼ젙 蹂닿린",
                add_app_url: "�깆뒪�좎뼱 留곹겕 異붽�",
                add_budget: "�쇱씪 �덉궛 異붽�",
                adriel_help: "臾몄쓽 �ы빆�� �덉쑝�몄슂?",
                ads_slide_bottom_tooltip_click: "�대┃��",
                ads_slide_bottom_tooltip_cpc: "�대┃ �� 鍮꾩슜",
                ads_slide_bottom_tooltip_ctr: "�대┃��",
                ads_slide_bottom_tooltip_totalCost: "珥앸퉬��",
                all_time: "�꾩쟻",
                best_channel: "踰좎뒪�� 梨꾨꼸",
                best_modal_audience: "��寃� 怨좉컼",
                best_modal_best_website: "踰좎뒪�� �뱀궗�댄듃",
                best_modal_cost: "鍮꾩슜",
                best_modal_keywords: "�ㅼ썙��",
                best_modal_result: "寃곌낵",
                billing_history: "寃곗젣 �댁뿭",
                budget_left: "�⑥� �덉궛",
                budget_platform_include_fee: "�섏닔猷� �ы븿 �� �덉궛",
                budget_spent: "吏�異� 湲덉븸",
                card_title_age: "�섏씠",
                card_title_best_ad: "踰좎뒪�� 愿묎퀬",
                card_title_best_channel: "踰좎뒪�� 梨꾨꼸",
                card_title_budget: "吏�異� �덉궛",
                card_title_clicks: "�대┃ ��",
                card_title_cpc: "�대┃ �� 鍮꾩슜",
                card_title_gender: "�깅퀎",
                card_title_keywords: "踰좎뒪�� �ㅼ썙��",
                card_title_location: "��寃� 吏���",
                card_title_new_returning: "�좉퇋 & �щ갑臾� 怨좉컼",
                card_title_sales: "留ㅼ텧",
                card_title_time_spent: "�뱀궗�댄듃�먯꽌 蹂대궦 �쒓컙",
                clicks: "�대┃",
                clicks_of_today: "吏��� 24�쒓컙 �대┃��",
                clicks_total: "�대┃��",
                connect_ga: "愿묎퀬二쇰떂�� �뱀궗�댄듃�� 援ш� �좊꼸由ы떛�ㅼ씠 �곌껐�섏뼱 �덈떎硫�, �꾨옒 踰꾪듉�� �대┃�섏뀛�� 援ш� �좊꼸由ы떛�� 怨꾩젙�� 濡쒓렇�명빐二쇱꽭��.",
                connect_ga_on_proposal: "�뱀궗�댄듃 �몃옒而� 沅뚰븳�� 怨듭쑀�섏� �딆븯嫄곕굹, �꾪솚 紐⑺몴媛� �ㅼ젙�섏� �딆븯�듬땲��. �꾪솚 紐⑺몴瑜� 異붽��섏떆�ㅻ㈃ �꾨옒�� 踰꾪듉�� �뚮윭 �ㅼ젙 �섏씠吏�濡� �대룞�섏꽭��.",
                conversion_input_placeholder: "�먮뒗 �꾪솚 URL�� 吏곸젒 �낅젰�섏꽭��",
                conversion_placeholder: "�꾪솚�� �좏깮�섏꽭��.",
                conversions: "�꾪솚",
                cost: "鍮꾩슜",
                cost_budget_spent: "吏�異� �덉궛",
                cost_per_click: "�대┃ �� 鍮꾩슜",
                cost_platform: "愿묎퀬 �뚮옯�� 吏�異� 鍮꾩슜",
                cost_platform_include_fee: "�섏닔猷� �ы븿 珥� 鍮꾩슜",
                customer_new_tooltip: "援ш� �좊꼸由ы떛�� �먮뒗 �쎌� �뺣낫瑜� 怨듭쑀�� 寃쎌슦, 愿묎퀬二쇰떂�� �뱀궗�댄듃瑜� 諛⑸Ц�섎뒗 �щ엺�ㅺ낵 鍮꾩듂�� �꾨줈�꾩쓣 媛�吏� ��寃잛쓣 李얠븘 愿묎퀬瑜� �대낫�낅땲��.",
                customer_return_tooltip: "愿묎퀬二쇰떂�� �뱀궗�댄듃瑜� 諛⑸Ц�덈뜕 �щ엺�ㅼ쓣 由ы�寃잜똿�섏뿬 愿묎퀬瑜� �대낫�낅땲��. 援ш� �좊꼸由ы떛�� �먮뒗 �쎌��� 怨듭쑀�섏� �딆� 寃쎌슦 �� 湲곕뒫�� �댁슜�� �� �놁뒿�덈떎.",
                daily_budget_comment: "�멸났吏��μ쓽 理쒖쟻�� 怨쇱젙�� �섑빐 �뚮옯�� 蹂� �ㅼ젣 吏묓뻾 鍮꾩슜��<br> �� �덉궛�� 珥덇낵 �먮뒗 誘몃떖�� �� �덉뒿�덈떎.(�덉궛 由щ갭�곗떛)",
                dashboard_title: "愿묎퀬寃곌낵",
                deactivated_ad: "醫낅즺�� 愿묎퀬",
                directUrl: "�섏씠�ㅻ턿�먯꽌 �� 愿묎퀬 吏곸젒 蹂닿린",
                download_excel: "�곗씠�� �ㅼ슫濡쒕뱶",
                form_btn1: "留곹겕 �닿린",
                form_btn2: "�몄쭛�섍린",
                form_title: "DB 愿묎퀬",
                gender_female: "�ъ꽦",
                gender_male: "�⑥꽦",
                gender_unknown: "�뚯닔�놁쓬",
                go_to_set_goal: "�꾪솚紐⑺몴 �ㅼ젙�섎윭 媛�湲�",
                history_clicks: "�대┃",
                history_imps: "�몄텧",
                history_period: "理쒓렐 {days}��",
                history_sub_title_budget: "�� �덉궛",
                history_sub_title_clicks: "�대┃��",
                history_sub_title_conversionRate: "�꾪솚��",
                history_sub_title_conversions: "�꾪솚�� ",
                history_sub_title_cpc: "�대┃�� 鍮꾩슜",
                history_sub_title_ctr: "�대┃��",
                history_sub_title_impressions: "�몄텧��",
                history_sub_title_platform: "愿묎퀬 �뚮옯�� 吏�異� 鍮꾩슜",
                history_sub_title_video10s: "10珥� �댁긽 �쒖껌��",
                history_sub_title_videoViewRatio: "10珥� �댁긽 �쒖껌��",
                history_tab_title_clicks: "�대┃",
                history_tab_title_conversions: "�꾪솚",
                history_tab_title_cpc: "�대┃�� 鍮꾩슜",
                history_tab_title_dailyBudget: "�� �덉궛",
                history_tab_title_totalCost: "珥앸퉬��",
                history_tab_title_video10s: "10珥� �댁긽 �쒖껌��",
                imp_click_clicks: "�대┃",
                imp_click_ctr: "�대┃��",
                imp_click_imps: "�몄텧",
                in_external_review: "�꾩쭅 愿묎퀬媛� �ъ궗以묒뿉 �덉뒿�덈떎.",
                keywords: "�ㅼ썙��",
                last_day: "�ㅻ뒛�� 愿묎퀬媛� 吏묓뻾�섎뒗 留덉�留� �좎씠�먯슂!",
                need_ga: "援ш� �좊꼸由ы떛�� �곌껐 �쒖뿉 �뺤씤 媛��ν븳 �뺣낫�낅땲��.",
                new_returning_new: "�좉퇋怨좉컼",
                new_returning_returning: "湲곗〈怨좉컼",
                new_target: "�좉퇋 怨좉컼",
                not_available: "�댁슜 媛��ν븳 �뺣낫媛� �놁뒿�덈떎.",
                not_available_for_sample: "Not available for sample",
                number_of_running_ads: "寃뚯옱�� 愿묎퀬 �� ",
                percent_of_spent: "�대떦 愿묎퀬�� �ъ슜�� �덉궛 鍮꾩쑉",
                placement_tooltip: "愿묎퀬二쇰떂�� 愿묎퀬�� 紐⑤뱺 寃뚯옱 媛��ν븳 �꾩튂�먯꽌 �몄텧�섍퀬 �덉쑝硫�, �꾨뱶由ъ뿕�� 寃곌낵 �섏씠吏��먯꽌�� 媛��� �깃낵媛� 醫뗭� 寃뚯옱 �꾩튂留� 蹂댁뿬�쒕━怨� �덉뒿�덈떎.",
                ran_for: '<span class="campaign-left-top-number first">{day}</span><span class="campaign-left-top-text">�� �숈븞 吏묓뻾</span>',
                re_target: "�щ갑臾� 怨좉컼",
                remain_for: '<span class="campaign-left-top-text">愿묎퀬 �≫뻾 �붿뿬 湲곌컙</span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">��</span>',
                run_again: "愿묎퀬 �ㅼ떆 吏묓뻾",
                run_longer: "愿묎퀬 湲곌컙 �곗옣",
                run_search_ad: "寃��� 愿묎퀬 吏묓뻾 �쒖뿉 �뺤씤 媛��ν븳 �뺣낫�낅땲��.",
                running_for: '<span class="campaign-left-top-number first">{day}</span><span class="campaign-left-top-text">�� �숈븞 吏묓뻾</span>',
                sales_of_today: "吏��� 24�쒓컙 留ㅼ텧",
                sales_total: "留ㅼ텧",
                scheduled_for: '<span class="campaign-left-top-text">愿묎퀬 吏묓뻾源뚯�</span><span class="campaign-left-top-number">{day}</span><span class="campaign-left-top-text">��</span>',
                see_more_ads: "愿묎퀬 �붾낫湲�",
                service_in_prepare: "�쒕퉬�� 以�鍮� 以�",
                summary_elem_impression: "�몄텧",
                timespent_bounce_rate: "�댄깉��",
                timespent_fomula: "珥� / 諛⑸Ц",
                today: "吏��� 24�쒓컙",
                views: "�몄텧"
            }
        }
    }, "7a74": function (e, t) {
        e.exports = {
            en: {
                enter_information: "Please enter the title and the text",
                help_title: "Help",
                request_sent: "Your request has been sent to Adriel Team.\n We will get back to you as soon as possible.",
                select_placeholder: "Choose the campaign you need to help with",
                text: "Text",
                title: "Title",
                we_here: "We're here to help you",
                we_here_content: "It doesn't matter what kind of<br> question you have.<br> Simply send us a message and<br> we'll be happy to assist you."
            },
            jp: {
                enter_information: "�욍궎�덀꺂�ⓩ쑍�뉎굮鼇섇뀯�쀣겍�뤵걽�뺛걚",
                help_title: "�섅꺂��",
                request_sent: "�ゃ궚�ⓦ궧�덀겘�㏂깋�ゃ궓�ャ겗�곥꺖�졼겓�곦에�뺛굦�얇걮�잆��\n 瓦낂�잆겓野얍퓶�꾠걼�쀣겲�쇻��",
                select_placeholder: "�섅꺂�쀣걣恙낁쫨�ゃ궘�ｃ꺍�싥꺖�녈굮�멩뒢�쀣겍�뤵걽�뺛걚��",
                text: "�ф뻼",
                title: "�욍궎�덀꺂",
                we_here: "鵝뺛걢�듿쎇�듽겎�쇻걢竊�",
                we_here_content: "�⒲굯�よ나�뤵겎�귟걻�꾠겍�뤵걽�뺛걚�� �▲긿�삠꺖�멥굮�곥겂�╉걦�졼걬�뚣겙誤ゅ늾�ヨ퓭雅뗣걮�얇걲��"
            },
            ko: {
                enter_information: "�쒕ぉ怨� �댁슜�� �낅젰�� 二쇱꽭��",
                help_title: "臾몄쓽�섍린",
                request_sent: "愿묎퀬二쇰떂�� �붿껌�ы빆�� �묒닔�섏뿀�듬땲��.\n �꾨뱶由ъ뿕 ���먯꽌 �뺤씤 �� �곕씫 �쒕━�꾨줉 �섍쿋�듬땲��.",
                select_placeholder: "�꾩��� �꾩슂�� 愿묎퀬瑜� �좏깮�섏꽭��",
                text: "�댁슜",
                title: "�쒕ぉ",
                we_here: "�꾨뱶由ъ뿕�� �꾩��쒕┫寃뚯슂.",
                we_here_content: "�대뼡 吏덈Ц�대뱺 臾몄쓽�댁＜�몄슂.<br> 紐⑤뱺 吏덈Ц�� �깆떖猿� �듬��쒕━寃좎뒿�덈떎."
            }
        }
    }, "7a90": function (e, t) {
        e.exports = {
            en: {
                btn_text: "Go to My Campaigns",
                click_my_campaign: 'Click "Go to My Campaigns" and discover a whole new experience with Adriel!',
                thank_you_verifying: "Thanks for verifying your email!",
                verified: "Your email has been successfully verified!",
                what_we_will_send: "We will use this email address to send all communications related to:",
                will_send_1: "Your campaigns' status and performance report",
                will_send_2: "News, updates and notifications from Adriel"
            },
            jp: {
                btn_text: "�욁궎��깵�녈깪�쇈꺍�ヨ죱��",
                click_my_campaign: "�앫깯�ㅳ궘�ｃ꺍�싥꺖�녈겓烏뚣걦�앫굮��꺁�껁궚�쀣겍�곥궋�됥꺁�ⓦ꺂�믢퐪蟯볝걮�╉걦�졼걬�꾬펯竊�",
                thank_you_verifying: "�▲꺖�ャ겗�붺▶沃띲걗�듽걣�ⓦ걝�붵걭�꾠겲�쇻��",
                verified: "�귙겒�잆겗�▲꺖�ャ겘閻븃첀�뺛굦�얇걮�잆��",
                what_we_will_send: "�볝겗�▲꺖�ャ궋�됥꺃�밤겓���곦툔鼇섅겗�끻젿�믡걫�ｇ덧�쀣겲�쇻��",
                will_send_1: "��깵�녈깪�쇈꺍�밤깇�쇈궭�밤겏�앫겗�묆깢�⒲꺖�욁꺍�밤꺃�앫꺖��",
                will_send_2: "�㏂깋�ゃ궓�ャ걢�됥겗�딁윥�됥걵�ⓦ궋�껁깤�뉎꺖��"
            },
            ko: {
                btn_text: "�� 愿묎퀬 蹂닿린",
                click_my_campaign: '"�� 愿묎퀬 蹂닿린" �대┃ �� �꾨뱶由ъ뿕怨� �④퍡 �덈줈�� �⑤씪�� 留덉��낆쓣 寃쏀뿕�섏꽭��.',
                thank_you_verifying: "�대찓�쇱쓣 �뺤씤�댁＜�붿꽌 媛먯궗�⑸땲��.",
                verified: "�대찓�쇱씠 �뺤긽�곸쑝濡� �뺤씤�섏뿀�듬땲��",
                what_we_will_send: "�욎쑝濡� �꾨뱶由ъ뿕 �쒕퉬�ㅼ� 愿��⑤맂 而ㅻ��덉��댁뀡�� �대떦 �대찓�쇱쓣 �듯빐 �대（�댁쭏 �덉젙�낅땲��.",
                will_send_1: "愿묎퀬二쇰떂�� 愿묎퀬 �꾪솴 諛� �깃낵 由ы룷��",
                will_send_2: "湲곕뒫 �낅뜲�댄듃 �� 湲고� �뺣낫 �뚮┝"
            }
        }
    }, "81f3": function (e, t) {
        e.exports = {
            en: {
                ad_common_placeholder: "Click here to enter your text",
                ad_types_title: "Where do you want your ads to appear?",
                ad_types_type_appleSearch: "Apple search",
                ad_types_type_facebookFeed: "Facebook",
                ad_types_type_googleDisplay: "Google display",
                ad_types_type_googleSearch: "Google search",
                ad_types_type_googleUniversalApp: "Google Universal App",
                ad_types_type_instagramFeed: "Instagram",
                ads_change_image: "Change image",
                ads_change_media: "Change video/image",
                ads_edit: "Edit",
                ads_list_mobile_dirty: "There seems to be some important information missing. Please check the guideline in red.",
                ads_list_title: "Great! Check how your ads will appear to your target audience. \n Please review, edit and select the ads to run.",
                ads_preview: "Preview",
                ads_title: "Please edit your ad.",
                ads_upload_image: "Upload image",
                ads_upload_logo: "Logo",
                ads_upload_media: "Upload video/image",
                adsList_at_least_one: "At least one ad must be selected.",
                app_url_app_store: "Appstore",
                app_url_first_box_title: "Yes,\n it is a mobile app.",
                app_url_play_store: "Playstore",
                app_url_second_box_title: "No,\n it�셲 not a mobile app",
                app_url_title: "What do you want to advertise: Your website or your mobile app?",
                apple_search_warning: "You cannot edit your app description here.",
                audience_title: "Target audience profile",
                back: "Back",
                budget_best_option: "Our recommended budget for high conversion rates",
                budget_minimum_budget: "A Minimum of {value} is required to run ads",
                budget_most_used: "Most popular",
                budget_popover: '<div class="kr_text"> <p> <b>You only pay for the clicks to your ads.</b> <br>Simply set a daily budget and you�셱e ready to go. Adriel will optimize your ads around the clock to get as much engagement as possible.<br> Once your maximum daily budget is spent, your ads will not be shown for the rest of the day. When you come to pay your total cost, a 19% service fee will be charged along with your budget spent.<br> The average cost per click varies by industry and target audience. It is usually between $0.02 - $1.50.<br> You can increase impressions, clicks and sales conversions with a bigger budget, but major factors of effective online ads are accurate targeting and well-designed ads. <br>Well performing campaigns typically start with a daily budget of around $30.</p> </div> ',
                budget_radio_format: "{value} / Day",
                budget_title: "Please enter your daily budget.",
                can_change_ad_contents: "You can always edit your text and images later.",
                can_change_ad_platforms: "You can change your chosen platforms at any time.",
                can_change_anytime: "You can always edit your information later.",
                can_download: "You can easily download all the collected data",
                close_button: "Do it later",
                copy_from_previous: "Copy from previous ad",
                do_it_later: "Do this later",
                done: "Done",
                facebook_feed_bottom_0: "Like",
                facebook_feed_bottom_1: "Comment",
                facebook_feed_bottom_2: "Share",
                facebook_feed_id: "Business page",
                facebook_popover: '<div class="kr_text"> <p> Not sure about your copywriting and images? No worries. Adriel will review your ads before launching and you can modify them at any time. <br> Once that�셲 done, a performance manager at Adriel will provide suggestions to improve each ad while your campaign is running.<br> <b>Want to use a range of different text and images? </b> 쨌You can create multiple ads and run A/B tests after you launch this campaign.<br><br> <b>Image recommendation</b> 쨌Size: <strong>1080 * 1080 pixel</strong><br> 쨌Minimum size: <strong>500 * 500 pixel</strong><br> 쨌File format: <strong>JPEG, PNG</strong><br> <br> <b>Video recommendation</b> 쨌Length: <strong>15 seconds</strong><br> 쨌Maximum length: <strong>60 minutes</strong><br> 쨌Screen ratio: <strong>4:5 (vertical)</strong><br> 쨌Sound with caption<br> 쨌File format: <strong>.mp4, .mov</strong><br> 쨌Minimum width: <strong>600 pixel</strong><br> 쨌File size: <strong>maximum 4GB</strong><br> <br>If you have any issues uploading the images/videos, click �쐓ave and skip.�� Email your content to <a href="mailto:support@adriel.ai">support@adriel.ai</a> and our customer success team will upload it for you. </p> </div>',
                facebookpage_title: "Don�셳 forget to connect your Facebook page",
                fb_app_title: "Please connect your Facebook App.",
                finish_confirm: "OK",
                finish_content: "Your ad review will take up to 24 hrs",
                finish_go_back: "Go Back",
                finish_title: "Everything�셲 done.",
                form_popover: '<div class="kr_text""> <p>Lead ads allow you to collect information from potential customers. The information you can collect includes, but not limited to, names, email addresses, phone numbers and more. Addtionally, you can create customized questions.<br/><br/>Lead ads can be used to collect sign-ups for newsletters, price estimates and business information, enabling you to identify potential customers and share relevant information with them.<br/><br/>You can download the leads data from the <b class="formPopover">"Ads result"</b> page after launching the campaign.<br/><br/><b class="formPopover">Image recommendation</b> <br/>쨌Image ratio: <b class="formPopover">2.4:1</b> <br/>쨌Recommended resolution: <b class="formPopover">1000px*417px </b><br/>쨌File type : <b class="formPopover">jpg or png</b></p></div>',
                form_title: "Create a lead generation form to collect contact information of your prospects.",
                form_website_address: "Website address (Optional, not required)",
                go_to_review_and_payment: "Go to Review and Payment",
                google_display_popover: '<div class="kr_text"> <p> Not sure about your copywriting and images? No worries. Adriel will review your ads before launching and you can modify them at any time. <br> Once that�셲 done, a performance manager at Adriel will provide suggestions to improve each ad while your campaign is running.<br> <b>Want to use a range of different text and images? </b> 쨌You can create multiple ads and run A/B tests after you launch this campaign.<br><br> <b>Image recommendation</b> 쨌Minimum size: <strong>600 * 314 pixel</strong><br>쨌File format: <strong>JPEG, PNG</strong><br> <br>If you have any issues uploading the images/videos, click �쐓ave and skip��. Email your content to <a href="mailto:support@adriel.ai">support@adriel.ai</a> and our customer success team will upload it for you. </p> </div>',
                google_search_add_description: "Add a description (Optional)",
                google_search_add_description_placeholder: "Enter a description",
                google_search_add_title: "Add a title (Optional)",
                google_search_add_title_placeholder: "Enter a title",
                google_search_default_title: "Title",
                google_search_description_1: "description (1)",
                google_search_popover: '<div class="kr_text"> <p> Keywords for Google ads will be chosen by Adriel, but you can always add more or remove them. <br>Not sure about your copywriting? No worries. Adriel will review everything before launching and you can edit your ads at any time. <br>Your performance manager at Adriel will provide suggestions to improve each ad as your campaign is running <br> <b>Want to use more than one set of text and images?</b> 쨌You can upload multiple ads to run A/B tests and see which ad works best after you launch this campaign. <br><br>If you have any issues uploading the images/videos, click �쐓ave and skip��. Email your content to <a href="mailto:support@adriel.ai">support@adriel.ai</a> and our customer success team will upload it for you. </p> </div> ',
                google_uac_add_description_placeholder: "Ad text idea",
                google_uac_install_text: "Install",
                google_universal_app_popover: '<div class="kr_text"><p>Not sure about your copywriting? No worries. Adriel will review your ads before launching and you can modify them at any time. Once that�셲 done, a performance manager at Adriel will provide suggestions to improve each ad while your campaign is running.Enter text that can be used to generate your ads<br/><br/><b>How it works:</b> Your ads can be assembled using any of the lines of the text you entered below and any content (images, text, videos) from your app\'s listing. Make sure that the text you enter is 25 characters or less. Make sure the text in each text box is unique. Your text may not be used for some ad formats.<br/><br/><b>Example:</b>Idea 1: Download this app today!<br/>Idea 2: Have fun and win big.<br/>OR<br/><br/>Idea 1: Have fun and win big.<br/>Idea 2: Download this app to</p></div>',
                instagram_feed_id: "Business page",
                instagram_feed_learn_more: "Learn more",
                instagram_popover: '<div class="kr_text"> <p> Not sure about your copywriting and images? No worries. Adriel will review your ads before launching and you can modify them at any time. <br> Once that�셲 done, a performance manager at Adriel will provide suggestions to improve each ad as your campaign is running.<br> <b>Want to mix things up and use a range of different text and images?</b> 쨌You can create multiple ads to run A/B tests and see which ad works best after you launch this campaign. <br><br> <b>Image recommendation</b> 쨌Size: <strong>1080 * 1080 pixel</strong><br> 쨌Minimum size: <strong>500 * 500 pixel</strong><br> 쨌File format: <strong>JPEG, PNG</strong><br> <br> <b>Video recommendation</b> 쨌Length: <strong>15 seconds</strong><br> 쨌Maximum length: <strong>1 minutes</strong><br> 쨌Screen ratio: <strong>4:5 (vertical)</strong><br> 쨌Sound with caption<br> 쨌File format: <strong>.mp4, .mov</strong><br> 쨌Minimum width: <strong>600 pixel</strong><br> 쨌File size: <strong>maximum 4GB</strong><br> <br>If you have any issues uploading the images/videos, click �쐓ave and skip��. Email your content to <a href="mailto:support@adriel.ai">support@adriel.ai</a> and our customer success team will upload it for you. </p> </div>',
                interest_tooltip: "We will show your ads to people with certain interests and hobbies.",
                interests_input_placeholder: "Add Audience interests",
                interests_input_placeholder_empty_tag: "Enter Audience interests",
                keyword_title: "Check suggested search keywords and interests of your target audience.",
                keyword_tooltip: "We will show your ads to people who search for things related to certain keywords.",
                keywords_can_change: "We use your website to get suggestions, but you can always edit the information later",
                keywords_input_placeholder: "Add search keywords",
                keywords_input_placeholder_empty_tag: "Enter search keywords",
                launch_campaign: "Launch This Campaign",
                list_of_suggested_interests: "List of suggested audience interests for Facebook and Instagram Ads.",
                list_of_suggested_keywords: "List of suggested keywords for Google search Ads.",
                location_input_placeholder: "Add target location",
                location_input_placeholder_empty_tag: "Enter target location",
                location_popover: '<div class="kr_text"><p><b>With Adriel you can advertize your business anywhere in the world<br>Here, you can choose your targeting locations. You can change the locations at any time.</b> <br> <b>Quick tip from Adriel:</b> 쨌If you can�셳 find the specific location or if you don�셳 know what to put, you can enter a location at country-level and ask our Customer Success Team after you launch this campaign. Our CS Team will be more than happy to assist you.<br> 쨌Please do not enter duplicate locations - e.g. UK and London. </p> </div>',
                location_title: "Where are your customers based?",
                need_fb_page: "This is required in order to run Facebook and Instagram ads.",
                next: "Next",
                no_ads_edit_ad: "Go back and finish editing",
                no_ads_title: "At least one ad must be created in order to continue",
                objectives: "Select an objective",
                progress_text: "{value}% completed",
                review_process_additional_audience: "Don't forget to <b>set your target audience settings!</b>",
                review_process_additional_facebook: "Don't forget to <b>connect your Facebook Page!</b>",
                review_process_additional_facebook_sdk: "Don't forget to <b>share your Facebook App!</b>",
                review_process_additional_ga: "Don't forget to <b>connect your Google Analytics!</b>",
                review_process_details: "Your request is complete and ready for review.\n Here are the next steps!",
                review_process_further_audience: "Set my target audience",
                review_process_further_facebook: "Connect my Facebook Page",
                review_process_further_facebook_sdk: "Share my Facebook App",
                review_process_further_ga: "Connect my Google Analytics",
                review_process_later: "I�셪l do later",
                review_process_step_1: "Review by Adriel�셲 Marketing Team",
                review_process_step_2: "Review by Google or Facebook",
                review_process_step_3: "Your ads are up and running!",
                review_process_title: "Nice!",
                save_and_skip: "Save draft and skip",
                save_draft_and_continue: "Save draft and continue",
                schedule_end_date: "End Date",
                schedule_first_box_title: "Start running my ads ASAP",
                schedule_period_format: "Run for {value} days",
                schedule_popover: ' <div class="kr_text"> <p> <strong>It�셲 really important to run ads continuously, even if that means starting with a small daily budget.</strong><br> The longer they run, the more engagement your ads will gain and the more Adriel will be able to collect the data needed to optimize your campaigns. <br> Even with a relatively small budget, ads that run for more than a week see a 300% increase in performance on average. <br><br> <b>For Adriel to collect and learn from enough data, a minimum of 5 days is required. After a week you will notice the impact: boosted performance and increased sales conversions. </b> </p> </div> ',
                schedule_recommended: "Recommended",
                schedule_second_box_title: "Set a start and end date",
                schedule_start_date: "Start Date",
                schedule_title: "How long do you want your ads to run?",
                search_interests: "Enter audience interests or select the suggested ones",
                search_keywords: "Include or select the suggested keywords",
                selection_first_box_btn_text: "START",
                selection_first_box_desc: "Let Adriel�셲 marketing team create powerful ads for you.",
                selection_first_box_title: "Design it for me, Adriel!",
                selection_second_box_btn_text: "START",
                selection_second_box_desc: "Start with a few simple questions to create your ads.",
                selection_second_box_title: "I will design it myself",
                selection_top_title: "How do you want to create your ads?",
                skip: "Skip",
                submit: "Submit",
                summary_ads: "Ads",
                summary_budget_total: "Total",
                summary_comment_placeholder: "Additional Notes",
                summary_coupon: "Coupon",
                summary_credit: "Credit",
                summary_daily_budget_title: "Daily",
                summary_final_amount: "Your final payment amount",
                summary_locations: "Target Location",
                summary_mobile_app: "Mobile app",
                summary_payment_note: "Your budget spent for the first 3 days will be paid first. The actual advertizing cost will be totalled every 3 days during the period your ads are running. If the amount spent on the ad platforms are less than your budget, you will be reimbursed the difference.",
                summary_schedule: "Scheduled Date",
                summary_service_fee: "Service fee",
                summary_title: "Please review and launch your campaign.",
                summary_today_payment: "The amount you pay today {value}",
                summary_website: "Website",
                tracker_title: "You can also connect your Facebook Pixel / Google Analytics.",
                web_or_app_app_store: "Apple Appstore",
                web_or_app_developer_site: "Developer website (Optional)",
                web_or_app_first_box_title: "Website",
                web_or_app_play_store: "Google Playstore",
                web_or_app_second_box_title: "Mobile App",
                web_or_app_third_box_title: "Lead form",
                web_or_app_website_address: "Website address",
                website_url_popover: '<div class="kr_text""> <p> <b>Which website address should you enter? Either:</b> 쨌Your business website<br> 쨌The address of your E-commerce store (ex. Amazon, Etsy, Shopify.)<br> 쨌Any website you would like to advertize (ex. Blog)<br><br> <b>If you would like to advertize a Facebook page or Instagram account, please contact Adriel chat support.</b><br> <b>Quick tip from Adriel:</b> 쨌Enter the URL of the page that you want your customers to be directed to. The more relevant the page is to the ad creatives, the more effective it will be. </p> </div>',
                website_url_title: "What is your website?"
            },
            jp: {
                ad_common_placeholder: "��꺁�껁궚�쀣겍�녴궘�밤깉�믣뀯�쎼걮�╉걦�졼걬�꾠��",
                ad_types_title: "�딀쐹�욍겗佯껃몜��깤�⒲긿�덀깢�⒲꺖�졼굮\n �멩뒢�쀣겍�뤵걽�뺛걚��",
                ad_types_type_facebookFeed: "�뺛궒�ㅳ궧�뽧긿��",
                ad_types_type_googleDisplay: "�겹꺖�겹꺂�뉎궍�밤깤�с궎",
                ad_types_type_googleSearch: "�겹꺖�겹꺂濾쒐뇨",
                ad_types_type_instagramFeed: "�ㅳ꺍�밤궭�겹꺀��",
                ads_change_image: "�ㅳ깳�쇈궦鸚됪쎍",
                ads_change_media: "�ㅳ깳�쇈궦�얇걼��땿�삣쨯��",
                ads_edit: "渶③썓�쇻굥",
                ads_list_title: " 佯껃몜訝삥쭣��킔�담굢�쀣걚佯껃몜�뚥빳訝뗣겗�덀걝�ゆ꽏�섅겎�꾠걤�얇걲�� \n 濾쒑쮱�꾡엶閭ｅ풄���永귞쉪�ュ윿烏뚣걬�뚣굥佯껃몜�믧겦�욁걮�╉걦�졼걬�꾠��",
                ads_preview: " �쀣꺃�볝깷�� ",
                ads_title: "佯껃몜�믤쩂鼇롢굜\n 岳���쀣겍�뤵걽�뺛걚��",
                ads_upload_image: "�ㅳ깳�쇈궦�㏂긿�쀣꺆�쇈깋",
                ads_upload_logo: "��궡",
                ads_upload_media: "�ㅳ깳�쇈궦�얇걼�뺟뵽�㏂긿�쀣꺆�쇈깋",
                adsList_at_least_one: "訝��ㅴ빳訝듽겗佯껃몜�뚪겦�욁걬�뚣굥�밤걤�㎯걲��",
                app_url_app_store: "APP�밤깉��",
                app_url_first_box_title: "��걚��\n �㏂깘�ㅳ꺂�㏂깤�ゅ틕�듽겎�쇻��",
                app_url_play_store: "�겹꺖�겹꺂�쀣꺃�ㅳ궧�덀궋",
                app_url_second_box_title: "�꾠걚�덀�� \n �㏂깘�ㅳ꺂�㏂깤�ゃ겎��걗�듽겲�쎼굯��",
                app_url_title: "鵝뺛굮佯껃몜�쀣겲�쇻걢�� \n�╉궒�뽧궢�ㅳ깉�얇걼��깴�먦궎�ャ궋�쀣꺁",
                back: "�삠굥",
                budget_best_option: "鶯꿜툓遙섅겗罌쀥ㄷ��걼�곥겗���⒳틛嶸�",
                budget_minimum_budget: "��弱� {value}�뗣굢佯껃몜�룩죱�뚦룾�썬겎�쇻��",
                budget_most_used: "���귛닶�ⓦ걲�뗤틛嶸�",
                budget_popover: '<div class="kr_text"> <p> <b>�㏂깋�ゃ궓�ャ겘黎뷩굙�됥굦�잓뇫窈띲굮�→씉餓띈쳦黎귙걵�싥�곩츪�쎼겓�룩죱�뺛굦�잌틕�듽겗�욤げ�묆걣烏뚣굩�뚣겲�쇻�� </b> <br> 訝��δ틛嶸쀣걚�뤵굢�믢퐬�ι뼋鵝욜뵪�쇻굥�ⓩ군�곥굥�볝겏�㎯�곲�곲뇫窈띲굮�뉐츣�쇻굥壤℡폀�㎯�곥궋�됥꺁�ⓦ꺂��군�곥굢�뚣걼�묌죲�끹겎��鸚㎯겗��꺁�껁궚�겹걣�뷩굥�덀걝�ョ텤泳싩쉪�ユ��⒴뙑�믤뎸鴉앫걝壤밧돯�믤옖�잆걮�얇걲�� <br><br> 佯껃몜訝삥쭣�뚥��ζ�鸚㏝솏佯╉굮鼇�츣�쇻굥�ⓦ�곥궚�ゃ긿��겎雅덄츞易덂갹�ャ겒�뗣겏�곥걹��뿥��뼋�ャ겘�볝굦餓δ툓佯껃몜�뚪쑓�뷩걬�뚣겒�꾣뼶凉뤵겎�쇻�� �ㅳ겲�듽�곦��ι뼋易덂갹�뺛굦�뗥틕�듽겗�묌죲����δ틛嶸쀣굮擁끹걟�ゃ걚�쀣�곫�永귙겗黎뷸툑��셽��<strong>易덂갹�뺛굦�잋틛嶸쀣꺍鵝�13竊끾뎸�경뼑</strong> �뚩げ�쎼굢�뚣겍獄뗦콆�ⓦ걡��돂�꾠걬�뚣겲�쇻��<br><br> ��꺁�껁궚�섆쐴縕사뵪�묌죲��틟璵��곥궭�쇈궟�껁깉窈㎩��ュ퓶�섅겍�욃만�ョ빊�ゃ굤�� 揶믢퐪�ε뜕堊▲겘$ 0.02 - $ 1.00葉뗥벧��칱�꿔겎��쨯�꾠겎�쇻�� <br><br> 佯껃몜��쩀�뤵겗雅덄츞�믡걢�묆굥�삠겑�꿨눣�ⓦ궚�ゃ긿���곮빪�쏁럤�뚪쳵�얇굥�볝겏�뚥틟若잆겎�쇻걣�곫�閻뷩겒�욍꺖�꿔긿�덀겏榮졿쓲��뵻�꾠굮�싥걯��듅�뉎굮��鸚㎩뙑�쀣겍�곫뒘蘊뉐��방옖�믧쳵�곥굥�볝겏�뚣겎�띲겲�쇻�귙궋�됥꺁�㏂꺖�롢궓�ャ굮�룡쐿�꾠겓�⑴뵪�뺛굦�뗥틕�듾말��뼶�끹겗 <strong> ��깵�녈깪�쇈꺍�귙걼�듽겗亮녑쓦1�γ겗雅덄츞 </strong>��큵30�됥꺂�㎯걲�� </p> </div> ',
                budget_radio_format: "{value} / ��",
                budget_title: "1�δ틛嶸쀣굮�ε뒟�쀣겍�뤵걽�뺛걚��",
                can_change_ad_contents: " �ε뒟�쀣걼�끻젿���곥걚�ㅳ겎�귚엶閭ｅ룾�썬겎�쇻��",
                can_change_ad_platforms: "�멩뒢�쀣걼�쀣꺀�껁깉�뺛궔�쇈깲��걚�ㅳ겎�귛쨯�닷룾�썬겎�쇻��",
                can_change_anytime: " �ε뒟�쀣걼�끻젿���곥걚�ㅳ겎�귚엶閭ｅ룾�썬겎�쇻��",
                can_download: '<div class="kr_text"> <p> 佯껃몜訝삥쭣�� 獒썲뱚�꾠궢�쇈깛�밤겗怜▼뜕�よが�롢굜 <br> �뺛궔�쇈깲�먨눣孃뚣�곥겑��걪�ⓦ걣�꿱죱�뺛굦�뗣걢�믥가�섅겓�ε뒟�쀣겍�뤵걽�뺛걚��<br> �볝걪�ラ¨若㏂걣�끻젿�믤룓�뷩걲�뗣걪�ⓦ겓�덀굤孃쀣굢�뚣굥�⑴궧�ャ겇�꾠겍凉룩た�쇻굥�볝겏�뚧��귡뇥誤곥겎�쇻��</p> </div>',
                close_button: "孃뚣겎�쇻굥",
                copy_from_previous: "�띲겗佯껃몜�녈깞�쇈걲��",
                do_it_later: "孃뚣겎�쇻굥",
                done: "若뚥틙",
                facebook_feed_bottom_0: "�꾠걚��",
                facebook_feed_bottom_1: "�녈깳�녈깉",
                facebook_feed_bottom_2: "�길쐣�쇻굥",
                facebook_feed_id: "�볝궦�띲궧�싥꺖��",
                facebook_popover: '<div class="kr_text"> <p> 佯껃몜��궠�붵꺖�ⓦ궎�▲꺖�멥걣��걚�먩옖�믣눣�쇻걢�믥▶岳▲걲�뗣걪�ⓦ겘�곈썵�㎯걗�뗣걣�곩퓘�띲걮�ゃ걚�㎯걦�졼걬�꾠��<br> �ε뒟�뺛굦�� �끻젿���곩틕�듿윿烏뚦풄�귙�곥걚�ㅳ겎�귚엶閭ｃ걣��꺗�㎯걗�듽�곩틕�듿윿烏뚣겗誤곮쳦孃뚣겓�㏂깋�ゃ궋�ⓦ꺂�욁꺖�긱깇�ｃ꺍�겹긽�쇈깲�㎫킔�먦굮�с깛�γ꺖�쀣�곫뵻�꾠걮�얇걲��<br> 燁곥걼�▲겗�곥꺖�졼겎���곩틕�듽겗�먩옖�믣릲訝듽걬�쎼굥�잆굙��깢�с꺖�뷰엶閭ｃ굜榮졿쓲��룓旅덀굮泳숂텥�꾠겓�꾠걼�쀣겲�쇻�� <br><br> <b>礪섅�끹겒榮졿쓲�믢슴�ⓦ걮�잆걚�㎯걲�뗣��</b> 쨌�룩죱誤곮쳦餓ι솉�곥걹��궘�ｃ꺍�싥꺖�녈겓榮졿쓲�믦눎�긱겓瓦썲뒥�쀣�� A/B �녴궧�덀굮�꿱죱�㎯걤�얇걲�� <br><br> <b> �삣깗��렓也ⓧ퍞礪� </b> 쨌�ⓨⅷ�듐궎��: <strong>1080 * 1080�붵궚�삠꺂</strong><br> 쨌��弱뤵궢�ㅳ궨: <strong>500 * 500�붵궚�삠꺂</strong><br> 쨌�뺛궊�ㅳ꺂壤℡폀: <strong>JPEG, PNG</strong><br> <br> <b>�뺟뵽/�밤꺀�ㅳ깋�룔깾�� �ⓨⅷ餓뺞쭣 </b> 쨌�ⓨⅷ�룔걬: <strong>��鸚�15燁�</strong><br> 쨌��鸚㏝빓��: <strong>10��</strong><br> 쨌�삯씊驪붺럤: <strong> 潁�뼶�� (4:5)</strong><br> 쨌��: <strong>鵝욜뵪(��깵�쀣궥�㎯꺍�믣맜��)</strong><br> 쨌�뺛궊�ㅳ꺂壤℡폀: <strong>.mp4, .mov</strong><br> 쨌 鰲ｅ깗佯��弱뤷퉭: <strong>600�붵궚�삠꺂</strong><br> 쨌�뺛궊�ㅳ꺂�듐궎��: <strong>��鸚㏆폇G</strong><br> <br> �ㅳ깳�쇈궦�얇걼�뺟뵽�뚣궋�껁깤��꺖�됥걬�뚣겒�꾢졃�덀��<strong> 訝��귚퓷耶� </strong> �쒌궭�녈굮�쇈걮�잌풄�곥깢�▲궎�ャ굮 <a href="mailto:support@adriel.ai">support@adriel.ai</a>�면�곥겂�╉걦�졼걬�꾠�� 燁곥걼�▲겗�곥꺖�졼걣�㏂긿�쀣꺆�쇈깋�뺛걵�╉걚�잆걽�띲겲�쇻��</p> </div>',
                facebookpage_title: "�뺛궒�ㅳ궧�뽧긿��깪�쇈궦�ゃ꺍��",
                fb_app_title: "�뺛궒�ㅳ궧�뽧긿��궋�쀣꺁�믣뀻�됥걮�╉걦�졼걬�꾠��",
                finish_confirm: "閻븃첀",
                finish_content: "�с깛�γ꺖�ャ겘��鸚�24�귡뼋��誤곥걲�뗥졃�덀굚�귙굤�얇걲��",
                finish_go_back: "�삠굥",
                finish_title: "瓦썲뒥鵝쒏��얇겎�쇻겧��츑雅녴걮�얇걮�잆��",
                form_title: '<div class="kr_text"> <p> 逆쒎쑉�꾠겒窈㎩���떉�녴깢�⒲꺖�졼굮 <br> 鵝쒏닇�쀣겍�뤵걽�뺛걚�� </p> </div>',
                form_website_address: "�╉궒�뽧궢�ㅳ깉�㏂깋�с궧�ε뒟(�ゃ깤�룔깾��)",
                go_to_review_and_payment: "濾쒑쮱�꾠걡��돂�꾠깪�쇈궦�ョ㎉��",
                google_display_popover: '<div class="kr_text"> <p> 佯껃몜��궠�붵꺖�ⓦ궎�▲꺖�멥걣��걚�먩옖�믣눣�쇻걢�믥▶岳▲걲�뗣걪�ⓦ겘�곈썵�㎯걗�뗣걣�곩퓘�띲걮�ゃ걚�㎯걦�졼걬�꾠��<br> �ε뒟�뺛굦�� �끻젿���곩틕�듿윿烏뚦풄�귙�곥걚�ㅳ겎�귚엶閭ｃ걣��꺗�㎯걗�듽�곩틕�듿윿烏뚣겗誤곮쳦孃뚣겓�㏂깋�ゃ궋�ⓦ꺂�욁꺖�긱깇�ｃ꺍�겹긽�쇈깲�㎫킔�먦굮�с깛�γ꺖�쀣�곫뵻�꾠걮�얇걲��<br> 燁곥걼�▲겗�곥꺖�졼겎���곩틕�듽겗�먩옖�믣릲訝듽걬�쎼굥�잆굙��깢�с꺖�뷰엶閭ｃ굜榮졿쓲��룓旅덀굮泳숂텥�꾠겓�꾠걼�쀣겲�쇻�� <br><br> <b> 礪섅�끹겒榮졿쓲�믢슴�ⓦ걮�잆걚�㎯걲�뗣��</b> 쨌 �룩죱誤곮쳦餓ι솉�곥걹��궘�ｃ꺍�싥꺖�녈겓榮졿쓲�믦눎�긱겓瓦썲뒥�쀣�� A/B �녴궧�덀굮�꿱죱�㎯걤�얇걲�� <br><br> <b> �삣깗��렓也ⓧ퍞礪� </b> 쨌��弱뤵궢�ㅳ궨: <strong>600 * 314�붵궚�삠꺂</strong><br> 쨌�뺛궊�ㅳ꺂壤℡폀: <strong>JPEG, PNG</strong><br> <br> �ㅳ깳�쇈궦�얇걼�뺟뵽�뚣궋�껁깤��꺖�됥걬�뚣겒�꾢졃�덀�� <strong> 訝��귚퓷耶� </strong> �쒌궭�녈굮�쇈걮�잌풄�곥깢�▲궎�ャ굮<a href="mailto:support@adriel.ai">support@adriel.ai</a>�면�곥겂�╉걦�졼걬�꾠�� 燁곥걼�▲겗�곥꺖�졼걣�㏂긿�쀣꺆�쇈깋�뺛걵�╉걚�잆걽�띲겲�쇻��</p> </div> ',
                google_search_add_description: "佯껃몜�녴궘�밤깉�ε뒟 (�멩뒢)",
                google_search_add_description_placeholder: " 佯껃몜�녴궘�밤깉�ε뒟 ",
                google_search_add_title: "佯껃몜�욍궎�덀꺂瓦썲뒥 (�멩뒢)",
                google_search_add_title_placeholder: "�욍궎�덀꺂�ε뒟",
                google_search_default_title: "�욍궎�덀꺂",
                google_search_description_1: "沃ф삇 (1)",
                google_search_popover: '<div class="kr_text"> <p> 濾쒐뇨��꺖��꺖�됥겘�곥궋�됥꺁�ⓦ꺂�곥꺖�졼겎�멨츣�듽겞�ε뒟�꾠걼�쀣겲�쇻�귙굚�▲굧�볟틕�듿윿烏뚣겗孃뚣�곩틕�듾말�뚨쎍�γ궘�쇈꺈�쇈깋�믥▶沃띲걮�곦엶閭ｃ�곮옙�졼걲�뗣걪�ⓦ걣�㎯걤�얇걲<br> 佯껃몜��궠�붵꺖�ⓦ궎�▲꺖�멥걣��걚�먩옖�믣눣�쇻걢�믥▶岳▲걲�뗣걪�ⓦ겘�곈썵�㎯걗�뗣걣�곩퓘�띲걮�ゃ걚�㎯걦�졼걬�꾠��<br> �ε뒟�뺛굦�� �끻젿���곩틕�듿윿烏뚦풄�귙�곥걚�ㅳ겎�귚엶閭ｃ걣��꺗�㎯걗�듽�곩틕�듿윿烏뚣겗誤곮쳦孃뚣겓�㏂깋�ゃ궋�ⓦ꺂�욁꺖�긱깇�ｃ꺍�겹긽�쇈깲�㎫킔�먦굮�с깛�γ꺖�쀣�곫뵻�꾠걮�얇걲��<br> 燁곥걼�▲겗�곥꺖�졼겎���곩틕�듽겗�먩옖�믣릲訝듽걬�쎼굥�잆굙��깢�с꺖�뷰엶閭ｃ굜榮졿쓲��룓旅덀굮泳숂텥�꾠겓�꾠걼�쀣겲�쇻��<br> <br> <b> 礪섅�끹겒榮졿쓲�믢슴�ⓦ걮�잆걚�㎯걲�뗣��</b> 쨌 �룩죱誤곮쳦餓ι솉�곥걹��궘�ｃ꺍�싥꺖�녈겓榮졿쓲�믦눎�긱겓瓦썲뒥�쀣�� A/B �녴궧�덀굮�꿱죱�㎯걤�얇걲�� <br> <br> �ㅳ깳�쇈궦�얇걼�뺟뵽�뚣궋�껁깤��꺖�됥걬�뚣겒�꾢졃�덀�� <strong> 訝��귚퓷耶� </strong> �쒌궭�녈굮�쇈걮�잌풄�곥깢�▲궎�ャ굮<a href="mailto:support@adriel.ai">support@adriel.ai</a>�면�곥겂�╉걦�졼걬�꾠�� 燁곥걼�▲겗�곥꺖�졼걣�㏂긿�쀣꺆�쇈깋�뺛걵�╉걚�잆걽�띲겲�쇻��</p> </div>',
                instagram_feed_id: "�볝궦�띲궧�싥꺖��",
                instagram_feed_learn_more: "�귙겂�ⓦ겳��",
                instagram_popover: '<div class="kr_text"> <p> 佯껃몜��궠�붵꺖�ⓦ궎�▲꺖�멥걣��걚�먩옖�믣눣�쇻걢�믥▶岳▲걲�뗣걪�ⓦ겘�곈썵�㎯걗�뗣걣�곩퓘�띲걮�ゃ걚�㎯걦�졼걬�꾠��.<br> �ε뒟�뺛굦�잍깄�긱겘�곩틕�듿윿烏뚦풄�귙�곥걚�ㅳ겎�귚엶閭ｃ걣��꺗�㎯걗�듽�곩틕�듿윿烏뚣겗誤곮쳦孃뚣겓�㏂깋�ゃ궋�ⓦ꺂�욁꺖�긱깇�ｃ꺍�겹긽�쇈깲�㎫킔�먦굮�с깛�γ꺖�쀣�곫뵻�꾠걮�얇걲��<br> 燁곥걼�▲겗�곥꺖�졼겎���곩틕�듽겗�먩옖�믣릲訝듽걬�쎼굥�잆굙��깢�с꺖�뷰엶閭ｃ굜榮졿쓲��룓旅덀굮泳숂텥�꾠겓�꾠걼�쀣겲�쇻��<br><br> <b> 礪섅�끹겒榮졿쓲�믢슴�ⓦ걮�잆걚�㎯걲�뗣��</b> 쨌 �룩죱誤곮쳦餓ι솉�곥걹��궘�ｃ꺍�싥꺖�녈겓榮졿쓲�믦눎�긱겓瓦썲뒥�쀣�� A/B �녴궧�덀굮�꿱죱�㎯걤�얇걲��<br><br> <b> �삣깗��렓也ⓧ퍞礪� </b> 쨌 �ⓨⅷ�듐궎��: <strong>1080 * 1080�붵궚�삠꺂</strong><br> 쨌 ��弱뤵궢�ㅳ궨: <strong>500 * 500�붵궚�삠꺂</strong><br> 쨌�뺛궊�ㅳ꺂壤℡폀: <strong>JPEG, PNG</strong><br> <br> <b> �뺟뵽/�밤꺀�ㅳ깋�룔깾�� �ⓨⅷ餓뺞쭣 </b> 쨌�ⓨⅷ�룔걬: <strong>��鸚�15燁�</strong><br> 쨌��鸚㏝빓��: <strong>1��</strong><br> 쨌�삯씊驪붺럤: <strong>潁�뼶�� (4:5)</strong><br> 쨌��: <strong>鵝욜뵪(��깵�쀣궥�㎯꺍�믣맜��)</strong><br> 쨌�뺛궊�ㅳ꺂壤℡폀: <strong>.mp4, .mov</strong><br> 쨌 鰲ｅ깗佯��弱뤷퉭: <strong>600�붵궚�삠꺂</strong><br> 쨌�뺛궊�ㅳ꺂�듐궎��: <strong>��鸚㏆폇G</strong><br> <br> �ㅳ깳�쇈궦�얇걼�뺟뵽�뚣궋�껁깤��꺖�됥걬�뚣겒�꾢졃�덀��<strong> 訝��귚퓷耶� </strong> �쒌궭�녈굮�쇈걮�잌풄�곥깢�▲궎�ャ굮 <a href="mailto:support@adriel.ai">support@adriel.ai</a>�면�곥겂�╉걦�졼걬�꾠�� 燁곥걼�▲겗�곥꺖�졼걣�㏂긿�쀣꺆�쇈깋�뺛걵�╉걚�잆걽�띲겲�쇻�� </p> </div> ',
                interest_tooltip: "�뺛궒�ㅳ궧�뽧긿��꺕�ㅳ꺍�밤궭�겹꺀�졼겗佯껃몜�룩죱��셽�밧츣�덂뫑�꾤릤鰲ｉ뼟岳귙굮�곥겂�╉걚�뗤볶�끹겓佯껃몜�믡걮�얇걲��",
                interests_input_placeholder: "�욍꺖�꿔긿�덆¨若�늿�녔쩂榮�",
                interests_input_placeholder_empty_tag: "�욍꺖�꿔긿�덆¨若�늿�녔쩂榮�",
                keyword_title: "�먩죭�뺛굦�잌틕�딀쩂榮㏂궘�쇈꺈�쇈깋�ⓦ궭�쇈궟�껁깉窈㎩���늿�녈굮濾쒑쮱�①▶沃띲걮�╉걦�졼걬�꾠��",
                keyword_tooltip: "�겹꺖�겹꺂濾쒐뇨佯껃몜�룩죱�쇻굥�ⓦ걤�곲겦�욁걲�뗣궘�쇈꺈�쇈깋�③뼟�ｃ걬�뚣걼濾쒐뇨�믡걲�뗤볶�끹겓佯껃몜�믡걮�얇걲�� ",
                keywords_input_placeholder: "濾쒐뇨��꺖��꺖�됧뀯��",
                keywords_input_placeholder_empty_tag: "濾쒐뇨��꺖��꺖�됧뀯��",
                launch_campaign: "黎뷸툑�ⓨ윿烏뚩쫨獄�",
                list_of_suggested_interests: " 佯껃몜訝삥쭣��깛�멥깓�밤겓�덀겂�잍쩂榮㏂궘�쇈꺈�쇈깋�②뼣�ゃ궧�덀겎�� ��",
                list_of_suggested_keywords: "佯껃몜訝삥쭣��깛�멥깓�밤겓�덀겂�잍쩂榮㏂궘�쇈꺈�쇈깋�②뼣�ゃ궧�덀겎�� ��",
                location_input_placeholder: "�욍꺖�꿔긿�덂쑑�잌뀯��",
                location_input_placeholder_empty_tag: "�욍꺖�꿔긿�덂쑑�잌뀯��",
                location_popover: '<div class="kr_text"> <p> <b>�㏂깋�ゃ궓�ャ굮 �싥걵�겹�곦툟�뚥릎��겑�볝겓�㎯굚佯껃몜�믤렡雍됥걲�뗣걪�ⓦ걣�㎯걤�얇걲�귙겲�잆�곩쩀�뤵겗�겼윜�믦쨭�겼뀯�쎼걲�뗣걪�ⓦ걣�㎯걤�얇걲��</b> <br> <b>�㏂깋�ゃ궓�� TIP!</b> 쨌 �딀렋�쀣겗�밧츣��쑑�잆걣濾쒎눣�뺛굦�ゃ걚�뗣�곥겑��굠�녴겒�겼윜�믣뀯�쎼걲�뗥퓚誤곥걣�ゃ걚�뤵걢�됥겒�꾢졃�덀겘�곩쎖�섆퐤�㎩쑑�잆굮�멩뒢�쀣겍�뤵걽�뺛걚��<br> 쨌 佯껃몜�룩죱��풄�곲¨若㏂궢�쇈깛�밤궩�녈궭�쇈겲�㎯걡�뤵걚�덀굩�쎼걦�졼걬�ｃ걼�됥�곥걚�뤵굢�㎯굚岳���꾠걼�쀣겲�쇻��<br> 쨌 窈㎩��듐꺖�볝궧�삠꺍�욍꺖 ��걡�뤵걚�덀굩�쎼굮餓뗣걮��돶若싥겗�겼윜��뜇孃�5km�ゃ겑�곫쎍�ユ�閻뷩겒�겼윜�믡궭�쇈궟�녴궍�녈궛�뚣겎�띲겲�쇻��<br> 쨌 �띹쨭�쀣겍�꾠굥�겼윜�믣뀯�쎼걮�ゃ걚�㎯걦�졼걬�꾠��<br> (堊�: �볟쎖�③윋�썬겗�썬궑�ャ굮�뚧셽�ュ뀯�쎼걮�ゃ걚�㎯걦�졼걬�꾠��) </p> </div>',
                location_title: "�⒲겗�겼윜��\n 佯껃몜�쀣걼�꾠겎�쇻걢��",
                need_fb_page: "�뺛궒�ㅳ궧�뽧긿���� �ㅳ꺍�밤궭�겹꺀�졼겗佯껃몜��걼�곥겓恙낁쫨�㎯걲��",
                next: "轝�",
                no_ads_edit_ad: "�삠굥\n 渶③썓若뚥틙.",
                no_ads_title: "佯껃몜�룩죱��걼�곥겓��\n ��弱뤶�葉�줊餓δ툓�� \n 佯껃몜榮졿쓲�뚦츑雅녴겲�잏▶沃�\n �뺛굦�뗣겧�띲겎�쇻��",
                objectives: "邕€룢��쮽�믦Þ若싥걮�╉걦�졼걬�꾠��",
                progress_text: "{value}% �꿱죱",
                review_process_additional_facebook: " �귙걝訝��ㅿ펯<br/><b>�뺛궒�ㅳ궧�뽧긿��깪�쇈궦�믡꺁�녈궚�쀣겍�뤵걽�뺛걚��</b>",
                review_process_details: "佯껃몜�룩죱誤곮쳦�뚧닇�잏쉪�ュ츑雅녴걮�얇걮�잆��\n 佯껃몜�룩죱�롧쮮��А�ャ겒�듽겲�쇻��",
                review_process_further_facebook: "餓듽꺁�녈궚�쇻굥",
                review_process_later: "孃뚣겎�쇻굥",
                review_process_step_1: "�㏂깋�ゃ궓�ャ깯�쇈궞�녴궍�녈궛�곥꺖�졼겗\n 1轝→쩂鼇�",
                review_process_step_2: "�꾢틕�듽깤�⒲긿�덀깢�⒲꺖�졼겗\n 2轝→쩂鼇�",
                review_process_step_3: "佯껃몜�룩죱��\n ���⒴뙑",
                review_process_title: "�듽굙�㎯겏�녴걫�뽧걚�얇걲竊�",
                save_and_skip: "訝��귚퓷耶섅걮�곥궧��긿��",
                save_draft_and_continue: "訝��귚퓷耶섅걮�곩풄�㎯걲�� ",
                schedule_end_date: "永귚틙",
                schedule_first_box_title: "餓딀뿥�뗣굢�싥겂�ⓨ틕�듽걲��",
                schedule_period_format: "{value}�ι뼋�룩죱",
                schedule_popover: ' <div class="kr_text"> <p> <strong>佯껃몜��컩窈띲겎�귙겂�╉굚��若잆겓�꿔굙�뗣걪�ⓦ걣�띹쫨�㎯걲��</strong> �룔걚�볝�곩틕�듽걣�꿨눣�뺛굦�뗣겏�곥궠�▲꺍�덀굜�꾠걚��겗�겹겗罌쀥뒥�ⓩ��⒲궭�쇈궟�껁깉�곫��⒲깤�⒲긿�덀깢�⒲꺖�졼굮�㏂걮�╉걚�뤷�玲믡걣�꿱죱�뺛굦�곩틕�듽겗�밭럤�뚨텤泳싩쉪�ユ뵻�꾠걬�뚣굥�방옖�뚣걗�듽겲�쇻��<br> 1�γ겗雅덄츞�믣컩�ゃ걦鼇�츣�쀣겍�귙��1�깁뼋餓δ툓�룔걦�곩틕�듽굮��若잆겓�룩죱�뺛굦�뗥졃�덀겓���곭뎠鼇덄쉪�ュ뭄��3�띴빳訝듽겗佯껃몜��닇�쒌걣�밧뻹�뺛굦�얇걲��<br><br> <b>佯껃몜�쀣꺀�껁깉�뺛궔�쇈깲���玲믤쐿�볝�곥궋�됥꺁�ⓦ꺂雅뷴램�θ꺗�ⓦ꺍�멥꺍���玲믤쐿�볝겗�쇻겧��컩�ゃ걦�ⓦ굚5�γ겭�⒲걢�뗣굤�곦빳�띲�곩츪�쎼겗蘊쇠껭邕€룢�ゃ겑�곩츪蘊ょ쉪�ゅ틕�듿듅�쒌굮鵝볣쮶�쇻굥�볝겏�뚣겎�띲겲�� ��</b> </p> </div> ',
                schedule_recommended: "鶯꿜툓遙섅겗罌쀥뒥��걼�곥겗���⒵쐿��",
                schedule_second_box_title: "�룩죱�잓뼋�믤뙁若싥걲��",
                schedule_start_date: "�뗥쭓",
                schedule_title: "佯껃몜�η쮮�믣뀯�쎼걮�╉걦�졼걬�꾠��",
                search_interests: "濾쒐뇨��꺖��꺖��",
                search_keywords: "濾쒐뇨��꺖��꺖�� ",
                selection_first_box_btn_text: "�딃줁��",
                selection_first_box_desc: "�듿퓳�쀣걚佯껃몜訝삥쭣�ヤ빰�뤵겂�╉�곩틕�딁킔�먬겦若싥걢�됥궩�껁깇�ｃ꺍�겹겲�㎯궋�됥꺁�㏂궓�ャ걣�꾠걼�쀣겲�쇻��",
                selection_first_box_title: "�㏂깋�ゃ궓�ャ걣\n �쀣겍�뤵걽�뺛걚��",
                selection_second_box_btn_text: "�뗥쭓",
                selection_second_box_desc: "怜▼뜕�よ나�묈퓶嶺붵굮�싥걯��틕�듽굮�뜸퐳�쀣겲�쇻��",
                selection_second_box_title: "�닸렏\n �뜸퐳�쀣겍�욍겲�쇻��",
                selection_top_title: "�⒲굯�ゆ뼶力뺛겎\n 佯껃몜�믣댍鵝쒌걮�얇걲�뗣��",
                skip: "�밤궘�껁깤 ",
                submit: "�딀뵱�뺛걚�ⓨ윿烏뚩쫨獄�",
                summary_ads: "佯껃몜�쀣꺀�껁깉�뺛궔�쇈깲",
                summary_budget_total: "渶뤺꼇��",
                summary_comment_placeholder: "餓뽧겗誤곮쳦雅뗩쟿",
                summary_coupon: "��꺖�앫꺍",
                summary_credit: "��꺃�멥긿��",
                summary_daily_budget_title: "訝��δ틛嶸�",
                summary_final_amount: "��永귙걡��돂�꾦뇫窈�",
                summary_locations: "�욍꺖�꿔긿�덂쑑��",
                summary_mobile_app: "�㏂깘�ㅳ꺂�㏂깤�ゅ틕��",
                summary_payment_note: "訝됪뿥雅덄츞�뚦뀍�ユ군歷덀걬�뚣�곩츪�쎼겓佯껃몜�뚦윿烏뚣걬�뚣겍�꾠굥�잓뼋訝�겓3�γ걫�ⓨ츪��틕�딃뇫窈띲걣暎양츞�뺛굦�얇걲�귝군歷덀걬�뚣걼雅덄츞�덀굤�귛컩�ゃ걦鵝욜뵪�쇻굥�닷릦���곦��ⓦ겗�욤첀�뽧굤易덀걮�ⓦ겒�듽겲�쇻�� ",
                summary_schedule: "�η쮮",
                summary_service_fee: "�듐꺖�볝궧縕사뵪",
                summary_title: "濾쒑쮱�ⓦ걡��돂��",
                summary_today_payment: "餓딀뿥��걡��돂�꾦뇫窈� {value}",
                summary_website: "�╉궒�뽧궢�ㅳ깉",
                tracker_title: " �겹꺖�겹꺂�삠궋�듽꺁�녴궍��궧�ⓦ깞��궩�ュ뀻�� ",
                web_or_app_app_store: "APP�밤깉��",
                web_or_app_developer_site: "�뗧쇇�끹궢�ㅳ깉(�멩뒢)",
                web_or_app_first_box_title: "�╉궒�뽧궢�ㅳ깉",
                web_or_app_play_store: "�겹꺖�겹꺂�쀣꺃�ㅳ궧�덀궋",
                web_or_app_second_box_title: "�㏂깘�ㅳ꺂�㏂깤��",
                web_or_app_third_box_title: "逆쒎쑉窈㎩�\n閻뷴젿�뺛궔�쇈깲",
                web_or_app_website_address: "�╉궒�뽧궢�ㅳ깉�㏂깋�с궧�ε뒟",
                website_url_popover: '<div class="kr_text""> <p> <b>�⒲겗�╉궒�뽧궢�ㅳ깉��궋�됥꺃�밤굮�ε뒟�쀣겲�쇻걢��</b> 쨌�や퐪�╉궒�뽧궢�ㅳ깉�ゃ꺍��<br> 쨌NAVER�밤깯�쇈깉�밤깉�㏂꺁�녈궚<br> 쨌 NAVER�밤깯�쇈깉�쀣꺃�ㅳ궧�ゃ꺍��<br> 쨌G�욁꺖�긱긿��, �㏂깯�얇꺍�ゃ겑�ゃ꺖�쀣꺍�욁꺖�긱긿�덀꺁�녈궚<br> 쨌�뽧꺆��, CAF횋�ゃ꺍��<br> 쨌餓뽧겗佯껃몜�쀣꺀�껁깉�뺛궔�쇈깲�� �띈즯誤곭킔�뚣겒�꾠꺁�녈궚<br> <br> <b>轝▲겗�╉궒�뽧궢�ㅳ깉�ゃ꺍��겗�닷릦窈㎩��듐깮�쇈궭�쇈궩�녈궭�쇈겓�듿븦�꾢릦�뤵걵�뤵걽�뺛걚��</b> 쨌�뺛궒�ㅳ궧�뽧긿��깪�쇈궦<br> 쨌NAVER�겼쎋�ゃ꺍��<br><br> <b>�㏂깋�ゃ궓�� TIP!</b> 쨌雅뷩�끹걣 佯껃몜�믡궚�ゃ긿��걮�╉�곫탛�γ걲�뗣궑�㎯깣�듐궎�덀굮�ε뒟�쀣겍�뤵걽�뺛걚�귛틕�듽겗�끻��①킔�먦겏��쎍�γ겗���ｆ�㎯걣�귙굥�㏂깋�с궧�믣뀯�쎼걮�╉걚�잆걽�묆굦�겹�곩틕�듿듅�쒌굮遙섅굙�뗣겗�ュ슝塋뗣걾�얇걲��</p> </div> ',
                website_url_title: "WEB�듐궎�덀겗�㏂깋�с궧��\n �γ굢�쎼겍�뤵걽�뺛걚��"
            },
            ko: {
                ad_common_placeholder: "�대┃�댁꽌 �띿뒪�몃� �낅젰�댁＜�몄슂",
                ad_types_title: "�먰븯�쒕뒗 愿묎퀬 �뚮옯�쇱쓣\n �좏깮�댁＜�몄슂.",
                ad_types_type_appleSearch: "�좏뵆 �쒖튂",
                ad_types_type_facebookFeed: "�섏씠�ㅻ턿",
                ad_types_type_googleDisplay: "援ш� �붿뒪�뚮젅��",
                ad_types_type_googleSearch: "援ш� 寃���",
                ad_types_type_googleUniversalApp: "援ш� �좊땲踰꾩꽕 ��",
                ad_types_type_instagramFeed: "�몄뒪��洹몃옩",
                ads_change_image: "�대�吏� 蹂�寃�",
                ads_change_media: "�대�吏� �먮뒗 鍮꾨뵒�� 蹂�寃�",
                ads_edit: "�몄쭛�섍린",
                ads_list_mobile_dirty: "紐⑤뱺 �꾩닔 �뺣낫媛� �낅젰�섏뿀�붿� �뺤씤 �� 二쇱꽭��.",
                ads_list_title: "愿묎퀬二쇰떂�� 硫뗭쭊 愿묎퀬媛� �꾨옒�� 媛숈� �먮굦�쇰줈 �섍컩�덈떎. \n 寃��� 諛� �섏젙 �� 理쒖쥌 吏묓뻾�� 愿묎퀬瑜� �좏깮�댁＜�몄슂.",
                ads_preview: "誘몃━蹂닿린",
                ads_title: "愿묎퀬瑜� 寃��� 諛�\n �섏젙�댁＜�몄슂.",
                ads_upload_image: "�대�吏� �낅줈��",
                ads_upload_logo: "濡쒓퀬",
                ads_upload_media: "�대�吏� �먮뒗 鍮꾨뵒�� �낅줈��",
                adsList_at_least_one: "�섎굹 �댁긽�� 愿묎퀬媛� �좏깮�섏뼱�� �⑸땲��.",
                app_url_app_store: "�깆뒪�좎뼱",
                app_url_first_box_title: "��,\n 紐⑤컮�� �� 愿묎퀬�낅땲��",
                app_url_play_store: "援ш� �뚮젅�댁뒪�좎뼱",
                app_url_second_box_title: "�꾨땲��, \n 紐⑤컮�� �깆씠 �꾨떃�덈떎.",
                app_url_title: "臾댁뾿�� 愿묎퀬�섏떆寃좎뼱��? \n�뱀궗�댄듃 �먮뒗 紐⑤컮�� ��",
                apple_search_warning: "�� 愿��� �뺣낫�� App Store Connect �먯꽌留� �섏젙 媛��ν빀�덈떎.",
                audience_title: "��寃� 怨좉컼 �뺣낫瑜� �뺤씤�댁＜�몄슂.",
                back: "�ㅻ줈",
                budget_best_option: "留ㅼ텧 利앸�瑜� �꾪븳 理쒖쟻 �덉궛",
                budget_minimum_budget: "理쒖냼 {value}遺��� 愿묎퀬 吏묓뻾�� 媛��ν빀�덈떎.",
                budget_most_used: "媛��� 留롮씠 �댁슜�섎뒗 �덉궛",
                budget_popover: '<div class="kr_text"> <p> <b>�꾨뱶由ъ뿕�� �뺥빐吏� 湲덉븸�� 臾댁“嫄� 泥�뎄�섏� �딆쑝硫�, �ㅼ젣濡� 吏묓뻾�� 愿묎퀬�� ���댁꽌留� 怨쇨툑�� �대（�댁쭛�덈떎.</b> <br> 1�� �덉궛 �쇰쭏瑜� 紐� �쇰룞�� �ъ슜�섍린濡� 寃곗젙�⑥쑝濡쒖뜥 珥� 湲덉븸�� 吏��뺥븯�쒕뒗 �뺤떇�대ŉ, �꾨뱶由ъ뿕�� �뺥빐吏� 湲덉븸 �댁뿉�� 理쒕��� �대┃�섍� �섏삱 �� �덈룄濡� 怨꾩냽 理쒖쟻�붾� �꾩��쒕━�� ��븷�� �섑뻾�⑸땲��. <br><br> 愿묎퀬二쇰떂�� 1�� 理쒕� �쒕룄瑜� �ㅼ젙�섏떆硫�, �대┃�� �쇱뼱�� �덉궛 �뚯쭊�� �� �� �� 洹� �� �숈븞�먮뒗 �� �댁긽 愿묎퀬媛� �몄텧�섏� �딅뒗 諛⑹떇�낅땲��.利� �섎（�숈븞 �뚯쭊�섎뒗 愿묎퀬 湲덉븸�� 1�� �덉궛�� 珥덇낵�섏� �딆쑝硫�. 理쒖쥌 寃곗젣 �� <strong>�뚯쭊�� �덉궛�� 13% �섏닔猷�</strong>媛� 遺�怨쇰릺�� 泥�뎄 諛� 寃곗젣�⑸땲��. <br><br> �대┃�� 鍮꾩슜 湲덉븸�� �ъ뾽, ��寃� 怨좉컼�� �곕씪 留ㅼ슦 �щ씪吏�硫�, 留ㅼ껜蹂� �대┃�� 鍮꾩슜�� $0.02 - $1.00 �뺣룄�� 踰붿쐞�먯꽌 媛�蹂��곸엯�덈떎. <br><br> 愿묎퀬�� 留롮� �덉궛�� �ㅼ씪�섎줉 �몄텧怨� �대┃, 援щℓ �꾪솚�⑥씠 �믪븘吏��� 寃껋씠 �ъ떎�대굹, �뺥솗�� ��寃잜똿怨� �뚯옱 媛쒖꽑�� �듯빐 �⑥쑉�� 洹밸��뷀븯�� �ъ옄 ��鍮� �④낵瑜� �믪씪 �� �덉뒿�덈떎. �꾨뱶由ъ뿕�� �κ린�곸쑝濡� �댁슜�섏떆�� 愿묎퀬二� 遺꾨뱾�� <strong>罹좏럹�� �� �됯퇏 1�� �덉궛</strong>�� �� 30�щ윭�낅땲��. </p> </div> ',
                budget_radio_format: "{value} / ��",
                budget_title: "1�� �덉궛�� �낅젰�댁＜�몄슂.",
                can_change_ad_contents: "�낅젰�섏떊 �뺣낫�� �몄젣�좎� �섏젙 媛��ν빀�덈떎.",
                can_change_ad_platforms: "�좏깮�섏떊 �뚮옯�쇱� �몄젣�좎� 蹂�寃� 媛��ν빀�덈떎.",
                can_change_anytime: "�낅젰�섏떊 �뺣낫�� �몄젣�좎� �섏젙 媛��ν빀�덈떎.",
                can_download: "愿묎퀬二쇰떂�� �ъ뾽�� ���� 媛꾨떒�� �ㅻ챸, �먮뒗 紐⑥쑝怨좎옄 �섎뒗 怨좉컼 �뺣낫瑜� �뚮젮二쇱꽭��. 醫뗭� 怨좉컼DB �섏쭛 愿묎퀬�� �뺣낫瑜� �④��쇰줈�� �쒓났�섎뒗 �쒗깮�� �� 媛뺤“�⑸땲��.",
                close_button: "�섏쨷�� 留뚮뱾湲�",
                copy_from_previous: "�� 愿묎퀬 蹂듭궗�섍린",
                do_it_later: "�섏쨷�� �섍린",
                done: "�꾨즺",
                facebook_feed_bottom_0: "醫뗭븘��",
                facebook_feed_bottom_1: "�볤��ш린",
                facebook_feed_bottom_2: "怨듭쑀�섍린",
                facebook_feed_id: "鍮꾩쫰�덉뒪 �섏씠吏�",
                facebook_popover: '<div class="kr_text"> <p> 愿묎퀬 臾멸뎄�� �대�吏�媛� 醫뗭� �깃낵瑜� �� 吏� �뺤떊�섍린 �대졄�붾씪��, 嫄깆젙�섏� 留덉꽭��.<br> �낅젰�섏떊 �뺣낫�� 愿묎퀬 吏묓뻾 �꾩뿉�� �몄젣�좎� �섏젙�� 媛��ν븯硫� 愿묎퀬 吏묓뻾 �붿껌 �� �꾨뱶由ъ뿕 留덉��� ���먯꽌 �뚯옱瑜� 由щ럭�섍퀬 媛쒖꽑�⑸땲��.<br> ���� ���먯꽌 愿묎퀬 �깃낵 �μ긽�� �꾪븳 臾멸뎄 �섏젙 諛� �뚯옱 �쒖븞�� 吏��띿쟻�쇰줈 �대뱶由쎈땲��. <br><br> <b>�щ윭媛�吏� �뚯옱瑜� �ъ슜�섍퀬 �띠쑝�좉���?</b> 쨌吏묓뻾 �붿껌 �댄썑 �대떦 罹좏럹�몄뿉 �뚯옱瑜� �먯쑀濡�쾶 異붽��섏뿬 A/B �뚯뒪�몃� 吏꾪뻾�섏떎 �� �덉뒿�덈떎. <br><br> <b>�대�吏� 沅뚯옣 �ъ뼇</b> 쨌沅뚯옣 �ъ씠利�: <strong>1080 * 1080�쎌�</strong><br> 쨌理쒖냼 �ъ씠利�: <strong>500 * 500�쎌�</strong><br> 쨌�뚯씪 �뺤떇: <strong>JPEG, PNG</strong><br> <br> <b>�숈쁺��/�щ씪�대뱶�� 沅뚯옣 �ъ뼇</b> 쨌沅뚯옣 湲몄씠: <strong>理쒕� 15珥�</strong><br> 쨌理쒕� 湲몄씠: <strong>60遺�</strong><br> 쨌�붾㈃ 鍮꾩쑉: <strong>�몃줈諛⑺뼢 (4:5)</strong><br> 쨌�뚮━: <strong>�ъ슜(罹≪뀡 �ы븿)</strong><br> 쨌�뚯씪�뺤떇: <strong>.mp4, .mov</strong><br> 쨌�댁긽�� 理쒖냼 �덈퉬: <strong>600�쎌�</strong><br> 쨌�뚯씪 �ш린: <strong>理쒕� 4湲곌�</strong><br> <br> �대�吏� �먮뒗 鍮꾨뵒�ㅺ� �낅줈�� �섏� �딅뒗�ㅻ㈃, <strong>�꾩떆����</strong> 踰꾪듉�� �꾨Ⅴ�� �� �뚯씪�� <a href="mailto:support@adriel.ai">support@adriel.ai</a>濡� 蹂대궡二쇱꽭��. ���� ���� �낅줈�쒗빐 �쒕━寃좎뒿�덈떎. </p> </div>',
                facebookpage_title: "�섏씠�ㅻ턿 �섏씠吏� �곌껐",
                fb_app_title: "�섏씠�ㅻ턿 �깆쓣 怨듭쑀�댁＜�몄슂.",
                finish_confirm: "�뺤씤",
                finish_content: "由щ럭�먮뒗 理쒕� 24�쒓컙源뚯� �뚯슂 �� �� �덉뒿�덈떎.",
                finish_go_back: "�ㅻ줈媛�湲�",
                finish_title: "異붽� �묒뾽源뚯� 紐⑤몢 �꾨즺�섏뿀�듬땲��.",
                form_popover: '<div class="kr_text""> <p>�좎옱 怨좉컼�� 愿묎퀬瑜� �댁슜�섎㈃ �좎옱 怨좉컼�쇰줈遺��� �뺣낫瑜� �섏쭛�� �� �덉뒿�덈떎. �섏쭛 媛��ν븳 �뺣낫濡쒕뒗 �대쫫, �대찓�� 二쇱냼, �꾪솕踰덊샇 �깆씠 �덉쑝硫� �댁뿉 �쒗븳�섏� �딆뒿�덈떎. �먰븳 以묒슂�섍쾶 �앷컖�섎뒗 留욎땄 吏덈Ц�� �щ엺�ㅼ뿉寃� �쒖떆�� �� �덉뒿�덈떎.<br/><br/>�좎옱 怨좉컼�� 愿묎퀬�� �댁뒪�덊꽣 �좎껌, 寃ъ쟻�� 諛� 鍮꾩쫰�덉뒪 �뺣낫瑜� �섏쭛�섎뒗 �� �쒖슜�� �� �덉쑝硫�, �좎옱 怨좉컼�� �뚯븙�섏뿬 �ν썑�� 愿��� �뺣낫瑜� 怨듭쑀�� �� �덉뒿�덈떎.<br/><br/>�좎옱 怨좉컼 �뺣낫 �ㅼ슫濡쒕뱶�� 愿묎퀬 �곗묶 �� <b class="formPopover">�쒓킅怨� 寃곌낵 蹂닿린��</b>�먯꽌 �ㅼ슫濡쒕뱶�섏떎 �� �덉뒿�덈떎.                                                                <br/><br/><b class="formPopover">�대�吏� 沅뚯옣 �ъ뼇</b><br/>쨌�대�吏� 鍮꾩쑉 : <b class="formPopover">2.4:1</b><br/>쨌沅뚯옣 �댁긽�� : <b class="formPopover">1000px*417px</b><br/>쨌�뚯씪 �뺤떇 : <b class="formPopover">jpg �먮뒗 png</b><br/></p></div>',
                form_title: '<div class="kr_text"> <p> 怨좉컼DB 紐⑥쭛 �묒떇�� <br> �묒꽦�댁＜�몄슂. </p> </div>',
                form_website_address: "�뱀궗�댄듃 二쇱냼 �낅젰 (�좏깮�ы빆, �꾩닔�꾨떂)",
                go_to_review_and_payment: "寃��� 諛� 寃곗젣 �섏씠吏�濡� �대룞",
                google_display_popover: '<div class="kr_text"> <p> 愿묎퀬 臾멸뎄�� �대�吏�媛� 醫뗭� �깃낵瑜� �� 吏� �뺤떊�섍린 �대졄�붾씪��, 嫄깆젙�섏� 留덉꽭��.<br> �낅젰�섏떊 �뺣낫�� 愿묎퀬 吏묓뻾 �꾩뿉�� �몄젣�좎� �섏젙�� 媛��ν븯硫� 愿묎퀬 吏묓뻾 �붿껌 �� �꾨뱶由ъ뿕 留덉��� ���먯꽌 �뚯옱瑜� 由щ럭�섍퀬 媛쒖꽑�⑸땲��.<br> ���� ���먯꽌 愿묎퀬 �깃낵 �μ긽�� �꾪븳 臾멸뎄 �섏젙 諛� �뚯옱 �쒖븞�� 吏��띿쟻�쇰줈 �대뱶由쎈땲��. <br><br> <b>�щ윭媛�吏� �뚯옱瑜� �ъ슜�섍퀬 �띠쑝�좉���?</b> 쨌吏묓뻾 �붿껌 �댄썑 �대떦 罹좏럹�몄뿉 �뚯옱瑜� �먯쑀濡�쾶 異붽��섏뿬 A/B �뚯뒪�몃� 吏꾪뻾�섏떎 �� �덉뒿�덈떎. <br><br> <b>�대�吏� 沅뚯옣 �ъ뼇</b> 쨌理쒖냼 �ъ씠利�: <strong>600 * 314�쎌�</strong><br> 쨌�뚯씪 �뺤떇: <strong>JPEG, PNG</strong><br> <br> �대�吏� �먮뒗 鍮꾨뵒�ㅺ� �낅줈�� �섏� �딅뒗�ㅻ㈃, <strong>�꾩떆����</strong> 踰꾪듉�� �꾨Ⅴ�� �� �뚯씪�� <a href="mailto:support@adriel.ai">support@adriel.ai</a>濡� 蹂대궡二쇱꽭��. ���� ���� �낅줈�쒗빐 �쒕━寃좎뒿�덈떎. </p> </div> ',
                google_search_add_description: "愿묎퀬 �띿뒪�� 異붽� (�좏깮)",
                google_search_add_description_placeholder: "愿묎퀬 �띿뒪�� �낅젰",
                google_search_add_title: "愿묎퀬 �쒕ぉ 異붽� (�좏깮)",
                google_search_add_title_placeholder: "�쒕ぉ �낅젰",
                google_search_default_title: "�쒕ぉ",
                google_search_description_1: "�ㅻ챸 (1)",
                google_search_popover: '<div class="kr_text"> <p> 寃��� �ㅼ썙�쒕뒗 �꾨뱶由ъ뿕 ���먯꽌 �좎젙 諛� �낅젰�대뱶由쎈땲��. 臾쇰줎 愿묎퀬 吏묓뻾 �� 愿묎퀬二쇰떂猿섏꽌 吏곸젒 �ㅼ썙�쒕� �뺤씤 諛� �섏젙, 異붽��섏떎 �� �덉뒿�덈떎. <br>愿묎퀬 臾멸뎄媛� 醫뗭� �깃낵瑜� �� 吏� �뺤떊�섍린 �대졄�붾씪��, 嫄깆젙�섏� 留덉꽭��. <br> �낅젰�섏떊 �뺣낫�� 愿묎퀬 吏묓뻾 �꾩뿉�� �몄젣�좎� �섏젙�� 媛��ν븯硫� 愿묎퀬 吏묓뻾 �붿껌 �� �꾨뱶由ъ뿕 留덉��� ���먯꽌 �뚯옱瑜� 由щ럭�섍퀬 媛쒖꽑�⑸땲��. <br> ���� ���먯꽌 愿묎퀬 �깃낵 �μ긽�� �꾪븳 臾멸뎄 �섏젙 諛� �뚯옱 �쒖븞�� 吏��띿쟻�쇰줈 �대뱶由쎈땲��. <br> <br> <b>�щ윭媛�吏� �뚯옱瑜� �ъ슜�섍퀬 �띠쑝�좉���?</b> 쨌吏묓뻾 �붿껌 �댄썑 �대떦 罹좏럹�몄뿉 �뚯옱瑜� �먯쑀濡�쾶 異붽��섏뿬 A/B �뚯뒪�몃� 吏꾪뻾�섏떎 �� �덉뒿�덈떎. <br> <br> �대�吏� �먮뒗 鍮꾨뵒�ㅺ� �낅줈�� �섏� �딅뒗�ㅻ㈃, <strong>�꾩떆����</strong> 踰꾪듉�� �꾨Ⅴ�� �� �뚯씪�� <a href="mailto:support@adriel.ai">support@adriel.ai</a>濡� 蹂대궡二쇱꽭��. ���� ���� �낅줈�쒗빐 �쒕━寃좎뒿�덈떎. </p> </div>',
                google_uac_add_description_placeholder: "愿묎퀬 臾몄븞 �꾩씠�붿뼱",
                google_uac_install_text: "�ㅼ튂",
                google_universal_app_popover: '<div class="kr_text"><p>愿묎퀬瑜� �앹꽦�섎뒗 �� �ъ슜�� �띿뒪�몃� �낅젰�섏꽭��.<br/> <br/>愿묎퀬 臾멸뎄媛� 醫뗭� �깃낵瑜� �� 吏� �뺤떊�섍린 �대졄�붾씪��, 嫄깆젙�섏� 留덉꽭��. �낅젰�섏떊 �뺣낫�� 愿묎퀬 吏묓뻾 �꾩뿉�� �몄젣�좎� �섏젙�� 媛��ν븯硫� 愿묎퀬 吏묓뻾 �붿껌 �� �꾨뱶由ъ뿕 留덉��� ���먯꽌 �뚯옱瑜� 由щ럭�섍퀬 媛쒖꽑�⑸땲��.���� ���먯꽌 愿묎퀬 �깃낵 �μ긽�� �꾪븳 臾멸뎄 �섏젙 諛� �뚯옱 �쒖븞�� 吏��띿쟻�쇰줈 �대뱶由쎈땲��. <br/><br/><b>�묐룞 諛⑹떇</b>: �꾨옒 �낅젰�� �띿뒪�� 以꾧낵 �� 紐⑸줉�� 肄섑뀗痢�(�대�吏�, �띿뒪��, �숈쁺��)濡� 愿묎퀬瑜� 援ъ꽦�� �� �덉뒿�덈떎. �띿뒪�몃뒗 媛� 以꾩쓣 �곷Ц 25��(�쒓� 12��) �대궡濡� �묒꽦�섎ŉ, �쒖꽌�� 愿�怨꾩뾾�� 媛� 以꾩씠 �낅┰�곸쑝濡� �섎�瑜� �꾨떖�� �� �덉뼱�� �⑸땲��. �쇰� 愿묎퀬 �뺤떇�먯꽌�� �낅젰�섏떊 �띿뒪�몃� �ъ슜�� �� �놁뒿�덈떎.<br/> <b>��:</b><br/>�낅젰�� 1: 吏�湲� �� �깆쓣 �ㅼ슫濡쒕뱶�섏꽭��. <br/>�낅젰�� 2: �ㅼ쟻�� �믪뿬 蹂댁꽭��. <br/>�먮뒗 <br/><br/>�낅젰�� 1: �ㅼ쟻�� �믪뿬 蹂댁꽭��.<br/>�낅젰�� 2: 吏�湲� �� �깆쓣 �ㅼ슫濡쒕뱶�섏꽭��.</p></div>',
                instagram_feed_id: "鍮꾩쫰�덉뒪 �섏씠吏�",
                instagram_feed_learn_more: "�� �뚯븘蹂닿린",
                instagram_popover: '<div class="kr_text"> <p> 愿묎퀬 臾멸뎄�� �대�吏�媛� 醫뗭� �깃낵瑜� �� 吏� �뺤떊�섍린 �대졄�붾씪��, 嫄깆젙�섏� 留덉꽭��.<br> �낅젰�섏떊 �뺣낫�� 愿묎퀬 吏묓뻾 �꾩뿉�� �몄젣�좎� �섏젙�� 媛��ν븯硫� 愿묎퀬 吏묓뻾 �붿껌 �� �꾨뱶由ъ뿕 留덉��� ���먯꽌 �뚯옱瑜� 由щ럭�섍퀬 媛쒖꽑�⑸땲��.<br> ���� ���먯꽌 愿묎퀬 �깃낵 �μ긽�� �꾪븳 臾멸뎄 �섏젙 諛� �뚯옱 �쒖븞�� 吏��띿쟻�쇰줈 �대뱶由쎈땲��.<br><br> <b>�щ윭媛�吏� �뚯옱瑜� �ъ슜�섍퀬 �띠쑝�좉���?</b> 쨌吏묓뻾 �붿껌 �댄썑 �대떦 罹좏럹�몄뿉 �뚯옱瑜� �먯쑀濡�쾶 異붽��섏뿬 A/B �뚯뒪�몃� 吏꾪뻾�섏떎 �� �덉뒿�덈떎. <br><br> <b>�대�吏� 沅뚯옣 �ъ뼇</b> 쨌沅뚯옣 �ъ씠利�: <strong>1080 * 1080�쎌�</strong><br> 쨌理쒖냼 �ъ씠利�: <strong>500 * 500�쎌�</strong><br> 쨌�뚯씪 �뺤떇: <strong>JPEG, PNG</strong><br> <br> <b>�숈쁺��/�щ씪�대뱶�� 沅뚯옣 �ъ뼇</b> 쨌沅뚯옣 湲몄씠: <strong>理쒕� 15珥�</strong><br> 쨌理쒕� 湲몄씠: <strong>1遺�</strong><br> 쨌�붾㈃ 鍮꾩쑉: <strong>�몃줈諛⑺뼢 (4:5)</strong><br> 쨌�뚮━: <strong>�ъ슜(罹≪뀡 �ы븿)</strong><br> 쨌�뚯씪�뺤떇: <strong>.mp4, .mov</strong><br> 쨌�댁긽�� 理쒖냼 �덈퉬: <strong>600�쎌�</strong><br> 쨌�뚯씪 �ш린: <strong>理쒕� 4湲곌�</strong><br> <br> �대�吏� �먮뒗 鍮꾨뵒�ㅺ� �낅줈�� �섏� �딅뒗�ㅻ㈃, <strong>�꾩떆����</strong> 踰꾪듉�� �꾨Ⅴ�� �� �뚯씪�� <a href="mailto:support@adriel.ai">support@adriel.ai</a>濡� 蹂대궡二쇱꽭��. ���� ���� �낅줈�쒗빐 �쒕━寃좎뒿�덈떎. </p> </div> ',
                interest_tooltip: "�섏씠�ㅻ턿 �먮뒗 �몄뒪��洹몃옩 愿묎퀬 吏묓뻾 �� �뱀젙 �λ��� �댄빐愿�怨꾨� 媛�吏� �щ엺�ㅼ뿉寃� 愿묎퀬瑜� �대낫�낅땲��.",
                interests_input_placeholder: "��寃� 怨좉컼 愿��ъ궗 寃���",
                interests_input_placeholder_empty_tag: "��寃� 怨좉컼 愿��ъ궗 寃���",
                keyword_title: "�쒖븞�� 愿묎퀬 寃��됲궎�뚮뱶�� ��寃� 怨좉컼 愿��ъ궗瑜� 寃��� 諛� �뺤씤 �댁＜�몄슂.",
                keyword_tooltip: "援ш� 寃��� 愿묎퀬 吏묓뻾 �� �좏깮�섏떆�� �ㅼ썙�쒖� 愿��⑤맂 寃��됱쓣 �섎뒗 �щ엺�ㅼ뿉寃� 愿묎퀬瑜� �대낫�낅땲��.",
                keywords_can_change: "愿묎퀬二쇰떂�� �뱀궗�댄듃瑜� 李멸퀬�섏뿬 異붿쿇�� �뺣낫�낅땲��. �몄젣�좎� �섏젙�섏떎 �� �덉뒿�덈떎",
                keywords_input_placeholder: "寃��� �ㅼ썙�� �낅젰",
                keywords_input_placeholder_empty_tag: "寃��� �ㅼ썙�� �낅젰",
                launch_campaign: "寃곗젣 諛� 吏묓뻾�붿껌",
                list_of_suggested_interests: "愿묎퀬二쇰떂�� 鍮꾩쫰�덉뒪�� 留욌뒗 ��寃� 怨좉컼 愿��ъ궗 異붿쿇 由ъ뒪�� �낅땲��.",
                list_of_suggested_keywords: "愿묎퀬二쇰떂�� 鍮꾩쫰�덉뒪�� 留욌뒗 寃��� �ㅼ썙�� 異붿쿇 由ъ뒪�� �낅땲��.",
                location_input_placeholder: "��寃� 吏��� 寃���",
                location_input_placeholder_empty_tag: "��寃� 吏��� 寃���",
                location_popover: '<div class="kr_text"> <p> <b>�꾨뱶由ъ뿕�� �듯븯硫� �꾩꽭怨� �대뵒�먮굹 愿묎퀬�� �� �덉뒿�덈떎. �먰븳 �щ윭 吏���쓣 蹂듭닔濡� �낅젰�� �� �덉뒿�덈떎.</b> <br> <b>�꾨뱶由ъ뿕 TIP!</b> 쨌李얠쑝�쒕뒗 �뱀젙 吏���씠 寃��됰릺吏� �딄굅�� �대뼡 吏���쓣 �낅젰�댁빞 �� 吏� 紐⑤Ⅴ�� 寃쎌슦, 援�� �⑥쐞濡� 吏���쓣 �좏깮�댁＜�몄슂. <br> 쨌愿묎퀬 吏묓뻾 �� 怨좉컼 �쇳꽣濡� 臾몄쓽�섏떆硫� �쇰쭏�좎� �섏젙�대뱶由쎈땲��. <br> 쨌怨좉컼�쇳꽣 臾몄쓽瑜� �듯빐 �뱀젙 吏��� 諛섍꼍 5km ��, �붿슧 �뺢탳�� 吏��� ��寃잜똿�� 媛��ν빀�덈떎.<br> 쨌以묐났�섎뒗 吏���쓣 �낅젰�섏� 留먯븘二쇱꽭��. <br> (��: ���쒕�援�낵 ���쒕�援� �쒖슱�밸퀎�쒕� �숈떆�� �낅젰�섏� 留먯븘二쇱꽭��) </p> </div>',
                location_title: "�대뒓 吏���뿉 \n 愿묎퀬�섍퀬 �띠쑝�좉���?",
                need_fb_page: "�섏씠�ㅻ턿, �몄뒪��洹몃옩 愿묎퀬瑜� �꾪빐 �꾩슂�⑸땲��.",
                next: "�ㅼ쓬",
                no_ads_edit_ad: "�ㅻ줈 �뚯븘媛�\n �몄쭛 �꾨즺�섍린.",
                no_ads_title: "愿묎퀬 吏묓뻾�� �꾪빐�쒕뒗\n 理쒖냼 �� 醫낅쪟 �댁긽�� \n 愿묎퀬 �뚯옱媛� �꾨즺 諛� �뺤씤\n �섏뼱�� �⑸땲��.",
                objectives: "�꾪솚 紐⑺몴瑜� �ㅼ젙�댁＜�몄슂.",
                progress_text: "{value}% 吏꾪뻾",
                review_process_additional_audience: "�쒓�吏� ��! <b>��寃� 怨좉컼�� �ㅼ젙�댁＜�몄슂.</b>",
                review_process_additional_facebook: "�쒓�吏� ��! <br/><b>�섏씠�ㅻ턿 �섏씠吏�瑜� �곌껐�� 二쇱꽭��.</b>",
                review_process_additional_facebook_sdk: "�쒓�吏� ��! <br/><b>�섏씠�ㅻ턿 �깆쓣 �곌껐�� 二쇱꽭��.</b>",
                review_process_additional_ga: "�쒓�吏� ��! <b>援ш� �좊꼸由ы떛�ㅻ� �곌껐�� 二쇱꽭��.</b>",
                review_process_details: "愿묎퀬 吏묓뻾�붿껌�� �깃났�곸쑝濡� �꾨즺�섏뿀�듬땲��.\n 愿묎퀬 吏묓뻾 怨쇱젙�� �ㅼ쓬怨� 媛숈뒿�덈떎.",
                review_process_further_audience: "吏�湲� �ㅼ젙�섍린",
                review_process_further_facebook: "吏�湲� �곌껐�섍린",
                review_process_further_facebook_sdk: "吏�湲� �곌껐�섍린",
                review_process_further_ga: "吏�湲� �곌껐�섍린",
                review_process_later: "�섏쨷�� �섍린",
                review_process_step_1: "�꾨뱶由ъ뿕 留덉��� ����\n 1李� 寃���",
                review_process_step_2: "媛� 愿묎퀬 �뚮옯�쇱쓽\n 2李� 寃���",
                review_process_step_3: "愿묎퀬 吏묓뻾 諛�\n 理쒖쟻��",
                review_process_title: "異뺥븯�⑸땲��!",
                save_and_skip: "�꾩떆���� �섍퀬 嫄대꼫�곌린",
                save_draft_and_continue: "�꾩떆 ���� �� �섏쨷�� 怨꾩냽�섍린",
                schedule_end_date: "醫낅즺",
                schedule_first_box_title: "�ㅻ뒛遺��� 怨꾩냽 愿묎퀬�섍린",
                schedule_period_format: "{value}�� 媛� 吏묓뻾",
                schedule_popover: ' <div class="kr_text"> <p> <strong>愿묎퀬�� �뚯븸�쇱��쇰룄 袁몄��� 吏꾪뻾�섎뒗 寃껋씠 以묒슂�⑸땲��.</strong> �ㅻ옖 湲곌컙 愿묎퀬媛� �몄텧�섎㈃ �볤� 諛� 醫뗭븘�� �� 利앷� 諛� 理쒖쟻 ��寃�, 理쒖쟻 �뚮옯�쇱쓣 李얠븘媛��� �숈뒿�� 吏꾪뻾�섎㈃�� 愿묎퀬�� �⑥쑉�� 吏��띿쟻�쇰줈 媛쒖꽑�섎뒗 �④낵媛� �덉뒿�덈떎.<br> 1�� �덉궛�� �묎쾶 �ㅼ젙�섎뜑�쇰룄 1二쇱씪 �댁긽 �ㅻ옯�숈븞 愿묎퀬瑜� 袁몄��� 吏묓뻾�섏떆�� 寃쎌슦, �됯퇏 3諛� �댁긽 愿묎퀬 �깃낵媛� 媛쒖꽑�⑸땲��. <br><br> <b>愿묎퀬 �뚮옯�쇱쓽 �숈뒿 湲곌컙, �꾨뱶由ъ뿕 �멸났吏��� �붿쭊�� �숈뒿 湲곌컙 紐⑤몢 理쒖냼 5�� �뺣룄 �뚯슂�섎ŉ, �댄썑 �ㅼ젣 援щℓ �꾪솚 �� �ㅼ쭏�곸씤 愿묎퀬 �④낵瑜� 泥댄뿕�섏떎 �� �덉뒿�덈떎.</b> </p> </div> ',
                schedule_recommended: "留ㅼ텧 利앸�瑜� �꾪븳 理쒖쟻 湲곌컙",
                schedule_second_box_title: "吏묓뻾 湲곌컙 吏��뺥븯湲�",
                schedule_start_date: "�쒖옉",
                schedule_title: "愿묎퀬 �쇱젙�� �낅젰�댁＜�몄슂.",
                search_interest: "��寃� 怨좉컼 愿��ъ궗",
                search_interests: "��寃� 怨좉컼 愿��ъ궗",
                search_keywords: "寃��� �ㅼ썙��",
                selection_first_box_btn_text: "遺��곹븯湲�",
                selection_first_box_desc: "諛붿걯�� 愿묎퀬二쇰떂�� ���좏빐, 愿묎퀬 �뚯옱 �좎젙遺��� �명똿源뚯� �꾨뱶由ъ뿕�� �대뱶由쎈땲��.",
                selection_first_box_title: "�꾨뱶由ъ뿕��\n �댁＜�몄슂.",
                selection_second_box_btn_text: "�쒖옉�섍린",
                selection_second_box_desc: "媛꾨떒�� 吏덉쓽 �묐떟�� �듯븯�� 愿묎퀬瑜� �쒖옉�⑸땲��.",
                selection_second_box_title: "吏곸젒\n 留뚮뱾�� 蹂쇨쾶��.",
                selection_top_title: "�대뼡 諛⑸쾿�쇰줈\n 愿묎퀬瑜� �쒖옉�좉퉴��?",
                skip: "嫄대꼫�곌린 ",
                submit: "寃곗젣 諛� 吏묓뻾�붿껌",
                summary_ads: "愿묎퀬�뚮옯��",
                summary_budget_total: "珥앸퉬��",
                summary_comment_placeholder: "湲고� �붿껌�ы빆",
                summary_coupon: "荑좏룿",
                summary_credit: "�щ젅��",
                summary_daily_budget_title: "1�� �덉궛",
                summary_final_amount: "理쒖쥌 吏�遺� 湲덉븸",
                summary_locations: "��寃� 吏���",
                summary_mobile_app: "紐⑤컮�� �� 愿묎퀬",
                summary_payment_note: "3�� �덉궛�� 癒쇱� 寃곗젣�섎ŉ, �ㅼ젣濡� 愿묎퀬媛� 吏묓뻾�섎뒗 湲곌컙 �숈븞 留� 3�� 留덈떎 �� 愿묎퀬湲덉븸�� �뺤궛�⑸땲��. 寃곗젣�� �덉궛蹂대떎 �� �ъ슜�� 寃쎌슦 �쇰� �뱀씤 痍⑥냼媛� �⑸땲��.",
                summary_schedule: "�쇱젙",
                summary_service_fee: "�쒕퉬�� 鍮꾩슜",
                summary_title: "寃��� 諛� 寃곗젣",
                summary_today_payment: "�ㅻ뒛 寃곗젣 湲덉븸 {value}",
                summary_website: "�뱀궗�댄듃",
                tracker_title: "援ш� �좊꼸由ы떛�� 諛� �쎌� 怨듭쑀",
                web_or_app_app_store: "�깆뒪�좎뼱",
                web_or_app_developer_site: "媛쒕컻�� �ъ씠�� (�좏깮)",
                web_or_app_first_box_title: "�뱀궗�댄듃",
                web_or_app_play_store: "援ш� �뚮젅�댁뒪�좎뼱",
                web_or_app_second_box_title: "紐⑤컮�� ��",
                web_or_app_third_box_title: "DB 愿묎퀬",
                web_or_app_website_address: "�뱀궗�댄듃 二쇱냼 �낅젰",
                website_url_popover: '<div class="kr_text""> <p> <b>�대뼡 �뱀궗�댄듃 二쇱냼瑜� �낅젰�댁빞 �섎굹��?</b> 쨌�먯껜 �뱀궗�댄듃 留곹겕<br> 쨌�ㅼ씠踰� �ㅻ쭏�몄뒪�좎뼱 留곹겕<br> 쨌�ㅼ씠踰� �ㅻ쭏�명뵆�덉씠�� 留곹겕<br> 쨌G留덉폆, �꾨쭏議� �� �ㅽ뵂留덉폆 留곹겕<br> 쨌釉붾줈洹�, 源뚰럹 留곹겕<br> 쨌湲고� 愿묎퀬 �뚮옯�쇱쓽 �쒖옱 �붿냼媛� �녿뒗 留곹겕<br> <br> <b>�ㅼ쓬 �뱀궗�댄듃 留곹겕�� 寃쎌슦 怨좉컼�쇳꽣�� 臾몄쓽�댁＜�몄슂.</b> 쨌�섏씠�ㅻ턿 �섏씠吏�<br> 쨌�ㅼ씠踰� 吏��꾨쭅��<br><br> <b>�꾨뱶由ъ뿕 TIP!</b> 쨌�щ엺�ㅼ씠 愿묎퀬瑜� �대┃�섏뿬 �좎엯�� �뱀궗�댄듃瑜� �낅젰�댁＜�몄슂. 愿묎퀬 �댁슜 諛� �뚯옱�� 吏곸젒�곸씤 �곌��깆씠 �덈뒗 二쇱냼瑜� �낅젰�댁＜�쒕㈃ 愿묎퀬 �④낵瑜� �믪씠�� �� �꾩��� �⑸땲��. </p> </div> ',
                website_url_title: "�뱀궗�댄듃 二쇱냼瑜�\n �뚮젮二쇱꽭��."
            }
        }
    }, 8279: function (e, t, a) {
    }, "847b": function (e, t, a) {
        "use strict";
        var n = a("778f"), o = a.n(n);
        o.a
    }, "847d": function (e, t, a) {
        "use strict";
        t["a"] = {
            name: "FormCheckBox", render: function () {
                var e = arguments[0], t = this.value ? "#5aabe3" : "#ccc", a = this.value ? "check-square" : "square";
                return e("b-icon", {attrs: {pack: "far", icon: a}, style: {color: t}})
            }, props: {value: {type: Boolean, default: !1}}
        }
    }, 8495: function (e, t, a) {
        "use strict";
        var n = a("707a"), o = a.n(n);
        o.a
    }, 8501: function (e, t, a) {
    }, 8809: function (e, t, a) {
        "use strict";
        a("96cf");
        var n = a("3b8d"), o = "/files";

        function i(e) {
            return r.apply(this, arguments)
        }

        function r() {
            return r = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.delete("".concat(o).concat(t));
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), r.apply(this, arguments)
        }

        t["a"] = {deleteFile: i}
    }, "895d": function (e, t, a) {
    }, 8975: function (e, t, a) {
        "use strict";
        (function (e) {
            a("a481");
            var t = a("2b0e"), n = a("c1df"), o = a.n(n), i = a("fa7d");
            t["default"].filter("replace", function (e, t) {
                var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "";
                if (e) return e.replace(t, a)
            }), t["default"].filter("truncStr", function () {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 10;
                return e.ifElse(e.compose(e.gt(e.__, a), e.prop("length")), e.compose(e.concat(e.__, "..."), e.take(a)), e.identity)(t)
            }), t["default"].filter("dateFormat", function (e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "[created on] MMM D YYYY";
                return o()(e).format(t)
            }), t["default"].filter("addComma", i["l"]), t["default"].filter("upperHead", i["fb"]), t["default"].filter("formatNumber", i["n"])
        }).call(this, a("b17e"))
    }, "8b5f": function (e, t, a) {
    }, "8cf7": function (e, t, a) {
    }, "8d2e": function (e, t, a) {
        "use strict";
        (function (e) {
            a("96cf");
            var n = a("3b8d"), o = a("cebc"), i = a("2d1f"), r = a.n(i), s = a("768b"), c = (a("ac6a"), a("2f62")),
                l = a("c1df"), d = a.n(l), u = a("31a2"), p = a("1229"), _ = a("dde5"), m = a("fa7d"),
                f = (a("7ab5"), "all"), g = 10;
            t["a"] = {
                components: {"switch-view": u["a"]},
                props: ["campaigns", "totalData", "LIST_DEFAULT_PARAMS", "isRefresh", "validTabs"],
                data: function () {
                    return {
                        checkedRows: [],
                        selectedFilter: [],
                        dataFilter: {},
                        orderElement: "",
                        orderDir: "",
                        page: 1,
                        listNumPerPage: g,
                        defaultSort: []
                    }
                },
                computed: {
                    query: e.path(["$route", "query"]),
                    isAllSelectedCanceled: e.compose(e.any(e.equals(!1)), e.values, e.dissoc(f), e.propOr({}, "dataFilter")),
                    isAllSelectedExceptStatusAll: function () {
                        var t = {};
                        return this.validTabs.forEach(function (e) {
                            return t[e] = e !== f
                        }), e.whereEq(t, this.dataFilter)
                    },
                    filteredValue: e.compose(e.map(function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
                            t = Object(s["a"])(e, 1), a = t[0];
                        return a
                    }), e.filter(e.prop(1)), r.a, e.dissoc(f), e.prop("dataFilter")),
                    sortAndPagingParams: function () {
                        var e = 4 === this.filteredValue.length ? {} : {filters: this.filteredValue};
                        return this.orderElement || this.orderDir ? Object(o["a"])({
                            orderElement: this.orderElement,
                            orderDir: this.orderDir
                        }, e) : e
                    }
                },
                mixins: [p["a"]],
                methods: Object(o["a"])({}, Object(c["mapMutations"])("campaign", ["setCampaigns", "setCachedCampaigns"]), {
                    formatDate: function (e) {
                        return d()(e).format("YYYY-MM-DD")
                    },
                    onMouseOverTitle: function (e) {
                        e.currentTarget.classList.add("hover")
                    },
                    onMouseLeaveTitle: function (e) {
                        var t = e.currentTarget.lastChild.classList.contains("is-active");
                        t || e.currentTarget.classList.remove("hover")
                    },
                    makeChannelsList: e.compose(e.filter(e.__, ["facebook", "instagram", "google"]), e.flip(e.contains), e.map(e.prop("name")), e.filter(e.prop("value")), e.pathOr([], ["proposal", "budget", "channels"])),
                    isFacebookPageLinked: e.path(["intel", "facebookPage"]),
                    makeChannelListForExpress: function (t) {
                        var a = this.makeChannelsList(t), n = ["facebook", "instagram"];
                        return "in_review_express" === t.status ? [] : "deactivated" !== t.status && e.intersection(a, n).length && !this.isFacebookPageLinked(t) ? e.without(n, a) : a
                    },
                    onClickDeleteChecked: function () {
                        var t = this.checkedRows.map(e.prop("id"));
                        this.$emit("delete", {
                            ids: t,
                            params: Object(o["a"])({}, this.LIST_DEFAULT_PARAMS, this.paramOptionAfterAction)
                        })
                    },
                    onClickCheckbox: function (e, t) {
                        var a = this, n = this.dataFilter;
                        n[t] = e, t === f ? this.validTabs.forEach(function (t) {
                            return a.dataFilter[t] = e
                        }) : this.isAllSelectedExceptStatusAll ? n[f] = !0 : this.isAllSelectedCanceled && (n[f] = !1), this.$emit("getCampaignsByParams", Object(o["a"])({}, this.LIST_DEFAULT_PARAMS, this.sortAndPagingParams, {page: this.page - 1}))
                    },
                    onSortTable: function (e, t) {
                        this.orderElement = e, this.orderDir = "asc" === t ? 0 : 1, this.$emit("getCampaignsByParams", Object(o["a"])({}, this.LIST_DEFAULT_PARAMS, this.sortAndPagingParams, {page: this.page - 1}))
                    },
                    onChangePage: function (e) {
                        this.page = e - 1, this.$emit("getCampaignsByParams", Object(o["a"])({}, this.LIST_DEFAULT_PARAMS, this.sortAndPagingParams, {page: e - 1}))
                    },
                    setStatusByParmas: function (t) {
                        var a = this, n = t.page, o = t.filters, i = t.orderElement, r = t.orderDir;
                        if (void 0 !== n && (this.page = +n + 1), o ? e.compose(e.forEach(function (e) {
                                return a.dataFilter[e] = !1
                            }), e.without(e.__, this.validTabs))(o) : this.validTabs.forEach(function (e) {
                                return a.dataFilter[e] = !0
                            }), i && r) {
                            this.orderElement = i, this.orderDir = r;
                            var s = 0 === r ? "asc" : "dsc";
                            this.defaultSort = [i || "", s]
                        }
                    },
                    setCheckedRows: function (e) {
                        this.checkedRows = e
                    },
                    onClickCheckBoxNative: function (e) {
                        var t = e.currentTarget.dataset.status;
                        1 === this.filteredValue.length && t === this.filteredValue[0] && e.preventDefault()
                    },
                    onClickSelectAllinMobile: function () {
                        this.checkedRows = this.checkedRows.length === g ? [] : this.campaigns
                    },
                    onClickExcelDownload: function () {
                        var e = Object(n["a"])(regeneratorRuntime.mark(function e() {
                            var t;
                            return regeneratorRuntime.wrap(function (e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        return e.prev = 0, e.next = 3, _["d"].downloadCampaignsExcel();
                                    case 3:
                                        t = e.sent, Object(m["h"])({
                                            blob: new Blob([t], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}),
                                            name: "".concat(this.formatDate(new Date), "_campaigns.xlsx")
                                        }), e.next = 10;
                                        break;
                                    case 7:
                                        e.prev = 7, e.t0 = e["catch"](0), alert(this.$t("MY_CAMPAIGNS.fail_to_export_excel"));
                                    case 10:
                                    case"end":
                                        return e.stop()
                                }
                            }, e, this, [[0, 7]])
                        }));

                        function t() {
                            return e.apply(this, arguments)
                        }

                        return t
                    }(),
                    onClickCampaignTitle: function (e) {
                        var t = e.event, a = e.props, n = t.target.classList.contains("fal");
                        n || this.boxClicked({
                            status: a.row.status,
                            id: a.row.id,
                            campaignType: this.checkCampaignType(a.row)
                        })
                    },
                    onClickDropdownMenus: function (e) {
                        var t = e.event, a = e.emitEvent;
                        t.stopPropagation(), a()
                    },
                    onClickFilterItem: function () {
                        this.$refs.dropdown.toggle()
                    },
                    setLocaleString: function (e) {
                        return e ? e.toLocaleString() : e
                    },
                    checkCampaignType: function (t) {
                        return e.pathOr("", ["proposal", "meta", "webOrApp", "type"])(t)
                    }
                }),
                created: function () {
                    var t = this;
                    this.isRefresh || this.setStatusByParmas(this.query), this.validTabs.forEach(function (a) {
                        return t.dataFilter = e.assoc(a, !0, t.dataFilter)
                    })
                }
            }
        }).call(this, a("b17e"))
    }, "8e48": function (e, t, a) {
        "use strict";
        var n = a("e49d"), o = a.n(n);
        o.a
    }, "8fac": function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {
                    staticClass: "campaign-box-element",
                    class: e.status,
                    on: {click: e.boxClicked}
                }, [a("div", {staticClass: "campaign-box-top"}, [a("div", {staticClass: "campaign-box-cover-wrapper"}, [a("bg-img", {
                    staticClass: "campaign-box-cover-img",
                    style: {
                        "border-top-left-radius": "5px",
                        "border-top-right-radius": "5px",
                        "background-color": "#4d5058"
                    },
                    attrs: {
                        dimension: [325, 150],
                        src: e.data.imageURL || "",
                        fallbackStyle: {"background-size": "60px 58px"}
                    },
                    on: {
                        "load:fail": function (t) {
                            e.bannerLoaded = !1
                        }
                    }
                }), e.bannerExist ? a("div", {staticClass: "campaign-box-cover"}) : e._e()], 1), a("div", {staticClass: "campaign-box-top-info-wrapper"}, [a("span", {staticClass: "campaign-box-top-info-status"}, [e._v(e._s(e.$t(e.statusString)))]), a("span", {staticClass: "campaign-box-icons"}, [e.deletable({status: e.status}) ? a("img", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: e.$t("MY_CAMPAIGNS.delete"),
                        expression: "$t('MY_CAMPAIGNS.delete')"
                    }],
                    staticClass: "hover-opacity",
                    attrs: {src: "/img/campaign/campaign_delete_icon.png"},
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.$emit("delete", {ids: [e.data.id]})
                        }
                    }
                }) : e._e(), e.duplicable({status: e.status}) ? a("img", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: e.$t("MY_CAMPAIGNS.copy"),
                        expression: "$t('MY_CAMPAIGNS.copy')"
                    }],
                    staticClass: "hover-opacity",
                    attrs: {src: "/img/campaign/copy_campaign.png"},
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.$emit("copy", e.data.id)
                        }
                    }
                }) : e._e()])])]), a("div", {staticClass: "campaign-box-bottom"}, [a("div", {staticClass: "title-wrapper"}, [a("span", {
                    staticClass: "title-area",
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.$emit("click:title", e.campaignId)
                        }
                    }
                }, [a("span", {staticClass: "campaign-box-title adriel-ellipsis"}, [e._v(e._s(e.data.title))]), a("b-icon", {
                    style: {
                        "font-size": "10px",
                        color: "#666666"
                    }, attrs: {pack: "fal", icon: "pen"}
                })], 1)]), a("span", {staticClass: "campaign-box-created"}, [e._v(e._s(e._f("dateFormat")(e.data.created_at, e.$t("MY_CAMPAIGNS.date_format"))))]), a("div", {staticClass: "campaign-box-status-info"}, [e.isStatusMentVisible ? a("span", {
                    staticClass: "status-ment",
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.checkRunLonger(t)
                        }
                    }
                }, [a("b-icon", {
                    style: {"font-size": "12px", color: "#ef646f"},
                    attrs: {pack: "fas", icon: "clock"}
                }), a("span", {
                    staticClass: "status-ment--text",
                    domProps: {innerHTML: e._s(e.$t("MY_CAMPAIGNS.campaign_left", {days: e.$t("MY_CAMPAIGNS.campaign_left_" + e.daysLeft)}))}
                })], 1) : e._e(), e.changeEnablePossible({status: e.status}) ? a("switch-view", {
                    ref: "switch",
                    attrs: {value: e.data.active},
                    on: {
                        "update:value": function (t) {
                            return e.switchValueChanged({value: t})
                        }
                    },
                    nativeOn: {
                        click: function (e) {
                            e.stopPropagation()
                        }
                    }
                }) : e._e()], 1), a("hr"), a("div", {staticClass: "campaign-box-buttons"}, [a("span", {
                    staticClass: "each setting",
                    class: {disabled: !e.changeSettingPossible({status: e.status})},
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.checkEdit(t)
                        }
                    }
                }, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "pen-square"
                    }
                }), a("span", {
                    staticClass: "each--text adriel-ellipsis",
                    domProps: {textContent: e._s(e.$t("MY_CAMPAIGNS.ad_setting"))}
                })], 1), a("span", {
                    staticClass: "each run",
                    class: {disabled: !e.runLongerPossible({data: e.data})},
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.checkRunLonger(t)
                        }
                    }
                }, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "calendar-plus"
                    }
                }), a("span", {
                    staticClass: "each--text adriel-ellipsis",
                    domProps: {textContent: e._s(e.extendText)}
                })], 1), a("span", {
                    staticClass: "each results",
                    class: {disabled: !e.resultsPossible({status: e.status})},
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.checkResult(t)
                        }
                    }
                }, [a("b-icon", {
                    attrs: {
                        pack: "fas",
                        icon: "chart-bar"
                    }
                }), a("span", {
                    staticClass: "each--text adriel-ellipsis",
                    domProps: {textContent: e._s(e.$t("MY_CAMPAIGNS.see_result"))}
                })], 1), a("span", {
                    staticClass: "each budget",
                    class: {disabled: !e.extendBudgetPossible({status: e.status})},
                    on: {
                        click: function (t) {
                            return t.stopPropagation(), e.checkBudget(t)
                        }
                    }
                }, [a("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "usd-circle"
                    }
                }), a("span", {
                    staticClass: "each--text adriel-ellipsis",
                    domProps: {textContent: e._s(e.$t("MY_CAMPAIGNS.add_budget"))}
                })], 1)])]), a("div", {staticClass: "campaign-box-indicator"})])
            }, o = [], i = a("0e1c"), r = i["a"], s = (a("5a7a"), a("9044"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "2813bb6e", null);
        t["a"] = c.exports
    }, "8fe1": function (e, t) {
        e.exports = {
            en: {
                adriel_offer: "What's included in Adriel's service?",
                adriel_offer_contents_0: ["We edit your text and images for various ad platforms such as Facebook, Instagram and Google.", "We make sure that texts and images are optimized in order to maximize the performance on ad platforms."],
                adriel_offer_contents_1: ["We set search keywords and audience interests", "We re-target people who have visited your website or have similar profiles to those who visit your website"],
                adriel_offer_contents_2: ["After we launch the ads, more people will visit your website, hence incereasing sales. We compare and analyze the performance on Facebook, Instagram and Google.", "We will also email you the summary of campaign performance "],
                adriel_offer_contents_3: ["Text copies, images and keywords are constantly managed and suggested by Adriel�셲 marketing team in a proactive manner", "AI rebalances your budget between ads, target audience groups and ad platforms according to performance."],
                adriel_offer_image_0: "/img/en_service_screen_0.png",
                adriel_offer_image_1: "/img/en_service_screen_1.png",
                adriel_offer_image_2: "/img/en_service_screen_2.png",
                adriel_offer_image_3: "/img/en_service_screen_3.png",
                adriel_offer_title_0: "Ad Creative Optimization",
                adriel_offer_title_1: "Targeting Optimization",
                adriel_offer_title_2: "Traffic, Sales & Reporting",
                adriel_offer_title_3: "Proactive suggestions to improve performance",
                already_signed: "Already signed up?",
                confirm_password: "Confirm password",
                email: "Email",
                email_exist: "Email already exists.",
                failed_to_create_account: "Error to create user.",
                fb_no_email: "We cannot create a user for you unless you agree to give us permission.",
                invalid_email_format: "Invalid email format.",
                optional: "Optional",
                password: "Password",
                password_not_match: "Password does not match the with confirm password.",
                password_placeholder: "Password (8 characters minimum)",
                password_too_short: "Password is too short, the minimum length is 8 characters.",
                should_check_terms: "You must agree to our terms in order to sign up.",
                sign_up: "Sign up",
                sign_up_with: "Sign up with {provider}",
                social_email_exist: "There's already a user created with {email}. If you don't remember the password, please reset it by clicking <a href=\"/#/sign/forgot\">Forgot password.</a>",
                terms_services_1: "I agree to the",
                terms_services_2: "and",
                terms_services_3: "",
                terms_services_policy: "Privacy Policy",
                terms_services_terms: "Terms of Service",
                website_url: "Company website",
                website_url_placeholder: "Website URL",
                welcome_content: "Welcome on board! A verification email has been sent to {email}. Please check your email and follow the link to finish creating a user."
            },
            jp: {
                adriel_offer: "�㏂깋�ゃ궓�ャ겗�듐꺖�볝궧�ャ겘鵝뺛걣�ャ겲�뚣겍�꾠겲�쇻걢竊�",
                adriel_offer_contents_0: [" Facebook�갏nstagram�갍oogle Search�믣맜���곫쭣�끹겒�듐궎�덁툓��깘�듽꺖佯껃몜�졼겓�덀걝�덀걝�ャ궚�ゃ궓�ㅳ깇�ｃ깣�꾣쑍�뉎굮�ゅ땿�㎫랬�녴걮�얇걲��", "佯껃몜��쑍�뉎굜�삣깗��걹�뚣걻�뚣겗�쀣꺀�껁깉�뺛궔�쇈깲�㎪�鸚㎯겗�방옖�믣눣�쎼굥�덀걝�ユ��⒴뙑�뺛굦�얇걲��"],
                adriel_offer_contents_1: ["濾쒐뇨��꺖��꺖�됥겏�ゃ꺖�뉎궍�ⓦ꺍�밤겗�덂뫑�ャ깇�담꺁�쇈굮鼇�츣�쀣겲�쇻��", "訝�佯╉궢�ㅳ깉�ヨØ�뚣걼�╉꺖�뜰꺖�곥겲�잆겘�앫굦�ヤ세�잆깺�쇈궣�쇈겓擁녈걮�꾠겍�ゃ궭�쇈궟�녴궍�녈궛�띴에�믦죱�꾠겲�쇻��"],
                adriel_offer_contents_2: ["佯껃몜�븀㉮孃뚣�곥깺�쇈궣�쇈걣�귙겒�잆겗�듐궎�덀겓鼇ゃ굦�곩２訝듽걣�잆겳�뷩걬�뚣굥�덀걝�ャ겒�듽겲�쇻�괚acebook�갏nstagram�갍oogle search�믣맜���곫쭣�끹겒�듐궎�덁툓��깘�듽꺖佯껃몜��깙�뺛궔�쇈깯�녈궧�믤캈雍껃늽�먦걮�얇걲��", "��깵�녈깪�쇈꺍�묆깢�⒲꺖�욁꺍�밤겗�с깮�쇈깉�믡깳�쇈꺂�㎯걡掠듽걨�쀣겲�쇻��"],
                adriel_offer_contents_3: ["�뤵궎�묆깢�⒲꺖�욁꺍�밤굮�뷩걲�잆굙�ャ�곩틕�딀쑍�뉎�곭뵽�뤵�곥궘�쇈꺈�쇈깋��만�ャ궋�됥꺁�ⓦ꺂��깯�쇈궞�녴궍�녈궛�곥꺖�졼겓嶸←릤�뺛굦�╉걡�듽겲�쇻��", "�귙겒�잆겗雅덄츞�녽뀓���곩틕�듽�곥궭�쇈궟�녴궍�녈궛�겹꺂�쇈깤�곩룋�녈깤�⒲긿�덀깢�⒲꺖�졼겗�끻젿�믡굚�ⓦ겓AI�뚩눎�뺛겎���⒴뙑�꾠걼�쀣겲�쇻��"],
                adriel_offer_image_0: "/img/en_service_screen_0.png",
                adriel_offer_image_1: "/img/en_service_screen_1.png",
                adriel_offer_image_2: "/img/en_service_screen_2.png",
                adriel_offer_image_3: "/img/en_service_screen_3.png",
                adriel_offer_title_0: "��꺁�ⓦ궎�녴궍�뽧겗���⒴뙑",
                adriel_offer_title_1: "�욍꺖�꿔긿�덀겗���⒴뙑",
                adriel_offer_title_2: "�덀꺀�뺛궍�껁궚�ⓨ２�듾툓�믡겗�с깮�쇈깇�ｃ꺍��",
                adriel_offer_title_3: "�묆깢�⒲꺖�욁꺍�밧릲訝듽겗�잆굙��궋�됥깘�ㅳ궧",
                already_signed: "�삯뙯歷덀겳�㎯걲�뗰폕",
                confirm_password: "�묆궧��꺖�됥굮閻븃첀�쀣겍�뤵걽�뺛걚��",
                email: "Email",
                email_exist: "�앫겗�▲꺖�ャ궋�됥꺃�밤겘�㏂겓�삯뙯�뺛굦�╉걚�얇걲��",
                failed_to_create_account: "�㏂궖�╉꺍�덁퐳�먦겓鸚길븮�쀣겲�쀣걼��",
                fb_no_email: "�㏂궖�╉꺍�덀굮鵝쒏닇�쇻굥�ャ겘�곥궋�됥꺁�ⓦ꺂�뚩㉠獄얇굮孃쀣굥��겓�뚧꼷�쇻굥恙낁쫨�뚣걗�듽겲�쇻��",
                invalid_email_format: "�▼듅�ゃ깳�쇈꺂�㏂깋�с궧",
                optional: "�ゃ깤�룔깾��",
                password: "�묆궧��꺖��",
                password_not_match: "�묆궧��꺖�됥걣�뺛겂�╉걚�얇걲��",
                password_placeholder: "�묆궧��꺖�� (��鵝�8�뉐춻)",
                password_too_short: "�묆궧��꺖�됥걣��걲�롢겲�쇻�귙깙�밤꺈�쇈깋���鵝�8�뉐춻�㎯걲��",
                should_check_terms: "�듐꺖�볝궧榮꾣Ь�ⓨ�뗤볶�끻젿��릤�백뇺�ュ릪�뤵걮�╉걦�졼걬�꾠��",
                sign_up: "�삯뙯�쇻굥",
                sign_up_with: " {provider}�㎫쇉�꿔걲��",
                social_email_exist: '�쇻겎�� {email}�㎯궋�ャ궑�녈깉��퐳�먦걬�뚣겍�꾠겲�쇻��. �묆궧��꺖�됥굮恙섅굦�잌졃�덀겘��<a href="/#/sign/forgot">Forgot password.</a>�믡궚�ゃ긿��걮�╉꺁�삠긿�덀걮�╊툔�뺛걚��',
                terms_services_1: "燁곥겘訝뗨쮼�ュ릪�뤵걮�얇걲��",
                terms_services_2: "and",
                terms_services_3: "",
                terms_services_policy: "�쀣꺀�ㅳ깘�룔꺖�앫꺁�룔꺖",
                terms_services_terms: "�⑴뵪誤뤹큵",
                website_url: "鴉싩ㅎ�╉궒�뽧궢�ㅳ깉",
                website_url_placeholder: "�╉궒�뽧궢�ㅳ깉URL",
                welcome_content: "�덀걝�볝걹竊곭▶沃띲깳�쇈꺂�� {email}�ラ�곦에�뺛굦�얇걮�잆�귞▶沃띲깳�쇈꺂�ヨ쮼雍됥걬�뚣겍�꾠굥�ゃ꺍��걢�됥궋�ャ궑�녈깉鵝쒏닇�믡걮�╉걦�졼걬�꾠��"
            },
            ko: {
                adriel_offer: "�꾨뱶由ъ뿕 �쒕퉬�� �ы븿 �ы빆",
                adriel_offer_contents_0: ["�섏씠�ㅻ턿, �몄뒪��洹몃옩, 援ш� 寃���, �ㅼ뼇�� �뱀궗�댄듃 諛곕꼫愿묎퀬 �щ㎎�� 留욊쾶 �띿뒪�� 諛� �대�吏� �몄쭛", "愿묎퀬 �뚮옯�쇱뿉�� �쎄쾶 �뱀씤�섍퀬 �몄텧�� �� �섎뒗 愿묎퀬臾멸뎄/�대�吏� 異붿쿇 諛� 理쒖쟻��"],
                adriel_offer_contents_1: ["寃��� �ㅼ썙�� �ㅼ젙 諛� ��寃� 愿��ъ궗 �ㅼ젙", "愿묎퀬二쇰떂�� �뱀궗�댄듃瑜� 諛⑸Ц�덈뜕 �щ엺�ㅼ쓣 由ы�寃잜똿 �먮뒗 愿묎퀬二쇰떂�� �뱀궗�댄듃瑜� 諛⑸Ц�섎뒗 �щ엺�ㅺ낵 鍮꾩듂�� �꾨줈�꾩쓣 媛�吏� ��寃잛쓣 李얠븘 愿묎퀬 �몄텧"],
                adriel_offer_contents_2: ["愿묎퀬 吏묓뻾 �� 吏��띿쟻�쇰줈 �섏뼱�섎뒗 諛⑸Ц�먯닔 諛� 留ㅼ텧 �ㅼ떆媛� 議고쉶", "�섏씠�ㅻ턿, �몄뒪��洹몃옩, 援ш� 寃���, �ㅼ뼇�� �뱀궗�댄듃 諛곕꼫愿묎퀬�� �깃낵瑜� �쒕늿�� 鍮꾧탳 遺꾩꽍", "愿묎퀬 醫낅즺 �� �대찓�쇰줈 愿묎퀬 寃곌낵 遺꾩꽍吏� �〓�"],
                adriel_offer_contents_3: ["�멸났吏��� 愿�由ш� �대젮�� 愿묎퀬 臾멸뎄, �대�吏� 諛� �ㅼ썙�쒕뒗 �꾨뱶由ъ뿕 留덉��� ���� 吏��띿쟻�쇰줈 愿�由� 諛� �쒖븞 異붿쿇", "�멸났吏��μ쑝濡� �� �깃낵媛� 醫뗭� 愿묎퀬, ��寃� 怨좉컼 諛�        愿묎퀬 �뚮옯�쇱뿉 �� 留롮� �덉궛�� �몄꽦"],
                adriel_offer_image_0: "/img/ko_service_screen_0.png",
                adriel_offer_image_1: "/img/ko_service_screen_1.png",
                adriel_offer_image_2: "/img/ko_service_screen_2.png",
                adriel_offer_image_3: "/img/ko_service_screen_3.png",
                adriel_offer_title_0: "愿묎퀬 �대�吏�/臾멸뎄 理쒖쟻��",
                adriel_offer_title_1: "��寃잜똿 理쒖쟻��",
                adriel_offer_title_2: "諛⑸Ц�먯닔 諛� 留ㅼ텧 利앸�/�깃낵 蹂닿퀬��",
                adriel_offer_title_3: "�꾨뱶由ъ뿕 ���� 吏��띿쟻�� �깃낵 愿�由�",
                already_signed: "�대� 媛��낇븯�⑤굹��?",
                confirm_password: "鍮꾨�踰덊샇 �뺤씤",
                email: "�대찓��",
                email_exist: "�숈씪�� �대찓�� 二쇱냼濡� �앹꽦�� �ъ슜�먭� �대� 議댁옱�⑸땲��.",
                failed_to_create_account: "�ъ슜�먮� �앹꽦�섎뒗 怨쇱젙�먯꽌 �먮윭媛� 諛쒖깮�덉뒿�덈떎.",
                fb_no_email: "�섏씠�ㅻ턿 沅뚰븳�� �덉슜�섏� �딆븘 媛��낆쓣 吏꾪뻾�� �� �놁뒿�덈떎.",
                invalid_email_format: "�대찓�� 二쇱냼�� �뺤떇�� 留욎� �딆뒿�덈떎.",
                optional: "�좏깮",
                password: "鍮꾨�踰덊샇",
                password_not_match: "鍮꾨�踰덊샇媛� �쇱튂�섏� �딆뒿�덈떎.",
                password_placeholder: "鍮꾨�踰덊샇 (理쒖냼 8��)",
                password_too_short: "鍮꾨�踰덊샇�� 8�� �댁긽�쇰줈 �ㅼ젙�� 二쇱꽭��.",
                should_check_terms: "�쒕퉬�� �쎄�怨� 媛쒖씤�뺣낫泥섎━諛⑹묠�� �숈쓽�댁＜�몄슂.",
                sign_up: "媛��낇븯湲�",
                sign_up_with: "{provider}濡� �뚯썝媛���",
                social_email_exist: '{email}濡� �대� 媛��낅맂 �ъ슜�먭� 議댁옱�⑸땲��. 鍮꾨�踰덊샇媛� 湲곗뼲�섏� �딆쑝�쒕㈃ <a href="/#/sign/forgot">鍮꾨�踰덊샇 李얘린</a>瑜� �댁슜�섏뿬 �덈줈�� 鍮꾨�踰덊샇濡� 由ъ뀑�댁＜�몄슂.',
                terms_services_1: "",
                terms_services_2: "怨�",
                terms_services_3: "�� �숈쓽�⑸땲��.",
                terms_services_policy: "媛쒖씤�뺣낫泥섎━諛⑹묠",
                terms_services_terms: "�쒕퉬�� �쎄�",
                website_url: "�뱀궗�댄듃 二쇱냼",
                website_url_placeholder: "�뱀궗�댄듃 二쇱냼",
                welcome_content: "�섏쁺�⑸땲��! �대찓�� 二쇱냼 �뺤씤�� �꾪빐 {email}濡� 硫붿씪�� 蹂대깉�듬땲��. 硫붿씪 �댁쓽 留곹겕瑜� �대┃�섏뿬 �ъ슜�� �앹꽦�� �꾨즺�댁＜�몄슂."
            }
        }
    }, 9044: function (e, t, a) {
        "use strict";
        var n = a("a176"), o = a.n(n);
        o.a
    }, 9133: function (e, t, a) {
    }, 9153: function (e, t, a) {
        "use strict";
        var n = a("aede"), o = a("9c56");

        function i() {
            var e = Object(n["a"])(["\n    font-size: 20px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.5;\n    letter-spacing: normal;\n    text-align: center;\n    color: #797d88;\n    width: 100%;\n    margin-top: 30px;\n    white-space: pre-line;\n    @media screen and (max-width: 800px) {\n        font-size: 15px;\n    }\n"]);
            return i = function () {
                return e
            }, e
        }

        function r() {
            var e = Object(n["a"])(["\n    &:hover .icon {\n        color: #5aabe3;\n    }\n    .icon {\n        color: #b3b9c6;\n        font-size: 22px;\n        @media screen and (max-width: 800px) {\n            font-size: 18px;\n        }\n    }\n    .express-content {\n        font-size: 18px;\n        ", ";\n        font-weight: 800;\n        color: #797d88;\n        text-align: center;\n        white-space: pre-wrap;\n        @media screen and (max-width: 800px) {\n            font-size: 15px;\n        }\n    }\n    .express-title {\n        ", ";\n        font-size: 14px;\n        text-align: center;\n        margin-top: 14px;\n        font-weight: 600;\n        color: #797d88;\n        @media screen and (max-width: 800px) {\n            font-size: 12px;\n        }\n    }\n    .express-info {\n        display: flex;\n        flex-direction: row;\n        justify-content: center;\n        align-items: center;\n        font-weight: 800;\n        .origin {\n            ", ";\n            font-size: 16px;\n            color: #999999;\n            text-decoration: line-through;\n            margin-right: 4px;\n            @media screen and (max-width: 800px) {\n                font-size: 14px;\n            }\n        }\n        .discount {\n            ", ";\n            font-size: 20px;\n            color: #797d88;\n            @media screen and (max-width: 800px) {\n                font-size: 18px;\n            }\n        }\n    }\n"]);
            return r = function () {
                return e
            }, e
        }

        function s() {
            var e = Object(n["a"])(["\n    &:hover .fa-plus {\n        color: #5aabe3;\n    }\n    & > .fa-plus {\n        font-size: 40px;\n        color: #b3b9c6;\n        @media screen and (max-width: 800px) {\n            font-size: 30px;\n        }\n    }\n    span {\n        ", ";\n        font-weight: 800;\n        color: #797d88;\n        margin-top: 5px;\n    }\n"]);
            return s = function () {
                return e
            }, e
        }

        function c() {
            var e = Object(n["a"])(["\n    width: 310px;\n    height: 250px;\n    margin: 9.5px;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    align-self: center;\n    font-size: 20px;\n    & > div {\n        width: 100%;\n        display: inline-flex;\n        border: 2px dashed #b3b9c6;\n        justify-content: center;\n        flex-direction: column;\n        border-radius: 5px;\n        align-items: center;\n        cursor: pointer;\n        margin: 0;\n        &:hover {\n            border-color: #5aabe3;\n        }\n    }\n    @media screen and (max-width: 800px) {\n        width: 250px;\n        height: 190px;\n        font-size: 16px;\n    }\n"]);
            return c = function () {
                return e
            }, e
        }

        function l() {
            var e = Object(n["a"])(["\n    display: flex;\n    flex: 1;\n    flex-wrap: wrap;\n    justify-content: center;\n    margin-top: 200px;\n    @media screen and (max-width: 800px) {\n        margin-top: 15px;\n    }\n"]);
            return l = function () {
                return e
            }, e
        }

        function d() {
            var e = Object(n["a"])(["\n    flex: 1;\n    align-self: center;\n"]);
            return d = function () {
                return e
            }, e
        }

        function u() {
            var e = Object(n["a"])(["\n    font-family: 'Rubik', 'sans-serif', 'Noto Sans';\n"]);
            return u = function () {
                return e
            }, e
        }

        var p = Object(o["a"])(u()), _ = o["b"].div(d()), m = o["b"].div(l()), f = o["b"].div(c()),
            g = o["b"].div(s(), p), h = o["b"].div(r(), p, p, p, p), b = o["b"].div(i());
        t["a"] = {
            name: "EmptyCampaign", props: {
                startChat: {
                    type: Function, default: function () {
                        return {}
                    }
                }, goToExpress: {
                    type: Function, default: function () {
                        return {}
                    }
                }
            }, render: function () {
                var e = arguments[0];
                return e(_, [e(m, [e(f, [e(g, {on: {click: this.startChat}}, [e("i", {class: "fal fa-plus"}), e("span", [this.$t("MY_CAMPAIGNS.new_campaign")])])]), e(f, [e(h, {on: {click: this.goToExpress}}, [e("b-icon", {
                    attrs: {
                        pack: "fas",
                        icon: "magic"
                    }, style: "{ 'font-size': '23px', width: 'auto', height: 'auto' }"
                }), e("span", {class: "express-title"}, [this.$t("MY_CAMPAIGNS.new_campaign")]), e("span", {class: "express-content word-break"}, [this.$t("EXPRESS.by_adriel")]), e("span", {class: "express-info"}, [e("span", {class: "origin"}, [this.formatCurrency(19)]), e("span", {class: "discount"}, [this.formatCurrency(9)])])])])]), e(b, [this.$t("MY_CAMPAIGNS.no_campaign")])])
            }
        }
    }, 9225: function (e, t, a) {
        "use strict";
        var n = a("2b0e"), o = a("a925"), i = a("cebc"), r = a("f88b"), s = a.n(r), c = a("7143"), l = a.n(c),
            d = a("79f7"), u = a.n(d), p = a("7a90"), _ = a.n(p), m = a("4381"), f = a.n(m), g = a("4537"), h = a.n(g),
            b = a("7a74"), v = a.n(b), y = a("8fe1"), w = a.n(y), k = a("55cd"), x = a.n(k), O = a("760d"), A = a.n(O),
            C = a("3593"), S = a.n(C), E = a("f391"), T = a.n(E), P = a("97e4"), I = a.n(P), L = a("ff29"), j = a.n(L),
            M = a("da8c"), R = a.n(M), N = a("23a2"), D = a.n(N), $ = a("9636"), B = a.n($), F = a("1f73"), G = a.n(F),
            U = a("939f"), q = a.n(U), Y = a("81f3"), V = a.n(Y), z = a("6ebf"), W = a.n(z), H = a("d157"), K = a.n(H),
            J = a("79f5"), Q = a.n(J), X = a("9621"), Z = a.n(X), ee = a("9968"), te = a.n(ee), ae = a("5d07"), ne = {
                ACCOUNT_SETTING: s.a.en,
                BILLING_HISTORY: D.a.en,
                COMMON: l.a.en,
                DASHBOARD: u.a.en,
                EMAIL_VERIFY: _.a.en,
                FOOTER: f.a.en,
                FORGOT: h.a.en,
                HELP: v.a.en,
                JOIN: w.a.en,
                LAUNCHED: x.a.en,
                LEFT_NAV: A.a.en,
                LOGIN: S.a.en,
                MY_CAMPAIGNS: T.a.en,
                NOT_FOUND: R.a.en,
                PROPOSAL: I.a.en,
                RESET: j.a.en,
                EXPRESS: B.a.en,
                DEMO: G.a.en,
                COUPON: q.a.en,
                ONBOARDING: V.a.en,
                FAQ: W.a.en,
                VAT: K.a.en,
                FORM: Q.a.en,
                MANAGER: Object(i["a"])({}, Z.a.en, ae["locale"].optimizer.en, ae["locale"].connectActionsRequiredMessages.en),
                IMAGE_TEMPLATE: te.a.en
            }, oe = {
                ACCOUNT_SETTING: s.a.ko,
                BILLING_HISTORY: D.a.ko,
                COMMON: l.a.ko,
                DASHBOARD: u.a.ko,
                EMAIL_VERIFY: _.a.ko,
                FOOTER: f.a.ko,
                FORGOT: h.a.ko,
                HELP: v.a.ko,
                JOIN: w.a.ko,
                LAUNCHED: x.a.ko,
                LEFT_NAV: A.a.ko,
                LOGIN: S.a.ko,
                MY_CAMPAIGNS: T.a.ko,
                NOT_FOUND: R.a.ko,
                PROPOSAL: I.a.ko,
                RESET: j.a.ko,
                EXPRESS: B.a.ko,
                DEMO: G.a.ko,
                COUPON: q.a.ko,
                ONBOARDING: V.a.ko,
                FAQ: W.a.ko,
                VAT: K.a.ko,
                FORM: Q.a.ko,
                MANAGER: Object(i["a"])({}, Z.a.ko, ae["locale"].optimizer.ko, ae["locale"].connectActionsRequiredMessages.ko),
                IMAGE_TEMPLATE: te.a.ko
            }, ie = {
                ACCOUNT_SETTING: s.a.jp,
                BILLING_HISTORY: D.a.jp,
                COMMON: l.a.jp,
                DASHBOARD: u.a.jp,
                EMAIL_VERIFY: _.a.jp,
                FOOTER: f.a.jp,
                FORGOT: h.a.jp,
                HELP: v.a.jp,
                JOIN: w.a.jp,
                LAUNCHED: x.a.jp,
                LEFT_NAV: A.a.jp,
                LOGIN: S.a.jp,
                MY_CAMPAIGNS: T.a.jp,
                NOT_FOUND: R.a.jp,
                PROPOSAL: I.a.jp,
                RESET: j.a.jp,
                EXPRESS: B.a.jp,
                DEMO: G.a.jp,
                COUPON: q.a.jp,
                ONBOARDING: V.a.jp,
                MANAGER: Z.a.jp,
                IMAGE_TEMPLATE: te.a.jp
            };
        n["default"].use(o["a"]);
        t["a"] = new o["a"]({locale: "en", fallbackLocale: "en", messages: {en: ne, ko: oe, jp: ie}})
    }, "92ae": function (e, t, a) {
        "use strict";
        var n = a("028c"), o = a.n(n);
        o.a
    }, "939f": function (e, t) {
        e.exports = {
            en: {
                add_modal_title: "Adriel Ad Coupon",
                consumed: "Used",
                coupon: "Coupon",
                expired: "Expired",
                expires_on: "Expires on {date}",
                failed_to_register_coupon: "The coupon is already redeemed",
                remaining: "{amount} remaining"
            },
            jp: {
                add_modal_title: "Adriel��꺖�앫꺍",
                consumed: "易덅꼇歷덀겳",
                coupon: "��꺖�앫꺍",
                expired: "�잓솏�뉎굦",
                expires_on: "�됧듅�잓솏 {date}",
                failed_to_register_coupon: "�볝겗��꺖�앫꺍��뿢�ヤ슴�ⓦ걬�뚣겍�꾠겲�쇻��",
                remaining: "餘뗣굤 {amount} "
            },
            ko: {
                add_modal_title: "�꾨뱶由ъ뿕 荑좏룿",
                consumed: "�ъ슜��",
                coupon: "荑좏룿",
                expired: "Expired",
                expires_on: "�좏슚湲곌컙: {date}",
                failed_to_register_coupon: "�대� �곸슜�섏뿀�듬땲��",
                remaining: "�붿뿬 湲덉븸 {amount}"
            }
        }
    }, 9621: function (e, t) {
        e.exports = {
            ko: {
                top_title: "�� 留ㅻ땲��",
                no_image: "�대�吏� �놁쓬",
                no_content: "�꾩쭅 �낅뜲�댄듃 �ы빆�� �녾뎔��!\n�곗씠�곌� �섏쭛 �섎㈃ �뚮젮�쒕━寃좎뒿�덈떎.",
                delete_modal: "�≪뀡�� ��젣�섏떆寃좎뒿�덇퉴?",
                delete_modal_confirm: "��젣",
                newsfeed: "�댁뒪�쇰뱶",
                actionRequired: "�꾩슂�� 議곗튂",
                thread: "�꾩껜 ����",
                contact_adriel: "�꾨뱶由ъ뿕�� �곕씫�섍린",
                REVOKED_TITLE: "�꾨뱶由ъ뿕 而댄넗 寃곌낵: 鍮꾩듅��",
                REVOKED: "",
                STRIPE: "�좎슜移대뱶 寃곗젣",
                COUPONS: "荑좏룿 寃곗젣",
                TRANSFER: "�꾧툑 寃곗젣",
                REVOKED_PAYMENT: "�붿껌�섏떊 愿묎퀬 {campaign_title} 媛� 寃곗젣�섎떒 �ㅻ쪟濡� �뱀씤�섏� �딆븯�듬땲��. 寃곗젣�섎떒�쇰줈 �깅줉�섏떊 �좎슜移대뱶�� �붿븸 �먮뒗 �좏슚湲곌컙 �� �ㅻ쪟 �먯씤�� �뺤씤�섏떊 �� �ㅼ떆 �좎껌�댁＜�쒓린 諛붾엻�덈떎.",
                REVOKED_POLICY: '�붿껌�섏떊 愿묎퀬 {campaign_title} 媛� 愿묎퀬�뺤콉 �꾨컲�쇰줈 �뱀씤�섏� �딆븯�듬땲��.\n<a href="https://intercom.help/adriel/en/articles/2661761">�꾨뱶由ъ뿕 愿묎퀬 �뺤콉 �뺤씤�섍린�몛</a>',
                REVOKED_BO: "�붿껌�섏떊 愿묎퀬 {campaign_title} 媛� �뱀씤�섏� �딆븯�듬땲��.\n{backoffice_note}\n鍮꾩듅�� �ъ쑀瑜� 李멸퀬�섏떆�� 愿묎퀬瑜� �섏젙�댁＜�몄슂.",
                PUBLISHING_ERROR_TITLE: "愿묎퀬瑜� �섏젙�댁＜�몄슂.",
                PUBLISHING_ERROR: "{campaign} 愿묎퀬瑜� 吏묓뻾�섍린 �꾪빐�� �꾨옒 �덈궡�ы빆�� �뺤씤�댁＜�몄슂.",
                DISAPPROVED_TITLE: "{campaign} 寃��� 寃곌낵: 鍮꾩듅��",
                DISAPPROVED: "{campaign} 愿묎퀬媛� �꾨옒 �ъ쑀濡� �뱀씤�섏� �딆븯�듬땲��.\n愿묎퀬媛� �뱀씤�� �� �덈룄濡� 鍮꾩듅�� �ъ쑀瑜� 李멸퀬�섏떆�� 愿묎퀬 �댁슜�� �섏젙�댁＜�몄슂.",
                DEACTIVATED_TITLE: "�꾨뱶由ъ뿕 ���� 愿묎퀬瑜� �뺤��섏��듬땲��.",
                DEACTIVATED_PAYMENT: "寃곗젣 �ㅽ뙣濡� 愿묎퀬媛� �뺤��섏뿀�듬땲��.\n`寃곗젣�섎떒�� �낅뜲�댄듃�댁＜�몄슂.",
                DEACTIVATED_BO: "{backoffice_note}",
                FACEBOOK_PAGE_NOT_PUBLISHED: '�섏씠�ㅻ턿 �섏씠吏�瑜� 怨듦컻 �곹깭濡� �꾪솚�댁＜�몄슂. �섏씠吏�媛� 鍮꾧났媛� �곹깭�� �뚮뒗 愿묎퀬瑜� �≪텧�� �� �놁뒿�덈떎.\n<a href="https://www.facebook.com/help/184605634921611">�섏씠吏� 怨듦컻�곹깭濡� �꾪솚�섎뒗 諛⑸쾿 �뺤씤�섍린</a>',
                FACEBOOK_INVALID_LINK: "�섏씠�ㅻ턿 愿묎퀬 �뚮옯�쇱쓽 �뺤콉�� �곕씪 �섏씠�ㅻ턿 �섏씠吏��� �섏씠�ㅻ턿 諛� �몄뒪��洹몃옩 愿묎퀬�� �쒕뵫 URL濡� �ъ슜�� �� �놁뒿�덈떎. 愿묎퀬二쇰떂�� 鍮꾩쫰�덉뒪 �뱀궗�댄듃, 留덉폆�뚮젅�댁뒪 URL �먮뒗 釉붾줈洹� �� �몃� 肄섑뀗痢좊줈 �곌껐�섎뒗 �쒕뵫 �섏씠吏� URL濡� 蹂�寃쏀빐二쇱꽭��.",
                FACEBOOK_INVALID_CTA: "�� �ㅼ튂瑜� 紐⑺몴濡� 理쒖쟻�뷀븯�� 愿묎퀬 吏묓뻾�� �꾪빐 �섏씠�ㅻ턿 �깆쓣 �곌껐�댁＜�몄슂. �섏씠�ㅻ턿 �깆씠 �놁쑝�� 寃쎌슦, �뱁럹�댁� URL�� �낅젰�댁＜�몄슂.",
                FACEBOOK_INVALID_DATE: "愿묎퀬 吏묓뻾 �붿뿬湲곌컙�� 24�쒓컙 誘몃쭔�� 寃쎌슦 愿묎퀬瑜� �낅뜲�댄듃�� �� �놁뒿�덈떎. �쇱젙 諛� �덉궛 �섏씠吏��먯꽌 愿묎퀬 湲곌컙�� �섎（ �댁긽 �곗옣�섏떊 �� �낅뜲�댄듃 �� 二쇱꽭��.",
                FACEBOOK_INVALID_DATE_UPDATE: "愿묎퀬 吏묓뻾 �붿뿬湲곌컙�� 24�쒓컙 誘몃쭔�� 寃쎌슦 愿묎퀬瑜� �낅뜲�댄듃�� �� �놁뒿�덈떎. �쇱젙 諛� �덉궛 �섏씠吏��먯꽌 愿묎퀬 湲곌컙�� �섎（ �댁긽 �곗옣�섏떊 �� �낅뜲�댄듃 �� 二쇱꽭��.",
                FACEBOOK_INVALID_CAROUSEL_LINK: "紐⑤뱺 �щ씪�대뱶 �대�吏��� �쒕뵫 URL�� �낅젰�섏뼱 �덈뒗吏� �뺤씤�댁＜�몄슂.",
                FACEBOOK_REACH_LOW: "��寃� �ㅼ젙�� �덈Т 援ъ껜�곸씤 寃쎌슦 愿묎퀬�� �몄텧�� �쒗븳�⑸땲��. ��寃� 怨좉컼�� 愿��ъ궗瑜� 異붽��섍굅�� 吏���, �곕졊 �깆쓽 踰붿쐞瑜� �뺣��섏뿬 ��寃� 踰붿쐞瑜� �볧엳�몄슂.",
                FACEBOOK_ABUSIVE_CONTENT: "�섏씠�ㅻ턿 �ъ슜�먯뿉 �섑빐 肄섑뀗痢좎뿉 ���� 遺��뺤쟻�� �쇰뱶諛깆씠 �꾩쟻�섏뼱 愿묎퀬 �뚮옯�쇱뿉 �섑빐 愿묎퀬�� �앹꽦�� 鍮꾩듅�몃릺�덉뒿�덈떎.",
                GOOGLE_POLICY: "怨듭쑀' �⑥뼱 �ъ슜�� 援ш� 愿묎퀬 �뚮옯�쇱뿉 �섑빐 �쒗븳�⑸땲��. 愿묎퀬 臾몄븞怨� �ㅼ썙�쒖뿉 �ы븿�� 湲덉� �⑥뼱 <b>{trigger}</b>瑜� ��젣�댁＜�몄슂.",
                GOOGLE_PROHIBITED: "援ш� 愿묎퀬 �뚮옯�쇱� 蹂몃옒 �⑸룄�먯꽌 踰쀬뼱�� 援щ몢�먯씠�� 湲고샇�� �ъ슜�� �쒗븳�⑸땲��. 愿묎퀬 臾몄븞�� �ы븿�� 援щ몢�먯씠�� 湲고샇 <b>{trigger}</b>�� ��젣�댁＜�몄슂.",
                UNEXPECTED_ERROR_DISAPPROVED: "{campaign} 愿묎퀬媛� �뱀씤�섏� �딆븯�듬땲��. {campaign}�먯꽌 �뺤씤�섎뒗 鍮꾩듅�� �ъ쑀瑜� 李멸퀬�댁꽌 愿묎퀬 �댁슜�� �섏젙�댁＜�몄슂.\n<b>{backoffice_note}</b>\n 鍮꾩듅�� �ъ쑀 �뺤씤�� �대졄�ㅻ㈃ �꾨뱶由ъ뿕 ���� �꾩��� �붿껌�섏꽭��.",
                button_missing_pixel: "�쎌� �ㅼ튂�섍린",
                button_missing_analytics: "援ш� �먮꼸由ы떛�� �ㅼ튂�섍린",
                button_add_more_keywords: "�ㅼ썙�� 異붽��섍린",
                button_conversion_budget_too_small: "{campaign} �덉궛 利앹븸�섍린",
                button_too_much_text_in_image: "�ъ쭊 蹂�寃쏀븯湲�",
                button_increase_audience_size: "��寃� 愿��ъ궗 異붽��섍린",
                button_low_relevancy: "�ъ쭊 & 臾멸뎄 蹂�寃쏀븯湲�",
                button_revoked_payment: "寃곗젣�섎떒 �뺤씤�섍린",
                button_revoked_policy: "�뺤콉 �뺤씤�섍린",
                button_revoked_bo: "愿묎퀬 �댁슜 �섏젙�섍린",
                button_deactivated_payment: "寃곗젣�섎떒 �뺤씤�섍린",
                button_deactivated_bo: "愿묎퀬 �ъ쭛�됲븯湲�",
                button_page_not_published: "�섏씠�ㅻ턿 �섏씠吏� �ㅼ젙 蹂�寃쏀븯湲�",
                button_facebook_invalid_link: "�쒕뵫 URL �섏젙�섍린",
                button_google_prohibited: "愿묎퀬 臾몄븞 �섏젙�섍린",
                button_facebook_invalid_date_update: "愿묎퀬 湲곌컙 �곗옣�섍린",
                button_facebook_invalid_carousel_link: "�쒕뵫 URL �뺤씤�섍린",
                button_disapproved: "愿묎퀬 �섏젙�섍린",
                button_publishing_error_default: "愿묎퀬 �ㅼ젙 �섏젙�섍린",
                button_closed: "�닿껐�� ��ぉ",
                button_pending: "�뺤씤 以�",
                toast_add_more_keywords: "吏곸젒 �ㅼ썙�쒕� 異붽��섍굅�� �⑤씪�� 梨꾪똿李쎌뿉 遺��곹븯�몄슂.",
                toast_conversion_budget_too_small: "{campaign}�� 理쒖냼 �덉궛�� {min}�쇰줈 利앹븸�섎㈃ 醫뗭뒿�덈떎",
                toast_too_much_text_in_image: "�띿뒪�� 鍮꾩쑉�� 20% 誘몃쭔�� �ъ쭊�쇰줈 蹂�寃쏀빐 二쇱꽭��.",
                toast_increase_audience_size: "��寃� 愿��ъ궗 �먮뒗 ��寃� �곕졊��瑜� 異붽��� 二쇱꽭��.",
                toast_low_relevancy: "�붿슧 留ㅻ젰�곸씤 �ъ쭊 & 臾멸뎄濡� 蹂�寃쏀빐 二쇱꽭��. �⑤씪�� 梨꾪똿李쎌뿉 遺��곹븯�붾룄 �⑸땲��.",
                toast_revoked_payment: "寃곗젣�섎떒�쇰줈 �깅줉�섏떊 �좎슜移대뱶�� �붿븸 �먮뒗 �좏슚湲곌컙 �� �ㅻ쪟 �먯씤�� �뺤씤�섏떊 �� �ㅼ떆 �좎껌�댁＜�쒓린 諛붾엻�덈떎.",
                toast_revoked_bo: "{backoffice_note} 鍮꾩듅�� �ъ쑀瑜� 李멸퀬�섏떆�� 愿묎퀬瑜� �섏젙�댁＜�몄슂",
                toast_deactivated_payment: "寃곗젣 �ㅽ뙣濡� �명빐 愿묎퀬媛� �뺤��섏뿀�듬땲��. 寃곗젣�섎떒�쇰줈 �깅줉�섏떊 �좎슜移대뱶�� �붿븸 �먮뒗 �좏슚湲곌컙 �� �ㅻ쪟 �먯씤�� �뺤씤�섏떊 �� �ㅼ떆 �좎껌�댁＜�쒓린 諛붾엻�덈떎.",
                toast_deactivated_bo: "{backoffice_note} �꾩쓽 �ъ쑀瑜� 李멸퀬�섏떆�� 愿묎퀬瑜� �ъ쭛�됲빐二쇱꽭��.",
                toast_disapproved: "{disapproval_reason} 鍮꾩듅�� �ъ쑀瑜� 李멸퀬�섏뿬 吏곸젒 �섏젙�댁＜�붾룄 臾대갑�⑸땲��.",
                toast_facebook_invalid_link: "�섏씠�ㅻ턿怨� �몄뒪��洹몃옩 愿묎퀬�� �쒕뵫 URL�� �섏젙�댁＜�몄슂.",
                toast_facebook_invalid_cta: "�섏씠�ㅻ턿�깆쓣 �곌껐�댁＜�몄슂. �섏씠�ㅻ턿�깆씠 �놁쑝�쒕떎硫� �뱀궗�댄듃 URL�� �낅젰�댁＜�몄슂.",
                toast_google_policy: "援ш�愿묎퀬�� 臾몄븞怨� �ㅼ썙�쒖뿉�� 湲덉� �⑥뼱 {trigger}瑜� ��젣�댁＜�몄슂.",
                toast_google_prohibited: "援ш� 愿묎퀬�� 臾몄븞�먯꽌 湲덉��� 湲고샇 諛� 援щ몢�� {trigger}�� ��젣�댁＜�몄슂.",
                toast_facebook_invalid_date_update: "愿묎퀬 湲곌컙�� �곗옣�댁＜�몄슂.",
                channel_google_search_campaign: "援ш� 寃���",
                channel_google_display_campaign: "援ш� �붿뒪�뚮젅��",
                channel_google_campaign: "援ш�",
                channel_facebook_instagram_campaign: "�섏씠�ㅻ턿/�몄뒪��洹몃옩",
                channel_instagram_campaign: "�몄뒪��洹몃옩",
                channel_facebook_campaign: "�섏씠�ㅻ턿",
                channel_facebook_facebook_carousel_campaign: "�섏씠�ㅻ턿 �щ씪�대뱶",
                channel_facebook_facebook_photo_campaign: "�섏씠�ㅻ턿 �대�吏�",
                channel_facebook_instagram_carousel_campaign: "�몄뒪��洹몃옩 �щ씪�대뱶",
                channel_facebook_instagram_photo_campaign: "�몄뒪��洹몃옩 �대�吏�",
                channel_google_display_ResponsiveDisplayAd_campaign: "援ш� �붿뒪�뚮젅��",
                channel_google_search_ExpandedTextAd_campaign: "援ш� 寃���",
                channel_google_universal_universalApp_campaign: "援ш� �� 愿묎퀬",
                channel_apple_search_search_campaign: "�좏뵆 �� 寃���",
                carousel: "�щ씪�대뱶",
                photo: "�대�吏�",
                ResponsiveDisplayAd: "�붿뒪�뚮젅��",
                ExpandedTextAd: "寃���",
                universalApp: "�� 愿묎퀬",
                search: "�� 寃���",
                campaign_history_start_title: "{user}�섏씠 愿묎퀬瑜� �좎껌�섏뀲�듬땲��.",
                campaign_history_start_message: "愿묎퀬 �좎껌�� �묒닔�섏뿀�듬땲��. �뚮옯�쇱쓽 寃��좊� 嫄곗튇 �꾩뿉 愿묎퀬媛� 吏묓뻾�⑸땲��.\n愿묎퀬 寃��좊뒗 �곸뾽�� 湲곗� 24�쒓컙 �댁뿉 �꾨즺�섎ŉ, 寃곌낵�� �대찓�쇰줈 �덈궡�쒕━寃좎뒿�덈떎.",
                campaign_history_update_title: "�꾨뱶由ъ뿕 ���� 愿묎퀬瑜� �섏젙�덉뒿�덈떎.",
                campaign_history_update_OPTIMIZE: "�꾨뱶由ъ뿕 ���� {campaignTitle} 愿묎퀬瑜� 媛쒖꽑�섏��듬땲��.\n愿묎퀬 �뚮옯�쇱쓽 寃��좉� �꾨즺�섎㈃ 愿묎퀬媛� 吏묓뻾�⑸땲��.",
                campaign_history_update_DISAPPROVED: "�꾨뱶由ъ뿕 ���� 鍮꾩듅�몃맂 {adType} 愿묎퀬瑜� �섏젙�섏��듬땲��.\n愿묎퀬 �뚮옯�쇱쓽 寃��좉� �꾨즺�섎㈃ 愿묎퀬媛� 吏묓뻾�⑸땲��.",
                campaign_history_update_PUBLISHING_ERROR: "�꾨뱶由ъ뿕 ���� {campaignTitle} 愿묎퀬瑜� �섏젙�섏��듬땲��.\n愿묎퀬 �뚮옯�쇱쓽 寃��좉� �꾨즺�섎㈃ 愿묎퀬媛� 吏묓뻾�⑸땲��.",
                campaign_history_update_DEFAULT: "�꾨뱶由ъ뿕 ���� {campaignTitle} 愿묎퀬瑜� �섏젙�섏��듬땲��.\n愿묎퀬 �뚮옯�쇱쓽 寃��좉� �꾨즺�섎㈃ 愿묎퀬媛� 吏묓뻾�⑸땲��.",
                campaign_history_update_OTHER: "�꾨뱶由ъ뿕 ���� {campaignTitle} 愿묎퀬瑜� �섏젙�섏��듬땲��.\n愿묎퀬 �뚮옯�쇱쓽 寃��좉� �꾨즺�섎㈃ 愿묎퀬媛� 吏묓뻾�⑸땲��.",
                emails_template_title: "�꾨뱶由ъ뿕���� �대찓�쇱쓣 諛쒖넚�섏��듬땲��. {emailTitle}",
                emails_template_message: " ",
                emails_template_title_event_message: "�대찓�� �댁슜 蹂닿린",
                campaign_history_ad_update_title: "�꾨뱶由ъ뿕 ���� 愿묎퀬瑜� �섏젙�덉뒿�덈떎.",
                campaign_history_ad_update_message: "�꾨뱶由ъ뿕 ���� {campaignTitle} 愿묎퀬瑜� 媛쒖꽑�섏��듬땲��. ",
                connect_publishing_error_title: "愿묎퀬瑜� �섏젙�댁＜�몄슂.",
                connect_publishing_error_message: "{adType} 愿묎퀬媛� {adPlatform} 愿묎퀬 �뚮옯�쇱쓽 �뺤콉�� 遺��⑺븯吏� �딆쓬�� �곕씪 吏묓뻾�� 吏��곕릺怨� �덉뒿�덈떎.\n愿묎퀬 吏묓뻾�� �꾪빐 �꾩슂�� 議곗튂瑜� 痍⑦빐二쇱꽭��.",
                connect_ad_approved_title: "愿묎퀬媛� 吏묓뻾�섍퀬 �덉뒿�덈떎.",
                connect_ad_approved_message: "{adType} 愿묎퀬 寃��좉� �꾨즺�섏뼱 愿묎퀬媛� 寃뚯옱 諛� �몄텧�섍퀬 �덉뒿�덈떎.",
                connect_ad_disapproved_title: "{platform} 寃��� 寃곌낵: 鍮꾩듅��",
                connect_ad_disapproved_message: "{adType} 愿묎퀬媛� �꾨옒 �ъ쑀濡� �명빐 �뱀씤�섏� �딆븯�듬땲��.\n{disapprovalReason}\n愿묎퀬媛� �뱀씤�� �� �덈룄濡� 鍮꾩듅�� �ъ쑀瑜� 李멸퀬�섏떆�� 愿묎퀬 �댁슜�� �섏젙�댁＜�몄슂. ",
                intercom_conversation_title: "�ㅼ떆媛� �곷떞 梨꾪똿 ���붾궡�� ",
                intercom_conversation_message: " ",
                intercom_conversation_event_message: "���붾궡�� 蹂닿린",
                message_support_title: "臾몄쓽�ы빆�� �묒닔�섏뿀�듬땲��.",
                message_support_event_message: "臾몄쓽 �댁슜 蹂닿린",
                transaction_completed_title: "愿묎퀬鍮꾧� 泥�뎄�섏뿀�듬땲��.",
                transaction_completed_message: "寃곗젣 �섎떒: {method} \n 寃곗젣 湲덉븸: {amount}",
                transaction_completed_event_message: "寃곗젣 �댁뿭 �뺤씤�섍린",
                transaction_error_title: "寃곗젣 �ㅻ쪟媛� 諛쒖깮�덉뒿�덈떎.",
                transaction_error_message: "�깅줉�� 寃곗젣�섎떒�� �좏슚�깆쓣 �뺤씤�댁＜�몄슂. ",
                transaction_error_event_message: "寃곗젣 �댁뿭 �뺤씤�섍린",
                transaction_refunded_title: "愿묎퀬鍮꾧� �섎텋�섏뿀�듬땲��.",
                transaction_refunded_message: "愿묎퀬鍮� {amount} 媛� �섎텋�섏뿀�듬땲��. �� �먯꽭�� �ы빆�� �꾨뱶由ъ뿕 ���� �듯빐 臾몄쓽�댁＜�몄슂. ",
                transaction_refunded_event_message: "寃곗젣 �댁뿭 �뺤씤�섍린",
                campaign_history_ad_update_link_message: "愿묎퀬 �ㅼ젙 蹂닿린",
                campaign_history_update_link_message: "愿묎퀬 �ㅼ젙 蹂닿린",
                connect_publishing_error_link_message: "痍⑦빐�� �� 議곗튂 �뺤씤�섍린",
                connect_ad_approved_link_message: "愿묎퀬 寃곌낵 �꾪솴 蹂닿린",
                connect_ad_disapproved_link_message: "愿묎퀬 �ㅼ젙 蹂닿린",
                campaign_history_revoked_title: "�꾨뱶由ъ뿕 寃��� 寃곌낵: 鍮꾩듅��",
                campaign_history_revoked_message_A: "�붿껌�섏떊 愿묎퀬 {campaignTitle} 媛� 寃곗젣�섎떒 �ㅻ쪟濡� �뱀씤�섏� �딆븯�듬땲��.\n寃곗젣�섎떒�쇰줈 �깅줉�섏떊 �좎슜移대뱶�� �붿븸 �먮뒗 �좏슚湲곌컙 �� �ㅻ쪟 �먯씤�� �뺤씤�섏떊 �� �ㅼ떆 �좎껌�댁＜�쒓린 諛붾엻�덈떎.",
                campaign_history_revoked_message_B: "�붿껌�섏떊 愿묎퀬 {campaignTitle} 媛� 愿묎퀬�뺤콉 �꾨컲�쇰줈 �뱀씤�섏� �딆븯�듬땲��.\n�꾨뱶由ъ뿕 愿묎퀬 �뺤콉 �뺤씤�섍린�몛 https://intercom.help/adriel/en/articles/2661761",
                campaign_history_revoked_message_C: "�붿껌�섏떊 愿묎퀬 {campaignTitle} 媛� �뱀씤�섏� �딆븯�듬땲��.\n{backofficeNote}\n鍮꾩듅�� �ъ쑀瑜� 李멸퀬�섏떆�� 愿묎퀬瑜� �섏젙�댁＜�몄슂. ",
                campaign_history_revoked_link_message: "痍⑦빐�� �� 議곗튂 �뺤씤�섍린 ",
                connect_ad_revoke_link_message: "痍⑦빐�� �� 議곗튂 �뺤씤�섍린 ",
                campaign_history_deactivate_title_ADRIEL: "�꾨뱶由ъ뿕 ���� 愿묎퀬瑜� �뺤��섏��듬땲��.",
                campaign_history_deactivate_message_ADRIEL_A: "寃곗젣 �ㅽ뙣濡� 愿묎퀬媛� �뺤��섏뿀�듬땲��. 寃곗젣�섎떒�� �낅뜲�댄듃�댁＜�몄슂. ",
                campaign_history_deactivate_message_ADRIEL_B: "{backofficeNote}",
                campaign_history_deactivate_link_message_ADRIEL: "痍⑦빐�� �� 議곗튂 �뺤씤�섍린",
                campaign_history_deactivate_title_USER: "{user}�섏씠 愿묎퀬 �쒖븞�� �좎껌�섏뀲�듬땲��.",
                campaign_history_deactivate_message_USER: "愿묎퀬瑜� 硫덉텛�� �밸퀎�� �댁쑀媛� �덉쑝�좉���? �꾨뱶由ъ뿕 ���먭쾶 �쇰뱶諛깆쓣 �④꺼二쇱꽭��.",
                campaign_history_deactivate_link_message_USER: "�꾨뱶由ъ뿕 �ъ슜 寃쏀뿕�� 怨듭쑀�댁＜�몄슂.",
                campaign_history_deactivate_title_DEFAULT: "�꾨뱶由ъ뿕 ���� 愿묎퀬瑜� �뺤��섏��듬땲��.",
                campaign_history_deactivate_message_DEFAULT_A: "愿묎퀬 湲곌컙�� �곗옣�섍퀬 �띠쑝�쒕떎硫�,",
                campaign_history_deactivate_message_DEFAULT_B: '鍮꾪솢�깊솕�댁빞 �� �댁쑀媛� 紐낇솗�섏� �딆� 寃쎌슦(踰꾧렇 �덈갑) --\x3e "吏덈Ц�� �덉쑝硫� �꾨뱶由ъ뿕�먭쾶 臾몄쓽�섏떗�쒖삤. "',
                campaign_history_deactivate_link_message_DEFAULT_A: " �ш린瑜� �뚮윭二쇱꽭��.",
                empty_chat_list: "�묒꽦�� �볤��� �놁뒿�덈떎.",
                empty_intercom: "�명꽣而댁씠 �놁뒿�덈떎.",
                empty_feed: "�쇰뱶媛� �놁뒿�덈떎.",
                no_title: "�쒕ぉ �놁쓬",
                email_modal_title: "�대찓��",
                see_more: "�붾낫湲�",
                close: "�リ린",
                move: "�대룞"
            },
            jp: {},
            en: {
                top_title: "My Manager",
                no_image: "No image",
                no_content: "There aren't any conversations on this topic yet.",
                delete_modal: "Are you sure you want to delete this action?",
                delete_modal_confirm: "Delete",
                newsfeed: "News feed",
                actionRequired: "Action required",
                thread: "Threads",
                contact_adriel: "Contact Adriel",
                REVOKED_TITLE: "Adriel disapproved your campaign.",
                REVOKED: "",
                STRIPE: "STRIPE",
                COUPONS: "COUPONS",
                TRANSFER: "TRANSFER",
                REVOKED_PAYMENT: "Your campaign, {campaign_title}, has been revoked due to a payment error. Please update your payment method before launching your campaign again.",
                REVOKED_POLICY: "Your campaign, {campaign_title}, has been revoked due to violation of our advertising policies.\nPlease see <a href='https://intercom.help/adriel/en/articles/2454735'>Adriel's Advertising Policies</a>",
                REVOKED_BO: "Your campaign, {campaign_title}, has been revoked due to the following reason.\n{backoffice_note}\nPlease revise your campaign according to the comment above.",
                PUBLISHING_ERROR_TITLE: "Your ads are pending.",
                PUBLISHING_ERROR: "Please take necessary actions in order to run your {campaign} ad.",
                DISAPPROVED_TITLE: "Your ads are disapproved.",
                DISAPPROVED: "{campaign} ad is disapproved due to the following reason.\nThe ads are paused for the time being.Team Adriel will take necessary actions within a working day.\nFeel free to update the ads yourself according to the disapproval reason. ",
                DEACTIVATED_TITLE: "Adriel stopped your campaign.",
                DEACTIVATED_PAYMENT: "Your campaign is deactivated due to payment failure.\nPlease update your payment method.",
                DEACTIVATED_BO: "{backoffice_note}",
                FACEBOOK_PAGE_NOT_PUBLISHED: 'Your Facebook page linked with Adriel ads is not published. In order to execute the ads, the Facebook page linked must be published. Currently, your Facebook ads are paused. <a href="https://www.facebook.com/help/184605634921611">See how to publish a page</a>',
                FACEBOOK_INVALID_LINK: "Unfortunately, you can't use a Facebook Page as a landing URL for your campaign. To quote the Facebook ad platform's policy, a link to external contents (e.g. an advertiser's website) must be provided as a landing page for Facebook and Instagram ads.\nAction requied: Please change your landing URL and update the campaign.",
                FACEBOOK_INVALID_CTA: "In order to optimize your ads with App Install objective, Facebook App is required. Please connect to your Facebook App. If you don't have one, please submit the landing URL.",
                FACEBOOK_INVALID_DATE: "If your ad has less than 24 hours untill it ends, you cannot edit it. Please go to 'Budget & Schedule' to extend the ad schedule in order to update your ad.",
                FACEBOOK_INVALID_DATE_UPDATE: "If your ad has less than 24 hours untill it ends, you cannot edit it. Please go to 'Budget & Schedule' to extend the ad schedule in order to update your ad.",
                FACEBOOK_INVALID_CAROUSEL_LINK: "Please check whether you have submitted your landing URL for every slide image.",
                FACEBOOK_REACH_LOW: "If your targeting setting is too specific, your ad impressions will be limited. Please expand your targeting range by including more audience interests or by submitting additional targeting age or location.",
                FACEBOOK_ABUSIVE_CONTENT: "Facebook Ad platfrom disapproved your ads as it includes content that other people on Facebook have reported as abusive.",
                GOOGLE_POLICY: "Google Ad platfrom does not allow the use of the word <b>{trigger}</b>. Please remove the wording from the ad text or the keywords.",
                GOOGLE_PROHIBITED: "Google policy does not permit excessive or unnecessary punctuation or symbols, or use of nonstandard punctuation, including tildes (~), asterisks (*),and vertical rules(|). Please remove forbidden punctuations or symbols  <b>{trigger}</b> from the ad text.",
                UNEXPECTED_ERROR_DISAPPROVED: "{campaign} ad is disapproved.\nPlease check the reason for {campaign} ad disapproval and edit your ad settings accordingly.\n<b>{backoffice_note}</b>\nFeel free to contact us if you're unsure of the reason.",
                publishing_error: "Your ads are pending",
                button_missing_pixel: "How do I install Pixel?",
                button_missing_analytics: "How do I install Google Analytics?",
                button_add_more_keywords: "Add more keywords",
                button_conversion_budget_too_small: "Increase {campaign}'s budget",
                button_too_much_text_in_image: "Upload a new image",
                button_increase_audience_size: "Add more audience interests",
                button_low_relevancy: "Change to a new image and texts",
                button_revoked_payment: "Update payment method",
                button_revoked_policy: "See more information",
                button_revoked_bo: "See more information",
                button_deactivated_payment: "Update payment method",
                button_deactivated_bo: "See more information",
                button_page_not_published: "Change facebook page setttings.",
                button_facebook_invalid_link: "Change landing URL.",
                button_google_prohibited: "Edit ad text",
                button_facebook_invalid_date_update: "Extend ad schedule",
                button_facebook_invalid_carousel_link: "Check landing URL",
                button_disapproved: "Update my campaign",
                button_publishing_error_default: "Go to campaign settings",
                button_closed: "Closed",
                button_pending: "Pending",
                toast_add_more_keywords: "Please add more keywords or contact Team Adriel for help.",
                toast_conversion_budget_too_small: "Please increase the {campaign} budget level to {min}.",
                toast_too_much_text_in_image: "Please upload a new image with less than 20 percent text coverage or an image without any text.",
                toast_increase_audience_size: "Set less limited audience profile by adding more interests or more age groups to your campaign.",
                toast_low_relevancy: "Please upload a new image and text that may be more relavant to your audience or contact Team Adriel.",
                toast_revoked_payment: "Please update your payment method before launching your campaign again",
                toast_revoked_bo: "{backoffice_note} Please revise your campaign according to the comment above.",
                toast_deactivated_payment: "Your campaign is deactivated due to payment failure. Please update your payment method.",
                toast_deactivated_bo: "{backoffice_note}\nPlease revise your campaign according to the comment above.",
                toast_disapproved: "{disapproval_reason} Update the ads yourself according to the disapproval reason.",
                toast_facebook_invalid_link: "Change the landing URL for your Facebook and/or Instagram ad.",
                toast_facebook_invalid_cta: "Please connect to your Facebook App. If you don't have one, please submit the landing URL.",
                toast_google_policy: "Please remove forbidden words {trigger} from your Googld Ads text and keywords.",
                toast_google_prohibited: "Please remove unnecessary punctuations or symbols  {trigger} from your Google Ads text and keywords.",
                toast_facebook_invalid_date_update: "Please extend your ad schedule.",
                channel_google_search_campaign: "Google search",
                channel_google_display_campaign: "Google display",
                channel_google_campaign: "Google",
                channel_facebook_instagram_campaign: "Facebook/Instagram",
                channel_instagram_campaign: "Instagram",
                channel_facebook_campaign: "Facebook",
                channel_facebook_facebook_carousel_campaign: "Facebook Carousel",
                channel_facebook_facebook_photo_campaign: "Facebook Image",
                channel_facebook_instagram_carousel_campaign: "Instagram Carousel",
                channel_facebook_instagram_photo_campaign: "Instagram Image",
                channel_google_display_ResponsiveDisplayAd_campaign: "Google Display",
                channel_google_search_ExpandedTextAd_campaign: "Google Search",
                channel_google_universal_universalApp_campaign: "Google App",
                channel_apple_search_search_campaign: "Apple App Search",
                carousel: "Carousel",
                photo: "Image",
                ResponsiveDisplayAd: "Display",
                ExpandedTextAd: "Search",
                universalApp: "App",
                search: "App Search",
                campaign_history_start_title: "You launched a campaign.",
                campaign_history_start_message: "Your campaign, {campaignTitle}, is under review by the ad platform(s).\nThis process may take up to 24 hours.\nYour ads will be up and running as soon as the review is done.\nThe results will be informed via email. ",
                campaign_history_update_title: "Adriel updated your campaign.",
                campaign_history_update_OPTIMIZE: "Team Adriel improved your campaign, {campaignTitle}.\nYour ads will be live after a review is done by the ad platfrom(s).",
                campaign_history_update_DISAPPROVED: "Team Adriel has modified the disapproved {adType} ads.\nYour ads will be live after a review is done by the ad platfrom(s). ",
                campaign_history_update_PUBLISHING_ERROR: "Team Adriel updated your campaign, {campaignTitle}.\nYour ads will be live after a review is done by the ad platform(s).",
                campaign_history_update_DEFAULT: "Team Adriel updated your campaign, {campaignTitle}.\nYour ads will be live after a review is done by the ad platform(s).",
                campaign_history_update_OTHER: "Team Adriel updated your campaign, {campaignTitle}.\nYour ads will be live after a review is done by the ad platform(s).",
                emails_template_title: "Adriel Team has sent an Email. {emailTitle}",
                emails_template_message: " ",
                emails_template_title_event_message: "See email",
                campaign_history_ad_update_title: "Adriel updated your campaign.",
                campaign_history_ad_update_message: "Team Adriel improved your campaign, {campaignTitle}. See what's been improved Your ads will be live after a review is done by the ad platfrom(s).",
                connect_publishing_error_title: "Your ads are pending",
                connect_publishing_error_message: "{adType} is pending  because it does not comply with the Ads Platform(s) Advertising Policies.\n Pending on {adPlatform}\nPlease take neccessary actions in order to run your {adType} ad.",
                connect_ad_approved_title: "Your ads are running",
                connect_ad_approved_message: "{adType} ad is now up and running!",
                connect_ad_disapproved_title: "Your ads are disapproved",
                connect_ad_disapproved_message: "{adType} ad is disapproved due to the following reason.\n{disapprovalReason}\nThe ads are paused for the time being.\nPlease update the ads according to the disapproval reason.",
                intercom_conversation_title: "Your conversation with Adriel ",
                intercom_conversation_message: " ",
                intercom_conversation_event_message: "See conversation",
                message_support_title: "Support request submitted. {campaignTitle} {requestTitle} ",
                message_support_event_message: "See conversation",
                transaction_completed_title: "Transaction completed.",
                transaction_completed_message: "Payment Method: {method} \n Paid Amount: {amount}",
                transaction_completed_event_message: "Link to Invoice History",
                transaction_error_title: "Transaction failed.",
                transaction_error_message: "Please update your payment Method.",
                transaction_error_event_message: "Link to User Setting Page",
                transaction_refunded_title: "Transaction canceled.",
                transaction_refunded_message: "Adriel has refunded you {amount}. Feel free to contact us for more details",
                transaction_refunded_event_message: "Link to Invoice History",
                campaign_history_ad_update_link_message: "Things we have done page",
                campaign_history_update_link_message: "Things we have done page",
                connect_publishing_error_link_message: "See the actions required list",
                connect_ad_approved_link_message: "See the campaign results dashboard",
                connect_ad_disapproved_link_message: "See the campaign",
                campaign_history_revoked_title: "Adriel disapproved your campaign.",
                campaign_history_revoked_message_A: "Your campaign, {campaignTitle}, has been revoked due to a payment error.\nPlease update your payment method before launching your campaign again. ",
                campaign_history_revoked_message_B: "Your campaign, {campaignTitle}, has been revoked due to violation of our advertising policies.\nPlease see Adriel's Advertising Policies( https://intercom.help/adriel/en/articles/2454735)",
                campaign_history_revoked_message_C: "Your campaign, {campaignTitle}, has been revoked due to the following reason.\n{backofficeNote}\nPlease revise your campaign according to the comment above. ",
                campaign_history_revoked_link_message: "Take an action to get approval",
                connect_ad_revoke_link_message: "Take an action to get approval",
                campaign_history_deactivate_title_ADRIEL: "Adriel Stopped your campaign.",
                campaign_history_deactivate_message_ADRIEL_A: "Your campaign is deactivated due to payment failure.  Please update your payment method. ",
                campaign_history_deactivate_message_ADRIEL_B: "{backofficeNote}",
                campaign_history_deactivate_link_message_ADRIEL: "See the actions required list",
                campaign_history_deactivate_title_USER: "You deactivated the campaign",
                campaign_history_deactivate_message_USER: "Any reason for stopping the camapaign? Any feedback is appreciated.",
                campaign_history_deactivate_link_message_USER: "Tell us about your experience with Adriel ",
                campaign_history_deactivate_title_DEFAULT: "Your campaign is deactivated.",
                campaign_history_deactivate_message_DEFAULT_A: "If you want to extend the campaign schedule, ",
                campaign_history_deactivate_message_DEFAULT_B: 'If reason to deactivate not clear (bug prevention) --\x3e " If you have questions about it, please contact Adriel. "',
                campaign_history_deactivate_link_message_DEFAULT_A: "click here",
                empty_chat_list: "No Message",
                empty_intercom: "No Intercom",
                empty_feed: "No Feed",
                no_title: "No Title",
                email_modal_title: "email",
                see_more: "See more",
                close: "Close",
                move: "Link"
            }
        }
    }, 9636: function (e, t) {
        e.exports = {
            en: {
                availables: "We provide service in",
                availables_0: "English",
                availables_1: "French",
                availables_2: "Korean",
                by_adriel: "Crafted by Marketing\nExperts at Adriel AI",
                comments: "Additional comments (optional)",
                comments_placeholder: "e.g. my customers are mostly in London",
                express_title: "Create a campaign for me",
                image_upload: "Images or videos you would like to use (optional)",
                includes_content_1: "We create the ads. Using your website as a guide, Adriel will create tailored ads for your business.",
                includes_content_2: "We set up the details. Adriel tailors your ads that best fit each advertising platform including Facebook, Instagram and Google Ads.",
                includes_content_3: "We research your target customers. Adriel develops a customized target audience based on your brand, product or services.",
                includes_content_4: "We send you a proposal. Adriel provides a plan for schedule & budget to deliver your ads to your target customers",
                includes_title: "Adriel Proposal Request includes the following services:",
                info_detail_1: "Your Adriel Proposal will be sent to {email}. Please make sure this is the right email address.",
                info_detail_2: 'If you don�셳 want to use Adriel Proposal Request, you can simply click <span class="highlight">�쏯ew campaign��</span> to design ads on your own.',
                info_detail_3: "When you create your own ads, we recommend you to use a desktop computer(Chrome browser) instead of a mobile device to fully utilize the features of Adriel.",
                info_detail_4: "Adriel Proposal Request is only provided during working hours (Mon-Fri, 10am �� 7pm) and can take up to 3 business days.",
                info_detail_5: "Once your Adriel Proposal Request is submitted, there will be no refunds.",
                main_image: "/img/express/express_img_en.png",
                ment_top_big: "Adriel Proposal Request takes all the work out of creating an advertising campaign for your business.",
                ment_top_small: "Just provide us your website address. We'll do the rest. <br/>The cost? {currency} 9",
                payment_method: "Payment method",
                price_info: 'When you click apply button below, <span style="text-decoration: line-through">{currency} 19</span> <b style="color:red">{currency} 9</b> will be charged and your request is submitted automatically.',
                request_sent: "Your request has been sent to the Adriel Marketing Team.\n We will get back to you as soon as possible.",
                submit: "Apply",
                title: "Adriel Proposal Request",
                url: "Website URL (required)",
                url_placeholder: "https://example.com",
                we_here: "We're here to help you",
                we_here_content: "It doesn't matter what kind of<br> questions you have.<br> Simply send us a message and<br> we'll be happy to assist you!"
            },
            jp: {
                availables: "�먧풘�쀣겍�꾠굥鼇�沃�",
                availables_0: "�볟쎖沃�",
                availables_1: "�김첑",
                availables_2: "�뺛꺀�녈궧沃�",
                by_adriel: "�㏂깋�ゃ궓�ャ겗�욁꺖�긱깇�ｃ꺍�겹궓��궧�묆꺖�덀겓�덀겂�╊퐳�먦걬�뚣겲�쀣걼��",
                comments: "�녈깳�녈깉",
                comments_placeholder: "�귙겒�잆겗�볝궦�띲궧�곥궖�밤궭�욁꺖�곥걡�덀겞�띴에�쀣걼�꾠궘�ｃ꺍�싥꺖�녈궭�ㅳ깤�ャ겇�꾠겍�쇻걟�╉걦�졼걬�꾠��",
                express_title: "��깵�녈깪�쇈꺍�믢퐳�먦걲��",
                image_upload: "�삣깗�곥겲�잆겘�뺟뵽 (�ゃ깤�룔깾��)",
                includes_content_1: "�㏂깋�ゃ궓�ャ걣佯껃몜�믢퐳�먦걮�얇걲�귙걗�ゃ걼��궑�㎯깣�듐궎�덀굮�с궎�됥꺀�ㅳ꺍�ⓦ걮��뢿�㎯걲�뗣걪�ⓦ겎�곥걗�ゃ걼��깛�멥깓�밤겓�덀겂�잌틕�듽굮鵝쒏닇�쀣겲�쇻��",
                includes_content_2: "屋녕눗�믦Þ若싥걮�얇걲�귙궋�됥꺁�ⓦ꺂�뚣걗�ゃ걼��틕�듽굮�갌acebook/Instagram/Google�믣맜���ⓦ겍��깤�⒲긿�덀깢�⒲꺖�졼겓�덀걝�덀걝�ユ��⒴뙑�쀣겲�쇻��",
                includes_content_3: "�㏂깋�ゃ궓�ャ겘�귙겒�잆겗�뽧꺀�녈깋�삣븚�곥꺕�듐꺖�볝궧�ャ걗�ｃ걼�욍꺖�꿔긿�덀궕�쇈깈�ｃ궓�녈궧�믦た�삠걚�잆걮�얇걲��",
                includes_content_4: "�㏂깋�ゃ궓�ャ겘�귙겒�잆겗�욍꺖�꿔긿�덀겓�덀겂�잆궧�긱궦�γ꺖�ャ겏雅덄츞�먩죭�믤룓堊쎼걮�얇걲��",
                includes_title: "�㏂깋�ゃ궓�ャ꺕�ⓦ궚�밤깤�с궧��А��궢�쇈깛�밤굮�ャ겳�얇걲竊�",
                info_detail_1: "�㏂깋�ゃ궓�ャ꺕�ⓦ궚�밤깤�с궧�ャ굠�뗦룓旅덀겘{email}�ラ�곥굢�뚣겲�쇻�귙걪��깳�쇈꺂�㏂깋�с궧�뚧��쀣걚�뗧▶沃띲걮�╉걦�졼걬�꾠��",
                info_detail_2: '�㏂깋�ゃ궓�ャ꺕�ⓦ궚�밤깤�с궧�믡걫�⑴뵪�ャ겒�됥겒�꾢졃�덀겘�� <span class="highlight">�쒏뼭誤뤵궘�ｃ꺍�싥꺖�년��</span> �믤듉�쀣겍��깵�녈깪�쇈꺍�믢퐳�먦걮�╉걦�졼걬�꾠��',
                info_detail_3: "�귙겒�잆걣佯껃몜�믢퐳�먦걲�뗥졃�덀겘�곥궋�됥꺁�ⓦ꺂��찣�썬굮�ⓦ겍鵝욍걟�뗣�곥깈�밤궚�덀긿�쀧뮥罌껁겎烏뚣걝�볝겏�믤렓也ⓦ걮�얇걲��",
                info_detail_4: "�㏂깋�ゃ궓�ャ꺕�ⓦ궚�밤깤�с궧��뼳璵�셽�� (9a - 6pm) ��겳�먧풘�뺛굦�얇걲�귡뼀冶뗣겲�㎯걡�덀걹3�뜻��γ걢�뗣굤�얇걲��",
                info_detail_5: "訝�佯╉궋�됥꺁�ⓦ꺂�삠궓��궧�쀣꺃�밤걣�ゃ궚�ⓦ궧�덀걬�뚣굥�ⓦ�곫돂�꾣댗�쀣겘�귙굤�얇걵�볝��",
                main_image: "/img/express/express_img_en.png",
                ment_top_big: "�㏂깋�ゃ궓�ャ꺕�ⓦ궚�밤깤�с궧��궘�ｃ꺍�싥꺖�녈굮�ャ��ⓦ겍��램葉뗣굮獄뗣걨縕졼걚�얇걲��",
                ment_top_small: "�╉궒�뽧궢�ㅳ깉�믤룓堊쎼걮�╉걚�잆걽�묆굦�겹�곥궋�됥꺁�ⓦ꺂�뚦풄��걲�밤겍�ゅ땿�㎬죱�꾠겲�쇻��<br/> 縕사뵪�㎯걲�뗰폕 {currency} 9",
                payment_method: "�딀뵱�뺛걚�방퀡",
                price_info: 'We will charge you <span style="text-decoration: line-through">{currency}19</span> <b style="color:red">{currency}9</b> to create a proposal for you',
                request_sent: "�ゃ궚�ⓦ궧�덀겘�㏂깋�ゃ궓�ャ겗�곥꺖�졼겓�곦에�뺛굦�얇걮�잆��\n 瓦낂�잆겓野얍퓶�꾠걼�쀣겲�쇻��",
                submit: "�뺟㉮",
                title: "�㏂깋�ゃ궓�ャ꺕�ⓦ궚�밤깤�с궧",
                url: "�╉궒�뽧궢�ㅳ깉 URL",
                url_placeholder: "https://example.com",
                we_here: "鵝뺛걢�듿쎇�듽겎�쇻걢竊�",
                we_here_content: "It doesn't matter what kind of<br> question you have.<br> Simply send us a message and<br> we'll be happy to assist you."
            },
            ko: {
                availables: "愿묎퀬 �쒖옉 媛��� �몄뼱",
                availables_0: "�쒓뎅��",
                availables_1: "�곸뼱",
                availables_2: "遺덉뼱",
                by_adriel: "by �꾨뱶由ъ뿕 留덉��� ��",
                comments: "湲고� �붿껌�ы빆 (�좏깮)",
                comments_placeholder: '"諛앹� �먮굦�� �대�吏�瑜� �ъ슜�댁＜�몄슂" "諛쒕엫�� 愿묎퀬 移댄뵾瑜� �⑥＜�몄슂" "誘몄＜ 吏���쓣 ��寃잜븯怨� �띠뒿�덈떎" �� 愿묎퀬 �뚯옱 諛� ���곸뿉 ���� 媛꾨떒�� �붿껌�ы빆怨� 愿묎퀬�� 紐⑹쟻�� �④꺼二쇱꽭��. �대뒗 李멸퀬 �ы빆�대ŉ, 紐⑤뱺 �붿껌 �ы빆�� 諛섏쁺�섏� �딆쓣 �� �덉뒿�덈떎.',
                express_title: "愿묎퀬 �쒖븞 �쒕퉬�� �좎껌 �섏씠吏�",
                image_upload: "愿묎퀬 �대�吏� 諛� 鍮꾨뵒�� (�좏깮)",
                includes_content_1: "愿묎퀬二쇰떂�� �뱀궗�댄듃瑜� 李멸퀬�섏뿬 �뚯옱瑜� �좎젙, �곸젅�� 愿묎퀬 �대�吏��� 移댄뵾瑜� �쒖븞�⑸땲��.",
                includes_content_2: "�섏씠�ㅻ턿怨� �몄뒪��洹몃옩 쨌 援ш� 媛� �뚮옯�쇱뿉 留욊쾶 �명똿�⑸땲��.",
                includes_content_3: "愿묎퀬二쇰떂�� 釉뚮옖�쒖� �쒗뭹 諛� �쒕퉬�ㅼ뿉 留욌뒗 ��寃� 怨좉컼援곗쓣 �ㅼ젙�⑸땲��.",
                includes_content_4: "�꾪솚 �⑥쑉�� �믪씠湲� �꾪븳 �꾨뱶由ъ뿕 留덉��� ���� 媛꾨떒�� �쒖븞�� �대찓�쇰줈 �꾨떖 �쒕┰�덈떎.",
                includes_title: "愿묎퀬 �쒖븞 �쒕퉬�ㅼ뿉�� �ㅼ쓬 �ы빆�� �ы븿�⑸땲��.",
                info_detail_1: "�꾨뱶由ъ뿕�� �쒖븞 �꾨즺 �덈궡 �대찓�쇱� {email} �쇰줈 �〓��⑸땲��. 諛섎뱶�� �대찓�쇱쓣 �뺤씤 �댁＜�몄슂.",
                info_detail_2: '�좊즺 �쒖븞 �쒕퉬�ㅻ� �먰븯吏� �딆쑝�� 寃쎌슦, "<span class="highlight">�� 愿묎퀬 留뚮뱾湲�</span>" 踰꾪듉�� �꾨Ⅸ �� �먯돺寃� 愿묎퀬二쇰떂�� 吏곸젒 愿묎퀬瑜� �쒖옉�섏떎 �� �덉뒿�덈떎.<br/> �� 寃쎌슦 �좊즺 �쒖븞 �쒕퉬�� 鍮꾩슜�� 泥�뎄�섏� �딆쑝硫�, 愿묎퀬 吏묓뻾 �댄썑 �ㅼ젣 �대┃�� �섑빐 諛쒗뻾�� 鍮꾩슜留� 泥�뎄�⑸땲��.',
                info_detail_3: "吏곸젒 愿묎퀬瑜� �쒖옉�섏떎 寃쎌슦, �쒖옉 怨쇱젙�� 紐⑤컮�쇰낫�� �곗뒪�ы깙 �섍꼍 (�щ＼ 釉뚮씪�곗�)�먯꽌 理쒖쟻�� �섏뼱 �덉쑝�� 李멸퀬�� 二쇱떆湲� 諛붾엻�덈떎.",
                info_detail_4: "愿묎퀬 �쒖븞 �쒕퉬�ㅻ뒗 �됱씪 �낅Т�쒓컙 �댁뿉 �대（�댁�硫�, 怨듯쑕�쇱쓣 �쒖쇅�섍퀬 理쒕� 3�쇨퉴吏� �뚯슂�� �� �덉뒿�덈떎.",
                info_detail_5: "�쒖옉 �댄썑�먮뒗 �섎텋�섏� �딆뒿�덈떎.",
                main_image: "/img/express/express_img.png",
                ment_top_big: "愿묎퀬 �쒖옉�� �대젮�곗떊媛���?",
                ment_top_small: "諛붿걯�� 愿묎퀬二쇰떂�� ���좏빐, 愿묎퀬 �뚯옱 �좎젙遺��� �명똿源뚯� 紐⑤몢 �꾨뱶由ъ뿕�� �대뱶由쎈땲��.<br> �댄썑 愿묎퀬 吏묓뻾 �щ��� 愿묎퀬二쇰떂猿섏꽌 寃곗젙�섏떎 �� �덉쑝��, 媛�蹂띻쾶 �좎껌�대낫�몄슂.",
                payment_method: "寃곗젣 �섎떒",
                price_info: '"�좎껌�섍린" 踰꾪듉�� �꾨Ⅴ�쒕㈃ <span style="text-decoration: line-through">{currency}19</span> <b>{currency}9</b> 寃곗젣 �� �쒕퉬�� �좎껌�� �먮룞�쇰줈 �묒닔�⑸땲��.',
                request_sent: "愿묎퀬 �쒖븞 �쒕퉬�� �좎껌�� �깃났�곸쑝濡� �묒닔�섏뿀�듬땲��.\n �꾨뱶由ъ뿕 ���먯꽌 �뺤씤 �� �대찓�쇰줈 �곕씫 �쒕━�꾨줉 �섍쿋�듬땲��.",
                submit: "�좎껌�섍린",
                title: "�� 愿묎퀬 留뚮뱾湲�",
                url: "�뱀궗�댄듃 二쇱냼 (�꾩닔)",
                url_placeholder: "https://example.com",
                we_here: "�꾨뱶由ъ뿕�� �꾩��쒕┫寃뚯슂.",
                we_here_content: "�대뼡 吏덈Ц�대뱺 臾몄쓽�댁＜�몄슂.<br> 紐⑤뱺 吏덈Ц�� �깆떖猿� �듬��쒕━寃좎뒿�덈떎."
            }
        }
    }, "97a6": function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), o = a.n(n), i = (a("7f7f"), a("bd86")), r = a("768b"), s = a("75fc"), c = a("cebc"),
                l = a("15b8"), d = a.n(l), u = a("c082"), p = a("fdd2"), _ = a("288f"), m = a("fa7d"),
                f = (a("f2e5"), a("4c6b")), g = a("2bd2"), h = a("6e77"), b = a("9586"), v = a("1b92"), y = a("5670"),
                w = a("1a2d"), k = a("d792"), x = a("ebb6"), O = a("66aa"), A = a("f59d"), C = a("d96a"), S = a("9f2d"),
                E = a("c4cc"), T = a("4b59"), P = a("87ee"), I = a("1585"), L = a("dde5"), j = a("f13c"),
                M = [f["q"].up, f["q"].down, f["q"].enter], R = d()(["web", "app", "form"]), N = {
                    container: ".web-app-auto-complete",
                    easing: "ease-in",
                    offset: 0,
                    force: !0,
                    cancelable: !0,
                    x: !1,
                    y: !0
                }, D = d()({
                    attrs: {
                        type: "input",
                        resizable: !1,
                        additionalClass: "normal",
                        autocorrect: "off",
                        autocapitalize: "none"
                    }
                }), $ = {
                    type: "web",
                    playStoreUrl: "",
                    appStoreUrl: "",
                    websiteUrl: "",
                    developerSiteUrl: "",
                    playstoreMode: "search"
                };
            t["a"] = {
                name: "WebOrApp", mounted: function () {
                    this.observeSearch$(), this._value = e.mergeLeft(this.value, $), this.observeArrow$()
                }, props: {
                    value: {
                        type: Object, default: function () {
                            return Object(c["a"])({}, $)
                        }
                    }, dirty: {type: Boolean, default: !1}, showCheckIcon: {type: Boolean, default: !1}
                }, data: function () {
                    return {
                        search$: new g["a"],
                        appleSearch: "",
                        googleSearch: "",
                        appleResults: [],
                        googleResults: [],
                        resultsLength: 0,
                        isAppstoreLoading: !1,
                        isPlaystoreLoading: !1,
                        playstoreFailed: !1,
                        dropdownIdx: -1,
                        selectedValue: {}
                    }
                }, render: function () {
                    var e = arguments[0];
                    return e("div", {class: "onboarding-app-url-container"}, [e("div", {class: "onboarding-app-url--boxes"}, [this.types.map(this.renderBox)]), this.renderInputs(this.value.type)])
                }, computed: {
                    _value: {
                        get: e.prop("value"), set: function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            this.$emit("input", e)
                        }
                    },
                    websiteUrl: e.path(["_value", "websiteUrl"]),
                    appStoreUrl: e.path(["_value", "appStoreUrl"]),
                    playStoreUrl: e.path(["_value", "playStoreUrl"]),
                    developerWebsite: e.path(["_value", "developerSiteUrl"]),
                    noAppUrls: e.compose(e.isEmpty, m["k"], e.props(["appStoreUrl", "developerSiteUrl"])),
                    types: function () {
                        return R
                    },
                    playstoreMode: e.pathOr("search", ["value", "playstoreMode"]),
                    dropdownActive: function () {
                        return Object(m["N"])([].concat(Object(s["a"])(this.appleResults), Object(s["a"])(this.googleResults)))
                    },
                    _selectedValue: {
                        get: e.propOr({}, "selectedValue"), set: function (e) {
                            this.selectedValue = e
                        }
                    }
                }, methods: {
                    renderInputs: function () {
                        var t = this, a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "web",
                            n = this.$createElement, i = this.websiteUrl, r = "search" === this.playstoreMode;
                        return function () {
                            switch (a) {
                                case"web":
                                case"form":
                                    return n("div", {class: "onboarding-app-url--inputs"}, [n("div", {class: "field-container"}, [n("span", ["web" === a ? t.$t("ONBOARDING.web_or_app_website_address") : t.$t("ONBOARDING.form_website_address")]), n(u["a"], o()([{}, D, {
                                        attrs: {
                                            value: i,
                                            autofocus: !0,
                                            placeholder: "https://website.com",
                                            error: "web" == a && t.dirty && !Object(m["M"])(i) ? t.$t("COMMON.invalid_url") : ""
                                        }, on: {
                                            input: t.updateValue("websiteUrl").bind(t), keypress: function (e) {
                                                var a = e.keyCode;
                                                13 === a && t.$emit("next")
                                            }
                                        }
                                    }]))])]);
                                case"app":
                                    return n("div", {class: "onboarding-app-url--inputs app-url"}, [n("div", {class: "field-container"}, [n("span", [t.$t("ONBOARDING.web_or_app_app_store")]), t.renderAppstoreUrl()]), n("div", {class: "field-container"}, [n("span", [t.$t("ONBOARDING.web_or_app_play_store")]), t.renderPlaystoreUrl(), t.playstoreFailed && r && !Object(m["N"])(t._value.playstore) && n("span", o()([{class: "playstore-failed"}, {domProps: {innerHTML: t.$t("COMMON.appstore_search_failed")}}, {
                                        on: {
                                            click: function (e) {
                                                Object(m["bb"])("a", e) && t.updateValue("playstoreMode", "url")
                                            }
                                        }
                                    }]))]), n("div", {class: "field-container"}, [n("span", [t.$t("ONBOARDING.web_or_app_developer_site")]), n(u["a"], o()([{}, D, {
                                        attrs: {
                                            value: e.path(["_value", "developerSiteUrl"], t),
                                            placeholder: "https://website.com/"
                                        }, on: {input: t.updateValue("developerSiteUrl").bind(t)}
                                    }]))])])
                            }
                        }()
                    }, renderBox: function () {
                        var t = this, a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "web",
                            n = this.$createElement, o = ["onboarding-basic-box"],
                            i = e.pathEq(["value", "type"], a, this);
                        return i && o.push("selected"), n(_["a"], {
                            class: o,
                            attrs: {selected: i},
                            on: {
                                click: function () {
                                    return t.updateValue("type", a)
                                }
                            }
                        }, [n("span", [function () {
                            switch (a) {
                                case"web":
                                    return t.$t("ONBOARDING.web_or_app_first_box_title");
                                case"app":
                                    return t.$t("ONBOARDING.web_or_app_second_box_title");
                                case"form":
                                    return t.$t("ONBOARDING.web_or_app_third_box_title")
                            }
                        }()])])
                    }, updateValue: e.curry(function (t, a) {
                        this._value = e.assoc(t, a, this._value)
                    }), observeSearch$: function () {
                        var t = this, a = Object(h["a"])(document, "click"),
                            n = this.search$.pipe(Object(y["a"])(e.prop("term")), Object(w["a"])(200), Object(k["a"])(function (e) {
                                var n = "appstore" === e.store ? "isAppstoreLoading" : "isPlaystoreLoading";
                                return t[n] = !0, t.resetResults(), Object(b["a"])(function () {
                                    return L["p"].searchApp(e)
                                }).pipe(Object(x["a"])(function (t) {
                                    return Object(c["a"])({}, e, {results: t})
                                }), Object(O["a"])(Object(m["p"])()), Object(A["a"])(function () {
                                    return Object(v["b"])().pipe(Object(C["a"])(function () {
                                        "playstore" === e.store && (t.playstoreFailed = !0)
                                    }))
                                }), Object(S["a"])(a), Object(C["a"])(function () {
                                    return t[n] = !1
                                }))
                            }), Object(E["a"])(function (e) {
                                var a = e.results, n = void 0 === a ? [] : a;
                                return t.resultsLength = n.length
                            }), Object(T["a"])(), Object(P["a"])(e.propEq("store", "appstore"))),
                            o = Object(r["a"])(n, 2), i = o[0], s = o[1];
                        this.$subscribeTo(i, function (e) {
                            var a = e.results;
                            return t.appleResults = a
                        }), this.$subscribeTo(s, function (e) {
                            var a = e.results;
                            return t.googleResults = a
                        }), this.$subscribeTo(a, this.resetResults)
                    }, resetResults: function () {
                        this.appleResults = [], this.googleResults = [], this.dropdownIdx = -1, this.resultsLength = 0, this._selectedValue = {}
                    }, addValue: function (t, a) {
                        var n = this;
                        if (Object(m["a"])(t, a)) {
                            var o, r = "developerSiteUrl", s = e.path(["_value", r], this);
                            switch (t) {
                                case"appstore":
                                    this.appleSearch = "", o = ["appstore", "appStoreUrl"];
                                    break;
                                case"playstore":
                                    this.googleSearch = "", o = ["playstore", "playStoreUrl"];
                                    break
                            }
                            var l = Object(c["a"])({}, this._value, Object(i["a"])({}, r, s || a[r]), e.applySpec(e.zipObj(o, [e.identity, e.prop("url")]))(a));
                            this._value = l, this.resetResults(), this._value[r] || L["p"].completeMobile({webOrApp: l}).then(function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                m["N"] && (n._value = e.mergeWith(function (e, t) {
                                    return e || t
                                }, n._value, t))
                            })
                        }
                    }, renderItem: function () {
                        var e = this, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                            a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "appstore",
                            n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : -999,
                            o = this.$createElement, i = n === this.dropdownIdx, r = ["web-app-auto-complete__item"];
                        i && r.push("selected");
                        var s = o("span", {
                            class: r, on: {
                                click: function () {
                                    return n > -1 && e.addValue(a, t)
                                }
                            }
                        }, [o(p["a"], {
                            attrs: {
                                src: t.logo,
                                fallback: "/img/icons/".concat(a, "_logo.png")
                            }
                        }), o("div", {class: "web-app-auto-complete__contents"}, [o("span", {class: "web-app-auto-complete__name"}, [t.name]), o("span", {class: "web-app-auto-complete__info"}, [o("span", {
                            class: "web-app-auto-complete__id",
                            on: {
                                click: function (n) {
                                    n.stopPropagation(), e.openWindow(t.url, function () {
                                        return e.addValue(a, t)
                                    })
                                }
                            }
                        }, [t.id]), o("span", {class: "web-app-auto-complete__corp"}, ["혻-혻", t.developer])])])]);
                        return i && (this._selectedValue = {dom: s, value: t, type: a}), s
                    }, renderAutoComplete: function () {
                        var e = this, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "appstore",
                            a = arguments.length > 1 ? arguments[1] : void 0, n = this.$createElement;
                        return Object(m["N"])(a) ? n("div", {
                            class: "web-app-auto-complete",
                            ref: "dropdown-container"
                        }, [a.map(function (a, n) {
                            return e.renderItem(a, t, n)
                        })]) : null
                    }, renderAutoInput: function (e) {
                        var t = this, a = e.store, n = e.value, i = e.inputFunc, r = e.placeholder,
                            s = e.autoCompleteData, c = e.error, l = this.$createElement,
                            d = "appstore" === a ? this.isAppstoreLoading : this.isPlaystoreLoading;
                        return l(u["a"], o()([{}, D, {
                            attrs: {value: n, placeholder: r, loading: d, error: c},
                            class: "auto-complete",
                            on: {input: i}
                        }]), [l("i", {
                            class: "far fa-search search-icon", slot: "icon", on: {
                                click: function () {
                                    t.search$.next({store: a, term: n})
                                }
                            }
                        }), l("transition", {
                            attrs: {"enter-active-class": "fadeIn", "leave-active-class": "fadeOut"},
                            slot: "last"
                        }, [this.renderAutoComplete(a, s)])])
                    }, renderAppstoreUrl: function () {
                        var t = this, a = this.$createElement, n = e.path(["value", "appstore"], this);
                        if (n) return a("div", {class: "app-object-box"}, [this.renderItem(n), a("i", {
                            class: "far fa-times-circle",
                            on: {
                                click: function () {
                                    return t._value = Object(c["a"])({}, t.value, {appstore: void 0, appStoreUrl: ""})
                                }
                            }
                        })]);
                        var o = this._value.appStoreUrl,
                            i = this.dirty && (o || this.noAppUrls) && !Object(m["L"])(o) ? this.$t("COMMON.invalid_app_store_url") : "";
                        return this.renderAutoInput({
                            store: "appstore",
                            value: this.appleSearch,
                            inputFunc: function (e) {
                                t.appleSearch = e, Object(m["I"])(e) && t.updateValue("appStoreUrl", e), t.search$.next({
                                    store: "appstore",
                                    term: e
                                })
                            },
                            placeholder: this.$t("COMMON.appstore_search_placeholder"),
                            autoCompleteData: this.appleResults,
                            error: i
                        })
                    }, switchPlaystoreMode: function () {
                        this.updateValue("playstoreMode", "search" === this.playstoreMode ? "url" : "search")
                    }, renderPlaystoreUrl: function () {
                        var e = this, t = this.$createElement, a = this.value, n = this.dirty, i = this.noAppUrls,
                            r = a.playstoreMode, s = a.playStoreUrl, l = a.playstore,
                            d = n && (s || i) && !Object(m["L"])(s) ? this.$t("COMMON.invalid_play_store_url") : "";
                        return "search" === r && Object(m["N"])(l) ? t("div", {class: "app-object-box"}, [this.renderItem(l, "playstore"), t("i", {
                            class: "far fa-times-circle",
                            on: {
                                click: function () {
                                    return e._value = Object(c["a"])({}, e.value, {playstore: void 0, playStoreUrl: ""})
                                }
                            }
                        })]) : "search" !== r || Object(m["N"])(l) ? t(u["a"], o()([{}, D, {
                            attrs: {
                                value: s,
                                placeholder: "https://play.google.com/",
                                error: d
                            }, on: {
                                input: function (t) {
                                    e._value = Object(c["a"])({}, e._value, {playStoreUrl: t, playstore: void 0})
                                }
                            }
                        }])) : this.renderAutoInput({
                            store: "playstore",
                            value: this.googleSearch,
                            inputFunc: function (t) {
                                e.googleSearch = t, Object(m["L"])(t) && e.updateValue("playStoreUrl", t), e.search$.next({
                                    store: "playstore",
                                    term: t
                                })
                            },
                            placeholder: this.$t("COMMON.playstore_search_placeholder"),
                            autoCompleteData: this.googleResults,
                            error: d
                        })
                    }, openWindow: function (t) {
                        var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : e.identity;
                        if (this.dropdownActive) return a();
                        window.open(t, "_blank")
                    }, moveSelection: function (t) {
                        var a = this, n = this.dropdownIdx + t;
                        n > this.resultsLength - 1 ? n = 0 : n < 0 && (n = this.resultsLength - 1), this.dropdownIdx = n, this.$nextTick(function () {
                            var t = e.path(["selectedValue", "dom", "elm"], a);
                            if (t) {
                                var n = a.getContainer(), o = n.scrollTop, i = o + Object(m["A"])(n), r = t.offsetTop;
                                (r < o || r > i) && j.scrollTo(t, 200, N)
                            }
                        })
                    }, observeArrow$: function () {
                        var t = this,
                            a = Object(h["a"])(document, "keydown").pipe(Object(I["a"])("keyCode"), Object(y["a"])(function () {
                                return t.dropdownActive
                            }), Object(y["a"])(e.contains(e.__, M)));
                        this.$subscribeTo(a, function (a) {
                            switch (Object(m["c"])(), a) {
                                case f["q"].enter:
                                    var n = e.props(["type", "value"], t._selectedValue);
                                    t.addValue.apply(t, Object(s["a"])(n));
                                    break;
                                case f["q"].up:
                                    t.moveSelection(-1);
                                    break;
                                case f["q"].down:
                                    t.moveSelection(1);
                                    break;
                                default:
                                    break
                            }
                        })
                    }, getContainer: function () {
                        return e.path(["$refs", "dropdown-container"], this)
                    }
                }
            }
        }).call(this, a("b17e"))
    }, "97e4": function (e, t) {
        e.exports = {
            en: {
                ad_config_all_description: "Description of the ad",
                ad_config_all_logo: "Logo Image",
                ad_config_all_main_title: "Main title of the ad",
                ad_config_all_media: "Main Image / Video",
                ad_config_all_short_title: "Short version of the title",
                ad_config_all_url: "Landing URL",
                ad_config_app_url: "Mobile",
                ad_config_appstore_url: "App store",
                ad_config_business_name: "Business Name",
                ad_config_carousel_url_common: "Landing URL (Common)",
                ad_config_facebook_carousel_description: "Description",
                ad_config_facebook_carousel_description_common: "Main Text (Common)",
                ad_config_facebook_description: "Main Text",
                ad_config_facebook_headline: "Headline",
                ad_config_facebook_link_description: "Link Description",
                ad_config_form_url: "Leads generation form",
                ad_config_google_display_description: "Description",
                ad_config_google_display_image: "Main Image",
                ad_config_google_display_title: "Title",
                ad_config_google_display_title_long: "Long Title",
                ad_config_google_text_description: "Description 1",
                ad_config_google_text_description_2: "Description 2",
                ad_config_google_text_title_1: "Title 1",
                ad_config_google_text_title_2: "Title 2",
                ad_config_google_text_title_3: "Title 3",
                ad_config_optional: "Optional",
                ad_config_playstore_url: "Play store",
                ad_config_required: "Required",
                ad_config_url_choose: "Choose and type the URL you want to advertise.",
                ad_config_web_url: "Web",
                ad_config_web_url_for_app: "Website URL",
                ad_edit_image_recommendations: "Image Recommendations",
                ad_edit_recommendations_file_format_title: "- File format:",
                ad_edit_recommendations_max_length_content: "Max {value} min",
                ad_edit_recommendations_max_length_title: "- Length:",
                ad_edit_recommendations_min_size_content: "{width} * {height}px",
                ad_edit_recommendations_min_size_title: "- Minimum size:",
                ad_edit_recommendations_recommended_length_content: "Up to {value} seconds",
                ad_edit_recommendations_recommended_length_title: "- Recommended length:",
                ad_edit_recommendations_recommended_video_ratio_content: "Vertical ({h}:{v})",
                ad_edit_recommendations_recommended_video_ratio_title: "- Aspect Ratio:",
                ad_edit_recommendations_size_content: "{width} * {height}px",
                ad_edit_recommendations_size_title: "- Recommended size:",
                ad_edit_recommendations_sound_content: "Enabled with captions included",
                ad_edit_recommendations_sound_title: "- Sound:",
                ad_edit_recommendations_video_format_title: "- File type:",
                ad_edit_recommendations_video_resolution_content: "{value}px minimum width",
                ad_edit_recommendations_video_resolution_title: "- Resolusion:",
                ad_edit_recommendations_video_size_content: "Up to {value}GB max",
                ad_edit_recommendations_video_size_title: "- File size:",
                ad_edit_video_recommendations: "Video Recommendations",
                ad_status_disabled: "This ad will not go live",
                ad_status_enabled: "This ad will go live",
                add_adriel_coupon: "Adriel Ad Coupon",
                add_instagram_page: "Add Instagram Page",
                add_keywords_modal_placeholder: "Search Keywords",
                add_keywords_modal_title: "Search Keywords",
                add_location_modal_placeholder: "Add more locations",
                add_location_modal_title: "Add more locations",
                add_new_ad: "New Ad",
                add_slide_indexed: "Add Slide {index}",
                add_slides: "Add slides",
                ads_business_name_missing: "Please enter business name.",
                ads_description_missing: "Please enter ad copies.",
                ads_image_missing: "Please upload an image",
                ads_invalid_min_dimension: "Video width must be at least 500px",
                ads_invalid_url: "Please enter a valid landing URL (where you send visitors to from your ads)",
                ads_invalid_video_length: "Video duration should be less than {value} seconds",
                ads_invalid_video_ratio: "video ratio should be 1:1",
                ads_media_missing: "An image or a video is required to run ads on Facebook, Instagram or Google Display.",
                ads_should_be_less: "{paramName} must contain less than {max} characters.",
                ads_title_missing: "Please enter ad title(s).",
                ads_upload_fail_dimension: "The image dimensions are too small.",
                ads_upload_fail_size: "The file size is too large.",
                after_copy_cancel: "Close",
                after_copy_confirm: "Go to check",
                ap_card_content_new: "New",
                ap_card_content_returning: "Returning",
                ap_card_customer_list_desc: "Following list(s) will be included in the audience.",
                ap_card_device_desktop: "Desktop",
                ap_card_device_mobile: "Mobile",
                ap_card_edit_customer_list: "Add a list",
                ap_card_edit_interests: "Add interests",
                ap_card_edit_keywords: "Add search keywords",
                ap_card_edit_locations: "Add more location",
                ap_card_edit_objectives: "Choose objectives",
                ap_card_edit_reset: "Retrieve Adriel's initial proposal",
                ap_card_edit_visitor: "Edit website tracker",
                ap_card_income_high: "high",
                ap_card_income_low: "low",
                ap_card_income_medium: "medium",
                ap_card_income_top: "top",
                ap_card_interests_info: "Tell us what your audience is into, such as hobbies and interests.",
                ap_card_let_adriel: "Let Adriel choose",
                ap_card_location_info: "We will show your ads to people who live in the following areas.",
                ap_card_location_info_empty: "There's no location information provided.",
                ap_card_login_google_tracker: "share google analytics",
                ap_card_login_pixel_tracker: "Share Pixel",
                ap_card_no_customer_list: "There's no customer list provided.",
                ap_card_no_interests: "We will show your ads to people with certain interests and hobbies.",
                ap_card_no_objectives: "Adding an objective is usually effective with a daily budget of more than {currency}40.",
                ap_card_no_objectives_unconnected: " Please share your Facebook Pixel or Google Analytics in order to use this feature.",
                ap_card_no_objectives_unconnected_GA: " Please share your Google Analytics in order to use this feature.",
                ap_card_no_objectives_unconnected_FB: " Please share your Facebook Pixel in order to use this feature.",
                ap_card_no_pixels: "In order to choose objectives, please connect GA or share Pixel with us.",
                ap_card_no_search_keywords: "We will show your ads to people who search for things related to certain keywords.",
                ap_card_no_tracker: "We will also show your ads to people who visited your website and optimize towards a selected conversion.",
                ap_card_objectives_info: "Choose objectives",
                ap_card_relation_married: "married",
                ap_card_relation_relationship: "relationship",
                ap_card_relation_single: "single",
                ap_card_search_keywords_info: "We will include people who search for things related to the keywords that you provide here.",
                ap_card_select_objectives: "Select an objective or more",
                ap_card_title_age: "Age",
                ap_card_title_customers: "List of Past & Current Customers",
                ap_card_title_customers_tooltip: "Emails are good enough. Telephone data cannot be used.",
                ap_card_title_device: "Target Device",
                ap_card_title_gender: "Gender",
                ap_card_title_income: "Income",
                ap_card_title_interests: "Audience Interests",
                ap_card_title_location: "Location",
                ap_card_title_new_returning: "New & Returning Customers",
                ap_card_title_objectives: "Objectives",
                ap_card_title_objectives_tooltip: "This is not required, but nice to have for the best possible results. Drop us a message in the chat if you�셱e stuck.",
                ap_card_title_relation: "Relationship Status",
                ap_card_title_search_keywords: "Search Keywords",
                ap_card_title_tracker: "Website Tracker",
                ap_card_title_tracker_tooltip: "This is not required, but nice to have for the best possible results. Drop us a message in the chat if you�셱e stuck.",
                ap_card_tracker_fb_app_info: "Facebook App Permission",
                ap_card_tracker_google_content: "If your website is also using Google Analytics, please click below button to log in to your GA.",
                ap_card_tracker_info: "Google Analytics account",
                ap_card_tracker_pixel_content: "If your website is also using Pixel, please click below button to share it.",
                ap_card_tracker_pixel_info: "Facebook Pixel Permission",
                ap_summary_close: "Close details",
                ap_summary_seemore: "See more details",
                ap_summary_title: "We will show your ads to people who are likely to become your customers.",
                behaviors: "Behaviors",
                below_minimum_budget: "The amount you've put is less than the minimum required budget.",
                billing_faq: "Billing FAQ",
                billing_faq_content_1: "You will only pay when someone clicks your ad. We�셪l charge your card automatically as your ad receives clicks or impressions.<br/> Upon launching a campaign, we hold a security deposit amounting to your campaign�셲 daily budget. This step is necessary for us to ensure that the billing credit card information is valid. As soon as the card is deemed valid, the charge is immediately cancelled.",
                billing_faq_content_2: 'You�셪l be automatically charged for the first ten payments when your advertising costs reach the $50 threshold.<br/> Advertisers with a reliable payment history are charged once every three days. If your daily budget is more than $ 100, your payment will be made once a day.<br/> You can refer to <a href="https://intercom.help/adriel/en/articles/2624679-payment-scheme" target="_blank">this link</a> for more details.',
                billing_faq_content_3: "Yes, once your ads are active and running, you can stop them anytime you want. You will only pay for ads that have actually ran. ",
                billing_faq_content_4: "Coupon codes are applied to your ad account as a credit and are deducted from your balance due. If your campaign spend exceeds the amount of your coupon code, your credit card will be charged. To ensure your campaign does not spend over the coupon code amount, be sure to set a total budget for the same amount as your coupon code. ",
                billing_faq_content_5: 'Your ads may be shown more often on days when traffic is higher. The total daily cost might go up to 200% more than your daily budget for <a target="_blank" href="https://support.google.com/google-ads/answer/2375423?hl=en">Google</a> and 25% more for <a target="_blank" href="https://www.facebook.com/business/help/190490051321426">Facebook</a> on high traffic days.',
                billing_faq_content_6: "If the daily budget is updated to a smaller amount than the original budget, the update will be applied the next day.",
                billing_faq_title_1: "When will I first be billed?",
                billing_faq_title_2: "How often will I be billed?",
                billing_faq_title_3: "Can I stop my ads at any moment?",
                billing_faq_title_4: "How does a coupon work?",
                billing_faq_title_5: "Why costs might exceed your average daily budget.",
                billing_faq_title_6: "What happens when I change my daily budget in the middle of the day?",
                cancel_copy: "Cancel",
                change_fb_page: "Change Facebook page",
                confirm_appstore_connect: "You haven't finished connecting the app with us. Do you wish to exit?",
                confirm_appstore_connect_exit: "Yes, exit.",
                confirm_appstore_connect_stay: "No, stay.",
                confirm_pixel_confirm: "No, wait.",
                confirm_pixel_sharing: "You haven't finished sharing your Pixel with us. Are you sure you want to leave this page without sharing your Pixel?",
                confirm_remove_ad: "Are you sure you want to delete this Ad?",
                confirm_remove_ad_group: "Are you sure you want to delete Ad {index}?",
                confirm_unpublished: "Do you want to save the changes you made and update the campaign?",
                confirm_unpublished_btn_cancel: "No, don't save the changes",
                confirm_unpublished_btn_confirm: "Yes, save and update",
                connect_app_store: "Connect your app from the Apple App Store",
                connect_appstore_step_0: 'First, make sure you are the admin of your app. Afterwards, log in at the following link: <a href="https://appstoreconnect.apple.com/access/users" target="_blank">https://appstoreconnect.apple.com/access/users</a> and click on the + button to add a new user, Adriel.',
                connect_appstore_step_1: "Fill up the new user details with information as below and click 'Invite'.",
                connect_appstore_steps: [{
                    image: "/img/screenshots/en_appstore_connect_1.jpg",
                    title: "PROPOSAL.connect_appstore_step_0"
                }, {
                    image: "/img/screenshots/en_appstore_connect_2.jpg",
                    title: "PROPOSAL.connect_appstore_step_1"
                }],
                content_copied: "Content copied",
                conversions: "Conversions",
                conversions_modal_add_url: "Add a custom URL",
                conversions_modal_title: "Select an objective or more",
                copied_facebook: "Copied to Facebook.",
                copied_instagram: "Copied to Instagram.",
                copy_ad: "Copy ad",
                copy_carousel_facebook: "Copy this ad to Facebook",
                copy_carousel_instagram: "Copy this ad to Instagram",
                create_by_onboarding_tooltip: "/img/common/create_by_onboarding_tooltip_en.png",
                crop_modal_title: "Crop Image",
                crop_tooltip: "Crop image",
                echo: "{content}",
                edit_all: "Edit all",
                edit_all_index: "Edit ad {index}",
                edit_this_ad: "Edit this ad",
                enter_adriel_coupon: "Enter coupon code",
                estimation_per_day: "{value} per day",
                express: "Please do it for me!",
                failed_to_register_coupon: "The coupon is already used",
                family_statuses: "Family status",
                fb_permissions_missing: "Not enough permission granted",
                fb_reason_no_image: "No profile picture on the page",
                fb_reason_no_permission: "Not enough permission on the page",
                fb_reason_no_published: "The page is not published",
                fb_reason_token_permission: "business_management permission is required. Please contact the support team for more details",
                finish_copy: "Finish copying",
                go_to_schedule: "Go to schedule settings",
                indicator_label_ad: "Ads",
                indicator_label_ap: "Who will see your ads?",
                indicator_label_plan: "Schedule & Budget",
                indicator_label_form: "Form builder",
                indicator_nav_label_ad: "Ads",
                indicator_nav_label_ap: "Audience",
                indicator_nav_label_plan: "Schedule\n& Budget",
                indicator_nav_label_form: "Form\nbuilder",
                interests: "Interests",
                launch_campaign: "Launch This Campaign",
                launch_check_payment: "Please add your payment method to launch this campaign.",
                navigation_finish: "Finish",
                navigation_publish: "Publish",
                navigation_reset: "Reset",
                need_extend_schedule: "Please extend your end date",
                new_returning_new_tooltip: "If you have Google Analytics or Pixel, new people similar to your past and current customers will be included. This list is created based on Google Analytics or Pixel.",
                new_returning_returning_tooltip: "Your past and current customers based on website visitors or list of past & current customers. Not available if there is no Pixel, Google Analytics or list of past & current customers.",
                next: "Next",
                next_step: "Next Step",
                no_ads_created: "Click on the icon above to start creating your ad.",
                no_ads_warn: "Please switch on at least 1 ad box at the 1st stage.",
                no_ads_warn_ad: "Please switch on at least 1 ad box.",
                no_ads_warn_form: "Please switch on at least 1 ad box at the 2nd stage.",
                no_fb_admin_pages: "You don't have any Facebook page that you manage as Page Admin. Please check your account level on Facebook. <a href='https://www.facebook.com/help/510247025775149?helpref=faq_content' target='_blank'>Page Role Settings</a>",
                no_pixel_detected: "We haven't detected any Pixel associated with your Facebook account.",
                no_preview_mp4: "No preview is available for\n video in mp4 format",
                not_enough_information: "We don't have enough information to launch this campaign.",
                overspent_tooltip: "Your ads may be shown more often on days when traffic is higher. The total daily cost might go up to 200% more than your daily budget for Google and 25% more for Facebook on high traffic days.",
                past_end_date: "The end date of your campaign is in the past",
                paste: "Paste",
                pixel_how_to: [{
                    image: "/img/proposal/pixel_screenshot1.png",
                    title: "PROPOSAL.pixel_how_to_title_0"
                }, {
                    image: "/img/proposal/pixel_screenshot2.png",
                    title: "PROPOSAL.pixel_how_to_title_1"
                }, {
                    image: "/img/proposal/pixel_screenshot3.png",
                    title: "PROPOSAL.pixel_how_to_title_2"
                }],
                pixel_how_to_title_0: 'Click this link <a href="{link}" target="_blank">{link}</a> (You must be logged in Facebook) to open the Facebook webpage where you can share your Pixel. Select the Pixel you\'d like to share and click the "Assign Partner" button.',
                pixel_how_to_title_1: 'On the pop-up page, select <b>"Pixel Editor"</b> as the Role. This is important for Adriel in order to find a better target audience for you, based on the information from your Pixel.',
                pixel_how_to_title_2: 'Enter <span class="copy-target"><b>525463574501100</b><span class="copy">COPY</span></span> (Adriel\'s Facebook Business ID) in Business ID. Click "Confirm" to finish that page. Click the "Done" button below and you\'re all set.',
                plan_budget_daily: "Daily",
                plan_budget_day: "day",
                plan_budget_minimum: "Budget Minimum",
                plan_budget_payment_method: "Payment Method",
                plan_budget_total: "Total",
                plan_estimated_clicks: "Ad clicks",
                plan_estimated_not_available: "Results are not available",
                plan_estimated_warning: "The numbers are provided to give you an idea of the performance for your budget. These are only estimates and do not guarantee results.",
                plan_go_to_ads: "Go to Ads",
                plan_optimization_info: "Adriel will optimize the campaign strategy in order to maximize your return.",
                plan_reason_apple_not_connected: "Apple app is not connected",
                plan_reason_facebook_page_missing: "Your facebook page is missing",
                plan_reason_no_ads_enabled: "You didn't activate any ad",
                plan_reason_not_enabled: "You didn't activate any {channel} ad",
                plan_schedule_asap: "ASAP",
                plan_schedule_continuously: "Run my campaign continuously, starting today",
                plan_schedule_end: "End",
                plan_schedule_set_start: "Set a start date",
                plan_schedule_spend_head: "You'll spend no more than",
                plan_schedule_spend_tail: "per day.",
                plan_schedule_start: "Start",
                plan_schedule_start_end: "Set a start and end date",
                plan_schedule_start_tooltip: "The actual start date and time may be different due to the review process.",
                plan_title_budget: "Budget",
                plan_title_optimization: "Auto-Optimization by Adriel",
                plan_title_results: "Estimated Daily Results",
                plan_title_schedule: "Schedule",
                preview_copy_ad: "Copy ad content",
                price_credit: "Credit",
                price_day: "Day",
                price_platform_fee: "Ad platform fee",
                price_platform_fee_tooltip: "This is what you pay to ad platforms such as Facebook or Google.",
                price_service_fee: "Service fee",
                price_service_fee_tooltip: "This helps us run our platform and offer services like ad performance management and technical support.",
                price_to_pay: "Price you pay",
                price_total: "Total",
                price_total_tooltip: "You won't be charged yet. We will only hold a small deposit to ensure that your credit card has sufficient funds. You will only be charged the exact price when people click your ads to visit your website.",
                published_toast: "Published",
                remove_image_tooltip: "Remove image/Video",
                run_continuously: "Run the campaign continuously",
                schedule_date_format: "D MMM YYYY",
                schedule_date_month_names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                schedule_day_names: ["Su", "M", "Tu", "W", "Th", "F", "S"],
                select_date: "Click to select...",
                select_fb_page: "Select Facebook page",
                select_fb_page_desc: "To run Facebook ads, Adriel needs information about your business Facebook page. Please log in to Facebook by clicking the button below.",
                select_fb_page_ment: "Please select the page that you want to use",
                select_fb_pixel_ment: "Please select the pixel that you want to share",
                service_preview_image_0: "/img/proposal/en_budget_service_screen_0.png",
                service_preview_image_1: "/img/proposal/en_budget_service_screen_1.png",
                service_preview_text: "What�셲 included in Adriel�셲 services?",
                slide_delete_tooltip: "Delete slide",
                slide_select_tooltip: "Select slilde",
                slide_upload_tooltip: "Upload image/video",
                terms_services_1: 'By clicking "Launch This Campaign",',
                terms_services_2: "you agree to our",
                terms_services_3: "and",
                terms_services_4: "",
                terms_services_policy: "Privacy Policy",
                terms_services_terms: "Terms of Service",
                tracker_modal_no_dec: "<span>Your current objective is <b>Link Click</b>.<br/>If you want to access more objectives,<br/>connect to your {}.</span>",
                tracker_modal_no_dec_GA: "<span>Your current objective is <b>Link Click</b>.<br/>If you want to access more objectives,<br/>connect to your Google Analytics.</span>",
                tracker_modal_no_fb: "We haven't detected any Pixel associated with your Facebook account.",
                tracker_modal_no_fb_help: "Learn how to connect to Facebook Pixel.",
                tracker_modal_no_ga: "There is no Google Analytics linked to your Google Account.",
                tracker_modal_no_ga_help: "Learn how to install and set up Google Analytics correctly.",
                tracker_modal_title: "Select Google Analytics account",
                unable_to_retarget_toast: "We cannot target returning customers because we do not have data of your past and current customers. Please provide a customer list, Google Analytics or Pixel.",
                undo_paste: "Undo"
            },
            jp: {
                ad_config_all_description: "佯껃몜��㈂榮�",
                ad_config_all_logo: "��궡�삣깗",
                ad_config_all_main_title: "佯껃몜��깳�ㅳ꺍�욍궎�덀꺂",
                ad_config_all_media: "�▲궎�녕뵽�� / �뺟뵽",
                ad_config_all_short_title: "�욍궎�덀꺂 (Short ver)",
                ad_config_all_url: "�⒲꺍�뉎궍�녈궛URL",
                ad_config_appstore_url: "App store",
                ad_config_business_name: "�볝궦�띲궧��",
                ad_config_carousel_url_common: "�⒲꺍�뉎궍�녈궛URL (�깁��)",
                ad_config_facebook_carousel_description: "屋녕눗",
                ad_config_facebook_carousel_description_common: "�녴궘�밤깉�▲긿�삠꺖�� (�깁��)",
                ad_config_facebook_description: "�녴궘�밤깉�▲긿�삠꺖��",
                ad_config_facebook_headline: "誤뗥눣��",
                ad_config_facebook_link_description: "�ゃ꺍��㈂榮�",
                ad_config_google_display_description: "屋녕눗",
                ad_config_google_display_image: "�▲궎�녕뵽��",
                ad_config_google_display_title: "�욍궎�덀꺂",
                ad_config_google_display_title_long: "�욍궎�덀꺂 (Long ver)",
                ad_config_google_text_description: "屋녕눗1",
                ad_config_google_text_description_2: "屋녕눗 2",
                ad_config_google_text_title_1: "�욍궎�덀꺂 1",
                ad_config_google_text_title_2: "�욍궎�덀꺂 2",
                ad_config_google_text_title_3: "�욍궎�덀꺂 3",
                ad_config_optional: "�ゃ깤�룔깾��",
                ad_config_playstore_url: "Play store",
                ad_config_required: "恙낂젅",
                ad_edit_image_recommendations: "�삣깗�ゃ궠�▲꺍�뉎꺖�룔깾��",
                ad_edit_recommendations_file_format_title: "- �뺛궊�ㅳ꺂�뺛궔�쇈깯�껁깉:",
                ad_edit_recommendations_max_length_content: "��鸚� {value} ��",
                ad_edit_recommendations_max_length_title: "- �룔걬:",
                ad_edit_recommendations_min_size_content: "{width} * {height}px",
                ad_edit_recommendations_min_size_title: "- ��弱뤵궢�ㅳ궨:",
                ad_edit_recommendations_recommended_length_content: "{value} 燁�",
                ad_edit_recommendations_recommended_length_title: "- �룔걬:",
                ad_edit_recommendations_recommended_video_ratio_content: "�귞쎍 ({h}:{v})",
                ad_edit_recommendations_recommended_video_ratio_title: "- 雅덃꺍驪붺럤:",
                ad_edit_recommendations_size_content: "{width} * {height}px",
                ad_edit_recommendations_size_title: "- �듽걲�쇻굙��궢�ㅳ궨:",
                ad_edit_recommendations_sound_content: "Enabled with captions included",
                ad_edit_recommendations_sound_title: "- �듐궑�녈깋:",
                ad_edit_recommendations_video_format_title: "- �뺛궊�ㅳ꺂�욍궎��:",
                ad_edit_recommendations_video_resolution_content: "{value}px minimum width",
                ad_edit_recommendations_video_resolution_title: "- �삭나:",
                ad_edit_recommendations_video_size_content: "Up to {value}GB max",
                ad_edit_recommendations_video_size_title: "- �뺛궊�ㅳ꺂�듐궎��:",
                ad_edit_video_recommendations: "�뺟뵽�с궠�▲꺍�뉎꺖�룔깾��",
                ad_status_disabled: "�볝겗佯껃몜��뀓岳▲겎�띲겲�쎼굯",
                ad_status_enabled: "�볝겗佯껃몜��뀓岳▲겎�띲겲��",
                add_adriel_coupon: "�㏂깋�ゃ궓�ャ궚�쇈깮��",
                add_instagram_page: "�ㅳ꺍�밤궭�겹꺀�졼깪�쇈궦瓦썲뒥",
                add_keywords_modal_placeholder: "濾쒐뇨��꺖��꺖�됥굮�ε뒟�쀣겍�뤵걽�뺛걚��",
                add_keywords_modal_title: "濾쒐뇨��꺖��꺖�됭옙��",
                add_location_modal_placeholder: "�욍궟�껁깉�겼윜�믦옙�졼걮�╉걦�졼걬�꾠��",
                add_location_modal_title: "�욍궟�껁깉�겼윜�믦옙�졼걮�╉걦�졼걬�꾠��",
                add_new_ad: "�겹걮�꾢틕��",
                add_slide_indexed: "�밤꺀�ㅳ깋�믦옙�졼걲�� {index}",
                add_slides: "�밤꺀�ㅳ깋�믦옙�졼걲��",
                ads_business_name_missing: "�볝궦�띲궧�띲굮�ε뒟�쀣겍�뤵걽�뺛걚��",
                ads_description_missing: "佯껃몜�녴궘�밤깉�믣뀯�쎼걮�╉걦�졼걬�꾠��",
                ads_image_missing: "佯껃몜�ㅳ깳�쇈궦�믡궋�껁깤��꺖�됥걮�╉걦�졼걬�꾠��",
                ads_invalid_min_dimension: "Instagram��깛�뉎궕�삯씊�듐궎�뷩겘弱묆겒�뤵겏��500�붵궚�삠꺂餓δ툓�㎯걲��",
                ads_invalid_url: "佯껃몜�믡궚�ゃ긿��걲�뗦셽�ャ궋��궩�밤걲�뗣궢�ㅳ깉��궋�됥꺃�밤굮閭ｃ걮�뤷뀯�쎼걮�╉걦�졼걬�꾠�� �밤꺀�ㅳ깋佯껃몜�믥뵟�먦걬�뚣걼�닷릦,�꾠궧�⒲궎�됥걫�ⓦ겓�듐궎�덀궋�됥꺃�밤걣�ε뒟�뺛굦�╉걚�뗣걢閻븃첀�쀣겍�뤵걽�뺛걚��",
                ads_invalid_video_length: "Instagram��깛�뉎궕��빓�뺛걣 {value}燁믤쑋繹��㎯겒�묆굦�겹겒�됥겒�꾠겎�쇻��",
                ads_invalid_video_ratio: "Instagram��깛�뉎궕��뵽�€캈�뉎걣恙끹걳1:1�㎯겒�묆굦�겹겒�듽겲�쎼굯��",
                ads_media_missing: "�ㅳ깳�쇈궦�얇걼��땿�삠궋�껁깤��꺖�됥걮�╉걦�졼걬�� Facebook, Instagram, Google �뉎궍�밤깤�с궎佯껃몜�믣츪�썬걲�뗣겓��,恙끹걳�ㅳ깳�쇈궦�얇걼��땿�삠걣恙낁쫨�㎯걲�� �ㅳ깳�쇈궦�ⓨ땿�삠걣�㏂긿�쀣꺆�쇈깋�뺛굦�ゃ걚�닷릦,餓뺞쭣�믦㈂�쀣걦閻븃첀�쀣겍�뤵걽�뺛걚��",
                ads_should_be_less: "{paramName}�� {max}�뉐춻�믦텈�덀굥�볝겏�뚣겎�띲겲�쎼굯��",
                ads_title_missing: "佯껃몜��궭�ㅳ깉�ャ굮�ε뒟�쀣겍�뤵걽�뺛걚��",
                ads_upload_fail_dimension: "�ㅳ깳�쇈궦�듐궎�뷩걣�ⓦ겍�귛컦�뺛걚�㎯걲�� ��弱뤵궢�ㅳ궨�믥▶沃띲걮�╉걢�됥궎�▲꺖�멥굮沃욘빐��,�얇걼�㏂긿�쀣꺆�쇈깋�쀣겍�뤵걽�뺛걚��",
                ads_upload_fail_size: "�뺛궊�ㅳ꺂若백뇧�뚧�鸚㎩�ㅳ굮擁낂걥�쀣겲�쀣걼�� �뺛궊�ㅳ꺂�듐궎�뷩굮�귙걝弱묆걮弱뤵걬�뤺た�담걮�잌풄,�귙걝訝�佯╉궋�껁깤��꺖�됥걮�╉걦�졼걬�꾠��",
                after_copy_cancel: "�됥걯��",
                after_copy_confirm: "�듾폏鼇덀겓�꿔�",
                ap_card_content_new: "�계쫸",
                ap_card_content_returning: "�℡춼",
                ap_card_customer_list_desc: "轝▲겗�ゃ궧�덀겗�듿�礪섅걣�욍꺖�꿔긿�덀궕�쇈깈�ｃ궓�녈궧�ヨ옙�졼걬�뚣겲�쇻��",
                ap_card_device_desktop: "�뉎궍�밤궚�덀긿��",
                ap_card_device_mobile: "�㏂깘�ㅳ꺂",
                ap_card_edit_customer_list: "�ゃ궧�덀굮瓦썲뒥�쇻굥",
                ap_card_edit_interests: "�덂뫑�ャ깇�담꺁�쇈굮瓦썲뒥�쇻굥",
                ap_card_edit_keywords: "濾쒐뇨��꺖��꺖�됥굮瓦썲뒥�쇻굥",
                ap_card_edit_locations: "��궞�쇈궥�㎯꺍�믦옙�졼걲��",
                ap_card_edit_objectives: "��쉪�믧겦�볝겎�뤵걽�뺛걚",
                ap_card_edit_reset: "�㏂깋�ゃ궓�ャ겗�앮쐿�먩죭�ユ댗��",
                ap_card_edit_visitor: "�╉궒�뽧궢�ㅳ깉�덀꺀�껁궖�쇈굮渶③썓�쇻굥",
                ap_card_income_high: "遙섉�孃쀥광",
                ap_card_income_low: "鵝롦�孃쀥광",
                ap_card_income_medium: "訝��孃쀥광",
                ap_card_income_top: "野뚩짆掠�",
                ap_card_interests_info: "�귙겒�잆겗�ャ궧�욍깯�쇈겗擁ｅ뫑�꾥늿�녈굮�쇻걟�╉걦�졼걬��",
                ap_card_let_adriel: "�㏂깋�ゃ궓�ャ겓�멥굯�㎯굚�됥걝",
                ap_card_location_info: "�멩뒢�뺛굦�잆궓�ゃ궋�ヤ퐦�볝겎�꾠굥�곥굚�쀣걦���瓦묆겲�㏛퐦�볝겎�꾠걼�╉꺖�뜰꺖�ュ틕�듽굮�띴에�쀣겲�쇻��",
                ap_card_location_info_empty: "�겼윜�끻젿�믣뀯�쎼걮�╉걦�졼걬�꾠��",
                ap_card_login_google_tracker: "Google�ャ꺆�겹궎�녈걲��",
                ap_card_login_pixel_tracker: "Pixel�믡궥�㎯궋�쇻굥",
                ap_card_no_customer_list: "�ャ궧�욍깯�쇈꺁�밤깉�뚣걗�듽겲�쎼굯",
                ap_card_no_interests: "�멩뒢�뺛굦�잒땃�녈꺕�덂뫑�ュ릦�녴깺�쇈궣�쇈겓佯껃몜�믧뀓岳▲걮�얇걲��",
                ap_card_no_objectives: "��쉪�뚪겦�욁걬�뚣겍�꾠겲�쎼굯�귙깉�⒲깢�ｃ긿��빊���鸚㎩뙑:<br />�뚣깈�뺛궔�ャ깉�㎬Þ若싥걬�뚣겲�쇻��",
                ap_card_no_pixels: "��쉪�믧겦�욁걲�뗣겓���갍oogle Analytics�얇걼�츿ixel�믡궋�됥꺁�ⓦ꺂�ユ렏泳싥걮�╉걦�졼걬�꾠��",
                ap_card_no_search_keywords: "�멩뒢�뺛굦�잆궘�쇈꺈�쇈깋�ラ뼟�ｃ걲�뗤틟�꾠굮濾쒐뇨�쀣겍�꾠굥�╉꺖�뜰꺖�ュ틕�듽굮�띴에�쀣겲�쇻��",
                ap_card_no_tracker: "�귙겒�잆겗�╉궒�뽧궢�ㅳ깉�ヨØ�뚣걼�볝겏��걗�뗣깺�쇈궣�쇈굮�욍꺖�꿔깇�ｃ꺍�겹걲�뗥졃�덀�곥궋�됥꺁�ⓦ꺂��걗�ゃ걼�췍oogle Analytics�ャ궋��궩�밤걲�뗥퓚誤곥걣�귙굤�얇걲�� Google Analytics�믡걫�⑴뵪��졃�덀겘訝뗣겗�쒌궭�녈굮��꺁�껁궚�쀣겍�뤵걽�뺛걚��",
                ap_card_objectives_info: "��쉪�믧겦��",
                ap_card_relation_married: "�℡찚",
                ap_card_relation_relationship: "雅ㅹ슋訝�",
                ap_card_relation_single: "�ц벴",
                ap_card_search_keywords_info: "�멩뒢�뺛굦�잆궘�쇈꺈�쇈깋�ラ뼟�ｃ걲�뗤틟�꾠굮濾쒐뇨�쀣겍�꾠굥�╉꺖�뜰꺖�ュ틕�듽굮�띴에�쀣겲�쇻��",
                ap_card_select_objectives: "��쉪�믧겦�욁걮�╉걦�졼걬�꾬펷筽뉑빊��펹",
                ap_card_title_age: "亮닻숱",
                ap_card_title_customers: "�롥렮�①뤎�ⓦ겗�ャ궧�욍깯�쇈꺁�밤깉",
                ap_card_title_customers_tooltip: "�▲꺖�ャ궋�됥꺃�밤겗�끻젿�㎩뛻�녴겎�쇻�귡쎔屋긺빁�룔깈�쇈궭��슴�ⓦ걲�뗣걪�ⓦ걣�㎯걤�얇걵�볝��",
                ap_card_title_device: "�욍꺖�꿔긿�덀깈�먦궎��",
                ap_card_title_gender: "�㎩닪",
                ap_card_title_income: "��孃�",
                ap_card_title_interests: "�ゃ꺖�뉎궍�ⓦ꺍�배늿�녈궖�녴궡�ゃ꺖",
                ap_card_title_location: "��궞�쇈궥�㎯꺍",
                ap_card_title_new_returning: "�계쫸 & �℡춼�ャ궧�욍깯��",
                ap_card_title_objectives: "��쉪",
                ap_card_title_objectives_tooltip: "�볝굦��퓚�싧퓚誤곥겒�볝겏�㎯겘�귙굤�얇걵�볝걣�곫��⒴뙑�ょ탳�쒌굮�뷩걲�뷩겓�귙굥�ⓧ씩�⒲겎�쇻�귙굚�쀥늽�뗣굢�ゃ걚�볝겏�뚣걗�뚣겙�곥깵�껁깉�ャ깳�껁궩�쇈궦�믡걮�╉걦�졼걬�꾠��",
                ap_card_title_relation: "雅ㅹ슋�뜻퀋",
                ap_card_title_search_keywords: "濾쒐뇨��꺖��꺖��",
                ap_card_title_tracker: "�╉궒�뽧궢�ㅳ깉�덀꺀�껁궖��",
                ap_card_title_tracker_tooltip: "�볝굦��퓚�싧퓚誤곥겒�볝겏�㎯겘�귙굤�얇걵�볝걣�곫��⒴뙑�ょ탳�쒌굮�뷩걲�뷩겓�귙굥�ⓧ씩�⒲겎�쇻�귙굚�쀥늽�뗣굢�ゃ걚�볝겏�뚣걗�뚣겙�곥깵�껁깉�ャ깳�껁궩�쇈궦�믡걮�╉걦�졼걬�꾠��",
                ap_card_tracker_fb_app_info: "Facebook�㏂깤�ゃ겗與⑶솏�욤첀",
                ap_card_tracker_google_content: "�귙겒�잆겗�╉궒�뽧궢�ㅳ깉�똆oogle Analytics�믢슴�ｃ겍�꾠굥�닷릦�곦툔��깭�욍꺍�믡궚�ゃ긿��걮�╉꺆�겹궎�녈걮�╉걦�졼걬�꾠��.",
                ap_card_tracker_info: "Google Analytics �㏂궖�╉꺍��",
                ap_card_tracker_pixel_content: "�귙겒�잆겗�╉궒�뽧궢�ㅳ깉�똏ixel�믢슴�ｃ겍�꾠굥�닷릦, 訝뗣겗�쒌궭�녈굮��꺁�껁궚�쀣겍�뚧쐿�쀣겍�뤵걽�뺛걚��",
                ap_card_tracker_pixel_info: "Facebook Pixel 鼇긷룾",
                ap_summary_close: "屋녕눗�믧뻾�섅굥",
                ap_summary_seemore: "屋녕눗�믦쫳��",
                ap_summary_title: "�ャ궧�욍깯�쇈겓�ゃ굥誤뗨씔�욍겗遙섅걚�╉꺖�뜰꺖�ュ틕�듽굮�띴에�쀣겲�쇻��",
                below_minimum_budget: "�귙겒�잆걣�γ굦�잋틛嶸쀣겘��鵝롣틛嶸쀩죲�믢툔�욁겂�╉걚�얇걲��",
                billing_faq: "獄뗦콆 FAQ",
                billing_faq_content_1: "沃겹걢�뚣걗�ゃ걼��틕�듽굮��꺁�껁궚�쀣걼�닷릦��겳��돂�꾤쑴�쇻걣�잆걯�얇걲�귛틕�듽걣��꺁�껁궚�뺛굦�잆굤�꾠궎�녈깤�с긿�룔깾�녈걣�븀뵟�쇻굥�②눎�뺛겎�삯뙯�뺛굦�잆궚�с궦�껁깉�ャ꺖�됥겓獄뗦콆�쀣겲�쇻��<br> ��깵�녈깪�쇈꺍��뀓岳↓뼀冶뗣겗�쎼겓���� ��깵�녈깪�쇈꺍��뿥雅덄츞�녴굮岳앲㉫�묆겏�쀣겍獄뗦콆�뺛걵��쟼�띲겲�쇻�귙걪��깤��궩�밤겘�삯뙯�뺛굦�잆궚�с궦�껁깉�ャ꺖�됥걣�됧듅�뗣겑�녴걢閻븃첀�쇻굥�뷩겗�귙겗�㎯걲�귙걪��쳦黎귙겘若잓슋��굚��겎��겒�뤵�� �겼뼳璵�뿥餓ε냵�ユ돂�꾣댗�쀣걬�뚣겲�쇻�귙궘�ｃ꺍�싥꺖�녈걣�띴에�뺛굦�╉걚�뗩솏�듽겘�곫칿24�귡뼋�붵겏�ャ궘�ｃ꺍�싥꺖�녈겗�δ틛嶸쀣굮岳앲㉫�묆겏�쀣겍獄뗦콆�뺛걵��쟼�띲겲�쇻��",
                billing_faq_content_2: "若잓슋��틕�딂꼇�ⓦ겏�앫굦�ヤ섦�녻꼇�ⓦ겘��깵�녈깪�쇈꺍�띴에孃뚣��3�뜻��γ겓獄뗦콆�뺛굦�얇걲�귙궘�ｃ꺍�싥꺖�녈걣72�귡뼋�덀굤�띲겓永귙굩�뗥졃�덀겘��깵�녈깪�쇈꺍�띴에永귚틙�귞궧�㎬쳦黎귙걬�뚣겲�쇻�귙궘�ｃ꺍�싥꺖�녜뀓岳▲걣3�γ굠�딃빓�꾢졃�덀겘�곫칿72�귡뼋�붵겏�ヨ쳦黎귙걬�뚣겲�쇻��",
                billing_faq_content_3: "�꾠걚�덀�귛틕�듾틛嶸쀣겏�띴에�잓뼋��눎�긱겓黎뷩굙�뗣걪�ⓦ걣�㎯걤�얇걲�귙걹�뚥빳鸚뽧겗�귙겗��걗�듽겲�쎼굯��",
                billing_faq_content_4: "��걚�귚�佯�틕�듽걣�띴에�뺛굦�╉굚�곥걚�ㅳ겎�귡뀓岳▲굮閭㏂굙�뗣걪�ⓦ걣�㎯걤�얇걲�귟꼇�ⓦ겘若잓슋�ラ뀓岳▲걬�뚣걼�녴걽�묋쳦黎귙걬�뚣겲�쇻��",
                billing_faq_content_5: "��꺖�앫꺍�녈꺖�됥겘�㏂궖�╉꺍�덁툓��츐遙섅겓�⑴뵪�뺛굦��폊�뗣굦�얇걲�귙궘�ｃ꺍�싥꺖�녈겗�띴에�뚣궚�쇈깮�녈궠�쇈깋��뇫窈띲굮擁끹걟�뗥졃�덀겘�곭쇉�꿔걬�뚣걼��꺃�멥긿�덀궖�쇈깋�ヨ쳦黎귙걬�뚣겲�쇻�귙궘�ｃ꺍�싥꺖�녜뀓岳▲걣��꺖�앫꺍�녈꺖�됥겗�묌죲�믦텈�덀겒�꾠굠�녴겓�쇻굥�잆굙�ャ겘�곩뀲鵝볞틛嶸쀣굮��꺖�앫꺍�녈꺖�됥겏�뚣걯�묌죲�ヨÞ若싥걮�╉걦�졼걬�꾠�� ",
                billing_faq_title_1: "���앫겗獄뗦콆��걚�ㅳ겎�쇻걢竊�",
                billing_faq_title_2: "�⒲굦�뤵굢�꾠겗�삣벧�㎬쳦黎귙걬�뚣겲�쇻걢竊�",
                billing_faq_title_3: "��鵝롣틛嶸쀣겘�귙굤�얇걲�뗰폕",
                billing_faq_title_4: "�꾠겇�㎯굚�띴에�믤��곥굥�볝겏��룾�썬겎�쇻걢竊�",
                billing_faq_title_5: "��꺖�앫꺍��겑��굠�녴겓�⑴뵪�쇻굥�볝겏�뚣겎�띲겲�쇻걢竊�",
                change_fb_page: "Facebook�싥꺖�멥굮鸚됪쎍�쇻굥",
                confirm_pixel_confirm: "�꾠걚�덀�귛푷�▲겲�쇻��",
                confirm_pixel_sharing: "Pixel�뚦릪�잆걬�뚣겍�꾠겲�쎼굯�귛릪�잆걣若뚥틙�뺛굦�뗥뎺�ャ깪�쇈궦�믧썴�뚣겲�쇻걢竊�",
                confirm_remove_ad: "�볝겗佯껃몜�믤쑍壤볝겓�딃솮�쀣겍�귙굠�띲걮�꾠겎�쇻걢竊�",
                confirm_remove_ad_group: "佯껃몜�믣뎷�ㅳ걮�╉굚�덀굧�쀣걚�㎯걲�뗰폕 {index}?",
                confirm_unpublished: "鸚됪쎍�믢퓷耶섅걮�╉�곥궘�ｃ꺍�싥꺖�녈겓�띷삝�뺛걵�얇걲�뗰폕",
                confirm_unpublished_btn_cancel: "�꾠걚�덀�귛쨯�담굮岳앭춼�쀣겲�쎼굯��",
                confirm_unpublished_btn_confirm: "��걚�귛쨯�담굮岳앭춼�쀣겍�띷삝�뺛걵�얇걲��",
                conversions: "�녈꺍�먦꺖�멥깾�녔빊",
                conversions_modal_add_url: "�ャ궧�욍깲URL�믣뒥�덀겍�뤵걽�뺛걚",
                conversions_modal_title: "��쉪�믧겦�욄빊�쀣겍�뤵걽�뺛걚竊덅쨭�겼룾竊�",
                copied_facebook: "Facebook�ャ궠�붵꺖�뺛굦�얇걮��",
                copied_instagram: "Instagram�ャ궠�붵꺖�뺛굦�얇걮��",
                copy_carousel_facebook: "�볝겗佯껃몜�묯acebook�ャ궠�붵꺖�쇻굥",
                copy_carousel_instagram: "�볝겗佯껃몜�묲nstagram�ャ궠�붵꺖�쇻굥",
                create_by_onboarding_tooltip: "/img/common/create_by_onboarding_tooltip_kr.png",
                crop_modal_title: "�삣깗��깉�ゃ깱�녈궛",
                crop_tooltip: "�대�吏� �먮Ⅴ湲�",
                echo: "{content}",
                edit_all: "�ⓦ겍渶③썓�쇻굥",
                edit_all_index: "佯껃몜�믥랬�녴걲�� {index}",
                edit_this_ad: "�볝겗佯껃몜渶③썓",
                enter_adriel_coupon: "��꺖�앫꺍�녈꺖�됥굮鼇섇뀯",
                estimation_per_day: "訝��� {value} ��꺁�껁궚",
                express: "�듽겲�뗣걵�쇻굥竊�",
                failed_to_register_coupon: "�볝겗��꺖�앫꺍��뿢�ヤ슴�ⓦ걬�뚣겍�꾠겲�쇻��",
                fb_permissions_missing: "�볝궦�띲궧嶸←릤��Ł�먦걣恙낁쫨�㎯걲",
                fb_reason_no_image: "�볝겗�싥꺖�멥겓��깤��깢�▲궎�ョ뵪�숂쐿��걗�듽겲�쎼굯��",
                fb_reason_no_permission: "�볝겗�싥꺖�멥겓恙낁쫨�よ㉠��걣擁녈굤�얇걵�볝��",
                fb_reason_no_published: "�볝겗�싥꺖�멥겘�븃죱�뺛굦�╉걚�얇걵�볝��",
                fb_reason_token_permission: "�볝궦�띲궧嶸←릤��Ł�먦걣恙낁쫨�㎯걲 (窈㎩��삠꺍�욍꺖�멥겗�뤵걚�덀굩��)",
                go_to_schedule: "�밤궞�멥깷�쇈꺂�띹Þ若�",
                indicator_label_ad: "佯껃몜",
                indicator_label_ap: "沃겹걣佯껃몜�믡겳�얇걲�뗰폕",
                indicator_label_plan: "�밤궞�멥깷�쇈꺂 & 雅덄츞",
                indicator_nav_label_ad: "佯껃몜",
                indicator_nav_label_ap: "�ゃ꺖�뉎궍�ⓦ꺍��",
                indicator_nav_label_plan: "�밤궞�멥깷�쇈꺂 &\n 雅덄츞",
                launch_campaign: "�볝겗��깵�녈깪�쇈꺍�믧뀓岳▲걲�뗣��",
                launch_check_payment: "�볝겗��깵�녈깪�쇈꺍�믧뀓岳▲걲�뗣겓���곥걡��돂�방퀡�믥쇉�꿔걮�╉걦�졼걬�꾠��",
                navigation_finish: "永귚틙�쇻굥",
                navigation_publish: "�븃죱�쇻굥",
                navigation_reset: "�ゃ궩�껁깉�쇻굥",
                new_returning_new_tooltip: "Google Analytics�얇걼�츿ixel��궋�ャ궑�녈깉�믡걡�곥걾��졃�덀겘�곲걥�삠겏�얍쑉��궖�밤궭�욁꺖�ラ줊鴉쇈걮�잍뼭誤뤵깺�쇈궣�쇈굚�ャ겲�뚣겲�쇻�귙걪��꺁�밤깉�츶ogole Analytics�쭾ixel��깄�긱겓榮먦겈�꾠겍�꾠겲�쇻��",
                new_returning_returning_tooltip: "�귙겒�잆겗�╉궒�뽧궢�ㅳ깉�ヨØ�뚣걼�볝겏��걗�뗣깺�쇈궣�쇈�곥겲�잆겘�ゃ궧�덀겓�귙겏�γ걚�잍뿢耶섌¨若㏂겎�쇻�괦ixel�갍oogle analytics�곥겲�잆겘�ゃ궧�덀걣�뚧쐿�뺛굦�╉걚�ゃ걚�닷릦��㈁壤볝겒�쀣겏�ゃ굤�얇걲��",
                next: "轝▲겦",
                next_step: "轝▲겗�밤깇�껁깤",
                no_ads_warn: "�ⓦ겍��틕�듽겘�띴에�쒏�訝�겎�쇻�귡뀓岳▲굮�뗥쭓�쇻굥�닷릦�칢witch�믤듉�쀣겍�뤵걽�뺛걚��",
                no_fb_admin_pages: "�싥꺖�며��녴겓�삯뙯�쀣겍�귙굥Facebook�싥꺖�멥겘�귙굤�얇걵�볝�괚acebook訝듽겎�㏂궖�╉꺍�덀굮閻븃첀�쀣겍�뤵걽�뺛걚�� <a href='https://www.facebook.com/help/510247025775149?helpref=faq_content' target='_blank'>Page Role Settings</a>",
                no_pixel_detected: "�귙겒�잆겗Facebook�㏂궖�╉꺍�덀겓榮먦겈�묆굢�뚣걼Pixel�믦쫳�ㅳ걨�뗣걪�ⓦ걣�㎯걤�얇걵�볝��",
                no_preview_mp4: "mp4�뺛궊�ㅳ꺂��땿�삠겓��깤�с깛�γ꺖��걗�듽겲�쎼굯��",
                not_enough_information: "��깵�녈깪�쇈꺍�믧뀓岳▲걲�뗣걼�곥겓恙낁쫨�ゆ깄�긱걣�귙굤�얇걵�볝��",
                past_end_date: "佯껃몜�룩죱永귚틙�γ걣�롥렮�ヨÞ若싥걬�뚣겍�꾠겲�쇻��",
                pixel_how_to: [{
                    image: "/img/proposal/pixel_screenshot1.png",
                    title: "PROPOSAL.pixel_how_to_title_0"
                }, {
                    image: "/img/proposal/pixel_screenshot2.png",
                    title: "PROPOSAL.pixel_how_to_title_1"
                }, {
                    image: "/img/proposal/pixel_screenshot3.png",
                    title: "PROPOSAL.pixel_how_to_title_2"
                }],
                pixel_how_to_title_0: 'Pixel�믣릪�잆걲�뗣걼�곥겓�볝겗�ゃ꺍��굮�쇈걮��<a href="{link}" target="_blank">{link}</a> (You must be logged in Facebook) Facebook�믧뼀�꾠겍�뤵걽�뺛걚�� �뚧쐿�쀣걼�껺ixel�믧겦�볝겎�뤵걽�뺛걚��"Assign Partner"�쒌궭�녈굮��꺁�껁궚�쀣겍�뤵걽�뺛걚',
                pixel_how_to_title_1: '�뗣걚�잆깪�쇈궦��<b>"Pixel Editor"</b> �믧겦�욁걮�╉걦�졼걬�꾠�괦ixel訝듽겗�뉎꺖�욍겘�㏂깋�ゃ궓�ャ걣�ゃ꺖�뉎궍�ⓦ꺍�밤궭�쇈궟�녴궍�녈궛�믤��⒴뙑�쇻굥�잆굙�ュ퓚誤곥겒�끻젿�㎯걲',
                pixel_how_to_title_2: 'Enter <span class="copy-target"><b>525463574501100</b><span class="copy">COPY</span></span> (Adriel\'s Facebook Business ID) in Business ID. Click "Confirm" to finish on that page. Click "Done" button below and you\'re all set.',
                plan_budget_daily: "�ε닪",
                plan_budget_day: "��",
                plan_budget_minimum: "��鵝롣틛嶸�",
                plan_budget_payment_method: "�딀뵱�뺞뼶力�",
                plan_budget_total: "�덅쮫",
                plan_estimated_clicks: "佯껃몜��꺁�껁궚��",
                plan_estimated_not_available: "屋꿨퐪�쇻굥永먩옖��걗�듽겲�쎼굯",
                plan_estimated_warning: "�볝걪��〃鹽뷩걬�뚣굥�겼춻��틛嶸쀣겓野얇걲�뗣깙�뺛궔�쇈깯�녈궧��렓若싥겎�귙굤�곩츪�쎼겗永먩옖�믢퓷鼇쇈걲�뗣굚��겎��걗�듽겲�쎼굯��",
                plan_go_to_ads: "佯껃몜�ヨ죱��",
                plan_optimization_info: "�㏂깋�ゃ궓�ャ겘��깵�녈깪�쇈꺍�묆깢�⒲꺖�욁꺍�밤굮��鸚㎩뙑�쇻굥�잆굙�ヨ눎�뺛겎���⒴뙑�쀣겲�쇻��",
                plan_reason_facebook_page_missing: "Facebook�싥꺖�멥걣誤뗣겇�뗣굤�얇걵�볝��",
                plan_reason_no_ads_enabled: "�띴에訝�겗佯껃몜��걗�듽겲�쎼굯��",
                plan_reason_not_enabled: "�띴에訝�겗 {channel} 佯껃몜��걗�듽겲�쎼굯��",
                plan_schedule_asap: "ASAP",
                plan_schedule_continuously: "��깵�녈깪�쇈꺍�띴에�믢퍓�γ걢�됬텤泳싩쉪�ュ쭓�곥굥",
                plan_schedule_end: "永귚틙",
                plan_schedule_set_start: "�뗥쭓�γ굮鼇�츣�쇻굥",
                plan_schedule_spend_head: "餓δ툓雅덄츞�믢슴�뤵겒��",
                plan_schedule_spend_tail: "訝��ε퐪�잆굤",
                plan_schedule_start: "�뗥쭓",
                plan_schedule_start_end: "�뗥쭓�γ겏永귚틙�γ굮鼇�츣�쇻굥��",
                plan_schedule_start_tooltip: "若잓슋��뼀冶뗦뿥�ⓩ셽�볝겘野⒵읅�ャ굠�ｃ겍�싥굦�뗥졃�덀걣�귙굤�얇걲��",
                plan_title_budget: "雅덄츞",
                plan_title_optimization: "�㏂깋�ゃ궓�ャ겓�덀굥�ゅ땿���⒴뙑",
                plan_title_results: "�ε닪永먩옖�ⓨ츣",
                plan_title_schedule: "�밤궞�멥깷�쇈꺂",
                price_credit: "��꺃�멥긿��",
                price_day: "��",
                price_platform_fee: "佯껃몜揶믢퐪縕�",
                price_platform_fee_tooltip: "�볝겗縕사뵪�츴acebook�껯oogle�ゃ겑��깤�⒲긿�덀깢�⒲꺖�졾닶�ⓦ겓鵝욍굩�뚣굥�귙겗�㎯걲��",
                price_service_fee: "�듐꺖�볝궧縕�",
                price_service_fee_tooltip: "�볝겗縕사뵪��궋�됥꺁�ⓦ꺂��걢�뜰굜�듐깮�쇈깉�뉎궧��닶�ⓦ겓鵝욍굩�뚣굥�귙겗�㎯걲��",
                price_to_pay: "�딀뵱�뺛걚�쇻굥�묌죲",
                price_total: "�덅쮫",
                price_total_tooltip: "獄뗦콆��겲�졼걬�뚣겲�쎼굯�귙궚�с궦�껁깉�ャ꺖�됥겗�됧듅�㎯굮閻뷩걢�곥굥�잆굙�ャ�곩컩窈띲겗岳앲㉫�묆굮獄뗦콆�뺛걵�╉걚�잆걽�띲겲�쇻�귛츪�쎼겗獄뗦콆��깺�쇈궣�쇈걣佯껃몜�믡궚�ゃ긿��걮�잌졃�덀겗�욜쇇�잆걮�얇걲��",
                published_toast: "�뉎꺖�요퓷耶�",
                remove_image_tooltip: "�삣깗/�뺟뵽�믣룚�딃솮��",
                run_continuously: "永귚틙�γ겒�쀣겓佯껃몜�룩죱 ",
                schedule_date_format: "D MMM YYYY",
                schedule_date_month_names: ["1��", "2��", "3��", "4��", "5��", "6��", "7��", "8��", "9��", "10��", "11��", "12��"],
                schedule_day_names: ["��", "��", "��", "麗�", "��", "��", "��"],
                select_fb_page: "Facebook page�믧겦��",
                select_fb_page_desc: "Facebook佯껃몜�믧뀓岳▲걲�뗣걼�곥겓���곥궋�됥꺁�ⓦ꺂�뚣걗�ゃ걼��깛�멥깓�밭뵪Facebook�싥꺖�멩깄�긱굮�뽩풓�쇻굥恙낁쫨�뚣걗�듽겲�쇻�귚툔��깭�욍꺍�믡궚�ゃ긿��걮�쪭acebook�ャ꺆�겹궎�녈걮�╉걦�졼걬�꾠��",
                select_fb_page_ment: "鵝욍걚�잆걚�싥꺖�멥굮�멩뒢�쀣겍�뤵걽�뺛걚",
                select_fb_pixel_ment: "�뚧쐿�쀣걼�껺ixel�믧겦�욁걮�╉걦�졼걬��",
                service_preview_image_0: "/img/proposal/en_budget_service_screen_0.png",
                service_preview_image_1: "/img/proposal/en_budget_service_screen_1.png",
                service_preview_text: "�㏂깋�ゃ궓�ャ겗�듐꺖�볝궧�ャ겘鵝뺛걣�ャ겲�뚣겍�꾠겲�쇻걢竊�",
                slide_delete_tooltip: "�밤꺀�ㅳ깋�믣뎷�ㅳ걲��",
                slide_select_tooltip: "�밤꺀�ㅳ깋�믧겦��",
                slide_upload_tooltip: "�삣깗/�뺟뵽�믡궋�껁깤��꺖�됥걲��",
                terms_services_1: ' "��깵�녈깪�쇈꺍�띴에"�믡궚�ゃ긿��걮�╉걦�졼걬�꾠��',
                terms_services_2: "�뚧꼷�쇻굥",
                terms_services_3: "��",
                terms_services_4: "",
                terms_services_policy: "�쀣꺀�ㅳ깘�룔꺖�앫꺁�룔꺖",
                terms_services_terms: "�⑴뵪誤뤹큵",
                tracker_modal_no_dec: "<span>Your current objective is <b>Link Click</b>. If you want to access more objectives, connect to your Google Analytics or Facebook Pixel.</span>",
                tracker_modal_no_dec_GA: "<span>Your current objective is <b>Link Click</b>.<br/>If you want to access more objectives,<br/>connect to your Google Analytics.</span>",
                tracker_modal_no_fb: "We haven't detected any Pixel associated with your Facebook account.",
                tracker_modal_no_fb_help: "Learn how to connect to Facebook Pixel.",
                tracker_modal_no_ga: "�귙겒�잆겗Google�㏂궖�╉꺍�덀겓�츶oogle Analytics�뚨킄�γ걚�╉걚�얇걵�볝��",
                tracker_modal_no_ga_help: "Google Analytics��궋�ャ궑�녈깉鼇�츣�ⓦ궎�녈궧�덀꺖�ユ뼶力뺛굮耶╉겤��",
                tracker_modal_title: "Google Analytics�㏂궖�╉꺍�덀굮�멩뒢�쇻굥��",
                unable_to_retarget_toast: "�롥렮�얇걼��뤎�ⓦ겗�ャ궧�욍깯�쇈깈�쇈궭�뚣겒�꾠걼�곥�곫뿢耶섅궖�밤궭�욁꺖�믡궭�쇈궟�녴궍�녈궛�쇻굥�볝겏�뚣겎�띲겲�쎼굯�귙궖�밤궭�욁꺖�ゃ궧�덀�� Google Analytics�곥겲�잆겘Pixels�믣릪�잆걮�╉걦�졼걬�꾠��"
            },
            ko: {
                ad_config_all_description: "愿묎퀬 �띿뒪��",
                ad_config_all_logo: "濡쒓퀬 �대�吏�",
                ad_config_all_main_title: "愿묎퀬 �쒕ぉ",
                ad_config_all_media: "硫붿씤 �대�吏� / 鍮꾨뵒��",
                ad_config_all_short_title: "吏㏃� �쒕ぉ",
                ad_config_all_url: "�쒕뵫 URL",
                ad_config_app_url: "紐⑤컮�� �� URL",
                ad_config_appstore_url: "App store",
                ad_config_business_name: "鍮꾩쫰�덉뒪 �대쫫",
                ad_config_carousel_url_common: "�쒕뵫 URL (怨듯넻)",
                ad_config_facebook_carousel_description: "愿묎퀬 �띿뒪��",
                ad_config_facebook_carousel_description_common: "愿묎퀬 �띿뒪�� (怨듯넻)",
                ad_config_facebook_description: "愿묎퀬 �띿뒪��",
                ad_config_facebook_headline: "�쒕ぉ",
                ad_config_facebook_link_description: "�ㅻ챸",
                ad_config_form_url: "DB 愿묎퀬",
                ad_config_google_display_description: "愿묎퀬 �띿뒪��",
                ad_config_google_display_image: "硫붿씤 �대�吏�",
                ad_config_google_display_title: "愿묎퀬 �쒕ぉ",
                ad_config_google_display_title_long: "湲� �쒕ぉ",
                ad_config_google_text_description: "愿묎퀬 �띿뒪�� 1",
                ad_config_google_text_description_2: "愿묎퀬 �띿뒪�� 2",
                ad_config_google_text_title_1: "�쒕ぉ 1",
                ad_config_google_text_title_2: "�쒕ぉ 2",
                ad_config_google_text_title_3: "�쒕ぉ 3",
                ad_config_optional: "�좏깮",
                ad_config_playstore_url: "Play store",
                ad_config_required: "�꾩닔",
                ad_config_url_choose: "愿묎퀬�� URL���낆쓣 �낅젰�섏뿬 �좏깮�섏꽭��.",
                ad_config_web_url: "�뱀궗�댄듃 URL",
                ad_config_web_url_for_app: "�뱀궗�댄듃 URL",
                ad_edit_image_recommendations: "�대�吏� 沅뚯옣 �ъ뼇",
                ad_edit_recommendations_file_format_title: "- �뚯씪 �뺤떇:",
                ad_edit_recommendations_max_length_content: "{value}遺�",
                ad_edit_recommendations_max_length_title: "- 理쒕� 湲몄씠:",
                ad_edit_recommendations_min_size_content: "{width} * {height}�쎌�",
                ad_edit_recommendations_min_size_title: "- 理쒖냼 �ъ씠利�:",
                ad_edit_recommendations_recommended_length_content: "理쒕� {value}珥�",
                ad_edit_recommendations_recommended_length_title: "- 沅뚯옣 湲몄씠:",
                ad_edit_recommendations_recommended_video_ratio_content: "�몃줈諛⑺뼢 ({h}:{v})",
                ad_edit_recommendations_recommended_video_ratio_title: "- �붾㈃ 鍮꾩쑉",
                ad_edit_recommendations_size_content: "{width} * {height}�쎌�",
                ad_edit_recommendations_size_title: "- 沅뚯옣 �ъ씠利�:",
                ad_edit_recommendations_sound_content: "�ъ슜(罹≪뀡 �ы븿)",
                ad_edit_recommendations_sound_title: "- �뚮━:",
                ad_edit_recommendations_video_format_title: "- �뚯씪�뺤떇:",
                ad_edit_recommendations_video_resolution_content: "{value}�쎌�",
                ad_edit_recommendations_video_resolution_title: "- �댁긽�� 理쒖냼 �덈퉬:",
                ad_edit_recommendations_video_size_content: "理쒕� {value}湲곌�",
                ad_edit_recommendations_video_size_title: "- �뚯씪 �ш린:",
                ad_edit_video_recommendations: "�숈쁺��/�щ씪�대뱶�� 沅뚯옣 �ъ뼇",
                ad_status_disabled: "愿묎퀬 鍮꾪솢�깊솕",
                ad_status_enabled: "愿묎퀬 �쒖꽦��",
                add_adriel_coupon: "�꾨뱶由ъ뿕 愿묎퀬 荑좏룿",
                add_instagram_page: "�몄뒪��洹몃옩 �섏씠吏� 異붽�",
                add_keywords_modal_placeholder: "寃��� �ㅼ썙�쒕� �낅젰�댁＜�몄슂",
                add_keywords_modal_title: "寃��� �ㅼ썙�� 異붽�",
                add_location_modal_placeholder: "��寃� 吏���쓣 異붽��댁＜�몄슂",
                add_location_modal_title: "��寃� 吏���쓣 異붽��댁＜�몄슂",
                add_new_ad: "愿묎퀬 �뚯옱 異붽�",
                add_slide_indexed: "�щ씪�대뱶 {index} 異붽�",
                add_slides: "�щ씪�대뱶 異붽�",
                ads_business_name_missing: "鍮꾩쫰�덉뒪 �대쫫�� �낅젰�댁＜�몄슂.",
                ads_description_missing: "愿묎퀬 �띿뒪�몃� �낅젰�댁＜�몄슂.",
                ads_image_missing: "愿묎퀬 �대�吏�瑜� �낅줈�쒗빐二쇱꽭��.",
                ads_invalid_min_dimension: "�몄뒪��洹몃옩�� 鍮꾨뵒�� �붾㈃ �ъ씠利덇� 理쒖냼 500�쎌� �댁긽�댁뼱�� �⑸땲��.",
                ads_invalid_url: "愿묎퀬瑜� �대┃�� �� 諛⑸Ц�섎뒗 �ъ씠�몄쓽 二쇱냼瑜� �뺥솗�� �낅젰�댁＜�몄슂. �щ씪�대뱶 愿묎퀬瑜� �앹꽦�섏뀲�� 寃쎌슦, 媛� �щ씪�대뱶留덈떎 �ъ씠�� 二쇱냼媛� �낅젰�섏뼱 �덈뒗吏� �뺤씤�댁＜�몄슂.",
                ads_invalid_video_length: "�몄뒪��洹몃옩�� 鍮꾨뵒�� 湲몄씠媛� {value}珥� 誘몃쭔�댁뼱�� �⑸땲��.",
                ads_invalid_video_ratio: "�몄뒪��洹몃옩�� 鍮꾨뵒�� �붾㈃ 鍮꾩쑉�� 諛섎뱶�� 1:1�댁뼱�� �⑸땲��.",
                ads_media_missing: "�대�吏� �먮뒗 �숈쁺�� �낅줈�쒗빐二쇱꽭��. �섏씠�ㅻ턿, �몄뒪��洹몃옩 諛� 援ш� �붿뒪�뚮젅�� 愿묎퀬瑜� 吏묓뻾�섏떆�ㅻ㈃ 諛섎뱶�� �대�吏� �먮뒗 �숈쁺�곸씠 �꾩슂�⑸땲��. �대�吏��� �숈쁺�곸씠 �낅줈�� �섏� �딆쓣 寃쎌슦 �ъ뼇�� �먯꽭�� �뺤씤�댁＜�몄슂.",
                ads_should_be_less: "{paramName}�� {max}湲��먮� 珥덇낵�� �� �놁뒿�덈떎.",
                ads_title_missing: "愿묎퀬 �쒕ぉ�� �낅젰�댁＜�몄슂.",
                ads_upload_fail_dimension: "�대�吏� �ъ씠利덇� �덈Т �묒뒿�덈떎. 理쒖냼 �ъ씠利덈� �뺤씤 �� �대�吏�瑜� 議곗젙�섏뿬 �ㅼ떆 �낅줈�쒗빐二쇱꽭��.",
                ads_upload_fail_size: "�뚯씪 �⑸웾�� 理쒕�移섎� 珥덇낵�섏��듬땲��. �뚯씪 �ъ씠利덈� 議곌툑 �� �묎쾶 議곗젙 �� �ㅼ떆 �낅줈�쒗빐二쇱꽭��.",
                after_copy_cancel: "�リ린",
                after_copy_confirm: "�대룞�섍린",
                ap_card_content_new: "�좉퇋",
                ap_card_content_returning: "�щ갑臾�",
                ap_card_customer_list_desc: "�ㅼ쓬 由ъ뒪�몄쓽 怨좉컼�� ��寃� �ㅻ뵒�몄뒪�� 異붽��⑸땲��.",
                ap_card_device_desktop: "�곗뒪�ы깙",
                ap_card_device_mobile: "紐⑤컮��",
                ap_card_edit_customer_list: "由ъ뒪�� 異붽�",
                ap_card_edit_interests: "怨좉컼 愿��ъ궗 異붽�",
                ap_card_edit_keywords: "寃��됱뼱 異붽�",
                ap_card_edit_locations: "吏��� 異붽�",
                ap_card_edit_objectives: "�꾪솚 紐⑺몴瑜� �좏깮�섏꽭��.",
                ap_card_edit_reset: "理쒖큹媛믪쑝濡� 由ъ뀑",
                ap_card_edit_visitor: "�뱀궗�댄듃 �몃옒而� 蹂�寃�",
                ap_card_income_high: "�곸쐞",
                ap_card_income_low: "�섏쐞",
                ap_card_income_medium: "以묒쐞",
                ap_card_income_top: "理쒖긽��",
                ap_card_interests_info: "愿묎퀬二쇰떂�� 怨좉컼�� �대뼡 愿��ъ궗瑜� 媛�吏�怨� �덈뒗吏� �뚮젮二쇱꽭��",
                ap_card_let_adriel: "�꾨뱶由ъ뿕�� �좏깮",
                ap_card_location_info: "�꾨옒 吏���뿉 �닿퀬 �덈뒗 �щ엺�ㅼ뿉寃� 愿묎퀬瑜� �대낫�낅땲��.",
                ap_card_location_info_empty: "吏��� �뺣낫瑜� �낅젰�댁＜�몄슂.",
                ap_card_login_google_tracker: "援ш� 濡쒓렇��",
                ap_card_login_pixel_tracker: "�쎌� 怨듭쑀",
                ap_card_no_customer_list: "怨좉컼 由ъ뒪�멸� �놁뒿�덈떎.",
                ap_card_no_interests: "�뱀젙 �λ��� �댄빐愿�怨꾨� 媛�吏� �щ엺�ㅼ뿉寃� 愿묎퀬瑜� �대낫�낅땲��.",
                ap_card_no_objectives: "�� �덉궛 {currency}40 �댁긽�쇱떆 �좎슜�� �ㅼ젙�낅땲��.<br> �� �덉궛 {currency}40 誘몃쭔�쇱떆 嫄대꼫�곗뀛�� �⑸땲��.",
                ap_card_no_objectives_unconnected: "�대떦 湲곕뒫�� �ъ슜�섍린 �꾪빐 癒쇱� 援ш� �좊꼸由ы떛�� �먮뒗 �섏씠�ㅻ턿 �쎌��� 怨듭쑀�댁＜�몄슂.",
                ap_card_no_objectives_unconnected_GA: "�대떦 湲곕뒫�� �ъ슜�섍린 �꾪빐 癒쇱� 援ш� �좊꼸由ы떛�ㅻ� 怨듭쑀�댁＜�몄슂.",
                ap_card_no_objectives_unconnected_FB: "�대떦 湲곕뒫�� �ъ슜�섍린 �꾪빐 癒쇱� �섏씠�ㅻ턿 �쎌��� 怨듭쑀�댁＜�몄슂.",
                ap_card_no_pixels: "�꾪솚 紐⑺몴 �ㅼ젙�� �먰븯�쒕㈃ 援ш� �좊꼸由ы떛�� �먮뒗 �쎌��� �곌껐�� 二쇱꽭��.",
                ap_card_no_search_keywords: "�좏깮�섏떆�� �ㅼ썙�쒖� 愿��⑤맂 寃��됱쓣 �섎뒗 �щ엺�ㅼ뿉寃� 愿묎퀬瑜� �대낫�낅땲��.",
                ap_card_no_tracker: '<span>愿묎퀬 �깃낵 洹밸���</span>瑜� �꾪빐 沅뚯옣 �쒕┰�덈떎.<span class="space">�곌껐 �� �뱀궗�댄듃 諛⑸Ц�� 由ы�寃뚰똿 諛� �꾪솚 紐⑺몴 理쒖쟻�붽� �대（�댁쭛�덈떎.</span>',
                ap_card_objectives_info: "�꾪솚 紐⑺몴瑜� �좏깮�섏꽭��.",
                ap_card_relation_married: "湲고샎",
                ap_card_relation_relationship: "�곗븷 以�",
                ap_card_relation_single: "�깃�",
                ap_card_search_keywords_info: "�꾨옒 �ㅼ썙�쒕� 寃��됲븯�� �щ엺�ㅼ뿉寃� 愿묎퀬瑜� �대낫�낅땲��.",
                ap_card_select_objectives: "�꾪솚 紐⑺몴 �좏깮�섍린",
                ap_card_title_age: "�섏씠",
                ap_card_title_customers: "怨쇨굅 & �꾩옱 怨좉컼 由ъ뒪��",
                ap_card_title_customers_tooltip: "�대찓�� �뺣낫媛� �닿릿 由ъ뒪�몃쭔 �쒖슜 媛��ν빀�덈떎 (�꾪솕踰덊샇 遺덇�) �대뒗 �좏깮 �ы빆�낅땲��.",
                ap_card_title_device: "��寃� �μ튂",
                ap_card_title_gender: "��寃� �깅퀎",
                ap_card_title_income: "�뚮뱷",
                ap_card_title_interests: "��寃� 怨좉컼�� 愿��ъ궗",
                ap_card_title_location: "��寃� 吏���",
                ap_card_title_new_returning: "�좉퇋 & �щ갑臾� 怨좉컼",
                ap_card_title_objectives: "�꾪솚 紐⑺몴 �ㅼ젙�섍린",
                ap_card_title_objectives_tooltip: "�대뒗 �좏깮 �ы빆�대굹, �� �믪� 愿묎퀬 �깃낵瑜� �꾪빐 諛섎뱶�� �꾩슂�⑸땲��. 臾몄쓽�ы빆�� �ㅻⅨ履� �꾨옒 梨꾪똿�� �댁슜�댁＜�몄슂.",
                ap_card_title_relation: "寃고샎/�곗븷 �곹깭",
                ap_card_title_search_keywords: "寃��� �ㅼ썙��",
                ap_card_title_tracker: "�뱀궗�댄듃 �몃옒而�",
                ap_card_title_tracker_tooltip: "�대뒗 �좏깮 �ы빆�대굹, �� �믪� 愿묎퀬 �깃낵瑜� �꾪빐 諛섎뱶�� �꾩슂�⑸땲��. 臾몄쓽�ы빆�� �ㅻⅨ履� �꾨옒 梨꾪똿�� �댁슜�댁＜�몄슂.",
                ap_card_tracker_fb_app_info: "�섏씠�ㅻ턿 �� 沅뚰븳 �뱀씤",
                ap_card_tracker_google_content: "愿묎퀬二쇰떂�� �뱀궗�댄듃�� 援ш� �좊꼸由ы떛�ㅻ룄 �곌껐�섏뼱 �덈떎硫�, �꾨옒 踰꾪듉�� �대┃�섏뀛�� 援ш� �좊꼸由ы떛�� 怨꾩젙�� 濡쒓렇�명빐二쇱꽭��.",
                ap_card_tracker_info: "援ш� �좊꼸由ы떛�� 怨꾩젙",
                ap_card_tracker_pixel_content: "愿묎퀬二쇰떂�� �뱀궗�댄듃�� �쎌��� �곌껐�섏뼱 �덈떎硫�, �꾨옒 踰꾪듉�� �대┃�섏뀛�� �쎌��� 怨듭쑀�댁＜�몄슂.",
                ap_card_tracker_pixel_info: "�섏씠�ㅻ턿 �쎌� 沅뚰븳 �뱀씤",
                ap_summary_close: "�リ린",
                ap_summary_seemore: "�� �먯꽭�� 蹂닿린",
                ap_summary_title: "愿묎퀬二쇰떂�� 怨좉컼�� �� 媛��μ꽦�� �믪� �щ엺�ㅼ뿉寃� 愿묎퀬瑜� 蹂대깄�덈떎.",
                behaviors: "�됰룞",
                below_minimum_budget: "理쒖냼 �덉궛 湲덉븸蹂대떎 �곸� 湲덉븸�� 湲곗엯�섏뀲�듬땲��.",
                billing_faq: "鍮꾩슜愿��� FAQ",
                billing_faq_content_1: '愿묎퀬瑜� 留뚮뱾怨� 吏묓뻾�섎뒗 怨쇱젙�� <span class="highlight">臾대즺�낅땲��.</span> <span class="dummy-space"></span>�щ엺�ㅼ씠 愿묎퀬二쇰떂�� �뱀궗�댄듃瑜� 諛⑸Ц�섍린 �꾪빐�� 愿묎퀬瑜� �대┃�� �� 鍮꾩슜�� 諛쒖깮�⑸땲��. �꾨뱶由ъ뿕�� �댁슜�섎뒗 愿묎퀬二쇰떂�ㅼ� 蹂댄넻 �대┃�� 150�먯뿉�� 2,000�� �ъ씠�� 鍮꾩슜�� 吏�遺덊빀�덈떎. �대떦 湲덉븸�� 援ш��대굹 �섏씠�ㅻ턿怨� 媛숈� 愿묎퀬 �뚮옯�쇱뿉 吏�遺덈릺硫�, �꾨뱶由ъ뿕�� 愿묎퀬�� �ъ슜�� 湲덉븸�� �쇱젙 鍮꾩쑉 (13%)濡쒖꽌 �곗젙�섎뒗 ���� �쒕퉬�� �섏닔猷뚮� 泥�뎄�⑸땲��. �대떦 �섏닔猷뚯뿉�� 愿묎퀬 �⑥쑉 愿�由� 諛� 理쒖쟻�� 湲곗닠 吏��� �� 愿묎퀬二쇰떂�� �깃낵瑜� 洹밸��뷀븯湲� �꾪븳 �ㅼ뼇�� ���� �쒕퉬�� 鍮꾩슜怨� �꾨뱶由ъ뿕 �뚮옯�� �쇱씠�좎뒪 媛�寃⑹씠 �ы븿�섏뼱 �덉뒿�덈떎.<span class="dummy-space"></span> 愿묎퀬二쇰떂�� 鍮꾩쫰�덉뒪 洹쒕え�� �꾩슂�� �곕씪 理쒖냼 12�щ윭遺��� 愿묎퀬 �덉궛�� �먯쑀濡�쾶 梨낆젙�� �� �덉뒿�덈떎. �덈� �ㅼ뼱, �섎（�� 3留뚯썝�� 3�� 媛� 愿묎퀬�섍만 �먰븯�쒕뒗 寃쎌슦, 3留뚯썝x3��=珥� 9留뚯썝�� 愿묎퀬 �뚮옯�� 鍮꾩슜怨� �� 1留뚯썝�� �쒕퉬�� �섏닔猷뚮� �뷀븯�� 珥� 10留뚯썝�쇰줈 愿묎퀬瑜� 吏묓뻾�� �� �덉뒿�덈떎. <span class="highlight">異붽� 鍮꾩슜�� �놁뒿�덈떎.</span>',
                billing_faq_content_2: '愿묎퀬 �뚮옯�� 鍮꾩슜�� �ы븿�� 紐⑤뱺 愿묎퀬 吏묓뻾 鍮꾩슜 諛� �쒕퉬�� �섏닔猷뚮뒗 �꾨뱶由ъ뿕�� �곗꽑 遺��댄븯硫�, <span class="highlight">愿묎퀬二쇰떂�� 珥덇린�� 吏�遺덊븯�� 鍮꾩슜�� �놁뒿�덈떎. </span> 理쒖큹 10踰덉쓽 寃곗젣�� ���댁꽌�� 愿묎퀬鍮꾩슜�� ��湲� 泥�뎄 湲곗��≪씤 50�щ윭�� �꾨떖�섎뒗 �쒖젏�� �먮룞�쇰줈 鍮꾩슜�� 泥�뎄�⑸땲��. <span class="dummy-space"></span>�좊ː�� 留뚰븳 寃곗젣 �댁뿭�� �꾩쟻�� 愿묎퀬二쇰떂�ㅼ뿉 ���댁꽌�� 愿묎퀬 吏묓뻾 鍮꾩슜�� 3�� 留덈떎 �� 踰덉뵫 寃곗젣�⑸땲��. �� �덉궛�� 100�щ윭 �댁긽�� 寃쎌슦, 寃곗젣�� �섎（�� �� 踰� �� �대（�� 吏묐땲��.<span class="dummy-space"></span> 泥�뎄 諛⑹떇�� ���� �� 援ъ껜�곸씤 �뺣낫�� �ш린(<a href="https://intercom.help/adriel/en/articles/2630018-�쒕퉬��-鍮꾩슜��-�대뼸寃�-泥�뎄�섎굹��" target="_blank">https://intercom.help/adriel/en/articles/2630018-�쒕퉬��-鍮꾩슜��-�대뼸寃�-泥�뎄�섎굹��</a>)�먯꽌 �뺤씤�댁＜�몄슂.',
                billing_faq_content_3: "��, �몄젣�좎� �먰븯�쒕뒗 �쒖젏�� 愿묎퀬瑜� �쎄쾶 以묐떒�� �� �덉뒿�덈떎. �ㅼ젣濡� 吏묓뻾�� 愿묎퀬�� ���댁꽌留� 鍮꾩슜 諛� �섏닔猷뚭� 泥�뎄�⑸땲��.",
                billing_faq_content_4: "��, �꾨뱶由ъ뿕�� 紐⑤뱺 �ъ슜�먮뒗 �뚯썝 媛��낃낵 �숈떆�� 10�щ윭, �� 1留� 1泥쒖썝�� 愿묎퀬 �щ젅�㏃씠 吏�湲됰맗�덈떎. �� �щ젅�� 留뚰겮 臾대즺濡� 愿묎퀬瑜� 吏묓뻾�섏떎 �� �덉쑝硫�, �щ젅�� �≪닔瑜� 珥덇낵�섎뒗 愿묎퀬 吏묓뻾 鍮꾩슜�� ���댁꽌留� �꾨텋 泥�뎄�⑸땲��.",
                billing_faq_content_5: '�몃옒�쎌씠 留롮� �좎뿉�� 愿묎퀬媛� �� �먯＜ 寃뚯옱�� �� �덉뒿�덈떎. �� 寃쎌슦 <a target="_blank" href="https://support.google.com/google-ads/answer/2375423?hl=ko">援ш�</a> 愿묎퀬�� �� �덉궛�� 理쒕� 200%, <a target="_blank" href="https://www.facebook.com/business/help/190490051321426">�섏씠�ㅻ턿</a> 愿묎퀬�� 理쒕� 25%源뚯� 吏묓뻾�섏뼱 鍮꾩슜�� 諛쒖깮�� �� �덉뒿�덈떎.',
                billing_faq_content_6: "�뱀씪 �뚯쭊�� 愿묎퀬 吏묓뻾鍮꾨낫�� �� �곸� 湲덉븸�쇰줈 �덉궛�� 蹂�寃쏀븯�쒕뒗 寃쎌슦, 蹂�寃쏀븯�� �덉궛�� �ㅼ쓬 �좊��� �곸슜�⑸땲��.",
                billing_faq_title_1: "�쒕퉬�� 媛�寃⑹� �쇰쭏�멸���?",
                billing_faq_title_2: "�쒕퉬�� 鍮꾩슜�� �대뼸寃� 泥�뎄�섎굹��?",
                billing_faq_title_3: "愿묎퀬瑜� �꾨Т �뚮굹 以묐떒�� �� �덈굹��?",
                billing_faq_title_4: "泥섏쓬 �댁슜�대큶�덈떎. 臾대즺濡� �댁슜�대낵 �� �덈굹��?",
                billing_faq_title_5: "愿묎퀬 吏묓뻾鍮꾧� �쇱삁�곗쓣 珥덇낵�댁꽌 �ъ슜�� �� �덈굹��?",
                billing_faq_title_6: "�섎（ 以묒뿉 �쇱씪 �덉궛�� 蹂�寃쏀븯硫� �대뼸寃� �섎굹��?",
                cancel_copy: "蹂듭궗痍⑥냼",
                change_fb_page: "�섏씠�ㅻ턿 �섏씠吏� 諛붽씀湲�",
                confirm_appstore_connect: "�� 怨듭쑀媛� �꾨즺�섏� �딆븯�듬땲��. 洹몃옒�� �� 李쎌쓣 �レ쑝�쒓쿋�듬땲源�?",
                confirm_appstore_connect_exit: "��",
                confirm_appstore_connect_stay: "�꾨땲��, �좎떆留뚯슂",
                confirm_pixel_confirm: "�꾨땲��, �좎떆留뚯슂",
                confirm_pixel_sharing: "�쎌� 怨듭쑀媛� �꾨즺�섏� �딆븯�듬땲��. 洹몃옒�� �� 李쎌쓣 �レ쑝�쒓쿋�듬땲源�?",
                confirm_remove_ad: "愿묎퀬瑜� ��젣�섏떆寃좎뒿�덇퉴?",
                confirm_remove_ad_group: "愿묎퀬 {index}�� ��젣�섏떆寃좎뒿�덇퉴?",
                confirm_unpublished: "蹂�寃� �ы빆�꾩��ν븯怨� 愿묎퀬瑜� �낅뜲�댄듃 �섏떆寃좎뒿�덇퉴?",
                confirm_unpublished_btn_cancel: "�꾨땲��, 蹂�寃쏀븯吏� �딆뒿�덈떎",
                confirm_unpublished_btn_confirm: "��, 蹂�寃쎌궗�� ���� �� �낅뜲�댄듃",
                connect_app_store: "�좏뵆 �깆뒪�좎뼱 �� 怨듭쑀",
                connect_appstore_step_0: '�ㅼ쓬 留곹겕 <a href="https://appstoreconnect.apple.com/access/users" target="_blank">https://appstoreconnect.apple.com/access/users</a> 瑜� �대┃�섏뿬 濡쒓렇�명븯硫� �꾨뱶由ъ뿕瑜� �묒꽭�� �� �� �덈뒗 �붾㈃�쇰줈 �곌껐 �⑸땲��. �꾨옒 �섏씠吏��먯꽌 + 踰꾪듉�� �대┃�섏뿬 �� �ъ슜�먮� 異붽��댁＜�몄슂. (�ъ슜�먮뒗 怨꾩젙 愿�由ъ옄 �ъ빞 �⑸땲��)',
                connect_appstore_step_1: "�꾨옒 �댁슜�� 李멸퀬�섏뿬 �꾨뱶由ъ뿕 �뺣낫瑜� 異붽��댁꽌 �ъ슜�먮줈 珥덈��댁＜�몄슂.",
                connect_appstore_steps: [{
                    image: "/img/screenshots/kr_appstore_connect_1.jpg",
                    title: "PROPOSAL.connect_appstore_step_0"
                }, {
                    image: "/img/screenshots/kr_appstore_connect_2.jpg",
                    title: "PROPOSAL.connect_appstore_step_1"
                }],
                content_copied: "�댁슜 蹂듭궗",
                conversions: "�쎌� �대깽��",
                conversions_modal_add_url: "吏곸젒 �꾪솚 URL �낅젰�섍린",
                conversions_modal_title: "�꾪솚 紐⑺몴 �좏깮�섍린 (蹂듭닔 �좏깮 媛���)",
                copied_facebook: "�섏씠�ㅻ턿�쇰줈 蹂듭궗�섏뿀�듬땲��.",
                copied_instagram: "�몄뒪��洹몃옩�쇰줈 蹂듭궗�섏뿀�듬땲��.",
                copy_ad: "愿묎퀬 蹂듭궗",
                copy_carousel_facebook: "�섏씠�ㅻ턿�� 蹂듭궗�섍린",
                copy_carousel_instagram: "�몄뒪��洹몃옩�� 蹂듭궗�섍린",
                create_by_onboarding_tooltip: "/img/common/create_by_onboarding_tooltip_kr.png",
                crop_modal_title: "�대�吏� �먮Ⅴ湲�",
                crop_tooltip: "�대�吏� �먮Ⅴ湲�",
                echo: "{content}",
                edit_all: "紐⑤몢 �몄쭛�섍린",
                edit_all_index: "愿묎퀬 {index} �몄쭛",
                edit_this_ad: "�� 愿묎퀬 �몄쭛�섍린",
                enter_adriel_coupon: "荑좏룿 肄붾뱶瑜� �낅젰�섏꽭��.",
                estimation_per_day: "�섎（ {value} �대┃",
                express: "�꾨뱶由ъ뿕�� �댁＜�몄슂!",
                failed_to_register_coupon: "�대� �곸슜�섏뿀�듬땲��",
                family_statuses: "媛�議깃�怨�",
                fb_permissions_missing: "鍮꾩��덉뒪 愿�由� 沅뚰븳�� �꾩슂�⑸땲��",
                fb_reason_no_image: "�섏씠吏��� �꾨줈�� �ъ쭊�� �깅줉�댁＜�몄슂.",
                fb_reason_no_permission: "�섏씠吏��� ���� 愿�由ъ옄 沅뚰븳�� �뺤씤�댁＜�몄슂",
                fb_reason_no_published: "�섏씠吏�瑜� 怨듦컻�곹깭濡� �ㅼ젙�댁＜�몄슂.",
                fb_reason_token_permission: "鍮꾩��덉뒪 愿�由� 沅뚰븳�� �꾩슂�⑸땲�� (怨좉컼�쇳꽣 臾몄쓽)",
                finish_copy: "蹂듭궗 �꾨즺",
                go_to_schedule: "�쇱젙 �ㅼ떆 �ㅼ젙�섍린",
                indicator_label_ad: "愿묎퀬",
                indicator_label_ap: "愿묎퀬瑜� 蹂닿쾶 �� �щ엺�ㅼ�?",
                indicator_label_plan: "�쇱젙 諛� �덉궛",
                indicator_label_form: "DB 愿묎퀬",
                indicator_nav_label_ad: "愿묎퀬",
                indicator_nav_label_ap: "��寃� 怨좉컼",
                indicator_nav_label_plan: "�쇱젙 諛� �덉궛",
                indicator_nav_label_form: "DB 愿묎퀬",
                interests: "愿��ъ궗",
                launch_campaign: "愿묎퀬 吏묓뻾�섍린",
                launch_check_payment: "愿묎퀬 吏묓뻾�� �꾪빐 移대뱶 �뺣낫瑜� �낅젰�댁＜�몄슂. �ㅻ뒛 諛붾줈 寃곗젣�섎뒗 湲덉븸�� �놁쑝硫�, 1�� �덉궛 媛�寃곗젣 �댄썑 �뱀씤 痍⑥냼�⑸땲��.",
                navigation_finish: "�꾨즺",
                navigation_publish: "諛섏쁺",
                navigation_reset: "珥덇린��",
                need_extend_schedule: "愿묎퀬 醫낅즺�쇱쓣 �곗옣 �� 二쇱꽭��",
                new_returning_new_tooltip: "援ш��좊꼸由ы떛�ㅻ굹 �쎌��� �곌껐�섏떆硫�, 愿묎퀬二쇰떂�� 怨좉컼怨� �좎궗�� �좉퇋 �좎옱 怨좉컼�� ��寃잛뿉 �ы븿�⑸땲��. 援ш��좊꼸由ы떛�ㅻ굹 �쎌��� �뺣낫瑜� 湲곕컲�쇰줈 ��寃� 怨좉컼 由ъ뒪�멸� 留뚮뱾�댁쭛�덈떎.",
                new_returning_returning_tooltip: "�뱀궗�댄듃 諛⑸Ц�먮뱾怨� 怨좉컼 由ъ뒪�몃� 湲곕컲�쇰줈 �� 怨쇨굅�� �꾩옱 怨좉컼�ㅼ쓣 �섎��⑸땲��. �쎌�, 援ш� �좊꼸由ы떛��, �먮뒗 怨좉컼 由ъ뒪�멸� �놁쓣 寃쎌슦 �좏깮�� �� �놁뒿�덈떎.",
                next: "�ㅼ쓬",
                next_step: "�ㅼ쓬 �④퀎",
                no_ads_created: "紐⑤뱺 愿묎퀬 �뚯옱瑜� 吏��곗뀲�ㅼ슂.\n �� 異붽� 踰꾪듉�� �뚮윭 �ㅼ떆 留뚮뱾�� 蹂댁꽭��.",
                no_ads_warn: "紐⑤뱺 愿묎퀬 諛뺤뒪�� �ㅼ쐞移섍� 爰쇱졇�덉뒿�덈떎. 泥ル쾲吏� �섏씠吏��먯꽌 �ㅼ쐞移섎� 耳� 二쇱꽭��.",
                no_ads_warn_ad: "紐⑤뱺 愿묎퀬 諛뺤뒪媛� 爰쇱졇�덉뒿�덈떎. 愿묎퀬 諛뺤뒪�� �ㅼ쐞移섎� 耳� 二쇱꽭��.",
                no_ads_warn_form: "紐⑤뱺 愿묎퀬 諛뺤뒪�� �ㅼ쐞移섍� 爰쇱졇�덉뒿�덈떎. �먮쾲吏� �섏씠吏��먯꽌 �ㅼ쐞移섎� 耳� 二쇱꽭��.",
                no_fb_admin_pages: "濡쒓렇�명븯�� �섏씠�ㅻ턿 怨꾩젙�� 愿�由ъ옄 沅뚰븳�� 媛�吏� �섏씠吏�媛� 諛쒓껄�섏� �딆븯�듬땲��. �곌껐�섏떆�ㅻ뒗 �섏씠吏��� 愿�由ъ옄 沅뚰븳�� 媛�吏� �섏씠�ㅻ턿 怨꾩젙�쇰줈 濡쒓렇�명빐二쇱꽭��. <a href='https://www.facebook.com/help/510247025775149?helpref=faq_content' target='_blank'>�섏씠吏� 愿�由ъ옄 �ㅼ젙 諛⑸쾿</a>",
                no_pixel_detected: "愿묎퀬二쇰떂�� �섏씠�ㅻ턿 怨꾩젙怨� �곌껐�� �쎌��� �뺤씤�섏� �딆뒿�덈떎",
                no_preview_mp4: "mp4 鍮꾨뵒�ㅻ뒗 誘몃━蹂닿린媛�\n 吏��먮릺吏� �딆뒿�덈떎",
                not_enough_information: "愿묎퀬瑜� 吏묓뻾�섎뒗�� �꾩슂�� �뺣낫媛� 異⑸텇�섏� �딆뒿�덈떎",
                overspent_tooltip: "�몃옒�쎌씠 留롮� �좎뿉�� 愿묎퀬媛� �� �먯＜ 寃뚯옱�� �� �덉뒿�덈떎. �� 寃쎌슦 援ш� 愿묎퀬�� �� �덉궛�� 理쒕� 200%, �섏씠�ㅻ턿 愿묎퀬�� 理쒕� 25%源뚯� 吏묓뻾�섏뼱 鍮꾩슜�� 諛쒖깮�� �� �덉뒿�덈떎.",
                past_end_date: "愿묎퀬 吏묓뻾 醫낅즺�쇱씠 怨쇨굅濡� �ㅼ젙�섏뼱 �덉뒿�덈떎.",
                paste: "遺숈뿬�ｊ린",
                pixel_how_to: [{
                    image: "/img/proposal/ko_pixel_screenshot1.png",
                    title: "PROPOSAL.pixel_how_to_title_0"
                }, {
                    image: "/img/proposal/ko_pixel_screenshot2.png",
                    title: "PROPOSAL.pixel_how_to_title_1"
                }, {
                    image: "/img/proposal/ko_pixel_screenshot3.png",
                    title: "PROPOSAL.pixel_how_to_title_2"
                }],
                pixel_how_to_title_0: '�ㅼ쓬 留곹겕 <a a href="{link}" target="_blank">{link}</a>瑜� �대┃�섎㈃ �꾨뱶由ъ뿕�� �쎌��� 怨듭쑀�� �� �덈뒗 �섏씠�ㅻ턿 �붾㈃�쇰줈 �곌껐�⑸땲��.(�섏씠�ㅻ턿�� 濡쒓렇�몃릺�� �덈뒗吏� �뺤씤�댁＜�몄슂) �꾨옒 �섏씠吏��먯꽌 怨듭쑀�섍퀬�� �섎뒗 �쎌��� �좏깮�섏떊 ��, �섑뙆�몃꼫 �좊떦竊� 踰꾪듉�� �대┃�댁＜�몄슂.',
                pixel_how_to_title_1: "�앹뾽 李쎌씠 �섑��섎㈃ ��븷�먯꽌 �섑뵿�� �몄쭛�먥�숇� �좏깮�댁＜�몄슂. 愿묎퀬二쇰떂�� �꾪빐 �� �곹빀�� �좎옱 怨좉컼�� 李얘린 �꾪빐�쒕뒗 �쎌��먯꽌 怨듭쑀�섎뒗 �뺣낫媛� �꾩슂�⑸땲��.",
                pixel_how_to_title_2: '鍮꾩쫰�덉뒪 ID�� <span class="copy-target"><b>525463574501100</b><span class="copy">蹂듭궗�섍린</span></span>瑜� �낅젰�섏떆怨� �뺤씤 踰꾪듉�� �뚮윭二쇱꽭��. �섎떒�� �쒖셿猷뚳펰踰꾪듉�� �뚮윭二쇱떆硫� 紐⑤뱺 �덉감媛� �꾨즺�⑸땲��.',
                plan_budget_daily: "1�� �덉궛",
                plan_budget_day: "��",
                plan_budget_minimum: "理쒖냼 �덉궛",
                plan_budget_payment_method: "寃곗젣 �섎떒",
                plan_budget_total: "珥� �덉궛",
                plan_estimated_clicks: "愿묎퀬 �대┃",
                plan_estimated_not_available: "�댁슜 媛��ν븳 �덉긽 寃곌낵媛� �놁뒿�덈떎.",
                plan_estimated_warning: "�덉긽 寃곌낵�� 愿묎퀬二쇰떂�� �덉궛�� 諛뷀깢�쇰줈 異붿젙�� �섏튂�� 肉� 媛숈� 寃곌낵媛� 蹂댁옣�섏��� �딆뒿�덈떎.",
                plan_go_to_ads: "愿묎퀬濡� �대룞",
                plan_optimization_info: "愿묎퀬 �깃낵瑜� 洹밸��뷀븯湲� �꾪빐 �꾨뱶由ъ뿕�� 愿묎퀬 �꾨왂�� 理쒖쟻�뷀빀�덈떎. 愿묎퀬 吏묓뻾 以� �붿슧 �깃낵媛� 醫뗭� 愿묎퀬 �뚮옯�� �먮뒗 ��寃� 怨좉컼�먭쾶 �� 留롮� �덉궛�� �먮룞�쇰줈 諛곕텇�⑸땲��.",
                plan_reason_apple_not_connected: "�깆뒪�좎뼱 �깆씠 �곌껐�섏뼱�덉� �딆뒿�덈떎.",
                plan_reason_facebook_page_missing: "�섏씠�ㅻ턿 �섏씠吏�媛� �곌껐�섏뼱�덉� �딆뒿�덈떎.",
                plan_reason_no_ads_enabled: "紐⑤뱺 愿묎퀬媛� 鍮꾪솢�깊솕 �곹깭�낅땲��.",
                plan_reason_not_enabled: "{channel} 愿묎퀬媛� �쒖꽦�붾릺吏� �딆븯�듬땲��.",
                plan_schedule_asap: "諛붾줈 愿묎퀬 �쒖옉�섍린",
                plan_schedule_continuously: "�ㅻ뒛遺��� 怨꾩냽 愿묎퀬�섍린",
                plan_schedule_end: "醫낅즺",
                plan_schedule_set_start: "�쒖옉�� 吏��뺥븯湲�",
                plan_schedule_spend_head: "�섎（��",
                plan_schedule_spend_tail: "�댁긽 吏�異쒗븯吏� �딆뒿�덈떎.",
                plan_schedule_start: "�쒖옉",
                plan_schedule_start_end: "�쒖옉�쇨낵 醫낅즺�� 吏��뺥븯湲�",
                plan_schedule_start_tooltip: "�ㅼ젣 愿묎퀬 �쒖옉�쇱� 愿묎퀬 寃��� �쒓컙�쇰줈 �명빐 �ㅼ젙�섏떊 �좎쭨�� �ㅻ� �� �덉뒿�덈떎.",
                plan_title_budget: "�덉궛",
                plan_title_optimization: "�꾨뱶由ъ뿕�� �먮룞 理쒖쟻�� 湲곕뒫",
                plan_title_results: "�쇱씪 �덉긽 寃곌낵",
                plan_title_schedule: "�쇱젙",
                preview_copy_ad: "愿묎퀬 �뚯옱 蹂듭궗",
                price_credit: "�щ젅��",
                price_day: "��",
                price_platform_fee: "愿묎퀬 �뚮옯�� 鍮꾩슜",
                price_platform_fee_tooltip: "�섏씠�ㅻ턿怨� 援ш� �� 愿묎퀬 �뚮옯�쇱뿉 吏�遺덊븯�� 留ㅼ껜 鍮꾩슜�낅땲��.",
                price_service_fee: "�쒕퉬�� 鍮꾩슜",
                price_service_fee_tooltip: "愿묎퀬 �⑥쑉 愿�由� 諛� 理쒖쟻�� 湲곗닠 吏��� �� 愿묎퀬二쇰떂�� �깃낵瑜� 洹밸��뷀븯湲� �꾪븳 �ㅼ뼇�� �쒕퉬�ㅼ� �꾨뱶由ъ뿕 �뚮옯�� �쇱씠�좎뒪 媛�寃⑹씠 �ы븿�섏뼱 �덉뒿�덈떎.",
                price_to_pay: "理쒖쥌 吏�遺� 湲덉븸",
                price_total: "珥앸퉬��",
                price_total_tooltip: "鍮꾩슜�� �뱀옣 吏�遺덈릺吏� �딆뒿�덈떎. 移대뱶 �깅줉 �� 罹좏럹�� 吏묓뻾�� �섏떆硫� 1�� �덉궛 湲덉븸�� 寃곗젣�섎굹, �좏슚�� 移대뱶�몄� �뺤씤�섎㈃ �먮룞�쇰줈 �뱀씤 痍⑥냼媛� �⑸땲��. �ㅼ젣 寃곗젣�� 愿묎퀬媛� 吏묓뻾�� �댄썑 吏�遺덊빐�� �� 湲덉븸�� �뺥솗�섍쾶 �곗젙�섎㈃ �대（�댁�寃� �⑸땲��.",
                published_toast: "諛섏쁺�섏뿀�듬땲��",
                remove_image_tooltip: "�대�吏�/鍮꾨뵒�� ��젣",
                run_continuously: "醫낅즺�� �놁씠 怨꾩냽 愿묎퀬�섍린 ",
                schedule_date_format: "YYYY[��] MM[��] D[��]",
                schedule_date_month_names: ["1��", "2��", "3��", "4��", "5��", "6��", "7��", "8��", "9��", "10��", "11��", "12��"],
                schedule_day_names: ["��", "��", "��", "��", "紐�", "湲�", "��"],
                select_date: "�좎쭨 �좏깮�섍린",
                select_fb_page: "�섏씠�ㅻ턿 �섏씠吏� �좏깮",
                select_fb_page_desc: "�섏씠�ㅻ턿 愿묎퀬瑜� 吏묓뻾�섍린 �꾪빐�쒕뒗 愿묎퀬二쇰떂�� �섏씠�ㅻ턿 鍮꾩쫰�덉뒪 �섏씠吏��� ���� �뺣낫媛� �꾩슂�⑸땲��. �꾨옒 踰꾪듉�� �대┃�댁꽌 �섏씠�ㅻ턿�� 濡쒓렇�명빐二쇱꽭��.",
                select_fb_page_ment: "愿묎퀬�� �ъ슜�� �섏씠吏�瑜� �좏깮�댁＜�몄슂",
                select_fb_pixel_ment: "�곌껐�� �먰븯�쒕뒗 �쎌��� �좏깮�댁＜�몄슂.",
                service_preview_image_0: "/img/proposal/ko_budget_service_screen_0.png",
                service_preview_image_1: "/img/proposal/ko_budget_service_screen_1.png",
                service_preview_text: "�꾨뱶由ъ뿕 �쒕퉬�� �ы븿 �ы빆",
                slide_delete_tooltip: "�щ씪�대뱶 ��젣",
                slide_select_tooltip: "�щ씪�대뱶 �좏깮",
                slide_upload_tooltip: "�대�吏�/鍮꾨뵒�� �낅줈��",
                terms_services_1: '"愿묎퀬 吏묓뻾�섍린"瑜� �대┃�⑥쑝濡쒖뜥,',
                terms_services_2: "",
                terms_services_3: "怨�",
                terms_services_4: "�� �숈쓽�⑸땲��.",
                terms_services_policy: "媛쒖씤�뺣낫�뺤콉",
                terms_services_terms: "�쒕퉬�� �쎄�",
                tracker_modal_no_dec: "<span>�꾩옱 留곹겕 �대┃ �꾩＜�� 愿묎퀬媛� 吏꾪뻾�섍퀬 �덉뒿�덈떎.<br/>�꾪솚 �꾩＜ �� �ㅻⅨ 紐⑹쟻�� 愿묎퀬瑜� 吏꾪뻾�섏떆怨� �띕떎硫�<br/>援ш� �좊꼸由ы떛�� �먮뒗 �섏씠�ㅻ턿 �쎌��� �곌껐�댁＜�몄슂.<span>",
                tracker_modal_no_dec_GA: "<span>�꾩옱 留곹겕 �대┃ �꾩＜�� 愿묎퀬媛� 吏꾪뻾�섍퀬 �덉뒿�덈떎.<br/>�꾪솚 �꾩＜ �� �ㅻⅨ 紐⑹쟻�� 愿묎퀬瑜� 吏꾪뻾�섏떆怨� �띕떎硫�<br/>援ш� �좊꼸由ы떛�ㅻ� �곌껐�댁＜�몄슂.<span>",
                tracker_modal_no_fb: "濡쒓렇�명븯�� �섏씠�ㅻ턿 怨꾩젙怨� �곕룞�� �쎌��� �놁뒿�덈떎. ",
                tracker_modal_no_fb_help: "�쎌� �곌껐�섎뒗 踰�",
                tracker_modal_no_ga: "援ш� 怨꾩젙怨� �곌껐�� 援ш��좊꼸由ы떛�ㅺ� 議댁옱�섏� �딆뒿�덈떎. ",
                tracker_modal_no_ga_help: "援ш��좊꼸由ы떛�� �ㅼ튂 諛� �ㅼ젙 諛⑸쾿 �먯꽭�� 蹂닿린",
                tracker_modal_title: "援ш��좊꼸由ы떛�� 怨꾩젙 �좏깮",
                unable_to_retarget_toast: "愿묎퀬二쇰떂�� 湲곗〈 怨좉컼�� ���� �뺣낫媛� �놁씠�� �щ갑臾� 怨좉컼�� ���곸쑝濡� 愿묎퀬瑜� 吏묓뻾�� �� �놁뒿�덈떎. 怨좉컼 由ъ뒪�몃굹 援ш� �좊꼸由ы떛��, �먮뒗 �쎌� �뺣낫瑜� �쒓났�댁＜�몄슂.",
                undo_paste: "痍⑥냼"
            }
        }
    }, 9968: function (e, t) {
        e.exports = {
            en: {
                category_my_file: "My",
                category_finance: "Finance\nReal Estate",
                category_mobile: "Mobile App\nIT",
                category_beauty: "Beauty\nMedicine",
                category_business: "Business\nEducation",
                category_food: "Food\nBeverage",
                category_events: "Events\nWedding\nTravel",
                category_automobile: "Automobile\nElectronic Device",
                category_home: "Home/Living\nPets\nBaby Care",
                category_others: "Others",
                cannot_submit_when_crop: "You haven't finished cropping yet.",
                crop: "Crop",
                crop_description: "1:1 is the recommended size ratio for most platforms.",
                fail_to_import_image: "Image upload failed.",
                image_template: "Image template",
                no_media: "Please select image or video.",
                no_image: "Please select an image.",
                no_more_ten_my_files: "You can only upload up to 10 files on 'My list'",
                try_again: "Try again",
                upload_media: "Image/Video\nupload",
                upload_image: "Image upload",
                delete_image: "Delete Image"
            },
            jp: {
                category_my_file: "燁곥겗�뺛궊�ㅳ꺂",
                category_finance: "�묋엻/訝띶땿��",
                category_mobile: "�㏂깤��/IT",
                category_beauty: "�볝깷�쇈깇�ｃ꺖/獰롥�/�사셽",
                category_business: "�볝궦�띲궧/�숃궟",
                category_food: "繇잆겧��/繇잌쟼",
                category_events: "�ㅳ깧�녈깉/�╉궒�뉎궍�녈궛/�낁죱",
                category_automobile: "�ゅ땿邕�/�삣춴艅잌솳",
                category_home: "�ㅳ꺍�녴꺁��/�ゃ깛�녈궛/�싥긿��/�쇻깛��",
                category_others: "�앫겗餓�",
                cannot_submit_when_crop: "��꺆�껁깤�믣츑雅녴걮�╉걦�졼걬�꾠��",
                crop: "��꺆�껁깤",
                crop_description: "1:1 is the recommended ratio for most platforms.",
                fail_to_import_image: "�ㅳ깳�쇈궦�믣뫜�뜰겗�ュㅁ�쀣걮�얇걮�잆��",
                image_template: "�ㅳ깳�쇈궦�녴꺍�쀣꺃�쇈깉",
                no_media: "�ㅳ깳�쇈궦�얇걼��깛�뉎궕�믧겦�볝겎�뤵걽�뺛걚��",
                no_image: "�ㅳ깳�쇈궦�믧겦�볝겎�뤵걽�뺛걚��",
                no_more_ten_my_files: "10�ユ��ゆ��ャ궋�껁깤��꺖�됥걮�╉걦�졼걬�꾠��",
                try_again: "�띹ĳ烏�",
                upload_media: "�ㅳ깳�쇈궦/�볝깈��\n�㏂긿�쀣꺆�쇈깋",
                upload_image: "�ㅳ깳�쇈궦�㏂긿�쀣꺆�쇈깋"
            },
            ko: {
                category_my_file: "�� �뚯씪",
                category_finance: "湲덉쑖/遺��숈궛",
                category_mobile: "紐⑤컮�� ��/IT",
                category_beauty: "酉고떚/誘몄슜/�섎즺",
                category_business: "鍮꾩쫰�덉뒪/援먯쑁",
                category_food: "�뚯떇/�앸떦",
                category_events: "�대깽��/�⑤뵫/�ы뻾",
                category_automobile: "�먮룞李�/�꾩옄湲곌린",
                category_home: "��/由щ튃/��/踰좎씠鍮�",
                category_others: "湲고�",
                cannot_submit_when_crop: "�щ∼�� 癒쇱� �꾨즺�� 二쇱꽭��.",
                crop: "�먮Ⅴ湲�",
                crop_description: "1:1 鍮꾩쑉�� �대�吏�媛� ��遺�遺꾩쓽 愿묎퀬 �뚮옯�쇱뿉 �곹빀�⑸땲��.",
                fail_to_import_image: "�대�吏�瑜� 遺덈윭�ㅻ뒗�� �ㅽ뙣�덉뒿�덈떎.",
                image_template: "�대�吏� �쒗뵆由�",
                no_media: "�대�吏� �뱀� 鍮꾨뵒�ㅻ� �좏깮�댁＜�몄슂.",
                no_image: "�대�吏�瑜� �좏깮�댁＜�몄슂.",
                no_more_ten_my_files: "10媛� 誘몃쭔�쇰줈 �낅줈�� �댁＜�몄슂.",
                try_again: "�ㅼ떆 �쒕룄�섍린",
                upload_media: "�대�吏�/鍮꾨뵒��\n�낅줈��",
                upload_image: "�대�吏� �낅줈��",
                delete_image: "�대�吏� �쒓굅"
            }
        }
    }, "9bc9": function (e, t, a) {
        "use strict";
        var n = a("2638"), o = a.n(n), i = (a("ac6a"), a("cebc")), r = a("75fc"), s = a("2f62"), c = a("847d"), l = {
            render: function () {
                var e = arguments[0], t = this.value ? "#5aabe3" : "#ccc", a = this.value ? "dot-circle" : "circle";
                return e("b-icon", {attrs: {pack: "far", icon: a}, style: {color: t}})
            }, props: {value: {type: Boolean, default: !1}}
        }, d = {
            name: "InputBox", props: ["isEditable", "data", "index", "type"], data: function () {
                return {title: "", answer: ""}
            }, methods: {
                changed: function (e) {
                    var t = this;
                    return function (a) {
                        return t.changeStoreVal([e], a)
                    }
                }, changeStoreVal: function (e, t) {
                    var a = ["formAnswers", this.index].concat(Object(r["a"])(e));
                    return this.changeData([a, function (e, a) {
                        var n = a.length, o = a[n - 1], i = a.slice(0, n - 1).reduce(function (e, t, a) {
                            return e = e[t], e
                        }, e);
                        i[o] = t
                    }])
                }
            }, render: function () {
                var e = this, t = arguments[0];
                return t("div", {class: "onboarding-form-input-area"}, [t("div", {
                    style: "display: inline-block",
                    class: "onboarding-form-question"
                }, [(this.data.title.indexOf("FORM.") > -1 ? this.$t("".concat(this.data.title)) : this.data.title) || this.$t("".concat(this.data.default))]), t("input", o()([{
                    on: {
                        input: function (t) {
                            t.target.composing || (e.answer = t.target.value)
                        }
                    },
                    attrs: {type: "text", placeholder: "", disabled: "preview" === this.type},
                    class: "onboarding-form-input-answer",
                    domProps: {value: e.answer}
                }, {directives: [{name: "model", value: e.answer, modifiers: {}}]}]))])
            }
        }, u = {
            name: "Multiples",
            props: ["isEditable", "data", "index", "type"],
            data: function () {
                return {title: "", checked: !1}
            },
            methods: Object(i["a"])({}, Object(s["mapActions"])("form", ["setData", "changeData"]), {
                changed: function (e) {
                    var t = this;
                    return function (a) {
                        return t.changeStoreVal([e], a)
                    }
                }, changeStoreVal: function (e, t, a) {
                    var n = this, o = ["formResultData", "body", this.index].concat(Object(r["a"])(e));
                    return this.changeData([o, function (e, o) {
                        var i = o.length, r = o[i - 1], s = o.slice(0, i - 1).reduce(function (e, t, a) {
                            return e = e[t], e
                        }, e);
                        a ? (t ? a(s[r], t) : a(s, r), n.$forceUpdate()) : s[r] = t
                    }])
                }
            }),
            render: function () {
                var e = this, t = arguments[0];
                return t("div", {class: "onboarding-form-question-area noHover"}, [t("div", [t("div", {class: "onboarding-form-checkbox-header"}, [t("div", {class: "onboarding-form-input-title-wrap"}, [t("div", {class: "onboarding-form-multiple-title userform noHover"}, [(this.data.title.indexOf("FORM.") > -1 ? this.$t("".concat(this.data.title)) : this.data.title) || this.$t("".concat(this.data.default))])])])]), this.data.choices && this.data.choices.map(function (a, n) {
                    return "checkbox" === e.data.type ? t("div", {class: ["onboarding-form-checkbox-wrap", {userpage: "preview" !== e.type}]}, [t("div", {
                        class: "onboarding-form-checkbox",
                        on: {
                            click: function () {
                                return e.changeStoreVal(["choices", n, "isChecked"], !a.isChecked)
                            }
                        }
                    }, [t(c["a"], {attrs: {value: a.isChecked}}), t("div", {class: "onboarding-form-checkbox-input-wrap"}, [t("div", {class: "onboarding-form-option noHover"}, [a.choice || "".concat(e.$t("".concat(a.default)))])])])]) : t("div", {class: ["onboarding-form-multiple-wrap", {userpage: "preview" !== e.type}]}, [t("div", {
                        class: "onboarding-form-multiple",
                        on: {
                            click: function () {
                                return e.changeStoreVal(["choices", n], null, function (e, t) {
                                    e[t].isChecked || e.forEach(function (a, n) {
                                        return e[n].isChecked = n == t
                                    })
                                })
                            }
                        }
                    }, [t(l, {
                        attrs: {"native-value": !0}, model: {
                            value: a.isChecked, callback: function (e) {
                                a.isChecked = e
                            }
                        }
                    }), t("div", {class: "onboarding-form-checkbox-input-wrap"}, [t("div", {class: "onboarding-form-option onboarding-form-option-new noHover"}, [a.choice || "".concat(e.$t("".concat(a.default)))])])])])
                })])
            }
        }, p = {short: d, multiple: u, checkbox: u};
        t["a"] = {
            name: "FormResult",
            props: {type: {type: String}},
            data: function () {
                return {isEditable: !1, nowEdit: 1, value: "test", title: "email", previewStage: "question"}
            },
            created: function () {
            },
            render: function () {
                var e = this, t = arguments[0];
                return t("div", {class: "form_container"}, [t("div", {
                    class: ["form-preview-cancelbutton", {"not-visible": "preview" !== this.type}],
                    on: {
                        click: function () {
                            return e.$emit("setClose")
                        }
                    }
                }, [t("b-icon", {
                    attrs: {pack: "fal", icon: "times"},
                    class: "cancel-icon"
                })]), this.formCreate.img && t("div", {class: ["form_banner_area", {preview: "preview" === this.type}]}, [t("div", {class: "header_banner"}, [t("img", {attrs: {src: this.formCreate.img}})])]), t("div", {class: "form-white-area"}, [this.renderFormContents])])
            },
            computed: Object(i["a"])({}, Object(s["mapGetters"])("form", ["formCreate", "imgSrc", "formData", "thanksMessage"]), {
                _formResultData: {
                    get: function () {
                        return this.$store.state.form.formResultData
                    }, set: function (e) {
                        this.changeData(["formResultData", function (t, a) {
                            t[a] = e
                        }])
                    }
                }, renderQuestions: function () {
                    var e = this, t = this.$createElement;
                    return t("div", {class: ["form-area", {preview: "preview" === this.type}]}, [t("div", {class: "scroll-zone"}, [t("div", {class: "form-header-wrap"}, [this.formCreate && this.formCreate.header.map(function (a) {
                        return t("div", {class: "form-header-".concat(a.type)}, [a.title || e.$t("FORM.".concat(a.type, "Default"))])
                    })]), t("div", {class: "form-border"})]), t("div", {class: "form-inputs"}, [this.formCreate && this.formCreate.body.map(function (t, a) {
                        return e.renderBy(t, a, e.type)
                    })]), t("div", {class: "form-policy-wrap"}, [t("div", {class: "form-policy--exp"}, [this.$t("FORM.policy")]), t("div", {class: "form-policy--btn"}, [this.$t("FORM.policy_underline")])]), t("div", {class: "form-border"}), this.renderButton])
                }, renderThanksMessage: function () {
                    var e = this.$createElement;
                    return e("div", {class: ["form-area", {preview: "preview" === this.type}]}, [e("p", {class: "form-thanks-default"}, [this.$t("FORM.submitted")]), e("p", {class: "form-thanks-message"}, [this.thanksMessage]), this.renderButton])
                }, renderButton: function () {
                    var e = this, t = this.$createElement;
                    return t("div", {class: "form-footer-wrap"}, [t("button", {
                        class: "preview",
                        on: {
                            click: function () {
                                return "thanks" === e.previewStage ? e.$emit("setClose") : e.previewStage = "thanks"
                            }
                        }
                    }, ["question" === this.previewStage ? this.$t("FORM.submit_btn") : this.$t("COMMON.done")])])
                }, renderFormContents: function () {
                    switch (this.previewStage) {
                        case"question":
                            return this.renderQuestions;
                        case"thanks":
                            return this.renderThanksMessage
                    }
                }
            }),
            methods: Object(i["a"])({}, Object(s["mapActions"])("form", ["changeData"]), {
                renderBy: function (e, t, a) {
                    var n = this, i = this.$createElement, r = p[e.type], s = {
                        attrs: {isEditable: this.isEditable, data: e, index: t, type: a},
                        on: {
                            deleteItem: function (e) {
                                n.changeStoreVal([e], void 0, function (e, t) {
                                    return e.splice(t, 1)
                                })
                            }
                        }
                    };
                    return i(r, o()([{ref: t}, s]))
                }
            })
        }
    }, "9ced": function (e, t, a) {
        "use strict";
        var n = a("d955"), o = a.n(n);
        o.a
    }, "9de3": function (e, t, a) {
        "use strict";
        var n = a("56ba"), o = a.n(n);
        o.a
    }, "9ec2": function (e, t, a) {
        "use strict";
        var n = a("b417"), o = a.n(n);
        o.a
    }, "9f84": function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("cebc"), o = a("95c3"), i = a.n(o), r = (a("c499"), a("2bd2")), s = a("5670"), c = a("1585"),
                l = a("ebb6"), d = a("69dd"), u = {
                    guides: !0,
                    "view-mode": 2,
                    "drag-mode": "none",
                    autoCrop: !0,
                    "auto-crop-area": .8,
                    "min-container-width": 250,
                    "min-container-height": 180,
                    background: !1,
                    alt: "Source Image",
                    checkCrossOrigin: !1
                };
            t["a"] = {
                created: function () {
                    this.initCrop$()
                },
                render: function () {
                    var e = this, t = arguments[0];
                    return t("div", {
                        class: "cropper-wrapper", on: {
                            click: function (t) {
                                t.target === t.currentTarget && e.$emit("cancel")
                            }
                        }
                    }, [t(i.a, {
                        ref: "cropper",
                        props: Object(n["a"])({}, Object(n["a"])({}, u, {
                            src: this.src,
                            crop: function (t) {
                                return e.crop$.next(t)
                            },
                            ready: function () {
                                return e.$refs.cropper.crop()
                            },
                            imgStyle: {
                                width: "auto",
                                height: this.maxHeight ? "auto" : "468px",
                                maxWidth: "100%",
                                maxHeight: "".concat(this.maxHeight, "px")
                            },
                            aspectRatio: this.cropRatio
                        }))
                    }), this.renderBottom])
                },
                data: function () {
                    return {crop$: null, height: 0, width: 0}
                },
                computed: {
                    isLeadform: e.compose(e.equals("form"), e.prop("channel")), cropRatio: function () {
                        return this.isLeadform ? 2 : 1
                    }, renderBottom: function () {
                        var e = this.$createElement;
                        return this.isLeadform ? e("span", {class: "size-display"}, ["".concat(this.width, " X ").concat(this.height, " ( 1:").concat(this.cropRatio, " )")]) : e("div", {class: "ratio-description"}, [e("p", [this.$t("IMAGE_TEMPLATE.crop_description")]), e("span", {class: "size-display"}, ["".concat(this.width, " X ").concat(this.height, " ( 1:").concat(this.cropRatio, " )")])])
                    }
                },
                methods: {
                    getDataURL: function () {
                        return this.width < 300 || this.height < 300 ? (this.$emit("error:size"), !1) : this.$refs.cropper.getCroppedCanvas().toDataURL({
                            maxWidth: 1e3,
                            maxHeight: 1e3
                        })
                    }, initCrop$: function () {
                        var t = this;
                        this.crop$ = (new r["a"]).pipe(Object(s["a"])(e.identity), Object(c["a"])("detail"), Object(l["a"])(e.pick(["width", "height"])), Object(l["a"])(e.mapObjIndexed(e.unary(Math.floor))), Object(d["a"])(e.equals)), this.$subscribeTo(this.crop$, function (e) {
                            var a = e.width, n = e.height;
                            t.width = a, t.height = n, t.$refs.cropper.setData({
                                width: a,
                                height: n
                            }), t.$emit("update:size", {width: a, height: n})
                        })
                    }
                },
                props: {
                    src: {type: String, default: ""},
                    maxHeight: {type: Number, required: !1},
                    channel: {type: String, required: !1}
                }
            }
        }).call(this, a("b17e"))
    }, a176: function (e, t, a) {
    }, a5f8: function (e, t, a) {
        "use strict";
        var n = a("e2ce"), o = a.n(n);
        o.a
    }, a772: function (e, t, a) {
        "use strict";
        (function (e) {
            a.d(t, "j", function () {
                return O
            }), a.d(t, "l", function () {
                return C
            }), a.d(t, "h", function () {
                return S
            }), a.d(t, "w", function () {
                return T
            }), a.d(t, "g", function () {
                return E
            }), a.d(t, "i", function () {
                return x
            }), a.d(t, "p", function () {
                return l
            }), a.d(t, "v", function () {
                return u
            }), a.d(t, "q", function () {
                return _
            }), a.d(t, "r", function () {
                return m
            }), a.d(t, "t", function () {
                return f
            }), a.d(t, "m", function () {
                return g
            }), a.d(t, "o", function () {
                return w
            }), a.d(t, "s", function () {
                return k
            }), a.d(t, "n", function () {
                return h
            }), a.d(t, "u", function () {
                return b
            }), a.d(t, "f", function () {
                return P
            }), a.d(t, "d", function () {
                return L
            }), a.d(t, "k", function () {
                return j
            }), a.d(t, "b", function () {
                return N
            }), a.d(t, "a", function () {
                return D
            }), a.d(t, "x", function () {
                return $
            }), a.d(t, "e", function () {
                return B
            }), a.d(t, "c", function () {
                return F
            });
            a("20d6");
            var n = a("cebc"), o = (a("7514"), a("b76c")), i = a("4c6b"), r = a("fa7d"), s = o["a"].all.default,
                c = e.defaultTo({}), l = e.compose(e.whereEq(i["h"]), c),
                d = e.compose(e.whereEq({channel: "facebook", creativeType: "carousel"}), c),
                u = e.compose(e.whereEq(i["o"]), c),
                p = e.compose(e.whereEq({channel: "instagram", creativeType: "carousel"}), c),
                _ = e.compose(e.whereEq(i["i"]), c), m = e.compose(e.whereEq(i["k"]), c),
                f = e.compose(e.whereEq(i["l"]), c), g = e.compose(e.whereEq(i["c"]), c),
                h = e.compose(e.propEq("channel", "facebook"), c), b = e.compose(e.propEq("channel", "instagram"), c),
                v = e.anyPass([u, l]), y = e.anyPass([p, d]), w = e.anyPass([h, b]),
                k = e.compose(e.propEq("channel", "google"), c);

            function x() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                t = e.over(e.lensProp("creativeId"), e.defaultTo(Object(r["o"])()), t);
                var a = e.over(e.lensPath(["creative", "callToAction"])), n = e.curry(function (t, a) {
                    return a || e.clone(t)
                });
                return t = e.cond([[v, a(n("LEARN_MORE"))], [y, e.over(e.lensPath(["creative", "cards"]), e.map(e.over(e.lensProp("callToAction"), n("NO_BUTTON"))))], [_, a(n("Learn More"))], [e.T, e.identity]])(t), t = e.over(e.lensProp("meta"), n({isEditing: !0}), t), t
            }

            function O(t) {
                var a = t || {}, n = a.channel, i = a.creativeType;
                return e.pathOr(s, [n, i], o["a"])
            }

            var A = e.compose(e.find(e.propEq("key", "media")), e.defaultTo([]));

            function C(t, a) {
                var n = A(O(t).params);
                if (n) return e.defaultTo([], e.defaultTo(e.always([]), n.validate).call(n, a))
            }

            function S() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return e.compose(function (e) {
                    return e(t.creative, a)
                }, e.propOr(e.always({value: !0}), "executable"), e.defaultTo({}, O))(t)
            }

            var E = e.compose(e.defaultTo([]), e.cond([[v, function () {
                return e.reject(e.propEq("value", "NO_BUTTON"), i["f"])
            }], [y, e.always(i["f"])], [k, e.always(i["j"])], [e.T, e.always([])]]), c);

            function T(t, a) {
                return t === a ? a : (t = e.clone(t), a = e.clone(a), e.compose(e.defaultTo(a), function (e) {
                    return e ? e(t, a) : a
                }, e.prop("updateFrom"), e.defaultTo({}, O))(a))
            }

            var P = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : e.identity,
                        a = arguments.length > 1 ? arguments[1] : void 0;
                    return w(a) ? function (a) {
                        return e.assoc("text", t(a.text), a)
                    } : _(a) ? e.compose(e.applySpec({
                        text: e.identity,
                        value: e.identity
                    }), t, e.concat("COMMON.cta_google_")) : e.identity
                }, I = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    return [t.appStoreUrl, t.playStoreUrl].map(e.compose(e.trim(), e.defaultTo("")))
                },
                L = e.compose(e.both(e.complement(e.isNil), e.complement(e.isEmpty)), e.filter(e.identity), e.flatten, e.map(e.cond([[y, e.pipe(e.path(["creative", "cards"]), e.map(I), e.flatten)], [e.T, e.pipe(e.prop("creative"), e.props(I))]])), e.defaultTo([]));

            function j(e) {
                var t = e.reason, a = void 0 === t ? {} : t, n = a.paramName;
                switch (a.type) {
                    case"channel":
                        return;
                    case"tooLong":
                        return this.$t("PROPOSAL.ads_should_be_less", {
                            paramName: this.$t("PROPOSAL.ad_config_google_display_description"),
                            max: a.maxLength
                        });
                    case"missing":
                        if ("media" === n) return this.$t("PROPOSAL.ads_media_missing");
                        if ("image" === n) return this.$t("PROPOSAL.ads_image_missing");
                        if ("description" === n || "mainDescription" === n) return this.$t("PROPOSAL.ads_description_missing");
                        if ("title1" === n || "title2" === n || "longTitle" === n || "shortTitle" === n) return this.$t("PROPOSAL.ads_title_missing");
                        if ("businessName" === n) return this.$t("PROPOSAL.ads_business_name_missing");
                        if ("facebook_page" === n) return;
                        return "".concat(a.paramName, " missing");
                    case"invalid":
                        if ("url" === a.paramName) return this.$t("PROPOSAL.ads_invalid_url");
                        if ("appUrl" === a.paramName) return this.$t("PROPOSAL.ads_invalid_url");
                        if ("media" === a.paramName) {
                            if (a.ratio) return this.$t("PROPOSAL.ads_invalid_video_ratio");
                            if (a.minWidth || a.minHeight) return this.$t("PROPOSAL.ads_invalid_min_dimension");
                            if (a.tooLong) return this.$t("PROPOSAL.ads_invalid_video_length", {value: a.tooLong})
                        }
                        break;
                    case"notEnoughCards":
                        return "Upload at least 3 images or videos to run ads in a slide"
                }
            }

            var M = e.compose(e.defaultTo({}), e.converge(e.zipObj, [e.map(e.prop("key")), e.map(e.always(""))]), e.filter(e.identity), e.defaultTo([])),
                R = e.curryN(3, function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                        a = arguments.length > 1 ? arguments[1] : void 0, i = a.channel, r = void 0 === i ? "" : i,
                        s = a.creativeType, c = void 0 === s ? "" : s;
                    if (r && c) {
                        var l = o["a"][r][c], d = l.adType, u = l.subChannels, p = l.getDefaultCreative,
                            _ = void 0 === p ? e.always({}) : p;
                        return x({
                            enabled: !0,
                            executable: {value: !1},
                            adType: d,
                            channel: r,
                            creativeType: c,
                            subChannels: u,
                            creative: Object(n["a"])({}, M(l.params), _.call(l, t), {meta: {}})
                        })
                    }
                });

            function N() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                return a.map(R(t)).filter(e.identity)
            }

            function D() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [], a = e.propEq("creativeId"),
                    n = e.findIndex(e.__, t);
                return t.map(function (e, t) {
                    var o = n(a(e.creativeId));
                    return o === t && e.creativeId || (e.creativeId = Object(r["o"])()), e
                }).filter(e.identity)
            }

            function $() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = t.type,
                    n = void 0 === a ? "" : a, o = t.websiteUrl, i = t.appStoreUrl, s = t.playStoreUrl;
                if (!n) return !1;
                var c = e.test(/web/gi), l = e.test(/app/gi);
                if (c(n)) {
                    if (!Object(r["M"])(o)) return !1
                } else if (l(n)) {
                    if (!i && !s) return !1;
                    if (i && !Object(r["I"])(i)) return !1;
                    if (s && !Object(r["L"])(s)) return !1
                }
                return !0
            }

            var B = function () {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], n = e.find(e.__, a);
                return t.map(function (t) {
                    return n(e.equals(t)) || n(e.propEq("channel", t.channel)) || t
                })
            }, F = function (t) {
                var a = t.type, n = void 0 === a ? "web" : a, o = t.appstoreAvailable, s = void 0 === o || o,
                    c = (t.user, t.channel), l = void 0 === c ? "" : c, d = [], u = [];
                e.test(/web|form/gi, n) ? (d = [i["h"], i["o"], i["k"], i["i"]], u = [i["h"], i["o"], i["k"]]) : e.test(/app/gi, n) && (d = [i["h"], i["o"], i["l"], s ? i["c"] : null], u = [i["h"], i["o"], i["l"], s ? i["c"] : null]);
                var p = l ? e.filter(e.propEq("channel", l)) : e.identity;
                return {options: Object(r["k"])(d), selected: e.compose(p, r["k"])(u)}
            }
        }).call(this, a("b17e"))
    }, a79c: function (e, t, a) {
        "use strict";
        (function (e) {
            a.d(t, "b", function () {
                return u
            }), a.d(t, "a", function () {
                return p
            });
            var n = a("75fc"), o = a("768b"), i = a("2d1f"), r = a.n(i), s = a("a745"), c = a.n(s), l = a("4d8c"),
                d = a.n(l), u = function (e) {
                    return "".concat("https://form.adriel.ai", "/").concat(e)
                };

            function p(e, t) {
                return c()(e) ? e.every(function (e) {
                    return t[e]
                }) : r()(e).every(function (e) {
                    var a = Object(o["a"])(e, 2), n = a[0], i = a[1];
                    return c()(i) ? i.every(function (e) {
                        return e(t[n])
                    }) : "function" == typeof i ? i(t[n]) : p(i[t[n]], t)
                })
            }

            var _ = function (e) {
                return e.indexOf("FORM") > -1 || !!e
            }, m = {
                type: {
                    title: {title: _},
                    description: {title: _},
                    short: {title: _},
                    checkbox: {title: _},
                    multiple: {title: _},
                    thanks: {title: _},
                    choice: {
                        choice: function (e) {
                            return !!e
                        }
                    }
                }
            }, f = e.addIndex(e.map);
            e.pipe(e.defaultTo([]), f(function (e, t) {
                if ("checkbox" == e.type || "multiple" == e.type) {
                    var a = [t, p(m, e)], o = f(function (e, a) {
                        return [t, a, p(m, e || [])]
                    }, e.choices);
                    return [a].concat(Object(n["a"])(o))
                }
                return [[t, p(m, e)]]
            }), d.a, e.reject(function (e) {
                var t = Object(o["a"])(e, 3), a = (t[0], t[1]), n = t[2];
                return "boolean" === typeof n ? n : a
            }))
        }).call(this, a("b17e"))
    }, abd2: function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "my-campaigns-wrapper"}, [a("img", {
                    staticClass: "fake-gif",
                    attrs: {src: "/fake-gif-for-safari-ios.gif"}
                }), a("b-modal", {
                    staticClass: "my-campaigns-survey-modal",
                    attrs: {"has-modal-card": "", active: e.isSurveyModalActive, canCancel: ["x"]},
                    on: {
                        "update:active": function (t) {
                            e.isSurveyModalActive = t
                        }
                    }
                }, [a("stop-survey", {
                    on: {
                        submit: e.submitStopSurvey, close: function (t) {
                            e.isSurveyModalActive = !1
                        }
                    }
                })], 1), e.isExtendDaysModalActive ? a("extend-days-modal", e._b({
                    attrs: {active: e.isExtendDaysModalActive},
                    on: {
                        "update:active": function (t) {
                            e.isExtendDaysModalActive = t
                        }, submit: e.extendCampaignDays
                    }
                }, "extend-days-modal", e.propsToRunningStatus, !1)) : e._e(), e.isExtendBudgetModalActive ? a("extend-budget-modal", {
                    attrs: {active: e.isExtendBudgetModalActive},
                    on: {
                        "update:active": function (t) {
                            e.isExtendBudgetModalActive = t
                        }, submit: e.extendCampaignBudget
                    }
                }) : e._e(), a("confirm-modal", {
                    attrs: {
                        active: e.isConfirmModalVisible,
                        text: e.confirmModaltext,
                        confirmText: e.confirmModalConfirmText,
                        confirmCb: e.confirmCb
                    }, on: {
                        "update:active": function (t) {
                            e.isConfirmModalVisible = t
                        }
                    }
                }), e.editingCampaign ? a("update-title", {
                    attrs: {campaign: e.editingCampaign},
                    on: {
                        complete: function (t) {
                            e.editingCampaign = null
                        }
                    }
                }) : e._e(), a("left-nav", {staticClass: "my-campaigns-left-nav"}), a("mobile-nav", {staticClass: "my-campaigns-mobile-nav"}), e.hasCampaigns ? a("div", {staticClass: "my-campaigns-body"}, [a("div", {staticClass: "my-campaigns-top"}, [a("top-title", {attrs: {main: e.$t("MY_CAMPAIGNS.top_title")}}, [a("i", {
                    staticClass: "far fa-folder fa-lg",
                    attrs: {slot: "icon"},
                    slot: "icon"
                })]), a("div", {staticClass: "my-campaigns-top-icons"}, [a("b-icon", {
                    class: {active: e.campaignListType === e.listType.CARD},
                    attrs: {pack: "fas", icon: "th-large", "custom-class": "top-icon"},
                    nativeOn: {
                        click: function (t) {
                            return function () {
                                return e.onClickListType(e.listType.CARD)
                            }()
                        }
                    }
                }), a("b-icon", {
                    class: {active: e.campaignListType === e.listType.TABLE},
                    attrs: {icon: "format-list-bulleted", "custom-class": "top-icon"},
                    nativeOn: {
                        click: function (t) {
                            return function () {
                                return e.onClickListType(e.listType.TABLE)
                            }()
                        }
                    }
                })], 1)], 1), a("div", [e.queryListType === e.listType.TABLE ? a("div", {staticClass: "my-campaigns-list"}, [a("campaign-list", {
                    ref: "list",
                    attrs: {
                        campaigns: e.campaigns,
                        totalData: e.totalData,
                        LIST_DEFAULT_PARAMS: e.listDefaultParams,
                        isRefresh: e.isRefresh,
                        validTabs: e.validTabsForTable
                    },
                    on: {
                        startChat: e.startChat,
                        goToExpress: e.goToExpress,
                        delete: e.ensureDelete,
                        copy: e.ensureCopy,
                        active: e.ensureToggleActive,
                        "click:title": e.showTitleModal,
                        "modal:shedule": e.showScheduleModal,
                        "modal:days": e.showDaysModal,
                        "modal:budget": e.showBudgetModal,
                        getCampaignsByParams: e.getCampaignsByParams
                    }
                })], 1) : e.queryListType === e.listType.CARD ? a("div", {staticClass: "my-campaigns-card"}, [a("campaign-box-wrapper", {
                    ref: "card",
                    attrs: {
                        campaigns: e.campaigns,
                        CARD_DEFAULT_PARAMS: e.cardDefaultParams,
                        startChat: e.startChat,
                        goToExpress: e.goToExpress,
                        ensureCopy: e.ensureCopy,
                        ensureDelete: e.ensureDelete,
                        ensureToggleActive: e.ensureToggleActive,
                        showTitleModal: e.showTitleModal,
                        showScheduleModal: e.showScheduleModal,
                        showDaysModal: e.showDaysModal,
                        showBudgetModal: e.showBudgetModal,
                        isLoading: e.isCardLoading,
                        isRefresh: e.isRefresh,
                        validTabs: e.validTabsForCard
                    },
                    on: {getCampaignsByParams: e.getCampaignsByParams}
                })], 1) : e._e()])]) : a("div", {staticClass: "my-campaigns-body"}, [a("div", {staticClass: "my-campaigns-top"}, [a("top-title", {attrs: {main: e.$t("MY_CAMPAIGNS.top_title")}})], 1), a("EmptyCampaign", {
                    attrs: {
                        startChat: e.startChat,
                        goToExpress: e.goToExpress
                    }
                })], 1)], 1)
            }, o = [], i = a("75c1"), r = i["a"], s = (a("1f5d"), a("b1df"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "c8ff1b90", null), l = c.exports, d = a("4e4d"), u = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "help-campaign-wrapper"}, [a("message-modal", {
                    attrs: {
                        text: e.modalMent,
                        active: e.isModalOpen
                    }, on: {
                        "update:active": function (t) {
                            e.isModalOpen = t
                        }, close: e.checkRedirect, btnClicked: e.checkRedirect
                    }
                }), a("left-nav", {staticClass: "left-nav"}), a("mobile-nav", {staticClass: "mobile-nav"}), a("div", {staticClass: "help-campaign-body"}, [a("top-title", {attrs: {main: e.$t("HELP.help_title")}}, [a("i", {
                    staticClass: "far fa-envelope fa-lg",
                    attrs: {slot: "icon"},
                    slot: "icon"
                })]), a("div", {staticClass: "help-campaign-contents"}, [a("div", {staticClass: "help-form help-campaign-box"}, [a("b-select", {
                    class: {"campaign-selected": !!e.campaign_id},
                    attrs: {placeholder: e.$t("HELP.select_placeholder"), expanded: ""},
                    model: {
                        value: e.campaign_id, callback: function (t) {
                            e.campaign_id = t
                        }, expression: "campaign_id"
                    }
                }, e._l(e.campaigns, function (t) {
                    return a("option", {key: t.id, domProps: {value: t.id}}, [e._v(e._s(t.title))])
                }), 0), a("input", {
                    directives: [{name: "model", rawName: "v-model", value: e.title, expression: "title"}],
                    staticClass: "help-form-title",
                    attrs: {type: "text", placeholder: e.$t("HELP.title")},
                    domProps: {value: e.title},
                    on: {
                        input: function (t) {
                            t.target.composing || (e.title = t.target.value)
                        }
                    }
                }), a("textarea", {
                    directives: [{name: "model", rawName: "v-model", value: e.text, expression: "text"}],
                    staticClass: "help-form-content",
                    attrs: {placeholder: e.$t("HELP.text")},
                    domProps: {value: e.text},
                    on: {
                        input: function (t) {
                            t.target.composing || (e.text = t.target.value)
                        }
                    }
                }), a("button", {
                    staticClass: "help-submit",
                    attrs: {disabled: e.sending},
                    on: {click: e.sendHelp}
                }, [e._v("\n                    " + e._s(e.$t("COMMON.submit")) + "\n                    "), a("img", {attrs: {src: "/img/submit_icon.png"}})])], 1), a("div", {staticClass: "help-info help-campaign-box"}, [a("img", {attrs: {src: "/img/logos/logo_n_s.svg"}}), a("span", {staticClass: "help-info-title"}, [e._v(e._s(e.$t("HELP.we_here")))]), a("span", {
                    staticClass: "help-info-content",
                    domProps: {innerHTML: e._s(e.$t("HELP.we_here_content"))}
                }), e._m(0)])])], 1)], 1)
            }, p = [function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "help-info-contact"}, [a("span", {staticClass: "hr-line"}), a("span", {staticClass: "help-info-contact-detail"}, [a("a", {attrs: {href: "mailto:support@adriel.ai"}}, [e._v("support@adriel.ai")])])])
            }], _ = a("c671"), m = _["a"], f = (a("708a"), a("43de"), Object(s["a"])(m, u, p, !1, null, "094ebcab", null)),
            g = f.exports, h = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "BillingHistory"}, [a("left-nav", {staticClass: "left-nav"}), a("mobile-nav", {staticClass: "mobile-nav"}), a("div", {staticClass: "container"}, [a("div", {staticClass: "billing--title"}, [e._v(e._s(e.$t("BILLING_HISTORY.top_title")))]), a("div", {
                    staticClass: "billing--for",
                    domProps: {innerHTML: e._s(e.$t("BILLING_HISTORY.invoice_for", {email: e.email}))}
                }), a("div", {}, [a("div", {staticClass: "billing--info email"}, [a("span", {staticClass: "head"}, [e._v(e._s(e.$t("BILLING_HISTORY.invoice_info_email")))]), a("span", {staticClass: "tail"}, [e._v(e._s(e.email))])])]), e.transactions.length > 0 ? a("div", {staticClass: "billing--info excel"}, [a("a", {
                    staticClass: "excel-link",
                    attrs: {href: e.excelLink}
                }, [a("img", {attrs: {src: "/img/icons/dashboard_excel_icon.png"}}), a("span", [e._v(e._s(e.$t("DASHBOARD.download_excel")))])])]) : e._e(), a("div", {staticStyle: {}}), e.transactions.length > 0 ? a("div", {staticClass: "billing--table"}, [a("div", {staticClass: "head"}, [a("span", [e._v(e._s(e.$t("COMMON.date")))]), a("span", [e._v(e._s(e.$t("COMMON.campaign_id")))]), a("span", {staticClass: "campaign_title"}, [e._v("\n                    " + e._s(e.$t("COMMON.campaign_title")) + "\n                    "), a("v-popover", {
                    staticClass: "billing-popover",
                    attrs: {placement: "bottom", trigger: "click", popoverClass: "billing-popover-wrapper"}
                }, [a("img", {attrs: {src: "/img/icons/billing_filter_icon.png"}}), a("div", {
                    staticClass: "billing-popover-contents",
                    attrs: {slot: "popover"},
                    slot: "popover"
                }, [a("span", {
                    staticClass: "filter-each all", on: {
                        click: function (t) {
                            return e.selectAllIds(!e.isAllSelected)
                        }
                    }
                }, [a("check-box", {attrs: {value: e.isAllSelected}}), a("span", {staticClass: "name"}, [e._v("ALL")])], 1), a("div", {staticClass: "filter-each-container"}, e._l(e.campaigns, function (t) {
                    return a("span", {
                        key: t.id, staticClass: "filter-each", on: {
                            click: function (a) {
                                return e.toggleSelectedId(t.id)
                            }
                        }
                    }, [a("check-box", {attrs: {value: e.isIdSelected(t.id)}}), a("span", {staticClass: "name adriel-ellipsis"}, [e._v(e._s(t.name))])], 1)
                }), 0)])])], 1), a("span", [e._v(e._s(e.$t("COMMON.amount")))]), a("span", [e._v(e._s(e.$t("BILLING_HISTORY.payment_status")))]), a("span", [e._v(e._s(e.$t("COMMON.payment_method")))])]), a("div", {staticClass: "head mobile-head"}, [a("v-popover", {
                    staticClass: "billing-popover",
                    attrs: {placement: "auto", trigger: "click", popoverClass: "billing-popover-wrapper"}
                }, [a("img", {attrs: {src: "/img/icons/billing_filter_icon.png"}}), a("div", {
                    staticClass: "billing-popover-contents",
                    attrs: {slot: "popover"},
                    slot: "popover"
                }, [a("span", {
                    staticClass: "filter-each all", on: {
                        click: function (t) {
                            return e.selectAllIds(!e.isAllSelected)
                        }
                    }
                }, [a("check-box", {attrs: {value: e.isAllSelected}}), a("span", {staticClass: "name"}, [e._v("ALL")])], 1), a("div", {staticClass: "filter-each-container"}, e._l(e.campaigns, function (t) {
                    return a("span", {
                        key: t.id, staticClass: "filter-each", on: {
                            click: function (a) {
                                return e.toggleSelectedId(t.id)
                            }
                        }
                    }, [a("check-box", {attrs: {value: e.isIdSelected(t.id)}}), a("span", {staticClass: "name adriel-ellipsis"}, [e._v(e._s(t.name))])], 1)
                }), 0)])])], 1), a("div", {staticClass: "body"}, e._l(e._transactions, function (t, n) {
                    return a("div", {key: n, staticClass: "billing-row"}, [a("div", {
                        staticClass: "row-detail",
                        on: {
                            click: function (a) {
                                return e.toggleExpanded(t)
                            }
                        }
                    }, [a("span", {staticClass: "row-head"}, [a("span", {staticClass: "cell row-head--1"}, [e._v(e._s(e.formatDate(t.updated_at)))]), a("span", {staticClass: "cell row-head--2"}, [e._v(e._s(t.campaign_id))]), a("span", {staticClass: "cell row-head--3 word-break"}, [e._v(e._s(t.campaign_title))])]), a("span", {staticClass: "row-tail"}, [a("span", {
                        staticClass: "cell row-tail--1",
                        class: "amount-" + t.status
                    }, [e._v(e._s(e.formatCurrency(t.amount)))]), a("span", {staticClass: "cell row-tail--2"}, [a("span", {
                        staticClass: "status",
                        class: "status-" + t.status
                    }, [e._v(e._s(e.$t("BILLING_HISTORY." + t.status)))])]), a("span", {staticClass: "cell row-tail--3"}, [e._v(e._s(e.$t("BILLING_HISTORY." + t.payment_method)))])])]), e.expanded === t ? a("div", {staticClass: "row-expanded"}, [a("div", {staticClass: "expanded-head"}, [a("span", {staticClass: "expanded-head--title"}, [e._v(e._s(t.campaign_title))]), a("span", {staticClass: "expanded-head--status"}, [e._v(e._s(t.campaign_status))]), a("span", {staticClass: "expanded-head--fee"}, [e._v(e._s(e.$t("BILLING_HISTORY.service_fee", {fee: t.service_fee})))]), a("span", {staticClass: "expanded-head--price"}, [e._v(e._s(e.$t("BILLING_HISTORY.service_fee_include_exclude")))])]), a("div", {staticClass: "expanded-body"}, [a("div", {staticClass: "expanded-body--row"}, [a("span", {staticClass: "head word-break"}, [e._v(e._s(e.$t("BILLING_HISTORY.amount_this_time")))]), a("span", {staticClass: "tail"}, [a("span", [e._v(e._s(e.formatCurrency(t.amount)))])])]), a("div", {staticClass: "expanded-body--row"}, [a("span", {staticClass: "head word-break"}, [e._v(e._s(e.$t("BILLING_HISTORY.amount_other_time")))]), a("span", {staticClass: "tail"}, [a("span", [e._v(e._s(e.formatCurrency(t.total_paid - t.amount)))])])]), a("div", {staticClass: "expanded-body--row"}, [a("span", {staticClass: "head word-break"}, [e._v(e._s(e.$t("BILLING_HISTORY.amount_total")))]), a("span", {staticClass: "tail"}, [a("span", [e._v(e._s(e.formatCurrency(t.total_paid)))])])])])]) : e._e()])
                }), 0)]) : a("div", {
                    staticClass: "has-text-centered",
                    style: {"margin-top": "50px"}
                }, [e._v(e._s(e.$t("BILLING_HISTORY.no_invoices")))])])], 1)
            }, b = [], v = a("ad31"), y = v["a"], w = (a("847b"), Object(s["a"])(y, h, b, !1, null, null, null)),
            k = w.exports, x = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "page-not-found"}, [a("div", {staticClass: "page-not-found-contents"}, [a("img", {attrs: {src: "/img/page_not_found.png"}}), a("span", {staticClass: "not-found-title"}, [e._v(e._s(e.$t("NOT_FOUND.oops")))]), a("span", {staticClass: "not-found-content"}, [e._v(e._s(e.$t("NOT_FOUND.can_not_find")))]), a("router-link", {
                    attrs: {
                        to: "/",
                        tag: "button"
                    }
                }, [e._v(e._s(e.$t("NOT_FOUND.back_home")))])], 1)])
            }, O = [], A = (a("2c40"), {}), C = Object(s["a"])(A, x, O, !1, null, "72d63234", null), S = C.exports,
            E = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", [a("cropper", {attrs: {src: "https://demo.adriel.ai/api/images/clients/35/a6ca3460-5c1c-11e9-a360-010ede89d655/img-22143b1Na6GcOHe2t.jpg"}})], 1)
            }, T = [], P = a("9f84"), I = {
                created: function () {
                }, components: {Cropper: P["a"]}, data: function () {
                    return {}
                }, methods: {}
            }, L = I, j = Object(s["a"])(L, E, T, !1, null, null, null), M = j.exports, R = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", [e._v("Loading demo...")])
            }, N = [], D = {
                name: "DemoRedirection", methods: {
                    redirectToDemo: function () {
                        var e = "https://app.adriel.ai", t = "demo_us@adriel.ai", a = "demoadriel";
                        if ("ko" === this.language && (t = "demo_ko@adriel.ai"), navigator) {
                            var n = navigator.language;
                            "en-GB" !== n && "en-gb" !== n || (t = "demo_en@adriel.ai")
                        }
                        $.ajax({
                            method: "POST",
                            url: e + "/api/auth/auth",
                            data: {email: t, password: a},
                            xhrFields: {withCredentials: !0}
                        }).done(function (t, a) {
                            window.location.href = e
                        })
                    }
                }, mounted: function () {
                    this.redirectToDemo()
                }
            }, B = D, F = Object(s["a"])(B, R, N, !1, null, "5c255c49", null), G = F.exports, U = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "after-launch-campaign"}, [a("mobile-nav", {staticClass: "mobile-nav"}), a("left-nav", {staticClass: "left-nav"}), a("div", {staticClass: "after-launch-campaign-body"}, [a("span", {staticClass: "review-title"}, [e._v(e._s(e.$t("LAUNCHED.in_review")))]), a("img", {attrs: {src: "/img/inreview_icon.png"}}), a("span", {staticClass: "review-congratulations"}, [e._v("\n            " + e._s(e.$t("LAUNCHED.congratulations")) + "\n        ")]), a("span", {
                    staticClass: "review-info word-break",
                    domProps: {innerHTML: e._s(e.$t("LAUNCHED.contents"))}
                }), a("router-link", {
                    attrs: {
                        to: "/myCampaigns",
                        tag: "button"
                    }
                }, [e._v("\n            " + e._s(e.$t("LAUNCHED.go_to_my")) + "\n        ")])], 1)], 1)
            }, q = [], Y = a("b183"), V = {components: {"left-nav": d["a"], MobileNav: Y["a"]}}, z = V,
            W = (a("2a79"), a("4a03"), Object(s["a"])(z, U, q, !1, null, "779d60be", null)), H = W.exports,
            K = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "after-launch-campaign"}, [a("mobile-nav", {staticClass: "mobile-nav"}), a("left-nav", {staticClass: "left-nav"}), a("div", {staticClass: "after-launch-campaign-body"}, [a("span", {staticClass: "review-title"}, [e._v("In review")]), a("img", {attrs: {src: "/img/inreview_icon.png"}}), a("span", {staticClass: "review-congratulations"}, [e._v("Great! Your part is done.")]), e._m(0), a("router-link", {
                    attrs: {
                        to: "/myCampaigns",
                        tag: "button"
                    }
                }, [e._v("\n            Go to My campaigns\n        ")])], 1)], 1)
            }, J = [function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("span", {staticClass: "review-info"}, [e._v("\n            Your campaign is being reviewed."), a("br"), e._v("\n            You'll be able to see Adriel's proposal as soon as our internal review is done."), a("br")])
            }], Q = {
                created: function () {
                }, components: {"left-nav": d["a"], MobileNav: Y["a"]}
            }, X = Q, Z = (a("b6e8"), a("a5f8"), Object(s["a"])(X, K, J, !1, null, "f48da166", null)), ee = Z.exports;
        a.d(t, "g", function () {
            return l
        }), a.d(t, "e", function () {
            return g
        }), a.d(t, "a", function () {
            return H
        }), a.d(t, "b", function () {
            return k
        }), a.d(t, "h", function () {
            return S
        }), a.d(t, "d", function () {
            return M
        }), a.d(t, "f", function () {
            return ee
        }), a.d(t, "c", function () {
            return G
        })
    }, ac9e: function (e, t, a) {
        "use strict";
        var n = a("9133"), o = a.n(n);
        o.a
    }, ad31: function (e, t, a) {
        "use strict";
        (function (e) {
            a("20d6"), a("96cf");
            var n = a("3b8d"), o = (a("c5f6"), a("75fc")), i = a("c1df"), r = a.n(i), s = a("608d"), c = a("19e8"),
                l = a("170a"), d = a("4e4d"), u = a("b183"), p = a("dde5"),
                _ = (a("0a52"), "".concat("https://app.adriel.ai/api", "/transactions/downloadInvoiceExcel"));
            t["a"] = {
                name: "BillingHistory",
                components: {LeftNav: d["a"], Title: s["a"], Card: c["a"], MobileNav: u["a"], CheckBox: l["a"]},
                data: function () {
                    return {transactions: [], selectedIds: [], expanded: null, excelLink: _}
                },
                computed: {
                    email: e.path(["globalUser", "email"]), _transactions: function () {
                        return this.transactions.filter(e.propSatisfies(e.contains(e.__, this.selectedIds), "campaign_id"))
                    }, campaigns: function () {
                        return e.pipe(e.prop("transactions"), e.reduce(function (e, t) {
                            return [].concat(Object(o["a"])(e), [{id: Number(t.campaign_id), name: t.campaign_title}])
                        }, []), e.uniqBy(e.prop("id")), e.sortWith([e.descend(e.prop("id"))]))(this)
                    }, isAllSelected: function () {
                        return this.selectedIds.length === this.campaigns.length
                    }
                },
                methods: {
                    fetchTransactions: function () {
                        var e = Object(n["a"])(regeneratorRuntime.mark(function e() {
                            return regeneratorRuntime.wrap(function (e) {
                                while (1) switch (e.prev = e.next) {
                                    case 0:
                                        return e.next = 2, p["n"].query();
                                    case 2:
                                        this.transactions = e.sent, this.selectAllIds(!0);
                                    case 4:
                                    case"end":
                                        return e.stop()
                                }
                            }, e, this)
                        }));

                        function t() {
                            return e.apply(this, arguments)
                        }

                        return t
                    }(), formatDate: function (e) {
                        return r()(e).format("YYYY-MM-DD")
                    }, toggleSelectedId: function (t) {
                        var a = this.selectedIds.findIndex(e.equals(t));
                        a > -1 ? (this.selectedIds.splice(a, 1), this.selectedIds.length || this.selectAllIds(!0)) : this.selectedIds.push(t)
                    }, selectAllIds: function () {
                        var t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
                        this.selectedIds = !0 === t ? e.compose(e.uniq, e.pluck("campaign_id"))(this.transactions) : []
                    }, isIdSelected: function (t) {
                        return e.contains(t, this.selectedIds)
                    }, toggleExpanded: function (e) {
                        this.expanded === e ? this.expanded = null : this.expanded = e
                    }
                },
                mounted: function () {
                    this.fetchTransactions()
                }
            }
        }).call(this, a("b17e"))
    }, b012: function (e, t, a) {
        "use strict";
        (function (e) {
            a("96cf");
            var n = a("3b8d"), o = a("d3fb"), i = a("1585"), r = function (t) {
                return t.then(e.prop("data"))
            }, s = "/auth";

            function c(e) {
                return l.apply(this, arguments)
            }

            function l() {
                return l = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                    var a, n, o, i, r, c, l, d, u, p;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return a = t.email, n = t.password, o = t.googleAnalyticsId, i = t.facebookPage, t.facebookLongLifeToken, r = t.token, c = t.referral, l = t.websiteUrl, d = t.language, u = t.timezone, e.next = 3, axios.post("".concat(s, "/register"), {
                                    email: a,
                                    password: n,
                                    googleAnalyticsId: o,
                                    facebookPage: i,
                                    token: r,
                                    referral: c,
                                    websiteUrl: l,
                                    language: d,
                                    timezone: u
                                });
                            case 3:
                                return p = e.sent, e.abrupt("return", p.data);
                            case 5:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), l.apply(this, arguments)
            }

            function d(e, t) {
                return u.apply(this, arguments)
            }

            function u() {
                return u = Object(n["a"])(regeneratorRuntime.mark(function e(t, a) {
                    var n;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.post("".concat(s, "/auth"), {email: t, password: a});
                            case 2:
                                return n = e.sent, e.abrupt("return", n.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), u.apply(this, arguments)
            }

            var p = function () {
                return Object(o["a"])(axios.get("".concat(s, "/logout"))).pipe(Object(i["a"])("data")).toPromise()
            }, _ = function () {
                return Object(o["a"])(axios.get("".concat(s, "/user"))).pipe(Object(i["a"])("data")).toPromise()
            }, m = e.compose(r, function (e) {
                return axios.post("".concat(s, "/forgot"), {email: e})
            }), f = e.compose(r, function (e, t) {
                return axios.post("".concat(s, "/reset"), {token: e, password: t})
            });

            function g(e, t) {
                return h.apply(this, arguments)
            }

            function h() {
                return h = Object(n["a"])(regeneratorRuntime.mark(function e(t, a) {
                    var n;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.put("".concat(s, "/password"), {password: t, newPassword: a});
                            case 2:
                                return n = e.sent, e.abrupt("return", n.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), h.apply(this, arguments)
            }

            function b(e) {
                return v.apply(this, arguments)
            }

            function v() {
                return v = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                    var a;
                    return regeneratorRuntime.wrap(function (e) {
                        while (1) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, axios.get("".concat(s, "/verify"), {params: {token: t}});
                            case 2:
                                return a = e.sent, e.abrupt("return", a.data);
                            case 4:
                            case"end":
                                return e.stop()
                        }
                    }, e)
                })), v.apply(this, arguments)
            }

            t["a"] = {
                register: c,
                login: d,
                logout: p,
                session: _,
                forgot: m,
                reset: f,
                updatePassword: g,
                verifyEmail: b
            }
        }).call(this, a("b17e"))
    }, b183: function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "nav"}, [a("div", {staticClass: "nav__container"}, [a("transition", {attrs: {name: "menu-show"}}, [a("mobile-menu", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: e.isMobileMenuShown,
                        expression: "isMobileMenuShown"
                    }],
                    attrs: {"is-mobile-menu-shown": e.isMobileMenuShown},
                    on: {onMobileMenuClose: e.toggleMobileMenuShown}
                })], 1), a("div", {staticClass: "nav__left"}, [a("a", {
                    attrs: {
                        href: e.landingPage,
                        target: "_blank"
                    }
                }, [a("img", {attrs: {src: "/img/logos/logo_n_f.svg"}})])]), a("div", {staticClass: "nav__right"}, [a("button", {on: {click: e.toggleMobileMenuShown}}, [a("div", {class: ["menu-button", {active: e.isMobileMenuShown}]})])])], 1)])
            }, o = [], i = a("cebc"), r = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {staticClass: "menu"}, [a("div", {staticClass: "menu__container"}, [a("div", {staticClass: "menu__content"}, [a("ul", {staticClass: "menu__list"}, [e._l(e.menus, function (t) {
                    return a("router-link", {
                        key: t.name,
                        class: "menu__button menu__" + t.name,
                        attrs: {
                            tag: "li",
                            to: t.to,
                            event: "campaign" === t.name && "MyCampaigns" === e.$route.name ? "" : "click"
                        },
                        nativeOn: {
                            click: function (t) {
                                return e.onClickHandler("ACTION_ROUTER_CLICK")
                            }
                        }
                    }, [a("span", {staticClass: "menu__icon"}, [a("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: e.getIcon(t)
                        }
                    })], 1), a("span", {staticClass: "menu__text"}, [e._v(e._s(e.$t(t.label)))])])
                }), a("div", {
                    staticClass: "box-new-mobile", on: {
                        click: function (t) {
                            return e.onClickHandler("ACTION_NEW_CAMPAIGN")
                        }
                    }
                }, [a("i", {staticClass: "fal fa-plus"}), a("span", {staticClass: "box-new-mobile__text"}, [e._v(e._s(e.$t("LEFT_NAV.new_campaign")))])]), a("div", {
                    staticClass: "box-new-mobile express",
                    on: {
                        click: function (t) {
                            return e.onClickHandler("ACTION_EXPRESS")
                        }
                    }
                }, [a("b-icon", {
                    style: {color: "#ffffff", "font-size": "13px"},
                    attrs: {pack: "far", icon: "magic"}
                }), a("span", {staticClass: "box-new-mobile__text"}, [e._v(e._s(e.$t("LEFT_NAV.express")))])], 1), e.isLangType("ko") ? a("div", {staticClass: "box-new-mobile blog"}, [e._m(0)]) : e._e()], 2)]), a("div", {staticClass: "menu__bottom"}, [a("span", {
                    staticClass: "menu__logout",
                    on: {
                        click: function (t) {
                            return e.onClickHandler("ACTION_LOGOUT")
                        }
                    }
                }, [a("b-icon", {style: {color: "#666666"}, attrs: {pack: "far", icon: "sign-out"}})], 1)])])])
            }, s = [function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("a", {
                    staticClass: "box-new-mobile__text",
                    attrs: {href: "https://blog.naver.com/adrielai", target: "_blank"}
                }, [a("img", {attrs: {src: "/img/icons/left_nav_blog.png"}}), e._v("\n                        �꾨뱶由ъ뿕 釉붾줈洹� 蹂닿린\n                    ")])
            }], c = a("237c"), l = c["a"], d = (a("eeaa"), a("2877")),
            u = Object(d["a"])(l, r, s, !1, null, "7806b66a", null), p = u.exports, _ = a("2f62"), m = a("fa7d"), f = {
                components: {"mobile-menu": p}, data: function () {
                    return {isMobileMenuShown: !1}
                }, methods: {
                    toggleMobileMenuShown: function () {
                        this.isMobileMenuShown ? (this.isMobileMenuShown = !1, this.$intercom.update({hide_default_launcher: !1})) : (this.$intercom.update({hide_default_launcher: !0}), this.isMobileMenuShown = !0)
                    }, onMobileMenuClose: function (e) {
                        this.isMobileMenuShown = e
                    }
                }, beforeDestroy: function () {
                    this.$intercom.update({hide_default_launcher: !1})
                }, computed: Object(i["a"])({}, Object(_["mapGetters"])("user", ["user"]), {
                    landingPage: function () {
                        return "ko" === Object(m["w"])(this.user) ? "https://adriel.ai/kr" : "https://adriel.ai/en"
                    }
                })
            }, g = f, h = (a("02e4"), Object(d["a"])(g, n, o, !1, null, "060085cd", null));
        t["a"] = h.exports
    }, b1db: function (e, t, a) {
        "use strict";
        (function (e) {
            a("c5f6");
            var n = a("6f9a"), o = (a("fa7d"), ["3", "7", "30", "custom"]);
            t["a"] = {
                created: function () {
                },
                props: ["deactivated", "daysLeft", "asap", "startDate", "endDate", "continuously", "startedAt", "deactivatedAt", "active"],
                components: {"running-status": n["a"]},
                data: function () {
                    return {options: o, selected: 7, inputValue: ""}
                },
                computed: {
                    _active: {
                        get: e.prop("active"), set: function (e) {
                            this.$emit("update:active", e)
                        }
                    }, _btnText: function () {
                        return this.$t("COMMON.publish")
                    }
                },
                methods: {
                    confirm: function () {
                        this._active = !1;
                        var e = "custom" === this.selected ? this.inputValue : this.selected;
                        e = Math.max(0, Number(e)), this.$emit("submit", e)
                    }, close: function () {
                        this._active = !1, this.$emit("close")
                    }
                }
            }
        }).call(this, a("b17e"))
    }, b1df: function (e, t, a) {
        "use strict";
        var n = a("1097"), o = a.n(n);
        o.a
    }, b414: function (e, t, a) {
        "use strict";
        (function (e) {
            var a = "/myManager";

            function n(t) {
                var n = t.type, o = void 0 === n ? "user" : n, i = t.id, r = void 0 === i ? 0 : i, s = t.block,
                    c = void 0 === s ? 0 : s, l = t.startDate, d = void 0 === l ? "" : l, u = t.endDate,
                    p = void 0 === u ? "" : u, _ = "".concat(a, "/").concat(o, "/").concat(r);
                return axios.get(_, {params: {block: c, startDate: d || null, endDate: p || null}}).then(e.prop("data"))
            }

            function o(t) {
                return axios.get("/intercom/conversation/".concat(t)).then(e.prop("data"))
            }

            function i(t) {
                var n = t.lvl, o = t.id;
                return axios.get("".concat(a, "/").concat(n, "/").concat(o)).then(e.prop("data"))
            }

            function r(e) {
                return axios.post("".concat(a, "/threads/").concat(e.feedId), e).then(function (e) {
                    return e.data
                })
            }

            function s(t) {
                var n = t.campaignId, o = t.page, i = t.startDate, r = t.endDate, s = t.userId,
                    c = "".concat(a, "/actions/campaigns");
                return n && (c += "/".concat(n)), axios.get(c, {
                    params: {
                        page: o,
                        userId: s || null,
                        startDate: i || null,
                        endDate: r || null,
                        size: 10
                    }
                }).then(e.prop("data"))
            }

            function c(t) {
                var n = t.recoId, o = t.userId, i = t.feedId;
                return axios.get("".concat(a, "/actions/recommendation/").concat(n), {
                    params: {
                        userId: o || null,
                        feedId: i
                    }
                }).then(e.prop("data"))
            }

            function l(e) {
                var t = e.actionId, n = "".concat(a, "/actions");
                return t && (n += "/".concat(t)), axios.post(n)
            }

            function d(t) {
                var n = t.type, o = t.userId;
                return axios.get("".concat(a, "/campaignlist"), {params: {type: n, userId: o}}).then(e.prop("data"))
            }

            function u(t) {
                var a = t.userId;
                return axios.get("/myManager/actions/total", {params: {userId: a}}).then(e.prop("data"))
            }

            function p(e) {
                return axios.delete("/myManager/actions/".concat(e))
            }

            t["a"] = {
                getNewsfeed: n,
                getActionList: s,
                getIntercom: o,
                getFeedMessage: i,
                createFeedMessage: r,
                postActionState: l,
                getCampaignSummaries: d,
                getActionRequiredCount: u,
                deleteActionRequired: p,
                getActionItem: c
            }
        }).call(this, a("b17e"))
    }, b417: function (e, t, a) {
    }, b55e: function (e, t, a) {
    }, b6a0: function (e, t, a) {
    }, b6e8: function (e, t, a) {
        "use strict";
        var n = a("001c"), o = a.n(n);
        o.a
    }, b76c: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("75fc"), o = a("2d1f"), i = a.n(o), r = a("768b"), s = a("cebc"), c = a("5176"), l = a.n(c),
                d = a("15b8"), u = a.n(d), p = (a("ac6a"), a("fa7d")), _ = a("4c6b"), m = a("a772"), f = a("5d07"),
                g = f["adText"].cutText, h = {
                    SIZE: "PROPOSAL.ads_invalid_min_dimension",
                    RATIO: "PROPOSAL.ads_invalid_video_ratio",
                    DURATION: {key: "PROPOSAL.ads_invalid_video_length", value: {value: 60}}
                }, b = function (e) {
                    var t = {};
                    return this.params.forEach(function (a) {
                        var n = a.key;
                        t[n] = "businessName" === n ? e : ""
                    }), t
                }, v = u()({
                    recommendedVideoLength: {value: 15, unit: "seconds"},
                    recommendedVideoRatio: {h: 4, v: 5},
                    videoSound: "Enabled with captions included",
                    recommendedVideoFormat: [".mp4", ".mov"],
                    videoResolution: {value: 600, unit: "px"},
                    videoMaxFileSize: {value: 4, unit: "GB"},
                    maxVideoLength: {value: 1, unit: "min"}
                }), y = u()({key: "callToAction", name: "COMMON.call_to_action_title", type: "dropdown"}),
                w = function (t) {
                    var a = e.defaultTo("");
                    l()(t, {creativeType: "carousel"});
                    var n = e.clone(t.creative), o = a(n.description), c = a(n.url), d = n.callToAction,
                        u = e.compose(e.concat(["meta"]), e.pluck("key"), e.reject(e.propEq("common", !0)), e.prop("params"), m["j"])(t),
                        p = e.pickAll(u, n), _ = {
                            mainDescription: o,
                            mainUrl: c,
                            cards: [Object(s["a"])({}, e.clone(p), {
                                callToAction: d,
                                urlType: n.urlType,
                                url: n.url
                            }), Object(s["a"])({}, e.clone(p), {callToAction: d, url: n.url})]
                        }, f = e.last(_.cards),
                        g = e.contains(e.__, ["options", "description", "callToAction", "url", "title", "appStoreUrl", "playStoreUrl", "webUrl", "formUrl"]);
                    return e.compose(e.forEach(function (e) {
                        var t = Object(r["a"])(e, 1), a = t[0];
                        return f[a] = ""
                    }), e.reject(e.propSatisfies(g, "0")), i.a)(f), _.cards.forEach(function (t) {
                        e.compose(e.forEach(function (e) {
                            var a = Object(r["a"])(e, 1), n = a[0];
                            return t[n] = ""
                        }), e.filter(e.propEq("1", void 0)), i.a)(t)
                    }), t.creative = _, t
                }, k = function (t) {
                    var a = t.creative.cards, n = e.compose(e.clone, e.last)(a),
                        o = e.contains(e.__, ["options", "callToAction", "title", "url", "description", "appStoreUrl", "playStoreUrl", "webUrl", "formUrl"]);
                    return e.compose(e.forEach(function (e) {
                        var t = Object(r["a"])(e, 1), a = t[0];
                        return n[a] = ""
                    }), e.reject(e.propSatisfies(o, "0")), i.a)(n), n.url = n.url || t.creative.mainUrl, a.push(n), t
                }, x = function (t, a) {
                    var n = t.creative.cards;
                    if (n.splice(a, 1), 1 !== n.length) return t;
                    l()(t, {creativeType: "photo"});
                    var o = e.compose(e.concat(["meta", "url"]), e.pluck("key"), e.prop("params"), m["j"])(t),
                        c = e.pickAll(o, n[0]);
                    return e.compose(e.forEach(function (e) {
                        var t = Object(r["a"])(e, 1), a = t[0];
                        return c[a] = ""
                    }), e.filter(e.propEq("1", void 0)), i.a, e.pickAll(e.without(["mainDescription", "mainUrl"], o)))(c), l()(t, {
                        creative: Object(s["a"])({}, c, {description: t.creative.mainDescription || ""}),
                        callToAction: n[0].callToAction
                    }), t
                }, O = function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                        t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                    return t.reduce(function (t, a) {
                        return e[a] ? t : t.concat({type: "missing", paramName: a})
                    }, [])
                }, A = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                        a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, n = a.strictValidation,
                        o = void 0 !== n && n, i = a.type, r = void 0 === i ? "WEB" : i, s = [];
                    if (!0 === o) {
                        var c = e.assoc("paramName", e.__, {type: "invalid"});
                        if ("WEB" === r) {
                            var l = t.url;
                            Object(p["M"])(l) || s.push(c("url"))
                        } else if ("APP" === r) {
                            var d = t.appStoreUrl, u = t.playStoreUrl;
                            Object(p["I"])(d) || s.push(c("appStoreUrl")), Object(p["L"])(u) || s.push(c("playStoreUrl"))
                        }
                    }
                    return s
                }, C = function (t) {
                    return Object(p["N"])(t) ? e.assoc("reasons", t, {value: !1}) : {value: !0}
                }, S = u()([{key: "webUrl", type: "input", title: "PROPOSAL.ad_config_web_url"}, {
                    key: "appStoreUrl",
                    title: "PROPOSAL.ad_config_app_url",
                    name: "PROPOSAL.ad_config_appstore_url",
                    type: "input",
                    slot: "app"
                }, {key: "playStoreUrl", name: "PROPOSAL.ad_config_playstore_url", type: "input"}, {
                    type: "input",
                    name: "PROPOSAL.ad_config_web_url_for_app",
                    updateKey: "webUrl",
                    optional: !0,
                    show: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, a = t.appStoreUrl;
                        return !0 === a
                    }
                }, {key: "formUrl", type: "input", title: "PROPOSAL.ad_config_form_url", cannotedit: !0}]),
                E = ["JPEG", "PNG"], T = e.omit(["meta", "channel", "enabled", "creativeId"]), P = {
                    all: {
                        default: {
                            params: [Object(s["a"])({}, v, {
                                key: "media",
                                type: "image",
                                format: E,
                                ratio: "600:314",
                                minSize: {width: 600, height: 314},
                                maxFileSize: 1e3,
                                accept: _["t"],
                                name: "PROPOSAL.ad_config_all_media"
                            }), {
                                key: "mainText",
                                name: "PROPOSAL.ad_config_all_main_title",
                                type: "input",
                                maxLength: 40
                            }, {
                                key: "shortTitle",
                                name: "PROPOSAL.ad_config_all_short_title",
                                type: "input",
                                maxLength: 25,
                                show: e.compose(e.flip(e.gt)(25), p["e"], e.prop("mainText"))
                            }, {
                                key: "subText",
                                type: "textarea",
                                name: "PROPOSAL.ad_config_all_description"
                            }].concat(Object(n["a"])(S))
                        }
                    },
                    google: {
                        ExpandedTextAd: {
                            subChannel: ["search", "display"],
                            adType: "link",
                            getDefaultCreative: function (e) {
                                var t = this.params, a = {};
                                return t.forEach(function (t) {
                                    var n = t.key;
                                    a[n] = "title1" === n ? e : ""
                                }), a
                            },
                            params: [{
                                key: "title1",
                                name: "PROPOSAL.ad_config_google_text_title_1",
                                type: "input",
                                maxLength: 30
                            }, {
                                key: "title2",
                                name: "PROPOSAL.ad_config_google_text_title_2",
                                type: "input",
                                maxLength: 30
                            }, {
                                key: "title3",
                                name: "PROPOSAL.ad_config_google_text_title_3",
                                type: "input",
                                optional: !0,
                                maxLength: 30
                            }, {
                                key: "description",
                                maxLength: 90,
                                type: "textarea",
                                name: "PROPOSAL.ad_config_google_text_description"
                            }, {
                                key: "description2",
                                maxLength: 90,
                                type: "textarea",
                                optional: !0,
                                name: "PROPOSAL.ad_config_google_text_description_2"
                            }].concat(Object(n["a"])(S)),
                            executable: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    t = arguments.length > 1 ? arguments[1] : void 0,
                                    a = O(e, ["title1", "title2", "description"]), o = A(e, t);
                                return C([].concat(Object(n["a"])(a), Object(n["a"])(o)))
                            },
                            updateFrom: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                if (t === a) return a;
                                var n = t.creative, o = e.compose(e.of, e.defaultTo(""));
                                switch (t.channel) {
                                    case"facebook":
                                    case"instagram":
                                        var i, c, l;
                                        "photo" === t.creativeType ? (i = n.title, c = n.description, l = n.url) : (i = n.cards[0].title, c = n.mainDescription, l = n.mainUrl);
                                        var d = g(o(i), {maxPart: 3, maxLength: 30}) || [], u = Object(r["a"])(d, 3),
                                            p = u[0], _ = void 0 === p ? "" : p, m = u[1], f = void 0 === m ? "" : m,
                                            h = u[2], b = void 0 === h ? "" : h,
                                            v = g(o(c), {maxPart: 2, maxLength: 90}) || [], y = Object(r["a"])(v, 2),
                                            w = y[0], k = void 0 === w ? "" : w, x = y[1], O = void 0 === x ? "" : x;
                                        return Object(s["a"])({}, a, {
                                            creative: Object(s["a"])({}, a.creative, {
                                                title1: _,
                                                title2: f,
                                                title3: b,
                                                description: k,
                                                description2: O,
                                                url: l
                                            })
                                        });
                                    default:
                                        return a
                                }
                            }
                        },
                        ResponsiveDisplayAd: {
                            subChannel: ["display"],
                            adType: "link",
                            getDefaultCreative: b,
                            params: [{
                                key: "image",
                                type: "image",
                                format: E,
                                ratio: "600:314",
                                minSize: {width: 600, height: 314},
                                maxFileSize: 1e3,
                                name: "PROPOSAL.ad_config_google_display_image"
                            }, {key: "squareImage", show: e.F, type: "image"}, {
                                key: "logoImage",
                                type: "image",
                                format: E,
                                ratio: "1:1",
                                minSize: {width: 128, height: 128},
                                optional: !0,
                                maxFileSize: 1e3,
                                name: "PROPOSAL.ad_config_all_logo"
                            }, {
                                key: "shortTitle",
                                type: "input",
                                maxLength: 25,
                                name: "PROPOSAL.ad_config_google_display_title"
                            }, {
                                key: "longTitle",
                                type: "input",
                                maxLength: 90,
                                name: "PROPOSAL.ad_config_google_display_title_long"
                            }, {
                                key: "businessName",
                                type: "input",
                                maxLength: 25,
                                name: "PROPOSAL.ad_config_business_name"
                            }, {
                                key: "description",
                                type: "textarea",
                                maxLength: 90,
                                name: "PROPOSAL.ad_config_google_display_description"
                            }].concat(Object(n["a"])(S), [y]),
                            executable: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                                    a = ["image", "shortTitle", "businessName", "description"];
                                !0 === t.strictValidation && a.push("longTitle");
                                var o = O(e, a), i = A(e, t);
                                return C([].concat(Object(n["a"])(o), Object(n["a"])(i)))
                            },
                            updateFrom: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                if (t === a) return t;
                                var n = t.creative, o = e.compose(e.of, e.defaultTo(""));
                                switch (t.channel) {
                                    case"facebook":
                                    case"instagram":
                                        var i, c, l;
                                        "photo" === t.creativeType ? (i = n.title, c = n.description, T = n.url, l = n.media) : (i = n.cards[0].title, c = n.mainDescription, l = n.cards[0].media, T = n.mainUrl);
                                        var d = g(o(i), {maxPart: 2, maxLength: 25}) || [], u = Object(r["a"])(d, 2),
                                            _ = u[0], m = void 0 === _ ? "" : _, f = u[1], h = void 0 === f ? "" : f,
                                            b = g(o(c), {maxPart: 1, maxLength: 90}) || [], v = Object(r["a"])(b, 1),
                                            y = v[0], w = void 0 === y ? "" : y;
                                        return Object(s["a"])({}, a, {
                                            creative: Object(s["a"])({}, a.creative, {
                                                image: Object(p["O"])(l) ? a.image : l,
                                                shortTitle: m,
                                                longTitle: h,
                                                description: w,
                                                url: T
                                            })
                                        });
                                    case"google":
                                        var k = n.title1, x = void 0 === k ? "" : k, O = n.title2,
                                            A = void 0 === O ? "" : O, C = n.title3, S = void 0 === C ? "" : C, E = n.url,
                                            T = void 0 === E ? "" : E;
                                        return Object(s["a"])({}, a, {
                                            creative: Object(s["a"])({}, a.creative, {
                                                shortTitle: g(o(x + A + S), {
                                                    maxPart: 1,
                                                    maxLength: 25
                                                })[0] || "",
                                                description: g(o(w), {maxPart: 1, maxLength: 90})[0] || "",
                                                url: T
                                            })
                                        });
                                    default:
                                        return a
                                }
                            }
                        },
                        universalApp: {
                            subChannel: ["universal"],
                            adType: "link",
                            getDefaultCreative: function (e) {
                                var t = this.params, a = {};
                                return t.forEach(function (t) {
                                    var n = t.key;
                                    a[n] = "title1" === n ? e : ""
                                }), a
                            },
                            params: [{
                                key: "description1",
                                name: "PROPOSAL.ad_config_google_description1",
                                type: "input",
                                maxLength: 25
                            }, {
                                key: "description2",
                                name: "PROPOSAL.ad_config_google_description2",
                                type: "input",
                                maxLength: 25
                            }, {
                                key: "description3",
                                name: "PROPOSAL.ad_config_google_description3",
                                type: "input",
                                maxLength: 25
                            }, {
                                key: "description4",
                                type: "textarea",
                                name: "PROPOSAL.ad_config_google_description4",
                                maxLength: 25
                            }, {key: "url", type: "input", name: "PROPOSAL.ad_config_all_url"}].concat(Object(n["a"])(S)),
                            executable: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    t = arguments.length > 1 ? arguments[1] : void 0,
                                    a = O(e, ["description1", "description2", "description3", "description4"]), o = A(e, t);
                                return C([].concat(Object(n["a"])(a), Object(n["a"])(o)))
                            },
                            updateFrom: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                if (t === a) return a;
                                var n = t.creative, o = e.compose(e.of, e.defaultTo(""));
                                switch (t.channel) {
                                    case"facebook":
                                    case"instagram":
                                        var i;
                                        i = "photo" === t.creativeType ? n.description : n.mainDescription;
                                        var c = g(o(i), {maxPart: 4, maxLength: 25}) || [], l = Object(r["a"])(c, 4),
                                            d = l[0], u = void 0 === d ? "" : d, p = l[1], _ = void 0 === p ? "" : p,
                                            m = l[2], f = void 0 === m ? "" : m, h = l[3], b = void 0 === h ? "" : h;
                                        return Object(s["a"])({}, a, {
                                            creative: Object(s["a"])({}, a.creative, {
                                                description1: u,
                                                description2: _,
                                                description3: f,
                                                description4: b
                                            })
                                        });
                                    case"google":
                                        var v;
                                        "ExpandedTextAd" === t.creativeType && (i = n.description, v = n.description2);
                                        var y = g(o(i), {maxPart: 1, maxLength: 25}) || [], w = Object(r["a"])(y, 1),
                                            k = w[0];
                                        u = void 0 === k ? "" : k;
                                        var x = g(o(v), {maxPart: 1, maxLength: 25}) || [], O = Object(r["a"])(x, 1),
                                            A = O[0];
                                        return _ = void 0 === A ? "" : A, Object(s["a"])({}, a, {
                                            creative: Object(s["a"])({}, a.creative, {
                                                description1: u,
                                                description2: _,
                                                description3: f,
                                                description4: b
                                            })
                                        });
                                    default:
                                        return a
                                }
                            }
                        },
                        GmailAd: {
                            subChannel: ["display"],
                            params: [{
                                key: "image",
                                type: "image",
                                format: E,
                                ratio: "600:314",
                                minSize: {width: 600, height: 314},
                                maxFileSize: 1e3,
                                name: "Main Image"
                            }, {
                                key: "logoImage",
                                type: "image",
                                format: E,
                                ratio: "1:1",
                                minSize: {width: 128, height: 128},
                                optional: !0,
                                maxFileSize: 1e3,
                                name: "Logo Image"
                            }, {key: "title", type: "input", maxLength: 25, name: "Title"}, {
                                key: "businessName",
                                type: "input",
                                maxLength: 25,
                                name: "Business Name"
                            }, {
                                key: "description",
                                type: "textarea",
                                maxLength: 90,
                                name: "Description"
                            }, {
                                key: "descriptionImage",
                                type: "textarea",
                                maxLength: 90,
                                name: "descriptionImage"
                            }, {key: "url", type: "input", name: "Landing URL"}]
                        },
                        CallOnlyAd: {
                            subChannel: ["display"],
                            adType: "call",
                            getDefaultCreative: b,
                            params: [{
                                key: "businessName",
                                type: "input",
                                maxLength: 25,
                                name: "Business Name"
                            }, {
                                key: "description",
                                type: "textarea",
                                maxLength: 160,
                                name: "Description"
                            }, {key: "phoneNumber", type: "phoneNumber", name: "Phone Number"}, {
                                key: "url",
                                type: "input",
                                name: "Landing URL"
                            }]
                        }
                    },
                    facebook: {
                        photo: {
                            subChannel: ["facebook", "instagram", "audience_network", "messenger"],
                            adType: "link",
                            getDefaultCreative: b,
                            params: [Object(s["a"])({}, v, {
                                maxVideoLength: {value: 60, unit: "min"},
                                key: "media",
                                name: "PROPOSAL.ad_config_all_media",
                                accept: _["t"],
                                recommendedSize: {width: 1080, height: 1080},
                                format: E,
                                ratio: "1.91:1",
                                minSize: {width: 500, height: 500},
                                maxFileSize: 1e3,
                                type: "slides"
                            }), {
                                key: "description",
                                type: "textarea",
                                name: "PROPOSAL.ad_config_facebook_description"
                            }, {
                                key: "title",
                                type: "input",
                                optional: !0,
                                name: "PROPOSAL.ad_config_facebook_headline"
                            }, {
                                key: "linkDescription",
                                type: "input",
                                maxLength: 90,
                                name: "PROPOSAL.ad_config_facebook_link_description",
                                optional: !0
                            }].concat(Object(n["a"])(S), [y]),
                            addGroup: w,
                            executable: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                                    a = O(e, ["description", "media"]), o = A(e, t);
                                return C([].concat(Object(n["a"])(a), Object(n["a"])(o)))
                            },
                            updateFrom: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                switch (t.channel) {
                                    case"instagram":
                                    case"facebook":
                                        return Object(s["a"])({}, a, T(t), {meta: e.clone(t.meta)});
                                    default:
                                        return a
                                }
                            }
                        },
                        carousel: {
                            subChannel: ["facebook", "instagram", "audience_network", "messenger"],
                            adType: "link",
                            sharedParams: [{name: "mainDescription"}, {name: "mainUrl"}],
                            params: [Object(s["a"])({}, v, {
                                key: "media",
                                name: "PROPOSAL.ad_config_all_media",
                                accept: _["t"],
                                recommendedSize: {width: 1080, height: 1080},
                                format: E,
                                ratio: "1.91:1",
                                minSize: {width: 500, height: 500},
                                maxFileSize: 1e3,
                                type: "slides"
                            }), {
                                type: "textarea",
                                key: "mainDescription",
                                name: "PROPOSAL.ad_config_facebook_carousel_description_common",
                                common: !0
                            }, {
                                key: "mainUrl",
                                name: "PROPOSAL.ad_config_carousel_url_common",
                                type: "input",
                                common: !0
                            }, {
                                key: "title",
                                type: "input",
                                maxLength: 40,
                                name: "PROPOSAL.ad_config_facebook_headline",
                                optional: !0
                            }, {
                                key: "description",
                                type: "input",
                                name: "PROPOSAL.ad_config_facebook_carousel_description"
                            }].concat(Object(n["a"])(S), [y]),
                            addGroup: k,
                            removeGroup: x,
                            executable: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], n = [];
                                t.mainDescription || n.push({type: "missing", paramName: "mainDescription", index: 0});
                                var o = t.cards;
                                return (!o || o.length < 2) && n.push({type: "notEnoughCards"}), o.forEach(function (t, o) {
                                    var i = t.media, r = void 0 === i ? "" : i, s = t.meta;
                                    s = void 0 === s ? {} : s;
                                    var c = s.media, l = void 0 === c ? {} : c;
                                    r || n.push({
                                        type: "missing",
                                        paramName: "media",
                                        index: o
                                    }), n = n.concat(A(t, a).map(e.assoc("index", o))), Object(p["O"])(r) && l && l.duration > 3600 && n.push({
                                        type: "invalid",
                                        paramName: "media",
                                        tooLong: 3600
                                    })
                                }), C(n)
                            },
                            updateFrom: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                switch (t.channel) {
                                    case"instagram":
                                    case"facebook":
                                        return Object(s["a"])({}, a, T(t), {meta: e.clone(t.meta)});
                                    default:
                                        return a
                                }
                            }
                        }
                    },
                    instagram: {
                        photo: {
                            adType: "link",
                            getDefaultCreative: b,
                            params: [Object(s["a"])({
                                key: "media",
                                name: "PROPOSAL.ad_config_all_media",
                                type: "slides"
                            }, v, {
                                maxVideoLength: {value: 1, unit: "min"},
                                accept: _["t"],
                                recommendedSize: {width: 1080, height: 1080},
                                format: E,
                                ratio: "1:1",
                                minSize: {width: 500, height: 500},
                                maxFileSize: 1e3,
                                validate: function () {
                                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                        a = t[this.key], n = [];
                                    if (!a) return n;
                                    if (Object(p["O"])(a)) {
                                        var o = e.pathOr({}, ["meta", this.key], t);
                                        (o.width < 500 || o.height < 500) && n.push(h.SIZE), o.duration > 60 && n.push(h.DURATION)
                                    }
                                    return n
                                }
                            }), {
                                key: "description",
                                type: "textarea",
                                name: "PROPOSAL.ad_config_facebook_description"
                            }].concat(Object(n["a"])(S), [y]),
                            addGroup: w,
                            executable: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                                    a = O(e, ["description", "media"]), o = A(e, t);
                                return C([].concat(Object(n["a"])(a), Object(n["a"])(o)))
                            },
                            updateFrom: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                switch (t.channel) {
                                    case"facebook":
                                    case"instagram":
                                        return Object(s["a"])({}, a, T(t), {meta: e.clone(t.meta)});
                                    default:
                                        return a
                                }
                            }
                        },
                        carousel: {
                            adType: "link",
                            sharedParams: [{name: "mainDescription"}, {name: "mainUrl"}],
                            params: [Object(s["a"])({}, v, {
                                maxVideoLength: {value: 1, unit: "min"},
                                key: "media",
                                name: "PROPOSAL.ad_config_all_media",
                                type: "slides",
                                accept: _["t"],
                                recommendedSize: {width: 1080, height: 1080},
                                format: E,
                                ratio: "1.91:1",
                                minSize: {width: 500, height: 500},
                                maxFileSize: 1e3,
                                recommendedVideoRatio: {h: 1, v: 1},
                                validate: function () {
                                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                        a = t[this.key], n = [];
                                    if (!a) return n;
                                    if (Object(p["O"])(a)) {
                                        var o = e.pathOr({}, ["meta", this.key], t);
                                        1 != o.ratio && n.push(h.RATIO), (o.width < 500 || o.height < 500) && n.push(h.SIZE), o.duration > 60 && n.push(h.DURATION)
                                    }
                                    return n
                                }
                            }), {
                                type: "textarea",
                                key: "mainDescription",
                                name: "PROPOSAL.ad_config_facebook_carousel_description_common",
                                common: !0
                            }, {
                                key: "mainUrl",
                                name: "PROPOSAL.ad_config_carousel_url_common",
                                type: "input",
                                common: !0
                            }, {
                                key: "title",
                                type: "input",
                                maxLength: 40,
                                name: "PROPOSAL.ad_config_facebook_headline",
                                optional: !0
                            }].concat(Object(n["a"])(S), [y]),
                            addGroup: k,
                            removeGroup: x,
                            executable: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, n = [];
                                t.mainDescription || n.push({type: "missing", paramName: "mainDescription", index: 0});
                                var o = t.cards;
                                return (!o || o.length < 2) && n.push({type: "notEnoughCards"}), o.forEach(function (t, o) {
                                    var i = t.media, r = void 0 === i ? "" : i, s = t.meta;
                                    s = void 0 === s ? {} : s;
                                    var c = s.media, l = void 0 === c ? {} : c;
                                    r || n.push({
                                        type: "missing",
                                        paramName: "media",
                                        index: o
                                    }), n = n.concat(A(t, a).map(e.assoc("index", o))), Object(p["O"])(r) && l && (1 != l.ratio && n.push({
                                        type: "invalid",
                                        paramName: "media",
                                        ratio: 1,
                                        index: o
                                    }), (l.width < 500 || l.height) && n.push({
                                        type: "invalid",
                                        paramName: "media",
                                        minWidth: 500,
                                        index: o
                                    }), l.duration > 60 && n.push({
                                        type: "invalid",
                                        paramName: "media",
                                        tooLong: 60,
                                        index: o
                                    }))
                                }), C(n)
                            },
                            updateFrom: function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                                switch (t.channel) {
                                    case"facebook":
                                    case"instagram":
                                        return Object(s["a"])({}, a, T(t), {meta: e.clone(t.meta)});
                                    default:
                                        return a
                                }
                            }
                        }
                    },
                    apple: {search: {adType: "link", params: [{key: "mobileAppInfo"}]}}
                };
            t["a"] = P
        }).call(this, a("b17e"))
    }, b815: function (e, t, a) {
        "use strict";
        var n = a("5e64"), o = a.n(n);
        o.a
    }, b8cc: function (e, t, a) {
        "use strict";
        (function (e) {
            a.d(t, "m", function () {
                return k
            }), a.d(t, "l", function () {
                return x
            }), a.d(t, "h", function () {
                return O
            }), a.d(t, "c", function () {
                return A
            }), a.d(t, "d", function () {
                return C
            }), a.d(t, "a", function () {
                return S
            }), a.d(t, "j", function () {
                return E
            }), a.d(t, "i", function () {
                return T
            }), a.d(t, "f", function () {
                return P
            }), a.d(t, "b", function () {
                return I
            }), a.d(t, "e", function () {
                return L
            }), a.d(t, "g", function () {
                return j
            }), a.d(t, "k", function () {
                return M
            });
            var n = a("aede"), o = a("9c56"), i = a("bd2c");

            function r() {
                var e = Object(n["a"])(["\n            color: rgba(102, 102, 102, 0.3);\n            cursor: not-allowed;\n            &:hover {\n                color: rgba(102, 102, 102, 0.3);\n            }\n        "]);
                return r = function () {
                    return e
                }, e
            }

            function s() {
                var e = Object(n["a"])(["\n    color: #666666;\n    cursor: pointer;\n\n    &:last-child {\n        margin-left: 10px;\n    }\n\n    &:hover {\n        color: #999;\n    }\n\n    ", "\n"]);
                return s = function () {
                    return e
                }, e
            }

            function c() {
                var e = Object(n["a"])(["\n    ", "\n    font-size: 13px;\n    color: #4d5058;\n    margin-left: 5px;\n    margin-right: 5px;\n"]);
                return c = function () {
                    return e
                }, e
            }

            function l() {
                var e = Object(n["a"])(["\n    width: 15px;\n    height: 15px;\n    border-radius: 50%;\n    background-color: ", ";\n"]);
                return l = function () {
                    return e
                }, e
            }

            function d() {
                var e = Object(n["a"])(["\n    padding: 0 15px;\n    min-height: 45px;\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    cursor: pointer;\n\n    &:not(:last-child) {\n        border-bottom: 1px solid #eeeeee;\n    }\n\n    &:hover {\n        background-color: #f5f5f5;\n    }\n"]);
                return d = function () {
                    return e
                }, e
            }

            function u() {
                var e = Object(n["a"])(["\n    position: absolute;\n    top: 70px;\n    left: 0;\n    right: 0;\n    z-index: 1;\n    border-radius: 5px;\n    box-shadow: 0.7px 0.7px 10px 0 rgba(0, 0, 0, 0.15);\n    background-color: white;\n    animation-duration: 0.3s;\n"]);
                return u = function () {
                    return e
                }, e
            }

            function p() {
                var e = Object(n["a"])(["\n                  color: 666666;\n                  border: solid 1px #dddddd;\n              "]);
                return p = function () {
                    return e
                }, e
            }

            function _() {
                var e = Object(n["a"])(["\n                  color: white;\n              "]);
                return _ = function () {
                    return e
                }, e
            }

            function m() {
                var e = Object(n["a"])(["\n    ", "\n    width: 35px;\n    height: 35px;\n    border-radius: 5px;\n    cursor: pointer;\n    ", "\n    ", "\n\n    &:last-child {\n        margin-left: 10px;\n    }\n"]);
                return m = function () {
                    return e
                }, e
            }

            function f() {
                var e = Object(n["a"])(["\n    ", "\n    ", "\n    height: 25px;\n    border-radius: 3px;\n    border: solid 1px #dddddd;\n    background-color: #f2f3f5;\n    font-size: 11px;\n    color: #666666;\n    &:hover {\n        background-color: #eee;\n    }\n"]);
                return f = function () {
                    return e
                }, e
            }

            function g() {
                var e = Object(n["a"])(["\n    ", "\n    border-radius: 555px;\n    border: solid 1px #cccccc;\n    background-color: white;\n    font-size: 11px;\n    color: #999999;\n    padding: 0 7px;\n"]);
                return g = function () {
                    return e
                }, e
            }

            function h() {
                var e = Object(n["a"])(["\n    ", "\n    font-size: 15px;\n    font-weight: 600;\n    color: #4d5058;\n    margin-left: 5px;\n    margin-right: 10px;\n"]);
                return h = function () {
                    return e
                }, e
            }

            function b() {
                var e = Object(n["a"])(["\n    ", "\n    ", "\n    width: 35px;\n    height: 35px;\n    border-radius: 50%;\n    font-size: 16px;\n    font-weight: 600;\n    color: white;\n    background-color: ", ";\n"]);
                return b = function () {
                    return e
                }, e
            }

            function v() {
                var e = Object(n["a"])(["\n    ", "\n"]);
                return v = function () {
                    return e
                }, e
            }

            function y() {
                var e = Object(n["a"])(["\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    width: 100%;\n    padding: 0 15px;\n"]);
                return y = function () {
                    return e
                }, e
            }

            function w() {
                var e = Object(n["a"])(["\n    width: 100%;\n    height: 65px;\n    border-radius: 5px;\n    box-shadow: 0.7px 0.7px 10px 0 rgba(0, 0, 0, 0.1);\n    background-color: white;\n    position: relative;\n    ", "\n"]);
                return w = function () {
                    return e
                }, e
            }

            var k = o["b"].div(w(), i["f"]), x = o["b"].div(y()), O = o["b"].div(v(), i["f"]),
                A = Object(o["b"])("span", {color: String})(b(), i["f"], i["g"], e.propOr("#5aabe3", "color")),
                C = o["b"].span(h(), i["g"]), S = o["b"].span(g(), i["g"]), E = o["b"].button(f(), i["g"], i["f"]),
                T = Object(o["b"])("i", {isPlus: Boolean})(m(), i["f"], function (e) {
                    var t = e.isPlus;
                    return t ? i["k"] : i["i"]
                }, function (e) {
                    var t = e.isPlus;
                    return t ? Object(o["a"])(_()) : Object(o["a"])(p())
                }), P = o["b"].div(u()), I = o["b"].div(d()),
                L = Object(o["b"])("span", {color: String})(l(), e.propOr("#5aabe3", "color")),
                j = o["b"].span(c(), i["g"]), M = Object(o["b"])("i", {disabled: Boolean})(s(), function (e) {
                    var t = e.disabled;
                    return t && Object(o["a"])(r())
                })
        }).call(this, a("b17e"))
    }, b965: function (e, t, a) {
        "use strict";
        var n = a("b55e"), o = a.n(n);
        o.a
    }, bd2c: function (e, t, a) {
        "use strict";
        a.d(t, "a", function () {
            return g
        }), a.d(t, "b", function () {
            return h
        }), a.d(t, "f", function () {
            return b
        }), a.d(t, "c", function () {
            return v
        }), a.d(t, "g", function () {
            return y
        }), a.d(t, "j", function () {
            return w
        }), a.d(t, "k", function () {
            return k
        }), a.d(t, "i", function () {
            return x
        }), a.d(t, "e", function () {
            return O
        }), a.d(t, "d", function () {
            return A
        }), a.d(t, "h", function () {
            return C
        });
        var n = a("aede"), o = a("9c56");

        function i() {
            var e = Object(n["a"])(["\n    display: flex;\n    flex-direction: column;\n"]);
            return i = function () {
                return e
            }, e
        }

        function r() {
            var e = Object(n["a"])(["\n    ", "\n    ", "\n    color: white;\n    min-height: 30px;\n    border-radius: 3px;\n"]);
            return r = function () {
                return e
            }, e
        }

        function s() {
            var e = Object(n["a"])(["\n    ", "\n    ", "\n    color: white;\n    min-height: 30px;\n    border-radius: 3px;\n"]);
            return s = function () {
                return e
            }, e
        }

        function c() {
            var e = Object(n["a"])(["\n    background-color: #f2f3f5;\n    transition: background 0.2s;\n    &:hover {\n        background: #eee;\n    }\n"]);
            return c = function () {
                return e
            }, e
        }

        function l() {
            var e = Object(n["a"])(["\n    background-color: #5aabe3;\n    transition: background 0.2s;\n\n    &:hover {\n        background: #519acc;\n    }\n"]);
            return l = function () {
                return e
            }, e
        }

        function d() {
            var e = Object(n["a"])(["\n    display: inline-block;\n"]);
            return d = function () {
                return e
            }, e
        }

        function u() {
            var e = Object(n["a"])(["\n    font-family: Rubik, sans-serif, Noto Sans;\n"]);
            return u = function () {
                return e
            }, e
        }

        function p() {
            var e = Object(n["a"])(["\n    content: '';\n    width: 100%;\n    height: 100%;\n    position: absolute;\n    top: 0;\n    left: 0;\n"]);
            return p = function () {
                return e
            }, e
        }

        function _() {
            var e = Object(n["a"])(["\n    display: flex;\n    justify-content: center;\n    align-items: center;\n"]);
            return _ = function () {
                return e
            }, e
        }

        function m() {
            var e = Object(n["a"])(["\n    position: absolute;\n    top: 50%;\n    transform: translateY(-50%);\n"]);
            return m = function () {
                return e
            }, e
        }

        function f() {
            var e = Object(n["a"])(["\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n"]);
            return f = function () {
                return e
            }, e
        }

        var g = Object(o["a"])(f()), h = Object(o["a"])(m()), b = Object(o["a"])(_()), v = Object(o["a"])(p()),
            y = Object(o["a"])(u()), w = o["b"].span(d()), k = Object(o["a"])(l()), x = Object(o["a"])(c()),
            O = o["b"].button(s(), b, k), A = o["b"].button(r(), b, x), C = Object(o["a"])(i())
    }, bf53: function (e, t, a) {
        "use strict";
        (function (e) {
            a("e37d");
            t["a"] = {
                namespaced: !0,
                state: {
                    isLogoutModalActive: !1,
                    isLoadingAdriel: !1,
                    loadingMsg: "",
                    isNewCampaignModalActive: !1,
                    windowDimension: {}
                },
                mutations: {
                    setLogoutModalActive: function (e, t) {
                        e.isLogoutModalActive = t
                    }, setLoadingAdrielActive: function (e, t) {
                        e.isLoadingAdriel = t
                    }, setIsNewCampaignModalActive: function (e, t) {
                        e.isNewCampaignModalActive = t
                    }, setLoadingMsg: function (e, t) {
                        e.loadingMsg = t
                    }, setDemoAccountModalActive: function (e, t) {
                        e.isDemoAccountModalActive = t
                    }, setWindowDimension: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                        e.windowDimension = t
                    }
                },
                actions: {
                    setLoadingAdriel: function (e, t) {
                        var a = e.commit, n = t.active, o = t.message;
                        a("setLoadingAdrielActive", n), a("setLoadingMsg", o)
                    }
                },
                getters: {
                    isLogoutModalActive: e.prop("isLogoutModalActive"),
                    isLoadingAdriel: e.prop("isLoadingAdriel"),
                    isNewCampaignModalActive: e.prop("isNewCampaignModalActive"),
                    loadingMsg: e.prop("loadingMsg"),
                    windowDimension: e.propOr({width: 0, height: 0}, "windowDimension")
                }
            }
        }).call(this, a("b17e"))
    }, c082: function (e, t, a) {
        "use strict";
        (function (e) {
            a("28a5"), a("ac6a");
            var n = a("2638"), o = a.n(n), i = (a("c5f6"), a("da7a")), r = a("a748"), s = a("1a2d"), c = a("4677"),
                l = a("ebb6"), d = a("5670"), u = a("fa7d"), p = e.path(["target", "value"]);
            t["a"] = {
                name: "InputField",
                mounted: function () {
                    var e = this;
                    this.$nextTick(function () {
                        return e.watchValue$()
                    });
                    var t = this.$watchAsObservable("_value", {immediate: !this.isLimitLine}),
                        a = this.$watchAsObservable("windowDimension").pipe(Object(s["a"])(300)),
                        n = Object(r["a"])(t, a).pipe(Object(c["a"])(150), Object(l["a"])(function () {
                            return e.$refs.elem
                        }), Object(d["a"])(function (t) {
                            return t && "textarea" === e.type
                        }));
                    if (this.$subscribeTo(n, u["Y"]), this.autofocus) {
                        var o = this.$refs.elem;
                        o && o.focus()
                    }
                    this.isLimitLine && this.setTextareaStyle(!1)
                },
                props: {
                    value: {type: [String, Number], default: ""},
                    type: {type: String, default: "input"},
                    placeholder: String,
                    limit: {type: Number | String},
                    error: {type: String, default: null},
                    resizable: {type: Boolean, default: !0},
                    autofocus: {type: Boolean, default: !1},
                    disabled: {type: Boolean, default: !1},
                    additionalClass: {type: String, default: ""},
                    loading: {type: Boolean, default: !1},
                    isLimitLine: {type: Boolean, default: !1},
                    headerText: {type: String},
                    cleaner: {type: Function, default: e.identity},
                    autocorrect: {type: String, default: "on"},
                    autocapitalize: {type: String, default: "on"}
                },
                data: function () {
                    return {tooltipIsOpen: !0, focused: !1}
                },
                render: function () {
                    var e = this, t = arguments[0], a = this.limit, n = this._value, r = this.placeholder,
                        s = void 0 === r ? "" : r, c = this.className, l = void 0 === c ? "" : c, d = this.type,
                        u = this.autofocus, _ = this.disabled, m = this.focused, f = this.error,
                        g = this.placeholderPosition, h = this.autocorrect, b = this.autocapitalize, v = {
                            attrs: {
                                class: l,
                                autofocus: u,
                                placeholder: "bottom" === g && s,
                                disabled: _,
                                autocorrect: h,
                                autocapitalize: b
                            }, ref: "elem", on: {
                                focus: function () {
                                    e.focused = !0, e.$emit("focus"), e.isLimitLine && e.setTextareaStyle(!0)
                                }, blur: function () {
                                    e.focused = !1, e.isLimitLine && e.setTextareaStyle(!1)
                                }, keypress: function (t) {
                                    return e.$emit("keypress", t)
                                }, keyup: function (t) {
                                    return e.$emit("keyup", t)
                                }
                            }, directives: [{name: "maxchars", value: a}]
                        }, y = ["adriel-field-wrapper", this.additionalClass];
                    a && y.push("has-limit"), m && y.push("focused"), "textarea" === d && y.push("textarea-wrapper"), f && y.push("adriel-field-has-error"), "top" === g ? y.push("placeholder-top") : "bottom" === g && y.push("placeholder-top-remove");
                    var w = "input" === d ? t("input", o()([{}, v, {
                        domProps: {value: this._value},
                        on: {
                            input: function (t) {
                                return e._value = p(t)
                            }
                        }
                    }])) : t("textarea", o()([{
                        on: {
                            input: function (t) {
                                t.target.composing || (e._value = t.target.value)
                            }
                        }
                    }, v, {domProps: {value: e._value}}, {
                        directives: [{
                            name: "model",
                            value: e._value,
                            modifiers: {}
                        }]
                    }])), k = "";
                    if (f) {
                        var x = [{name: "tooltip", value: {content: f}}];
                        k = t("button", o()([{}, {directives: x}]), [t("i", {class: "fas fa-map-marker-exclamation"})])
                    }
                    return t("span", {class: y.join(" ")}, [t("span", {class: "header-text"}, [s]), this.$slots.icon, w, k, a && t("character-counter", {
                        attrs: {
                            str: n,
                            max: a
                        }
                    }), this.$slots.last, this.loading && t(i["a"], {attrs: {color: "#999", size: "20px"}})])
                },
                methods: {
                    watchValue$: function () {
                        var e = this, t = this.$watchAsObservable("_value", {
                            immediate: !0,
                            deep: !0
                        }).pipe(Object(c["a"])(200), Object(d["a"])(function () {
                            return e.resizable && "textarea" !== e.type
                        }));
                        this.$subscribeTo(t, function () {
                            var t = document.createElement("span");
                            e.className.split(" ").forEach(function (e) {
                                return t.classList.add(e)
                            }), t.classList.add("adriel-span-for-input"), t.innerText = e.value, document.body.appendChild(t), e.$refs.elem.style.width = t.offsetWidth + "px", t.parentNode.removeChild(t)
                        })
                    }, focus: function () {
                        var e = this.$refs.elem;
                        e && e.focus()
                    }, setTextareaStyle: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0], t = this.$refs.elem,
                            a = t.classList;
                        a.add("input-view"), e ? (a.add("onFocus"), a.remove("onBlur"), Object(u["Y"])(t, 5)) : (a.remove("onFocus"), a.add("onBlur"))
                    }
                },
                computed: {
                    _value: {
                        get: e.compose(e.defaultTo(""), e.prop("value")), set: function (e) {
                            var t = this.cleaner(e);
                            this.$emit("input", t), t !== e && this.$forceUpdate()
                        }
                    }, className: function () {
                        var e = ["adriel-field-".concat(this.type)];
                        return this.limit && e.push("has-limit"), e.join(" ")
                    }, placeholderPosition: function () {
                        return this._value.trim() ? "top" : "bottom"
                    }
                }
            }
        }).call(this, a("b17e"))
    }, c177: function (e, t, a) {
    }, c384: function (e, t, a) {
        "use strict";
        (function (e) {
            a("20d6"), a("ac6a");
            var n = a("75fc"), o = a("170a"),
                i = (a("f2a6"), ["LAUNCH_ANOTHER", "NOT_ENOUGH_TRAFFIC", "NOT_ENOUGH_CONVERSION", "NEED_MORE_GUIDE", "NO_LONGER_NEEDED"]);
            t["a"] = {
                render: function () {
                    var e = this, t = arguments[0];
                    return t("div", {class: "stop-survey-container"}, [t("img", {
                        attrs: {src: "/img/icons/modal_close_btn.png"},
                        class: "close-btn",
                        on: {
                            click: function () {
                                return e.$emit("close")
                            }
                        }
                    }), t("div", {class: "head"}, [t("img", {attrs: {src: "/img/icons/stop_survey_icon.png"}}), t("span", [this.$t("MY_CAMPAIGNS.stop_survey_title")])]), t("div", {class: "body"}, [t("div", {class: "list--container"}, [this.list()]), t("adriel-textarea", {
                        attrs: {value: this.extra},
                        on: {
                            input: function (t) {
                                return e.extra = t
                            }
                        }
                    }), t("div", {class: "list--button"}, [t("button", {on: {click: this.submit}}, [this.$t("COMMON.submit")])])])])
                }, data: function () {
                    return {selected: [], extra: "", values: i}
                }, methods: {
                    list: function () {
                        var t = this, a = this.$createElement, i = e.contains(e.__, this.selected),
                            r = e.compose(this.$t.bind(this), e.concat("MY_CAMPAIGNS.stop_survey_reason_"), e.toLower),
                            s = function (e) {
                                return a("span", {class: "text word-break"}, [r(e)])
                            };
                        return [].concat(Object(n["a"])(this.values.map(function (e) {
                            return a("div", {
                                class: "list--each", on: {
                                    click: function () {
                                        return t.toggleSelected(e)
                                    }
                                }
                            }, [a(o["a"], {attrs: {value: i(e)}}), s(e)])
                        })), [a("div", {class: "list--each others"}, [s("others")])])
                    }, toggleSelected: function (t) {
                        var a = this.selected, n = a.findIndex(e.equals(t));
                        n > -1 ? a.splice(n, 1) : a.push(t)
                    }, submit: function () {
                        var e = {values: this.selected, comment: this.extra};
                        if (!this.selected.length && !this.extra.trim()) return this.toast(this.$t("MY_CAMPAIGNS.stop_survey_warning"));
                        this.$emit("submit", e)
                    }
                }
            }
        }).call(this, a("b17e"))
    }, c499: function (e, t, a) {
    }, c50d: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("bd86"), o = a("cebc"), i = a("dde5"), r = a("fa7d");
            t["a"] = {
                namespaced: !0,
                state: {imageList: {}, categoryList: [], metaData: {}, channels_path: {}},
                mutations: {
                    setCategoryList: function (e, t) {
                        t.pop(), e.categoryList = t
                    }, setImgList: function (e, t) {
                        var a = t.category, i = t.data, s = i.map(function (t) {
                            var a = t.original_path, n = t.thumbnail_path, o = t.id, i = t.meta, s = t.channels_path;
                            return i && (e.metaData[o] = i), s.length && (e.channels_path[o] = s), {
                                original_path: Object(r["U"])(a),
                                thumbnail_path: Object(r["U"])(n),
                                id: o
                            }
                        });
                        e.imageList = Object(o["a"])({}, e.imageList, Object(n["a"])({}, a, s))
                    }
                },
                actions: {
                    getCategoryList: function (e) {
                        var t = e.commit;
                        return i["h"].getCategoryList().then(function (e) {
                            var a = e.data;
                            return t("setCategoryList", a)
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, getImageList: function (e, t) {
                        var a = e.commit, n = e.state, o = t.category, r = t.id, s = t.isRenew, c = t.campaignId;
                        if (!n.imageList[o] || s) return i["h"].getImageList(r, c).then(function (e) {
                            var t = e.data;
                            return a("setImgList", {category: o, data: t})
                        }).catch(function (e) {
                            return console.log(e)
                        })
                    }, deleteMyFile: function (e, t) {
                        var a = e.dispatch, n = t.id, o = t.campaignId;
                        return i["h"].deleteMyFiles(n, o).then(function () {
                            return a("getImageList", {category: "my_file", id: 1, isRenew: !0, campaignId: o})
                        })
                    }
                },
                getters: {
                    imageList: e.prop("imageList"),
                    categoryList: e.prop("categoryList"),
                    metaData: e.prop("metaData"),
                    channels_path: e.prop("channels_path")
                }
            }
        }).call(this, a("b17e"))
    }, c671: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("cebc"), o = a("4e4d"), i = a("b183"), r = a("608d"), s = (a("2ee0"), a("2f62")), c = a("dde5");
            t["a"] = {
                components: {"left-nav": o["a"], "top-title": r["a"], "mobile-nav": i["a"]},
                data: function () {
                    return {sending: !1, title: "", text: "", campaign_id: null, modalType: "", isModalOpen: !1}
                },
                methods: Object(n["a"])({}, Object(s["mapActions"])("campaign", ["getCampaigns"]), {
                    checkRedirect: function () {
                        "success" === this.modalType && this.$router.push("/myCampaigns")
                    }, sendHelp: function () {
                        var t = this, a = e.pick(["title", "text", "campaign_id"])(this);
                        if (!this.sending) {
                            if (!a.title || !a.text) return this.modalType = "warn", void(this.isModalOpen = !0);
                            this.sending = !0, e.compose(function (e) {
                                e.then(function () {
                                    t.sending = !1, t.modalType = "success", t.isModalOpen = !0
                                }).catch(function () {
                                    t.sending = !1, alert("err occur")
                                })
                            }, c["m"].save)(a)
                        }
                    }
                }),
                computed: Object(n["a"])({}, Object(s["mapGetters"])("campaign", ["campaigns"]), {
                    modalMent: function () {
                        var t = e.equals(this.modalType);
                        return t("warn") ? this.$t("HELP.enter_information") : t("success") ? this.$t("HELP.request_sent") : void 0
                    }
                }),
                created: function () {
                    this.getCampaigns()
                }
            }
        }).call(this, a("b17e"))
    }, c772: function (e, t, a) {
        "use strict";
        (function (e) {
            a("96cf");
            var n = a("3b8d"), o = "/ads";

            function i(e) {
                return r.apply(this, arguments)
            }

            function r() {
                return r = Object(n["a"])(regeneratorRuntime.mark(function t(a) {
                    var n, i = arguments;
                    return regeneratorRuntime.wrap(function (t) {
                        while (1) switch (t.prev = t.next) {
                            case 0:
                                return n = i.length > 1 && void 0 !== i[1] && i[1], t.abrupt("return", axios.post("".concat(o, "/generate"), {
                                    url: a,
                                    isAppAds: n
                                }).then(e.prop("data")));
                            case 2:
                            case"end":
                                return t.stop()
                        }
                    }, t)
                })), r.apply(this, arguments)
            }

            function s(e) {
                return c.apply(this, arguments)
            }

            function c() {
                return c = Object(n["a"])(regeneratorRuntime.mark(function t(a) {
                    return regeneratorRuntime.wrap(function (t) {
                        while (1) switch (t.prev = t.next) {
                            case 0:
                                return t.abrupt("return", axios.post("".concat(o, "/generate"), a).then(e.prop("data")));
                            case 1:
                            case"end":
                                return t.stop()
                        }
                    }, t)
                })), c.apply(this, arguments)
            }

            t["a"] = {generate: i, generateByMeta: s}
        }).call(this, a("b17e"))
    }, cab9: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("cebc"), o = a("2b0e"), i = a("5477"), r = a("56d7"), s = a("2f62"), c = function (t) {
                return o["default"].component("WithAccountModal", {
                    props: Object(n["a"])({}, t.props),
                    listeners: Object(n["a"])({}, t.listeners),
                    render: function (e) {
                        return e(t, {
                            props: Object(n["a"])({}, this.$props),
                            on: Object(n["a"])({}, this.$listeners, {showAccountModal: this.showAccountModal})
                        })
                    },
                    methods: Object(n["a"])({}, Object(s["mapActions"])("user", ["createAccount", "updateAccount"]), {
                        showAccountModal: function (e) {
                            var t, a = this, s = o["default"].extend(i["a"]), c = function () {
                                t.$destroy(!0), a.$el.removeChild(t.$el)
                            };
                            t = new s({
                                propsData: Object(n["a"])({active: !0}, e),
                                i18n: r["i18n"],
                                store: r["store"]
                            }), t.$on("confirm", function (t) {
                                a.handleConfirm(e, t), c()
                            }), t.$on("update:active", function (e) {
                                e || c()
                            }), t.$mount(), this.$el.appendChild(t.$el)
                        }, handleConfirm: function (t, a) {
                            var n = this;
                            t.editMode ? this.updateAccount({
                                id: a.id,
                                account: e.pick(["color", "name"], a)
                            }) : this.createAccount(a).then(function (e) {
                                var t = e.id;
                                n.changeAccount(t, n.$route.meta.canChangeAccount)
                            })
                        }
                    })
                })
            };
            t["a"] = c
        }).call(this, a("b17e"))
    }, cb6b: function (e, t, a) {
        "use strict";
        var n = a("8501"), o = a.n(n);
        o.a
    }, d157: function (e, t) {
        e.exports = {
            ko: {
                vat_qualify: "�ъ뾽�� �깅줉踰덊샇 �낅젰 �� 硫댁젣",
                vat_description: "2019�� 遺�媛��몃쾿 媛쒖젙�쇰줈 �명빐, �ъ뾽�먮벑濡앸쾲�몃� 湲곗엯�섏�\n                �딆� �ъ슜�먯뿉 ���댁꽌�� 2019�� 7�� 1�쇰��� �쒕퉬��\n                寃곗젣 湲덉븸�� 10%�� �대떦�섎뒗 遺�媛�媛�移섏꽭媛� 遺�怨쇰맗�덈떎.<br />\n                �ъ뾽�먮벑濡앸쾲�멸� �덈떎硫� �닿납�� �깅줉�댁＜�몄슂. �대뒗 愿묎퀬\n                吏묓뻾�� �꾩닔 議곌굔�� �꾨땲硫�, �ъ뾽�먮벑濡앸쾲�멸� �깅줉��\n                �ъ슜�먮뒗 遺�媛�媛�移섏꽭 (10%)媛� 泥�뎄�섏� �딆뒿�덈떎.<br />\n                �ъ뾽�먮벑濡앸쾲�몃� �깅줉�섏� �딆� 寃쎌슦 遺�媛�媛�移섏꽭媛�\n                遺�怨쇰맗�덈떎.<br />\n                (鍮꾩궗�낆옄�� ���� 遺�媛��� 遺�怨� �뺤콉�� 援ш� 諛� �섏씠�ㅻ턿��\n                �숈씪�섍쾶 �곸슜 諛� �쒗뻾)",
                vat: "遺�媛���",
                submit_business_no: "�ъ뾽�� �깅줉踰덊샇 �쒖텧",
                not_in_korea: "�쒓뎅�� �꾨땶 �댁쇅�� �깅줉�� �ъ뾽泥대� �댁쁺 以묒엯�덈떎",
                business_no: "�ъ뾽�� �깅줉踰덊샇"
            },
            jp: {
                vat_qualify: "雅뗦��끿쇉�꿰빁�룟뀯�쎿셽�띺솮",
                vat_description: "2019亮담�곦퍡�졽쐴�ㅷ쮱力뺞뵻閭ｃ겓�덀굤�곦틟璵��끿쇉�꿰빁�룔굮鼇섇뀯�쀣겍�꾠겒�꾢틕�듾말��궋�ャ궑�녈깉�ュ��쀣겍��2019亮�7��1�γ걢�됥궢�쇈깛�밤겗黎뷸툑�묌죲��10%�ュ퐪�잆굥餓섇뒥堊▼�ㅷ쮱�뚩낌沃꿔걬�뚣겲�쇻�� 雅뗦��끿쇉�꿰빁�룔걣�귙굦�겹걪�볝겓�삯뙯�쀣겍�뤵걽�뺛걚�� �볝굦���곩틕�듿윿烏뚣겗恙낂젅�▽뻑��겒�뤵�곦틟璵��끿쇉�꿰빁�룔걣�삯뙯�뺛굦�잆궋�ャ궑�녈깉�ャ겘餓섇뒥堊▼�ㅷ쮱(10%)�뚩쳦黎귙걬�뚣겲�쎼굯�� 雅뗦��끿쇉�꿰빁�룔굮�삯뙯�쀣겒�꾢졃��,餓섇뒥堊▼�ㅷ쮱�뚩げ�쎼굢�뚣겲�쇻�� (�욂틟璵��끹겓野얇걲�뗤퍡�졽쐴�ㅷ쮱蘊�げ�욜춺�츶oogle�듽겞Facebook�귛릪�섅겓�⑴뵪�듽겞�썼죱)",
                vat: "餓섇뒥葉�",
                submit_business_no: "雅뗦��끿쇉�꿰빁�룡룓��",
                not_in_korea: "�볟쎖�㎯겘�ゃ걦役룟쨼�ョ쇉�꿔걬�뚣겍�꾠굥雅뗦�鵝볝굮�뗥뼳�쀣겍�꾠겲�쇻��",
                business_no: "雅뗦��끿쇉�꿰빁��"
            },
            en: {
                vat_qualify: "Do I qualify for an exemption?",
                vat_description: "Please enter your Korean business registration number here to be eligible for the VAT exemption. With the effect of the new VAT law starting in 1st July, 2019, it is legally required to charge an additional 10% VAT on top of the total cost if there is no business registration number submitted.<br />\n                If you run a business registered outside of South Korea, you are also eligible for the VAT exemption.\n                 (This tax system applies to users on Facebook and Google as well.)",
                vat: "VAT",
                submit_business_no: "Submit my business registration number",
                not_in_korea: "Exempt me - I am running a business registered outside of South Korea",
                business_no: "Business registration number"
            }
        }
    }, d296: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("f499"), o = a.n(n), i = a("75fc"), r = a("cebc"), s = (a("7f7f"), a("5176")), c = a.n(s),
                l = (a("7514"), a("ac6a"), a("20d6"), a("dde5")), d = a("fa7d"), u = a("a772"), p = a("c1df"),
                _ = a.n(p), m = e.path(["data", "schedule"]), f = e.compose(function (e) {
                    return e.endDate && (e.endDate = Object(d["cb"])(e.endDate)), e.startDate && (e.startDate = Object(d["cb"])(e.startDate)), e
                }, e.clone, e.propOr({}, "schedule"), e.propOr({}, "_backupData")), g = e.pathOr({}, ["data", "budget"]),
                h = e.compose(e.clone, e.propOr({}, "budget"), e.propOr({}, ["_backupData"])),
                b = e.pathOr([], ["data", "channelAds"]), v = e.pathOr([], ["_backupData", "channelAds"]),
                y = e.pathOr([], ["data", "channels"]), w = e.pathOr([], ["data", "sites"]),
                k = e.pathOr([], ["data", "searchEngines"]), x = e.pathOr([], ["data", "objectives", "targets"]),
                O = e.compose(e.filter(e.propEq("type", "link")), x),
                A = e.compose(e.filter(e.propEq("type", "call")), x), C = function () {
                    var t, a, n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [], o = n.slice(0),
                        i = e.findIndex(e.propEq("name", "companySizeDecisionMaker"), o);
                    i > -1 && (t = o.splice(i, 1)[0]), i = e.findIndex(e.propEq("name", "decisionMaker"), o), i > -1 && (a = o.splice(i, 1)[0]);
                    var r = e.findIndex(e.propEq("name", "newAndReturningCustomers"), o);
                    return r > -1 && n.splice(r, 1)[0], t && a ? (o.splice(i, 0, {
                        name: "sizeAndDecision",
                        values: {sizes: t.values, decisionMakers: a.values}
                    }), o) : n
                }, S = e.compose(C, e.path(["data", "audienceProfile"])),
                E = e.compose(C, e.path(["_backupData", "audienceProfile"])), T = e.curry(function (t, a) {
                    return e.compose(e.find(e.propEq("name", t)), S)(a)
                }), P = e.curry(function (t, a) {
                    return e.compose(e.clone, e.find(e.propEq("name", t)), E)(a)
                }), I = T("gender"), L = P("gender"), j = T("newAndReturningCustomers"), M = P("newAndReturningCustomers"),
                R = T("websiteVisitors"), N = P("websiteVisitors"), D = T("objectives"), $ = P("objectives"),
                B = T("listOfCustomers"), F = P("listOfCustomers"), G = T("location"), U = P("location"),
                q = T("audienceInterests"), Y = P("audienceInterests"), V = T("age"), z = P("age"),
                W = T("searchKeywords"), H = P("searchKeywords"), K = T("income"), J = P("income"),
                Q = T("relationshipStatus"), X = P("relationshipStatus"), Z = T("educationLevel"),
                ee = P("educationLevel"), te = T("deviceTarget"), ae = P("deviceTarget"), ne = T("sizeAndDecision"),
                oe = P("sizeAndDecision"), ie = T("sector"), re = P("sector"), se = [{
                    label: "PROPOSAL.indicator_label_ad",
                    navLabel: "PROPOSAL.indicator_nav_label_ad",
                    path: "ads"
                }, {
                    label: "PROPOSAL.indicator_label_ap",
                    navLabel: "PROPOSAL.indicator_nav_label_ap",
                    path: "audienceProfile"
                }, {label: "PROPOSAL.indicator_label_plan", navLabel: "PROPOSAL.indicator_nav_label_plan", path: "plan"}],
                ce = {
                    label: "PROPOSAL.indicator_label_form",
                    navLabel: "PROPOSAL.indicator_nav_label_form",
                    path: "form"
                }, le = function () {
                    return {
                        data: {},
                        _backupData: void 0,
                        facebookProfile: {},
                        _backupAds: void 0,
                        isUploadingVideo: !1,
                        gaConversions: [],
                        appUrlEnabled: !1
                    }
                };
            t["a"] = {
                namespaced: !0, state: le(), mutations: {
                    setData: function (t, a) {
                        a.channelAds.forEach(function (e) {
                            !1 === e.executable.value && (e.enabled = !1)
                        }), t.data = a, t._backupData = e.clone(a)
                    }, clearProposal: function (e) {
                        c()(e, le())
                    }, updateData: function (e, t) {
                        e.data = t
                    }, setContinuously: function (e, t) {
                        m(e).continuously = t
                    }, setStartDate: function (e, t) {
                        m(e).startDate = t
                    }, setEndDate: function (e, t) {
                        m(e).endDate = t
                    }, resetSchedule: function (e) {
                        e.data.schedule = f(e)
                    }, setDailyBudget: function (e, t) {
                        g(e).daily = t || 0
                    }, setChannelBudget: function (t, a) {
                        var n = a.name, o = a.value, i = g(t),
                            r = e.compose(e.find(e.propEq("name", n)), e.propOr([], "channels"))(i);
                        r && (r.value = o || 0)
                    }, resetBudget: function (e) {
                        e.data.budget = h(e)
                    }, setFacebookProfile: function (e, t) {
                        e.facebookProfile = t
                    }, updateGender: function (e, t) {
                        var a = I(e).values;
                        a[0].value = t[0], a[1].value = t[1]
                    }, resetGender: function (t) {
                        I(t).values = e.compose(e.prop("values"), L)(t)
                    }, updateNewAndReturningCustomers: function (e, t) {
                        var a = j(e).values;
                        a.newPercentage = t, a.returningPercentage = 100 - t
                    }, resetNewAndReturningCustomers: function (t) {
                        j(t).values = e.compose(e.prop("values"), M)(t)
                    }, setWebsiteVisitors: function (t, a) {
                        var n = a.type, o = a.value, i = R(t), s = e.propEq("type"), c = e.identity;
                        "googleAnalytics" === n ? (s = s("googleAnalytics"), c = e.path(["properties", 0])) : s = s("fbApp" === n ? "fbApp" : "facebookPixel");
                        var l = e.findIndex(s, i.values);
                        l > -1 && i.values.splice(l, 1), i.values.push(Object(r["a"])({type: n}, c(o)))
                    }, removeWebsiteVisitors: function (t, a) {
                        var n, o = R(t);
                        if ("google" === a ? n = e.propEq("type", "googleAnalytics") : "fbApp" === a ? n = e.propEq("type", "fbApp") : "facebook" === a && (n = e.propEq("type", "facebookPixel")), n) {
                            var i = e.findIndex(n, o.values);
                            i > -1 && o.values.splice(i, 1)
                        }
                    }, resetWebsiteVisitors: function (t) {
                        R(t).values = e.compose(e.prop("values"), N)(t)
                    }, setObjectives: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        D(e).values = t
                    }, removeObjectives: function (e, t) {
                        D(e).values.splice(t, 1)
                    }, resetObjectives: function (t) {
                        D(t).values = e.compose(e.prop("values"), $)(t)
                    }, addListOfCustomers: function (e) {
                        var t, a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        (t = B(e).values).push.apply(t, Object(i["a"])(a))
                    }, removeListOfCustomers: function (e, t) {
                        B(e).values.splice(t, 1)
                    }, resetListOfCustomers: function (t) {
                        B(t).values = e.compose(e.prop("values"), F)(t)
                    }, addLocations: function (e) {
                        var t, a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        (t = G(e).values).push.apply(t, Object(i["a"])(a))
                    }, setLocations: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        G(e).values = t
                    }, removeLocation: function (e, t) {
                        G(e).values.splice(t, 1)
                    }, resetLocation: function (t) {
                        G(t).values = e.compose(e.prop("values"), U)(t)
                    }, addAudienceInterests: function (e) {
                        var t, a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        (t = q(e).values).push.apply(t, Object(i["a"])(a))
                    }, setAudienceInterests: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        q(e).values = t
                    }, removeAudienceInterests: function (e, t) {
                        q(e).values.splice(t, 1)
                    }, resetAudienceInterests: function (t) {
                        q(t).values = e.compose(e.prop("values"), Y)(t)
                    }, updateAge: function (e, t) {
                        var a = t.idx, n = t.value;
                        V(e).values[a].value = n
                    }, resetAge: function (t) {
                        V(t).values = e.compose(e.prop("values"), z)(t)
                    }, addSearchKeywords: function (e, t) {
                        var a;
                        (a = W(e).values).push.apply(a, Object(i["a"])(t))
                    }, setSearchKeywords: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        W(e).values = t
                    }, removeSearchKeywords: function (e, t) {
                        W(e).values.splice(t, 1)
                    }, resetSearchKeywords: function (t) {
                        W(t).values = e.compose(e.prop("values"), H)(t)
                    }, updateIncome: function (e, t) {
                        var a = t.idx, n = t.value;
                        K(e).values[a].value = n
                    }, resetIncome: function (t) {
                        K(t).values = e.compose(e.prop("values"), J)(t)
                    }, updateRelationshipStatus: function (e, t) {
                        var a = t.idx, n = t.value;
                        Q(e).values[a].value = n
                    }, resetRelationshipStatus: function (t) {
                        Q(t).values = e.compose(e.prop("values"), X)(t)
                    }, updateEducationLevel: function (e, t) {
                        var a = t.idx, n = t.value;
                        Z(e).values[a].value = n
                    }, resetEducationLevel: function (t) {
                        Z(t).values = e.compose(e.prop("values"), ee)(t)
                    }, updateDeviceTarget: function (e, t) {
                        var a = t.idx, n = t.value;
                        te(e).values[a].value = n
                    }, resetDeviceTarget: function (t) {
                        te(t).values = e.compose(e.prop("values"), ae)(t)
                    }, updateSizeAndDecision: function (e, t) {
                        var a = t.type, n = t.index, o = t.value, i = ne(e).values[a];
                        i.splice(n, 1, {name: i[n].name, value: o})
                    }, resetSizeAndDecision: function (t) {
                        var a = e.compose(e.prop("values"), oe)(t), n = a.sizes, o = a.decisionMakers,
                            i = t.data.audienceProfile;
                        if (!e.isEmpty(n)) {
                            var r = e.find(e.propEq("name", "companySizeDecisionMaker"), i);
                            r.values = n
                        }
                        if (!e.isEmpty(o)) {
                            var s = e.find(e.propEq("name", "decisionMaker"), i);
                            s.values = o
                        }
                    }, addSectors: function (t) {
                        var a, n, o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [],
                            r = e.groupBy(e.ifElse(e.has("value"), e.always("categories"), e.always("customs")), o),
                            s = r.categories, c = void 0 === s ? [] : s, l = r.customs, d = void 0 === l ? [] : l,
                            u = ie(t).values;
                        (a = u.categories).push.apply(a, Object(i["a"])(c)), (n = u.customCategories).push.apply(n, Object(i["a"])(d))
                    }, removeSector: function (e, t) {
                        var a = ie(e).values, n = a.categories, o = void 0 === n ? [] : n, i = a.customCategories,
                            r = void 0 === i ? [] : i, s = o.length;
                        t < s ? o.splice(t, 1) : r.splice(t - s, 1)
                    }, resetSector: function (t) {
                        ie(t).values = e.compose(e.prop("values"), re)(t)
                    }, backupAds: function (t) {
                        t._backupAds = e.compose(e.clone, b)(t)
                    }, restoreAds: function (t) {
                        if (t._backupAds) {
                            var a = e.compose(e.not, e.converge(e.equals, [e.prop("_backupAds"), b]))(t);
                            a && (t.data.channelAds = t._backupAds), t._backupAds = void 0
                        }
                    }, mergeChannelAds: function (t) {
                        var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], n = b(t) || [],
                            o = e.path(["meta", "isEditing"]), i = e.find(e.__, a), s = e.propEq("creativeId");
                        n = n.map(function (t) {
                            var a = t.creativeId, n = ["executable", "id"], c = o(t), l = i(s(a));
                            if (Object(d["N"])(l)) return c || n.push("creative"), !l.enabled && c || n.push("enabled"), Object(r["a"])({}, t, e.pick(n, l))
                        }), t.data.channelAds = n
                    }, assignProposal: function (e, t) {
                        c()(e.data, t)
                    }, setIsUploadingVideo: function (e, t) {
                        e.isUploadingVideo = t
                    }, setAsap: function (e, t) {
                        m(e).asap = t
                    }, resetAds: function (t) {
                        t.data.channelAds = e.clone(v(t))
                    }, resetAudienceProfile: function (t) {
                        t.data.audienceProfile = e.clone(E(t))
                    }, setGAConversionList: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        e.gaConversions = t
                    }, setAppUrlEnabled: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                        e.appUrlEnabled = t
                    }, removeAd: function (t, a) {
                        t.data.channelAds = e.reject(e.propEq("creativeId", a), t.data.channelAds)
                    }, setChannelAds: function (e) {
                        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                        e.data.channelAds = Object(u["a"])(t)
                    }, updateChannelAd: function (t, a) {
                        var n = b(t), o = n.findIndex(e.propEq("creativeId", a.creativeId));
                        o > -1 && (t.data.channelAds = e.update(o, a, n))
                    }, updateMetaHash: function (e, t) {
                        var a = e.data.meta || {};
                        e.data.meta = Object(r["a"])({}, a, {hash: t || Object(d["o"])()})
                    }
                }, actions: {
                    getFacebookProfile: function (e, t) {
                        var a = e.commit;
                        return l["d"].getFacebookProfile(t).then(function (e) {
                            return a("setFacebookProfile", e), e
                        })
                    }, updateData: function (t, a) {
                        var n = t.commit, o = e.prop(e.__, a);
                        n("mergeChannelAds", o("channelAds"));
                        var i = ["channelAds"];
                        n("assignProposal", e.omit(i, a))
                    }, resetPlan: function (e) {
                        var t = e.commit;
                        t("resetSchedule"), t("resetBudget")
                    }
                }, getters: {
                    indicators: function (t, a) {
                        return "FORM" === a.baseType ? e.prepend(ce, se) : se
                    },
                    proposal: e.prop("data"),
                    isFirst: e.path(["data", "isFirst"]),
                    loaded: e.compose(e.not, e.isEmpty, e.prop("data")),
                    continuously: e.compose(e.prop("continuously"), m),
                    startDate: e.compose(d["cb"], e.prop("startDate"), m),
                    endDate: e.compose(d["cb"], e.prop("endDate"), m),
                    asap: e.compose(e.prop("asap"), m),
                    dailyBudget: e.compose(e.prop("daily"), g),
                    totalBudget: function (t, a) {
                        var n = a.startDate, o = a.endDate, i = a.asap, r = a.dailyBudget;
                        i && (n = new Date), n = _()(n).startOf("day"), o = _()(o).startOf("day");
                        var s = o.diff(n, "days") + 1;
                        return e.max(0, r * s)
                    },
                    weeklyBudget: function (e, t) {
                        return 7 * t.dailyBudget
                    },
                    monthlyBudget: function (e, t) {
                        return 28 * t.dailyBudget
                    },
                    budgetChannels: e.compose(e.prop("channels"), g),
                    facebookProfile: e.prop("facebookProfile"),
                    channels: y,
                    sites: w,
                    searchEngines: k,
                    urls: O,
                    contacts: A,
                    gender: I,
                    genderBackup: L,
                    newAndReturningCustomers: j,
                    newAndReturningCustomersBackup: M,
                    audienceProfile: S,
                    audienceProfileBackup: E,
                    websiteVisitors: R,
                    websiteVisitorsBackup: N,
                    objectives: D,
                    objectivesBackup: $,
                    listOfCustomers: B,
                    listOfCustomersBackup: F,
                    schedule: m,
                    backupSchedule: f,
                    budget: g,
                    backupBudget: h,
                    location: G,
                    locationBackup: U,
                    audienceInterests: q,
                    audienceInterestsBackup: Y,
                    age: V,
                    ageBackup: z,
                    searchKeywords: W,
                    searchKeywordsBackup: H,
                    income: K,
                    incomeBackup: J,
                    relationshipStatus: Q,
                    relationshipStatusBackup: X,
                    educationLevel: Z,
                    educationLevelBackup: ee,
                    deviceTarget: te,
                    deviceTargetBackup: ae,
                    sizeAndDecision: ne,
                    sizeAndDecisionBackup: oe,
                    sector: ie,
                    sectorBackup: re,
                    channelAds: b,
                    enabledChannelAds: e.compose(e.filter(e.propEq("enabled", !0)), b),
                    executableChannelAds: e.compose(e.filter(e.pathEq(["executable", "value"], !0)), b),
                    channelAdsBackup: v,
                    goals: e.pathOr([], ["data", "objectives", "goals"]),
                    hasPaymentMethod: function (t, a, n) {
                        var o = n.user;
                        return Boolean(e.path(["paymentMethods", "length"], o))
                    },
                    canLaunch: function (t, a, n, o) {
                        var i = n.user, r = a.dailyBudget, s = a.enabledChannelAds, c = a.executableChannelAds,
                            l = a.runningAndChanged,
                            d = e.path(["paymentMethods", "length"], i) || e.path(["user", "backoffice_admin"], i) || !o["campaign/needCreditCard"],
                            u = "PROPOSAL.not_enough_information";
                        return d ? l || (u = "") : u = "PROPOSAL.launch_check_payment", {
                            value: !!s.length && !!c.length && r && d && l,
                            reason: u
                        }
                    },
                    summary: e.pathOr([], ["data", "audienceSummary"]),
                    isUploadingVideo: e.prop("isUploadingVideo"),
                    isPlanResetable: function (t) {
                        return !e.equals(o()(m(t)), o()(f(t))) || !e.equals(g(t), h(t))
                    },
                    audienceTags: e.pathOr([], ["data", "audienceTags"]),
                    backupData: e.prop("_backupData"),
                    fbPixel: function (t, a) {
                        var n = a.websiteVisitors;
                        if (!n || !n.values) return null;
                        var o = n.values.find(e.propEq("type", "facebookPixel"));
                        return o
                    },
                    fbApp: function (t, a) {
                        var n = a.websiteVisitors;
                        if (!n || !n.values) return null;
                        var o = n.values.find(e.propEq("type", "fbApp"));
                        return o
                    },
                    ga: function (t, a) {
                        var n = a.websiteVisitors;
                        if (!n || !n.values) return null;
                        var o = n.values.find(e.propEq("type", "googleAnalytics"));
                        return o
                    },
                    pixelConversions: function (t, a) {
                        var n = a.websiteVisitors;
                        if (!n || !n.values) return [];
                        var o = n.values.find(e.propEq("type", "facebookPixel"));
                        return o && o.availableConversions ? o.availableConversions : []
                    },
                    appUrlEnabled: e.prop("appUrlEnabled"),
                    runningAndChanged: function (t, a, n, o) {
                        var i = t._backupData, r = t.data;
                        n.user;
                        return !e.equals(i, r) || !e.propEq("status", "running", o["campaign/campaign"] || {})
                    },
                    baseType: e.compose(e.toUpper, e.pathOr("WEB", ["data", "meta", "webOrApp", "type"])),
                    baseUrl: e.pathOr("", ["data", "meta", "webOrApp", "websiteUrl"]),
                    baseAppStoreUrl: e.pathOr("", ["data", "meta", "webOrApp", "appStoreUrl"]),
                    baseAppstore: e.pathOr("", ["data", "meta", "webOrApp", "appstore"]),
                    basePlayStoreUrl: e.pathOr("", ["data", "meta", "webOrApp", "playStoreUrl"]),
                    generatedAds: e.path(["data", "meta", "generatedAds"]),
                    isAppType: function (e, t) {
                        var a = t.baseType, n = void 0 === a ? "" : a;
                        return "APP" === n.toUpperCase()
                    },
                    appleAdAvailabe: function (e, t) {
                        var a = t.isAppType, n = t.baseAppstore;
                        return a && Object(d["N"])(n)
                    }
                }
            }
        }).call(this, a("b17e"))
    }, d317: function (e, t, a) {
        "use strict";
        var n = a("35c3"), o = a.n(n);
        o.a
    }, d44f: function (e, t, a) {
    }, d53c: function (e, t, a) {
    }, d805: function (e, t, a) {
    }, d807: function (e, t, a) {
    }, d955: function (e, t, a) {
    }, da8c: function (e, t) {
        e.exports = {
            ko: {oops: "��!", can_not_find: "�섏씠吏�瑜� 李얠쓣 �� �놁뒿�덈떎.", back_home: "�뚯븘媛�湲�"},
            jp: {oops: "Oops!", can_not_find: "�싥꺖�멥걣誤뗣겇�뗣굤�얇걵��", back_home: "�쎼꺖�졼겓�삠굥"},
            en: {oops: "Oops!", can_not_find: "Cannot find the page", back_home: "Back to Homepage"}
        }
    }, dde5: function (e, t, a) {
        "use strict";
        var n = a("473f"), o = a("c772"), i = a("b012"), r = a("4a1d"), s = (a("96cf"), a("3b8d")), c = "/supports";

        function l(e) {
            return d.apply(this, arguments)
        }

        function d() {
            return d = Object(s["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.post("".concat(c, "/"), t);
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), d.apply(this, arguments)
        }

        var u = {save: l}, p = a("632d"), _ = a("024b"), m = a("35ca"), f = "/transactions";

        function g() {
            return h.apply(this, arguments)
        }

        function h() {
            return h = Object(s["a"])(regeneratorRuntime.mark(function e() {
                var t;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.get(f);
                        case 2:
                            return t = e.sent, e.abrupt("return", t.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), h.apply(this, arguments)
        }

        var b = {query: g}, v = a("1bc2"), y = a("e436"), w = "/express";

        function k(e) {
            return x.apply(this, arguments)
        }

        function x() {
            return x = Object(s["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.post("".concat(w, "/"), t);
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), x.apply(this, arguments)
        }

        var O = {save: k}, A = "/stopSurveys";

        function C(e) {
            return S.apply(this, arguments)
        }

        function S() {
            return S = Object(s["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.post("".concat(A, "/"), t);
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), S.apply(this, arguments)
        }

        var E = {save: C}, T = (a("7f7f"), a("cebc")), P = a("5176"), I = a.n(P), L = a("a79c"), j = {
            forms: {
                index: {url: "/forms", method: "get", params: {}, data: {}, requiredFns: {}},
                getFormUrl: {url: "/link", method: "get", params: {campaignId: ""}, data: {}, requiredFns: {}},
                create: {url: "/forms/create", method: "post", params: {}, data: {}, requiredFns: {}},
                update: {url: "/forms/update", method: "put", params: {}, data: {}, requiredFns: {}}
            }
        }, M = function (e) {
            return e && "post" === e.method || "put" === e.method || "delete" === e.method
        };

        function R(e, t) {
            return t && t.reduce(function (t, a) {
                return t = "".concat(e, "/").concat(a), t
            }, t)
        }

        function N(e, t, a) {
            var n = a.path, o = a.params, i = a.data, r = I()({}, t);
            return r.params = Object(T["a"])({}, r.params, o), n.length && (r.url = R(r.url, n)), M(r) && (r.data = Object(T["a"])({}, r.data, i)), r
        }

        function D(e) {
            var t = e.url, a = e.method, n = e.params, o = e.data, i = e.cfg, r = {
                method: a,
                url: t,
                headers: {"Content-Type": "application/json"},
                params: n,
                data: o,
                timeout: 5e4
            };
            return axios(i ? I()(r, i) : r).then(function (e) {
                return e.data
            })
        }

        function $(e, t, a) {
            var n = a.path, o = void 0 === n ? [] : n, i = a.params, r = void 0 === i ? {} : i, s = a.data,
                c = void 0 === s ? {} : s, l = N(t, j[e][t], {path: o, params: r, data: c});
            if (!Object(L["a"])(l.requiredFns || {}, Object(T["a"])({}, r, c))) throw new Error("cannot find body data required to api");
            return l.data = l.finalizeDataForm ? l.finalizeDataForm(l.data) : l.data, D(l)
        }

        var B = {callApi: $, isValidData: L["a"]}, F = a("b414"), G = (a("c5f6"), "/images");

        function U(e) {
            return q.apply(this, arguments)
        }

        function q() {
            return q = Object(s["a"])(regeneratorRuntime.mark(function e(t) {
                var a, n, o, i, r;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return a = t.channel, n = t.type, o = t.image, e.next = 3, axios.post("".concat(G, "/process"), {
                                channel: a,
                                type: n,
                                image: o
                            });
                        case 3:
                            if (i = e.sent, r = i.data, !r) {
                                e.next = 7;
                                break
                            }
                            return e.abrupt("return", r.processedImage);
                        case 7:
                            return e.abrupt("return", null);
                        case 8:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), q.apply(this, arguments)
        }

        function Y(e) {
            return V.apply(this, arguments)
        }

        function V() {
            return V = Object(s["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.delete("".concat(G).concat(t));
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), V.apply(this, arguments)
        }

        function z(e) {
            var t = e.type, a = e.formData, n = e.params;
            return axios.post("/uploads/".concat(t), a, {header: {"Content-Type": "multipart/form-data"}, params: n})
        }

        function W(e, t) {
            return axios.post("/category/addMedia", e, {params: {campaignId: t}})
        }

        function H() {
            return axios.get("/category")
        }

        function K(e, t) {
            var a = "thumbnails" === e ? e : "media/".concat(e);
            return axios.get("/category/".concat(a), {params: {campaignId: t}})
        }

        function J(e, t) {
            return axios.delete("/category/deleteMyfile", {data: {id: Number(e)}, params: {campaignId: t}})
        }

        var Q = {
            process: U,
            deleteImage: Y,
            getCategoryList: H,
            getImageList: K,
            deleteMyFiles: J,
            uploadMyFile: z,
            generateMedia: W
        };
        a("8809");
        a.d(t, "a", function () {
            return n["a"]
        }), a.d(t, "b", function () {
            return o["a"]
        }), a.d(t, "c", function () {
            return i["a"]
        }), a.d(t, "d", function () {
            return r["a"]
        }), a.d(t, "p", function () {
            return _["a"]
        }), a.d(t, "o", function () {
            return m["a"]
        }), a.d(t, "m", function () {
            return u
        }), a.d(t, "i", function () {
            return p["a"]
        }), a.d(t, "n", function () {
            return b
        }), a.d(t, "k", function () {
            return v["a"]
        }), a.d(t, "e", function () {
            return y["a"]
        }), a.d(t, "f", function () {
            return O
        }), a.d(t, "l", function () {
            return E
        }), a.d(t, "g", function () {
            return B
        }), a.d(t, "j", function () {
            return F["a"]
        }), a.d(t, "h", function () {
            return Q
        })
    }, e2ce: function (e, t, a) {
    }, e436: function (e, t, a) {
        "use strict";
        a("96cf");
        var n = a("3b8d"), o = "/coupons";

        function i(e) {
            return r.apply(this, arguments)
        }

        function r() {
            return r = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.get("".concat(o, "/check"), {params: {code: t}});
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), r.apply(this, arguments)
        }

        function s(e) {
            return c.apply(this, arguments)
        }

        function c() {
            return c = Object(n["a"])(regeneratorRuntime.mark(function e(t) {
                var a;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.post("".concat(o, "/consume"), {code: t});
                        case 2:
                            return a = e.sent, e.abrupt("return", a.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), c.apply(this, arguments)
        }

        function l() {
            return d.apply(this, arguments)
        }

        function d() {
            return d = Object(n["a"])(regeneratorRuntime.mark(function e() {
                var t;
                return regeneratorRuntime.wrap(function (e) {
                    while (1) switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2, axios.get("".concat(o, "/usedCoupons"));
                        case 2:
                            return t = e.sent, e.abrupt("return", t.data);
                        case 4:
                        case"end":
                            return e.stop()
                    }
                }, e)
            })), d.apply(this, arguments)
        }

        t["a"] = {check: i, consume: s, fetchUsedCoupons: l}
    }, e49d: function (e, t, a) {
    }, ea2e: function (e, t, a) {
    }, eaf5: function (e, t, a) {
        "use strict";
        var n = a("0a81"), o = a.n(n);
        o.a
    }, eeaa: function (e, t, a) {
        "use strict";
        var n = a("f7d3"), o = a.n(n);
        o.a
    }, ef2e: function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("section", {staticClass: "my-campaigns-list"}, [a("div", {staticClass: "my-campaigns-list-btns"}, [a("div", [a("button", {
                    staticClass: "my-campaigns-list-btns-newcampaign",
                    on: {
                        click: function (t) {
                            return e.$emit("startChat")
                        }
                    }
                }, [a("i", {staticClass: "fal fa-plus"}), e._v("\n                " + e._s(e.$t("MY_CAMPAIGNS.new_campaign")) + "\n            ")]), a("button", {
                    staticClass: "my-campaigns-list-btns-adrielcampaign",
                    on: {
                        click: function (t) {
                            return e.$emit("goToExpress")
                        }
                    }
                }, [a("i", {staticClass: "fal fa-magic"}), e._v("\n                " + e._s(e.$t("MY_CAMPAIGNS.new_campaign_by_adriel")) + "\n            ")]), a("button", {
                    staticClass: "my-campaigns-list-btns-selectall",
                    on: {click: e.onClickSelectAllinMobile}
                }, [e._v(e._s(e.$t("MY_CAMPAIGNS.select_all")))])]), a("div", {staticClass: "my-campaigns-list-btns-right"}, [e.checkedRows.length ? a("button", {
                    staticClass: "my-campaigns-list-btns-delete",
                    on: {click: e.onClickDeleteChecked}
                }, [a("i", {staticClass: "fas fa-trash-alt"})]) : e._e(), a("b-dropdown", {
                    attrs: {
                        multiple: "",
                        position: "is-bottom-left",
                        animation: "none",
                        "mobile-modal": !1
                    }
                }, [a("button", {
                    staticClass: "my-campaigns-list-btns-ellipsis",
                    attrs: {slot: "trigger", type: "button"},
                    slot: "trigger"
                }, [a("i", {staticClass: "fas fa-ellipsis-h"})]), a("b-dropdown-item", [a("router-link", {attrs: {to: "/billingHistory"}}, [e._v(e._s(e.$t("BILLING_HISTORY.top_title")))])], 1), a("b-dropdown-item", {on: {click: e.onClickExcelDownload}}, [e._v(e._s(e.$t("DASHBOARD.download_excel")))])], 1), a("b-dropdown", {
                    ref: "dropdown",
                    staticClass: "my-campaigns-list-btns-filter",
                    attrs: {multiple: "", position: "is-bottom-left", animation: "none", "mobile-modal": !1},
                    model: {
                        value: e.selectedFilter, callback: function (t) {
                            e.selectedFilter = t
                        }, expression: "selectedFilter"
                    }
                }, [a("button", {
                    staticClass: "is-primary",
                    attrs: {slot: "trigger", type: "button"},
                    slot: "trigger"
                }, [e._v("\n                    " + e._s(e.$t("MY_CAMPAIGNS.filter")) + "\n                    "), a("i", {staticClass: "fas fa-sort-down"})]), e._l(e.validTabs, function (t) {
                    return a("b-dropdown-item", {
                        key: t,
                        on: {click: e.onClickFilterItem}
                    }, [a("b-checkbox", {
                        class: {hide: "all" === t && e.dataFilter[t]},
                        attrs: {value: e.dataFilter[t], "data-status": t},
                        on: {
                            input: function (a) {
                                return e.onClickCheckbox(a, t)
                            }
                        },
                        nativeOn: {
                            click: function (t) {
                                return e.onClickCheckBoxNative(t)
                            }
                        }
                    }, [e._v(e._s(e.$t("MY_CAMPAIGNS.tab_" + t)))])], 1)
                })], 2), a("button", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: e.$t("DASHBOARD.download_excel"),
                        expression: "$t('DASHBOARD.download_excel')"
                    }], staticClass: "my-campaigns-list-btns-download", on: {click: e.onClickExcelDownload}
                }, [a("i", {staticClass: "fas fa-arrow-alt-to-bottom"})]), a("button", {staticClass: "my-campaigns-list-btns-invoice"}, [a("router-link", {attrs: {to: "/billingHistory"}}, [e._v(e._s(e.$t("BILLING_HISTORY.top_title")))])], 1)], 1)]), a("b-table", {
                    attrs: {
                        "sort-icon": "menu-up",
                        data: this.campaigns,
                        "checked-rows": e.checkedRows,
                        total: e.totalData,
                        "current-page": e.page,
                        "is-row-checkable": function (e) {
                            return !e.active
                        },
                        "checkbox-position": "left",
                        checkable: "",
                        paginated: "",
                        bordered: "",
                        narrowed: "",
                        "backend-pagination": "",
                        "backend-sorting": "",
                        "per-page": e.listNumPerPage,
                        "default-sort": e.defaultSort,
                        "default-sort-direction": "desc"
                    }, on: {
                        "update:checkedRows": function (t) {
                            e.checkedRows = t
                        }, "update:checked-rows": function (t) {
                            e.checkedRows = t
                        }, sort: e.onSortTable, "page-change": e.onChangePage, "update:currentPage": function (t) {
                            e.page = t
                        }, "update:current-page": function (t) {
                            e.page = t
                        }
                    }, scopedSlots: e._u([{
                        key: "default", fn: function (t) {
                            return [a("b-table-column", {
                                attrs: {
                                    field: "active",
                                    label: "ON/OFF",
                                    centered: "",
                                    width: "73"
                                }
                            }, [a("p", {class: t.row.status}, [e._v(e._s(e.$t("MY_CAMPAIGNS.status_" + ("proposal_review" === t.row.status ? "in_review" : t.row.status))))]), e.changeEnablePossible({status: t.row.status}) ? a("switch-view", {
                                staticClass: "my-campaigns-list-switch",
                                attrs: {value: t.row.active},
                                on: {
                                    "update:value": function (a) {
                                        return e.switchValueChanged({value: a, status: t.row.status, id: t.row.id})
                                    }
                                },
                                nativeOn: {
                                    click: function (e) {
                                        e.stopPropagation()
                                    }
                                }
                            }) : e._e()], 1), a("b-table-column", {
                                attrs: {
                                    field: "channel",
                                    "data-field": "channel",
                                    label: e.$t("MY_CAMPAIGNS.header_channel"),
                                    centered: "",
                                    width: "65"
                                }
                            }, [e.makeChannelListForExpress(t.row).length ? e._l(e.makeChannelListForExpress(t.row), function (e) {
                                return a("img", {
                                    key: e,
                                    class: ["my-campaigns-list-icons", e],
                                    attrs: {src: "/img/proposal/" + e + "_icon.png", alt: "channel_image"}
                                })
                            }) : [e._v("-")]], 2), a("b-table-column", {
                                attrs: {
                                    field: "title",
                                    "data-field": "campaign_title",
                                    label: e.$t("MY_CAMPAIGNS.header_campaign_title"),
                                    sortable: "",
                                    width: "169"
                                }
                            }, [a("div", {
                                on: {
                                    mouseover: e.onMouseOverTitle,
                                    mouseleave: e.onMouseLeaveTitle,
                                    click: function (a) {
                                        return e.onClickCampaignTitle({event: a, props: t})
                                    }
                                }
                            }, [a("p", [a("a", [e._v(e._s(t.row.title))])]), a("b-dropdown", {
                                attrs: {
                                    position: "is-bottom-left",
                                    animation: "none",
                                    "mobile-modal": !1
                                }
                            }, [a("b-icon", {
                                attrs: {slot: "trigger", pack: "fal", icon: "ellipsis-v", type: "button"},
                                slot: "trigger"
                            }), a("b-dropdown-item", {
                                attrs: {disabled: !e.changeSettingPossible({status: t.row.status})},
                                nativeOn: {
                                    click: function (a) {
                                        return function (a) {
                                            return e.onClickDropdownMenus({
                                                event: a, emitEvent: function () {
                                                    return e.checkEdit({
                                                        status: t.row.status,
                                                        id: t.row.id,
                                                        campaignType: e.checkCampaignType(t.row)
                                                    })
                                                }
                                            })
                                        }(a)
                                    }
                                }
                            }, [a("b-icon", {
                                attrs: {
                                    pack: "far",
                                    icon: "pen-square"
                                }
                            }), a("span", [e._v(e._s(e.$t("MY_CAMPAIGNS.ad_setting")))])], 1), a("b-dropdown-item", {
                                attrs: {disabled: !e.runLongerPossible({data: t.row})},
                                nativeOn: {
                                    click: function (a) {
                                        return function (a) {
                                            return e.onClickDropdownMenus({
                                                event: a, emitEvent: function () {
                                                    return e.checkRunLonger({data: t.row})
                                                }
                                            })
                                        }(a)
                                    }
                                }
                            }, [a("b-icon", {
                                attrs: {
                                    pack: "far",
                                    icon: "calendar-plus"
                                }
                            }), a("span", [e._v(e._s(e.$t("DASHBOARD.run_longer")))])], 1), a("b-dropdown-item", {
                                attrs: {disabled: !e.resultsPossible({status: t.row.status})},
                                nativeOn: {
                                    click: function (a) {
                                        return function (a) {
                                            return e.onClickDropdownMenus({
                                                event: a, emitEvent: function () {
                                                    return e.checkResult({status: t.row.status, id: t.row.id})
                                                }
                                            })
                                        }(a)
                                    }
                                }
                            }, [a("b-icon", {
                                attrs: {
                                    pack: "fas",
                                    icon: "chart-bar"
                                }
                            }), a("span", [e._v(e._s(e.$t("MY_CAMPAIGNS.see_result")))])], 1), a("b-dropdown-item", {
                                attrs: {disabled: !e.extendBudgetPossible({status: t.row.status})},
                                nativeOn: {
                                    click: function (a) {
                                        return function (a) {
                                            return e.onClickDropdownMenus({
                                                event: a, emitEvent: function () {
                                                    return e.checkBudget({status: t.row.status, id: t.row.id})
                                                }
                                            })
                                        }(a)
                                    }
                                }
                            }, [a("b-icon", {
                                attrs: {
                                    pack: "far",
                                    icon: "usd-circle"
                                }
                            }), a("span", [e._v(e._s(e.$t("MY_CAMPAIGNS.add_budget")))])], 1), a("b-dropdown-item", {
                                nativeOn: {
                                    click: function (a) {
                                        return function (a) {
                                            return e.onClickDropdownMenus({
                                                event: a, emitEvent: function () {
                                                    return e.$emit("click:title", t.row.id)
                                                }
                                            })
                                        }(a)
                                    }
                                }
                            }, [a("b-icon", {
                                attrs: {
                                    pack: "fal",
                                    icon: "pen"
                                }
                            }), a("span", [e._v(e._s(e.$t("COMMON.campaign_update_title")))])], 1), a("b-dropdown-item", {
                                attrs: {disabled: !e.duplicable({status: t.row.status})},
                                on: {
                                    click: function (a) {
                                        return e.$emit("copy", t.row.id)
                                    }
                                },
                                nativeOn: {
                                    click: function (a) {
                                        return function (a) {
                                            return e.onClickDropdownMenus({
                                                event: a, emitEvent: function () {
                                                    return e.$emit("copy", t.row.id)
                                                }
                                            })
                                        }(a)
                                    }
                                }
                            }, [a("b-icon", {attrs: {icon: "content-copy"}}), a("span", [e._v(e._s(e.$t("MY_CAMPAIGNS.copy")))])], 1), a("b-dropdown-item", {
                                attrs: {disabled: !e.deletable({status: t.row.status})},
                                on: {
                                    click: function (a) {
                                        return e.$emit("delete", {ids: [t.row.id]})
                                    }
                                },
                                nativeOn: {
                                    click: function (a) {
                                        return function (a) {
                                            return e.onClickDropdownMenus({
                                                event: a, emitEvent: function () {
                                                    return e.$emit("delete", {ids: [t.row.id]})
                                                }
                                            })
                                        }(a)
                                    }
                                }
                            }, [a("b-icon", {
                                attrs: {
                                    pack: "fas",
                                    icon: "trash-alt"
                                }
                            }), a("span", [e._v(e._s(e.$t("MY_CAMPAIGNS.delete_campaign")))])], 1)], 1)], 1)]), a("b-table-column", {
                                attrs: {
                                    field: "objective",
                                    "data-field": "objective",
                                    label: e.$t("MY_CAMPAIGNS.header_objective")
                                }
                            }, [e._v(e._s(t.row.objective ? e.$t("MY_CAMPAIGNS.objective_" + t.row.objective.toLowerCase()) : "-"))]), a("b-table-column", {
                                attrs: {
                                    field: "result",
                                    "data-field": "result",
                                    label: e.$t("MY_CAMPAIGNS.header_result")
                                }
                            }, [e._v(e._s(t.row.resultCount ? e.setLocaleString(t.row.resultCount) : "-"))]), a("b-table-column", {
                                attrs: {
                                    field: "ctr",
                                    "data-field": "ctr",
                                    label: e.$t("MY_CAMPAIGNS.header_ctr"),
                                    sortable: ""
                                }
                            }, [e._v(e._s(null !== t.row.ctr ? t.row.ctr + " %" : "-"))]), a("b-table-column", {
                                attrs: {
                                    field: "daily_budget",
                                    "data-field": "daily_budget",
                                    label: e.$t("MY_CAMPAIGNS.header_daily_budget"),
                                    sortable: ""
                                }
                            }, [e._v(e._s(null !== t.row.daily_budget ? e.getCurrencySymbol() + " " + e.setLocaleString(t.row.daily_budget) : "-"))]), a("b-table-column", {
                                attrs: {
                                    field: "total_cost",
                                    "data-field": "total_cost",
                                    label: e.$t("MY_CAMPAIGNS.header_total_cost"),
                                    sortable: ""
                                }
                            }, [e._v(e._s(null !== t.row.total_cost ? e.getCurrencySymbol() + " " + e.setLocaleString(t.row.total_cost) : "-"))]), a("b-table-column", {
                                attrs: {
                                    field: "cpc",
                                    "data-field": "cost_per_click",
                                    label: e.$t("MY_CAMPAIGNS.header_cost_per_click"),
                                    sortable: ""
                                }
                            }, [e._v(e._s(null !== t.row.cpc ? e.getCurrencySymbol() + " " + e.setLocaleString(t.row.cpc) : "-"))]), a("b-table-column", {
                                attrs: {
                                    field: "schedule",
                                    "data-field": "schedule",
                                    label: e.$t("MY_CAMPAIGNS.header_schedule"),
                                    sortable: ""
                                }
                            }, [e._v(e._s(null !== t.row.schedule ? t.row.schedule : "-"))])]
                        }
                    }])
                })], 1)
            }, o = [], i = a("8d2e"), r = i["a"], s = (a("d317"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, null, null);
        t["a"] = c.exports
    }, f2a6: function (e, t, a) {
    }, f2e5: function (e, t, a) {
    }, f309: function (e, t, a) {
    }, f391: function (e, t) {
        e.exports = {
            en: {
                ad_setting: "Ad settings",
                add_budget: "Add budget",
                campaign_goal_text: "What would you like to advertize:\nYour website or your mobile app?",
                campaign_left: "This will end <span>{days}.</span>",
                campaign_left_0: "today",
                campaign_left_1: "tomorrow",
                campaign_left_2: "in 2 days",
                campaign_left_3: "in 3 days",
                check_plan_confirm: "Please check Schedule & Budget before launching this campaign.",
                copy: "Copy",
                copy_confirm: "Are you sure you want to copy?",
                date_format: "[created on] MMM D YYYY",
                deactivate_confirm: "Are you sure you want to deactivate this campaign?",
                deactivate_confirm_warning: "Are you pausing the ad because of low conversion rate?<br>This campaign is still at a learning phase.<br>It can take up to 3 days to show optimized results.",
                delete: "Delete",
                delete_campaign: "Delete campaign",
                delete_confirm: "Are you sure you want to delete this campaign?<br>Once deleted, you won't be able to get it back.",
                express: "Create for me",
                fail_to_export_excel: "We weren't able to generate the excel file",
                filter: "Filter",
                go_to_snb: "Go to Schedule & Budget",
                header_campaign_title: "Campaign title",
                header_channel: "Channel",
                header_cost_per_click: "Cost per click",
                header_ctr: "CTR",
                header_daily_budget: "Daily Budget",
                header_objective: "Objective",
                header_result: "Result",
                header_schedule: "Schedule",
                header_total_cost: "Total cost",
                new_campaign: "New campaign",
                new_campaign_by_adriel: "Make Adriel do it",
                new_campaign_copy_content: "Copy",
                new_campaign_create: "Start",
                new_campaign_detail_content: "Advanced",
                new_campaign_easy_content: "Quick & Simple",
                new_campaign_select_campaign: "Start",
                new_campaign_title: "How would you like to start?",
                no_campaign: "You haven't created any campaigns yet\nClick on the icon to create a new campaign.",
                objective_app_install: "App install",
                objective_conversion: "Conversion",
                objective_lead_form: "Customer leads",
                objective_link_click: "Link click",
                results: "Results",
                review_warn: "This campaign is being reviewed. It will become active as soon as the review is done and you'll be able to see the campaign results.",
                see_result: "Ad results",
                select_all: "Select All",
                status_active: "Active",
                status_all: "All",
                status_deactivated: "Deactivated",
                status_in_progress: "Draft",
                status_in_review: "In review",
                status_in_review_manager: "In review",
                status_in_review_express: "In review by our team",
                status_onboarding: "Draft",
                status_proposal_review: "Proposal Review",
                status_running: "Running",
                status_sample: "Sample",
                status_destroyed: "Deleted",
                stop_button: "Stop",
                stop_survey_reason_launch_another: "I�셝 like to launch another campaign.",
                stop_survey_reason_need_more_guide: "Need more guidance to understand the service.",
                stop_survey_reason_no_longer_needed: "Campaign no longer needed.",
                stop_survey_reason_not_enough_conversion: "Ads did not generate conversions up to expectation. (not enough sales, etc)",
                stop_survey_reason_not_enough_traffic: "Ads did not generate traffic up to expectation. (not enough clicks)",
                stop_survey_reason_others: "Others:",
                stop_survey_title: "I'm stopping this campaign because:",
                stop_survey_warning: "Please choose one or more answers",
                tab_all: "All",
                tab_deactivated: "Deactivated",
                tab_in_progress: "Draft",
                tab_in_proposal_review: "Proposal review",
                tab_in_review: "In review",
                tab_running: "Active",
                top_title: "My Campaigns"
            },
            jp: {
                ad_setting: "佯껃몜鼇�츣",
                add_budget: "雅덄츞�믦옙�졼걲��",
                campaign_left: "�볝굦��<span>{days}.</span>�㎫탞雅녴걮�얇걲��",
                campaign_left_0: "餓딀뿥",
                campaign_left_1: "�롦뿥",
                campaign_left_2: "雅뚧뿥餓ε냵",
                campaign_left_3: "訝됪뿥餓ε냵",
                check_plan_confirm: "��깵�녈깪�쇈꺍�띴에�믧뼀冶뗣걲�뗥뎺�ャ�곦틛嶸쀣겏�밤궞�멥깷�쇈꺂�믥▶沃띲걮�╉걦�졼걬�꾠��",
                copy: "�녈깞��",
                copy_confirm: "�녈깞�쇈걮�╉굚�덀굧�쀣걚�㎯걲�뗰폕",
                date_format: "[created on] MMM D YYYY",
                deactivate_confirm: "��깵�녈깪�쇈꺍�띴에�믣걶閭㏂걮�╉굚�덀굧�쀣걚�㎯걲�뗰폕",
                deactivate_confirm_warning: "佯껃몜�먩옖�뚥퐥�뤷틕�듽굮訝�뼪�뺛굦�뗣굯�㎯걲�뗣�� �얍쑉佯껃몜�뚦�玲믢릎�㎯걗�듽�곫��⒴뙑�뺛굦�뗣겲�㎪�鸚�3�γ걣�뗣걢�듽겲�쇻��",
                delete: "�딃솮�쇻굥",
                delete_confirm: "�볝겗��깵�녈깪�쇈꺍�믣뎷�ㅳ걮�╉굚�덀굧�쀣걚�㎯걲�뗰폕<br>訝�佯�뎷�ㅳ걲�뗣겏�곩쑴�껁걲�뗣걪�ⓦ겘�㎯걤�얇걵�볝��",
                express: "鵝쒏닇�쇻굥",
                go_to_snb: "�밤궞�멥깷�쇈꺂�ⓧ틛嶸쀣겓烏뚣걦",
                new_campaign: "�겹걮�꾠궘�ｃ꺍�싥꺖��",
                new_campaign_by_adriel: "",
                new_campaign_copy_content: "�℡춼��틕�듽궠�붵꺖",
                new_campaign_create: "獒썰퐳",
                new_campaign_detail_content: "�뺛궔�쇈깲壤℡폀�㎫쎍�θÞ若싥걮��＝鵝�",
                new_campaign_easy_content: "蘊ゅ븦嶺붷펯�㎫가�섅겓獒썰퐳",
                new_campaign_select_campaign: "佯껃몜�멩뒢",
                new_campaign_title: "�⒲굯�ゅ숱�㎩틕�듽굮獒썰퐳�쀣겲�쇻걢��",
                no_campaign: "You haven't created any campaigns yet\nClick on the icon to create a new campaign.",
                results: "永먩옖",
                review_warn: "�볝겗��깵�녈깪�쇈꺍����삡릎�㎯걲�귛��삠걣永귙굩�딀А寧с�곥궘�ｃ꺍�싥꺖�녕탳�쒌굮閻븃첀�쇻굥�볝겏�뚣겎�띲겲�쇻��",
                see_result: "佯껃몜永먩옖",
                status_active: "�띴에訝�",
                status_deactivated: "�쒏�訝�",
                status_in_progress: "訝뗦쎑��",
                status_in_review: "野⒵읅訝�",
                status_in_review_manager: "野⒵읅訝�",
                status_in_review_express: "�㏂깋�ゃ궓�ャ겗�곥꺖�졼걣野⒵읅訝�겎�쇻��",
                status_onboarding: "訝뗦쎑��",
                status_sample: "�듐꺍�쀣꺂",
                stop_button: "佯껃몜訝�뼪",
                stop_survey_reason_launch_another: "餓뽧겗��깵�녈깪�쇈꺍��뀓岳▲굮�뗥쭓�쇻굥��",
                stop_survey_reason_need_more_guide: "�㏂깋�ゃ궓�ャ굮�⑴뵪�쇻굥��겓�덀굤屋녕눗�ゃ궗�ㅳ��녈궧�뚦퓚誤곥겎�쇻��",
                stop_survey_reason_no_longer_needed: "�볝겗��깵�녈깪�쇈꺍��걪�뚥빳訝딃뀓岳▲걲�뗥퓚誤곥겘�귙굤�얇걵�볝��",
                stop_survey_reason_not_enough_conversion: "�볝겗佯껃몜��쐿孃끹걬�뚣걼�겹겗�녈꺍�먦꺖�멥깾�녈굮�잆��볝겏�뚣겎�띲겲�쎼굯�㎯걮�잆�� (堊뗰폏�곩늽�ゃ궩�쇈꺂�밤겒��)",
                stop_survey_reason_not_enough_traffic: "�볝겗佯껃몜��쐿孃끹걬�뚣걼�겹겗�덀꺀�뺛궍�껁궚�믥뵟���볝겏�뚣겎�띲겲�쎼굯�㎯걮�잆�� (堊뗰폏�곩늽�ゃ궚�ゃ긿��빊�ゃ겑)",
                stop_survey_reason_others: "�앫겗餓뽳폏",
                stop_survey_title: "�볝겗��깵�녈깪�쇈꺍�믣걶閭㏂걮�얇걲�귙겒�쒌겒�됵폑",
                stop_survey_warning: "��鵝롣��ㅳ겗�욅춸�믧겦�볝겎�뤵걽�뺛걚��",
                tab_all: "�ⓦ겍",
                tab_deactivated: "�쒏�訝�",
                tab_in_progress: "訝뗦쎑��",
                tab_in_proposal_review: "",
                tab_in_review: "野⒵읅訝�",
                tab_running: "�띴에訝�",
                top_title: "�욁궎��깵�녈깪�쇈꺍"
            },
            ko: {
                ad_setting: "愿묎퀬 �ㅼ젙 蹂닿린",
                add_budget: "�쇱씪 �덉궛 異붽�",
                campaign_goal_text: "臾댁뾿�� 愿묎퀬�섏떆寃좎뼱��?\n�뱀궗�댄듃 �먮뒗 紐⑤컮�� ��",
                campaign_left: "<span>{days}</span> 愿묎퀬媛� 醫낅즺�쇱슂!",
                campaign_left_0: "�ㅻ뒛",
                campaign_left_1: "�댁씪",
                campaign_left_2: "2�쇰뮘",
                campaign_left_3: "3�쇰뮘",
                check_plan_confirm: "愿묎퀬瑜� 吏묓뻾�섏떆湲� �꾩뿉 �쇱젙怨� �덉궛�� �뺤씤�댁＜�몄슂.",
                copy: "蹂듭궗",
                copy_confirm: "愿묎퀬瑜� 蹂듭궗�섏떆寃좎뒿�덇퉴?",
                date_format: "YYYY[��] MM[��] D[�쇱뿉 �쒖옉]",
                deactivate_confirm: "愿묎퀬 吏묓뻾�� 以묐떒�섏떆寃좎뒿�덇퉴?",
                deactivate_confirm_warning: "愿묎퀬 �깃낵媛� ��븘 愿묎퀬瑜� 以묐떒�섏떆�� 嫄닿���?<br>�꾩옱 愿묎퀬媛� �숈뒿 以묒뿉 �덉쑝硫�,<br>理쒖쟻�붾릺湲곌퉴吏� 理쒕� 3�쇱씠 嫄몃┰�덈떎.",
                delete: "��젣",
                delete_campaign: "愿묎퀬 ��젣",
                delete_confirm: "愿묎퀬瑜� ��젣�섏떆寃좎뒿�덇퉴?<br>��젣�� 愿묎퀬�� �ㅼ떆 蹂듭썝�섏� �딆뒿�덈떎. ",
                fail_to_export_excel: "�묒� �뚯씪 異붿텧�� �ㅽ뙣�덉뒿�덈떎.",
                filter: "�꾪꽣",
                go_to_snb: "�쇱젙 諛� �덉궛 蹂닿린",
                header_campaign_title: "愿묎퀬 �쒕ぉ",
                header_channel: "梨꾨꼸",
                header_cost_per_click: "�대┃�� 鍮꾩슜",
                header_ctr: "�대┃��",
                header_daily_budget: "�� �덉궛",
                header_objective: "愿묎퀬 紐⑺몴",
                header_result: "寃곌낵",
                header_schedule: "�쇱젙",
                header_total_cost: "珥� 鍮꾩슜",
                new_campaign: "�� 愿묎퀬 留뚮뱾湲�",
                new_campaign_by_adriel: "�꾨뱶由ъ뿕�먭쾶 遺��곹븯湲�",
                new_campaign_copy_content: "湲곗〈 愿묎퀬 蹂듭궗",
                new_campaign_create: "�쒖옉",
                new_campaign_detail_content: "怨좉툒 �ㅼ젙",
                new_campaign_easy_content: "媛꾨떒 �ㅼ젙",
                new_campaign_select_campaign: "愿묎퀬�좏깮",
                new_campaign_title: "�대뼡 �뺤떇�쇰줈 愿묎퀬瑜� �쒖옉�섏떆寃좎뒿�덇퉴?",
                no_campaign: "�꾩쭅 �꾨Т 愿묎퀬�� 留뚮뱾吏� �딆쑝�④뎔��!\n�� 踰꾪듉�� �대┃�섏뿬 �덈줈�� 愿묎퀬瑜� �앹꽦�댁＜�몄슂.",
                objective_app_install: "�� �ㅼ튂",
                objective_conversion: "�꾪솚",
                objective_lead_form: "怨좉컼 DB �섏쭛",
                objective_link_click: "留곹겕 �대┃",
                results: "愿묎퀬 寃곌낵 蹂닿린",
                review_warn: '愿묎퀬媛� 寃��좊릺怨� �덉뒿�덈떎. 寃��좉� �꾨즺�섎뒗��濡� 愿묎퀬 �곹깭媛� "吏묓뻾 以�"�쇰줈 �꾪솚�섎ŉ, 愿묎퀬 吏묓뻾 �꾪솴�� �뺤씤�섏떎 �� �덉뒿�덈떎.',
                see_result: "寃곌낵 �꾪솴 蹂닿린",
                select_all: "�꾩껜 �좏깮",
                status_active: "吏묓뻾 以�",
                status_all: "�꾩껜",
                status_deactivated: "醫낅즺",
                status_in_progress: "�꾩떆 ���λ맖",
                status_in_review: "寃��� 以� | 理쒕� 24�쒓컙",
                status_in_review_manager: "寃��� 以�",
                status_in_review_express: "�꾨뱶由ъ뿕�� �쒖옉 以�",
                status_onboarding: "�꾩떆 ���λ맖",
                status_running: "吏묓뻾 以�",
                status_sample: "�섑뵆 愿묎퀬",
                status_destroyed: "��젣�� 罹좏럹��",
                stop_button: "愿묎퀬 以묐떒�섍린",
                stop_survey_reason_launch_another: "�ㅻⅨ 愿묎퀬 吏묓뻾 �덉젙",
                stop_survey_reason_need_more_guide: "愿묎퀬 �깃낵瑜� �댄빐�섍린 �대젮�뚯꽌",
                stop_survey_reason_no_longer_needed: "�� �댁긽 愿묎퀬瑜� 吏묓뻾�섏� �딆븘�� �섏뼱��",
                stop_survey_reason_not_enough_conversion: "愿묎퀬 �꾪솚�� 諛� 留ㅼ텧 �깃낵媛� 醫뗭� �딆븘��",
                stop_survey_reason_not_enough_traffic: "愿묎퀬 �대┃ �� 諛� �몃옒�� �깃낵媛� 醫뗭� �딆븘��",
                stop_survey_reason_others: "�� �몄뿉 �ㅻⅨ �댁쑀:",
                stop_survey_title: "�꾨뱶由ъ뿕��혻愿묎퀬二쇰떂�ㅼ쓽혻�댁빞湲곗뿉혻洹�혻湲곗슱�대ŉ\n �� �섏� �쒕퉬�ㅻ� 留뚮뱾湲� �꾪빐혻�몃젰�섍퀬혻�덉뒿�덈떎.\n�대뼡혻�댁쑀濡쑣좉킅怨좊�혻以묐떒�섏뀲�섏슂?",
                stop_survey_warning: "�꾩닔 ��ぉ�낅땲��.",
                tab_all: "紐⑤뱺 愿묎퀬",
                tab_deactivated: "醫낅즺�� 愿묎퀬",
                tab_in_progress: "�꾩떆 ���λ맂 愿묎퀬",
                tab_in_review: "寃��� 以묒씤 愿묎퀬",
                tab_running: "吏묓뻾 以묒씤 愿묎퀬",
                top_title: "�� 愿묎퀬"
            }
        }
    }, f59a: function (e, t, a) {
        "use strict";
        var n = a("25a5"), o = a.n(n);
        o.a
    }, f7d3: function (e, t, a) {
    }, f87c: function (e, t, a) {
        "use strict";
        var n = a("7549"), o = a.n(n);
        o.a
    }, f88b: function (e, t) {
        e.exports = {
            en: {
                account_modal_confirm_add: "Add",
                account_modal_confirm_edit: "Update",
                account_modal_header_add: "Add an account",
                account_modal_header_edit: "Edit an account",
                add_coupon: "Add a coupon",
                can_change_account_after_finish: "You can switch to another account once you finish updating your campaign.",
                cannot_delete_account: "You either have running campaigns or unsettled payments. In order to delete an account, please deactivate all the running campaigns and settle remaining payments.",
                change_payment: "Use another payment method",
                company: "Company",
                country: "Country",
                coupon_modal_info: "Enter coupon code",
                coupon_modal_title: "Adriel Ad Coupon",
                coupon_title: "Adriel Coupons",
                default_account: "default",
                delete_account: "Delete user",
                delete_account_cancel_btn: "Delete user",
                delete_account_content: "Deleting this user is permanent and will result in losing all contents, including your campaigns, performance results and profile settings. If you'd like to delete a specific campaign, you can do so on the My Campaigns page. Are you sure you want to delete this user?",
                delete_account_enter_password: "Enter password",
                delete_account_fail_password: "Please check your password again.",
                delete_account_title: "Are you sure you want to delete this user?",
                email: "Email",
                email_already_used: "There is already a user using this email address.",
                email_frequency: "Email frequency",
                email_frequency_d: "Daily",
                email_frequency_m: "Monthly",
                email_frequency_n: "No, thanks",
                email_frequency_w: "Weekly",
                email_report: "Email Report",
                invalid_card_number: "Your card number is invalid.",
                invalid_coupon: "The coupon is invalid.",
                lang_setting: "Language Settings",
                name: "Name",
                no_coupons: "There is no coupon registered yet.",
                no_payment_method: "There is no payment method registered yet.",
                payment_add_method: "Add Payment Method",
                payment_card_number: "Card Number",
                payment_confirm: "You are trying to remove your payment method. This will terminate your running campaigns and you will be charged the remaining balance.\n Are you sure?",
                payment_cvc: "CVC",
                payment_expire: "Expiration Date",
                payment_remove_confirm: "Are you sure you want to remove this payment method?",
                payment_setting_title: "Payment Settings",
                profile: "Your Profile",
                set_as_default_account: "Set this account as default",
                setting_change_password: "Change password",
                setting_save_profile: "Save profile",
                setting_title: "User Settings",
                wanna_delete_account: "Do you want to delete this account? You cannot undo this action.",
                unpaid_campaigns: "We cannot remove your credit card because there is a pending amount.\n If you recently stopped your campaign, please try again in 4 hours."
            },
            jp: {
                add_coupon: "��꺖�앫꺍�믣뀯�뚣겍�뤵걽�뺛걚��",
                cannot_delete_account: "�띴에訝�겗��깵�녈깪�쇈꺍�곥겲�잆겘��돂�꾠겗歷덀굯�㎯걚�ゃ걚��깵�녈깪�쇈꺍�뚣걗�듽겲�쇻�귙궋�ャ궑�녈깉�믣뎷�ㅳ걲�뗣겓���곩뀲��깵�녈깪�쇈꺍�믣걶閭℡풄�곫뵱�뺛걚�믡걡歷덀겲�쎼걦�졼걬�꾠��",
                change_payment: "餓뽧겗�딀뵱�뺛걚�방퀡�믧겦�욁걮�얇걲��",
                company: "鴉싩ㅎ��",
                country: "��",
                coupon_modal_info: "��꺖�앫꺍�녈꺖�됥굮�γ굦�╉걦�졼걬�꾠��",
                coupon_modal_title: "�㏂깋�ゃ궓�ャ���㏂깋��꺖�앫꺍",
                coupon_title: "�㏂깋�ゃ궓�ャ궚�쇈깮��",
                delete_account: "�㏂깋�ゃ궓�ャ겗�㏂궖�╉꺍�덀굮�딃솮",
                delete_account_cancel_btn: "�㏂궖�╉꺍�덀굮�딃솮",
                delete_account_content: "訝�佯╉궋�ャ궑�녈깉�믣뎷�ㅳ걲�뗣겏�곥깤��깢�▲궎�ャ꺕��깵�녈깪�쇈꺍�끻젿�삯뀓岳▼츪潁얏깄�긱굮�ャ��곩뀲�╉겗�녈꺍�녴꺍�꾠걣�딃솮�뺛굦�얇걲�귙궘�ｃ꺍�싥꺖�녈걽�묆굮�딃솮�쇻굥�닷릦���곥깯�ㅳ궘�ｃ꺍�싥꺖�녈깳�뗣깷�쇈겎�딃솮�뚦룾�썬겎�쇻�귙궋�ャ궑�녈깉�믣뎷�ㅳ걮�╉굚�덀굧�쀣걚�㎯걲�뗰폕",
                delete_account_enter_password: "�묆궧��꺖�됥굮�γ굦�╉걦�졼걬�꾠��",
                delete_account_fail_password: "�묆궧��꺖�됥굮閻븃첀�쀣겍�뤵걽�뺛걚��",
                delete_account_title: "�㏂궖�╉꺍�덀굮�딃솮�쀣겍�귙굠�띲걮�꾠겎�쇻걢?",
                email: "E�▲꺖��",
                email_frequency: "E�▲꺖�� �뺛꺁��궓�녈궥��",
                email_frequency_d: "Daily",
                email_frequency_m: "Monthly",
                email_frequency_n: "No, thanks",
                email_frequency_w: "Weekly",
                email_report: "E�▲꺖�� �с깮�쇈깉",
                invalid_card_number: "��꺃�멥긿�덀궖�쇈깋�ゅ뤇�뚨꽒�밤겎�쇻��",
                invalid_coupon: "�볝겗��꺖�앫꺍��꽒�밤겎�쇻��",
                lang_setting: "鼇�沃욆Þ若�",
                name: "�띶뎺",
                no_coupons: "��꺖�앫꺍�뚨쇉�꿔걬�뚣겍�꾠겲�쎼굯��",
                no_payment_method: "�딀뵱�뺛걚�방퀡�뚨쇉�꿔걬�뚣겍�꾠겲�쎼굯��",
                payment_add_method: "黎뷸툑�ャ꺖�됥겗瓦썲뒥",
                payment_card_number: "��꺃�멥긿�덀궖�쇈깋�ゅ뤇",
                payment_confirm: "�딀뵱�뺛걚�방퀡�믣뎷�ㅳ걲�뗣겏�곲뀓岳▽릎��궘�ｃ꺍�싥꺖�녈걣�쒏��쀣�곥걹�뚣겲�㎯겓鵝욍겂�잋틛嶸쀣걣獄뗦콆�뺛굦�얇걲�귙걡��돂�꾣뼶力뺛굮�딃솮�쀣겍�귙굠�띲걮�꾠겎�쇻걢竊�",
                payment_cvc: "CVC",
                payment_expire: "�됧듅�잓솏",
                payment_remove_confirm: "�볝겗�딀뵱�뺞뼶力뺛굮�딃솮�쀣겍�귙굠�띲걮�꾠겎�쇻걢竊�",
                payment_setting_title: "�딀뵱�뺛걚�방퀡鼇�츣",
                profile: "�쀣꺆�뺛궊�ㅳ꺂",
                setting_change_password: "�묆궧��꺖�됥굮鸚됥걟�╉걦�졼걬��",
                setting_save_profile: "�쀣꺆�뺛궊�ㅳ꺂�믢퓷耶�",
                setting_title: "�㏂궖�╉꺍�덅Þ若�",
                unpaid_campaigns: "�딀뵱�뺛걚��툑�볝겎�꾠겒�꾠궘�ｃ꺍�싥꺖�녈걣�귙굤�얇걲��"
            },
            ko: {
                account_modal_confirm_add: "異붽��섍린",
                account_modal_confirm_edit: "�섏젙�섍린",
                account_modal_header_add: "怨꾩젙 異붽��섍린",
                account_modal_header_edit: "怨꾩젙 �섏젙�섍린",
                add_coupon: "荑좏룿 異붽��섍린",
                can_change_account_after_finish: "愿묎퀬 �쒖옉 �꾨즺 �� 怨꾩젙 蹂�寃쎌씠 媛��ν빀�덈떎.",
                cannot_delete_account: "�꾩옱 吏묓뻾 以묒씤 愿묎퀬媛� �덇굅��, �꾨즺�섏� �딆� 寃곗젣 嫄댁씠 �덉쓣 寃쎌슦 怨꾩젙 ��젣媛� 遺덇��ν빀�덈떎.",
                change_payment: "�ㅻⅨ 寃곗젣 �섎떒 �ъ슜",
                company: "湲곗뾽紐�",
                country: "援��",
                coupon_modal_info: "荑좏룿 肄붾뱶瑜� �낅젰�섏꽭��",
                coupon_modal_title: "�꾨뱶由ъ뿕 愿묎퀬 荑좏룿",
                coupon_title: "�꾨뱶由ъ뿕 荑좏룿",
                default_account: "湲곕낯",
                delete_account: "�ъ슜�� ��젣",
                delete_account_cancel_btn: "�ъ슜�� ��젣",
                delete_account_content: "�ъ슜�먮� ��젣�섏떆硫� �앹꽦�� 紐⑤뱺 愿묎퀬 諛� 愿��� �곗씠�곌� ��젣�섎ŉ, �� 踰� ��젣�� �곗씠�곕뒗 �ㅼ떆 蹂듦뎄�섎뒗 寃껋씠 遺덇��ν빀�덈떎. �뱀젙 愿묎퀬 ��젣瑜� �щ쭩�섏떊�ㅻ㈃ �� 愿묎퀬�먯꽌 �대떦 愿묎퀬瑜� 吏��뚯＜�몄슂. �뺣쭚 �ъ슜�먮� ��젣�섏떆寃좎뒿�덇퉴?",
                delete_account_enter_password: "鍮꾨�踰덊샇 �낅젰",
                delete_account_fail_password: "鍮꾨�踰덊샇瑜� �ㅼ떆 �뺤씤�댁＜�몄슂.",
                delete_account_title: "�ъ슜�먮� ��젣�섏떆寃좎뒿�덇퉴?",
                email: "�대찓��",
                email_already_used: "�대� �깅줉�� �대찓�� 二쇱냼�낅땲��.",
                email_frequency: "�섏떊 鍮덈룄",
                email_frequency_d: "留ㅼ씪",
                email_frequency_m: "留ㅼ썡",
                email_frequency_n: "�섏떊�섏� �딆쓬",
                email_frequency_w: "留ㅼ＜",
                email_report: "�대찓�� 由ы룷��",
                invalid_card_number: "�좏슚�� 移대뱶 踰덊샇瑜� �낅젰�댁＜�몄슂.",
                invalid_coupon: "荑좏룿�� �좏슚�섏� �딆뒿�덈떎.",
                lang_setting: "�몄뼱 �ㅼ젙",
                name: "�대쫫",
                no_coupons: "�꾩쭅 �깅줉�� 荑좏룿�� �놁뒿�덈떎.",
                no_payment_method: "�꾩쭅 �깅줉�� 寃곗젣�섎떒�� �놁뒿�덈떎.",
                payment_add_method: "寃곗젣 �섎떒 �낅젰",
                payment_card_number: "移대뱶踰덊샇",
                payment_confirm: "寃곗젣 �섎떒�� ��젣�섏떆寃좎뒿�덇퉴? �뺤씤 踰꾪듉�� �꾨Ⅴ�쒕㈃ 吏묓뻾 以묒씤 愿묎퀬媛� 醫낅즺�섎ŉ, \n �ㅻ뒛源뚯��� 寃곗젣 湲덉븸�� 泥�뎄�⑸땲��. ",
                payment_cvc: "CVC 肄붾뱶",
                payment_expire: "�좏슚湲곌컙 留뚮즺��",
                payment_remove_confirm: "�대떦 寃곗젣 �섎떒�� ��젣�섏떆寃좎뒿�덇퉴?",
                payment_setting_title: "寃곗젣 �ㅼ젙",
                set_as_default_account: "湲곕낯 怨꾩젙�쇰줈 �ㅼ젙",
                profile: "�ъ슜�� �뺣낫",
                setting_change_password: "鍮꾨�踰덊샇 蹂�寃�",
                setting_save_profile: "����",
                setting_title: "�ъ슜�� �ㅼ젙",
                unpaid_campaigns: "誘멸껐�� 湲덉븸�쇰줈 �명빐 移대뱶 �뺣낫 ��젣媛� 遺덇��ν빀�덈떎. \n 諛⑷툑 愿묎퀬瑜� 以묐떒�섏뀲�ㅻ㈃, 4�쒓컙 �꾩뿉 �ㅼ떆 �쒕룄�� 二쇱꽭��. ",
                wanna_delete_account: "�대떦 怨꾩젙�� ��젣�섏떆寃좎뒿�덇퉴? ��젣�� 怨꾩젙�� 蹂듦뎄�� �� �놁뒿�덈떎."
            }
        }
    }, f9be: function (e, t, a) {
        "use strict";
        (function (e) {
            t["a"] = {
                props: {checked: {type: Boolean, default: !1}},
                computed: {src: e.compose(e.concat("/img/"), e.ifElse(e.propEq("checked", !0), e.always("checkbox_active.png"), e.always("checkbox_default.png")))}
            }
        }).call(this, a("b17e"))
    }, f9e7: function (e, t, a) {
    }, fa7d: function (e, t, a) {
        "use strict";
        (function (e) {
            a.d(t, "U", function () {
                return Y
            }), a.d(t, "T", function () {
                return V
            }), a.d(t, "W", function () {
                return z
            }), a.d(t, "K", function () {
                return W
            }), a.d(t, "fb", function () {
                return ne
            }), a.d(t, "l", function () {
                return te
            }), a.d(t, "cb", function () {
                return re
            }), a.d(t, "E", function () {
                return se
            }), a.d(t, "M", function () {
                return H
            }), a.d(t, "I", function () {
                return K
            }), a.d(t, "L", function () {
                return J
            }), a.d(t, "R", function () {
                return ce
            }), a.d(t, "P", function () {
                return le
            }), a.d(t, "t", function () {
                return pe
            }), a.d(t, "e", function () {
                return me
            }), a.d(t, "p", function () {
                return fe
            }), a.d(t, "N", function () {
                return ge
            }), a.d(t, "O", function () {
                return we
            }), a.d(t, "Q", function () {
                return ee
            }), a.d(t, "V", function () {
                return Oe
            }), a.d(t, "v", function () {
                return be
            }), a.d(t, "B", function () {
                return ve
            }), a.d(t, "y", function () {
                return ye
            }), a.d(t, "Z", function () {
                return G
            }), a.d(t, "u", function () {
                return de
            }), a.d(t, "j", function () {
                return Ae
            }), a.d(t, "o", function () {
                return B
            }), a.d(t, "eb", function () {
                return Se
            }), a.d(t, "w", function () {
                return Pe
            }), a.d(t, "ab", function () {
                return Ie
            }), a.d(t, "d", function () {
                return je
            }), a.d(t, "J", function () {
                return Me
            }), a.d(t, "n", function () {
                return Re
            }), a.d(t, "F", function () {
                return Ne
            }), a.d(t, "db", function () {
                return De
            }), a.d(t, "Y", function () {
                return $e
            }), a.d(t, "h", function () {
                return Be
            }), a.d(t, "i", function () {
                return Fe
            }), a.d(t, "b", function () {
                return Ge
            }), a.d(t, "gb", function () {
                return oe
            }), a.d(t, "c", function () {
                return Ue
            }), a.d(t, "A", function () {
                return qe
            }), a.d(t, "S", function () {
                return Ye
            }), a.d(t, "k", function () {
                return Te
            }), a.d(t, "g", function () {
                return Ve
            }), a.d(t, "G", function () {
                return Je
            }), a.d(t, "H", function () {
                return xe
            }), a.d(t, "a", function () {
                return ze
            }), a.d(t, "f", function () {
                return We
            }), a.d(t, "D", function () {
                return ke
            }), a.d(t, "s", function () {
                return Ce
            }), a.d(t, "m", function () {
                return He
            }), a.d(t, "x", function () {
                return Ke
            }), a.d(t, "C", function () {
                return q
            }), a.d(t, "bb", function () {
                return Ze
            }), a.d(t, "z", function () {
                return et
            }), a.d(t, "X", function () {
                return tt
            }), a.d(t, "q", function () {
                return Qe
            }), a.d(t, "r", function () {
                return Xe
            });
            var n = a("b388"), o = a.n(n), i = a("7618"), r = a("15b8"), s = a.n(r), c = a("59ad"), l = a.n(c),
                d = a("e814"), u = a.n(d), p = a("3be2"), _ = a.n(p), m = (a("7f7f"), a("3b2b"), a("a745")), f = a.n(m),
                g = (a("ac6a"), a("5df3"), a("db0c")), h = a.n(g), b = (a("96cf"), a("3b8d")), v = a("cebc"),
                y = a("768b"), w = a("795b"), k = a.n(w),
                x = (a("aef6"), a("c5f6"), a("28a5"), a("a481"), a("f559"), a("dde5")), O = a("6612"), A = a.n(O),
                C = a("4c6b"), S = a("a5b83"), E = a("17f5"), T = a("5e53"), P = a("808d"), I = (a("6e77"), a("4b59")),
                L = a("87ee"), j = a("1585"), M = a("d792"), R = a("742d"), N = a("a6c5"),
                D = (a("ebb6"), a("5670"), a("3fab"), a("f59d"), a("d337"), a("4467")), $ = a.n(D), B = a("c437"),
                F = a("337f"), G = e.flip(e.binary($.a)), U = e.ifElse(e.identity, e.__, e.always(void 0)),
                q = e.path(["navigator", "msSaveBlob"], window),
                Y = U(e.ifElse(e.either(e.startsWith("https://app.adriel.ai/api"), e.startsWith("http")), e.identity, e.compose(e.concat("https://app.adriel.ai/api"), e.concat("/images")))),
                V = U(e.ifElse(e.startsWith("https://app.adriel.ai/api"), e.identity, e.compose(e.concat("https://app.adriel.ai/api"), e.concat("/files")))),
                z = U(e.ifElse(e.startsWith("https://app.adriel.ai") || e.startsWith("http"), e.identity, e.compose(e.concat("https://app.adriel.ai"))));

            function W(e) {
                var t = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return t.test(e)
            }

            var H = function () {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                e = e.toLowerCase();
                var t = ["facebook.com"];
                if (-1 === e.indexOf("http://") && -1 === e.indexOf("https://") && (e = "http://" + e), t.some(function (t) {
                        return e.indexOf(t) >= 0
                    })) return !1;
                var a = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
                return !!a.test(e)
            };

            function K(e) {
                return !!H(e) && (-1 !== e.indexOf("itunes.apple.com") || -1 !== e.indexOf("apps.apple.com"))
            }

            function J(e) {
                return !!H(e) && -1 !== e.indexOf("play.google.com")
            }

            var Q = e.concat("$ ");

            function X(e) {
                return isNaN(e) ? e : (e + "").replace(/\b(\d+)((\.\d+)*)\b/g, function (e, t, a) {
                    return (t.charAt(0) > 0 && !(a || ".").lastIndexOf(".") ? t.replace(/(\d)(?=(\d{3})+$)/g, "$1,") : t) + a
                })
            }

            function Z(t) {
                if (isNaN(t)) return t;
                if (t = String(t), t.indexOf(".") < 0) return t;
                t = t.split(".").map(e.split(""));
                while (0 == t[1][t[1].length - 1]) t[1].pop();
                return t[1].length && t[0].push("."), t[0].concat(t[1]).join("")
            }

            var ee = function () {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 2;
                    return e.compose(X, Z, e.ifElse(isNaN, e.identity, function (e) {
                        return e.toFixed(t)
                    }), e.ifElse(isNaN, e.identity, Number))
                }, te = ee(), ae = (e.compose(Q, te), e.ifElse(e.identity, e.identity, e.always(""))),
                ne = e.compose(e.converge(e.concat, [e.compose(e.toUpper, e.head), e.tail]), ae),
                oe = e.compose(e.join(" "), e.map(ne), e.split(" "), e.trim(), ae);

            function ie(e) {
                return null != e && !isNaN(e) && "undefined" !== typeof e.getDate
            }

            var re = function (e) {
                if (e) return ie(e || void 0) ? e : new Date(e)
            }, se = function (t, a, n) {
                return e.both(e.gte(e.__, a), e.lte(e.__, n))(t)
            }, ce = function (e) {
                return {
                    feed: "News Feed",
                    instagram_stories: "Story",
                    instagram_standard: "News Feed",
                    desktop_feed_standard: "News Feed"
                }[e] || e
            }, le = e.curry(function (t, a) {
                return e.anyPass(t.map(function (t) {
                    return e.endsWith(t)
                }))(a)
            });

            function de() {
                return new k.a(function (t, a) {
                    var n = Object(S["a"])(window.FB.getLoginStatus)().pipe(Object(I["a"])(), Object(L["a"])(e.propEq("status", "connected"))),
                        o = Object(y["a"])(n, 2), i = o[0], r = o[1];
                    i.pipe(Object(j["a"])("authResponse", "accessToken"), Object(M["a"])(function (e) {
                        return e ? Object(E["a"])(e) : Object(T["a"])()
                    })).subscribe(t, a), r.subscribe(a, a)
                })
            }

            function ue(t, a, n) {
                window.FB.login(function (n) {
                    var o = e.path(["authResponse", "accessToken"], n);
                    o ? x["p"].fbInfo(o).then(t, a) : a({code: "FB_LOGIN_ERROR"})
                }, Object(v["a"])({}, C["g"], {auth_type: n}))
            }

            function pe(t) {
                var a = t.token, n = t.authType;
                return new k.a(function (t, o) {
                    var i = function (a) {
                        var n = a.permissions.data.filter(function (e) {
                            return "declined" !== e.status
                        }).map(e.prop("permission")), i = ["manage_pages", "business_management", "ads_management"];
                        i.every(function (e) {
                            return n.indexOf(e) > -1
                        }) ? t(a) : o({code: "PERMISSIONS_MISSING", data: a.permissions.data})
                    };
                    a ? x["p"].fbInfo(a).then(i, o).catch() : ue(i, o, n)
                })
            }

            function _e(e) {
                return "\n" === e || (e = e.charCodeAt(), e >= 4352 && (e <= 4447 || 9001 === e || 9002 === e || 11904 <= e && e <= 12871 && 12351 !== e || 12880 <= e && e <= 19903 || 19968 <= e && e <= 42182 || 43360 <= e && e <= 43388 || 44032 <= e && e <= 55203 || 63744 <= e && e <= 64255 || 65040 <= e && e <= 65049 || 65072 <= e && e <= 65131 || 65281 <= e && e <= 65376 || 65504 <= e && e <= 65510 || 110592 <= e && e <= 110593 || 127488 <= e && e <= 127569 || 131072 <= e && e <= 262141))
            }

            var me = e.pipe(e.defaultTo(""), e.split(""), e.reduce(function (e, t) {
                return _e(t) ? e + 2 : e + 1
            }, 0)), fe = function () {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 3,
                    a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 500;
                return function (n) {
                    return n.pipe(Object(R["a"])(function (t, a) {
                        return {error: a, count: e.inc(t.count)}
                    }, {error: null, count: 0}), Object(N["a"])(function (e) {
                        var n = e.error, o = e.count;
                        return o > t ? Object(T["a"])(n) : Object(P["a"])(o * a)
                    }))
                }
            }, ge = e.both(e.complement(e.isNil), e.complement(e.isEmpty)), he = e.once(function () {
                gapi.auth2.init({
                    apiKey: "AIzaSyC2FcgGiOUP7W24bkNjOntUVQttW-ezJf4",
                    discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/analytics/v3/rest"],
                    clientId: "530609157666-cet4nhbec4uo9fuo5en09kq6ogglou68.apps.googleusercontent.com",
                    scope: "https://www.googleapis.com/auth/analytics https://www.googleapis.com/auth/analytics.manage.users"
                })
            }), be = function () {
                var t = Object(b["a"])(regeneratorRuntime.mark(function t() {
                    var a, n, o, i, r, s, c;
                    return regeneratorRuntime.wrap(function (t) {
                        while (1) switch (t.prev = t.next) {
                            case 0:
                                return he(), a = gapi.auth2.getAuthInstance(), t.next = 4, a.signIn({prompt: "select_account"});
                            case 4:
                                return t.next = 6, gapi.client.analytics.management.webproperties.list({accountId: "~all"});
                            case 6:
                                return n = t.sent, o = n.result, o = void 0 === o ? {} : o, i = o.username, r = void 0 === i ? "" : i, s = o.items, c = void 0 === s ? [] : s, t.abrupt("return", {
                                    username: r,
                                    properties: c.filter(e.compose(e.propEq("length", 2), e.intersection(["EDIT", "MANAGE_USERS"]), h.a, e.pathOr([], ["permissions", "effective"]))).map(e.pick(["accountId", "id", "name", "websiteUrl"]))
                                });
                            case 11:
                            case"end":
                                return t.stop()
                        }
                    }, t)
                }));
                return function () {
                    return t.apply(this, arguments)
                }
            }();

            function ve(e) {
                var t = e.accountId, a = e.propertyId;
                return k.a.all(["analytics2-4@seventh-country-251404.iam.gserviceaccount.com", "adrielanalytics2@gmail.com"].map(function (e) {
                    return new k.a(function (n, o) {
                        gapi.client.analytics.management.webpropertyUserLinks.insert({
                            accountId: t,
                            webPropertyId: a,
                            resource: {permissions: {local: ["EDIT", "MANAGE_USERS"]}, userRef: {email: e}}
                        }).execute(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            return e.error ? o() : n()
                        })
                    })
                }))
            }

            var ye = e.ifElse(f.a, e.head, e.identity);
            var we = e.compose(e.test(/^.*\.(avi|AVI|wmv|WMV|flv|FLV|mpg|MPG|mp4|MP4|mov)$/), e.toLower, e.defaultTo("")),
                ke = e.compose(e.test(/^.*\.(jpg|jpeg|png|svg)$/), e.toLower, e.defaultTo(""));

            function xe(e) {
                var t = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:\/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
                return t.test(e)
            }

            function Oe(t) {
                var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : ["image", "media"],
                    n = e.contains(e.__, a);
                return e.pipe(e.ifElse(e.identity, e.clone, e.always({})), e.mapObjIndexed(function (e, t) {
                    return n(t) ? Y(e) : e
                }))(t)
            }

            function Ae(e) {
                var t;
                return e ? (t = e.indexOf("//") > -1 ? e.split("/")[2] : e.split("/")[0], t = t.split("?")[0], t) : ""
            }

            function Ce(e) {
                if (e) return e.substring(e.lastIndexOf(".") + 1, e.length).toLowerCase()
            }

            function Se() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                    t = arguments.length > 1 ? arguments[1] : void 0;
                if ("string" !== typeof e) return e;
                var a = 0, n = 0, o = e.length;
                for (n = 0; n < o; n++) {
                    if (a += _e(e[n]) ? 2 : 1, a > t) {
                        n--;
                        break
                    }
                    if (a === t) break
                }
                return e.substr(0, n + 1)
            }

            function Ee(e, t) {
                t || (t = window.location.href), e = e.replace(/[\[\]]/g, "\\$&");
                var a = new RegExp("[?&]" + e + "(=([^&#]*)|&|#|$)"), n = a.exec(t);
                return n ? n[2] ? decodeURIComponent(n[2].replace(/\+/g, " ")) : "" : null
            }

            var Te = e.filter(e.identity);

            function Pe() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                    a = e.prop(["language"], t);
                if (a) return a;
                var n = e.contains(e.__, ["kr", "ko"]), o = "en", i = Ee("lang");
                if (n(i)) return "ko";
                if ("en" === i) return "en";
                if ("jp" === i) return "jp";
                var r = Le("lang");
                return n(r) ? "ko" : "en" === r ? "en" : "jp" === r ? "jp" : (a = e.compose(e.defaultTo(""), e.head, Te, e.props(["userLanguage", "language"]), e.prop("navigator"))(window), n(a) && (o = "ko"), o)
            }

            function Ie(e, t) {
                var a = window.localStorage;
                a && a.setItem(e, t)
            }

            function Le(e) {
                var t = window.localStorage;
                if (t) return t.getItem(e)
            }

            function je(e) {
                return e.charAt(0).toUpperCase() + e.slice(1)
            }

            function Me() {
                var e = F.getParser(window.navigator.userAgent), t = e.getBrowser(), a = t.name, n = t.version;
                return !("Internet Explorer" === a && Number(n) < 11)
            }

            function Re(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "0.0a",
                    a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "N/A",
                    n = _()(e) && e < 1e3 ? e : A()(e).format(t);
                return _()(Number(n)) ? u()(n) : isNaN(l()(n)) ? a : n
            }

            function Ne() {
                var e = !1;
                return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) && (e = !0), e
            }

            var De = e.ifElse(isNaN, e.always("-"), function (e) {
                return (100 * e).toFixed(2) + "%"
            }), $e = function (e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0, a = e.style,
                    n = l()(a.height);
                a.height = "5px";
                var o = e.scrollHeight;
                e.value && !e.value.trim() && (o += 5), t && se(o, n - t, n + t) && (o = n), a.height = "".concat(o, "px")
            };

            function Be(e) {
                var t = e.uri, a = e.name, n = e.blob;
                if (q) t && window.open(t, "_blank"), n && window.navigator.msSaveOrOpenBlob(n, a); else {
                    var o = document.createElement("a");
                    o.download = a, o.href = n ? URL.createObjectURL(n) : t, document.body.appendChild(o), o.click(), o.remove()
                }
            }

            function Fe() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                return /[\��\��\��\��|$&~=\\\/��@+*!?({[\]})<>�밟�봔ヂ�.;:^�섃�쇺�쒋��'",���`쨌\�™�졻�≤겸�냈≤욋��#�뽂련�%��\�믠�굿뜯�꿎�는�_�뼿�]+$/.test(e)
            }

            var Ge = e.ifElse(e.test(/https?:\/\//gi), e.identity, e.concat("http://"));

            function Ue() {
                e.when(e.identity, G("blur"))(window.document.activeElement)
            }

            function qe(e) {
                return ge(e) ? l()(getComputedStyle(e, null).height.replace("px", "")) : 0
            }

            function Ye(e, t) {
                if (!ge(e)) return 0;
                if (!0 === t) {
                    var a = e.offsetWidth, n = getComputedStyle(e);
                    return a += u()(n.marginLeft) + u()(n.marginRight), a
                }
                return e.offsetWidth
            }

            function Ve() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                e.parentNode && e.parentNode.removeChild(e)
            }

            function ze() {
                for (var t = arguments.length, a = new Array(t), n = 0; n < t; n++) a[n] = arguments[n];
                return e.all(ge, a)
            }

            function We(e) {
                return e ? (s()(e), h()(e).forEach(function (e) {
                    !e || "object" !== Object(i["a"])(e) && "function" !== typeof e || o()(e) || We(e)
                }), e) : e
            }

            function He(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2;
                if (!e) return "0 Bytes";
                var a = 1024, n = t < 0 ? 0 : t, o = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                    i = Math.floor(Math.log(e) / Math.log(a));
                return l()((e / Math.pow(a, i)).toFixed(n)) + " " + o[i]
            }

            function Ke(e) {
                var t = Object(y["a"])(e, 1), a = t[0], n = a.intersectionRatio;
                return n > 0 && window.scrollY > 0
            }

            function Je() {
                return window.innerWidth <= 800
            }

            function Qe(t) {
                var a = e.contains(e.__, t.subChannels);
                return "google" === t.channel ? a("search") ? "MANAGER.channel_google_search_campaign" : a("display") ? "MANAGER.channel_google_display_campaign" : "MANAGER.channel_google_campaign" : "facebook" === t.channel ? a("facebook") && a("instagram") ? "MANAGER.channel_facebook_instagram_campaign" : a("instagram") ? "MANAGER.channel_instagram_campaign" : "MANAGER.channel_facebook_campaign" : void 0
            }

            function Xe(e) {
                var t = e.channel, a = void 0 === t ? "" : t, n = e.subChannels, o = void 0 === n ? [] : n,
                    i = e.creativeType, r = void 0 === i ? "" : i;
                if (a && ge(o) && r) return "MANAGER.channel_".concat(a, "_").concat(o[0], "_").concat(r, "_campaign")
            }

            var Ze = e.curry(function (t, a) {
                return e.pathSatisfies(e.both(e.test(new RegExp(t, "gi")), e.propEq("length", t.length)), ["target", "tagName"], a)
            }), et = e.compose(e.head, e.defaultTo("")), tt = function (e, t) {
                return Math.floor(Math.random() * (t - e + 1) + e)
            }
        }).call(this, a("b17e"))
    }, fb1d: function (e, t, a) {
        "use strict";
        (function (e) {
            var n = a("2638"), o = a.n(n), i = a("cebc"), r = a("4a1d"), s = a("2f62"), c = a("54da"), l = a("808d"),
                d = a("1585"), u = a("4355"), p = (a("0041"), ["ONBOARDING", "EMPTY", "COPY"]), _ = a("f13c"), m = {
                    container: "#modal-container",
                    easing: "ease-in",
                    offset: 0,
                    force: !0,
                    cancelable: !0,
                    x: !1,
                    y: !0
                };
            t["a"] = {
                render: function (e) {
                    var t = this;
                    if ("GOAL" !== this.mode) {
                        var a = {
                            attrs: {active: this._active, canCancel: ["escape", "outside"]},
                            on: {
                                "update:active": function (e) {
                                    return t._active = e
                                }
                            }
                        };
                        return e("b-modal", o()([{}, a, {class: "modal new-campaign-modal"}]), [e("div", {
                            class: "new-campaign-container",
                            attrs: {id: "modal-container"}
                        }, [e("img", {
                            attrs: {src: "/img/icons/modal_close_btn.png"},
                            class: "close-btn",
                            on: {
                                click: function () {
                                    return t._active = !1
                                }
                            }
                        }), e("div", {class: "new-campaign-container--title"}, [this.$t("MY_CAMPAIGNS.new_campaign_title")]), e("div", {class: "new-campaign-container--boxes"}, [p.map(this.renderBox)]), this.renderCampaigns()])])
                    }
                    var n = {
                        attrs: {active: !0}, on: {
                            "update:active": function (e) {
                                e || (t.mode = "BASIC")
                            }, confirm: this.createEmptyCampaign
                        }
                    };
                    return e(u["a"], o()([{}, n]))
                },
                props: {active: {type: Boolean, default: !1}},
                computed: {
                    _active: {
                        get: e.prop("active"), set: function (e) {
                            this.campaigns = [], this.$emit("update:active", e)
                        }
                    }
                },
                data: function () {
                    return {mode: "BASIC", campaigns: []}
                },
                methods: Object(i["a"])({}, Object(s["mapActions"])("view", ["setLoadingAdriel"]), {
                    renderBox: function (t) {
                        var a = this, n = this.$createElement, o = [];
                        switch (t) {
                            case"ONBOARDING":
                                o = [this.$t("MY_CAMPAIGNS.new_campaign_easy_content"), "onboarding", function () {
                                    a._active = !1, a.$router.push("/onboarding?c=webOrApp")
                                }, this.$t("PROPOSAL.create_by_onboarding_tooltip")];
                                break;
                            case"EMPTY":
                                o = [this.$t("MY_CAMPAIGNS.new_campaign_detail_content"), "empty", this.setGoalMode, "/img/common/create_by_empty_tooltip.png"];
                                break;
                            case"COPY":
                                o = [this.$t("MY_CAMPAIGNS.new_campaign_copy_content"), "copy", this.fetchCopy];
                                break
                        }
                        var i = e.nth(e.__, o);
                        return n("div", {
                            class: "box-each ".concat(t),
                            on: {click: i(2)}
                        }, [i(3) && n("v-popover", [n("img", {
                            slot: "popover",
                            attrs: {src: i(3)}
                        }), n("b-icon", {
                            attrs: {pack: "fas", icon: "question-circle"}, nativeOn: {
                                click: function (e) {
                                    e.preventDefault(), e.stopPropagation()
                                }
                            }
                        })]), n("img", {attrs: {src: "/img/common/create_by_".concat(i(1), ".png")}}), n("span", {class: "box-each--title word-break"}, [i(0)])])
                    }, renderCampaigns: function () {
                        var e = this, t = this.$createElement, a = this.campaigns, n = void 0 === a ? [] : a;
                        if ("COPY" === this.mode && n.length) return t("div", {
                            class: "new-campaign-container--campaigns",
                            ref: "campaigns"
                        }, [n.map(function (a) {
                            var n = a.imageURL, i = void 0 === n ? "" : n, r = a.title, s = void 0 === r ? "" : r,
                                c = a.id, l = {attrs: {src: i, className: "campaign-box-cover-img"}};
                            return t("div", {
                                class: "campaign-each", on: {
                                    click: function () {
                                        return e.copy(c)
                                    }
                                }
                            }, [t("div", {class: "campaign-each--top"}, [t("bg-img", o()([{}, l, {style: {"background-color": "#dddddd"}}]))]), t("div", {class: "campaign-each--bottom adriel-ellipsis"}, [t("span", {class: "adriel-ellipsis word-break"}, [s])]), t("div", {class: "cover-area adriel-flex-center"}, [t("button", [e.$t("COMMON.copy")])])])
                        })])
                    }, copy: function (e) {
                        var t = this;
                        r["a"].copy(e).then(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = e.id;
                            t._active = !1, t.pushToRoute({name: "Proposal/Ads", params: {id: a}})
                        })
                    }, createEmptyCampaign: function (e) {
                        var t = this;
                        this._active = !1, this.setLoadingAdriel({
                            active: !0,
                            message: this.$t("COMMON.generate_empty_campaign")
                        }), Object(c["a"])(r["a"].createEmpty({webOrApp: e}), Object(l["a"])(2500)).pipe(Object(d["a"])(0)).subscribe(function () {
                            var a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, n = a.id;
                            t.setLoadingAdriel({
                                active: !1,
                                message: " "
                            }), t.$router.push({
                                name: "form" === e.type ? "Proposal/Form" : "Proposal/Ads",
                                params: {id: n}
                            })
                        })
                    }, fetchCopy: function () {
                        var t = this;

                        function a(e) {
                            e && _.scrollTo(e, 500, m)
                        }

                        e.isEmpty(this.campaigns) ? r["a"].query().then(function (n) {
                            t.campaigns = n, t.$nextTick(function () {
                                a(e.path(["$refs", "campaigns"], t))
                            })
                        }) : a(e.path(["$refs", "campaigns"], this)), this.mode = "COPY"
                    }, setGoalMode: function () {
                        this.mode = "GOAL"
                    }
                })
            }
        }).call(this, a("b17e"))
    }, fb72: function (e, t, a) {
        "use strict";
        var n = a("f309"), o = a.n(n);
        o.a
    }, fdd2: function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("div", {
                    ref: "elem",
                    staticClass: "img-video-wrapper",
                    class: {isLoading: e.isLoading, "validation-fail": e.notValid}
                }, [e.isLoading ? [a("clip-loader", {
                    attrs: {
                        color: "#999",
                        size: "20px"
                    }
                })] : [e.notValid ? [a("v-popover", {attrs: {placement: e.placement}}, [a("div", {
                    staticClass: "img-video-load-fail validations",
                    class: {error: e.error}
                }, [a("img", {
                    staticClass: "popover-icon",
                    attrs: {src: "/img/proposal/ads_validation_fail.png"}
                })]), a("div", {
                    staticClass: "img-video-validation-popover",
                    attrs: {slot: "popover"},
                    slot: "popover"
                }, e._l(e.validations, function (t, n) {
                    return a("span", {key: n, domProps: {textContent: e._s("- " + e.renderValidation(t))}})
                }), 0)])] : [e.previewNotAvailable ? [a("div", {
                    staticClass: "img-video-load-fail mp4",
                    class: {error: e.error}
                }, [a("div", [a("img", {attrs: {src: "/img/proposal/no_preview_available.png"}}), a("div", [e._v(e._s(e.$t("PROPOSAL.no_preview_mp4")))])])])] : [e.successUrl ? e._e() : a("div", {
                    staticClass: "img-video-load-fail",
                    class: {error: e.error}
                }, [e._t("fallback", [a("img", {attrs: {src: e.fallback}})])], 2), e.successUrl ? ["image" === e.type ? a("img", {
                    staticClass: "img-video-content",
                    attrs: {src: e.successUrl},
                    on: {load: e.handleLoad, error: e.handleError}
                }) : e._e(), "video" === e.type ? a("video", {
                    key: e.src,
                    staticClass: "img-video-content",
                    attrs: {controls: e.controls},
                    on: {loadstart: e.handleLoad, error: e.handleError}
                }, [a("source", {attrs: {src: e.successUrl}}), e._v("Your browser does not support HTML5 video.\n                    ")]) : e._e(), e._t("success")] : e._e()]]], e._t("last")], 2)
            }, o = [], i = a("4bdc"), r = i["a"], s = (a("1f11"), a("8495"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, "456157f4", null);
        t["a"] = c.exports
    }, fe51: function (e, t, a) {
        "use strict";
        var n = function () {
                var e = this, t = e.$createElement, a = e._self._c || t;
                return a("b-modal", {
                    attrs: {
                        active: !0,
                        canCancel: [],
                        width: "450px"
                    }
                }, [a("div", {staticClass: "title-update-modal-wrapper"}, [a("div", {staticClass: "title-update-modal-header"}, [e._v("\n            " + e._s(e.$t("COMMON.campaign_update_title")) + "\n        ")]), a("div", {staticClass: "title-update-modal-body"}, [a("div", [e._v(e._s(e.$t("COMMON.campaign_title")))]), a("input", {
                    directives: [{
                        name: "model",
                        rawName: "v-model",
                        value: e.editingCampaign.title,
                        expression: "editingCampaign.title"
                    }],
                    ref: "input",
                    attrs: {type: "text"},
                    domProps: {value: e.editingCampaign.title},
                    on: {
                        keypress: function (t) {
                            return !t.type.indexOf("key") && e._k(t.keyCode, "enter", 13, t.key, "Enter") ? null : e.confirm(t)
                        }, input: function (t) {
                            t.target.composing || e.$set(e.editingCampaign, "title", t.target.value)
                        }
                    }
                }), a("div", {staticClass: "modal-button-wrapper"}, [a("button", {
                    on: {
                        click: function (t) {
                            return e.$emit("complete")
                        }
                    }
                }, [e._v("\n                    " + e._s(e.$t("COMMON.cancel")) + "\n                ")]), a("button", {on: {click: e.confirm}}, [e._v("\n                    " + e._s(e.$t("COMMON.ok")) + "\n                ")])])])])])
            }, o = [], i = a("648b"), r = i["a"], s = (a("fb72"), a("2877")),
            c = Object(s["a"])(r, n, o, !1, null, null, null);
        t["a"] = c.exports
    }, ff29: function (e, t) {
        e.exports = {
            ko: {
                title: "鍮꾨�踰덊샇 �ъ꽕��",
                password: "鍮꾨�踰덊샇",
                next: "�ㅼ쓬",
                new_password_again: "�� 鍮꾨�踰덊샇 �뺤씤",
                new_password: "�� 鍮꾨�踰덊샇"
            },
            jp: {
                title: "�묆궧��꺖�됥굮�ゃ궩�껁깉�쇻굥",
                password: "�묆궧��꺖��",
                next: "轝▲겦",
                new_password_again: "�묆궧��꺖�됥굮�귙걝訝�佯�뀯�뚣겍�뤵걽�뺛걚",
                new_password: "�겹걮�꾠깙�밤꺈�쇈깋"
            },
            en: {
                title: "Reset your password",
                password: "Password",
                next: "Next",
                new_password_again: "Re Enter password",
                new_password: "New password"
            }
        }
    }
});
//# sourceMappingURL=app.535e6615.js.map