(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["proposal"], {
    "0178": function (t, e, n) {
    }, "01e5": function (t, e, n) {
        "use strict";
        var a = n("c01f"), i = n.n(a);
        i.a
    }, "028d": function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("75fc"), i = n("3de4"), s = n("fb8b"), o = n("8809"), r = n("fa7d"), c = n("4c6b"), l = n("54da"),
                u = n("d96a"), d = Object(r["P"])(c["e"]), p = t.compose(t.map(function (t) {
                    var e = t.originalname, n = t.path;
                    return {filename: e, url: n}
                }), t.flatten);
            e["a"] = {
                mixins: [i["a"]], created: function () {
                }, data: function () {
                    return {dropFiles: [], accept: c["e"].map(t.concat(".")).join(", ")}
                }, props: {}, computed: {
                    contactFilesTypesText: function () {
                        return c["e"].map(function (t) {
                            return "." + t
                        }).join(", ")
                    }
                }, components: {}, methods: {
                    edit: function () {
                        this.$emit("edit:start")
                    }, fileChanges: function (e) {
                        var n = this;
                        if (e && !t.isEmpty(e)) {
                            this.dropFiles = [];
                            var i = e.filter(t.compose(d, t.prop("name")));
                            if (i.length !== e.length && (this.toast("You may upload ".concat(this.contactFilesTypesText)), !i.length)) return this.$emit("edit:end");
                            l["a"].apply(void 0, Object(a["a"])(i.map(function (t) {
                                return s["a"].submitFile("upload", t)
                            }))).pipe(Object(u["a"])(function () {
                                return n.$emit("edit:end")
                            })).subscribe(function (t) {
                                return n.$emit("edit:add", p(t))
                            }, function (t) {
                                return console.log(t)
                            })
                        }
                    }, deleteFile: function (e) {
                        var n = this;
                        o["a"].deleteFile(t.path(["values", e, "url"], this)).finally(function () {
                            return n.$emit("update:remove", e)
                        })
                    }, makeUrl: r["T"]
                }
            }
        }).call(this, n("b17e"))
    }, "03ee": function (t, e, n) {
    }, "04f35": function (t, e, n) {
        "use strict";
        var a = n("cd39"), i = n.n(a);
        i.a
    }, "059a": function (t, e, n) {
    }, "05d2": function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("6510"), i = n("b271");
            n("f43e");
            e["a"] = {
                created: function () {
                },
                props: {active: {type: Boolean, default: !1}},
                data: function () {
                    return {}
                },
                components: {AdrielButtons: a["a"], ServicePreview: i["a"]},
                computed: {
                    _active: {
                        get: t.prop("active"), set: function (t) {
                            t !== this.active && this.$emit("update:active", t)
                        }
                    }, slides: function () {
                        var e = this;
                        return t.times(function (n) {
                            var a = n + 2;
                            return {
                                title: e.$t("JOIN.adriel_offer_title_".concat(a)),
                                contents: t.defaultTo([], e.$t("JOIN.adriel_offer_contents_".concat(a))),
                                mainImage: e.$t("PROPOSAL.service_preview_image_".concat(n))
                            }
                        }, 2)
                    }
                },
                methods: {
                    confirm: function () {
                        this._active = !1
                    }
                }
            }
        }).call(this, n("b17e"))
    }, "06d7": function (t, e, n) {
    }, "0864": function (t, e, n) {
        "use strict";
        var a = n("06d7"), i = n.n(a);
        i.a
    }, "0962": function (t, e, n) {
    }, 1111: function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("card-view", {
                staticClass: "optimization-toggle-card",
                class: t.className,
                attrs: {label: t.$t("PROPOSAL.plan_title_optimization"), editable: !1}
            }, [n("span", {
                staticClass: "optimization-toggle-slot-header",
                attrs: {slot: "slot-header"},
                slot: "slot-header"
            }, [n("switch-view", {
                ref: "switch",
                staticClass: "optimization-toggle-switch",
                attrs: {value: t.isAuto},
                on: {"update:value": t.valueChanged}
            })], 1), n("div", {staticClass: "optimization-toggle-card-body"}, [n("img", {attrs: {src: "/img/logos/logo_n_s.svg"}}), n("span", [t._v(t._s(t.$t("PROPOSAL.plan_optimization_info")))])])])
        }, i = [], s = n("19e8"), o = n("31a2"), r = {
            props: ["className", "isAuto"],
            components: {"card-view": s["a"], "switch-view": o["a"]},
            methods: {
                valueChanged: function (t) {
                    this.$emit("update:isAuto", t)
                }
            }
        }, c = r, l = (n("67f3"), n("2877")), u = Object(l["a"])(c, a, i, !1, null, null, null);
        e["a"] = u.exports
    }, 1415: function (t, e, n) {
        "use strict";
        var a = n("630a"), i = n.n(a);
        i.a
    }, 1458: function (t, e, n) {
    }, 1835: function (t, e, n) {
    }, "189a": function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "objectives"}, [n("card-view", {
                    attrs: {
                        label: "",
                        className: t.className,
                        editable: t.hasAnalytics,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        settingImgSrc: "/img/proposal/card_add_btn.png",
                        confirmLabel: "I'd like to change this setting",
                        settingLabel: t.$t("PROPOSAL.ap_card_edit_objectives")
                    }, on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [n("span", {
                    staticClass: "card-title",
                    attrs: {slot: "slot-header"},
                    slot: "slot-header"
                }, [t._v("\n            " + t._s(t.$t("PROPOSAL.ap_card_title_objectives")) + "\n            "), n("b-icon", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: t.$t("PROPOSAL.ap_card_title_objectives_tooltip"),
                        expression: "$t('PROPOSAL.ap_card_title_objectives_tooltip')"
                    }],
                    style: {"font-size": "12px", color: "$#666666", width: "auto", height: "auto"},
                    attrs: {pack: "fas", icon: "question-circle"}
                })], 1), n("b-tooltip", {
                    attrs: {
                        slot: "setting",
                        label: t.$t("PROPOSAL.ap_card_edit_objectives"),
                        type: "is-light",
                        position: "is-bottom"
                    }, slot: "setting"
                }, [n("b-icon", {
                    style: {"font-size": "14px", margin: "0 5px"},
                    attrs: {pack: "far", icon: "plus"},
                    nativeOn: {
                        click: function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                })], 1), t.hasAnalytics && !t._values.length ? n("div", {staticClass: "objectives-no-values"}, [n("no-content", {
                    on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                }, [n("p", {
                    staticClass: "word-break",
                    domProps: {innerHTML: t._s(t.$t("PROPOSAL.ap_card_no_objectives", {currency: t.getCurrencySymbol()}))}
                }), n("tags", {
                    staticClass: "objectives-tags",
                    attrs: {
                        values: [t.$t("PROPOSAL.ap_card_let_adriel")],
                        removable: !1,
                        "max-width": "300",
                        mainColor: "",
                        deleteImgSrc: "/img/proposal/profile_delete_icon_dark.png"
                    },
                    on: {
                        click: function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                })], 1)], 1) : t._e(), t.hasAnalytics && t._values.length > 0 ? n("div", {staticClass: "objectives-values"}, [n("span", {staticClass: "objectives-ment-guide word-break"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_objectives_info")))]), n("tags", {
                    staticClass: "objectives-tags",
                    attrs: {
                        values: t._values,
                        "value-getter": t.valueGetter,
                        "max-width": "265",
                        mainColor: "",
                        deleteImgSrc: "/img/proposal/profile_delete_icon_dark.png"
                    },
                    on: {
                        "update:remove": function (e) {
                            return t.$emit("update:remove", e)
                        }
                    }
                })], 1) : t._e(), t.hasAnalytics ? t._e() : n("div", [n("span", {staticClass: "objectives-ment-guide"}, [n("img", {attrs: {src: "/img/icons/conversion_icon.png"}}), n("p", {domProps: {innerHTML: t._s(t.$t("PROPOSAL." + t.getUnconnectedMessage, {currency: t.getCurrencySymbol()}))}})])])], 1)], 1)
            }, i = [], s = n("345a"), o = s["a"], r = (n("63ff"), n("410a"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "12893f68", null);
        e["a"] = c.exports
    }, "1c9b": function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "sector-wrapper"}, [n("message-modal", {
                attrs: {
                    active: t.isModalOpen,
                    text: "At least one sector needs to be here."
                }, on: {
                    "update:active": function (e) {
                        t.isModalOpen = e
                    }
                }
            }), n("card-view", {
                attrs: {
                    label: "Sector",
                    className: t.className,
                    editable: t.editable,
                    isEditing: t.isEditing,
                    dimmed: t.dimmed,
                    resetable: t.resetable,
                    resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                    settingImgSrc: "/img/proposal/card_add_btn.png",
                    confirmLabel: "I'd like to change this setting"
                }, on: {
                    "edit:start": function (e) {
                        return t.$emit("edit:start")
                    }, "edit:end": function (e) {
                        return t.$emit("edit:end")
                    }, "edit:reset": t.reset
                }
            }, [t._values.length ? n("div", {staticClass: "sector-values"}, [n("tags", {
                attrs: {
                    className: "website-visitors-tags",
                    values: t._values,
                    "value-getter": function (t) {
                        return t.text ? t.text : t
                    }
                }, on: {"update:remove": t.handleRemove}
            })], 1) : n("div", {staticClass: "sector-no-values"}, [n("no-content")], 1)])], 1)
        }, i = [], s = n("75fc"), o = (n("ac6a"), n("3de4")), r = n("2d2d"), c = {
            mixins: [o["a"]], created: function () {
            }, data: function () {
                return {isModalOpen: !1}
            }, props: {}, computed: {
                _values: function () {
                    var t = this.values, e = t.categories, n = void 0 === e ? [] : e, a = t.customCategories,
                        i = void 0 === a ? [] : a;
                    return [].concat(Object(s["a"])(n), Object(s["a"])(i))
                }
            }, components: {tags: r["a"]}, methods: {
                handleRemove: function (t) {
                    1 !== this._values.length ? this.$emit("update:remove", t) : this.isModalOpen = !0
                }
            }
        }, l = c, u = (n("95a6"), n("8b04"), n("2877")), d = Object(u["a"])(l, a, i, !1, null, "4eefc43e", null);
        e["a"] = d.exports
    }, "1e62": function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "BillingPlans"}, [n("div", {
                staticClass: "billing-plan",
                class: {selected: "M" === t.selected},
                on: {
                    click: function (e) {
                        t.selected = "M"
                    }
                }
            }, [n("h2", [t._v("Monthly")]), n("div", [n("span", {staticClass: "billing-plan-budget"}, [t._v("\n                $ " + t._s(t._f("addComma")(t.monthlyBudget)) + "\n            ")])]), n("small", [t._v("Based on 4 weeks")])]), n("div", {
                staticClass: "billing-plan",
                class: {selected: "W" === t.selected},
                on: {
                    click: function (e) {
                        t.selected = "W"
                    }
                }
            }, [n("h2", [t._v("Weekly")]), n("div", [n("span", {staticClass: "billing-plan-budget"}, [t._v("\n                $ " + t._s(t._f("addComma")(t.weeklyBudget)) + "\n            ")])]), n("small", [t._v("혻")])])])
        }, i = [], s = (n("c5f6"), {
            name: "BillingPlans",
            props: {billingPlan: {type: String}, monthlyBudget: {type: Number}, weeklyBudget: {type: Number}},
            computed: {
                selected: {
                    get: function () {
                        return this.billingPlan
                    }, set: function (t) {
                        this.$emit("update:billingPlan", t)
                    }
                }
            }
        }), o = s, r = (n("0864"), n("2877")), c = Object(r["a"])(o, a, i, !1, null, null, null);
        e["a"] = c.exports
    }, "1efe": function (t, e, n) {
        "use strict";
        var a = n("92e8"), i = n.n(a);
        i.a
    }, "259b": function (t, e, n) {
        "use strict";
        var a = n("4bd5"), i = n.n(a);
        i.a
    }, "27b6": function (t, e, n) {
        "use strict";
        var a = n("6987"), i = n.n(a);
        i.a
    }, "2f56": function (t, e, n) {
        "use strict";
        n.r(e);
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {
                    staticClass: "proposalWrapper",
                    class: [t.stepClass, {"publish-available": t.publishable}]
                }, [n("terms-and-policy", {
                    attrs: {active: t.isTermsActive, type: t.termsType},
                    on: {
                        "update:active": function (e) {
                            t.isTermsActive = e
                        }
                    }
                }), t.boModal ? n("bo-update-modal", {
                    attrs: {active: t.boModal}, on: {
                        "update:active": function (e) {
                            t.boModal = e
                        }, confirm: t.handleBoSurvey
                    }
                }) : t._e(), n("mobile-nav", {staticClass: "mobile-nav"}), n("left-nav", {staticClass: "left-nav"}), n("div", {staticClass: "contentWrapper"}, [n("top-nav", {
                    attrs: {
                        indicators: t.indicators,
                        stepIndex: t.stepIndex,
                        isLastStep: t.isLastStep,
                        canLaunch: t.canLaunch,
                        "campaign-title": t.title,
                        icon: t._campaignImage,
                        baseType: t.baseType
                    }, on: {"request:go": t.launchCampaign}
                }), n("router-view", [n("div", {
                    staticClass: "nav-above",
                    attrs: {slot: "navigation"},
                    slot: "navigation"
                }, [t.resetable ? n("button", {
                    staticClass: "reset-btn",
                    on: {click: t.reset}
                }, [t._v(t._s(t.$t("PROPOSAL.navigation_reset")))]) : t._e(), t.publishable ? n("button", {
                    staticClass: "reset-btn save",
                    on: {click: t.debounceExecute}
                }, [t._v(t._s(t.$t("PROPOSAL.navigation_publish")))]) : t._e(), t.isLastStep ? t._e() : n("router-link", {
                    staticClass: "next",
                    attrs: {to: t.nextPath, tag: "span"}
                }, [t._v("\n                    " + t._s(t.$t(t.isUpdate ? "PROPOSAL.next" : "PROPOSAL.next_step")) + "\n                    "), n("img", {attrs: {src: t.publishable ? "/img/proposal/next_step_icon_gray.png" : "/img/proposal/next_step_icon.png"}})]), t.isLastStep ? n("span", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: {content: t.$t(t.canLaunch.value ? "" : t.canLaunch.reason)},
                        expression: "{ content: $t(!canLaunch.value ? canLaunch.reason : '') }"
                    }], staticClass: "goBtn", class: {disabled: !t.canLaunch.value}, on: {
                        click: function (e) {
                            return t.launchCampaign()
                        }
                    }
                }, [t._v("\n                    " + t._s(t.$t(t.isUpdate ? "PROPOSAL.navigation_finish" : "PROPOSAL.launch_campaign")) + "\n                    "), t.canLaunch.value ? n("img", {attrs: {src: "/img/proposal/next_step_icon.png"}}) : n("img", {attrs: {src: "/img/proposal/go_btn_disabled.png"}})]) : t._e()], 1)]), n("div", {staticClass: "bottomNav"}, [0 !== t.stepIndex ? n("router-link", {
                    staticClass: "prevBtn",
                    attrs: {to: t.prevPath, tag: "span"}
                }, [n("img", {attrs: {src: "/img/proposal/back_step_icon.png"}})]) : t._e(), t.isLastStep ? t._e() : n("router-link", {
                    staticClass: "nextBtn",
                    attrs: {to: t.nextPath, tag: "span"}
                }, [t._v("\n                " + t._s(t.$t(t.isUpdate ? "PROPOSAL.next" : "PROPOSAL.next_step")) + "\n                "), n("img", {attrs: {src: "/img/proposal/next_step_icon.png"}})]), t.isLastStep ? n("span", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: {content: t.$t(t.canLaunch.value ? "" : t.canLaunch.reason)},
                        expression: "{ content: $t(!canLaunch.value ? canLaunch.reason : '') }"
                    }], staticClass: "goBtn", class: {disabled: !t.canLaunch.value}, on: {
                        click: function (e) {
                            return t.launchCampaign()
                        }
                    }
                }, [t._v("\n                " + t._s(t.isUpdate ? "Finish" : t.$t("PROPOSAL.launch_campaign")) + "\n                "), t.canLaunch.value ? n("img", {attrs: {src: "/img/proposal/next_step_icon.png"}}) : n("img", {attrs: {src: "/img/proposal/go_btn_disabled.png"}})]) : t._e()], 1), t.isLastStep ? n("span", {staticClass: "launch-warning"}, [t._v("\n            " + t._s(t.$t("PROPOSAL.terms_services_1")) + "\n            "), n("br"), t._v("\n            " + t._s(t.$t("PROPOSAL.terms_services_2")) + "\n            "), n("a", {
                    on: {
                        click: function (e) {
                            return t.showTerms("terms")
                        }
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.terms_services_terms")))]), t._v("\n            " + t._s(t.$t("PROPOSAL.terms_services_3")) + "\n            "), n("a", {
                    on: {
                        click: function (e) {
                            return t.showTerms("policy")
                        }
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.terms_services_policy")))]), t._v("\n            " + t._s(t.$t("PROPOSAL.terms_services_4")) + "\n        ")]) : t._e()], 1)], 1)
            }, i = [], s = n("c8fa"), o = s["a"], r = (n("e886"), n("3b87"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "04ea8cd8", null);
        e["default"] = c.exports
    }, "33eb": function (t, e, n) {
        "use strict";
        var a = n("77f0"), i = n.n(a);
        i.a
    }, "345a": function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("cebc"), i = (n("7f7f"), n("3de4")), s = n("2d2d"), o = n("2f62"), r = {
                showFacebookPixel: "ap_card_no_objectives_unconnected_FA",
                showFacebookSdk: "ap_card_no_objectives_unconnected_FA",
                showGoogleAnalytics: "ap_card_no_objectives_unconnected_GA",
                all: "ap_card_no_objectives_unconnected"
            };
            e["a"] = {
                mixins: [i["a"]],
                props: {channelVisibility: {type: Object, default: t.always({})}},
                methods: {
                    valueGetter: function () {
                        var t, e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        return t = "LINK" === e.type ? e.value : e.name, "ga" === e.analyticType ? t += " (Google)" : "pixel" === e.analyticType && (t += " (Facebook)"), t
                    }
                },
                computed: Object(a["a"])({}, Object(o["mapGetters"])("user", ["isAdmin"]), Object(o["mapGetters"])("proposal", ["fbPixel", "ga"]), {
                    hasAnalytics: function () {
                        return Boolean(this.fbPixel || this.ga)
                    }, _values: t.compose(t.defaultTo([]), t.prop("values")), getUnconnectedMessage: function () {
                        return this.channelVisibility.showFacebookPixel ? r["all"] : r["showGoogleAnalytics"]
                    }
                }),
                components: {tags: s["a"]}
            }
        }).call(this, n("b17e"))
    }, "3a8d": function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("cebc"), i = n("3de4"), s = n("8db5"), o = n("2f62");
            e["a"] = {
                mixins: [i["a"]],
                created: function () {
                },
                data: function () {
                    return {
                        newTooltip: this.$t("PROPOSAL.new_returning_new_tooltip"),
                        returningTooltip: this.$t("PROPOSAL.new_returning_returning_tooltip")
                    }
                },
                computed: Object(a["a"])({}, Object(o["mapGetters"])("proposal", ["websiteVisitors", "listOfCustomers"]), Object(o["mapGetters"])("campaign", ["intel"]), {pastList: t.path(["listOfCustomers", "values", "length"])}),
                methods: {
                    tryEdit: function () {
                        var e = this.intel, n = e.facebookPixel, a = e.googleAnalytics;
                        if (n = n || {}, a = a || {}, t.isEmpty(n) && t.isEmpty(a) && !this.pastList) return this.toast(this.$t("PROPOSAL.unable_to_retarget_toast"));
                        this.$emit("edit:start")
                    }
                },
                components: {"gender-gauge-view": s["a"]}
            }
        }).call(this, n("b17e"))
    }, "3b87": function (t, e, n) {
        "use strict";
        var a = n("536e"), i = n.n(a);
        i.a
    }, "3d75": function (t, e, n) {
    }, "3f96": function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("cebc"), i = n("2f62"), s = n("dde5"), o = n("baba"), r = n("12e7"), c = n("6510"), l = n("53cc"),
                u = n("fa7d"), d = t.max(0);
            e["a"] = {
                created: function () {
                    this.compFacebookProfile = t.clone(this.facebookProfile) || {}, this.getFacebookProfile(this.campaignId)
                },
                watch: {
                    pages: {
                        handler: function () {
                            var t = this;
                            this.$nextTick(function () {
                                t.top = "".concat(d(window.innerHeight / 2 - Object(u["A"])(t.$refs.wrapper) / 2), "px")
                            })
                        }, immediate: !0, deep: !0
                    }
                },
                props: {active: {type: Boolean, default: !1}},
                data: function () {
                    return {
                        values: [],
                        top: void 0,
                        left: "auto",
                        checked: [],
                        compFacebookProfile: {},
                        isWarning: !1,
                        addInstaPage: !1
                    }
                },
                computed: Object(a["a"])({}, Object(i["mapGetters"])("proposal", ["facebookProfile"]), Object(i["mapGetters"])("user", ["fbInfo"]), Object(i["mapGetters"])("campaign", ["campaignId", "intel"]), Object(i["mapState"])("user", ["permissionsMissing"]), {
                    wrapperStyle: function () {
                        return {top: this.top, left: this.left}
                    },
                    _active: {
                        get: function () {
                            return this.active
                        }, set: function (t) {
                            this.$emit("update:active", t)
                        }
                    },
                    _compFacebookProfile: {
                        get: function () {
                            return this.compFacebookProfile
                        }, set: function (t) {
                            this.compFacebookProfile = t
                        }
                    },
                    pages: function () {
                        return t.pathOr([], ["fbInfo", "pages"], this)
                    },
                    selectablePages: t.pipe(t.prop("pages"), t.defaultTo([]), t.filter(t.prop("selectable"))),
                    unselectablePages: function () {
                        return this.pages.filter(function (t) {
                            return !t.selectable
                        })
                    },
                    status: function () {
                        return t.cond([[t.pathSatisfies(t.equals(0), ["pages", "length"]), t.always("noPages")], [t.isEmpty, t.always("notAuth")], [t.T, t.always("pages")]])(this.fbInfo)
                    },
                    instaPage: function () {
                        return this._compFacebookProfile ? this._compFacebookProfile.instagram : null
                    },
                    buttonTypes: function () {
                        switch (this.status) {
                            case"notAuth":
                            case"noPages":
                                return ["cancel"];
                            default:
                                return ["cancel", "confirm"]
                        }
                    }
                }),
                components: {
                    "journey-editable-box": o["a"],
                    "journey-edit-input": r["a"],
                    buttons: c["a"],
                    FbLoginButton: l["a"]
                },
                methods: Object(a["a"])({}, Object(i["mapActions"])("proposal", ["getFacebookProfile"]), Object(i["mapActions"])("user", ["fetchFacebookProfile"]), {
                    confirm: function () {
                        var e = this,
                            n = (t.path(["facebookProfile", "id"], this), t.path(["compFacebookProfile", "id"], this));
                        s["d"].updateFacebook(this.campaignId, n, this.fbInfo.longLifeToken, this.addInstaPage ? this.instaPage : null).then(function () {
                            e.$emit("confirm"), e._active = !1
                        }).catch(function (t) {
                            console.log(t), alert("error occured")
                        })
                    }, cancel: function () {
                        this.$emit("cancel"), this._active = !1
                    }, radioSelected: function (e) {
                        this.compFacebookProfile = t.clone(e)
                    }
                }),
                beforeDestroy: function () {
                },
                mounted: function () {
                    t.path(["intel", "instagram"], this) && (this.addInstaPage = !0)
                }
            }
        }).call(this, n("b17e"))
    }, "410a": function (t, e, n) {
        "use strict";
        var a = n("be91"), i = n.n(a);
        i.a
    }, "48f2": function (t, e, n) {
        "use strict";
        var a = n("8bcc"), i = n.n(a);
        i.a
    }, 4928: function (t, e, n) {
        "use strict";
        var a = n("3d75"), i = n.n(a);
        i.a
    }, "4bd5": function (t, e, n) {
    }, "50e7": function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("cebc"), i = n("0a63"), s = n("2f62"), o = n("fa7d");
            e["a"] = {
                created: function () {
                },
                props: {
                    slides: {type: Array, default: t.always([])},
                    paginationActiveColor: {type: String, default: "#18649b"}
                },
                components: {Carousel: i["Carousel"], Slide: i["Slide"]},
                computed: Object(a["a"])({}, Object(s["mapGetters"])("user", ["user"])),
                methods: {
                    isLang: function (t) {
                        return Object(o["w"])(this.user) === t
                    }
                }
            }
        }).call(this, n("b17e"))
    }, "536e": function (t, e, n) {
    }, "53b3": function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("5176"), i = n.n(a), s = n("cebc"), o = n("2f62"), r = n("ac1d"), c = n("189a"), l = n("9e4e"),
                u = n("68f9"), d = n("ad27"), p = n("8fda"), f = n("a1f6"), b = n("c2e3"), m = n("34b0"), h = n("3d93"),
                v = n("938b"), g = n("7d47"), _ = n("0a7c"), O = n("b944"), y = n("1c9b"),
                C = t.converge(t.complement(t.equals)),
                P = t.contains(t.__, ["websiteVisitors", "newAndReturningCustomers", "objectives", "listOfCustomers", "location", "audienceInterests", "searchKeywords", "sector"]),
                A = {
                    newAndReturningCustomers: ["newAndReturningCustomers", "newAndReturningCustomersBackup"],
                    websiteVisitors: ["websiteVisitors", "websiteVisitorsBackup"],
                    objectives: ["objectives", "objectivesBackup"],
                    listOfCustomers: ["listOfCustomers", "listOfCustomersBackup"],
                    gender: ["gender", "genderBackup"],
                    location: ["location", "locationBackup"],
                    audienceInterests: ["audienceInterests", "audienceInterestsBackup"],
                    age: ["age", "ageBackup"],
                    searchKeywords: ["searchKeywords", "searchKeywordsBackup"],
                    income: ["income", "incomeBackup"],
                    relationshipStatus: ["relationshipStatus", "relationshipStatusBackup"],
                    educationLevel: ["educationLevel", "educationLevelBackup"],
                    deviceTarget: ["deviceTarget", "deviceTargetBackup"],
                    sizeAndDecision: ["sizeAndDecision", "sizeAndDecisionBackup"],
                    sector: ["sector", "sectorBackup"]
                };

            function x(t) {
                return t.charAt(0).toUpperCase() + t.slice(1)
            }

            e["a"] = {
                name: "CardCompWrapper",
                props: {
                    type: {type: String, required: !0},
                    isEditing: {type: Boolean, default: !1},
                    dimmed: {type: Boolean, default: !1},
                    isFormType: {type: Boolean, default: !1}
                },
                computed: Object(s["a"])({}, Object(o["mapGetters"])("proposal", ["gender", "genderBackup", "newAndReturningCustomers", "newAndReturningCustomersBackup", "websiteVisitors", "websiteVisitorsBackup", "objectives", "objectivesBackup", "listOfCustomers", "listOfCustomersBackup", "location", "locationBackup", "audienceInterests", "audienceInterestsBackup", "age", "ageBackup", "searchKeywords", "searchKeywordsBackup", "income", "incomeBackup", "relationshipStatus", "relationshipStatusBackup", "educationLevel", "educationLevelBackup", "deviceTarget", "deviceTargetBackup", "sizeAndDecision", "sizeAndDecisionBackup", "sector", "sectorBackup"]), Object(o["mapGetters"])("campaign", ["googleAnalytics", "googleAnalyticsBackup"])),
                methods: Object(s["a"])({}, Object(o["mapMutations"])("proposal", ["updateGender", "resetGender", "updateNewAndReturningCustomers", "resetNewAndReturningCustomers", "addListOfCustomers", "removeListOfCustomers", "resetListOfCustomers", "removeLocation", "resetLocation", "removeAudienceInterests", "resetAudienceInterests", "updateAge", "resetAge", "removeSearchKeywords", "resetSearchKeywords", "updateIncome", "resetIncome", "updateRelationshipStatus", "resetRelationshipStatus", "updateEducationLevel", "resetEducationLevel", "updateDeviceTarget", "resetDeviceTarget", "updateSizeAndDecision", "resetSizeAndDecision", "removeSector", "resetSector", "removeWebsiteVisitors", "resetWebsiteVisitors", "removeObjectives", "resetObjectives"]), Object(o["mapActions"])("campaign", ["updateGa", "resetGa", "resetFbApp", "resetFacebookPixel", "update", "updateFacebookPixel", "updateFbApp"]), {
                    getComp: function () {
                        switch (this.type) {
                            case"newAndReturningCustomers":
                                return r["a"];
                            case"websiteVisitors":
                                return l["a"];
                            case"objectives":
                                return c["a"];
                            case"gender":
                                return d["a"];
                            case"listOfCustomers":
                                return u["a"];
                            case"location":
                                return p["a"];
                            case"audienceInterests":
                                return f["a"];
                            case"age":
                                return b["a"];
                            case"searchKeywords":
                                return m["a"];
                            case"income":
                                return h["a"];
                            case"relationshipStatus":
                                return v["a"];
                            case"educationLevel":
                                return g["a"];
                            case"deviceTarget":
                                return _["a"];
                            case"sizeAndDecision":
                                return O["a"];
                            case"sector":
                                return y["a"]
                        }
                    }, isResetable: function () {
                        var e = A[this.type];
                        if (e && !t.isEmpty(e)) return C(e.map(function (e) {
                            return t.compose(t.pick(["name", "values"]), t.prop(e))
                        }))(this)
                    }, getProps: function () {
                        var e = {resetable: this.isResetable(), isFormType: this.isFormType};
                        return P(this.type) && (e.editable = !0), "websiteVisitors" !== this.type && "objectives" !== this.type || (e.channelVisibility = t.pick(["showFacebookPixel", "showFacebookSdk", "showGoogleAnalytics"], this.websiteVisitors)), t.compose(t.merge(i()(e, this.$props)), function (t) {
                            return {values: t}
                        }, t.path([this.type, "values"]))(this)
                    }, update: function (t) {
                        var e = this["update".concat(x(this.type))];
                        e && e(t)
                    }, remove: function (e) {
                        var n = this, a = {
                            websiteVisitors: function () {
                                "google" === e ? (n.updateGa(), n.removeWebsiteVisitors("google")) : "fbApp" === e ? (n.updateFbApp(), n.removeWebsiteVisitors("fbApp")) : "facebook" === e && (n.updateFacebookPixel(), n.removeWebsiteVisitors("facebook"))
                            }
                        }, i = a[this.type] || this["remove".concat(x(this.type))];
                        "Function" === t.type(i) && i(e)
                    }, reset: function () {
                        var e = this, n = {
                            websiteVisitors: function () {
                                e.resetGa(), e.resetFacebookPixel(), e.resetFbApp(), e.resetWebsiteVisitors()
                            }
                        }, a = n[this.type] || this["reset".concat(x(this.type))];
                        "Function" === t.type(a) && a()
                    }, add: function (t) {
                        switch (this.type) {
                            case"listOfCustomers":
                                return this.addListOfCustomers(t)
                        }
                    }
                }),
                render: function (t) {
                    var e = this;
                    return t(this.getComp(), {
                        props: this.getProps(), on: {
                            "edit:start": function (t) {
                                return e.$emit("edit:start", t)
                            },
                            "edit:end": function () {
                                return e.$emit("edit:end")
                            },
                            "edit:reset": this.reset,
                            "edit:add": this.add,
                            "update:value": this.update,
                            "update:remove": this.remove
                        }
                    })
                }
            }
        }).call(this, n("b17e"))
    }, "53cc": function (t, e, n) {
        "use strict";
        var a = n("aede"), i = n("9c56");

        function s() {
            var t = Object(a["a"])(["\n    ", "\n    font-size: 13px;\n    font-weight: 600;\n    color: white;\n"]);
            return s = function () {
                return t
            }, t
        }

        function o() {
            var t = Object(a["a"])(["\n    height: 14px;\n"]);
            return o = function () {
                return t
            }, t
        }

        function r() {
            var t = Object(a["a"])(["\n    ", "\n    border-radius: 3px;\n    background-color: #3b5998;\n    padding: 7px 30px;\n    min-height: 30px;\n"]);
            return r = function () {
                return t
            }, t
        }

        function c() {
            var t = Object(a["a"])(["\n    display: flex;\n    align-items: center;\n    justify-content: center;\n"]);
            return c = function () {
                return t
            }, t
        }

        function l() {
            var t = Object(a["a"])(["\n    font-family: 'Rubik', 'sans-serif', 'Noto Sans';\n"]);
            return l = function () {
                return t
            }, t
        }

        var u = Object(i["a"])(l()), d = Object(i["a"])(c()), p = i["b"].button(r(), d), f = i["b"].img(o()),
            b = i["b"].span(s(), u);
        e["a"] = {
            render: function () {
                var t = arguments[0];
                return t(p, [t(f, {attrs: {src: "/img/icons/facebook_logo.png"}}), t(b, [this.$t("COMMON.log_in_to_fb")])])
            }
        }
    }, "58c8": function (t, e, n) {
        "use strict";
        var a = n("1458"), i = n.n(a);
        i.a
    }, 5913: function (t, e, n) {
        "use strict";
        (function (t) {
            n("6762"), n("2fdb"), n("7f7f");
            var a = n("cebc"), i = n("e25b"), s = n("dde5"), o = n("2f62"), r = n("5d07"), c = n("53b3"), l = n("08fe"),
                u = n("f754"), d = n("7957"), p = n("8f6b"), f = n("f4d5"), b = n("172d"), m = n("4c6b"),
                h = ["audienceInterests"],
                v = t.contains(t.__, ["websiteVisitors", "searchKeywords", "location", "sector", "audienceInterests", "objectives"]),
                g = t.contains(t.__, ["websiteVisitors", "searchKeywords", "sector"]);
            e["a"] = {
                created: function () {
                    this.$nextTick(function () {
                        window.scrollTo(0, 0)
                    })
                },
                data: function () {
                    return {
                        currentEditCmp: null,
                        isModalOpen: !1,
                        isTrackerModalOpen: !1,
                        isConversionsModalOpen: !1,
                        isPixelModalOpen: !1,
                        isFbAppModalOpen: !1,
                        headers: {
                            websiteVisitors: "Select Google Analytics account",
                            searchKeywords: this.$t("PROPOSAL.add_keywords_modal_title"),
                            location: this.$t("PROPOSAL.add_location_modal_title"),
                            sector: "Add more sectors",
                            audienceInterests: "Add more interests"
                        },
                        placeholders: {
                            websiteVisitors: "Add more Website",
                            searchKeywords: this.$t("PROPOSAL.add_keywords_modal_placeholder"),
                            location: this.$t("PROPOSAL.add_location_modal_placeholder"),
                            sector: "Add more sectors",
                            audienceInterests: "Add more interests"
                        },
                        currentEditIdx: void 0
                    }
                },
                computed: Object(a["a"])({}, Object(o["mapGetters"])("proposal", ["loaded", "audienceProfile", "location", "audienceInterests", "searchKeywords", "websiteVisitors", "objectives", "summary", "audienceTags", "baseType"]), Object(o["mapGetters"])("campaign", ["campaignId", "googleAnalytics", "isUpdate", "intel", "proposal"]), Object(o["mapGetters"])("user", ["isAdmin"]), {
                    types: t.compose(t.filter(t.prop("visible")), t.prop("audienceProfile")),
                    isFormType: t.propEq("baseType", "FORM"),
                    addedValues: function () {
                        return t.path([this.currentEditCmp, "values"], this)
                    }
                }),
                components: {
                    "a-comp": c["a"],
                    ConversionsModal: d["a"],
                    "inputs-modal": l["a"],
                    TrackerModal: u["a"],
                    PixelModal: p["a"],
                    SummaryTags: f["a"],
                    FbAppModal: b["a"]
                },
                watch: {
                    proposal: {
                        handler: function (t) {
                            t && r["audience"].chooseAudienceVisibility({
                                proposal: this.proposal,
                                intel: this.intel,
                                type: this.baseType.toLowerCase()
                            })
                        }, deep: !0, immediate: !0
                    }
                },
                methods: Object(a["a"])({}, Object(o["mapMutations"])("proposal", ["addWebsiteVisitors", "addSearchKeywords", "addLocations", "setLocations", "addSectors", "addAudienceInterests", "setWebsiteVisitors", "setObjectives", "setAudienceInterests", "setSearchKeywords"]), Object(o["mapActions"])("campaign", ["updateGa", "updateFacebookPixel"]), {
                    isEditable: function (t) {
                        return g(t)
                    }, getAddType: function (t) {
                        return g(t) ? "general" : "search"
                    }, setCurrentEditCmp: function (t, e) {
                        t || (this.currentEditIdx = void 0), this.currentEditCmp = t, "websiteVisitors" === t ? ("pixel" === e ? this.isPixelModalOpen = !0 : "fbApp" === e ? this.isFbAppModalOpen = !0 : this.isTrackerModalOpen = !0, this.currentEditCmp += ":".concat(e)) : "objectives" === t ? this.isConversionsModalOpen = !0 : ("searchKeywords" === t && (this.currentEditIdx = e), this.isModalOpen = v(t))
                    }, getProps: function (t) {
                        return {
                            type: t,
                            dimmed: this.currentEditCmp && this.currentEditCmp !== t,
                            isEditing: this.currentEditCmp === t
                        }
                    }, handleConfirmModal: function (t) {
                        var e = this.currentEditCmp;
                        switch (this.isModalOpen = !1, this.isTrackerModalOpen = !1, this.setCurrentEditCmp(null), e) {
                            case"websiteVisitors:google":
                                return this.setWebsiteVisitors({
                                    type: "googleAnalytics",
                                    value: t
                                }), void this.updateGa(t);
                            case"websiteVisitors:pixel":
                                return void this.setWebsiteVisitors({type: "facebookPixel", value: t});
                            case"websiteVisitors:fbApp":
                                return void this.setWebsiteVisitors({type: "fbApp", value: t});
                            case"objectives":
                                return void this.setObjectives(t);
                            case"searchKeywords":
                                return this.setSearchKeywords(t);
                            case"location":
                                return this.setLocations(t);
                            case"sector":
                                return this.addSectors(t);
                            case"audienceInterests":
                                return this.setAudienceInterests(t)
                        }
                    }, getSearchFunc: function (t) {
                        switch (t) {
                            case"location":
                                return s["i"].autocomplete;
                            case"audienceInterests":
                                return i["a"].getInterests
                        }
                    }, getNameGetter: function (e) {
                        var n = this;
                        return "location" === e ? t.prop("text") : "sector" === e ? t.ifElse(t.has("text"), t.prop("text"), t.identity) : "audienceInterests" === e ? function (t) {
                            var e = t.name;
                            return ["interests", "behaviors", "family_statuses"].includes(t.type) && (e = n.$t("PROPOSAL.".concat(t.type)) + ": " + e), n.isAdmin && t.channel && (e += " (" + t.channel + ")"), e
                        } : void 0
                    }, getValueGetter: function (e) {
                        return "location" === e ? t.prop("place_id") : "sector" === e ? t.ifElse(t.has("value"), t.prop("value"), t.identity) : "audienceInterests" === e ? t.prop("id") : void 0
                    }, getInputValidator: function (e) {
                        if ("searchKeywords" === e) return t.test(m["p"])
                    }, getInputWarnMessage: function (t) {
                        if ("searchKeywords" === t) return "Invalid keyword"
                    }, getIsKeepSearchType: t.contains(t.__, h)
                })
            }
        }).call(this, n("b17e"))
    }, "59b7": function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return c
        });
        var a = n("9ab4"), i = n("1453"), s = n("e9b9"), o = n("3060"), r = n("ce8b");

        function c(t, e) {
            return e ? function (n) {
                return new d(n, e).lift(new l(t))
            } : function (e) {
                return e.lift(new l(t))
            }
        }

        var l = function () {
            function t(t) {
                this.delayDurationSelector = t
            }

            return t.prototype.call = function (t, e) {
                return e.subscribe(new u(t, this.delayDurationSelector))
            }, t
        }(), u = function (t) {
            function e(e, n) {
                var a = t.call(this, e) || this;
                return a.delayDurationSelector = n, a.completed = !1, a.delayNotifierSubscriptions = [], a.index = 0, a
            }

            return a["a"](e, t), e.prototype.notifyNext = function (t, e, n, a, i) {
                this.destination.next(t), this.removeSubscription(i), this.tryComplete()
            }, e.prototype.notifyError = function (t, e) {
                this._error(t)
            }, e.prototype.notifyComplete = function (t) {
                var e = this.removeSubscription(t);
                e && this.destination.next(e), this.tryComplete()
            }, e.prototype._next = function (t) {
                var e = this.index++;
                try {
                    var n = this.delayDurationSelector(t, e);
                    n && this.tryDelay(n, t)
                } catch (a) {
                    this.destination.error(a)
                }
            }, e.prototype._complete = function () {
                this.completed = !0, this.tryComplete(), this.unsubscribe()
            }, e.prototype.removeSubscription = function (t) {
                t.unsubscribe();
                var e = this.delayNotifierSubscriptions.indexOf(t);
                return -1 !== e && this.delayNotifierSubscriptions.splice(e, 1), t.outerValue
            }, e.prototype.tryDelay = function (t, e) {
                var n = Object(r["a"])(this, t, e);
                if (n && !n.closed) {
                    var a = this.destination;
                    a.add(n), this.delayNotifierSubscriptions.push(n)
                }
            }, e.prototype.tryComplete = function () {
                this.completed && 0 === this.delayNotifierSubscriptions.length && this.destination.complete()
            }, e
        }(o["a"]), d = function (t) {
            function e(e, n) {
                var a = t.call(this) || this;
                return a.source = e, a.subscriptionDelay = n, a
            }

            return a["a"](e, t), e.prototype._subscribe = function (t) {
                this.subscriptionDelay.subscribe(new p(t, this.source))
            }, e
        }(s["a"]), p = function (t) {
            function e(e, n) {
                var a = t.call(this) || this;
                return a.parent = e, a.source = n, a.sourceSubscribed = !1, a
            }

            return a["a"](e, t), e.prototype._next = function (t) {
                this.subscribeToSource()
            }, e.prototype._error = function (t) {
                this.unsubscribe(), this.parent.error(t)
            }, e.prototype._complete = function () {
                this.unsubscribe(), this.subscribeToSource()
            }, e.prototype.subscribeToSource = function () {
                this.sourceSubscribed || (this.sourceSubscribed = !0, this.unsubscribe(), this.source.subscribe(this.parent))
            }, e
        }(i["a"])
    }, "59e7": function (t, e, n) {
        "use strict";
        (function (t) {
            e["a"] = {
                props: ["data"],
                computed: {tags: t.compose(t.filter(t.propSatisfies(t.identity, "value")), t.defaultTo([]), t.prop("data"))}
            }
        }).call(this, n("b17e"))
    }, "59ed": function (t, e, n) {
        "use strict";
        (function (t) {
            n("7514");
            var a = n("cebc"), i = n("3de4"), s = n("fa7d"), o = n("2f62"), r = n("d3fb"), c = n("d96a"), l = n("d337");
            e["a"] = {
                mixins: [i["a"]],
                props: {
                    channelVisibility: {
                        type: Object, default: function () {
                            return {}
                        }
                    }
                },
                computed: Object(a["a"])({}, Object(o["mapGetters"])("proposal", ["isAppType"]), {
                    google: t.compose(t.find(t.propEq("type", "googleAnalytics")), t.prop("values")),
                    pixel: t.compose(t.find(t.propEq("type", "facebookPixel")), t.prop("values")),
                    fbApp: t.compose(t.find(t.propEq("type", "fbApp")), t.prop("values")),
                    noTools: t.compose(t.not, s["N"], t.filter(s["N"]), t.props(["google", "pixel"])),
                    type: t.ifElse(t.propEq("isAppType", !0), t.always("APP"), t.always("WEB")),
                    cardTitle: function () {
                        return "WEB" === this.type ? this.$t("PROPOSAL.ap_card_title_tracker") : this.$t("COMMON.fb_app_modal_header")
                    }
                }),
                methods: Object(a["a"])({}, Object(o["mapMutations"])("user", ["setGaInfo", "setPixelInfo"]), Object(o["mapActions"])("user", ["fetchFacebookProfile"]), {
                    handleLogin: function () {
                        var t = this;
                        this.isDemo ? this.setDemoAccountModalActive(!0) : Object(s["v"])().then(function (e) {
                            t.setGaInfo(e), t.$emit("edit:start", "google")
                        }).catch(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            console.log(e), 403 === e.status && (t.setGaInfo(), t.$emit("edit:start"))
                        })
                    }, handlePixelLogin: function () {
                        var t = this;
                        this.isDemo ? this.setDemoAccountModalActive(!0) : Object(r["a"])(Object(s["u"])()).pipe(Object(c["a"])(function () {
                            return t.$emit("edit:start", "pixel")
                        }), Object(l["a"])(2e3)).subscribe(this.fetchFacebookProfile, function () {
                            console.log("rejected")
                        })
                    }, getTooltip: function (t) {
                        if (!this.noTools) {
                            var e, n = {
                                show: !0,
                                autoHide: !1,
                                hideOnTargetClick: !1,
                                trigger: "manual",
                                placement: "top",
                                classes: "tooltip adriel-tooltip tracker-tooltip",
                                offset: 10
                            };
                            return e = "google" === t ? this.$t("PROPOSAL.ap_card_tracker_google_content") : this.$t("PROPOSAL.ap_card_tracker_pixel_content"), Object(a["a"])({}, n, {content: e})
                        }
                    }, handleFbAppLogin: function () {
                        var t = this;
                        this.isDemo ? this.setDemoAccountModalActive(!0) : Object(r["a"])(Object(s["u"])()).pipe(Object(c["a"])(function () {
                            return t.$emit("edit:start", "fbApp")
                        }), Object(l["a"])(2e3)).subscribe(this.fetchFacebookProfile, function () {
                            return console.log("rejected")
                        })
                    }
                })
            }
        }).call(this, n("b17e"))
    }, "5a17": function (t, e, n) {
        "use strict";
        var a = n("059a"), i = n.n(a);
        i.a
    }, "5ae3": function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "topHeader"}, [t.isTitleUpdate ? n("update-title", {
                    attrs: {campaign: t.campaign},
                    on: {
                        complete: function (e) {
                            t.isTitleUpdate = null
                        }
                    }
                }) : t._e(), n("div", {staticClass: "topHeader__top_left"}, [n("span", {style: t.imageStyle}), n("div", {staticClass: "topHeader__top_left_title"}, [n("span", [t._v("\n                " + t._s(t.campaignTitle) + "\n                "), n("img", {
                    attrs: {src: "/img/edit_icon.png"},
                    on: {
                        click: function (e) {
                            t.isTitleUpdate = !0
                        }
                    }
                })]), n("span", {staticClass: "big-title"}, [t._v(t._s(t.title))])])]), n("span", {staticClass: "indicator-wrapper"}, [t._l(t.indicators, function (e, a) {
                    return n("span", {
                        key: e.path,
                        staticClass: "indicator",
                        class: [t.baseType, "step-index-" + a, {
                            "step-done": a <= t.stepIndex,
                            "step-current": a === t.stepIndex
                        }]
                    }, [n("router-link", {
                        staticClass: "indicator-number",
                        attrs: {to: e.path, tag: "span"}
                    }, [t._v(t._s(a + 1))]), n("span", {
                        staticClass: "indicator-title",
                        domProps: {textContent: t._s(t.$t(e.navLabel))}
                    })], 1)
                }), n("span", {
                    staticClass: "indicator indicator-for-last",
                    class: {deActive: !t.isLastStep || !t.canLaunch.value}
                }, [n("span", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: {content: t.$t(t.canLaunch.value ? "" : t.canLaunch.reason)},
                        expression: "{\n                    content: $t(!canLaunch.value ? canLaunch.reason : ''),\n                }"
                    }], staticClass: "indicator-number", on: {click: t.requestGo}
                }, [t._v("Go!")])])], 2)], 1)
            }, i = [], s = n("a137"), o = s["a"], r = (n("27b6"), n("4928"), n("5b0d"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "a2ec00a0", null);
        e["a"] = c.exports
    }, "5b0d": function (t, e, n) {
        "use strict";
        var a = n("03ee"), i = n.n(a);
        i.a
    }, "5de0": function (t, e, n) {
    }, "630a": function (t, e, n) {
    }, "63ff": function (t, e, n) {
        "use strict";
        var a = n("9c1a"), i = n.n(a);
        i.a
    }, 6491: function (t, e, n) {
    }, "67f3": function (t, e, n) {
        "use strict";
        var a = n("fe0b"), i = n.n(a);
        i.a
    }, "68f9": function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "website-visitors"}, [n("card-view", {
                    attrs: {
                        label: "",
                        className: t.className,
                        editable: t.editable,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        confirmLabel: "I'd like to change this setting"
                    }, on: {
                        "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [n("span", {
                    staticClass: "card-title",
                    attrs: {slot: "slot-header"},
                    slot: "slot-header"
                }, [t._v("\n            " + t._s(t.$t("PROPOSAL.ap_card_title_customers")) + "\n            "), n("b-icon", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: t.$t("PROPOSAL.ap_card_title_customers_tooltip"),
                        expression: "$t('PROPOSAL.ap_card_title_customers_tooltip')"
                    }],
                    style: {"font-size": "12px", color: "$#666666", width: "auto", height: "auto"},
                    attrs: {pack: "fas", icon: "question-circle"}
                })], 1), n("b-tooltip", {
                    attrs: {
                        slot: "setting",
                        label: t.$t("PROPOSAL.ap_card_edit_customer_list"),
                        type: "is-light",
                        position: "is-bottom"
                    }, slot: "setting"
                }, [n("b-upload", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.editable && !t.isEditing,
                        expression: "editable && !isEditing"
                    }],
                    attrs: {multiple: "", "drag-drop": "", accept: t.accept},
                    on: {input: t.fileChanges},
                    model: {
                        value: t.dropFiles, callback: function (e) {
                            t.dropFiles = e
                        }, expression: "dropFiles"
                    }
                }, [n("b-icon", {
                    style: {"font-size": "14px"},
                    attrs: {pack: "far", icon: "plus"}
                })], 1)], 1), t.values.length ? n("div", {staticClass: "customers-values"}, [n("span", {staticClass: "customers-info"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_customer_list_desc")))]), n("div", {staticClass: "customers-list-files"}, t._l(t.values, function (e, a) {
                    return n("span", {key: e.url, staticClass: "customers-list-file"}, [n("img", {
                        staticClass: "excel-icon",
                        attrs: {src: "/img/proposal/contact_file_icon.png"}
                    }), n("span", {staticClass: "customers-list-file-name adriel-ellipsis"}, [t._v(t._s(e.filename || e.originalname))]), n("span", {staticClass: "customers-list-file-actions"}, [n("a", {
                        attrs: {
                            href: t.makeUrl(e.url || e.path),
                            download: ""
                        }
                    }, [n("img", {
                        staticClass: "download-icon",
                        attrs: {src: "/img/proposal/profile_down_btn.png"}
                    })]), n("img", {
                        staticClass: "delete-icon",
                        attrs: {src: "/img/proposal/profile_delete_icon_dark.png"},
                        on: {
                            click: function (e) {
                                return t.deleteFile(a)
                            }
                        }
                    })])])
                }), 0)]) : n("div", {staticClass: "customers-no-values"}, [n("no-content", {
                    attrs: {imgUrl: "/img/proposal/profile_no_file.png"},
                    on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                }, [t._v("\n                " + t._s(t.$t("PROPOSAL.ap_card_no_customer_list")) + "\n                "), n("br"), t._v("\n                (" + t._s(t.$t("COMMON.file_format")) + " : " + t._s(t.contactFilesTypesText) + ")\n            ")])], 1)], 1)], 1)
            }, i = [], s = n("028d"), o = s["a"], r = (n("b7ca"), n("01e5"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "2a0628d5", null);
        e["a"] = c.exports
    }, 6987: function (t, e, n) {
    }, "6c8e": function (t, e, n) {
    }, "6e43": function (t, e, n) {
        "use strict";
        n.d(e, "g", function () {
            return h
        }), n.d(e, "b", function () {
            return v
        }), n.d(e, "d", function () {
            return g
        }), n.d(e, "c", function () {
            return _
        }), n.d(e, "e", function () {
            return O
        }), n.d(e, "a", function () {
            return y
        }), n.d(e, "f", function () {
            return C
        });
        var a = n("aede"), i = n("2b0e"), s = n("9c56"), o = n("649d");

        function r() {
            var t = Object(a["a"])(["\n    @media screen and (max-width: 800px) {\n        .modal-content {\n            max-height: initial;\n            height: 100vh;\n        }\n    }\n"]);
            return r = function () {
                return t
            }, t
        }

        function c() {
            var t = Object(a["a"])(["\n    width: 100%;\n"]);
            return c = function () {
                return t
            }, t
        }

        function l() {
            var t = Object(a["a"])(["\n    position: absolute;\n    right: 15px;\n    top: 15px;\n    cursor: pointer;\n"]);
            return l = function () {
                return t
            }, t
        }

        function u() {
            var t = Object(a["a"])(["\n    ", "\n    height: 50px;\n    border-radius: 3px;\n    font-size: 16px;\n    font-weight: bold;\n    text-align: center;\n    width: 50%;\n    color: #fff7f7;\n    margin: 0 7px;\n    background-color: ", ";\n    @media screen and (max-width: 800px) {\n        font-size: 14px;\n    }\n"]);
            return u = function () {
                return t
            }, t
        }

        function d() {
            var t = Object(a["a"])(["\n    display: flex;\n    justify-content: space-between;\n    margin: 0 -7px;\n    margin-top: 55px;\n    @media screen and (max-width: 800px) {\n        margin-top: auto;\n    }\n"]);
            return d = function () {
                return t
            }, t
        }

        function p() {
            var t = Object(a["a"])([""]);
            return p = function () {
                return t
            }, t
        }

        function f() {
            var t = Object(a["a"])(["\n    ", "\n    width: 750px;\n    margin: 0 auto;\n    border-radius: 5px;\n    background-color: white;\n    padding: 50px 75px 30px 75px;\n    @media screen and (max-width: 800px) {\n        width: 100vw;\n        height: 100%;\n        padding: 50px 15px 15px 15px;\n        overflow-y: auto;\n        display: flex;\n        flex-direction: column;\n    }\n"]);
            return f = function () {
                return t
            }, t
        }

        function b() {
            var t = Object(a["a"])(["\n    font-family: 'Rubik', 'sans-serif', 'Noto Sans';\n"]);
            return b = function () {
                return t
            }, t
        }

        var m = Object(s["a"])(b()), h = s["b"].div(f(), m), v = s["b"].div(p()), g = s["b"].div(d()),
            _ = Object(s["b"])("button", {isConfirm: Boolean})(u(), m, function (t) {
                var e = t.isConfirm;
                return !0 === e ? "#5aabe3" : "#999999"
            }), O = s["b"].img(l()), y = Object(s["b"])(i["default"].component("ads-selection-modal", o["a"]))(c()),
            C = Object(s["b"])(i["default"].options.components.BModal)(r())
    }, "6e9e": function (t, e, n) {
        "use strict";
        n.r(e);
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return t.loaded ? n("div", {
                    staticClass: "targetWrapper",
                    class: {FORM: t.isFormType}
                }, [t.isModalOpen ? n("inputs-modal", {
                    attrs: {
                        type: "general",
                        active: t.isModalOpen,
                        headerText: t.headers[t.currentEditCmp],
                        placeholder: t.placeholders[t.currentEditCmp],
                        editable: t.isEditable(t.currentEditCmp),
                        addType: t.getAddType(t.currentEditCmp),
                        searchFunc: t.getSearchFunc(t.currentEditCmp),
                        "name-getter": t.getNameGetter(t.currentEditCmp),
                        "value-getter": t.getValueGetter(t.currentEditCmp),
                        "added-values": t.addedValues,
                        "input-validator": t.getInputValidator(t.currentEditCmp),
                        "input-warn-message": t.getInputWarnMessage(t.currentEditCmp),
                        currentEditIdx: t.currentEditIdx,
                        isKeepSearchType: t.getIsKeepSearchType(t.currentEditCmp)
                    }, on: {
                        "update:active": function (e) {
                            t.isModalOpen = e
                        }, confirm: t.handleConfirmModal, cancel: function (e) {
                            return t.setCurrentEditCmp(null)
                        }
                    }
                }, ["searchKeywords" === t.currentEditCmp && t.isLangType("ko") ? n("span", {
                    staticClass: "modal-adder-text",
                    attrs: {slot: "title"},
                    slot: "title"
                }, [t._v("�뺥솗�� �꾩뼱�곌린�� �� 留롮� �몄텧�� �⑸땲��.")]) : t._e()]) : t._e(), t.isConversionsModalOpen ? n("conversions-modal", {
                    attrs: {
                        headerText: t.headers[t.currentEditCmp],
                        active: t.isConversionsModalOpen,
                        selected: t.objectives
                    }, on: {
                        "update:active": function (e) {
                            t.isConversionsModalOpen = e
                        }, confirm: t.handleConfirmModal, cancel: function (e) {
                            return t.setCurrentEditCmp(null)
                        }
                    }
                }) : t._e(), t.isFbAppModalOpen ? n("fb-app-modal", {
                    attrs: {
                        active: t.isFbAppModalOpen,
                        campaignIdProp: t.campaignId
                    }, on: {
                        "update:active": function (e) {
                            t.isFbAppModalOpen = e
                        }, confirm: t.handleConfirmModal, cancel: function (e) {
                            return t.setCurrentEditCmp(null)
                        }
                    }
                }) : t._e(), t.isTrackerModalOpen ? n("tracker-modal", {
                    attrs: {
                        headerText: t.headers[t.currentEditCmp],
                        active: t.isTrackerModalOpen,
                        selected: t.websiteVisitors
                    }, on: {
                        "update:active": function (e) {
                            t.isTrackerModalOpen = e
                        }, confirm: t.handleConfirmModal, cancel: function (e) {
                            return t.setCurrentEditCmp(null)
                        }
                    }
                }) : t._e(), t.isPixelModalOpen ? n("pixel-modal", {
                    attrs: {
                        active: t.isPixelModalOpen,
                        selected: t.websiteVisitors
                    }, on: {
                        "update:active": function (e) {
                            t.isPixelModalOpen = e
                        }, confirm: t.handleConfirmModal, cancel: function (e) {
                            return t.setCurrentEditCmp(null)
                        }
                    }
                }) : t._e(), t._t("navigation"), n("div", {staticClass: "target-summary"}, [n("summary-tags", {attrs: {data: t.audienceTags}})], 1), n("transition", {
                    attrs: {
                        "enter-active-class": "fadeInUp",
                        "leave-active-class": "fadeOutDown"
                    }
                }, [n("div", {staticClass: "components-group"}, t._l(t.types, function (e) {
                    return n("a-comp", t._b({
                        key: e.name,
                        attrs: {type: e.name, isFormType: t.isFormType},
                        on: {
                            "edit:start": function (n) {
                                return t.setCurrentEditCmp(e.name, n)
                            }, "edit:end": function (e) {
                                return t.setCurrentEditCmp(null)
                            }, "update:value": function (e) {
                                return t.setCurrentEditCmp(null)
                            }
                        }
                    }, "a-comp", t.getProps(e.name), !1))
                }), 1)])], 2) : t._e()
            }, i = [], s = n("5913"), o = s["a"], r = (n("1415"), n("7029"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, null, null);
        e["default"] = c.exports
    }, 7029: function (t, e, n) {
        "use strict";
        var a = n("6491"), i = n.n(a);
        i.a
    }, 7503: function (t, e, n) {
    }, "76fb": function (t, e, n) {
    }, "77f0": function (t, e, n) {
    }, 7957: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    staticClass: "conversions-modal",
                    attrs: {active: t._active, canCancel: ["escape", "x"], width: "450px"},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }, close: t.cancel
                    }
                }, [n("div", {
                    ref: "wrapper",
                    staticClass: "conversions-modal-wrapper",
                    style: t.wrapperStyle
                }, [n("div", {
                    ref: "header",
                    staticClass: "conversions-modal-header"
                }, [t._v(t._s(t.$t("PROPOSAL.conversions_modal_title")))]), n("div", {staticClass: "conversions-modal-adder-wrapper"}, [t.websiteVisitors.values.length > 0 ? [n("ObjectiveSelector", {
                    attrs: {
                        value: t.selectedObjectives,
                        campaignId: t.campaignId
                    }, on: {input: t.setSelector}
                }, t._l(t.selectedObjectives, function (e, a) {
                    return n("editable-input", {
                        key: a,
                        attrs: {slot: "tags", editable: "LINK" === e.type, text: e.value + t.getAnalyticType(e)},
                        on: {
                            "click:delete": function (e) {
                                return t.selectedObjectives.splice(a, 1)
                            }, "edit:complete": function (t) {
                                e.value = t
                            }
                        },
                        slot: "tags",
                        model: {
                            value: e.value, callback: function (n) {
                                t.$set(e, "value", n)
                            }, expression: "obj.value"
                        }
                    })
                }), 1)] : [n("div", {staticClass: "no-tracker"}, [n("div", {
                    staticClass: "no-tracker-box",
                    domProps: {innerHTML: t._s(t.$t("PROPOSAL." + t.getUnconnectedMessage))}
                }), n("div", {staticClass: "no-tracker-box"}, [n("span", [t._v(t._s(t.$t("PROPOSAL.tracker_modal_no_ga")))]), n("a", {
                    attrs: {
                        href: "https://support.google.com/analytics/answer/1008015",
                        target: "_blank"
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.tracker_modal_no_ga_help")))])]), t.channelVisibility.showFacebookPixel ? n("div", {staticClass: "no-tracker-box"}, [n("span", [t._v(t._s(t.$t("PROPOSAL.tracker_modal_no_fb")))]), n("a", {
                    attrs: {
                        href: " https://www.facebook.com/business/help/952192354843755?id=1205376682832142",
                        target: "_blank"
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.tracker_modal_no_fb_help")))])]) : t._e()])]], 2), n("div", {staticClass: "buttons-wrapper"}, [n("button", {
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.cancel
                    }
                }, [t._v(t._s(t.$t("COMMON.cancel")))]), n("button", {
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.confirm
                    }
                }, [t._v(t._s(t.$t("COMMON.ok")))])])])])
            }, i = [], s = n("8082"), o = s["a"], r = (n("48f2"), n("ae2b"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "50dc67f6", null);
        e["a"] = c.exports
    }, "7b17": function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("b-modal", {
                attrs: {active: t._active, canCancel: ["escape", "x"], width: "650px"},
                on: {
                    "update:active": function (e) {
                        t._active = e
                    }
                }
            }, [n("div", {staticClass: "before-korean-modal-wrapper"}, [n("div", {staticClass: "before-korean-modal-header"}, [t._v("\n            愿묎퀬 吏묓뻾 ��, 瑗� �뺤씤�섏꽭��!\n        ")]), n("div", {staticClass: "before-korean-modal-body"}, [t._l(t.contents, function (e, a) {
                return n("div", {
                    key: a,
                    staticClass: "before-korean-modal-body__each"
                }, [n("span", {staticClass: "each-title"}, [n("span"), t._v("\n                    " + t._s(e.title) + "\n                ")]), n("span", {
                    staticClass: "each-content",
                    domProps: {innerHTML: t._s(e.content)}
                })])
            }), n("div", {staticClass: "button-wrapper"}, [n("button", {
                on: {
                    click: function (e) {
                        t._active = !1
                    }
                }
            }, [t._v("\n                    " + t._s(t.$t("COMMON.close")) + "\n                ")])])], 2)])])
        }, i = [], s = {
            props: {active: {type: Boolean, default: !1}}, computed: {
                _active: {
                    get: function () {
                        return this.active
                    }, set: function (t) {
                        this.$emit("update:active", t)
                    }
                }, contents: function () {
                    return [{
                        title: "�먰븯�� 愿묎퀬�뚮옯�쇱뿉 �곸젅�� �덉궛�� �ㅼ젙�섏뀲�섏슂?",
                        content: "�덉궛 ��뿉�� �먰븯�� 愿묎퀬�뚮옯�쇱뿉 �덉궛 諛곕텇�� �� �� �덈뒗 諛뺤뒪媛� 蹂댁씠吏� �딅뒗�ㅻ㈃, �쒓킅怨졻�앹뒪�앹뿉�� �대떦 �뚮옯�쇱쓽 愿묎퀬 �ㅼ쐞移섎� 耳쒖꽌 �쒖꽦�붿떆耳쒖＜�몄슂."
                    }, {
                        title: "愿묎퀬 湲곌컙�� �덈Т 吏㏐쾶 �ㅼ젙�섏쭊 �딆쑝�⑤굹��?",
                        content: '\n                        愿묎퀬�� <span class="highlight">�뚯븸�쇱��쇰룄 袁몄���</span> 吏꾪뻾�섎뒗 寃껋씠 以묒슂�⑸땲��. �ㅻ옖 湲곌컙 愿묎퀬媛� �몄텧�섎㈃ �볤� 諛� 醫뗭븘�� �� 利앷� 諛� 理쒖쟻 ��寃�, 理쒖쟻 �뚮옯�쇱쓣 李얠븘媛��� �숈뒿�� 吏꾪뻾�섎㈃�� 愿묎퀬�� �⑥쑉�� 吏��띿쟻�쇰줈 媛쒖꽑�섎뒗 �④낵媛� �덉뒿�덈떎. <span class="highlight">1�� �덉궛�� �묎쾶 �ㅼ젙�섎뜑�쇰룄 1二쇱씪 �댁긽 �ㅻ옯�숈븞 愿묎퀬瑜� 袁몄��� 吏묓뻾�섏떆�� 寃쎌슦, �됯퇏 3諛� �댁긽 愿묎퀬 �깃낵媛� 媛쒖꽑�⑸땲��. </span>\n                    '
                    }, {
                        title: "�쇱젙怨� �덉궛 �ㅼ젙�� �꾩��� �꾩슂�섏떊媛���?",
                        content: '\n                        �뱀옣 �쇰쭏瑜� 愿묎퀬�� �ъ옄�댁빞 �좎� 寃곗젙�섍린 �대졄�ㅻ㈃, <span class="highlight">�섎（ 10-20�щ윭 �뺣룄�� �뚯븸�쇰줈 2二쇱씪媛� 吏묓뻾</span> �� �ㅼ젣 �ъ옄 ��鍮� �④낵 �곗씠�곕� �섏쭛�섎뒗 寃껋씠 �ν썑 愿묎퀬 �꾨왂 �섎┰�� �꾩��� �⑸땲��. <span class="dummy-space"></span>\n                        理쒕�濡� �ъ슜�� �� �덈뒗 珥� �덉궛�� �뺥빐�� �덈떎硫�, <span class="highlight">湲� 湲곌컙 �숈븞 議곌툑�� 諛곕텇�섏뿬 吏묓뻾�섎뒗 寃껋씠 醫뗭뒿�덈떎.</span> 珥� 媛��� �덉궛�� 300�щ윭�� 寃쎌슦, �섎（ 100�щ윭�� 3�쇰쭔�� �뚯쭊�섎뒗 寃껊낫�� �섎（ 30�щ윭�� 10�쇨컙 袁몄��� 愿묎퀬瑜� 吏묓뻾�섎뒗 寃껋씠 鍮꾩슜 ��鍮� �④낵瑜� �믪씠�� 諛⑸쾿�낅땲��.\n                    '
                    }, {
                        title: this.$t("PROPOSAL.billing_faq_title_5"),
                        content: this.$t("PROPOSAL.billing_faq_content_5")
                    }, {
                        title: this.$t("PROPOSAL.billing_faq_title_6"),
                        content: this.$t("PROPOSAL.billing_faq_content_6")
                    }]
                }
            }
        }, o = s, r = (n("259b"), n("2877")), c = Object(r["a"])(o, a, i, !1, null, null, null);
        e["a"] = c.exports
    }, "7d47": function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", [n("card-view", {
                attrs: {
                    label: "Education Level",
                    className: t.className,
                    editable: t.editable,
                    isEditing: t.isEditing,
                    dimmed: t.dimmed,
                    resetable: t.resetable,
                    resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                    confirmLabel: "I'd like to change this setting"
                }, on: {
                    "edit:start": function (e) {
                        return t.$emit("edit:start")
                    }, "edit:end": function (e) {
                        return t.$emit("edit:end")
                    }, "edit:reset": t.reset
                }
            }, [n("on-off-list", {
                attrs: {
                    "name-getter": function (t) {
                        return t.name
                    }, "value-getter": function (t) {
                        return t.value
                    }, values: t.values, images: t.images
                }, on: {
                    "update:value": function (e) {
                        return t.$emit("update:value", e)
                    }
                }
            })], 1)], 1)
        }, i = [], s = n("3de4"), o = n("73ff"), r = {
            mixins: [s["a"]], created: function () {
            }, data: function () {
                return {images: ["/img/proposal/relationship_single.png", "/img/proposal/relationship_relationship.png", "/img/proposal/relationship_married.png"]}
            }, methods: {}, props: {}, components: {"on-off-list": o["a"]}
        }, c = r, l = n("2877"), u = Object(l["a"])(c, a, i, !1, null, null, null);
        e["a"] = u.exports
    }, "7dcd": function (t, e, n) {
    }, 8082: function (t, e, n) {
        "use strict";
        (function (t) {
            n("ac6a");
            var a = n("5176"), i = n.n(a), s = n("cebc"), o = n("2f62"), r = (n("b047"), n("e380"), n("fa7d")),
                c = n("4a48"), l = n("6e77"), u = n("a748"), d = n("17f5"), p = n("c4cc"), f = n("5670"), b = n("ebb6"),
                m = n("3e18"), h = n("9f2d"), v = n("1a2d"), g = n("4b59"), _ = n("baba"), O = n("12e7"),
                y = (n("dde5"), {showGoogleAnalytics: "tracker_modal_no_dec_GA", all: "tracker_modal_no_dec"}),
                C = t.max(0);
            e["a"] = {
                created: function () {
                    var e = t.path(["selected", "values", "0"], this);
                    this.compSelected = Object(r["N"])(e) ? e : {}
                },
                watch: {
                    properties: function () {
                        this.alignVerticalCenter()
                    }
                },
                props: {
                    active: {type: Boolean, default: !1},
                    headerText: String,
                    selected: {type: Object, default: t.always({})}
                },
                data: function () {
                    return {
                        top: void 0,
                        left: "auto",
                        compSelected: void 0,
                        selectedObjectives: [],
                        urlObjective: "",
                        selectedConversion: ""
                    }
                },
                computed: Object(s["a"])({}, Object(o["mapGetters"])("user", ["gaInfo", "fbInfo"]), Object(o["mapGetters"])("campaign", ["campaignId"]), Object(o["mapState"])("campaign", ["gaConversions", "pixelConversions"]), Object(o["mapGetters"])("proposal", ["websiteVisitors"]), {
                    wrapperStyle: function () {
                        return {top: this.top, left: this.left}
                    }, _active: {
                        get: function () {
                            return this.active
                        }, set: function (t) {
                            this.$emit("update:active", t)
                        }
                    }, properties: function () {
                        var e = this.gaInfo.properties || [];
                        return t.isEmpty(e) && Object(r["N"])(this.compSelected) && this.compSelected.id && e.push(this.compSelected), e
                    }, channelVisibility: function () {
                        return t.pick(["showFacebookPixel", "showFacebookSdk", "showGoogleAnalytics"], this.websiteVisitors)
                    }, getUnconnectedMessage: function () {
                        return this.channelVisibility.showFacebookPixel ? y["all"] : y["showGoogleAnalytics"]
                    }
                }),
                components: {"editable-box": _["a"], "editable-input": O["a"], ObjectiveSelector: c["a"]},
                methods: {
                    setSelector: function (t) {
                        this.selectedObjectives = t
                    }, getAnalyticType: function (t) {
                        var e = "";
                        return "ga" === t.analyticType ? e += " (Google)" : "pixel" === t.analyticType && (e += " (Facebook)"), e
                    }, setDrag: function () {
                        var e = this, n = this.$refs.wrapper, a = Object(l["a"])(n, "mousedown"),
                            i = Object(l["a"])(document, "mousemove"), s = Object(l["a"])(document, "mouseup"),
                            o = a.pipe(Object(p["a"])(function (t) {
                                return t.preventDefault()
                            }), Object(f["a"])(t.propEq("which", 1)), Object(b["a"])(function (t) {
                                t.target;
                                var e = t.clientX, a = t.clientY, i = n.getClientRects()[0], s = i.left, o = i.top;
                                return {offsetX: e - s, offsetY: a - o}
                            }), Object(m["a"])(function (t) {
                                var e = t.offsetX, n = t.offsetY;
                                return i.pipe(Object(b["a"])(function (t) {
                                    var a = t.clientX, i = t.clientY;
                                    return {left: a - e + "px", top: i - n + "px"}
                                }), Object(h["a"])(s))
                            }));
                        this.$subscribeTo(o, function (t) {
                            var n = t.top, a = t.left;
                            e.top = n, e.left = a
                        })
                    }, confirm: function () {
                        this.$emit("confirm", this.selectedObjectives), this.cancel()
                    }, cancel: function () {
                        this._active = !1, this.$emit("cancel")
                    }, radioSelected: function (t) {
                        this.compSelected = t
                    }, alignVerticalCenter: function () {
                        var t = C(window.innerHeight / 2 - Object(r["A"])(this.$refs.wrapper) / 2);
                        this.top = "".concat(t, "px")
                    }
                },
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        t.alignVerticalCenter()
                    }), this.selectedObjectives = i()([], this.selected.values)
                },
                subscriptions: function () {
                    var e = this,
                        n = Object(u["a"])(Object(d["a"])(""), Object(l["a"])(window, "resize")).pipe(Object(v["a"])(200), Object(b["a"])(function () {
                            return .8 * window.innerHeight
                        }), Object(b["a"])(t.flip(t.subtract)(200)), Object(g["a"])(), Object(h["a"])(this.getBeforeDestroy$()));
                    return n.pipe(Object(v["a"])(300)).subscribe(function () {
                        e.alignVerticalCenter()
                    }), {maxHeight: n}
                }
            }
        }).call(this, n("b17e"))
    }, 8398: function (t, e, n) {
    }, "869a": function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement;
            t._self._c;
            return t._m(0)
        }, i = [function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "PaypalButton"}, [n("div", {attrs: {id: "paypal-button"}})])
        }], s = (n("c5f6"), n("1bc2")), o = {
            name: "PaypalButton",
            props: {amount: {type: Number, required: !0}},
            methods: {
                createButton: function () {
                    var t = this, e = function (e, n) {
                        return console.log("createPayment", e, "actions", n), s["a"].createPayment(t.amount).then(function (t) {
                            console.log("createPayment", t), e(t.paymentId)
                        })
                    }, n = function (e, n) {
                        return console.log("onAuthorize", e, n), s["a"].executePayment({
                            payerId: e.payerID,
                            paymentId: e.paymentID,
                            amount: t.amount,
                            campaignId: t.$store.state.campaign.campaign.id
                        }).then(function (e) {
                            t.$emit("success"), t.$toast.open({message: "Payment successful", type: "is-primary"})
                        })
                    }, a = Object({
                        NODE_ENV: "production",
                        VUE_APP_STRIPE_PUBLIC_KEY: "pk_live_OKISgmc18NAmcQfyGPUncS5z",
                        VUE_APP_INTERCOM_APP_ID: "i11ny0xx",
                        VUE_APP_CAULY_TRACK_CODE: "07016bef-e1a2-4279-880f-1f9ed598ea04",
                        VUE_APP_GA_SEND_HIT: "1",
                        VUE_APP_API_URL: "https://app.adriel.ai/api",
                        VUE_APP_URL: "https://app.adriel.ai",
                        VUE_APP_FORM_URL: "https://form.adriel.ai",
                        VUE_APP_I18N_LOCALE: "en",
                        VUE_APP_I18N_FALLBACK_LOCALE: "en",
                        BASE_URL: "/"
                    }).PAYPAL_ENV;
                    paypal.Button.render({
                        env: a,
                        style: {layout: "vertical", size: "responsive", shape: "rect", color: "silver"},
                        funding: {allowed: [paypal.FUNDING.CARD]},
                        payment: e,
                        onAuthorize: n,
                        onError: function (t, e) {
                            console.log(t, e), console.log("ERRORRRRRR")
                        }
                    }, "#paypal-button")
                }
            },
            mounted: function () {
                this.createButton()
            }
        }, r = o, c = n("2877"), l = Object(c["a"])(r, a, i, !1, null, null, null);
        e["a"] = l.exports
    }, 8909: function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("19e8"), i = (n("fa7d"), t.defaultTo("-")), s = function (t) {
                return void 0 === t || null === t
            };
            e["a"] = {
                props: ["className", "value"],
                components: {"card-view": a["a"]},
                computed: {
                    _clicks: function () {
                        return this.adjustValues(t.pathOr([], ["value", "clicks"])(this))
                    }
                },
                methods: {
                    adjustValues: function (e) {
                        return t.all(s, e) ? null : "".concat(i(e[0]), " ~ ").concat(i(e[1]))
                    }
                }
            }
        }).call(this, n("b17e"))
    }, "8b04": function (t, e, n) {
        "use strict";
        var a = n("cad0"), i = n.n(a);
        i.a
    }, "8bcc": function (t, e, n) {
    }, "8f77": function (t, e, n) {
        "use strict";
        var a = n("ec3e"), i = n.n(a);
        i.a
    }, "91f0": function (t, e, n) {
        "use strict";
        var a = n("fea0"), i = n.n(a);
        i.a
    }, "92e8": function (t, e, n) {
    }, 9599: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("card-view", {
                    staticClass: "estimated-result-card",
                    class: t.className,
                    attrs: {label: t.$t("PROPOSAL.plan_title_results"), editable: !1}
                }, [n("div", {staticClass: "estimation-summary"}, [t._clicks ? n("span", {staticClass: "estimation-summary-elem-full estimation-summary-audience"}, [n("span", {staticClass: "estimated-ad-audience"}, [t._v("\n                " + t._s(t.$t("PROPOSAL.plan_estimated_clicks")) + "\n            ")]), n("span", {staticClass: "estimated-ad-audience-data"}, [t._v(t._s(t.$t("PROPOSAL.estimation_per_day", {value: t._clicks})))])]) : n("span", {staticClass: "estimation-summary-elem-full estimation-summary-audience"}, [n("span", {staticClass: "estimated-ad-audience-data"}, [t._v("\n                " + t._s(t.$t("PROPOSAL.plan_estimated_not_available")) + "\n            ")])])]), n("span", {staticClass: "estimated-warning"}, [t._v("\n        " + t._s(t.$t("PROPOSAL.plan_estimated_warning")) + "\n    ")])])
            }, i = [], s = n("8909"), o = s["a"], r = (n("e996"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, null, null);
        e["a"] = c.exports
    }, "95a6": function (t, e, n) {
        "use strict";
        var a = n("f745"), i = n.n(a);
        i.a
    }, "9c1a": function (t, e, n) {
    }, "9e4e": function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "website-visitors"}, [n("card-view", {
                    attrs: {
                        label: "",
                        className: t.className,
                        editable: !1,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        settingLabel: t.$t("PROPOSAL.ap_card_edit_visitor"),
                        settingImgSrc: "/img//box_edit_icon.png",
                        confirmLabel: "I'd like to change this setting"
                    }, on: {
                        "edit:start": t.handleLogin, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [n("span", {
                    staticClass: "card-title",
                    attrs: {slot: "slot-header"},
                    slot: "slot-header"
                }, [t._v("\n            " + t._s(t.cardTitle) + "\n            "), n("b-icon", {
                    directives: [{
                        name: "tooltip",
                        rawName: "v-tooltip",
                        value: t.$t("PROPOSAL.ap_card_title_tracker_tooltip"),
                        expression: "$t('PROPOSAL.ap_card_title_tracker_tooltip')"
                    }],
                    style: {"font-size": "12px", color: "$#666666", width: "auto", height: "auto"},
                    attrs: {pack: "fas", icon: "question-circle"}
                })], 1), "WEB" === t.type ? [t.noTools ? n("div", {staticClass: "visitors-no-tools"}, [n("no-content", [n("span", {
                    staticClass: "ment-no-tools word-break",
                    domProps: {innerHTML: t._s(t.$t("PROPOSAL.ap_card_no_tracker"))}
                })])], 1) : n("div", {
                    staticClass: "visitors-has-tools",
                    class: {both: t.google && t.pixel}
                }, [t.google ? n("div", {staticClass: "tracker-container google"}, [n("span", {staticClass: "visitors-ment-guide"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_tracker_info")))]), n("div", [n("span", {staticClass: "page-elem-box"}, [n("span", {staticClass: "adriel-ellipsis"}, [n("img", {attrs: {src: "/img/proposal/login_google_icon.png"}}), n("span", {staticClass: "page-name"}, [t._v(t._s(t.google.name || "-"))]), n("span", {staticClass: "page-url adriel-ellipsis"}, [t._v(t._s(" | " + (t.google.websiteUrl || "-") + " " + (t.google.propertyId || t.google.id || "-")))])]), n("img", {
                    staticClass: "delete-icon",
                    attrs: {src: "/img/proposal/profile_delete_icon_dark.png"},
                    on: {
                        click: function (e) {
                            return t.$emit("update:remove", "google")
                        }
                    }
                })])])]) : t._e(), t.pixel ? n("div", {staticClass: "tracker-container pixel"}, [n("span", {staticClass: "visitors-ment-guide"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_tracker_pixel_info")))]), n("div", [n("span", {staticClass: "page-elem-box"}, [n("span", {staticClass: "adriel-ellipsis"}, [n("img", {attrs: {src: "/img/proposal/facebook_icon.png"}}), n("span", {staticClass: "page-name"}, [t._v(t._s(t.pixel.name || "-"))]), n("span", {staticClass: "page-url adriel-ellipsis"}, [t._v(t._s(" |  " + (t.pixel.propertyId || t.pixel.id || "-")))])]), n("img", {
                    staticClass: "delete-icon",
                    attrs: {src: "/img/proposal/profile_delete_icon_dark.png"},
                    on: {
                        click: function (e) {
                            return t.$emit("update:remove", "facebook")
                        }
                    }
                })])])]) : t._e()]), n("div", {staticClass: "tracker-buttons"}, [t.google ? t._e() : [t.noTools ? t._e() : n("div", {staticClass: "visitors-tooltip"}, [n("span", [t._v(t._s(t.$t("PROPOSAL.ap_card_tracker_google_content")))])]), n("button", {
                    staticClass: "auth-btn google",
                    on: {click: t.handleLogin}
                }, [n("img", {attrs: {src: "/img/proposal/login_google_icon.png"}}), n("span", [t._v(t._s(t.$t("PROPOSAL.ap_card_login_google_tracker")))])])], t.channelVisibility.showFacebookPixel && !t.pixel ? [t.noTools ? t._e() : n("div", {staticClass: "visitors-tooltip"}, [n("span", [t._v(t._s(t.$t("PROPOSAL.ap_card_tracker_pixel_content")))])]), n("button", {
                    staticClass: "auth-btn pixel",
                    on: {click: t.handlePixelLogin}
                }, [n("img", {attrs: {src: "/img/proposal/login_pixel_icon.png"}}), n("span", [t._v(t._s(t.$t("PROPOSAL.ap_card_login_pixel_tracker")))])])] : t._e()], 2)] : [n("div", {staticClass: "fb-app-container"}, [t.fbApp ? [n("div", {staticClass: "tracker-container fbApp"}, [n("span", {staticClass: "visitors-ment-guide"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_tracker_fb_app_info")))]), n("div", [n("span", {staticClass: "page-elem-box"}, [n("span", {staticClass: "adriel-ellipsis"}, [n("img", {attrs: {src: "/img/proposal/facebook_icon.png"}}), n("span", {staticClass: "page-name"}, [t._v(t._s(t.fbApp.name || "-"))]), n("span", {staticClass: "page-url adriel-ellipsis"}, [t._v(t._s(" |  " + (t.fbApp.propertyId || t.fbApp.id || "-")))])]), n("img", {
                    staticClass: "delete-icon",
                    attrs: {src: "/img/proposal/profile_delete_icon_dark.png"},
                    on: {
                        click: function (e) {
                            return t.$emit("update:remove", "fbApp")
                        }
                    }
                })])])])] : [n("div", {staticClass: "fb-app-container--no-app"}, [n("span", {
                    staticClass: "fb-app-container--no-app--text",
                    domProps: {innerHTML: t._s(t.$t("COMMON.fb_app_box_content"))}
                }), n("button", {
                    staticClass: "auth-btn pixel",
                    on: {click: t.handleFbAppLogin}
                }, [n("img", {attrs: {src: "/img/proposal/login_pixel_icon.png"}}), n("span", [t._v(t._s(t.$t("COMMON.fb_app_modal_header")))])])])]], 2)]], 2)], 1)
            }, i = [], s = n("59ed"), o = s["a"], r = (n("ff9e"), n("bf61"), n("1efe"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "17bcd92d", null);
        e["a"] = c.exports
    }, a137: function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("cebc"), i = n("2f62"), s = n("fe51");
            e["a"] = {
                props: ["indicators", "stepIndex", "icon", "isLastStep", "canLaunch", "campaignTitle", "baseType"],
                created: function () {
                },
                data: function () {
                    return {isTitleUpdate: !1}
                },
                computed: Object(a["a"])({}, Object(i["mapGetters"])("campaign", ["campaign"]), {
                    title: function () {
                        return t.compose(this.$t.bind(this), t.path(["indicators", this.stepIndex, "label"]))(this)
                    }, subLabel: function () {
                        return t.compose(this.$t.bind(this), t.path(["indicators", this.stepIndex, "subLabel"]))(this)
                    }, imageStyle: function () {
                        var t = {"background-position": "center", "background-repeat": "no-repeat"};
                        return this.icon ? Object(a["a"])({}, t, {
                            "background-image": "url('".concat(this.icon, "')"),
                            "background-size": "cover"
                        }) : Object(a["a"])({}, t, {
                            "background-image": "url('/img/proposal/ads_title_default_icon.png')",
                            "background-size": "auto"
                        })
                    }
                }),
                components: {"update-title": s["a"]},
                methods: {
                    requestGo: function () {
                        this.isLastStep && this.$emit("request:go")
                    }
                }
            }
        }).call(this, n("b17e"))
    }, a1d5: function (t, e, n) {
        "use strict";
        n.r(e);
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "plan-wrapper"}, [t.isBillingModalActive ? n("billing-faq", {
                    attrs: {active: t.isBillingModalActive},
                    on: {
                        "update:active": function (e) {
                            t.isBillingModalActive = e
                        }
                    }
                }) : t._e(), t.isBeforeKoreanModalActive ? n("before-korean", {
                    attrs: {active: t.isBeforeKoreanModalActive},
                    on: {
                        "update:active": function (e) {
                            t.isBeforeKoreanModalActive = e
                        }
                    }
                }) : t._e(), t.isPreviewModalActive ? n("preview-modal", {
                    attrs: {active: t.isPreviewModalActive},
                    on: {
                        "update:active": function (e) {
                            t.isPreviewModalActive = e
                        }
                    }
                }) : t._e(), t.isCouponsModalActive ? n("coupons-modal", {
                    attrs: {
                        active: t.isCouponsModalActive,
                        coupons: t.coupons,
                        campaignId: t.campaignId
                    }, on: {
                        "update:active": function (e) {
                            t.isCouponsModalActive = e
                        }, "update:add": t.addCoupon, "update:bind": t.bindCoupons
                    }
                }) : t._e(), n("modal-box", {
                    attrs: {
                        active: t.isBusinessModalActive,
                        hasButtons: !1,
                        headerText: t.$t("VAT.submit_business_no")
                    }, on: {
                        "update:active": function (e) {
                            t.isBusinessModalActive = e
                        }, confirm: t.handleCert
                    }
                }, [n("business-cert", {
                    attrs: {type: "modal-type"}, on: {
                        cancel: function (e) {
                            t.isBusinessModalActive = !1
                        }, complete: function (e) {
                            t.isBusinessModalActive = !1
                        }
                    }
                })], 1), t._t("navigation"), t.loaded ? n("div", {staticClass: "plan-cards-wrapper"}, [n("card-view", {
                    staticClass: "plan-big-card card-schedule",
                    attrs: {
                        editable: !1,
                        isEditableAlways: !0,
                        resetable: t.isResetable("schedule"),
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        label: t.$t("PROPOSAL.plan_title_schedule")
                    },
                    on: {"edit:reset": t.resetSchedule}
                }, [n("div", {staticClass: "schedule-radio"}, [n("div", {staticClass: "schedule-radio-elem"}, [n("adriel-radio", {
                    attrs: {
                        "native-value": !0,
                        label: t.$t("PROPOSAL.plan_schedule_continuously")
                    }, model: {
                        value: t._continuously, callback: function (e) {
                            t._continuously = e
                        }, expression: "_continuously"
                    }
                })], 1), n("div", {staticClass: "schedule-radio-elem"}, [n("adriel-radio", {
                    attrs: {
                        "native-value": !1,
                        label: t.$t("PROPOSAL.plan_schedule_start_end")
                    }, model: {
                        value: t._continuously, callback: function (e) {
                            t._continuously = e
                        }, expression: "_continuously"
                    }
                })], 1)]), t._continuously ? n("div", {staticClass: "result-continuously"}, [n("img", {attrs: {src: "/img/proposal/run_my_campaign_icon.png"}})]) : n("div", [n("div", {staticClass: "schedule-calendar"}, [n("b-field", [n("div", [n("span", [t._v(t._s(t.$t("PROPOSAL.plan_schedule_start")))]), n("info-tooltip", {
                    style: {"margin-left": "5px"},
                    attrs: {label: t.$t("PROPOSAL.plan_schedule_start_tooltip")}
                }), n("div", {staticClass: "asap-box"}, [n("div", {staticClass: "schedule-radio-elem"}, [n("adriel-radio", {
                    attrs: {
                        "native-value": !0,
                        label: t.$t("PROPOSAL.plan_schedule_asap")
                    }, model: {
                        value: t._asap, callback: function (e) {
                            t._asap = e
                        }, expression: "_asap"
                    }
                })], 1), n("div", {staticClass: "schedule-radio-elem"}, [n("adriel-radio", {
                    attrs: {
                        "native-value": !1,
                        label: t.$t("PROPOSAL.plan_schedule_set_start")
                    }, model: {
                        value: t._asap, callback: function (e) {
                            t._asap = e
                        }, expression: "_asap"
                    }
                })], 1)])], 1), n("div", {
                    staticClass: "schedule-box",
                    class: {"schedule-disabled": t.startDisabled}
                }, [n("b-datepicker", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: !t._asap,
                        expression: "!_asap"
                    }],
                    attrs: {
                        placeholder: t.$t("PROPOSAL.select_date"),
                        icon: "calendar-today",
                        "min-date": t.minStartDate,
                        "date-formatter": t.dateFormat,
                        disabled: t.startDisabled,
                        "month-names": t.$t("PROPOSAL.schedule_date_month_names"),
                        "day-names": t.$t("PROPOSAL.schedule_day_names")
                    },
                    model: {
                        value: t._startDate, callback: function (e) {
                            t._startDate = e
                        }, expression: "_startDate"
                    }
                })], 1)])], 1), n("div", {staticClass: "schedule-calendar end"}, [n("b-field", {attrs: {label: t.$t("PROPOSAL.plan_schedule_end")}}, [n("div", {staticClass: "schedule-box"}, [n("b-datepicker", {
                    attrs: {
                        placeholder: t.$t("PROPOSAL.select_date"),
                        icon: "calendar-today",
                        "min-date": t.minEndDate,
                        "date-formatter": t.dateFormat,
                        "month-names": t.$t("PROPOSAL.schedule_date_month_names"),
                        "day-names": t.$t("PROPOSAL.schedule_day_names")
                    }, model: {
                        value: t._endDate, callback: function (e) {
                            t._endDate = e
                        }, expression: "_endDate"
                    }
                })], 1)])], 1)]), n("div", {staticClass: "service-preview-text"}, [n("span", {
                    on: {
                        click: function (e) {
                            t.isPreviewModalActive = !0
                        }
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.service_preview_text")))])]), t.isLangType("ko") ? n("div", {staticClass: "service-preview-text ko"}, [n("span", {
                    on: {
                        click: function (e) {
                            t.isBeforeKoreanModalActive = !0
                        }
                    }
                }, [t._v("愿묎퀬 吏묓뻾 ��, 瑗� �뺤씤�섏꽭��!")])]) : t._e()]), n("card-view", {
                    staticClass: "plan-big-card card-budget",
                    attrs: {
                        editable: !1,
                        isEditableAlways: !0,
                        resetable: t.isResetable("budget"),
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        label: t.$t("PROPOSAL.plan_title_budget")
                    },
                    on: {"edit:reset": t.resetBudget}
                }, [n("div", {staticClass: "budget-summary"}, [n("span", {staticClass: "budget-summary-daily-wrapper"}, [n("span", {staticClass: "budget-label"}, [t._v("\n                        " + t._s(t.$t("PROPOSAL.plan_budget_daily")) + "\n                        "), n("info-tooltip", {attrs: {label: t.$t("PROPOSAL.overspent_tooltip")}})], 1), n("span", {staticClass: "budget-period"}, [n("span", {staticClass: "budget-value"}, [t._v(t._s(t.formatCurrency(t.dailyBudget)))]), n("span", {staticClass: "budget-etc"}, [t._v("/ " + t._s(t.$t("PROPOSAL.plan_budget_day")))])])]), t._continuously ? t._e() : n("span", {staticClass: "budget-summary-total-wrapper"}, [n("span", {staticClass: "budget-label"}, [t._v(t._s(t.$t("PROPOSAL.plan_budget_total")))]), n("span", {staticClass: "budget-value"}, [t._v(t._s(t.formatCurrency(t.totalBudget)))])])]), n("div", {staticClass: "divider"}), t.hasNoActiveAds ? n("div", {staticClass: "no-ads-enabled text-center"}, [t._v("\n                " + t._s(t.$t("PROPOSAL.plan_reason_no_ads_enabled")) + "\n                "), n("button", {
                    staticClass: "goToAdsBtn",
                    on: {
                        click: function (e) {
                            return t.goToAds("all")
                        }
                    }
                }, [t._v("\n                    " + t._s(t.$t("PROPOSAL.plan_go_to_ads")) + "\n                    "), n("img", {attrs: {src: "/img/proposal/next_step_icon.png"}})])]) : t._e(), n("div", {staticClass: "budget-each-wrapper"}, t._l(t._budgetChannels, function (e) {
                    return n("span", {key: e.name}, [t.channelAdExists(e) ? n("span", {
                        directives: [{
                            name: "tooltip",
                            rawName: "v-tooltip",
                            value: t.getTooltip(e),
                            expression: "getTooltip(budget)"
                        }], staticClass: "budget-each-elem", class: "budget_" + e.name
                    }, [n("img", {attrs: {src: t.getBudgetIcon(e.name)}}), n("span", {
                        staticClass: "input-currency-wrapper",
                        class: "currency-" + t.globalUser.currency
                    }, [n("adriel-input", {
                        directives: [{
                            name: "stream",
                            rawName: "v-stream:input",
                            value: t.budgetInput$,
                            expression: "budgetInput$",
                            arg: "input"
                        }],
                        ref: "budgetInput",
                        refInFor: !0,
                        staticClass: "budget-channel-input-field",
                        attrs: {
                            placeholder: "budget for channel",
                            value: t.getCommaValue(e.value),
                            type: "number",
                            name: e.name
                        },
                        nativeOn: {
                            blur: function (n) {
                                return t.budgetChanged(e, n)
                            }, keydown: function (e) {
                                return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.blurElement(e)
                            }
                        }
                    })], 1), n("span", {staticClass: "budget-channel-day-text"}, [t._v("/ " + t._s(t.$t("PROPOSAL.plan_budget_day")))])]) : n("span", {
                        staticClass: "budget-each-elem",
                        class: "budget_" + e.name
                    }, [n("v-popover", {
                        attrs: {
                            trigger: "manual",
                            open: t.budgetPopovers[e.name],
                            "auto-hide": !0,
                            "open-group": e.name
                        }, on: {
                            "update:open": function (n) {
                                return t.updatePopover(e.name, n)
                            }
                        }
                    }, [n("img", {attrs: {src: t.getBudgetIcon(e.name)}}), n("span", {
                        staticClass: "input-currency-wrapper",
                        class: "currency-" + t.globalUser.currency
                    }, [n("adriel-input", {
                        ref: "budgetInput",
                        refInFor: !0,
                        staticClass: "budget-channel-input-field",
                        attrs: {
                            placeholder: "budget for channel",
                            value: t.getCommaValue(e.value),
                            type: "tel",
                            name: e.name,
                            readonly: ""
                        },
                        nativeOn: {
                            click: function (n) {
                                return t.updatePopover(e.name)
                            }, keydown: function (e) {
                                return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.blurElement(e)
                            }
                        }
                    })], 1), n("span", {staticClass: "budget-channel-day-text"}, [t._v("/ " + t._s(t.$t("PROPOSAL.plan_budget_day")))]), n("template", {slot: "popover"}, [t._v("\n                                " + t._s(t.$t("PROPOSAL.plan_reason_" + t.getReasonAds(e.name), {channel: e.name})) + "\n                                "), n("button", {
                        staticClass: "goToAdsBtn",
                        on: {
                            click: function (n) {
                                return t.goToAds(e.name)
                            }
                        }
                    }, [t._v("\n                                    " + t._s(t.$t("PROPOSAL.plan_go_to_ads")) + "\n                                    "), n("img", {attrs: {src: "/img/proposal/next_step_icon.png"}})])])], 2)], 1)])
                }), 0), t.schedule ? n("div", {staticClass: "payment-list-wrapper price"}, [n("h4", [t._v("\n                    " + t._s(t.$t("PROPOSAL.price_to_pay")) + "\n                    "), n("span", {
                    on: {
                        click: function (e) {
                            t.isBillingModalActive = !0
                        }
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.billing_faq")))])]), n("hr"), n("div", {staticClass: "details"}, [n("div", {staticClass: "details-each"}, [n("span", {staticClass: "left"}, [t._v("\n                            " + t._s(t.$t("PROPOSAL.price_platform_fee")) + "\n                            "), n("info-tooltip", {attrs: {label: t.$t("PROPOSAL.price_platform_fee_tooltip")}})], 1), n("span", {staticClass: "right"}, [n("span", {staticClass: "amount"}, [t._v("\n                                " + t._s(t.formatCurrency(t.pricePlatformFee)) + "\n                            ")]), t._continuously ? n("span", {
                    staticClass: "unit",
                    domProps: {textContent: t._s("/ " + t.$t("PROPOSAL.price_day"))}
                }) : t._e()])]), n("div", {staticClass: "details-each"}, [n("span", {staticClass: "left"}, [t._v("\n                            " + t._s(t.$t("PROPOSAL.price_service_fee")) + "\n                            "), n("info-tooltip", {attrs: {label: t.$t("PROPOSAL.price_service_fee_tooltip")}})], 1), n("span", {staticClass: "right"}, [n("span", {staticClass: "amount"}, [t._v(t._s(t.formatCurrency(t.priceServiceFee)))]), t._continuously ? n("span", {
                    staticClass: "unit",
                    domProps: {textContent: t._s("/ " + t.$t("PROPOSAL.price_day"))}
                }) : t._e()])]), t.maybeKorean ? n("div", {staticClass: "details-each tax"}, [n("span", {staticClass: "left"}, [n("span", [t._v(t._s(t.$t("VAT.vat")))]), n("button", {
                    on: {
                        click: function (e) {
                            t.isBusinessModalActive = !0
                        }
                    }
                }, [t._v(t._s(t.$t("VAT.vat_qualify")))])]), n("span", {staticClass: "right"}, [n("span", {staticClass: "total-amount"}, [t._v(t._s(t.formatCurrency(t.vat)))]), t._continuously ? n("span", {
                    staticClass: "unit",
                    domProps: {textContent: t._s("/ " + t.$t("PROPOSAL.price_day"))}
                }) : t._e()])]) : t._e(), n("div", {staticClass: "details-each"}, [n("span", {staticClass: "left"}, [t._v("\n                            " + t._s(t.$t("PROPOSAL.price_total")) + "\n                            "), n("info-tooltip", {attrs: {label: t.$t("PROPOSAL.price_total_tooltip")}})], 1), n("span", {staticClass: "right"}, [n("span", {staticClass: "total-amount"}, [t._v(t._s(t.formatCurrency(t.pricePlatformFee + t.priceServiceFee + t.vat)))]), t._continuously ? n("span", {
                    staticClass: "unit",
                    domProps: {textContent: t._s("/ " + t.$t("PROPOSAL.price_day"))}
                }) : t._e()])]), n("div", {staticClass: "details-each credit"}, [n("div", [n("span", {staticClass: "left"}, [t._v(t._s(t.$t("PROPOSAL.price_credit")))]), n("span", {staticClass: "right"}, [n("span", {staticClass: "credit-used"}, [t._v(t._s(t.formatCurrency(t.couponAssigned)))]), n("span", {staticClass: "credit-total"}, [t._v("/ " + t._s(t.formatCurrency(t.couponAmount)))]), n("button", {
                    on: {
                        click: function (e) {
                            t.isCouponsModalActive = !0
                        }
                    }
                }, [t._v(t._s(t.$t("COUPON.coupon")))])])]), t.couponTextVisible ? n("div", {staticClass: "coupon-text"}, [t._v("\n                            10�щ윭源뚯��� �꾨뱶由ъ뿕�� 愿묎퀬鍮꾨� 遺��댄빐�쒕┰�덈떎.\n                            "), n("br"), t._v("�щ젅�㏃쑝濡� �ㅻ뒛 諛붾줈 �쒖옉�대낫�몄슂.\n                        ")]) : t._e()])])]) : t._e(), n("div", {staticClass: "payment-list-wrapper method"}, [n("h4", [t._v(t._s(t.$t("PROPOSAL.plan_budget_payment_method")))]), n("hr"), n("default-payment-method", {attrs: {deletable: !1}})], 1)]), n("div", {staticClass: "plan-additionals"}, [n("estimated-view", {
                    staticClass: "plan-normal-card",
                    attrs: {value: t.estimations}
                }), n("optimization-toggle", {
                    staticClass: "plan-small-card",
                    attrs: {isAuto: t.autoOptimization},
                    on: {"update:isAuto": t.setAutoOptimization}
                })], 1)], 1) : t._e()], 2)
            }, i = [], s = n("b84f"), o = s["a"], r = (n("04f35"), n("e190"), n("91f0"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "9868beca", null);
        e["default"] = c.exports
    }, a61b: function (t, e, n) {
    }, a79b: function (t, e, n) {
        "use strict";
        n.r(e), function (t) {
            var a = n("2638"), i = n.n(a), s = (n("7514"), n("75fc")), o = (n("ac6a"), n("cebc")),
                r = (n("a481"), n("774e")), c = n.n(r), l = n("e814"), u = n.n(l), d = n("4c6b"), p = n("ea61"),
                f = n("d868"), b = n("f876f"), m = n("3352"), h = n("2f62"), v = n("fa7d"), g = n("a772"),
                _ = n("a79c"), O = n("2bd2"), y = n("808d"), C = n("9586"), P = n("1b92"), A = n("d3fb"), x = n("a748"),
                w = n("6e77"), j = n("59b7"), k = n("ebb6"), $ = n("d792"), S = n("66aa"), L = n("f59d"), M = n("9f2d"),
                R = n("d96a"), I = n("d337"), T = n("b7d7"), E = n("4677"), B = n("1585"), N = n("5670"), F = n("bee6"),
                V = n("3fab"), D = n("d263"),
                z = t.compose(t.not, t.ifElse(t.is(Boolean), t.identity, t.prop("value")), t.defaultTo({}), g["h"]),
                G = t.applySpec({
                    url: t.prop("baseUrl"),
                    webUrl: t.prop("baseUrl"),
                    appStoreUrl: t.prop("baseAppStoreUrl"),
                    playStoreUrl: t.prop("basePlayStoreUrl")
                }), U = function (t) {
                    var e = t.item, n = t.rowGap, a = t.rowHeight, i = e.querySelector(".m-content");
                    if (i) {
                        var s = Math.ceil((i.getBoundingClientRect().height + n) / (a + n));
                        e.style.gridRowEnd = "span ".concat(s)
                    }
                }, q = function () {
                    var t = document.getElementsByClassName("masonry")[0];
                    if (Object(v["N"])(t)) for (var e = window.getComputedStyle(t), n = u()(e.getPropertyValue("grid-row-gap")), a = u()(e.getPropertyValue("grid-auto-rows")), i = document.getElementsByClassName("m-item"), s = 0; s < i.length; s++) U({
                        item: i[s],
                        rowGap: n,
                        rowHeight: a
                    })
                },
                K = t.compose(t.prop("length"), t.intersection(["next", "prev", "arrow-left", "arrow-right"]), c.a, t.path(["target", "classList"]));
            e["default"] = {
                created: function () {
                    var e = this;
                    this.$nextTick(function () {
                        e.observeProfile$(), e.observeRedraw$()
                    }), this.emphasizeTurnOn = !!t.path(["$route", "query", "emphasize"], this)
                },
                watch: {
                    campaignId: {
                        handler: function () {
                            this.profile$.next(500)
                        }, immediate: !0
                    }, fbPageInfo: {
                        handler: function (t) {
                            this.fbCoverVisible = !Object(v["N"])(t)
                        }, immediate: !0
                    }
                },
                mounted: function () {
                    this.watchChannelAdsFirst$();
                    var e = this.$route.query, n = e.channel;
                    this.$router.replace({query: t.dissoc("emphasize", e)}), this.setSelectedChannel(n), this.visibleAdIds = t.compose(v["f"], t.pathOr([], ["$route", "query", "ids"]))(this)
                },
                data: function () {
                    return {
                        selectedChannel: "all",
                        profile$: new O["a"],
                        redraw$: new O["a"],
                        cancelClick$: new O["a"],
                        modalMeta: {fbPages: !1, adsSelection: !1, appstore: !1},
                        emphasizeTurnOn: !1,
                        copyOriginId: void 0,
                        copyBackup: {},
                        adsBackup: {},
                        copyHighlighted: !1,
                        fbCoverVisible: !1,
                        visibleAdIds: []
                    }
                },
                render: function () {
                    var t = arguments[0], e = this.fbPageButtonVisible, n = this.showFbModal,
                        a = this.renderFbPagesModal, i = this.renderAdsSelectionModal, s = this.renderAppstoreModal,
                        o = this.allVisible, r = this.renderChannels, c = this.renderTopBtn, l = this.renderAds;
                    return t(p["L"], {ref: "container"}, [a, i, s, t(p["g"], [o && r]), t(p["d"], [e && t(p["y"], {nativeOn: {click: n}}, [t(p["w"], {attrs: {src: "/img/icons/proposal_fb_page_icon.png"}}), t(p["z"], [this.$t("PROPOSAL.change_fb_page")])]), c]), l])
                },
                computed: Object(o["a"])({}, Object(h["mapGetters"])("proposal", ["channelAds", "facebookProfile", "loaded", "baseType", "baseUrl", "baseAppStoreUrl", "basePlayStoreUrl", "generatedAds", "baseAppstore"]), Object(h["mapGetters"])("campaign", ["campaign", "campaignId", "intelFacebookPage", "isUpdate", "appleAppIdConnected"]), {
                    channels: function () {
                        var e = t.compose(t.keys, t.groupBy(t.prop("channel")), t.prop("channelAds"))(this);
                        return ["all"].concat(Object(s["a"])(t.innerJoin(t.equals, d["a"], e)))
                    },
                    _filterFunc: function () {
                        var e = this.selectedChannel, n = {};
                        return "all" !== e && (n.channel = t.equals(e)), t.where(n)
                    },
                    displayAds: t.compose(t.apply(t.concat), t.partition(t.prop("enabled")), t.prop("ads")),
                    ads: function () {
                        var e = this;
                        return t.compose(t.filter(this._filterFunc), function (n) {
                            return e.allVisible ? n : t.innerJoin(function (t, e) {
                                var n = t.creativeId;
                                return n === e
                            }, n, e.visibleAdIds)
                        }, t.propOr([], "channelAds"))(this)
                    },
                    fbPageInfo: function () {
                        return this.intelFacebookPage || this.facebookProfile
                    },
                    fbPageButtonVisible: t.compose(t.test(/(facebook|instagram)/gi), t.prop("selectedChannel")),
                    noAds: t.compose(t.not, v["N"], t.prop("ads")),
                    isCopying: t.compose(Boolean, t.prop("copyOriginId")),
                    formFillAds: function () {
                        return "FORM" === this.baseType ? t.compose(t.assocPath(["creative", "url"], Object(_["b"])(this.campaignId)), g["i"]) : g["i"]
                    },
                    renderFbPagesModal: function () {
                        var e = this, n = this.$createElement;
                        if (t.path(["modalMeta", "fbPages"], this)) {
                            var a = {
                                attrs: {active: !0},
                                on: {
                                    "update:active": this.updateModalMeta("fbPages").bind(this), confirm: function () {
                                        return e.profile$.next()
                                    }
                                }
                            };
                            return n(f["a"], i()([{}, a]))
                        }
                    },
                    renderAdsSelectionModal: function () {
                        var e = this.$createElement, n = "adsSelection";
                        if (t.path(["modalMeta", n], this)) {
                            var a = {
                                attrs: {
                                    active: !0,
                                    type: this.baseType,
                                    appstoreAvailable: Object(v["N"])(this.baseAppstore) && !this.channelAds.find(t.propEq("channel", "apple")),
                                    channel: "all" === this.selectedChannel ? void 0 : this.selectedChannel
                                }, on: {"update:active": this.updateModalMeta(n).bind(this), confirm: this.addNewAds}
                            };
                            return e(b["a"], i()([{}, a]))
                        }
                    },
                    renderAppstoreModal: function () {
                        var e = this, n = this.$createElement, a = "appstore";
                        if (t.path(["modalMeta", a], this)) {
                            var s = {
                                attrs: {
                                    active: !0,
                                    id: t.path(["baseAppstore", "id"], this),
                                    campaignId: this.campaignId
                                }, on: {
                                    "update:active": this.updateModalMeta(a).bind(this), confirm: function (t) {
                                        e.setAppleAppId(t), e.updateMetaHash()
                                    }
                                }
                            };
                            return n(m["a"], i()([{}, s]))
                        }
                    },
                    renderChannels: function () {
                        var t = this, e = this.$createElement, n = this.channels, a = void 0 === n ? [] : n,
                            i = this.selectedChannel, s = void 0 === i ? "" : i;
                        return a.map(function (n) {
                            var a = s === n;
                            return e(p["f"], {
                                attrs: {selected: a}, nativeOn: {
                                    click: function () {
                                        return t.setSelectedChannel(n)
                                    }
                                }
                            }, [e(p["e"], {attrs: {channel: n, selected: a}})])
                        })
                    },
                    renderTopBtn: t.path(["$slots", "navigation", 0]),
                    renderAds: function () {
                        var t = this.$createElement, e = this.displayAds;
                        if (Object(v["N"])(e)) {
                            var n = ["masonry"];
                            return v["C"] && n.push("IE"), t(p["b"], {class: n}, [this.allVisible && this.renderNewBox, e.map(this.renderPreview)])
                        }
                        return t(p["r"], [this.renderNewBox, t(p["q"], [this.$t("PROPOSAL.no_ads_created")])])
                    },
                    renderNewBox: function () {
                        var t = this, e = this.$createElement, n = {
                            class: "m-item new-box", nativeOn: {
                                click: function () {
                                    return t.updateModalMeta("adsSelection", !0)
                                }
                            }
                        };
                        return e(p["E"], i()([{}, n]), [e(p["F"], {class: "m-content"}, [e(p["G"], {
                            class: "normal-icon",
                            attrs: {src: "/img/icons/new_ad_icon.png"}
                        }), e(p["G"], {
                            class: "active-icon",
                            attrs: {src: "/img/icons/new_ad_icon_active.png"}
                        }), e(p["H"], [this.$t("PROPOSAL.add_new_ad")])])])
                    },
                    allVisible: t.compose(t.not, v["N"], t.prop("visibleAdIds"))
                }),
                methods: Object(o["a"])({}, Object(h["mapMutations"])("proposal", ["setChannelAds", "removeAd", "updateChannelAd", "updateMetaHash"]), Object(h["mapMutations"])("campaign", ["setAppleAppId"]), Object(h["mapActions"])("proposal", ["getFacebookProfile"]), Object(h["mapActions"])("user", ["fetchFacebookProfile"]), {
                    updateModalMeta: t.curry(function (t, e) {
                        this.modalMeta[t] = e
                    }), observeProfile$: function () {
                        var e = this,
                            n = this.profile$.pipe(Object(j["a"])(t.compose(y["a"], t.defaultTo(0))), Object(k["a"])(function () {
                                return e.campaignId
                            }), Object($["a"])(function (n) {
                                return Object(C["a"])(function () {
                                    return e.getFacebookProfile(n)
                                }).pipe(Object(S["a"])(Object(v["p"])()), Object(L["a"])(t.always(Object(P["b"])())))
                            }), Object(M["a"])(this.getBeforeDestroy$()));
                        this.$subscribeTo(n, function () {
                            e.updateMetaHash(), e.setFbCoverVisible(!1)
                        }, function (t) {
                            return console.log(t)
                        })
                    }, setSelectedChannel: function (t) {
                        this.selectedChannel = t || "all"
                    }, showFbModal: function () {
                        var e = this, n = "fbPages";
                        t.path(["modalMeta", n], this) || Object(A["a"])(Object(v["u"])()).pipe(Object(R["a"])(function () {
                            return e.updateModalMeta(n, !0)
                        }), Object(I["a"])(2e3)).subscribe(this.fetchFacebookProfile)
                    }, renderDropdown: function (e) {
                        var n = this, a = this.$createElement;
                        if (Object(g["m"])(e)) return null;
                        var i = t.lensPath(["meta", "isEditing"]), s = t.view(i, e);
                        return a("b-dropdown", {
                            slot: "last-btn",
                            attrs: {position: "is-bottom-left"}
                        }, [a(p["p"], {
                            slot: "trigger",
                            class: "fal fa-ellipsis-v"
                        }), a("b-dropdown-item", {
                            nativeOn: {
                                click: function (a) {
                                    a.stopPropagation(), n._updateChannelAd(t.over(i, t.not, e))
                                }
                            }
                        }, [a(p["n"], [a(p["m"], {attrs: {src: "/img/icons/ad_edit_icon.png"}})]), a(p["o"], [s ? this.$t("ONBOARDING.ads_preview") : this.$t("ONBOARDING.ads_edit")])]), a("b-dropdown-item", {
                            on: {
                                click: function () {
                                    return n.duplicateAd(e)
                                }
                            }
                        }, [a(p["n"], [a(p["m"], {attrs: {src: "/img/icons/ad_duplicate_icon.png"}})]), a(p["o"], [this.$t("COMMON.duplicate")])])])
                    }, renderPreview: function (e) {
                        var n = this, a = this.$createElement, s = e.creativeId, r = this.copyOriginId === s,
                            c = this.baseType, l = r && this.copyHighlighted,
                            u = Object(g["o"])(e) && this.fbCoverVisible, d = [];
                        this.copyHighlighted && r && d.push("highlighted");
                        var f = {
                            key: s,
                            class: d,
                            attrs: Object(o["a"])({
                                removable: !0,
                                copyable: !1,
                                urlVisible: !0,
                                strictValidation: !0,
                                autofocus: !1,
                                ad: e,
                                type: c,
                                dirty: e.meta.isEditing && z(e, {strictValidation: !0, type: c})
                            }, t.pick(["facebookProfile", "campaignId"], this), {appleDisabled: Object(g["m"])(e) && !this.appleAppIdConnected}),
                            on: {
                                resize: function () {
                                    return n.redraw$.next()
                                }, input: this._updateChannelAd, remove: function () {
                                    return n.checkRemove(s)
                                }, "click:profile": this.showFbModal, "click:appstore": function () {
                                    return n.updateModalMeta("appstore", !0)
                                }
                            },
                            nativeOn: {
                                click: function (a) {
                                    Object(v["bb"])("i", a) || K(a) || n._updateChannelAd(t.assocPath(["meta", "isEditing"], !0, e))
                                }
                            }
                        };
                        return a(p["D"], {class: "m-item"}, [a(p["a"], i()([{}, f, {class: "m-content"}]), [a("transition", {
                            attrs: {
                                "enter-active-class": "fadeIn",
                                "leave-active-class": "fadeOut"
                            }, slot: "cover"
                        }, [l && a(p["i"], [a(p["h"], [a(p["j"], {class: "fal fa-clone"}), a(p["l"], [this.$t("PROPOSAL.content_copied")])])]), u && a(p["t"], {
                            nativeOn: {
                                click: function (t) {
                                    return t.stopPropagation()
                                }
                            }
                        }, [a(p["C"], [a(p["s"], [a(p["x"], {class: "word-break"}, [this.$t("PROPOSAL.select_fb_page_desc")]), a(p["v"], {
                            nativeOn: {
                                click: function () {
                                    n.showFbModal()
                                }
                            }
                        }), a(p["u"], {
                            nativeOn: {
                                click: function () {
                                    return n.setFbCoverVisible(!1)
                                }
                            }
                        }, [this.$t("COMMON.do_later")])])])])]), this.getOnOffSlot(e), this.getHeaderSlot(e), this.renderDropdown(e)])])
                    }, getHeaderSlot: function () {
                        var e, n = this, a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                            s = this.$createElement, o = a.creativeId;
                        if (Object(g["o"])(a)) {
                            var r = function (e) {
                                e.stopPropagation(), n.cancelClick$.next(), n.finishCopy(), n.copyOriginId = o, n.adsBackup = t.clone(n.channelAds), n.observeClick$(), n.$nextTick(function () {
                                    return n.copyHighlighted = !0
                                })
                            }, c = {
                                nativeOn: {click: r},
                                directives: [{name: "tooltip", value: this.$t("PROPOSAL.preview_copy_ad")}]
                            };
                            e = this.isCopying ? this.copyBackup[o] ? s(p["B"], {slot: "header-btn"}, [s(p["A"], {
                                nativeOn: {
                                    click: function () {
                                        return n.cancelCopy(a)
                                    }
                                }
                            }, [s(p["c"], {class: "fal fa-undo"}), this.$t("PROPOSAL.undo_paste")]), s(p["k"], i()([{}, c]))]) : o === this.copyOriginId ? s(p["B"], {slot: "header-btn"}, [s(p["k"], {attrs: {active: !0}})]) : s(p["B"], {slot: "header-btn"}, [s(p["A"], {
                                attrs: {type: "paste"},
                                nativeOn: {
                                    click: function (t) {
                                        t.stopPropagation(), n.handleCopy(a)
                                    }
                                }
                            }, [this.$t("PROPOSAL.paste")]), s(p["k"], i()([{}, c]))]) : s(p["B"], {
                                slot: "header-btn",
                                class: "default"
                            }, [s(p["k"], i()([{}, c]))])
                        }
                        return e
                    }, observeRedraw$: function () {
                        v["C"] || this.$subscribeTo(Object(x["a"])(this.redraw$, Object(y["a"])(500, 1e3).pipe(Object(T["a"])(3))).pipe(Object(E["a"])(220)), q)
                    }, addNewAds: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [], n = [],
                            a = ["channel", "creativeType"];
                        if (Object(v["N"])(this.generatedAds)) {
                            e = Object(g["e"])(e, t.project(a, this.generatedAds));
                            var i = t.find(t.__, this.generatedAds);
                            n = t.compose(v["k"], t.map(t.compose(i, t.whereEq, t.pick(a))))(e)
                        }
                        var o = t.project(a),
                            r = Object(g["b"])("", t.difference(o(e), o(n))).map(t.over(t.lensProp("creative"), t.mergeLeft(G(this))));
                        n = [].concat(Object(s["a"])(n), Object(s["a"])(r)).map(this.formFillAds), this.setChannelAds([].concat(Object(s["a"])(n), Object(s["a"])(this.channelAds)))
                    }, watchChannelAdsFirst$: function () {
                        var e = this.$watchAsObservable("channelAds", {
                                deep: !0,
                                immediate: !0
                            }).pipe(Object(B["a"])("newValue")),
                            n = this.$watchAsObservable("loaded", {immediate: !0}).pipe(Object(B["a"])("newValue"), Object(N["a"])(t.identity), Object(F["a"])(100), Object($["a"])(function () {
                                return e
                            }), Object(V["a"])(), Object(k["a"])(t.complement(v["N"])));
                        this.$subscribeTo(n, this.updateModalMeta("adsSelection").bind(this))
                    }, checkRemove: function (e) {
                        var n = this, a = t.find(t.propEq("creativeId", e), this.channelAds) || {}, i = a.channel;
                        this.confirmBefore({
                            text: this.$t("COMMON.confirm_remove_ad", {channel: this.$t("COMMON.channel_".concat(i))}),
                            cancelText: this.$t("COMMON.cancel"),
                            confirmText: this.$t("COMMON.delete"),
                            confirmCb: function () {
                                e === n.copyOriginId && n.finishCopy(), n.removeAd(e)
                            }
                        })
                    }, _updateChannelAd: function (e) {
                        var n = e.creativeId, a = t.find(t.propEq("creativeId", n), this.channelAds) || {};
                        e.enabled && this.emphasizeTurnOn && (this.emphasizeTurnOn = !1);
                        var i = t.omit(["meta", "enabled"]);
                        if (n !== this.copyOriginId || t.equals(i(a), i(e)) || this.finishCopy(), !Object(g["o"])(e) || this.fbCoverVisible || !e.enabled || a.enabled || Object(v["N"])(this.fbPageInfo) || (this.setFbCoverVisible(!0), e.enabled && (e = Object(o["a"])({}, e, {
                                enabled: !1,
                                meta: Object(o["a"])({}, e.meta, {isEditing: !1})
                            }))), e.enabled && !a.enabled && !e.executable.value) return this.showAdMessage(e);
                        this.updateChannelAd(e)
                    }, showAdMessage: function (t) {
                        var e = this;
                        console.log(t, "in showAdMessage");
                        var n = g["k"].call(this, {ad: t, reason: t.executable.reason});
                        n && this.confirmBefore({
                            text: n,
                            confirmText: this.$t("COMMON.edit"),
                            cancelText: this.$t("COMMON.close"),
                            confirmCb: function () {
                                return e.$nextTick(function () {
                                    e._updateChannelAd(Object(o["a"])({}, t, {
                                        enabled: !1,
                                        meta: Object(o["a"])({}, t.meta, {isEditing: !0})
                                    }))
                                })
                            }
                        })
                    }, restoreBeforeCopy: function () {
                        Object(v["N"])(this.copyBackup) && this.setChannelAds(this.adsBackup), this.finishCopy()
                    }, finishCopy: function () {
                        this.copyOriginId = void 0, this.copyBackup = {}, this.adsBackup = []
                    }, handleCopy: function (e) {
                        var n = t.find(t.propEq("creativeId", this.copyOriginId), this.channelAds);
                        if (Object(v["a"])(n, e)) {
                            var a = e.creativeId || Object(v["o"])();
                            this._updateChannelAd(Object(o["a"])({}, Object(g["w"])(n, e), {creativeId: a})), this.copyBackup[a] = e
                        }
                    }, cancelCopy: function (e) {
                        var n = e.creativeId, a = this.copyBackup[n];
                        Object(v["N"])(a) && (this._updateChannelAd(a), this.copyBackup = t.dissoc(n, this.copyBackup))
                    }, duplicateAd: function (e) {
                        this.setChannelAds([t.assoc("creativeId", Object(v["o"])(), e)].concat(Object(s["a"])(this.channelAds)))
                    }, observeClick$: function () {
                        var t = this,
                            e = Object(w["a"])(document, "click").pipe(Object(D["a"])(1), Object(V["a"])(), Object(M["a"])(this.cancelClick$));
                        this.$subscribeTo(e, function () {
                            return t.copyHighlighted = !1
                        })
                    }, closeAllModal: function () {
                        this.modalMeta = t.compose(t.converge(t.zipObj, [t.identity, t.map(t.F)]), t.keys, t.prop("modalMeta"))(this)
                    }, getOnOffSlot: function (e) {
                        var n = this, a = this.$createElement, s = e.enabled,
                            o = s ? this.$t("PROPOSAL.ad_status_enabled") : this.$t("PROPOSAL.ad_status_disabled"),
                            r = {
                                attrs: {value: s, emphasize: this.emphasizeTurnOn}, on: {
                                    "update:value": function (a) {
                                        return n._updateChannelAd(t.assoc("enabled", a, e))
                                    }
                                }
                            };
                        return a(p["K"], {slot: "onOff"}, [a(p["J"], i()([{}, r])), a(p["I"], {
                            class: "adriel-ellipsis",
                            attrs: {isOn: s}
                        }, [o])])
                    }, setFbCoverVisible: function (t) {
                        t && (this.copyHighlighted = !1), this.fbCoverVisible = t
                    }
                }),
                beforeRouteLeave: function (t, e, n) {
                    this.closeAllModal(), this.finishCopy(), n()
                }
            }
        }.call(this, n("b17e"))
    }, aa36: function (t, e, n) {
        "use strict";
        var a = n("bd67"), i = n.n(a);
        i.a
    }, ac1d: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "newAndReturningWrapper"}, [n("card-view", {
                    attrs: {
                        label: t.$t("PROPOSAL.ap_card_title_new_returning"),
                        className: t.className,
                        editable: t.editable,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        confirmLabel: "Ok",
                        settingImgSrc: "/img//box_edit_icon.png"
                    }, on: {
                        "edit:start": t.tryEdit, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [n("gender-gauge-view", {
                    attrs: {
                        isEditing: t.isEditing,
                        value: t.values.newPercentage,
                        colors: ["#7ebbd4", "#3f937e"]
                    }, on: {
                        "update:value": function (e) {
                            return t.$emit("update:value", e)
                        }, complete: function (e) {
                            return t.$emit("edit:end")
                        }
                    }
                }, [n("div", {
                    staticClass: "newAndReturn-slot",
                    attrs: {slot: "left"},
                    slot: "left"
                }, [n("span", {staticClass: "newAndReturning-title"}, [n("span", [t._v(t._s(t.$t("PROPOSAL.ap_card_content_new")))]), n("info-tooltip", {attrs: {label: t.newTooltip}})], 1), n("img", {attrs: {src: "/img/icons/new_customer.png"}})]), n("div", {
                    staticClass: "newAndReturn-slot",
                    attrs: {slot: "right"},
                    slot: "right"
                }, [n("span", {staticClass: "newAndReturning-title"}, [n("span", [t._v(t._s(t.$t("PROPOSAL.ap_card_content_returning")))]), n("info-tooltip", {attrs: {label: t.returningTooltip}})], 1), n("img", {attrs: {src: "/img/icons/returning_customer.png"}})])])], 1)], 1)
            }, i = [], s = n("3a8d"), o = s["a"], r = (n("b21a"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, null, null);
        e["a"] = c.exports
    }, ae2b: function (t, e, n) {
        "use strict";
        var a = n("1835"), i = n.n(a);
        i.a
    }, b21a: function (t, e, n) {
        "use strict";
        var a = n("6c8e"), i = n.n(a);
        i.a
    }, b271: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("carousel", {
                    staticClass: "preview-carousel",
                    class: {ko: t.isLang("ko")},
                    attrs: {
                        autoplay: !0,
                        autoplayTimeout: 3e3,
                        autoplayHoverPause: !0,
                        perPage: 1,
                        paginationEnabled: !0,
                        paginationActiveColor: t.paginationActiveColor,
                        paginationColor: "#dddddd",
                        paginationPadding: 5
                    }
                }, t._l(t.slides, function (e, a) {
                    return n("slide", {key: a}, [n("img", {attrs: {src: e.mainImage}}), n("div", {staticClass: "slide-bottom"}, [n("span", {staticClass: "slide-title"}, [t._v("\n                " + t._s(e.title) + "\n            ")]), e.contents ? n("div", {staticClass: "slide-contents"}, t._l(e.contents || [], function (e, a) {
                        return n("div", {key: a}, [n("span", {staticClass: "slide-contents--head"}, [t._v(" 쨌 ")]), n("span", {staticClass: "slide-contents--each"}, [t._v(t._s(e))])])
                    }), 0) : t._e()])])
                }), 1)
            }, i = [], s = n("50e7"), o = s["a"], r = (n("f560"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, null, null);
        e["a"] = c.exports
    }, b79b: function (t, e, n) {
        "use strict";
        var a = n("7503"), i = n.n(a);
        i.a
    }, b7ca: function (t, e, n) {
        "use strict";
        var a = n("76fb"), i = n.n(a);
        i.a
    }, b84f: function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("f499"), i = n.n(a), s = n("db0c"), o = n.n(s), r = n("bd86"),
                c = (n("b6d0"), n("7514"), n("7f7f"), n("c5f6"), n("a481"), n("cebc")), l = n("2f62"), u = n("dde5"),
                d = n("fa7d"), p = n("19e8"), f = n("d4a3"), b = n("9599"), m = n("1111"), h = n("fe19"), v = n("4179"),
                g = n("1e62"), _ = n("869a"), O = n("0c2f"), y = n("7b17"), C = n("2029"), P = n("e501"), A = n("4379"),
                x = n("51c6"), w = n("c1df"), j = n.n(w), k = n("2bd2"), $ = n("a748"), S = n("9586"), L = n("1b92"),
                M = n("1585"), R = n("5670"), I = n("ebb6"), T = n("d337"), E = n("3fab"), B = n("a744"), N = n("1a2d"),
                F = n("69dd"), V = n("d792"), D = n("66aa"), z = n("f59d"), G = n("9f2d");
            e["a"] = {
                name: "Plan",
                created: function () {
                    this.orderChannelsByBudget(), this.checkLaunched(), this.$nextTick(function () {
                        window.scrollTo(0, 0)
                    })
                },
                components: {
                    "card-view": p["a"],
                    "estimated-view": b["a"],
                    "optimization-toggle": m["a"],
                    PaymentList: h["a"],
                    DefaultPaymentMethod: v["a"],
                    BillingPlans: g["a"],
                    PaypalButton: _["a"],
                    BillingFaq: O["a"],
                    CouponsModal: C["a"],
                    PreviewModal: P["a"],
                    BeforeKorean: y["a"],
                    AdrielRadio: f["a"],
                    BusinessCert: A["a"],
                    ModalBox: x["a"]
                },
                data: function () {
                    return {
                        isBillingModalActive: !1,
                        isCouponsModalActive: !1,
                        isPreviewModalActive: !1,
                        isBeforeKoreanModalActive: !1,
                        isBusinessModalActive: !1,
                        couponAssigned: 0,
                        couponAmount: 0,
                        budgetOrder: null,
                        nbLaunchedCampaigns: 0,
                        budgetPopovers: {},
                        budgetInput$: new k["a"]
                    }
                },
                methods: Object(c["a"])({}, Object(l["mapMutations"])("proposal", ["setContinuously", "setStartDate", "setEndDate", "resetSchedule", "setDailyBudget", "setChannelBudget", "resetBudget", "setAsap"]), Object(l["mapMutations"])("campaign", ["setAutoOptimization", "setEstimations", "setBillingPlan"]), Object(l["mapActions"])("campaign", ["requestEstimations", "getAvailableCoupons"]), {
                    dateFormat: function (t) {
                        return j()(t).format(this.$t("PROPOSAL.schedule_date_format"))
                    }, getBudgetIcon: function (t) {
                        return "/img/icons/".concat(t, "_top_logo.png")
                    }, budgetChanged: function (t, e) {
                        var n = e.target, a = n.value;
                        if (a = a.replace(/[^0-9.]/g, ""), a = Number(a), 0 !== a && a < this.getMin(t)) return n.value = t.value, this.toast(this.$t("PROPOSAL.below_minimum_budget"));
                        this.setChannelBudget({name: t.name, value: a}), n.value = a
                    }, isResetable: function (e) {
                        switch (e) {
                            case"schedule":
                                return !t.equals(this.schedule, this.backupSchedule);
                            case"budget":
                                return !t.equals(this.budget, this.backupBudget)
                        }
                    }, getCommaValue: d["l"], getTooltip: function (t) {
                        return "".concat(this.$t("PROPOSAL.plan_budget_minimum"), " ").concat(this.formatCurrency(this.getMin(t)))
                    }, updatePopover: function (t) {
                        var e = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                        this.$set(this.budgetPopovers, t, e)
                    }, getReasonAds: function (t) {
                        return "google" === t || this.intelFacebookPage ? "not_enabled" : "apple" === t ? this.appleAppIdConnected ? "not_enabled" : "apple_not_connected" : "facebook_page_missing"
                    }, goToAds: function (e) {
                        var n = "all" === e || t.equals(this.getReasonAds(e), "not_enabled") ? 1 : 0;
                        this.$router.push({name: "Proposal/Ads", query: {channel: e, emphasize: n}})
                    }, getMin: function (t) {
                        var e = t.min;
                        return e
                    }, getAvailableCouponAmount: function () {
                        var t = this;
                        u["d"].availableCouponAmount(this.campaignId).then(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            t.couponAssigned = e.amountAssigned, t.couponAmount = e.totalAmount
                        }).catch(function (t) {
                            console.log(t)
                        })
                    }, addCoupon: function (t) {
                        var e = this;
                        u["e"].consume(t).then(function () {
                            e.getAvailableCoupons().then(function () {
                                var n = e.coupons.find(function (e) {
                                    return e.code === t
                                });
                                e.bindCoupons([n.id])
                            })
                        }).catch(function () {
                            e.toast(e.$t("PROPOSAL.failed_to_register_coupon"))
                        })
                    }, bindCoupons: function (t) {
                        var e = this;
                        u["d"].bindCoupons(this.campaignId, t).then(function () {
                            e.getAvailableCouponAmount(), e.getAvailableCoupons()
                        })
                    }, orderChannelsByBudget: function () {
                        var e = this;
                        this.$watchAsObservable("budgetChannels", {
                            deep: !0,
                            immediate: !0
                        }).pipe(Object(M["a"])("newValue"), Object(R["a"])(d["N"]), Object(I["a"])(t.sortWith([t.descend(t.prop("value")), t.descend(t.prop("min"))])), Object(I["a"])(t.map(t.prop("name"))), Object(T["a"])(1200), Object(E["a"])()).subscribe(function (t) {
                            return e.budgetOrder = t
                        }, function (t) {
                            console.log(t)
                        })
                    }, checkLaunched: function () {
                        var t = this;
                        u["d"].nbLaunchedCampaigns().then(function (e) {
                            return t.nbLaunchedCampaigns = e
                        })
                    }, channelAdExists: function (e) {
                        var n = e.name;
                        return t.pipe(t.prop("enabledChannelAds"), t.find(t.propEq("channel", n)))(this)
                    }, handleCert: function () {
                    }
                }),
                computed: Object(c["a"])({}, Object(l["mapGetters"])("proposal", ["loaded", "continuously", "startDate", "endDate", "dailyBudget", "totalBudget", "weeklyBudget", "monthlyBudget", "budgetChannels", "schedule", "backupSchedule", "budget", "backupBudget", "canLaunch", "asap", "enabledChannelAds"]), Object(l["mapGetters"])("campaign", ["autoOptimization", "estimations", "campaignId", "billingPlan", "serviceFee", "isUpdate", "intelFacebookPage", "appleAppIdConnected", "campaign"]), Object(l["mapState"])("campaign", ["coupons"]), Object(l["mapGetters"])("proposal", ["proposal", "appleAdAvailabe", "channelAds"]), Object(l["mapGetters"])("user", ["userLang"]), {
                    _budgetChannels: function () {
                        var e = this;
                        return t.compose(function (n) {
                            var a = t.filter(t.propEq("channel", "apple"), e.channelAds);
                            return e.appleAdAvailabe && a.length ? n : t.reject(t.whereEq({name: "apple", value: 0}), n)
                        }, t.uniq, t.prop("budgetChannels"))(this)
                    },
                    _continuously: {
                        get: t.prop("continuously"), set: function (t) {
                            this.setContinuously(t)
                        }
                    },
                    _startDate: {
                        get: t.prop("startDate"), set: function (t) {
                            this.isUpdate || (this.setStartDate(t), j()(this._endDate).isBefore(this.minEndDate) && (this._endDate = this.minEndDate))
                        }
                    },
                    _endDate: {
                        get: t.prop("endDate"), set: function (t) {
                            this.setEndDate(t)
                        }
                    },
                    hasNoBudget: t.pipe(t.prop("_budgetChannels"), t.all(t.propSatisfies(t.lte(t.__, 0), "value"))),
                    hasNoActiveAds: function () {
                        return 0 === this.enabledChannelAds.length
                    },
                    period: function () {
                        return this.continuously ? null : (this.endDate - this.startDate) / 6e4
                    },
                    minEndDate: function () {
                        return j()(this.startDate).add(2, "day").toDate()
                    },
                    _asap: {
                        get: t.prop("asap"), set: function (t) {
                            this.setAsap(t)
                        }
                    },
                    minStartDate: function () {
                        var t = j()();
                        return t.add(t.weekday() > 4 ? 2 : 1, "day").startOf("day").toDate()
                    },
                    priceServiceFee: t.converge(t.multiply, [t.prop("pricePlatformFee"), t.prop("serviceFee")]),
                    pricePlatformFee: function () {
                        return this._continuously ? this.dailyBudget : this.totalBudget
                    },
                    couponTextVisible: function () {
                        return this.isLangType("ko") && !!this.nbLaunchedCampaigns && 10 == this.couponAmount
                    },
                    vat: function () {
                        return !this.maybeKorean || this.globalUser.business_no ? 0 : t.multiply(.1, this.pricePlatformFee + this.priceServiceFee)
                    },
                    status: t.path(["campaign", "status"]),
                    startDisabled: function () {
                        return "running" === this.status
                    }
                }),
                subscriptions: function () {
                    var e = this, n = this.$watchAsObservable("schedule", {
                            deep: !0,
                            immedate: !0
                        }).pipe(Object(B["a"])({newValue: "init"})),
                        a = this.$watchAsObservable("budget", {deep: !0, immedate: !0});
                    Object($["a"])(n, a, this.budgetInput$).pipe(Object(N["a"])(500), Object(I["a"])(function () {
                        var n = e.$refs.budgetInput.reduce(function (t, e) {
                            return Object(c["a"])({}, t, Object(r["a"])({}, e.name, Number(e.$el.value)))
                        }, {}), a = o()(n), i = e.proposal.budget;
                        return t.all(t.is(Number), a) ? {
                            daily: t.sum(a), channels: i.channels.map(function (t) {
                                return Object(c["a"])({}, t, {value: n[t.name]})
                            })
                        } : i
                    }), Object(I["a"])(i.a), Object(F["a"])(t.equals), Object(I["a"])(JSON.parse), Object(V["a"])(function (t) {
                        return Object(S["a"])(function () {
                            return u["d"].getEstimations(e.campaignId, Object(c["a"])({}, e.proposal, {budget: t}))
                        }).pipe(Object(D["a"])(Object(d["p"])(1)), Object(z["a"])(function () {
                            return Object(L["b"])()
                        }))
                    }), Object(G["a"])(this.getBeforeDestroy$())).subscribe(function (t) {
                        console.log("estimation", t), e.setEstimations(t)
                    }, function (t) {
                        return console.log(t)
                    }), this.$watchAsObservable("budget.channels", {
                        deep: !0,
                        immedate: !0
                    }).pipe(Object(I["a"])(t.prop("newValue")), Object(I["a"])(t.compose(t.sum, t.pluck("value"))), Object(G["a"])(this.getBeforeDestroy$())).subscribe(this.setDailyBudget)
                },
                watch: {
                    campaignId: {
                        handler: function () {
                            this.campaignId && this.getAvailableCouponAmount(this.campaignId), this.campaignId && this.getAvailableCoupons()
                        }, immediate: !0
                    }, isCouponsModalActive: {
                        handler: function () {
                            !0 === this.isCouponsModalActive && this.getAvailableCoupons()
                        }, immediate: !0
                    }
                }
            }
        }).call(this, n("b17e"))
    }, b944: function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", [n("card-view", {
                attrs: {
                    label: "Company Size & Decision maker",
                    className: [t.className, "size-decision-card"],
                    editable: t.editable,
                    isEditing: t.isEditing,
                    dimmed: t.dimmed,
                    resetable: t.resetable,
                    resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                    confirmLabel: "I'd like to change this setting"
                }, on: {
                    "edit:start": function (e) {
                        return t.$emit("edit:start")
                    }, "edit:end": function (e) {
                        return t.$emit("edit:end")
                    }, "edit:reset": t.reset
                }
            }, [n("div", {staticClass: "company-size-wrapper"}, t._l(t.sizes, function (e, a) {
                return n("span", {
                    key: e.name,
                    staticClass: "size-decision-element",
                    class: {"selected-elem": e.value},
                    on: {
                        click: function (n) {
                            return t.$emit("update:value", {type: "sizes", index: a, value: !e.value})
                        }
                    }
                }, [e.value ? n("img", {attrs: {src: "/img/proposal/profile_check_box_press.png"}}) : n("img", {attrs: {src: "/img/proposal/profile_check_box.png"}}), t._v("\n            " + t._s(t._f("name")(e)) + "\n            ")])
            }), 0), n("div", {staticClass: "size-decision-divider"}), n("div", {staticClass: "company-size-wrapper"}, t._l(t.decisions, function (e, a) {
                return n("span", {
                    key: e.name,
                    staticClass: "size-decision-element",
                    class: {"selected-elem": e.value},
                    on: {
                        click: function (n) {
                            return t.$emit("update:value", {type: "decisionMakers", index: a, value: !e.value})
                        }
                    }
                }, [e.value ? n("img", {attrs: {src: "/img/proposal/profile_check_box_press.png"}}) : n("img", {attrs: {src: "/img/proposal/profile_check_box.png"}}), t._v("\n            " + t._s(t._f("name")(e)) + "\n            ")])
            }), 0)])], 1)
        }, i = [], s = (n("ac6a"), n("7f7f"), n("3de4")), o = {
            mixins: [s["a"]], created: function () {
            }, data: function () {
                return {}
            }, filters: {
                name: function (t) {
                    return t.name
                }
            }, computed: {
                sizes: function () {
                    return this.values.sizes
                }, decisions: function () {
                    return this.values.decisionMakers
                }
            }, methods: {}, props: {}, components: {}
        }, r = o, c = (n("5a17"), n("2877")), l = Object(c["a"])(r, a, i, !1, null, null, null);
        e["a"] = l.exports
    }, bd67: function (t, e, n) {
    }, be91: function (t, e, n) {
    }, bf61: function (t, e, n) {
        "use strict";
        var a = n("8398"), i = n.n(a);
        i.a
    }, c01f: function (t, e, n) {
    }, c575: function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return o
        });
        var a = n("e9b9"), i = n("4f50"), s = n("ff6d");

        function o(t, e) {
            return void 0 === t && (t = 0), void 0 === e && (e = i["a"]), (!Object(s["a"])(t) || t < 0) && (t = 0), e && "function" === typeof e.schedule || (e = i["a"]), new a["a"](function (n) {
                return n.add(e.schedule(r, t, {subscriber: n, counter: 0, period: t})), n
            })
        }

        function r(t) {
            var e = t.subscriber, n = t.counter, a = t.period;
            e.next(n), this.schedule({subscriber: e, counter: n + 1, period: a}, a)
        }
    }, c8fa: function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("f499"), i = n.n(a), s = n("795b"), o = n.n(s), r = n("a4bb"), c = n.n(r),
                l = (n("7f7f"), n("6762"), n("2fdb"), n("20d6"), n("28a5"), n("cebc")), u = n("2f62"), d = n("fa7d"),
                p = n("4e4d"), f = n("5ae3"), b = n("51c6"), m = n("b183"), h = n("d730"), v = n("dde5"), g = n("d3fb"),
                _ = n("54da"), O = n("808d"), y = n("c575"), C = n("9586"), P = n("1b92"), A = n("c4cc"), x = n("a6c5"),
                w = n("d337"), j = n("d792"), k = n("66aa"), $ = n("f59d"), S = n("d263"), L = n("1585"), M = n("ebb6"),
                R = n("69dd"), I = n("1a2d"), T = n("5670"), E = n("c1df"), B = n.n(E),
                N = t.converge(t.equals, [t.prop("lastUpdated"), t.path(["campaign", "proposal"])]),
                F = t.pipe(t.clone, t.over(t.lensPath(["proposal", "channelAds"]), t.compose(t.map(t.ifElse(t.pathEq(["executable", "value"], !1), t.set(t.lensProp("enabled"), !1), t.identity)), d["k"])));
            e["a"] = {
                name: "Proposal",
                destroyed: function () {
                    this.setCampaign(null), this.clearProposal(), "FORM" === this.baseType && this.clearData(["formCreate"]), window.onbeforeunload = null
                },
                data: function () {
                    return {
                        boModal: !1,
                        paymentModal: !1,
                        termsType: "",
                        isTermsActive: !1,
                        lastSaved: null,
                        launched: !1
                    }
                },
                components: {
                    "left-nav": p["a"], "top-nav": f["a"], ModalBox: b["a"], TermsAndPolicy: function () {
                        return n.e("chunk-b1925690").then(n.bind(null, "146f"))
                    }, "mobile-nav": m["a"], BoUpdateModal: h["a"]
                },
                computed: Object(l["a"])({}, Object(u["mapGetters"])("proposal", ["indicators", "schedule", "weeklyBudget", "monthlyBudget", "totalBudget", "canLaunch", "budget", "channelAds", "channelAdsBackup", "audienceProfile", "audienceProfileBackup", "isPlanResetable", "backupData", "enabledChannelAds", "hasPaymentMethod", "executableChannelAds", "isFirst", "runningAndChanged", "baseType", "loaded"]), Object(u["mapGetters"])("campaign", ["campaignId", "campaign", "lastUpdated", "billingPlan", "isUpdate", "title", "campaignImage", "couponsWithouCreditCard"]), Object(u["mapGetters"])("user", ["user", "isAdmin"]), {
                    amountToPayWithoutCredits: function () {
                        return t.path(["schedule", "continuously"], this) ? "M" === this.billingPlan ? this.monthlyBudget : "W" === this.billingPlan ? this.weeklyBudget : void 0 : this.totalBudget
                    },
                    amountToPay: function () {
                        return this.amountToPayWithoutCredits - this.user.credits
                    },
                    prevPath: function () {
                        return t.propOr("", "path", this.indicators[t.dec(this.stepIndex)])
                    },
                    nextPath: function () {
                        return t.propOr("", "path", this.indicators[t.inc(this.stepIndex)])
                    },
                    stepIndex: function () {
                        var e = t.compose(t.last, t.split("/"), t.path(["$route", "path"]))(this);
                        return t.findIndex(t.propEq("path", e))(this.indicators)
                    },
                    isLastStep: t.converge(t.equals, [t.prop("stepIndex"), t.compose(t.dec, t.path(["indicators", "length"]))]),
                    campaignStatus: t.path(["campaign", "status"]),
                    compCampaignId: t.path(["$route", "params", "id"]),
                    routeName: t.path(["$route", "name"]),
                    isResetButtonActive: function () {
                        if (!this.isUpdate || !this.routeName) return !1;
                        switch (t.toLower(this.routeName)) {
                            case"proposal/ads":
                                return !t.equals(this.channelAds, this.channelAdsBackup);
                            case"proposal/audienceprofile":
                                return !t.equals(this.audienceProfile, this.audienceProfileBackup);
                            case"proposal/plan":
                                return this.isPlanResetable
                        }
                    },
                    _campaignImage: t.compose(d["U"], t.prop("campaignImage")),
                    stepClass: function () {
                        var t = this.indicators[this.stepIndex] || {}, e = t.path;
                        return "proposal_step_".concat(e && e.toLowerCase())
                    },
                    resetable: function () {
                        return this.isUpdate && this.isResetButtonActive
                    },
                    publishable: function () {
                        var e = t.path(["campaign", "proposal"], this);
                        if (!e) return !1;
                        if (!this.isUpdate || !this.canLaunch.value || this.launched) return !1;
                        var n = t.complement(t.equals), a = this.lastSaved, i = t.dissoc("meta");
                        return !a && n(i(this.backupData), i(e)) || a && n(i(a), i(e))
                    },
                    noAds: t.ifElse(t.and(t.propEq("baseType", "FORM"), t.pathEq(["channelAds", "length"], 0)), t.always(!1), t.pathEq(["enabledChannelAds", "length"], 0))
                }),
                watch: {
                    compCampaignId: {
                        handler: function (t, e) {
                            t && e && t != e ? window.location.reload() : void 0 !== e && Object(d["N"])(this.campaign) || (this.setCampaign(null), this.getCampaign(t))
                        }, immediate: !0
                    }, "$route.name": {
                        handler: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                            if (this.loaded && t && (this.updateMetaHash(), this.noAds && ["Proposal/Ads", "Proposal/AudienceProfile", "Proposal/Plan"].includes(t))) {
                                var e = "Proposal/Ads" === t ? this.$t("PROPOSAL.no_ads_warn_ad") : "FORM" === this.baseType ? this.$t("PROPOSAL.no_ads_warn_form") : this.$t("PROPOSAL.no_ads_warn");
                                this.toast(e, void 0, void 0, 1e4)
                            }
                        }
                    }
                },
                methods: Object(l["a"])({}, Object(u["mapMutations"])("campaign", ["setCampaign"]), Object(u["mapMutations"])("proposal", ["setDailyBudget", "clearProposal", "resetAds", "resetAudienceProfile", "updateMetaHash"]), Object(u["mapActions"])("campaign", ["getCampaign", "updateProposal"]), Object(u["mapActions"])("proposal", ["resetPlan"]), Object(u["mapActions"])("view", ["setLoadingAdriel"]), Object(u["mapActions"])("user", ["fetchPaymentMethods"]), Object(u["mapActions"])("form", ["clearData"]), {
                    checkCanLaunch: function () {
                        if (!this.hasPaymentMethod && this.couponsWithouCreditCard.length > 0) for (var t = this.couponsWithouCreditCard[0], e = t.amount_per_channels, n = this.budget.channels.reduce(function (t, e) {
                            var n = e.name, a = e.value;
                            return "instagram" === n && (n = "facebook"), t[n] || (t[n] = 0), t[n] += a, t
                        }, {}), a = 0, i = c()(n); a < i.length; a++) {
                            var s = i[a];
                            if (e[s] || (e[s] = 0), 2 * n[s] > e[s]) return this.messageBefore({html: '[�섏씠�ㅻ턿 �뚰겕�� 荑좏룿]<br><br>\n                                    <span style="text-align: center">\n                                        �섏씠�ㅻ턿, �몄뒪��洹몃옩 1�� �덉궛��<br> $25 誘몃쭔�쇰줈 吏��뺥빐二쇱꽭��\n                                    </span>'}), !1
                        }
                        return !!this.runningAndChanged
                    },
                    checkEndDate: function () {
                        var e = this, n = t.pathOr({}, ["campaign", "proposal", "schedule"], this),
                            a = B()(n.endDate).subtract(1, "days").isBefore(new Date);
                        return new o.a(function (t, i) {
                            !n.continuously && a ? e.confirmBefore({
                                text: e.$t("PROPOSAL.need_extend_schedule"),
                                confirmText: e.$t("PROPOSAL.run_continuously"),
                                cancelText: e.$t("PROPOSAL.go_to_schedule"),
                                confirmCb: function () {
                                    n.continuously = !0, t(!0)
                                },
                                cancelCb: function () {
                                    e.$router.push({name: "Proposal/Plan", params: {id: e.campaign.id}}), t(!1)
                                }
                            }) : t(!0)
                        })
                    },
                    _launchCampaign: function () {
                        var t = this, e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        this.setLoadingAdriel({
                            active: !0,
                            message: " "
                        }), Object(g["a"])(this.updateCampaign(this.$route, !0)).pipe(Object(A["a"])(function () {
                            return t.launched = !0
                        }), Object(x["a"])(function (n) {
                            return Object(_["a"])(Object(O["a"])(3e3), v["d"].execute(t.campaignId, e))
                        }), Object(w["a"])(1e4)).subscribe(function (e) {
                            t.setLoadingAdriel(!1), t.$router.push({name: "CampaignLaunched"})
                        }, function (e) {
                            console.log(e), t.setLoadingAdriel(!1), alert("problem occured try again")
                        })
                    },
                    launchCampaign: function () {
                        var t = this;
                        if (this.checkCanLaunch()) {
                            var e = function () {
                                t.isAdmin && t.isUpdate ? t.boModal = !0 : t._launchCampaign()
                            };
                            this.checkEndDate().then(function (n) {
                                n && t.requestAdriel(e)
                            })
                        }
                    },
                    launchAndLeave: function (t) {
                        var e = this;
                        if (this.checkCanLaunch()) {
                            var n = function () {
                                Object(g["a"])(e.updateCampaign(e.$route, !0)).pipe(Object(x["a"])(function () {
                                    return v["d"].execute(e.campaignId)
                                }), Object(w["a"])(1e4)).subscribe(function () {
                                    return t()
                                }, function (e) {
                                    alert("problem occured try again"), console.log(e), t()
                                })
                            };
                            this.checkEndDate().then(function (t) {
                                t && e.requestAdriel(n)
                            })
                        }
                    },
                    debounceExecute: function () {
                        var e = this;
                        if (this.isUpdate && this.checkCanLaunch()) {
                            var n = function () {
                                Object(g["a"])(e.updateCampaign(e.$route, !0)).pipe(Object(A["a"])(function () {
                                    e.lastSaved = t.clone(e.campaign.proposal), e.toast(e.$t("PROPOSAL.published_toast"), void 0, "adriel-info")
                                }), Object(j["a"])(function () {
                                    return v["d"].debounceExecute(e.campaignId)
                                }), Object(w["a"])(1e4)).subscribe(t.identity, function (t) {
                                    console.log(t)
                                })
                            };
                            this.checkEndDate().then(function (t) {
                                t && (e.requestAdriel(n), e.requestOn())
                            })
                        }
                    },
                    requestOn: t.once(function () {
                        var t = this;
                        if (this.isUpdate) {
                            var e = Object(y["a"])(12e5).pipe(Object(j["a"])(function () {
                                return Object(C["a"])(function () {
                                    return v["d"].debounceExecute(t.campaignId)
                                }).pipe(Object(k["a"])(Object(d["p"])()), Object($["a"])(function () {
                                    return Object(P["b"])()
                                }))
                            }));
                            this.$subscribeTo(e, function () {
                                console.log("debounce execute request sent")
                            })
                        }
                    }),
                    requestAdriel: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : function () {
                        };
                        if (this.canLaunch.value) {
                            var e = this.isUpdate ? "UpdateCampaign" : "ExecuteCampaign";
                            this.$ga.event({
                                eventCategory: "App",
                                eventAction: "Campaign",
                                eventValue: 1,
                                eventLabel: e
                            }), fbq("trackCustom", e), this.$intercom.trackEvent(e), t()
                        }
                    },
                    updateCampaign: function (t, e) {
                        var n = t.name;
                        if (N(this)) return o.a.resolve();
                        var a = "";
                        return "Proposal/Form" !== n ? a += "step=".concat(this.getPageName(n)) : e = !0, e && (a += a ? "&save=1" : "save=1"), v["d"].update(this.campaignId, F(this.campaign), a)
                    },
                    getPageName: t.prop(t.__, {
                        "Proposal/AudienceProfile": "audiences",
                        "Proposal/AudienceJourney": "journey",
                        "Proposal/Ads": "creatives",
                        "Proposal/Plan": "budget",
                        "Proposal/Form": "form"
                    }),
                    watchCampaign$: function () {
                        var e = this,
                            n = this.$watchAsObservable("campaign", {deep: !0}).pipe(Object(S["a"])(1), Object(L["a"])("newValue"), Object(M["a"])(i.a), Object(R["a"])(t.equals), Object(M["a"])(function () {
                                return t.pick(["name"], e.$route)
                            }), Object(I["a"])(300), Object(j["a"])(function (t) {
                                return Object(C["a"])(function () {
                                    return e.updateCampaign(t)
                                }).pipe(Object(k["a"])(Object(d["p"])()), Object($["a"])(function (t) {
                                    return console.log(t), Object(P["b"])()
                                }))
                            }), Object(T["a"])(d["N"]));
                        this.$subscribeTo(n, this.updateProposal, function (t) {
                            return console.log(t)
                        })
                    },
                    showTerms: function (t) {
                        this.termsType = t, this.isTermsActive = !0
                    },
                    reset: function () {
                        if (this.isResetButtonActive) switch (t.toLower(this.routeName)) {
                            case"proposal/ads":
                                return this.resetAds();
                            case"proposal/audienceprofile":
                                return this.resetAudienceProfile();
                            case"proposal/plan":
                                return this.resetPlan()
                        }
                    },
                    handleBoSurvey: function (t) {
                        return this._launchCampaign({updateInfo: t})
                    }
                }),
                created: function () {
                    var t = this;
                    this.setCampaign(null), this.clearProposal(), this.$nextTick(function () {
                        t.watchCampaign$()
                    }), this.fetchPaymentMethods(), window.onbeforeunload = function (e) {
                        t.publishable && (e.preventDefault(), e.returnValue = "")
                    }
                },
                beforeRouteLeave: function (t, e, n) {
                    if (this.publishable) {
                        var a = this;
                        this.confirmBefore({
                            text: this.$t("PROPOSAL.confirm_unpublished"),
                            confirmText: this.$t("PROPOSAL.confirm_unpublished_btn_confirm"),
                            cancelText: this.$t("PROPOSAL.confirm_unpublished_btn_cancel"),
                            cancelCb: function () {
                                n()
                            },
                            confirmCb: function () {
                                a.launchAndLeave(n)
                            },
                            className: "proposal-confirm-publish"
                        })
                    } else n()
                }
            }
        }).call(this, n("b17e"))
    }, cad0: function (t, e, n) {
    }, cd39: function (t, e, n) {
    }, d263: function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return s
        });
        var a = n("9ab4"), i = n("1453");

        function s(t) {
            return function (e) {
                return e.lift(new o(t))
            }
        }

        var o = function () {
            function t(t) {
                this.total = t
            }

            return t.prototype.call = function (t, e) {
                return e.subscribe(new r(t, this.total))
            }, t
        }(), r = function (t) {
            function e(e, n) {
                var a = t.call(this, e) || this;
                return a.total = n, a.count = 0, a
            }

            return a["a"](e, t), e.prototype._next = function (t) {
                ++this.count > this.total && this.destination.next(t)
            }, e
        }(i["a"])
    }, d4a3: function (t, e, n) {
        "use strict";
        e["a"] = {
            render: function () {
                var t = this, e = arguments[0];
                return e("b-radio", {
                    attrs: {value: this.value, "native-value": this.nativeValue},
                    on: {
                        input: function (e) {
                            return t.$emit("input", e)
                        }
                    },
                    class: "adriel-radio"
                }, [this.label])
            }, props: {value: {type: Boolean, default: !1}, nativeValue: {type: Boolean, default: !0}, label: String}
        }
    }, d730: function (t, e, n) {
        "use strict";
        var a = n("768b"), i = n("2638"), s = n.n(i), o = n("aede"), r = n("2b0e"), c = n("9c56"), l = n("51c6");

        function u() {
            var t = Object(o["a"])([""]);
            return u = function () {
                return t
            }, t
        }

        function d() {
            var t = Object(o["a"])(["\n    ", "\n    margin: 10px 0;\n"]);
            return d = function () {
                return t
            }, t
        }

        function p() {
            var t = Object(o["a"])(["\n    ", "\n"]);
            return p = function () {
                return t
            }, t
        }

        function f() {
            var t = Object(o["a"])(["\n    display: flex;\n    flex-direction: column;\n"]);
            return f = function () {
                return t
            }, t
        }

        function b() {
            var t = Object(o["a"])(["\n    .modal-content {\n        .modal-box-wrapper {\n            width: 100%;\n            max-width: initial;\n\n            .buttons-wrapper {\n                justify-content: flex-end;\n\n                button:last-child {\n                    margin-left: 10px;\n                }\n            }\n        }\n    }\n"]);
            return b = function () {
                return t
            }, t
        }

        var m = Object(c["b"])(r["default"].component("bo-modal-box", l["a"]))(b()), h = Object(c["a"])(f()),
            v = c["b"].div(p(), h), g = c["b"].div(d(), h), _ = c["b"].span(u()),
            O = [["OPTIMIZE", "Optimize"], ["DISAPPROVED", "Update for disapproved ad(s)"], ["PUBLISHING_ERROR", "Update for publishing error(s)"], ["OTHER", "Other"]];
        e["a"] = {
            created: function () {
            }, name: "BoUpdateModal", props: {active: {type: Boolean, default: !1}}, data: function () {
                return {data: {visibleToUser: !1, type: "", message: ""}}
            }, render: function () {
                var t = this, e = arguments[0];
                if (this.active) {
                    var n = {
                        attrs: {
                            active: !0,
                            canCancel: ["escape"],
                            "max-width": "auto",
                            width: "750px",
                            headerText: "What did you change?",
                            cancelText: "Go back",
                            confirmText: "Submit"
                        }, on: {
                            "update:active": this.updateActive, cancel: function () {
                                return t.updateActive(!1)
                            }, confirm: function () {
                                t.$emit("confirm", t.data), t.updateActive(!1)
                            }
                        }
                    }, a = this.data, i = a.visibleToUser, o = a.message;
                    return e(m, s()([{}, n]), [e(v, [e(g, [e("b-checkbox", {
                        attrs: {value: i},
                        on: {input: this.handleCheckbox}
                    }, [e(_, ["Show on My Manager"])])]), this.renderTypes(), e(g, [e("b-field", {attrs: {label: "Note"}}, [e("b-input", {
                        attrs: {
                            value: o,
                            placeholder: "enter note"
                        }, on: {
                            input: function (e) {
                                return t.data.message = e
                            }
                        }
                    })])])])])
                }
            }, methods: {
                updateActive: function (t) {
                    this.$emit("update:active", t)
                }, renderTypes: function () {
                    var t = this, e = this.$createElement;
                    return e(g, [e("b-field", {attrs: {label: "Update reason"}}, [e("b-select", {
                        attrs: {
                            placeholder: "Select update reason",
                            value: this.data.type
                        }, on: {
                            input: function (e) {
                                return t.data.type = e
                            }
                        }
                    }, [O.map(function (t) {
                        var n = Object(a["a"])(t, 2), i = n[0], s = n[1];
                        return e("option", {domProps: {value: i}}, [s])
                    })])])])
                }, handleCheckbox: function (t) {
                    var e = this.data;
                    e.visibleToUser = t, !0 !== t || e.type ? !1 === t && (e.type = "") : e.type = O[0][0]
                }
            }
        }
    }, d868: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    staticClass: "fb-pages-modal",
                    attrs: {active: t._active, canCancel: ["escape", "x"], width: "450px"},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [n("div", {
                    ref: "wrapper",
                    staticClass: "fb-pages-modal-wrapper",
                    style: t.wrapperStyle
                }, [n("div", {
                    ref: "header",
                    staticClass: "fb-pages-modal-header"
                }, [t._v(t._s(t.$t("PROPOSAL.select_fb_page")))]), n("div", {staticClass: "fb-pages-modal-body"}, ["notAuth" === t.status ? n("div", {staticClass: "notAuth-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("PROPOSAL.select_fb_page_desc")))]), n("fb-auth-button", {
                    staticClass: "fb-auth-btn",
                    nativeOn: {
                        click: function (e) {
                            return e.stopPropagation(), t.fetchFacebookProfile()
                        }
                    }
                }), t.permissionsMissing ? n("p", {staticClass: "has-text-color34 mt-15"}, [t._v(t._s(t.$t("PROPOSAL.fb_permissions_missing")))]) : t._e()], 1) : t._e(), "noPages" === t.status ? n("div", {staticClass: "noPages-contents"}, [n("img", {attrs: {src: "/img/proposal/ads_no_fb_pages.png"}}), n("span", {
                    staticClass: "desc-span",
                    domProps: {innerHTML: t._s(t.$t("PROPOSAL.no_fb_admin_pages"))}
                })]) : t._e(), "pages" === t.status ? n("div", {staticClass: "pages-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("PROPOSAL.select_fb_page_ment")))]), t.selectablePages.length > 0 ? n("div", {staticClass: "page-elems-wrapper"}, t._l(t.selectablePages, function (e) {
                    return n("div", {key: e.id, staticClass: "page-elem"}, [n("b-radio", {
                        staticClass: "custom-radio",
                        class: {selected: t._compFacebookProfile.id === e.id},
                        attrs: {"native-value": e.id},
                        on: {
                            input: function (n) {
                                return t.radioSelected(e)
                            }
                        },
                        model: {
                            value: t._compFacebookProfile.id, callback: function (e) {
                                t.$set(t._compFacebookProfile, "id", e)
                            }, expression: "_compFacebookProfile.id"
                        }
                    }, [n("span", {staticClass: "page-elem-box"}, [n("img", {
                        staticClass: "page-elem-img",
                        attrs: {src: e.logo || "/img/proposal/fb_logo_none.png"}
                    }), n("span", {staticClass: "page-name"}, [t._v(t._s(e.name))]), e.website ? n("span", {staticClass: "page-url"}, [t._v(t._s("| " + e.website))]) : t._e(), e.id ? n("span", {staticClass: "page-url"}, [t._v(t._s("| " + e.id))]) : t._e()])])], 1)
                }), 0) : t._e(), n("div", {staticClass: "page-elems-wrapper"}, t._l(t.unselectablePages, function (e) {
                    return n("div", {
                        key: e.id,
                        staticClass: "page-elem disabled"
                    }, [n("span", {staticClass: "page-elem-box"}, [n("img", {
                        staticClass: "page-elem-img",
                        attrs: {src: e.logo || "/img/proposal/fb_logo_none.png"}
                    }), n("span", {staticClass: "page-name"}, [t._v(t._s(e.name))]), e.reason ? n("span", {staticClass: "page-url"}, [t._v(t._s(t.$t("PROPOSAL.fb_reason_" + e.reason)))]) : t._e()])])
                }), 0), t.instaPage ? n("div", {staticClass: "add-instagram-page"}, [n("b-checkbox", {
                    attrs: {size: "is-small"},
                    model: {
                        value: t.addInstaPage, callback: function (e) {
                            t.addInstaPage = e
                        }, expression: "addInstaPage"
                    }
                }, [n("img", {attrs: {src: "/img/icons/instagram_top_logo.png"}}), n("span", [t._v(t._s(t.$t("PROPOSAL.add_instagram_page")))])]), n("div", {staticClass: "page-elems-wrapper"}, [n("div", {staticClass: "page-elem"}, [n("span", {
                    staticClass: "page-elem-box",
                    class: {selected: t.addInstaPage}
                }, [n("img", {
                    staticClass: "page-elem-img",
                    attrs: {src: t.instaPage.profile_pic || "/img/proposal/fb_logo_none.png"}
                }), n("span", {staticClass: "page-name"}, [t._v(t._s(t.instaPage.username))])])])])], 1) : t._e()]) : t._e(), n("buttons", {
                    staticClass: "fb-pages-buttons",
                    class: t.status,
                    attrs: {
                        types: t.buttonTypes,
                        "cancel-text": t.buttonTypes.length > 1 ? t.$t("COMMON.cancel") : t.$t("COMMON.close"),
                        confirmText: t.$t("COMMON.select")
                    },
                    on: {
                        cancel: function (e) {
                            t._active = !1
                        }, confirm: t.confirm
                    }
                })], 1)])])
            }, i = [], s = n("3f96"), o = s["a"], r = (n("58c8"), n("fb64"), n("aa36"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "8f9df598", null);
        e["a"] = c.exports
    }, e190: function (t, e, n) {
        "use strict";
        var a = n("5de0"), i = n.n(a);
        i.a
    }, e501: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    attrs: {active: t._active, canCancel: ["escape", "x"], width: "750px"},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [n("div", {staticClass: "payment-preview-modal-wrapper"}, [n("div", {staticClass: "payment-preview-modal-header"}, [t._v("\n            " + t._s(t.$t("JOIN.adriel_offer")) + "\n        ")]), n("div", {staticClass: "payment-preview-modal-body"}, [n("service-preview", {
                    staticClass: "payment-preview-carousel",
                    attrs: {slides: t.slides, paginationActiveColor: "#5aabe3"}
                }), n("adriel-buttons", {
                    attrs: {types: ["confirm"], "confirm-text": t.$t("COMMON.done")},
                    on: {
                        cancel: function (e) {
                            t._active = !1
                        }, confirm: t.confirm
                    }
                })], 1)])])
            }, i = [], s = n("05d2"), o = s["a"], r = (n("8f77"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, null, null);
        e["a"] = c.exports
    }, e886: function (t, e, n) {
        "use strict";
        var a = n("0962"), i = n.n(a);
        i.a
    }, e996: function (t, e, n) {
        "use strict";
        var a = n("7dcd"), i = n.n(a);
        i.a
    }, ea61: function (t, e, n) {
        "use strict";
        (function (t) {
            n.d(e, "L", function () {
                return rt
            }), n.d(e, "g", function () {
                return ct
            }), n.d(e, "f", function () {
                return lt
            }), n.d(e, "e", function () {
                return pt
            }), n.d(e, "b", function () {
                return ft
            }), n.d(e, "a", function () {
                return bt
            }), n.d(e, "E", function () {
                return mt
            }), n.d(e, "F", function () {
                return ht
            }), n.d(e, "G", function () {
                return vt
            }), n.d(e, "H", function () {
                return gt
            }), n.d(e, "d", function () {
                return _t
            }), n.d(e, "y", function () {
                return Ot
            }), n.d(e, "w", function () {
                return yt
            }), n.d(e, "z", function () {
                return Ct
            }), n.d(e, "r", function () {
                return Pt
            }), n.d(e, "q", function () {
                return At
            }), n.d(e, "B", function () {
                return wt
            }), n.d(e, "A", function () {
                return jt
            }), n.d(e, "i", function () {
                return kt
            }), n.d(e, "h", function () {
                return $t
            }), n.d(e, "l", function () {
                return St
            }), n.d(e, "j", function () {
                return Lt
            }), n.d(e, "c", function () {
                return Mt
            }), n.d(e, "p", function () {
                return Rt
            }), n.d(e, "n", function () {
                return It
            }), n.d(e, "m", function () {
                return Tt
            }), n.d(e, "o", function () {
                return Et
            }), n.d(e, "k", function () {
                return Bt
            }), n.d(e, "K", function () {
                return Nt
            }), n.d(e, "J", function () {
                return Ft
            }), n.d(e, "I", function () {
                return Vt
            }), n.d(e, "D", function () {
                return Dt
            }), n.d(e, "t", function () {
                return zt
            }), n.d(e, "s", function () {
                return Gt
            }), n.d(e, "x", function () {
                return Ut
            }), n.d(e, "v", function () {
                return qt
            }), n.d(e, "u", function () {
                return Kt
            }), n.d(e, "C", function () {
                return Ht
            });
            var a = n("aede"), i = n("9c56"), s = n("2b0e"), o = n("3ec5"), r = n("31a2"), c = n("53cc"), l = n("bd2c");

            function u() {
                var t = Object(a["a"])(["\n    width: 90%;\n"]);
                return u = function () {
                    return t
                }, t
            }

            function d() {
                var t = Object(a["a"])(["\n    ", "\n    font-size: 13px;\n    color: #666666;\n    text-decoration: underline;\n    margin-top: 10px;\n    cursor: pointer;\n"]);
                return d = function () {
                    return t
                }, t
            }

            function p() {
                var t = Object(a["a"])(["\n    ", "\n    font-size: 13px;\n    text-align: center;\n    color: #333333;\n    margin-bottom: 10px;\n    max-width: 100%;\n"]);
                return p = function () {
                    return t
                }, t
            }

            function f() {
                var t = Object(a["a"])(["\n    ", "\n    flex-direction:column;\n    padding: 30px;\n    border-radius: 5px;\n    box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.3);\n    border: solid 2px #3b5998;\n    background-color: white;\n"]);
                return f = function () {
                    return t
                }, t
            }

            function b() {
                var t = Object(a["a"])(["\n    ", "\n    position: absolute;\n    top: 0;\n    right: 0;\n    left: 0;\n    bottom: 0;\n    background-color: rgba(255, 255, 255, 0.7);\n    z-index: 2;\n    border-radius: 5px;\n    animation-duration: 0.3s;\n    cursor: default;\n"]);
                return b = function () {
                    return t
                }, t
            }

            function m() {
                var t = Object(a["a"])(["\n    will-change: grid-row-end;\n"]);
                return m = function () {
                    return t
                }, t
            }

            function h() {
                var t = Object(a["a"])(["\n    ", "\n    font-size: 11px;\n    text-align: left;\n    margin-left: 5px;\n    color: ", ";\n"]);
                return h = function () {
                    return t
                }, t
            }

            function v() {
                var t = Object(a["a"])(["\n    ", "\n"]);
                return v = function () {
                    return t
                }, t
            }

            function g() {
                var t = Object(a["a"])(["\n    width: 21px;\n    height: 19px;\n    background-repeat: no-repeat;\n    background-size: 100%;\n    cursor: pointer;\n    background-image: ", ";\n\n    &:hover {\n        background-image: url('/img/icons/preview_copy_active.png');\n    }\n"]);
                return g = function () {
                    return t
                }, t
            }

            function _() {
                var t = Object(a["a"])(["\n    ", "\n    font-size: 12px;\n    color: #333333;\n"]);
                return _ = function () {
                    return t
                }, t
            }

            function O() {
                var t = Object(a["a"])(["\n    cursor: pointer;\n"]);
                return O = function () {
                    return t
                }, t
            }

            function y() {
                var t = Object(a["a"])(["\n    width: 25px;\n    display: flex;\n    align-items: center;\n"]);
                return y = function () {
                    return t
                }, t
            }

            function C() {
                var t = Object(a["a"])(["\n    cursor: pointer;\n    font-size: 30px;\n    padding: 0 5px;\n    position: relative;\n    top: 2px;\n    margin-left: 3px;\n    &:hover {\n        color: #999;\n    }\n"]);
                return C = function () {
                    return t
                }, t
            }

            function P() {
                var t = Object(a["a"])(["\n    font-size: 11px;\n    margin-right: 5px;\n"]);
                return P = function () {
                    return t
                }, t
            }

            function A() {
                var t = Object(a["a"])(["\n    color: white;\n    font-size: 30px;\n"]);
                return A = function () {
                    return t
                }, t
            }

            function x() {
                var t = Object(a["a"])(["\n    ", "\n    font-size: 14px;\n    font-weight: bold;\n    color: white;\n    margin-top: 5px;\n"]);
                return x = function () {
                    return t
                }, t
            }

            function w() {
                var t = Object(a["a"])(["\n    width: 150px;\n    height: 150px;\n    background-color: rgba(90, 171, 227, 0.9);\n    border-radius: 50%;\n    ", "\n    flex-direction: column;\n"]);
                return w = function () {
                    return t
                }, t
            }

            function j() {
                var t = Object(a["a"])(["\n    position: absolute;\n    top: 45px;\n    right: 0;\n    left: 0;\n    bottom: 0;\n    background-color: rgba(90, 171, 227, 0.3);\n    z-index: 2;\n    ", "\n    border-bottom-right-radius: 5px;\n    border-bottom-left-radius: 5px;\n"]);
                return j = function () {
                    return t
                }, t
            }

            function k() {
                var t = Object(a["a"])(["\n                  border: solid 1px #cccccc;\n                  background-color: #f2f3f5;\n                  color: #666666;\n              "]);
                return k = function () {
                    return t
                }, t
            }

            function $() {
                var t = Object(a["a"])(["\n                  border: none !important;\n                  color: white !important;\n                  background-color: #5aabe3 !important;\n              "]);
                return $ = function () {
                    return t
                }, t
            }

            function S() {
                var t = Object(a["a"])(["\n    height: 25px;\n    border-radius: 3px;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    ", "\n    font-size: 13px !important;\n    font-weight: 600;\n    ", "\n"]);
                return S = function () {
                    return t
                }, t
            }

            function L() {
                var t = Object(a["a"])(["\n    display: flex;\n    align-items: center;\n    margin: 0px !important;\n    & > * {\n        margin: 0 5px;\n    }\n"]);
                return L = function () {
                    return t
                }, t
            }

            function M() {
                var t = Object(a["a"])(["\n    ", "\n    background-color: #999999;\n    &:hover {\n        background-color: #adadad;\n    }\n"]);
                return M = function () {
                    return t
                }, t
            }

            function R() {
                var t = Object(a["a"])(["\n    ", "\n    background-color: #5aabe3;\n    &:hover {\n        background-color: #519acc;\n    }\n"]);
                return R = function () {
                    return t
                }, t
            }

            function I() {
                var t = Object(a["a"])(["\n    ", "\n    height: 45px;\n    border-radius: 3px;\n    padding: 0 20px;\n    font-size: 16px;\n    font-weight: 600;\n    color: white;\n    @media screen and (max-width: 800px) {\n        font-size: 14px;\n    }\n"]);
                return I = function () {
                    return t
                }, t
            }

            function T() {
                var t = Object(a["a"])(["\n    ", "\n    font-size: 20px;\n    color: #797d88;\n    margin-top: 35px;\n    white-space: pre-line;\n    text-align: center;\n    @media screen and (max-width: 800px) {\n        font-size: 18px;\n    }\n"]);
                return T = function () {
                    return t
                }, t
            }

            function E() {
                var t = Object(a["a"])(["\n    padding: 80px 0;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    @media screen and (max-width: 800px) {\n        padding: 50px 0;\n    }\n"]);
                return E = function () {
                    return t
                }, t
            }

            function B() {
                var t = Object(a["a"])(["\n    ", "\n    font-size: 13px;\n    color: #666666;\n    margin-left: 7px;\n"]);
                return B = function () {
                    return t
                }, t
            }

            function N() {
                var t = Object(a["a"])(["\n    width: 13px;\n    height: 16px;\n"]);
                return N = function () {
                    return t
                }, t
            }

            function F() {
                var t = Object(a["a"])(["\n    ", "\n    z-index: 1;\n    flex-shrink: 0;\n    border-radius: 3px;\n    border: solid 1px #cccccc;\n    background-color: #f2f3f5;\n    height: 35px;\n    padding: 0 10px;\n\n    &:hover {\n        background-color: #eee;\n    }\n"]);
                return F = function () {
                    return t
                }, t
            }

            function V() {
                var t = Object(a["a"])(["\n    z-index: 1;\n    display: flex;\n    width: 100%;\n    margin-top: 20px;\n\n    /* slot navigation */\n    .nav-above {\n        margin-left: auto;\n    }\n"]);
                return V = function () {
                    return t
                }, t
            }

            function D() {
                var t = Object(a["a"])(["\n    ", "\n    margin-top: 15px;\n    font-size: 18px;\n    text-align: center;\n    color: #797d88;\n    font-weight: 600;\n    @media screen and (max-width: 800px) {\n        font-size: 16px;\n    }\n"]);
                return D = function () {
                    return t
                }, t
            }

            function z() {
                var t = Object(a["a"])([""]);
                return z = function () {
                    return t
                }, t
            }

            function G() {
                var t = Object(a["a"])(["\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    position: relative;\n    height: 100%;\n"]);
                return G = function () {
                    return t
                }, t
            }

            function U() {
                var t = Object(a["a"])(["\n    height: 250px;\n    min-width: 320px;\n    border-radius: 5px;\n    background-color: transparent;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    cursor: pointer;\n    border: 2px dashed #b3b9c6;\n    .active-icon {\n        display: none;\n    }\n\n    &:hover {\n        .normal-icon {\n            display: none;\n        }\n        .active-icon {\n            display: block;\n        }\n    }\n"]);
                return U = function () {
                    return t
                }, t
            }

            function q() {
                var t = Object(a["a"])(["\n    grid-row-end: span 2;\n\n    .onboarding-ad-container {\n        max-width: 700px;\n    }\n\n    .onboarding-ad-preview--header .buttons-container {\n        .edit-btn {\n            display: none;\n            pointer-events: none;\n        }\n        .dropdown {\n            margin: 0;\n        }\n        .dropdown-content {\n            .dropdown-item {\n                display: flex;\n                align-items: center;\n            }\n        }\n    }\n\n    &:hover {\n        cursor: pointer;\n        border-color: transparent;\n        &::before {\n            content: '';\n            position: absolute;\n            pointer-events: none;\n            width: calc(100% + 4px);\n            height: calc(100% + 4px);\n            border: 2px solid #5aabe3;\n            background-color: transparent;\n            z-index: 2;\n            border-radius: 5px;\n            margin: -2px;\n        }\n\n        .onboarding-ad-preview--header {\n            border-color: #5aabe3;\n        }\n    }\n"]);
                return q = function () {
                    return t
                }, t
            }

            function K() {
                var t = Object(a["a"])(["\n    margin: 20px 0 -10px 0;\n    will-change: height;\n    display: grid;\n    grid-gap: 20px;\n    grid-row-gap: 12px;\n    grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));\n    grid-auto-rows: 0;\n\n    &.IE {\n        display: flex;\n        flex-wrap: wrap;\n        justify-content: center;\n        align-items: flex-start;\n        margin: 10px -10px -20px -10px;\n\n        .new-box {\n            width: 310px;\n        }\n\n        & > * {\n            min-width: 320px;\n            max-width: 380px;\n            margin: 10px;\n            @media screen and (max-width: 800px) {\n                width: 100%;\n            }\n        }\n    }\n"]);
                return K = function () {
                    return t
                }, t
            }

            function H() {
                var t = Object(a["a"])(["\n            opacity: 0.65;\n            background-color: transparent;\n        "]);
                return H = function () {
                    return t
                }, t
            }

            function W() {
                var t = Object(a["a"])(["\n    width: 100%;\n    height: 100%;\n    background-repeat: no-repeat;\n    background-position: center;\n    display: inline-block;\n    border-radius: 100px;\n    transition: all 0.2s;\n    ", ";\n    ", "\n    ", "\n"]);
                return W = function () {
                    return t
                }, t
            }

            function J() {
                var t = Object(a["a"])(["\n            background-image: url('/img/proposal/ads_tab_apple.png');\n        "]);
                return J = function () {
                    return t
                }, t
            }

            function Y() {
                var t = Object(a["a"])(["\n            background-image: url('/img/proposal/ads_tab_instagram.png');\n        "]);
                return Y = function () {
                    return t
                }, t
            }

            function X() {
                var t = Object(a["a"])(["\n            background-image: url('/img/proposal/ads_tab_facebook.png');\n        "]);
                return X = function () {
                    return t
                }, t
            }

            function Q() {
                var t = Object(a["a"])(["\n            &::before {\n                background-color: transparent;\n                color: #797d88;\n            }\n        "]);
                return Q = function () {
                    return t
                }, t
            }

            function Z() {
                var t = Object(a["a"])(["\n                background-image: url('/img/proposal/ads_tab_apple_active.png');\n                background-color: #069df5;\n            "]);
                return Z = function () {
                    return t
                }, t
            }

            function tt() {
                var t = Object(a["a"])(["\n                background-image: url('/img/proposal/ads_tab_google.png');\n                background-color: #fff;\n            "]);
                return tt = function () {
                    return t
                }, t
            }

            function et() {
                var t = Object(a["a"])(["\n                background-image: url('/img/proposal/ads_tab_instagram_active.png');\n                background-color: #d12798;\n            "]);
                return et = function () {
                    return t
                }, t
            }

            function nt() {
                var t = Object(a["a"])(["\n                background-image: url('/img/proposal/ads_tab_facebook_active.png');\n                background-color: #385c8e;\n            "]);
                return nt = function () {
                    return t
                }, t
            }

            function at() {
                var t = Object(a["a"])(["\n                &::before {\n                    background-color: #797d88;\n                    border-radius: 100px;\n                    content: 'All';\n                    display: flex;\n                    justify-content: center;\n                    align-items: center;\n                    width: 100%;\n                    height: 100%;\n                    font-size: 14px;\n                    font-weight: 500;\n                    color: white;\n                }\n            "]);
                return at = function () {
                    return t
                }, t
            }

            function it() {
                var t = Object(a["a"])(["\n    margin: 0 5px;\n    background-color: transparent;\n    width: 50px;\n    height: 30px;\n    border-radius: 100px;\n    cursor: pointer;\n    padding: 0;\n    border: ", ";\n"]);
                return it = function () {
                    return t
                }, t
            }

            function st() {
                var t = Object(a["a"])(["\n    display: flex;\n    margin: 0 -5px;\n    position: relative;\n    z-index: 1;\n    align-items: flex-end;\n"]);
                return st = function () {
                    return t
                }, t
            }

            function ot() {
                var t = Object(a["a"])(["\n    ", "\n    width: 100%;\n    display: flex;\n    flex-direction: column;\n    margin-top: 30px;\n    min-width: 1000px;\n    position: relative;\n    @media screen and (max-width: 800px) {\n        width: 90vw;\n        min-width: initial;\n    }\n"]);
                return ot = function () {
                    return t
                }, t
            }

            var rt = Object(i["b"])("section")(ot(), l["g"]), ct = i["b"].div(st()),
                lt = Object(i["b"])("button", {selected: Boolean, channel: String})(it(), function (t) {
                    var e = t.selected;
                    return e ? "none" : "1px solid #cccccc"
                }),
                ut = t.compose(t.cond([[t.equals("all"), t.always(Object(i["a"])(at()))], [t.equals("facebook"), t.always(Object(i["a"])(nt()))], [t.equals("instagram"), t.always(Object(i["a"])(et()))], [t.equals("google"), t.always(Object(i["a"])(tt()))], [t.equals("apple"), t.always(Object(i["a"])(Z()))]]), t.prop("channel")),
                dt = t.cond([[t.equals("all"), t.always(Object(i["a"])(Q()))], [t.equals("facebook"), t.always(Object(i["a"])(X()))], [t.equals("instagram"), t.always(Object(i["a"])(Y()))], [t.equals("apple"), t.always(Object(i["a"])(J()))]]),
                pt = Object(i["b"])("span", {channel: String, selected: Boolean})(W(), ut, function (t) {
                    var e = t.selected;
                    return !e && Object(i["a"])(H())
                }, function (t) {
                    var e = t.selected, n = t.channel;
                    return !e && dt(n)
                }), ft = Object(i["b"])("div")(K()),
                bt = Object(i["b"])(s["default"].component("ads-preview", o["a"]))(q()), mt = i["b"].div(U()),
                ht = i["b"].div(G()), vt = i["b"].img(z()), gt = i["b"].span(D(), l["g"]), _t = i["b"].div(V()),
                Ot = i["b"].button(F(), l["g"]), yt = i["b"].img(N()), Ct = i["b"].span(B(), l["g"]),
                Pt = i["b"].div(E()), At = i["b"].span(T(), l["g"]), xt = Object(i["a"])(I(), l["g"]),
                wt = (i["b"].button(R(), xt), i["b"].button(M(), xt), i["b"].div(L())),
                jt = Object(i["b"])("button", {type: String})(S(), l["g"], function (t) {
                    var e = t.type;
                    return "paste" === e ? Object(i["a"])($()) : Object(i["a"])(k())
                }), kt = i["b"].div(j(), l["f"]), $t = i["b"].div(w(), l["f"]), St = i["b"].span(x(), l["g"]),
                Lt = i["b"].i(A()), Mt = i["b"].i(P()), Rt = i["b"].i(C()), It = i["b"].span(y()), Tt = i["b"].img(O()),
                Et = i["b"].span(_(), l["g"]), Bt = Object(i["b"])("span", {active: Boolean})(g(), function (t) {
                    var e = t.active;
                    return e ? "url('/img/icons/preview_copy_active.png')" : "url('/img/icons/preview_copy.png')"
                }), Nt = i["b"].div(v(), l["f"]), Ft = r["a"],
                Vt = Object(i["b"])("span", {isOn: Boolean})(h(), l["g"], function (t) {
                    var e = t.isOn;
                    return e ? "#5aabe3" : "#999999"
                }), Dt = i["b"].div(m()), zt = i["b"].div(b(), l["f"]), Gt = i["b"].div(f(), l["f"]),
                Ut = i["b"].div(p(), l["g"]), qt = c["a"], Kt = i["b"].span(d(), l["g"]), Ht = l["j"].extend(u())
        }).call(this, n("b17e"))
    }, ec3e: function (t, e, n) {
    }, eeaf: function (t, e, n) {
        "use strict";
        var a = n("f20c"), i = n.n(a);
        i.a
    }, f20c: function (t, e, n) {
    }, f4d5: function (t, e, n) {
        "use strict";
        var a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "summary-tags"}, [n("span", {staticClass: "summary-tags-text"}, [t._v("\n        " + t._s(t.$t("PROPOSAL.ap_summary_title")) + "\n    ")]), n("div", {staticClass: "summary-tags-elements"}, t._l(t.tags, function (e) {
                    return n("span", {
                        key: e.key + "-" + e.value,
                        class: [e.key, e.value]
                    }, [n("span", {
                        directives: [{
                            name: "line-clamp",
                            rawName: "v-line-clamp:20",
                            value: 2,
                            expression: "2",
                            arg: "20"
                        }]
                    }, [t._v(t._s(e.value))])])
                }), 0)])
            }, i = [], s = n("59e7"), o = s["a"], r = (n("b79b"), n("eeaf"), n("2877")),
            c = Object(r["a"])(o, a, i, !1, null, "03472894", null);
        e["a"] = c.exports
    }, f560: function (t, e, n) {
        "use strict";
        var a = n("feea"), i = n.n(a);
        i.a
    }, f745: function (t, e, n) {
    }, f876f: function (t, e, n) {
        "use strict";
        (function (t) {
            var a = n("2638"), i = n.n(a), s = n("cebc"), o = n("6e43"), r = n("fa7d"), c = n("a772");
            e["a"] = {
                name: "AdsSelectionModal",
                props: {
                    active: {type: Boolean, default: !1},
                    type: {type: String, default: "WEB"},
                    appstoreAvailable: {type: Boolean, default: !0},
                    channel: String
                },
                created: function () {
                    var e = Object(c["c"])(t.pick(["type", "appstoreAvailable", "channel"], this)), n = e.options,
                        a = void 0 === n ? [] : n, i = e.selected, s = void 0 === i ? [] : i;
                    this.selected = s, this.options = Object(r["f"])(a)
                },
                data: function () {
                    return {selected: [], options: []}
                },
                render: function () {
                    var e = this, n = arguments[0];
                    if (this.active) {
                        var a = {
                            attrs: {active: !0, canCancel: ["escape"], "max-width": "auto", width: "750px"},
                            on: {"update:active": this.updateActive}
                        }, r = {
                            attrs: Object(s["a"])({}, t.pick(["type", "selected", "options"], this)),
                            on: {
                                "update:selected": function (t) {
                                    return e.selected = t
                                }
                            }
                        };
                        return n(o["f"], i()([{}, a]), [n(o["g"], [n(o["e"], {
                            attrs: {src: "/img/icons/modal_close_btn.png"},
                            nativeOn: {
                                click: function () {
                                    return e.updateActive(!1)
                                }
                            }
                        }), n(o["b"], [n(o["a"], i()([{}, r]))]), n(o["d"], [n(o["c"], {
                            nativeOn: {
                                click: function () {
                                    return e.updateActive(!1)
                                }
                            }
                        }, [this.$t("COMMON.cancel")]), n(o["c"], {
                            attrs: {isConfirm: !0},
                            nativeOn: {click: this.handleConfirm}
                        }, [this.$t("COMMON.create")])])])])
                    }
                },
                methods: {
                    updateActive: function (t) {
                        this.$emit("update:active", t)
                    }, handleConfirm: function () {
                        this.$emit("confirm", this.selected), this.updateActive(!1)
                    }
                }
            }
        }).call(this, n("b17e"))
    }, fb64: function (t, e, n) {
        "use strict";
        var a = n("0178"), i = n.n(a);
        i.a
    }, fe0b: function (t, e, n) {
    }, fe19: function (t, e, n) {
        "use strict";
        var a = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "payment-list"}, [t.loading ? n("div", [n("b-icon", {
                attrs: {
                    pack: "fas",
                    icon: "sync-alt",
                    "custom-class": "fa-spin"
                }
            })], 1) : n("div", [t.paymentMethods.length > 0 ? n("div", {staticClass: "payment-method-list"}, t._l(t.paymentMethods, function (e, a) {
                return n("div", {key: a, staticClass: "mt-10 mb-10"}, [n("payment-method", {
                    attrs: {
                        method: e,
                        selectable: !0,
                        default: e.default
                    }, on: {"set-default": t.setDefault, remove: t.deletePaymentMethod}
                })], 1)
            }), 0) : t._e(), t.paymentMethods && 0 !== t.paymentMethods.length ? t._e() : n("div", {staticClass: "has-text-centered"}, [n("img", {attrs: {src: "/img/icons/payment_nocard.png"}}), n("p", {staticClass: "has-text-color10 mt-15 mb-15"}, [t._v("\n            " + t._s(t.$t("ACCOUNT_SETTING.no_payment_method")) + "\n        ")])]), n("button", {
                staticClass: "mt-15 button has-text-color1 has-background-color7 is-fullwidth",
                on: {click: t.addPaymentMethod}
            }, [0 === t.paymentMethods.length ? [t._v("\n            " + t._s(t.$t("COMMON.add_payment_method")) + "\n        ")] : [t._v("\n            " + t._s(t.$t("ACCOUNT_SETTING.change_payment")) + "\n        ")]], 2)]), n("modal-box", {
                attrs: {
                    active: t.addPaymentModal,
                    hasButtons: !1,
                    headerText: t.$t("ACCOUNT_SETTING.payment_add_method")
                }, on: {
                    "update:active": function (e) {
                        t.addPaymentModal = e
                    }
                }
            }, [n("add-payment-method", {
                on: {
                    refresh: function (e) {
                        return t.fetchPaymentMethods()
                    }
                }
            })], 1)], 1)
        }, i = [], s = n("cebc"), o = n("2f62"), r = n("1bc2"), c = n("c10c"), l = n("ac5d"), u = n("51c6"), d = {
            name: "PaymentList",
            components: {PaymentMethod: l["a"], AddPaymentMethod: c["a"], ModalBox: u["a"]},
            data: function () {
                return {loading: !0, addPaymentModal: !1}
            },
            methods: Object(s["a"])({}, Object(o["mapActions"])("user", ["fetchPaymentMethods"]), {
                addPaymentMethod: function () {
                    this.isDemo ? this.showDemoAccountModal() : this.addPaymentModal = !0
                }, setDefault: function (t) {
                    var e = this;
                    this.loading = !0, r["a"].makeDefault(t).then(function () {
                        return e.fetchPaymentMethods().then(function () {
                            return e.loading = !1
                        })
                    })
                }, deletePaymentMethod: function (t) {
                    var e = this;
                    this.confirmBefore({
                        text: this.$t("ACCOUNT_SETTING.payment_remove_confirm"),
                        confirmCb: function () {
                            r["a"].deletePaymentMethod(t).then(e.fetchPaymentMethods).catch(function (n) {
                                var a = n.response;
                                if (a && a.data) {
                                    var i = a.data;
                                    "RUNNING_CAMPAIGN" === i.code ? e.stopRunningCampaign(t) : "CAMPAIGN_TO_PAY" === i.code && e.toast(e.$t("ACCOUNT_SETTING.unpaid_campaigns"))
                                }
                            })
                        }
                    })
                }, stopRunningCampaign: function (t) {
                    var e = this;
                    this.confirmBefore({
                        text: this.$t("ACCOUNT_SETTING.payment_confirm"), confirmCb: function () {
                            r["a"].deletePaymentMethod(t, !0).then(e.fetchPaymentMethods).catch(function (t) {
                                var n = t.response;
                                if (n && n.data) {
                                    var a = n.data;
                                    "CAMPAIGN_TO_PAY" === a.code && e.toast("You have some unpaid campaigns")
                                }
                            })
                        }
                    })
                }
            }),
            computed: Object(s["a"])({}, Object(o["mapGetters"])("user", ["paymentMethods"])),
            created: function () {
                var t = this;
                this.fetchPaymentMethods().then(function (e) {
                    t.loading = !1
                }).catch(function (e) {
                    t.loading = !1
                })
            }
        }, p = d, f = (n("33eb"), n("2877")), b = Object(f["a"])(p, a, i, !1, null, null, null);
        e["a"] = b.exports
    }, fea0: function (t, e, n) {
    }, feea: function (t, e, n) {
    }, ff9e: function (t, e, n) {
        "use strict";
        var a = n("a61b"), i = n.n(a);
        i.a
    }
}]);
//# sourceMappingURL=proposal.6c98880c.js.map