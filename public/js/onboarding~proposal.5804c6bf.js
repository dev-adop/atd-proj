(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["onboarding~proposal"], {
    "0320": function (t, e, n) {
    }, "068f": function (t, e, n) {
    }, "0819": function (t, e, n) {
    }, "08fe": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    staticClass: "inputs-modal",
                    class: {"is-loading": t.isLoading},
                    attrs: {active: t._active, canCancel: ["escape", "x"], width: "450px"},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }, close: t.cancel
                    }
                }, [n("div", {
                    ref: "wrapper",
                    staticClass: "inputs-modal-wrapper",
                    style: t.wrapperStyle
                }, [n("div", {
                    ref: "header",
                    staticClass: "inputs-modal-header"
                }, [t._v(t._s(t.headerText))]), n("div", {staticClass: "inputs-modal-adder-wrapper"}, [t._t("title"), "general" === t.addType ? n("div", {staticClass: "inputs-modal-adder"}, [n("b-autocomplete", {
                    ref: "input",
                    attrs: {data: t.autoCompleteArr, placeholder: t.placeholder, "clear-on-select": !0},
                    on: {select: t.addAutoCompleteValue},
                    nativeOn: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, keydown: function (e) {
                            return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.dAddValue(e)
                        }
                    },
                    model: {
                        value: t.searchKeyword, callback: function (e) {
                            t.searchKeyword = e
                        }, expression: "searchKeyword"
                    }
                }), n("img", {
                    attrs: {src: "/img/proposal/journey_add_btn.png"}, on: {
                        click: function (e) {
                            return t.addValue()
                        }, mousedown: function (t) {
                            t.stopPropagation()
                        }
                    }
                })], 1) : t._e(), "contact" === t.addType ? n("div", {staticClass: "inputs-modal-adder"}, [n("img", {
                    attrs: {src: "/img/proposal/journey_add_btn.png"},
                    on: {
                        click: t.addPhone, mousedown: function (t) {
                            t.stopPropagation()
                        }
                    }
                })]) : t._e(), "search" === t.addType ? n("div", {staticClass: "inputs-modal-adder async-search"}, [n("b-autocomplete", {
                    ref: "input",
                    attrs: {
                        data: t.autoCompleteArr,
                        placeholder: t.placeholder,
                        "clear-on-select": !0,
                        "open-on-focus": !!this.searchKeyword
                    },
                    on: {select: t.addAutoCompleteValue, focus: t.focusInput},
                    nativeOn: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }
                    },
                    model: {
                        value: t.searchKeyword, callback: function (e) {
                            t.searchKeyword = e
                        }, expression: "searchKeyword"
                    }
                }, [t.isLoading ? t._e() : n("template", {slot: "empty"}, [t._v("No results for " + t._s(t.searchKeyword))])], 2), n("img", {
                    ref: "searchBtn",
                    attrs: {src: "/img/proposal/search_icon.png"},
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation(), t.preventDefault()
                        }, mouseup: function (t) {
                            t.stopPropagation(), t.preventDefault()
                        }, click: function (t) {
                            t.stopPropagation(), t.preventDefault()
                        }
                    }
                })], 1) : t._e(), n("div", {staticClass: "inputs-modal-divider"}), t.isLoading ? n("clip-loader", {
                    attrs: {
                        color: "#999",
                        size: "20px"
                    }
                }) : t._e(), t.values.length && !t.isLoading ? n("div", {staticClass: "inputs-modal-editable-body"}, [n("editable-box", {
                    staticClass: "inputs-modal-editable-box",
                    style: {"max-height": t.maxHeight + "px"},
                    attrs: {
                        "btn-text": "Add more sites\n where your ads will appear",
                        values: t.values,
                        deletable: !0,
                        editable: t.editable,
                        btn: !1,
                        "name-getter": t.nameGetter,
                        "value-getter": t.valueGetter,
                        currentEditIdx: t.currentEditIdx
                    },
                    on: {"click:delete": t.handleDelete, "change:value": t.handleChange}
                })], 1) : t._e()], 2), n("div", {staticClass: "buttons-wrapper"}, [n("button", {
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.cancel
                    }
                }, [t._v(t._s(t.$t("COMMON.cancel")))]), n("button", {
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.confirm
                    }
                }, [t._v(t._s(t.$t("COMMON.ok")))])])])])
            }, a = [], r = n("68b6"), s = r["a"], o = (n("a501"), n("766f"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, "8126bf54", null);
        e["a"] = c.exports
    }, "0a7c": function (t, e, n) {
        "use strict";
        var i = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", [n("card-view", {
                attrs: {
                    label: t.$t("PROPOSAL.ap_card_title_device"),
                    className: t.className,
                    editable: t.editable,
                    isEditing: t.isEditing,
                    dimmed: t.dimmed,
                    resetable: t.resetable,
                    resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                    confirmLabel: "I'd like to change this setting"
                }, on: {
                    "edit:start": function (e) {
                        return t.$emit("edit:start")
                    }, "edit:end": function (e) {
                        return t.$emit("edit:end")
                    }, "edit:reset": t.reset
                }
            }, [n("on-off-list", {
                attrs: {
                    "name-getter": function (e) {
                        return t.$t("PROPOSAL.ap_card_device_" + e.name)
                    }, "value-getter": function (t) {
                        return t.value
                    }, values: t.values, images: t.images
                }, on: {
                    "update:value": function (e) {
                        return t.$emit("update:value", e)
                    }
                }
            })], 1)], 1)
        }, a = [], r = n("3de4"), s = n("73ff"), o = {
            mixins: [r["a"]], created: function () {
            }, data: function () {
                return {images: ["/img/proposal/device_desktop.png", "/img/proposal/device_mobile.png"]}
            }, methods: {}, props: {}, components: {"on-off-list": s["a"]}
        }, c = o, u = n("2877"), l = Object(u["a"])(c, i, a, !1, null, null, null);
        e["a"] = l.exports
    }, "0bba": function (t, e, n) {
    }, "0c2f": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    attrs: {active: t._active, canCancel: ["escape", "x"], width: "650px"},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [n("div", {staticClass: "billing-modal-wrapper"}, [n("div", {staticClass: "billing-modal-header"}, [t._v("\n            " + t._s(t.$t("PROPOSAL.billing_faq")) + "\n        ")]), n("div", {staticClass: "billing-modal-body"}, [t._l(t.contents, function (e, i) {
                    return n("div", {
                        key: i,
                        staticClass: "billing-modal-body__each"
                    }, [n("span", {staticClass: "each-title"}, [n("img", {attrs: {src: "/img/icons/billing_faq_icon.png"}}), t._v("\n                    " + t._s(e.title) + "\n                ")]), n("span", {
                        staticClass: "each-content",
                        domProps: {innerHTML: t._s(e.content)}
                    })])
                }), n("div", {staticClass: "button-wrapper"}, [n("button", {
                    on: {
                        click: function (e) {
                            t._active = !1
                        }
                    }
                }, [t._v("\n                    " + t._s(t.$t("COMMON.close")) + "\n                ")])])], 2)])])
            }, a = [], r = n("d70f"), s = r["a"], o = (n("99b8"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, null, null);
        e["a"] = c.exports
    }, "0e88": function (t, e, n) {
        "use strict";
        var i = n("d349"), a = n.n(i);
        a.a
    }, "100e": function (t, e, n) {
        var i = n("cd9d"), a = n("2286"), r = n("c1c9");

        function s(t, e) {
            return r(a(t, e, i), t + "")
        }

        t.exports = s
    }, "12e7": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "editable-input-wrapper"}, [t.isEditing ? t._e() : n("span", {
                    staticClass: "editable-element",
                    class: t.additionalClass,
                    on: {click: t.handleEdit}
                }, [t.text ? n("span", {staticClass: "content-span"}, [t._v(t._s(t._f("special")(t.text, t.specials)))]) : n("span", {staticClass: "content-span"}, [t._v(t._s(t._f("special")(t.value, t.specials)))]), t.getTooltip(t.value) ? n("v-popover", [n("img", {
                    staticClass: "popover-icon",
                    attrs: {src: "/img/proposal/journey_info_icon.png"}
                }), n("div", {
                    attrs: {slot: "popover"},
                    slot: "popover"
                }, [n(t.getTooltip(t.value), {tag: "component"})], 1)]) : t._e(), n("div", {staticClass: "editable-icons"}, [t.editable ? n("img", {
                    staticClass: "editable-edit",
                    attrs: {src: "/img/proposal/journey_edit_btn.png"},
                    on: {
                        click: t.handleEdit, mousedown: function (t) {
                            t.stopPropagation()
                        }
                    }
                }) : t._e(), t.deletable ? n("img", {
                    staticClass: "editable-delete",
                    attrs: {src: "/img/proposal/journey_delete_icon.png"},
                    on: {
                        click: t.handleDelete, mousedown: function (t) {
                            t.stopPropagation()
                        }
                    }
                }) : t._e()])], 1), n("span", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.isEditing,
                        expression: "isEditing"
                    }], staticClass: "editable-element editing"
                }, [n("b-input", {
                    ref: "input",
                    attrs: {placeholder: "Sub text input"},
                    on: {blur: t.handleBlur},
                    nativeOn: {
                        keypress: function (e) {
                            return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.blurElement(e)
                        }
                    },
                    model: {
                        value: t.inputText, callback: function (e) {
                            t.inputText = e
                        }, expression: "inputText"
                    }
                }), t.deletable ? n("img", {
                    staticClass: "editable-delete",
                    attrs: {src: "/img/proposal/journey_confirm_btn.png"},
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }
                    }
                }) : t._e()], 1)])
            }, a = [], r = n("ada6"), s = r["a"], o = (n("c4ca"), n("97cc"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, "38d602ee", null);
        e["a"] = c.exports
    }, 1571: function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("cebc"), a = (n("c5f6"), n("b311")), r = n.n(a), s = n("2f62"), o = n("dde5"), c = n("6510"),
                u = n("fa7d");
            e["a"] = {
                created: function () {
                },
                watch: {
                    apps: function (e) {
                        var n = t.head(e);
                        !Object(u["N"])(this.selectedApp) && n && (this.selectedApp = !0 === n.disable ? {} : n)
                    }
                },
                props: {active: {type: Boolean, default: !1}, campaignIdProp: [String, Number]},
                data: function () {
                    return {
                        values: [],
                        checked: [],
                        compFacebookProfile: {},
                        isWarning: !1,
                        howToPage: 0,
                        selectedApp: {},
                        isAppSelected: !1,
                        isWarningModalActive: !1
                    }
                },
                computed: Object(i["a"])({}, Object(s["mapGetters"])("proposal", ["facebookProfile"]), Object(s["mapGetters"])("user", ["fbInfo"]), Object(s["mapGetters"])("campaign", ["campaignId"]), {
                    _active: {
                        get: t.prop("active"),
                        set: function (t) {
                            t || this.$emit("cancel"), this.$emit("update:active", t)
                        }
                    },
                    apps: t.compose(t.defaultTo([]), t.apply(t.concat), t.partition(t.complement(t.propEq)("disable", !0)), t.pathOr([], ["fbInfo", "apps"])),
                    status: function () {
                        return Object(u["N"])(this.fbInfo) ? this.apps.length ? this.isAppSelected ? "howTo" : "apps" : "noApps" : "notAuth"
                    },
                    lastHowTo: t.converge(t.equals, [t.prop("howToPage"), t.compose(t.dec, t.path(["howTo", "length"]))]),
                    buttonTypes: function () {
                        switch (this.status) {
                            case"notAuth":
                            case"noApps":
                                return ["cancel"];
                            case"howTo":
                                return ["confirm"];
                            default:
                                return ["cancel", "confirm"]
                        }
                    },
                    howTo: function () {
                        return this.$t("COMMON.fb_app_how_to")
                    },
                    currentHowTo: function () {
                        return this.howTo[this.howToPage]
                    },
                    confirmText: function () {
                        switch (this.status) {
                            case"apps":
                                return this.$t("COMMON.ok");
                            case"howTo":
                                return this.howToPage === this.howTo.length - 1 ? this.$t("COMMON.done") : this.$t("COMMON.next");
                            default:
                                return this.$t("COMMON.done")
                        }
                    }
                }),
                components: {buttons: c["a"]},
                methods: Object(i["a"])({}, Object(s["mapActions"])("user", ["fetchFacebookProfile"]), {
                    cancel: function () {
                        this.$emit("cancel"), this._active = !1
                    }, radioSelected: function (t) {
                        t.disable || (this.selectedApp = t)
                    }, confirm: function () {
                        var t = this;
                        switch (this.status) {
                            case"apps":
                                return Object(u["N"])(this.selectedApp) ? void this.checkAdded().then(function () {
                                    t.$emit("confirm", t.selectedApp), t._active = !1
                                }, function () {
                                    return t.isAppSelected = !0
                                }) : this.toast("please select facebook app");
                            case"howTo":
                                if (this.howToPage === this.howTo.length - 1) return this.checkAdded().then(function () {
                                    t.$emit("confirm", t.selectedApp), t._active = !1
                                }, function () {
                                    return t.isWarningModalActive = !0
                                });
                                this.howToPage++
                        }
                    }, checkAdded: function () {
                        return o["d"].updateFbApp(this.campaignId || this.campaignIdProp, this.selectedApp)
                    }, getReason: function (t) {
                        var e = this.$t("COMMON.fb_app_modal_reason_no_tracker"),
                            n = this.$t("COMMON.fb_app_modal_reason_no_auth");
                        return {no_tracker: e, no_store: e, not_owner: n}[t]
                    }, getReasonHref: function (t) {
                        var e = this.$t("COMMON.fb_app_modal_reason_no_tracker_url"),
                            n = this.$t("COMMON.fb_app_modal_reason_no_auth_url");
                        return {no_tracker: e, no_store: e, not_owner: n}[t]
                    }
                }),
                mounted: function () {
                    var e = this;
                    this.$nextTick(function () {
                        var n = new r.a(".fb-app-modal-wrapper .copy-target", {text: t.always("525463574501100")});
                        n.on("success", function () {
                            return e.toast(e.$t("COMMON.copied"))
                        }), e.getBeforeDestroy$().subscribe(function () {
                            return n.destroy()
                        })
                    })
                }
            }
        }).call(this, n("b17e"))
    }, "172d": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    staticClass: "fb-app-modal",
                    class: {howTo: "howTo" === t.status},
                    attrs: {active: t._active, canCancel: ["escape", "x"], "has-modal-card": !0},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [n("confirm-modal", {
                    attrs: {
                        active: t.isWarningModalActive,
                        text: t.$t("COMMON.fb_app_modal_confirm_sharing"),
                        "cancel-text": t.$t("COMMON.yes"),
                        "confirm-text": t.$t("PROPOSAL.confirm_pixel_confirm")
                    }, on: {
                        "update:active": function (e) {
                            t.isWarningModalActive = e
                        }, cancel: t.cancel
                    }
                }), n("div", {ref: "wrapper", staticClass: "fb-app-modal-wrapper"}, [n("div", {
                    ref: "header",
                    staticClass: "fb-app-modal-header"
                }, [t._v(t._s(t.$t("COMMON.fb_app_modal_header")))]), n("div", {staticClass: "fb-app-modal-body"}, ["notAuth" === t.status ? n("div", {staticClass: "notAuth-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("COMMON.fb_app_modal_need_auth")))]), n("img", {
                    attrs: {src: "/img/proposal/ads_fb_login_btn.png"},
                    on: {
                        click: function (e) {
                            return e.stopPropagation(), t.fetchFacebookProfile()
                        }
                    }
                })]) : t._e(), "noApps" === t.status ? n("div", {staticClass: "noPages-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("COMMON.fb_app_modal_no_apps_found")))])]) : t._e(), "apps" === t.status ? n("div", {staticClass: "pages-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("COMMON.fb_app_modal_select_desc")))]), n("div", {staticClass: "page-elems-wrapper"}, t._l(t.apps, function (e) {
                    return n("div", {key: e.id, staticClass: "page-elem"}, [n("b-radio", {
                        staticClass: "custom-radio",
                        class: {selected: t.selectedApp.id === e.id},
                        attrs: {"native-value": e, disabled: e.disable},
                        on: {
                            input: function (n) {
                                return t.radioSelected(e)
                            }
                        },
                        model: {
                            value: t.selectedApp, callback: function (e) {
                                t.selectedApp = e
                            }, expression: "selectedApp"
                        }
                    }, [n("span", {staticClass: "page-elem-box"}, [n("img", {
                        staticClass: "page-elem-img",
                        attrs: {src: "/img/proposal/fb_logo_none.png"}
                    }), n("span", {staticClass: "page-name"}, [t._v(t._s(e.name))]), n("span", {staticClass: "page-url"}, [t._v(t._s("| " + e.id))])])]), e.disable ? n("div", {staticClass: "reason-area"}, [t._v("\n                            " + t._s(t.getReason(e.disableReason)) + "\n                            "), n("a", {
                        attrs: {
                            href: t.getReasonHref(e.disableReason),
                            target: "_blank"
                        }
                    }, [t._v(t._s(t.getReasonHref(e.disableReason)))])]) : t._e()], 1)
                }), 0)]) : t._e(), "howTo" === t.status ? n("div", {staticClass: "pixel-how-to"}, [n("span", {
                    staticClass: "pixel-title",
                    domProps: {innerHTML: t._s(t.$t(t.currentHowTo.title, {link: "https://business.facebook.com/settings/apps?business"}))}
                }), n("div", {staticClass: "pixel-how-to--img-container"}, [t.howToPage > 0 ? n("span", {
                    staticClass: "prev",
                    on: {
                        click: function (e) {
                            t.howToPage--
                        }
                    }
                }, [n("img", {attrs: {src: "/img/proposal/pixel_modal_prev.png"}})]) : t._e(), n("img", {attrs: {src: t.currentHowTo.image}}), t.lastHowTo ? t._e() : n("span", {
                    staticClass: "next",
                    on: {
                        click: function (e) {
                            t.howToPage++
                        }
                    }
                }, [n("img", {attrs: {src: "/img/proposal/pixel_modal_next.png"}})])]), n("div", {staticClass: "pages"}, t._l(t.howTo, function (e, i) {
                    return n("span", {
                        key: i, class: {active: i === t.howToPage}, on: {
                            click: function (e) {
                                t.howToPage = i
                            }
                        }
                    })
                }), 0)]) : t._e(), n("buttons", {
                    staticClass: "pixels-buttons",
                    class: t.status,
                    attrs: {
                        types: t.buttonTypes,
                        "cancel-text": t.buttonTypes.length > 1 ? t.$t("COMMON.cancel") : t.$t("COMMON.close"),
                        "confirm-text": t.confirmText
                    },
                    on: {cancel: t.cancel, confirm: t.confirm}
                })], 1)])], 1)
            }, a = [], r = n("1571"), s = r["a"], o = (n("b041"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, null, null);
        e["a"] = c.exports
    }, "193d": function (t, e, n) {
        "use strict";
        var i = n("e274"), a = n.n(i);
        a.a
    }, "1b50": function (t, e, n) {
    }, "1fcf": function (t, e, n) {
    }, 2029: function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    attrs: {active: t._active, canCancel: ["escape"], width: "450px"},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [n("div", {staticClass: "coupons-modal-wrapper"}, [n("div", {staticClass: "coupons-modal-header"}, [t._v("\n            " + t._s(this.$t("COUPON.add_modal_title")) + "\n        ")]), n("div", {staticClass: "coupons-modal-body"}, [n("div", {staticClass: "input-area"}, [n("b-input", {
                    attrs: {placeholder: t.$t("PROPOSAL.enter_adriel_coupon")},
                    nativeOn: {
                        keydown: function (e) {
                            return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.addCoupon(e)
                        }
                    },
                    model: {
                        value: t.couponCode, callback: function (e) {
                            t.couponCode = e
                        }, expression: "couponCode"
                    }
                }), n("img", {
                    attrs: {src: "/img/proposal/journey_add_btn.png"},
                    on: {click: t.addCoupon}
                })], 1), n("div", {staticClass: "coupons-modal-divider"}), n("div", {staticClass: "coupons-list"}, t._l(t.coupons, function (e) {
                    return n("coupon", {
                        key: e.id,
                        class: {active: t.isActive(e.id)},
                        attrs: {info: e},
                        nativeOn: {
                            click: function (n) {
                                return t.toggleActive(e)
                            }
                        }
                    }, [n("img", {attrs: {slot: "head", src: t.getImg(e)}, slot: "head"})])
                }), 1), n("adriel-buttons", {
                    on: {
                        cancel: function (e) {
                            t._active = !1
                        }, confirm: t.confirm
                    }
                })], 1)])])
            }, a = [], r = n("f876"), s = r["a"], o = (n("ba09"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, null, null);
        e["a"] = c.exports
    }, 2286: function (t, e, n) {
        var i = n("85e3"), a = Math.max;

        function r(t, e, n) {
            return e = a(void 0 === e ? t.length - 1 : e, 0), function () {
                var r = arguments, s = -1, o = a(r.length - e, 0), c = Array(o);
                while (++s < o) c[s] = r[e + s];
                s = -1;
                var u = Array(e + 1);
                while (++s < e) u[s] = r[s];
                return u[e] = n(c), i(t, this, u)
            }
        }

        t.exports = r
    }, "2b0e6": function (t, e, n) {
        "use strict";
        var i = n("8697"), a = n.n(i);
        a.a
    }, "2c47": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "on-off-wrapper"}, t._l(t.values, function (e, i) {
                    return n("span", {
                        key: t.nameGetter(e),
                        staticClass: "on-off-element",
                        class: "on-off-element-" + i
                    }, [n("img", {
                        staticClass: "main-img",
                        style: t.size(i),
                        attrs: {src: t.images[i]}
                    }), n("span", {staticClass: "on-off-range"}, [t._v(t._s(t.nameGetter(e)))]), n("span", {staticClass: "on-off-active-switch-wrapper"}, [n("switch-view", {
                        ref: "switch",
                        refInFor: !0,
                        attrs: {className: "on-off-active-switch", value: t.valueGetter(e)},
                        on: {
                            "update:value": function (e) {
                                return t.switchValueChanged(i, e)
                            }
                        },
                        nativeOn: {
                            click: function (t) {
                                t.stopPropagation()
                            }
                        }
                    })], 1)])
                }), 0)
            }, a = [], r = n("c771"), s = r["a"], o = (n("b151"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, null, null);
        e["a"] = c.exports
    }, "2d26": function (t, e, n) {
        "use strict";
        (function (t) {
            n("c5f6");
            var i = n("12e7"), a = n("fa7d");
            e["a"] = {
                props: {
                    className: String,
                    btnText: {type: String},
                    values: {type: Array, default: t.always([])},
                    deletable: {type: Boolean, default: !0},
                    editable: {type: Boolean, default: !0},
                    btnDisabled: {type: Boolean, default: !1},
                    btn: {type: Boolean, default: !0},
                    nameGetter: {type: Function, default: t.identity},
                    valueGetter: {type: Function, default: t.identity},
                    btnSrc: {type: String},
                    emptyText: {type: String},
                    specials: {type: Array, defualt: t.always([])},
                    currentEditIdx: Number
                }, components: {"edit-input": i["a"]}, mounted: function () {
                    var e = this;
                    this.$nextTick(function () {
                        t.compose(Object(a["Z"])("handleEdit"), t.pathOr({}, ["$refs", "input", e.currentEditIdx]))(e)
                    })
                }, methods: {
                    handleDelete: function (t) {
                        this.$emit("click:delete", t)
                    }, handleComplete: function (t, e) {
                        this.$emit("change:value", {value: t, idx: e})
                    }
                }
            }
        }).call(this, n("b17e"))
    }, "2f86": function (t, e, n) {
    }, "321c": function (t, e, n) {
        "use strict";
        var i = n("f53a"), a = n.n(i);
        a.a
    }, 3242: function (t, e, n) {
        "use strict";
        var i = n("0819"), a = n.n(i);
        a.a
    }, 3352: function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    staticClass: "appstore-connect-modal howTo",
                    attrs: {active: t._active, canCancel: ["escape", "x"], "has-modal-card": !0},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [n("confirm-modal", {
                    attrs: {
                        active: t.isWarningModalActive,
                        text: t.$t("PROPOSAL.confirm_appstore_connect"),
                        "cancel-text": t.$t("PROPOSAL.confirm_appstore_connect_exit"),
                        "confirm-text": t.$t("PROPOSAL.confirm_appstore_connect_stay")
                    }, on: {
                        "update:active": function (e) {
                            t.isWarningModalActive = e
                        }, cancel: t.cancel
                    }
                }), n("div", {ref: "wrapper", staticClass: "pixels-modal-wrapper"}, [n("div", {
                    ref: "header",
                    staticClass: "pixels-modal-header"
                }, [t._v(t._s(t.$t("PROPOSAL.connect_app_store")))]), n("div", {staticClass: "pixels-modal-body"}, [n("div", {staticClass: "pixel-how-to"}, [n("span", {
                    staticClass: "pixel-title",
                    domProps: {innerHTML: t._s(t.$t(t.currentHowTo.title))}
                }), n("div", {staticClass: "pixel-how-to--img-container"}, [t.howToPage > 0 ? n("button", {
                    staticClass: "prev",
                    on: {
                        click: function (e) {
                            t.howToPage--
                        }
                    }
                }, [n("span", {staticClass: "adriel-flex-center arrow-left"}, [n("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "chevron-left"
                    }
                })], 1)]) : t._e(), n("img", {attrs: {src: t.currentHowTo.image}}), t.lastHowTo ? t._e() : n("button", {
                    staticClass: "next",
                    on: {
                        click: function (e) {
                            t.howToPage++
                        }
                    }
                }, [n("span", {staticClass: "adriel-flex-center arrow-right"}, [n("b-icon", {
                    attrs: {
                        pack: "far",
                        icon: "chevron-right"
                    }
                })], 1)])]), n("div", {staticClass: "pages"}, t._l(t.howTo, function (e, i) {
                    return n("span", {
                        key: i, class: {active: i === t.howToPage}, on: {
                            click: function (e) {
                                t.howToPage = i
                            }
                        }
                    })
                }), 0)]), n("buttons", {
                    staticClass: "pixels-buttons howTo",
                    attrs: {
                        types: t.buttonTypes,
                        "cancel-text": t.buttonTypes.length > 1 ? t.$t("COMMON.cancel") : t.$t("COMMON.close"),
                        "confirm-text": t.confirmText
                    },
                    on: {cancel: t.cancel, confirm: t.confirm}
                })], 1)])], 1)
            }, a = [], r = n("be6e"), s = r["a"], o = (n("c4cd"), n("b22b"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, "9f44c910", null);
        e["a"] = c.exports
    }, "339d": function (t, e, n) {
        "use strict";
        var i = n("7c36"), a = n.n(i);
        a.a
    }, "34b0": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "locations"}, [n("card-view", {
                    attrs: {
                        label: t.$t("PROPOSAL.ap_card_title_search_keywords"),
                        className: t.className,
                        editable: t.editable,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        settingImgSrc: "/img/proposal/card_add_btn.png",
                        confirmLabel: "I'd like to change this setting",
                        settingLabel: t.$t("PROPOSAL.ap_card_edit_keywords")
                    }, on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [t.values.length ? n("div", {staticClass: "keywords-values"}, [n("span", {staticClass: "keywords-ment-guide"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_search_keywords_info")))]), n("tags", {
                    staticClass: "keywords-tags",
                    attrs: {
                        values: t.values,
                        hashTag: !0,
                        "max-width": "100%",
                        deleteImgSrc: "/img/proposal/profile_delete_icon_dark.png"
                    },
                    on: {
                        "update:remove": function (e) {
                            return t.$emit("update:remove", e)
                        }, click: function (e) {
                            return t.$emit("edit:start", e)
                        }
                    }
                })], 1) : n("div", {staticClass: "keywords-no-values"}, [n("no-content", {
                    on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                }, [t._v("\n                " + t._s(t.$t("PROPOSAL.ap_card_no_search_keywords")) + "\n                "), n("tags", {
                    staticClass: "keywords-tags",
                    attrs: {
                        values: [t.$t("PROPOSAL.ap_card_let_adriel")],
                        "max-width": "300",
                        removable: !1,
                        deleteImgSrc: "/img/proposal/profile_delete_icon_dark.png"
                    },
                    on: {
                        click: function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                })], 1)], 1)])], 1)
            }, a = [], r = n("3de4"), s = n("2d2d"), o = {mixins: [r["a"]], components: {tags: s["a"]}}, c = o,
            u = (n("193d"), n("4995"), n("321c"), n("2877")), l = Object(u["a"])(c, i, a, !1, null, "38f8b0d0", null);
        e["a"] = l.exports
    }, 3763: function (t, e, n) {
        "use strict";
        var i = n("0320"), a = n.n(i);
        a.a
    }, "38aa": function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("31a2");
            e["a"] = {
                created: function () {
                },
                props: {
                    values: {type: Array, default: t.always([])},
                    images: {type: Array, default: t.always([])},
                    nameGetter: {type: Function, default: t.identity},
                    valueGetter: {type: Function, default: t.identity}
                },
                methods: {
                    switchValueChanged: function (t, e) {
                        this.$emit("update:value", {idx: t, value: e})
                    }
                },
                components: {"switch-view": i["a"]}
            }
        }).call(this, n("b17e"))
    }, "3c9f": function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("2638"), a = n.n(i), r = n("774e"), s = n.n(r), o = (n("7f7f"), n("c082")), c = n("e811"),
                u = n("0d8e"), l = n("55de"), f = (n("ec2d"), n("4c6b"));
            e["a"] = {
                name: "InstagramFeed", mixins: [u["a"]], data: function () {
                    return {channel: "instagram"}
                }, props: {
                    value: {
                        type: Object, default: function () {
                            return {channel: "instagram", creativeType: "photo", creative: {media: "", description: ""}}
                        }
                    }
                }, computed: {
                    profileName: function () {
                        var e = t.path(["facebookProfile", "instagram"], this);
                        return e ? e.username || e.name : t.pathOr(this.$t("ONBOARDING.instagram_feed_id"), ["facebookProfile", "name"], this)
                    }, profileImg: function () {
                        var e = t.path(t.__, this), n = e(["facebookProfile", "instagram", "profile_pic"]),
                            i = e(["facebookProfile", "logo"]);
                        return n || i || "/img/onboarding/fb_profile.png"
                    }, checkUrlWrapperVisible: function () {
                        var t = this.editMode, e = this._type, n = void 0 === e ? "" : e, i = this.urlVisible,
                            a = void 0 !== i && i;
                        return !(!a || !t || "FORM" === n)
                    }
                }, render: function () {
                    var t = this, e = arguments[0], n = this.editMode, i = this._type, a = void 0 === i ? "" : i,
                        r = this.urlVisible, o = void 0 !== r && r, c = ["onboarding-ad-container", "instagram"];
                    return this.editable || c.push("not-editable"), e("div", {
                        class: this.classJoin(c),
                        ref: "elem"
                    }, [e("div", {class: "template-instagram-feed-container"}, [e("div", {class: "template-instagram-feed--body"}, [e("div", {
                        class: "top-header",
                        on: {
                            click: function () {
                                return t.$emit("click:profile")
                            }
                        }
                    }, [e("img", {
                        attrs: {
                            src: this.profileImg,
                            alt: "logo"
                        }
                    }), e("div", {class: "infos"}, [e("span", {class: "infos--id"}, [this.profileName]), e("span", {class: "infos--sponsor"}, ["Sponsored"])])]), n && e("div", {class: "feed-edit-box"}, [e("button", {
                        class: "adriel-flex-center",
                        on: {
                            click: function () {
                                var e = t.showImageTemplate();
                                e.$on("complete", t.mediaNext("carousel:add", "media").bind(t))
                            }
                        }
                    }, [e("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "plus"
                        }
                    }), this.$t("COMMON.ads_add_slide")])]), this.renderImage(n), e("div", {class: "middle-contents ".concat(n ? "editing" : "")}, [e("span", {class: "middle-contents--left"}, [this.renderCta(n)]), !n && e("b-icon", {
                        attrs: {
                            pack: "fal",
                            icon: "chevron-right"
                        }
                    }), this.checkUrlWrapperVisible && o && e("div", {class: "urls-wrapper"}, [this.renderUrl({
                        editMode: n,
                        type: a,
                        urlVisible: o
                    }), this.renderAppStoreUrl({
                        editMode: n,
                        type: a,
                        urlVisible: o
                    }), this.renderPlayStoreUrl({
                        editMode: n,
                        type: a,
                        urlVisible: o
                    }), this.renderOptionalUrl({
                        editMode: n,
                        type: a,
                        urlVisible: o
                    })]), e("hr")]), e("div", {class: "bottom-top"}, [s()({length: 4}).map(function (t, n) {
                        return e("img", {attrs: {src: "/img/onboarding/instagram_icon_".concat(++n, ".png")}})
                    })]), e("div", {class: "bottom"}, [e("span", {class: "bottom--id"}, [this.$t("ONBOARDING.instagram_feed_id")]), this.renderText(this.editMode)])])])])
                }, methods: {
                    renderImage: function (e) {
                        var n = this, i = this.$createElement, r = {
                            attrs: {
                                src: t.path(["_value", "creative", "media"], this),
                                meta: t.path(["_value", "creative", "meta", "media"], this),
                                type: "media",
                                editMode: e,
                                campaignId: this.campaignId,
                                isLoading: this.isLoading,
                                error: this._dirty,
                                channel: "instagram"
                            }, on: {
                                click: function () {
                                    return n.setMode(!0)
                                }, complete: this.mediaNext("single", "media").bind(this), remove: function () {
                                    return n.updateCreative("media", void 0)
                                }
                            }
                        };
                        return i(c["a"], a()([{}, r]))
                    }, renderText: function (t) {
                        var e = this, n = this.$createElement, i = this.getParamInfo("description"), a = i.value,
                            r = void 0 === a ? "" : a, s = i.error, c = i.reason, u = [];
                        return t ? n(o["a"], {
                            class: u,
                            attrs: {
                                type: "textarea",
                                value: r,
                                error: this._dirty && s ? this.$t("COMMON.ad_validation_".concat(c)) : "",
                                autofocus: this.autofocus,
                                placeholder: this.$t("COMMON.facebook_feed_description_placeholder")
                            },
                            style: {"margin-top": "10px"},
                            on: {input: this.updateCreative("description").bind(this)}
                        }) : (u.push("feed-description", "hover-edit", "just-word-break"), u = this.addClassIfEmpty(r, u), n("div", {
                            class: u,
                            on: {
                                click: function () {
                                    return e.setMode(!0)
                                }
                            }
                        }, [this.valueOrPlaceholder(r)]))
                    }, renderCta: function (e) {
                        var n = this, i = this.$createElement;
                        return i(l["a"], {
                            class: "cta-btn-edit-".concat(e),
                            attrs: {
                                isEditMode: e,
                                options: this.ctaOptions,
                                selected: t.path(["_value", "creative", "callToAction"], this)
                            },
                            ref: "cta",
                            nativeOn: {
                                click: function (t) {
                                    e || (t.stopPropagation(), n.setMode(!0), n.handleDropdownClick())
                                }
                            },
                            on: {input: this.updateCreative("callToAction").bind(this)}
                        })
                    }, renderUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "WEB" !== n) return null;
                        var s = [], c = this.getParamInfo("url"), u = c.value, l = c.error, d = c.reason;
                        return this._dirty && l && s.push("adriel-field-error"), r(o["a"], a()([{}, f["n"], {
                            class: s,
                            attrs: {
                                value: u,
                                error: this._dirty && l ? this.$t("COMMON.ad_validation_".concat(d)) : "",
                                placeholder: this.$t("COMMON.ad_landing_url_placeholder"),
                                headerText: "text"
                            },
                            ref: "url",
                            on: {input: this.updateCreative("url").bind(this)}
                        }]))
                    }, renderAppStoreUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "APP" !== n) return null;
                        var s = [], c = this.getParamInfo("appStoreUrl"), u = c.value, l = c.error, d = c.reason;
                        return this._dirty && l && s.push("adriel-field-error"), s.push("link-description-input"), r(o["a"], a()([{}, f["n"], {
                            class: s,
                            attrs: {
                                value: u,
                                error: this._dirty && l ? this.$t("COMMON.ad_validation_".concat(d)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_appstore_url")
                            },
                            ref: "appStoreUrl",
                            on: {input: this.updateCreative("appStoreUrl").bind(this)}
                        }]))
                    }, renderPlayStoreUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "APP" !== n) return null;
                        var s = [], c = this.getParamInfo("playStoreUrl"), u = c.value, l = c.error, d = c.reason;
                        return this._dirty && l && s.push("adriel-field-error"), s.push("link-description-input"), r(o["a"], a()([{}, f["n"], {
                            class: s,
                            attrs: {
                                value: u,
                                error: this._dirty && l ? this.$t("COMMON.ad_validation_".concat(d)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_playstore_url")
                            },
                            ref: "playStoreUrl",
                            on: {input: this.updateCreative("playStoreUrl").bind(this)}
                        }]))
                    }, renderOptionalUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "APP" !== n) return null;
                        var s = this.getParamInfo("url"), c = s.value;
                        return r(o["a"], a()([{}, f["n"], {
                            attrs: {
                                value: c,
                                placeholder: "".concat(this.$t("COMMON.ad_landing_url_placeholder"), " (").concat(this.$t("COMMON.optional"), ")")
                            }, ref: "OptionalUrl", on: {input: this.updateCreative("url").bind(this)}
                        }]))
                    }
                }
            }
        }).call(this, n("b17e"))
    }, "3d5c": function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("cebc"), a = (n("c5f6"), n("b311")), r = n.n(a), s = n("2f62"), o = n("dde5"), c = n("baba"),
                u = n("12e7"), l = n("6510"), f = n("fa7d");
            e["a"] = {
                created: function () {
                },
                props: {active: {type: Boolean, default: !1}, campaignIdProp: String | Number},
                data: function () {
                    return {
                        values: [],
                        top: void 0,
                        checked: [],
                        compFacebookProfile: {},
                        isWarning: !1,
                        howToPage: 0,
                        selectedPixel: {},
                        isPageSelected: !1,
                        isWarningModalActive: !1
                    }
                },
                computed: Object(i["a"])({}, Object(s["mapGetters"])("proposal", ["facebookProfile"]), Object(s["mapGetters"])("user", ["fbInfo"]), Object(s["mapGetters"])("campaign", ["campaignId"]), {
                    _active: {
                        get: function () {
                            return this.active
                        }, set: function (t) {
                            t || this.$emit("cancel"), this.$emit("update:active", t)
                        }
                    }, pages: function () {
                        var e = t.pathOr([], ["fbInfo", "pixels"], this);
                        return Object(f["N"])(this.selectedPixel) || (this.selectedPixel = e[0]), e
                    }, status: function () {
                        return Object(f["N"])(this.fbInfo) ? this.pages.length ? this.isPageSelected ? "howTo" : "pages" : "noPages" : "notAuth"
                    }, lastHowTo: function () {
                        return this.howToPage === t.dec(this.howTo.length)
                    }, buttonTypes: function () {
                        switch (this.status) {
                            case"notAuth":
                            case"noPages":
                                return ["cancel"];
                            case"howTo":
                                return ["confirm"];
                            default:
                                return ["cancel", "confirm"]
                        }
                    }, howTo: function () {
                        return this.$t("PROPOSAL.pixel_how_to")
                    }, currentHowTo: function () {
                        return this.howTo[this.howToPage]
                    }, confirmText: function () {
                        switch (this.status) {
                            case"pages":
                                return this.$t("COMMON.ok");
                            case"howTo":
                                return this.howToPage === this.howTo.length - 1 ? this.$t("COMMON.done") : this.$t("COMMON.next");
                            default:
                                return this.$t("COMMON.done")
                        }
                    }
                }),
                components: {"journey-editable-box": c["a"], "journey-edit-input": u["a"], buttons: l["a"]},
                methods: Object(i["a"])({}, Object(s["mapActions"])("user", ["fetchFacebookProfile"]), {
                    cancel: function () {
                        this.$emit("cancel"), this._active = !1
                    }, radioSelected: function (t) {
                        this.selectedPixel = t
                    }, confirm: function () {
                        var t = this;
                        switch (this.status) {
                            case"pages":
                                return Object(f["N"])(this.selectedPixel) ? void this.checkAdded().then(function (e) {
                                    t.$emit("confirm", t.selectedPixel), t._active = !1
                                }, function (e) {
                                    return t.isPageSelected = !0
                                }) : this.toast("please select pixel");
                            case"howTo":
                                if (this.howToPage === this.howTo.length - 1) return this.checkAdded().then(function () {
                                    t.$emit("confirm", t.selectedPixel), t._active = !1
                                }, function () {
                                    return t.isWarningModalActive = !0
                                });
                                this.howToPage++
                        }
                    }, checkAdded: function () {
                        return o["d"].updatePixel(this.campaignId || this.campaignIdProp, this.selectedPixel, this.fbInfo.longLifeToken)
                    }
                }),
                mounted: function () {
                    var e = this;
                    this.$nextTick(function () {
                        var n = new r.a(".pixels-modal-wrapper .copy-target", {text: t.always("525463574501100")});
                        n.on("success", function () {
                            return e.toast(e.$t("COMMON.copied"))
                        }), e.getBeforeDestroy$().subscribe(function () {
                            return n.destroy()
                        })
                    })
                }
            }
        }).call(this, n("b17e"))
    }, "3d93": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", [n("card-view", {
                    attrs: {
                        label: t.$t("PROPOSAL.ap_card_title_income"),
                        className: t.className,
                        editable: t.editable,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        confirmLabel: "I'd like to change this setting"
                    }, on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [n("div", {staticClass: "age-card-body"}, [n("on-off-view", {
                    attrs: {
                        editable: t.editable,
                        isEditing: t.isEditing,
                        values: t.values,
                        images: t.images,
                        "name-getter": function (e) {
                            return t.$t("PROPOSAL.ap_card_income_" + e.name)
                        },
                        "value-getter": function (t) {
                            return t.value
                        },
                        getImgSize: t.getImgSize
                    }, on: {
                        "update:value": function (e) {
                            return t.$emit("update:value", e)
                        }
                    }
                })], 1)])], 1)
            }, a = [], r = n("e6b3"), s = r["a"], o = (n("d6ac"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, null, null);
        e["a"] = c.exports
    }, "3de4": function (t, e, n) {
        "use strict";
        var i = n("19e8"), a = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "no-contents"}, [t.imgUrl ? n("img", {
                    staticClass: "no-contents-img",
                    attrs: {src: t.imgUrl}
                }) : t._e(), n("span", {staticClass: "profile-no-contents-text"}, [t._t("default")], 2), t.btnText ? n("button", {
                    staticClass: "profile-add-action-btn",
                    on: {
                        click: function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                }, [n("img", {attrs: {src: "/img/proposal/profile_plus_icon.png"}}), t._v("\n        " + t._s(t.btnText) + "\n    ")]) : t._e()])
            }, r = [], s = {props: ["btnText", "imgUrl"]}, o = s, c = (n("339d"), n("2877")),
            u = Object(c["a"])(o, a, r, !1, null, "20a25146", null), l = u.exports;
        e["a"] = {
            props: {
                editable: {type: Boolean, default: !1},
                className: {type: String, default: ""},
                values: {type: [Array, Object]},
                isEditing: {type: Boolean, default: !1},
                dimmed: {type: Boolean, default: !1},
                resetable: {type: Boolean, default: !1}
            }, methods: {
                reset: function () {
                    this.$emit("edit:end"), this.$emit("edit:reset")
                }, updateValue: function (t) {
                    this.$emit("update:value", t)
                }
            }, components: {"card-view": i["a"], "no-content": l}
        }
    }, "3e18": function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return a
        });
        var i = n("a6c5");

        function a(t, e) {
            return Object(i["a"])(t, e, 1)
        }
    }, "3ec5": function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("2638"), a = n.n(i), r = n("a4bb"), s = n.n(r), o = n("75fc"), c = n("cebc"), u = n("fa7d"),
                l = n("621e"), f = n("65d1"), d = n("9dd8"), h = n("6911"), p = n("3c9f"), b = n("89df"), v = n("dfd1"),
                _ = n("e391"), g = n("a772"), m = n("830c"), y = n("0d8e");
            n("10c7"), n("9915");
            e["a"] = {
                name: "AdPreview", props: Object(c["a"])({
                    ad: {
                        type: Object, default: function () {
                            return {}
                        }
                    },
                    type: {type: String, default: "web"},
                    ads: {
                        type: Array, default: function () {
                            return []
                        }
                    },
                    animateCheckbox: {type: Boolean, default: !1},
                    mode: {type: String, default: "preview"},
                    enablable: {type: Boolean, default: !0},
                    copyable: {type: Boolean, default: !1},
                    showPopover: {type: Boolean, default: !0},
                    removable: {type: Boolean, default: !1},
                    appleDisabled: {type: Boolean, default: !0}
                }, y["a"].props), created: function () {
                    var t = this;
                    this.$nextTick(function () {
                        t.watchSize()
                    })
                }, data: function () {
                    return {isPopoverActive: !1}
                }, render: function () {
                    var e = arguments[0], n = this.ad, i = t.propOr(!1, "enabled", n),
                        a = this["render".concat(Object(u["fb"])(this.adType(n)))] || t.always(""),
                        r = ["onboarding-ad-preview", this.className, n.channel];
                    return i && !0 === this.enablable && r.push("enabled"), e("div", {
                        class: r,
                        ref: "elem"
                    }, [this.getCover(), this.renderHeader(), a])
                }, methods: {
                    renderLeftTop: function () {
                        var e = this, n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                            i = this.$createElement;
                        if (!0 === this.enablable) {
                            var r = t.path(["$slots", "onOff", 0], this);
                            if (r) return r;
                            var s = t.propOr(!1, "enabled", n), o = {
                                attrs: {
                                    pack: s ? "fas" : "far",
                                    icon: s ? "check-square" : "square",
                                    class: ["icon ad-preview-".concat(s ? "enabled" : "disabled"), this.animateCheckbox ? "animate-color" : "", "preview-checkbox"].join(" ")
                                }, nativeOn: {
                                    click: function () {
                                        var t = e.comp;
                                        t && t.setEnabled(!s)
                                    }
                                }
                            };
                            return i("b-icon", a()([{}, o]))
                        }
                        var c = t.cond([[g["n"], t.always("facebook_logo")], [g["u"], t.always("instagram_logo")], [g["s"], t.always("google_logo")]])(this.ad);
                        return i("img", {attrs: {src: "/img/onboarding/".concat(c, ".png")}})
                    },
                    renderRightTop: function () {
                        var e = this, n = this.$createElement, i = t.pathOr(!1, ["meta", "isEditing"], this.ad);
                        return n("div", {class: "right-button-wrapper"}, [this.copyable && n("button", {
                            class: "adriel-flex-center copy-btn",
                            on: {
                                click: function () {
                                    return e.$emit("copy")
                                }
                            }
                        }, [this.$t("ONBOARDING.copy_from_previous")]), this.edtiable && n("button", {
                            class: "adriel-flex-center edit-btn",
                            on: {
                                click: function () {
                                    var t = e.comp;
                                    t && t.setMode(!i)
                                }
                            }
                        }, [i ? this.$t("ONBOARDING.ads_preview") : this.$t("ONBOARDING.ads_edit")]), this.renderPopover, this.removable && n("i", {
                            class: "fas fa-trash-alt remove-icon",
                            on: {
                                click: function () {
                                    return e.$emit("remove")
                                }
                            }
                        }), t.path(["$slots", "last-btn", 0], this)])
                    },
                    props: function () {
                        var e = this;
                        return {
                            ref: "comp",
                            attrs: Object(c["a"])({
                                value: this.ad,
                                type: this.type,
                                targets: [].concat(Object(o["a"])(this.targets), [this.className]).filter(t.identity),
                                includeEditButton: !1
                            }, t.pick(s()(y["a"].props), this)),
                            on: {
                                input: this.inputEmitter, "click:profile": function () {
                                    return e.$emit("click:profile")
                                }
                            }
                        }
                    },
                    adType: t.cond([[g["u"], t.always("instagramFeed")], [g["n"], t.always("facebookFeed")], [g["q"], t.always("googleDisplay")], [g["r"], t.always("googleSearch")], [g["t"], t.always("googleUniversalApp")], [g["m"], t.always("appleSearch")]]),
                    watchSize: function () {
                        var t = this, e = function () {
                            return t.$emit("resize")
                        };
                        this.elem && addResizeListener(this.elem, e), this.getBeforeDestroy$().subscribe(function () {
                            t.elem && removeResizeListener(t.elem, e)
                        })
                    },
                    renderHeader: function () {
                        var e = this.$createElement, n = this.ad;
                        return e("div", {class: "onboarding-ad-preview--header"}, [this.renderLeftTop(n), e("div", {class: "buttons-container"}, [t.path(["$slots", "header-btn"], this), this.renderRightTop()])])
                    },
                    getCover: function () {
                        var e = this, n = this.$createElement;
                        return Object(g["m"])(this.ad) && this.appleDisabled ? n(m["d"], {
                            nativeOn: {
                                click: function () {
                                    return e.$emit("click:appstore")
                                }
                            }
                        }, [n(m["e"], [n(m["b"], [n(m["c"], [this.$t("COMMON.connect_apple_app")]), n(m["a"], [this.$t("COMMON.connect_apple_app_start")])])])]) : t.path(["$slots", "cover", 0], this)
                    },
                    renderGoogleHeader: function () {
                        var t = this.$createElement;
                        return t("div", {class: "adriel-flex-center onboarding-ad-preview__google_header"}, [t("img", {attrs: {src: "/img/dashboard/header_google_logo.png"}}), t("span", [this.typeString])])
                    }
                }, computed: {
                    comp: t.path(["$refs", "comp"]), popover: function () {
                        var t = this.ad;
                        return Object(g["n"])(t) ? this.$t("ONBOARDING.facebook_popover") : Object(g["u"])(t) ? this.$t("ONBOARDING.instagram_popover") : Object(g["q"])(t) ? this.$t("ONBOARDING.google_display_popover") : Object(g["r"])(t) ? this.$t("ONBOARDING.google_search_popover") : Object(g["t"])(t) ? this.$t("ONBOARDING.google_universal_app_popover") : Object(g["m"])(t) ? this.$t("ONBOARDING.google_universal_app_popover") : void 0
                    }, elem: t.path(["$refs", "elem"]), className: function () {
                        return "preview-".concat(t.path(["ad", "creativeId"], this))
                    }, renderPopover: function () {
                        var t = this, e = this.$createElement, n = this.popover, i = this.showPopover;
                        if (n && i) return e("v-popover", {
                            attrs: {
                                trigger: "manual",
                                open: this.isPopoverActive,
                                popoverInnerClass: "ad-preview-popover",
                                popoverBaseClass: "ad-preview-popover-container",
                                autoHide: !1,
                                placement: "left-start"
                            }
                        }, [e("div", {class: "popover-content", slot: "popover"}, [e("b-icon", {
                            attrs: {
                                pack: "fas",
                                icon: "times"
                            }, class: "close", nativeOn: {
                                mousedown: function (t) {
                                    return t.stopPropagation()
                                }, click: function () {
                                    return t.isPopoverActive = !1
                                }
                            }
                        }), e("div", {
                            attrs: Object(c["a"])({}, {class: "center"}),
                            domProps: Object(c["a"])({}, {innerHTML: n})
                        })]), e("b-icon", {
                            attrs: {pack: "fas", icon: "question-circle"},
                            nativeOn: {
                                click: function () {
                                    return t.isPopoverActive = !t.isPopoverActive
                                }
                            }
                        })])
                    }, renderFacebookFeed: function () {
                        var t = this.$createElement,
                            e = Object(g["p"])(this.ad) ? t(b["a"], a()([{}, this.props()])) : t(v["a"], a()([{}, this.props()]));
                        return "preview" === this.mode ? t("div", {class: "onboarding-ad-preview--facebook"}, [t("img", {
                            class: "top head-img",
                            attrs: {src: "/img/onboarding/preview_facebook_head.png"}
                        }), e, t("img", {
                            class: "bottom bottom-img",
                            attrs: {src: "/img/onboarding/preview_facebook_bottom.png"}
                        })]) : e
                    }, renderInstagramFeed: function () {
                        var t = this.$createElement,
                            e = Object(g["v"])(this.ad) ? t(p["a"], a()([{}, this.props()])) : t(_["a"], a()([{}, this.props()]));
                        return "preview" === this.mode ? t("div", {class: "onboarding-ad-preview--instagram"}, [t("img", {
                            class: "top head-img",
                            attrs: {src: "/img/onboarding/preview_instagram_head.png"}
                        }), e, t("img", {
                            class: "top head-img",
                            attrs: {src: "/img/onboarding/preview_instagram_bottom.png"}
                        })]) : e
                    }, renderGoogleSearch: function () {
                        var t = this.$createElement, e = t(l["a"], a()([{}, this.props()]));
                        return "preview" === this.mode ? t("div", {class: "onboarding-ad-preview--google-search"}, [this.renderGoogleHeader(), t("img", {attrs: {src: "/img/onboarding/preview_google_search_head.png"}}), t("div", {class: "search-body"}, [e, t("img", {attrs: {src: "/img/onboarding/preview_google_search_bottom.png"}})])]) : e
                    }, renderGoogleDisplay: function () {
                        var t = this.$createElement, e = t(f["a"], a()([{}, this.props()]));
                        return "preview" === this.mode ? t("div", {class: "onboarding-ad-preview--google-display"}, [this.renderGoogleHeader(), t("img", {attrs: {src: "/img/onboarding/preview_google_display_head.png"}}), t("div", {class: "display-body"}, [e]), t("img", {attrs: {src: "/img/onboarding/preview_google_display_bottom.png"}})]) : e
                    }, renderGoogleUniversalApp: function () {
                        var t = this.$createElement, e = t(d["a"], a()([{}, this.props()]));
                        return "preview" === this.mode ? t("div", {class: "onboarding-ad-preview--google-uac"}, [this.renderGoogleHeader(), t("div", {class: "display-body"}, [e]), t("img", {
                            class: "onboarding-uac-footer-img",
                            attrs: {src: "/img/onboarding/preview_google_uac_bottom.png"}
                        })]) : e
                    }, renderAppleSearch: function () {
                        var t = this.$createElement, e = t(h["a"], a()([{}, this.props()]));
                        return "preview" === this.mode ? t("div", {class: "onboarding-ad-preview--google-search"}, [t("div", {
                            class: "search-body fullWidth",
                            style: "text-align: center"
                        }, [t("div", {class: "apple-search-header"}, [t("img", {attrs: {src: "/img/onboarding/apple-search.png"}})]), e, t("img", {attrs: {src: "/img/onboarding/preview_apple_search_bottom.png"}})])]) : e
                    }, typeString: function () {
                        return this.$t("COMMON.ad_types_type_".concat(this.adType(this.ad)))
                    }
                }
            }
        }).call(this, n("b17e"))
    }, 4179: function (t, e, n) {
        "use strict";
        var i = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "default-payment-method"}, [t.defaultPaymentMethod ? n("div", [n("payment-method", {
                attrs: {
                    method: t.defaultPaymentMethod,
                    default: !0,
                    deletable: t.deletable
                }
            })], 1) : t._e(), n("div", {staticClass: "default-payment-method-button-wrapper"}, [n("button", {
                staticClass: "mt-15 button has-text-color1 is-fullwidth",
                class: {disabled: !t.defaultPaymentMethod},
                on: {click: t.addPaymentMethod}
            }, [t._v("\n            " + t._s(t.defaultPaymentMethod ? t.$t("ACCOUNT_SETTING.change_payment") : t.$t("COMMON.add_payment_method")) + "\n        ")])]), n("modal-box", {
                attrs: {
                    active: t.addPaymentModal,
                    hasButtons: !1,
                    headerText: t.$t("ACCOUNT_SETTING.payment_add_method"),
                    confirmText: "OK",
                    cancelText: "CANCEL"
                }, on: {
                    "update:active": function (e) {
                        t.addPaymentModal = e
                    }
                }
            }, [n("add-payment-method", {
                on: {
                    refresh: function (e) {
                        return t.refresh()
                    }
                }
            })], 1)], 1)
        }, a = [], r = n("cebc"), s = n("ac5d"), o = n("2f62"), c = (n("1bc2"), n("51c6")), u = n("c10c"), l = {
            name: "DefaultPaymentMethod",
            components: {PaymentMethod: s["a"], ModalBox: c["a"], AddPaymentMethod: u["a"]},
            props: {deletable: {type: Boolean, default: !0}, fetchOnCreated: {type: Boolean, default: !0}},
            data: function () {
                return {addPaymentModal: !1}
            },
            methods: Object(r["a"])({}, Object(o["mapActions"])("user", ["fetchPaymentMethods"]), {
                refresh: function () {
                    this.fetchPaymentMethods()
                }, addPaymentMethod: function () {
                    this.isDemo ? this.showDemoAccountModal() : this.addPaymentModal = !0
                }
            }),
            computed: Object(r["a"])({}, Object(o["mapGetters"])("user", ["paymentMethods"]), Object(o["mapState"])("user", ["loadingPaymentMethods"]), {
                defaultPaymentMethod: function () {
                    if (this.paymentMethods && 0 !== this.paymentMethods.length) return this.paymentMethods[0]
                }
            }),
            created: function () {
                this.fetchOnCreated && this.refresh()
            }
        }, f = l, d = (n("d5c6"), n("2877")), h = Object(d["a"])(f, i, a, !1, null, "dde9eba0", null);
        e["a"] = h.exports
    }, 4460: function (t, e, n) {
        "use strict";
        var i = n("55b4"), a = n.n(i);
        a.a
    }, 4830: function (t, e, n) {
    }, 4995: function (t, e, n) {
        "use strict";
        var i = n("9ca3"), a = n.n(i);
        a.a
    }, "4a44": function (t, e, n) {
        "use strict";
        var i = n("daa4"), a = n.n(i);
        a.a
    }, "4a48": function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("f499"), a = n.n(i), r = (n("6762"), n("2fdb"), n("7f7f"), n("7514"), n("75fc")), s = n("cebc"),
                o = (n("c5f6"), n("62c8"), n("2f62")), c = n("dde5"), u = n("fa7d"), l = n("4c6b"),
                f = {name: "Link Click", type: "NON_CONVERSION", value: "link_click", analyticType: "NON_ANALYTIC"};
            e["a"] = {
                props: {
                    value: {
                        type: Array, defualt: function () {
                            return []
                        }
                    }, campaignId: String | Number
                },
                data: function () {
                    return {
                        urlObjective: "",
                        selectedObjective: "",
                        gaConversions: [],
                        pixelConversions: [],
                        tempUrl: ""
                    }
                },
                mounted: function () {
                    var t = this.intel, e = t.googleAnalytics, n = t.facebookPixel, i = t.facebookPage;
                    Object(u["N"])(e) && this.getGAConversionList(), (Object(u["N"])(n) || Object(u["N"])(i)) && this.getPixelConversionList()
                },
                computed: Object(s["a"])({}, Object(o["mapGetters"])("campaign", ["intel"]), {
                    selectedObjectives: {
                        get: t.prop("value"),
                        set: function (t) {
                            this.$emit("input", t)
                        }
                    }, urlObjectives: function () {
                        return this.selectedObjectives.filter(t.propEq("type", "LINK")).map(t.prop("value"))
                    }, selectedPixelConversions: function () {
                        return this.selectedObjectives.filter(function (t) {
                            var e = t.analyticType, n = void 0 === e ? "pixel" : e;
                            return "pixel" === n
                        })
                    }, selectedGAConversions: function () {
                        return this.selectedObjectives.filter(function (t) {
                            t.analyticType
                        })
                    }, conversions: function () {
                        var e = this, n = [];
                        return this.gaConversions && n.push.apply(n, Object(r["a"])(this.gaConversions.map(t.assoc("analyticType", "ga")).filter(function (n) {
                            var i = n.id;
                            return !e.selectedGAConversions.find(t.propEq("id", i))
                        }))), this.pixelConversions && n.push.apply(n, Object(r["a"])(this.pixelConversions.map(t.assoc("analyticType", "pixel")).filter(function (n) {
                            var i = n.name;
                            return !e.selectedPixelConversions.find(t.propEq("name", i))
                        }))), [].concat(n, [f])
                    }
                }),
                methods: {
                    getGAConversionList: function () {
                        var t = this;
                        c["d"].listConversions({campaignId: this.campaignId, type: "ga"}).then(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            return t.gaConversions = e
                        }, function () {
                            return t.gaConversions = []
                        })
                    }, getPixelConversionList: function () {
                        var t = this;
                        c["d"].listConversions({campaignId: this.campaignId, type: "pixel"}).then(function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            return t.pixelConversions = e
                        }, function () {
                            return t.pixelConversions = []
                        })
                    }, appendAnalyticType: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, e = t.analyticType;
                        switch (e) {
                            case"ga":
                                return " (Google)";
                            case"pixel":
                                return " (Facebook)";
                            default:
                                return ""
                        }
                    }, addObjective: function (e) {
                        this.selectedObjective = JSON.parse(t.path(["target", "value"], e)), this.selectedObjective && (this.selectedObjectives.push(Object(s["a"])({}, this.selectedObjective, {
                            type: "EVENT",
                            value: this.selectedObjective.name
                        })), this.selectedObjectives = Object(r["a"])(this.selectedObjectives)), this.selectedObjective = ""
                    }, addUrlObjective: function () {
                        this.urlObjective = this.tempUrl, this.urlObjective && !this.urlObjectives.includes(this.urlObjective) && Object(u["H"])(this.urlObjective) && (this.selectedObjectives.push({
                            type: "LINK",
                            name: "ViewContent",
                            value: this.urlObjective
                        }), this.selectedObjectives = this.selectedObjectives.slice(0), this.urlObjective = "")
                    }
                },
                render: function () {
                    var t = this, e = arguments[0];
                    return e("div", {class: "onboarding-conversions-container"}, [e("select", {
                        class: "select-conversions",
                        domProps: {value: this.selectedObjective},
                        on: {change: this.addObjective}
                    }, [e("option", {attrs: {value: ""}}, [this.$t("PROPOSAL.conversions_modal_title")]), this.conversions.map(function (n) {
                        return e("option", {domProps: {value: a()(n)}}, [e("template", [n.name ? n.name : n]), t.appendAnalyticType(n)])
                    })]), e("div", {class: "inputs-modal-adder"}, [e("b-autocomplete", {
                        attrs: {
                            value: this.urlObjective,
                            name: "url",
                            placeholder: this.$t("PROPOSAL.conversions_modal_add_url"),
                            "clear-on-select": !0
                        }, ref: "input", on: {
                            input: function (e) {
                                return t.tempUrl = e
                            }, mousedown: function (t) {
                                return t.stopPropagation()
                            }
                        }, nativeOn: {
                            keyup: function (e) {
                                return e.keyCode === l["q"].enter && t.addUrlObjective()
                            }
                        }
                    }), e("img", {
                        attrs: {src: "/img/proposal/journey_add_btn.png"},
                        on: {
                            click: this.addUrlObjective, mousedown: function (t) {
                                t.stopPropagation()
                            }
                        }
                    })]), this.urlObjective && !Object(u["H"])(this.urlObjective) && e("div", {class: "has-text-color34"}, [this.$t("COMMON.no_url")]), this.$slots.tags])
                }
            }
        }).call(this, n("b17e"))
    }, "4a68": function (t, e) {
        var n = "Expected a function";

        function i(t, e, i) {
            if ("function" != typeof t) throw new TypeError(n);
            return setTimeout(function () {
                t.apply(void 0, i)
            }, e)
        }

        t.exports = i
    }, "4ace": function (t, e, n) {
        "use strict";
        var i = n("f9d1"), a = n.n(i);
        a.a
    }, "4adc": function (t, e, n) {
        "use strict";
        var i = n("2f86"), a = n.n(i);
        a.a
    }, "4d91d": function (t, e, n) {
    }, "4d96": function (t, e, n) {
    }, "4daa": function (t, e, n) {
    }, 5024: function (t, e, n) {
    }, "52a1": function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("cebc"), a = (n("7514"), n("2f62")), r = (n("b047"), n("e380"), n("fa7d")), s = n("6e77"),
                o = n("a748"), c = n("17f5"), u = n("c4cc"), l = n("5670"), f = n("ebb6"), d = n("3e18"), h = n("9f2d"),
                p = n("1a2d"), b = n("4b59"), v = n("baba"), _ = n("12e7"), g = (n("dde5"), t.max(0));
            e["a"] = {
                created: function () {
                    this.compSelected = t.compose(t.defaultTo({}), t.find(t.propEq("type", "googleAnalytics")), t.pathOr([], ["selected", "values"]))(this)
                },
                watch: {
                    properties: function () {
                        this.alignVertialCenter()
                    }
                },
                props: {
                    active: {type: Boolean, default: !1},
                    headerText: String,
                    selected: {type: Object, default: t.always({})}
                },
                data: function () {
                    return {compSelected: void 0}
                },
                computed: Object(i["a"])({}, Object(a["mapGetters"])("user", ["gaInfo"]), Object(a["mapGetters"])("campaign", ["campaignId"]), {
                    _active: {
                        get: function () {
                            return this.active
                        }, set: function (t) {
                            this.$emit("update:active", t)
                        }
                    }, compSelectedId: {
                        get: function () {
                            return t.path(["compSelected", "id"], this)
                        }, set: function (t) {
                        }
                    }, properties: function () {
                        var e = this.gaInfo.properties || [];
                        return t.isEmpty(e) && Object(r["N"])(this.compSelected) && this.compSelected.id && e.push(this.compSelected), e
                    }
                }),
                components: {"editable-box": v["a"], "editable-input": _["a"]},
                methods: {
                    setDrag: function () {
                        var e = this, n = this.$refs.wrapper, i = Object(s["a"])(n, "mousedown"),
                            a = Object(s["a"])(document, "mousemove"), r = Object(s["a"])(document, "mouseup"),
                            o = i.pipe(Object(u["a"])(function (t) {
                                return t.preventDefault()
                            }), Object(l["a"])(t.propEq("which", 1)), Object(f["a"])(function (t) {
                                t.target;
                                var e = t.clientX, i = t.clientY, a = n.getClientRects()[0], r = a.left, s = a.top;
                                return {offsetX: e - r, offsetY: i - s}
                            }), Object(d["a"])(function (t) {
                                var e = t.offsetX, n = t.offsetY;
                                return a.pipe(Object(f["a"])(function (t) {
                                    var i = t.clientX, a = t.clientY;
                                    return {left: i - e + "px", top: a - n + "px"}
                                }), Object(h["a"])(r))
                            }));
                        this.$subscribeTo(o, function (t) {
                            var n = t.top, i = t.left;
                            e.top = n, e.left = i
                        })
                    }, confirm: function () {
                        var e = this;
                        if (t.equals(this.compSelected, this.selected) || !this.compSelected.accountId) this._active = !1, this.$emit("cancel"); else {
                            var n = this.compSelected, i = n.accountId, a = n.id,
                                s = gapi.auth2.getAuthInstance().currentUser.get(), o = s.getAuthResponse(),
                                c = o.access_token, u = o.id_token, l = {
                                    accountId: this.compSelected.accountId,
                                    properties: [this.compSelected],
                                    emails: ["analytics2-4@seventh-country-251404.iam.gserviceaccount.com", "adrielanalytics2@gmail.com"],
                                    access_token: c,
                                    id_token: u
                                };
                            Object(r["B"])({accountId: i, propertyId: a}).then(function () {
                                return e.$emit("confirm", l)
                            }, function () {
                                e.toast("error occured"), e._active = !1, e.$emit("cancel")
                            })
                        }
                    }, cancel: function () {
                        this._active = !1, this.$emit("cancel")
                    }, radioSelected: function (t) {
                        this.compSelected = t
                    }, alignVertialCenter: function () {
                        var e = t.divide(t.__, 2), n = g(e(window.innerHeight) - e(Object(r["A"])(this.$refs.wrapper)));
                        this.top = "".concat(n, "px")
                    }
                },
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        t.alignVertialCenter(), t.setDrag()
                    })
                },
                subscriptions: function () {
                    var e = this,
                        n = Object(o["a"])(Object(c["a"])(""), Object(s["a"])(window, "resize")).pipe(Object(p["a"])(200), Object(f["a"])(function () {
                            return .8 * window.innerHeight
                        }), Object(f["a"])(t.flip(t.subtract)(200)), Object(b["a"])(), Object(h["a"])(this.getBeforeDestroy$()));
                    return n.pipe(Object(p["a"])(300)).subscribe(function () {
                        e.alignVertialCenter()
                    }), {maxHeight: n}
                }
            }
        }).call(this, n("b17e"))
    }, "55b4": function (t, e, n) {
    }, 5698: function (t, e, n) {
        "use strict";
        var i = function (t, e) {
            return t < e ? -1 : t > e ? 1 : t >= e ? 0 : NaN
        }, a = function (t) {
            return 1 === t.length && (t = r(t)), {
                left: function (e, n, i, a) {
                    null == i && (i = 0), null == a && (a = e.length);
                    while (i < a) {
                        var r = i + a >>> 1;
                        t(e[r], n) < 0 ? i = r + 1 : a = r
                    }
                    return i
                }, right: function (e, n, i, a) {
                    null == i && (i = 0), null == a && (a = e.length);
                    while (i < a) {
                        var r = i + a >>> 1;
                        t(e[r], n) > 0 ? a = r : i = r + 1
                    }
                    return i
                }
            }
        };

        function r(t) {
            return function (e, n) {
                return i(t(e), n)
            }
        }

        var s = a(i), o = s.right, c = (s.left, o);
        var u = Array.prototype, l = (u.slice, u.map, Math.sqrt(50)), f = Math.sqrt(10), d = Math.sqrt(2),
            h = function (t, e, n) {
                var i, a, r, s, o = -1;
                if (e = +e, t = +t, n = +n, t === e && n > 0) return [t];
                if ((i = e < t) && (a = t, t = e, e = a), 0 === (s = p(t, e, n)) || !isFinite(s)) return [];
                if (s > 0) {
                    t = Math.ceil(t / s), e = Math.floor(e / s), r = new Array(a = Math.ceil(e - t + 1));
                    while (++o < a) r[o] = (t + o) * s
                } else {
                    t = Math.floor(t * s), e = Math.ceil(e * s), r = new Array(a = Math.ceil(t - e + 1));
                    while (++o < a) r[o] = (t - o) / s
                }
                return i && r.reverse(), r
            };

        function p(t, e, n) {
            var i = (e - t) / Math.max(0, n), a = Math.floor(Math.log(i) / Math.LN10), r = i / Math.pow(10, a);
            return a >= 0 ? (r >= l ? 10 : r >= f ? 5 : r >= d ? 2 : 1) * Math.pow(10, a) : -Math.pow(10, -a) / (r >= l ? 10 : r >= f ? 5 : r >= d ? 2 : 1)
        }

        function b(t, e, n) {
            var i = Math.abs(e - t) / Math.max(0, n), a = Math.pow(10, Math.floor(Math.log(i) / Math.LN10)), r = i / a;
            return r >= l ? a *= 10 : r >= f ? a *= 5 : r >= d && (a *= 2), e < t ? -a : a
        }

        var v = function (t) {
            var e, n, i, a = t.length, r = -1, s = 0;
            while (++r < a) s += t[r].length;
            n = new Array(s);
            while (--a >= 0) {
                i = t[a], e = i.length;
                while (--e >= 0) n[--s] = i[e]
            }
            return n
        };
        Array.prototype.slice;
        var _ = {
            value: function () {
            }
        };

        function g() {
            for (var t, e = 0, n = arguments.length, i = {}; e < n; ++e) {
                if (!(t = arguments[e] + "") || t in i) throw new Error("illegal type: " + t);
                i[t] = []
            }
            return new m(i)
        }

        function m(t) {
            this._ = t
        }

        function y(t, e) {
            return t.trim().split(/^|\s+/).map(function (t) {
                var n = "", i = t.indexOf(".");
                if (i >= 0 && (n = t.slice(i + 1), t = t.slice(0, i)), t && !e.hasOwnProperty(t)) throw new Error("unknown type: " + t);
                return {type: t, name: n}
            })
        }

        function x(t, e) {
            for (var n, i = 0, a = t.length; i < a; ++i) if ((n = t[i]).name === e) return n.value
        }

        function w(t, e, n) {
            for (var i = 0, a = t.length; i < a; ++i) if (t[i].name === e) {
                t[i] = _, t = t.slice(0, i).concat(t.slice(i + 1));
                break
            }
            return null != n && t.push({name: e, value: n}), t
        }

        m.prototype = g.prototype = {
            constructor: m, on: function (t, e) {
                var n, i = this._, a = y(t + "", i), r = -1, s = a.length;
                if (!(arguments.length < 2)) {
                    if (null != e && "function" !== typeof e) throw new Error("invalid callback: " + e);
                    while (++r < s) if (n = (t = a[r]).type) i[n] = w(i[n], t.name, e); else if (null == e) for (n in i) i[n] = w(i[n], t.name, null);
                    return this
                }
                while (++r < s) if ((n = (t = a[r]).type) && (n = x(i[n], t.name))) return n
            }, copy: function () {
                var t = {}, e = this._;
                for (var n in e) t[n] = e[n].slice();
                return new m(t)
            }, call: function (t, e) {
                if ((n = arguments.length - 2) > 0) for (var n, i, a = new Array(n), r = 0; r < n; ++r) a[r] = arguments[r + 2];
                if (!this._.hasOwnProperty(t)) throw new Error("unknown type: " + t);
                for (i = this._[t], r = 0, n = i.length; r < n; ++r) i[r].value.apply(e, a)
            }, apply: function (t, e, n) {
                if (!this._.hasOwnProperty(t)) throw new Error("unknown type: " + t);
                for (var i = this._[t], a = 0, r = i.length; a < r; ++a) i[a].value.apply(e, n)
            }
        };
        var O = g, M = "http://www.w3.org/1999/xhtml", C = {
            svg: "http://www.w3.org/2000/svg",
            xhtml: M,
            xlink: "http://www.w3.org/1999/xlink",
            xml: "http://www.w3.org/XML/1998/namespace",
            xmlns: "http://www.w3.org/2000/xmlns/"
        }, k = function (t) {
            var e = t += "", n = e.indexOf(":");
            return n >= 0 && "xmlns" !== (e = t.slice(0, n)) && (t = t.slice(n + 1)), C.hasOwnProperty(e) ? {
                space: C[e],
                local: t
            } : t
        };

        function A(t) {
            return function () {
                var e = this.ownerDocument, n = this.namespaceURI;
                return n === M && e.documentElement.namespaceURI === M ? e.createElement(t) : e.createElementNS(n, t)
            }
        }

        function T(t) {
            return function () {
                return this.ownerDocument.createElementNS(t.space, t.local)
            }
        }

        var N = function (t) {
            var e = k(t);
            return (e.local ? T : A)(e)
        };

        function P() {
        }

        var S = function (t) {
            return null == t ? P : function () {
                return this.querySelector(t)
            }
        }, $ = function (t) {
            "function" !== typeof t && (t = S(t));
            for (var e = this._groups, n = e.length, i = new Array(n), a = 0; a < n; ++a) for (var r, s, o = e[a], c = o.length, u = i[a] = new Array(c), l = 0; l < c; ++l) (r = o[l]) && (s = t.call(r, r.__data__, l, o)) && ("__data__" in r && (s.__data__ = r.__data__), u[l] = s);
            return new fe(i, this._parents)
        };

        function E() {
            return []
        }

        var j = function (t) {
            return null == t ? E : function () {
                return this.querySelectorAll(t)
            }
        }, I = function (t) {
            "function" !== typeof t && (t = j(t));
            for (var e = this._groups, n = e.length, i = [], a = [], r = 0; r < n; ++r) for (var s, o = e[r], c = o.length, u = 0; u < c; ++u) (s = o[u]) && (i.push(t.call(s, s.__data__, u, o)), a.push(s));
            return new fe(i, a)
        }, L = function (t) {
            return function () {
                return this.matches(t)
            }
        }, U = function (t) {
            "function" !== typeof t && (t = L(t));
            for (var e = this._groups, n = e.length, i = new Array(n), a = 0; a < n; ++a) for (var r, s = e[a], o = s.length, c = i[a] = [], u = 0; u < o; ++u) (r = s[u]) && t.call(r, r.__data__, u, s) && c.push(r);
            return new fe(i, this._parents)
        }, R = function (t) {
            return new Array(t.length)
        }, D = function () {
            return new fe(this._enter || this._groups.map(R), this._parents)
        };

        function V(t, e) {
            this.ownerDocument = t.ownerDocument, this.namespaceURI = t.namespaceURI, this._next = null, this._parent = t, this.__data__ = e
        }

        V.prototype = {
            constructor: V, appendChild: function (t) {
                return this._parent.insertBefore(t, this._next)
            }, insertBefore: function (t, e) {
                return this._parent.insertBefore(t, e)
            }, querySelector: function (t) {
                return this._parent.querySelector(t)
            }, querySelectorAll: function (t) {
                return this._parent.querySelectorAll(t)
            }
        };
        var B = function (t) {
            return function () {
                return t
            }
        }, F = "$";

        function H(t, e, n, i, a, r) {
            for (var s, o = 0, c = e.length, u = r.length; o < u; ++o) (s = e[o]) ? (s.__data__ = r[o], i[o] = s) : n[o] = new V(t, r[o]);
            for (; o < c; ++o) (s = e[o]) && (a[o] = s)
        }

        function q(t, e, n, i, a, r, s) {
            var o, c, u, l = {}, f = e.length, d = r.length, h = new Array(f);
            for (o = 0; o < f; ++o) (c = e[o]) && (h[o] = u = F + s.call(c, c.__data__, o, e), u in l ? a[o] = c : l[u] = c);
            for (o = 0; o < d; ++o) u = F + s.call(t, r[o], o, r), (c = l[u]) ? (i[o] = c, c.__data__ = r[o], l[u] = null) : n[o] = new V(t, r[o]);
            for (o = 0; o < f; ++o) (c = e[o]) && l[h[o]] === c && (a[o] = c)
        }

        var G = function (t, e) {
            if (!t) return h = new Array(this.size()), u = -1, this.each(function (t) {
                h[++u] = t
            }), h;
            var n = e ? q : H, i = this._parents, a = this._groups;
            "function" !== typeof t && (t = B(t));
            for (var r = a.length, s = new Array(r), o = new Array(r), c = new Array(r), u = 0; u < r; ++u) {
                var l = i[u], f = a[u], d = f.length, h = t.call(l, l && l.__data__, u, i), p = h.length,
                    b = o[u] = new Array(p), v = s[u] = new Array(p), _ = c[u] = new Array(d);
                n(l, f, b, v, _, h, e);
                for (var g, m, y = 0, x = 0; y < p; ++y) if (g = b[y]) {
                    y >= x && (x = y + 1);
                    while (!(m = v[x]) && ++x < p) ;
                    g._next = m || null
                }
            }
            return s = new fe(s, i), s._enter = o, s._exit = c, s
        }, z = function () {
            return new fe(this._exit || this._groups.map(R), this._parents)
        }, Y = function (t, e, n) {
            var i = this.enter(), a = this, r = this.exit();
            return i = "function" === typeof t ? t(i) : i.append(t + ""), null != e && (a = e(a)), null == n ? r.remove() : n(r), i && a ? i.merge(a).order() : a
        }, W = function (t) {
            for (var e = this._groups, n = t._groups, i = e.length, a = n.length, r = Math.min(i, a), s = new Array(i), o = 0; o < r; ++o) for (var c, u = e[o], l = n[o], f = u.length, d = s[o] = new Array(f), h = 0; h < f; ++h) (c = u[h] || l[h]) && (d[h] = c);
            for (; o < i; ++o) s[o] = e[o];
            return new fe(s, this._parents)
        }, X = function () {
            for (var t = this._groups, e = -1, n = t.length; ++e < n;) for (var i, a = t[e], r = a.length - 1, s = a[r]; --r >= 0;) (i = a[r]) && (s && 4 ^ i.compareDocumentPosition(s) && s.parentNode.insertBefore(i, s), s = i);
            return this
        }, K = function (t) {
            function e(e, n) {
                return e && n ? t(e.__data__, n.__data__) : !e - !n
            }

            t || (t = Z);
            for (var n = this._groups, i = n.length, a = new Array(i), r = 0; r < i; ++r) {
                for (var s, o = n[r], c = o.length, u = a[r] = new Array(c), l = 0; l < c; ++l) (s = o[l]) && (u[l] = s);
                u.sort(e)
            }
            return new fe(a, this._parents).order()
        };

        function Z(t, e) {
            return t < e ? -1 : t > e ? 1 : t >= e ? 0 : NaN
        }

        var J = function () {
            var t = arguments[0];
            return arguments[0] = this, t.apply(null, arguments), this
        }, Q = function () {
            var t = new Array(this.size()), e = -1;
            return this.each(function () {
                t[++e] = this
            }), t
        }, tt = function () {
            for (var t = this._groups, e = 0, n = t.length; e < n; ++e) for (var i = t[e], a = 0, r = i.length; a < r; ++a) {
                var s = i[a];
                if (s) return s
            }
            return null
        }, et = function () {
            var t = 0;
            return this.each(function () {
                ++t
            }), t
        }, nt = function () {
            return !this.node()
        }, it = function (t) {
            for (var e = this._groups, n = 0, i = e.length; n < i; ++n) for (var a, r = e[n], s = 0, o = r.length; s < o; ++s) (a = r[s]) && t.call(a, a.__data__, s, r);
            return this
        };

        function at(t) {
            return function () {
                this.removeAttribute(t)
            }
        }

        function rt(t) {
            return function () {
                this.removeAttributeNS(t.space, t.local)
            }
        }

        function st(t, e) {
            return function () {
                this.setAttribute(t, e)
            }
        }

        function ot(t, e) {
            return function () {
                this.setAttributeNS(t.space, t.local, e)
            }
        }

        function ct(t, e) {
            return function () {
                var n = e.apply(this, arguments);
                null == n ? this.removeAttribute(t) : this.setAttribute(t, n)
            }
        }

        function ut(t, e) {
            return function () {
                var n = e.apply(this, arguments);
                null == n ? this.removeAttributeNS(t.space, t.local) : this.setAttributeNS(t.space, t.local, n)
            }
        }

        var lt = function (t, e) {
            var n = k(t);
            if (arguments.length < 2) {
                var i = this.node();
                return n.local ? i.getAttributeNS(n.space, n.local) : i.getAttribute(n)
            }
            return this.each((null == e ? n.local ? rt : at : "function" === typeof e ? n.local ? ut : ct : n.local ? ot : st)(n, e))
        }, ft = function (t) {
            return t.ownerDocument && t.ownerDocument.defaultView || t.document && t || t.defaultView
        };

        function dt(t) {
            return function () {
                this.style.removeProperty(t)
            }
        }

        function ht(t, e, n) {
            return function () {
                this.style.setProperty(t, e, n)
            }
        }

        function pt(t, e, n) {
            return function () {
                var i = e.apply(this, arguments);
                null == i ? this.style.removeProperty(t) : this.style.setProperty(t, i, n)
            }
        }

        var bt = function (t, e, n) {
            return arguments.length > 1 ? this.each((null == e ? dt : "function" === typeof e ? pt : ht)(t, e, null == n ? "" : n)) : vt(this.node(), t)
        };

        function vt(t, e) {
            return t.style.getPropertyValue(e) || ft(t).getComputedStyle(t, null).getPropertyValue(e)
        }

        function _t(t) {
            return function () {
                delete this[t]
            }
        }

        function gt(t, e) {
            return function () {
                this[t] = e
            }
        }

        function mt(t, e) {
            return function () {
                var n = e.apply(this, arguments);
                null == n ? delete this[t] : this[t] = n
            }
        }

        var yt = function (t, e) {
            return arguments.length > 1 ? this.each((null == e ? _t : "function" === typeof e ? mt : gt)(t, e)) : this.node()[t]
        };

        function xt(t) {
            return t.trim().split(/^|\s+/)
        }

        function wt(t) {
            return t.classList || new Ot(t)
        }

        function Ot(t) {
            this._node = t, this._names = xt(t.getAttribute("class") || "")
        }

        function Mt(t, e) {
            var n = wt(t), i = -1, a = e.length;
            while (++i < a) n.add(e[i])
        }

        function Ct(t, e) {
            var n = wt(t), i = -1, a = e.length;
            while (++i < a) n.remove(e[i])
        }

        function kt(t) {
            return function () {
                Mt(this, t)
            }
        }

        function At(t) {
            return function () {
                Ct(this, t)
            }
        }

        function Tt(t, e) {
            return function () {
                (e.apply(this, arguments) ? Mt : Ct)(this, t)
            }
        }

        Ot.prototype = {
            add: function (t) {
                var e = this._names.indexOf(t);
                e < 0 && (this._names.push(t), this._node.setAttribute("class", this._names.join(" ")))
            }, remove: function (t) {
                var e = this._names.indexOf(t);
                e >= 0 && (this._names.splice(e, 1), this._node.setAttribute("class", this._names.join(" ")))
            }, contains: function (t) {
                return this._names.indexOf(t) >= 0
            }
        };
        var Nt = function (t, e) {
            var n = xt(t + "");
            if (arguments.length < 2) {
                var i = wt(this.node()), a = -1, r = n.length;
                while (++a < r) if (!i.contains(n[a])) return !1;
                return !0
            }
            return this.each(("function" === typeof e ? Tt : e ? kt : At)(n, e))
        };

        function Pt() {
            this.textContent = ""
        }

        function St(t) {
            return function () {
                this.textContent = t
            }
        }

        function $t(t) {
            return function () {
                var e = t.apply(this, arguments);
                this.textContent = null == e ? "" : e
            }
        }

        var Et = function (t) {
            return arguments.length ? this.each(null == t ? Pt : ("function" === typeof t ? $t : St)(t)) : this.node().textContent
        };

        function jt() {
            this.innerHTML = ""
        }

        function It(t) {
            return function () {
                this.innerHTML = t
            }
        }

        function Lt(t) {
            return function () {
                var e = t.apply(this, arguments);
                this.innerHTML = null == e ? "" : e
            }
        }

        var Ut = function (t) {
            return arguments.length ? this.each(null == t ? jt : ("function" === typeof t ? Lt : It)(t)) : this.node().innerHTML
        };

        function Rt() {
            this.nextSibling && this.parentNode.appendChild(this)
        }

        var Dt = function () {
            return this.each(Rt)
        };

        function Vt() {
            this.previousSibling && this.parentNode.insertBefore(this, this.parentNode.firstChild)
        }

        var Bt = function () {
            return this.each(Vt)
        }, Ft = function (t) {
            var e = "function" === typeof t ? t : N(t);
            return this.select(function () {
                return this.appendChild(e.apply(this, arguments))
            })
        };

        function Ht() {
            return null
        }

        var qt = function (t, e) {
            var n = "function" === typeof t ? t : N(t), i = null == e ? Ht : "function" === typeof e ? e : S(e);
            return this.select(function () {
                return this.insertBefore(n.apply(this, arguments), i.apply(this, arguments) || null)
            })
        };

        function Gt() {
            var t = this.parentNode;
            t && t.removeChild(this)
        }

        var zt = function () {
            return this.each(Gt)
        };

        function Yt() {
            return this.parentNode.insertBefore(this.cloneNode(!1), this.nextSibling)
        }

        function Wt() {
            return this.parentNode.insertBefore(this.cloneNode(!0), this.nextSibling)
        }

        var Xt = function (t) {
            return this.select(t ? Wt : Yt)
        }, Kt = function (t) {
            return arguments.length ? this.property("__data__", t) : this.node().__data__
        }, Zt = {}, Jt = null;
        if ("undefined" !== typeof document) {
            var Qt = document.documentElement;
            "onmouseenter" in Qt || (Zt = {mouseenter: "mouseover", mouseleave: "mouseout"})
        }

        function te(t, e, n) {
            return t = ee(t, e, n), function (e) {
                var n = e.relatedTarget;
                n && (n === this || 8 & n.compareDocumentPosition(this)) || t.call(this, e)
            }
        }

        function ee(t, e, n) {
            return function (i) {
                var a = Jt;
                Jt = i;
                try {
                    t.call(this, this.__data__, e, n)
                } finally {
                    Jt = a
                }
            }
        }

        function ne(t) {
            return t.trim().split(/^|\s+/).map(function (t) {
                var e = "", n = t.indexOf(".");
                return n >= 0 && (e = t.slice(n + 1), t = t.slice(0, n)), {type: t, name: e}
            })
        }

        function ie(t) {
            return function () {
                var e = this.__on;
                if (e) {
                    for (var n, i = 0, a = -1, r = e.length; i < r; ++i) n = e[i], t.type && n.type !== t.type || n.name !== t.name ? e[++a] = n : this.removeEventListener(n.type, n.listener, n.capture);
                    ++a ? e.length = a : delete this.__on
                }
            }
        }

        function ae(t, e, n) {
            var i = Zt.hasOwnProperty(t.type) ? te : ee;
            return function (a, r, s) {
                var o, c = this.__on, u = i(e, r, s);
                if (c) for (var l = 0, f = c.length; l < f; ++l) if ((o = c[l]).type === t.type && o.name === t.name) return this.removeEventListener(o.type, o.listener, o.capture), this.addEventListener(o.type, o.listener = u, o.capture = n), void(o.value = e);
                this.addEventListener(t.type, u, n), o = {
                    type: t.type,
                    name: t.name,
                    value: e,
                    listener: u,
                    capture: n
                }, c ? c.push(o) : this.__on = [o]
            }
        }

        var re = function (t, e, n) {
            var i, a, r = ne(t + ""), s = r.length;
            if (!(arguments.length < 2)) {
                for (o = e ? ae : ie, null == n && (n = !1), i = 0; i < s; ++i) this.each(o(r[i], e, n));
                return this
            }
            var o = this.node().__on;
            if (o) for (var c, u = 0, l = o.length; u < l; ++u) for (i = 0, c = o[u]; i < s; ++i) if ((a = r[i]).type === c.type && a.name === c.name) return c.value
        };

        function se(t, e, n) {
            var i = ft(t), a = i.CustomEvent;
            "function" === typeof a ? a = new a(e, n) : (a = i.document.createEvent("Event"), n ? (a.initEvent(e, n.bubbles, n.cancelable), a.detail = n.detail) : a.initEvent(e, !1, !1)), t.dispatchEvent(a)
        }

        function oe(t, e) {
            return function () {
                return se(this, t, e)
            }
        }

        function ce(t, e) {
            return function () {
                return se(this, t, e.apply(this, arguments))
            }
        }

        var ue = function (t, e) {
            return this.each(("function" === typeof e ? ce : oe)(t, e))
        }, le = [null];

        function fe(t, e) {
            this._groups = t, this._parents = e
        }

        function de() {
            return new fe([[document.documentElement]], le)
        }

        fe.prototype = de.prototype = {
            constructor: fe,
            select: $,
            selectAll: I,
            filter: U,
            data: G,
            enter: D,
            exit: z,
            join: Y,
            merge: W,
            order: X,
            sort: K,
            call: J,
            nodes: Q,
            node: tt,
            size: et,
            empty: nt,
            each: it,
            attr: lt,
            style: bt,
            property: yt,
            classed: Nt,
            text: Et,
            html: Ut,
            raise: Dt,
            lower: Bt,
            append: Ft,
            insert: qt,
            remove: zt,
            clone: Xt,
            datum: Kt,
            on: re,
            dispatch: ue
        };
        var he = de, pe = function (t) {
            return "string" === typeof t ? new fe([[document.querySelector(t)]], [document.documentElement]) : new fe([[t]], le)
        }, be = 0;

        function ve() {
            return new _e
        }

        function _e() {
            this._ = "@" + (++be).toString(36)
        }

        _e.prototype = ve.prototype = {
            constructor: _e, get: function (t) {
                var e = this._;
                while (!(e in t)) if (!(t = t.parentNode)) return;
                return t[e]
            }, set: function (t, e) {
                return t[this._] = e
            }, remove: function (t) {
                return this._ in t && delete t[this._]
            }, toString: function () {
                return this._
            }
        };

        function ge(t, e, n, i, a, r, s, o, c, u) {
            this.target = t, this.type = e, this.subject = n, this.identifier = i, this.active = a, this.x = r, this.y = s, this.dx = o, this.dy = c, this._ = u
        }

        ge.prototype.on = function () {
            var t = this._.on.apply(this._, arguments);
            return t === this._ ? this : t
        };
        var me = function (t, e, n) {
            t.prototype = e.prototype = n, n.constructor = t
        };

        function ye(t, e) {
            var n = Object.create(t.prototype);
            for (var i in e) n[i] = e[i];
            return n
        }

        function xe() {
        }

        var we = .7, Oe = 1 / we, Me = "\\s*([+-]?\\d+)\\s*", Ce = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)\\s*",
            ke = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)%\\s*", Ae = /^#([0-9a-f]{3})$/, Te = /^#([0-9a-f]{6})$/,
            Ne = new RegExp("^rgb\\(" + [Me, Me, Me] + "\\)$"), Pe = new RegExp("^rgb\\(" + [ke, ke, ke] + "\\)$"),
            Se = new RegExp("^rgba\\(" + [Me, Me, Me, Ce] + "\\)$"),
            $e = new RegExp("^rgba\\(" + [ke, ke, ke, Ce] + "\\)$"), Ee = new RegExp("^hsl\\(" + [Ce, ke, ke] + "\\)$"),
            je = new RegExp("^hsla\\(" + [Ce, ke, ke, Ce] + "\\)$"), Ie = {
                aliceblue: 15792383,
                antiquewhite: 16444375,
                aqua: 65535,
                aquamarine: 8388564,
                azure: 15794175,
                beige: 16119260,
                bisque: 16770244,
                black: 0,
                blanchedalmond: 16772045,
                blue: 255,
                blueviolet: 9055202,
                brown: 10824234,
                burlywood: 14596231,
                cadetblue: 6266528,
                chartreuse: 8388352,
                chocolate: 13789470,
                coral: 16744272,
                cornflowerblue: 6591981,
                cornsilk: 16775388,
                crimson: 14423100,
                cyan: 65535,
                darkblue: 139,
                darkcyan: 35723,
                darkgoldenrod: 12092939,
                darkgray: 11119017,
                darkgreen: 25600,
                darkgrey: 11119017,
                darkkhaki: 12433259,
                darkmagenta: 9109643,
                darkolivegreen: 5597999,
                darkorange: 16747520,
                darkorchid: 10040012,
                darkred: 9109504,
                darksalmon: 15308410,
                darkseagreen: 9419919,
                darkslateblue: 4734347,
                darkslategray: 3100495,
                darkslategrey: 3100495,
                darkturquoise: 52945,
                darkviolet: 9699539,
                deeppink: 16716947,
                deepskyblue: 49151,
                dimgray: 6908265,
                dimgrey: 6908265,
                dodgerblue: 2003199,
                firebrick: 11674146,
                floralwhite: 16775920,
                forestgreen: 2263842,
                fuchsia: 16711935,
                gainsboro: 14474460,
                ghostwhite: 16316671,
                gold: 16766720,
                goldenrod: 14329120,
                gray: 8421504,
                green: 32768,
                greenyellow: 11403055,
                grey: 8421504,
                honeydew: 15794160,
                hotpink: 16738740,
                indianred: 13458524,
                indigo: 4915330,
                ivory: 16777200,
                khaki: 15787660,
                lavender: 15132410,
                lavenderblush: 16773365,
                lawngreen: 8190976,
                lemonchiffon: 16775885,
                lightblue: 11393254,
                lightcoral: 15761536,
                lightcyan: 14745599,
                lightgoldenrodyellow: 16448210,
                lightgray: 13882323,
                lightgreen: 9498256,
                lightgrey: 13882323,
                lightpink: 16758465,
                lightsalmon: 16752762,
                lightseagreen: 2142890,
                lightskyblue: 8900346,
                lightslategray: 7833753,
                lightslategrey: 7833753,
                lightsteelblue: 11584734,
                lightyellow: 16777184,
                lime: 65280,
                limegreen: 3329330,
                linen: 16445670,
                magenta: 16711935,
                maroon: 8388608,
                mediumaquamarine: 6737322,
                mediumblue: 205,
                mediumorchid: 12211667,
                mediumpurple: 9662683,
                mediumseagreen: 3978097,
                mediumslateblue: 8087790,
                mediumspringgreen: 64154,
                mediumturquoise: 4772300,
                mediumvioletred: 13047173,
                midnightblue: 1644912,
                mintcream: 16121850,
                mistyrose: 16770273,
                moccasin: 16770229,
                navajowhite: 16768685,
                navy: 128,
                oldlace: 16643558,
                olive: 8421376,
                olivedrab: 7048739,
                orange: 16753920,
                orangered: 16729344,
                orchid: 14315734,
                palegoldenrod: 15657130,
                palegreen: 10025880,
                paleturquoise: 11529966,
                palevioletred: 14381203,
                papayawhip: 16773077,
                peachpuff: 16767673,
                peru: 13468991,
                pink: 16761035,
                plum: 14524637,
                powderblue: 11591910,
                purple: 8388736,
                rebeccapurple: 6697881,
                red: 16711680,
                rosybrown: 12357519,
                royalblue: 4286945,
                saddlebrown: 9127187,
                salmon: 16416882,
                sandybrown: 16032864,
                seagreen: 3050327,
                seashell: 16774638,
                sienna: 10506797,
                silver: 12632256,
                skyblue: 8900331,
                slateblue: 6970061,
                slategray: 7372944,
                slategrey: 7372944,
                snow: 16775930,
                springgreen: 65407,
                steelblue: 4620980,
                tan: 13808780,
                teal: 32896,
                thistle: 14204888,
                tomato: 16737095,
                turquoise: 4251856,
                violet: 15631086,
                wheat: 16113331,
                white: 16777215,
                whitesmoke: 16119285,
                yellow: 16776960,
                yellowgreen: 10145074
            };

        function Le(t) {
            var e;
            return t = (t + "").trim().toLowerCase(), (e = Ae.exec(t)) ? (e = parseInt(e[1], 16), new Be(e >> 8 & 15 | e >> 4 & 240, e >> 4 & 15 | 240 & e, (15 & e) << 4 | 15 & e, 1)) : (e = Te.exec(t)) ? Ue(parseInt(e[1], 16)) : (e = Ne.exec(t)) ? new Be(e[1], e[2], e[3], 1) : (e = Pe.exec(t)) ? new Be(255 * e[1] / 100, 255 * e[2] / 100, 255 * e[3] / 100, 1) : (e = Se.exec(t)) ? Re(e[1], e[2], e[3], e[4]) : (e = $e.exec(t)) ? Re(255 * e[1] / 100, 255 * e[2] / 100, 255 * e[3] / 100, e[4]) : (e = Ee.exec(t)) ? He(e[1], e[2] / 100, e[3] / 100, 1) : (e = je.exec(t)) ? He(e[1], e[2] / 100, e[3] / 100, e[4]) : Ie.hasOwnProperty(t) ? Ue(Ie[t]) : "transparent" === t ? new Be(NaN, NaN, NaN, 0) : null
        }

        function Ue(t) {
            return new Be(t >> 16 & 255, t >> 8 & 255, 255 & t, 1)
        }

        function Re(t, e, n, i) {
            return i <= 0 && (t = e = n = NaN), new Be(t, e, n, i)
        }

        function De(t) {
            return t instanceof xe || (t = Le(t)), t ? (t = t.rgb(), new Be(t.r, t.g, t.b, t.opacity)) : new Be
        }

        function Ve(t, e, n, i) {
            return 1 === arguments.length ? De(t) : new Be(t, e, n, null == i ? 1 : i)
        }

        function Be(t, e, n, i) {
            this.r = +t, this.g = +e, this.b = +n, this.opacity = +i
        }

        function Fe(t) {
            return t = Math.max(0, Math.min(255, Math.round(t) || 0)), (t < 16 ? "0" : "") + t.toString(16)
        }

        function He(t, e, n, i) {
            return i <= 0 ? t = e = n = NaN : n <= 0 || n >= 1 ? t = e = NaN : e <= 0 && (t = NaN), new ze(t, e, n, i)
        }

        function qe(t) {
            if (t instanceof ze) return new ze(t.h, t.s, t.l, t.opacity);
            if (t instanceof xe || (t = Le(t)), !t) return new ze;
            if (t instanceof ze) return t;
            t = t.rgb();
            var e = t.r / 255, n = t.g / 255, i = t.b / 255, a = Math.min(e, n, i), r = Math.max(e, n, i), s = NaN,
                o = r - a, c = (r + a) / 2;
            return o ? (s = e === r ? (n - i) / o + 6 * (n < i) : n === r ? (i - e) / o + 2 : (e - n) / o + 4, o /= c < .5 ? r + a : 2 - r - a, s *= 60) : o = c > 0 && c < 1 ? 0 : s, new ze(s, o, c, t.opacity)
        }

        function Ge(t, e, n, i) {
            return 1 === arguments.length ? qe(t) : new ze(t, e, n, null == i ? 1 : i)
        }

        function ze(t, e, n, i) {
            this.h = +t, this.s = +e, this.l = +n, this.opacity = +i
        }

        function Ye(t, e, n) {
            return 255 * (t < 60 ? e + (n - e) * t / 60 : t < 180 ? n : t < 240 ? e + (n - e) * (240 - t) / 60 : e)
        }

        me(xe, Le, {
            displayable: function () {
                return this.rgb().displayable()
            }, hex: function () {
                return this.rgb().hex()
            }, toString: function () {
                return this.rgb() + ""
            }
        }), me(Be, Ve, ye(xe, {
            brighter: function (t) {
                return t = null == t ? Oe : Math.pow(Oe, t), new Be(this.r * t, this.g * t, this.b * t, this.opacity)
            }, darker: function (t) {
                return t = null == t ? we : Math.pow(we, t), new Be(this.r * t, this.g * t, this.b * t, this.opacity)
            }, rgb: function () {
                return this
            }, displayable: function () {
                return 0 <= this.r && this.r <= 255 && 0 <= this.g && this.g <= 255 && 0 <= this.b && this.b <= 255 && 0 <= this.opacity && this.opacity <= 1
            }, hex: function () {
                return "#" + Fe(this.r) + Fe(this.g) + Fe(this.b)
            }, toString: function () {
                var t = this.opacity;
                return t = isNaN(t) ? 1 : Math.max(0, Math.min(1, t)), (1 === t ? "rgb(" : "rgba(") + Math.max(0, Math.min(255, Math.round(this.r) || 0)) + ", " + Math.max(0, Math.min(255, Math.round(this.g) || 0)) + ", " + Math.max(0, Math.min(255, Math.round(this.b) || 0)) + (1 === t ? ")" : ", " + t + ")")
            }
        })), me(ze, Ge, ye(xe, {
            brighter: function (t) {
                return t = null == t ? Oe : Math.pow(Oe, t), new ze(this.h, this.s, this.l * t, this.opacity)
            }, darker: function (t) {
                return t = null == t ? we : Math.pow(we, t), new ze(this.h, this.s, this.l * t, this.opacity)
            }, rgb: function () {
                var t = this.h % 360 + 360 * (this.h < 0), e = isNaN(t) || isNaN(this.s) ? 0 : this.s, n = this.l,
                    i = n + (n < .5 ? n : 1 - n) * e, a = 2 * n - i;
                return new Be(Ye(t >= 240 ? t - 240 : t + 120, a, i), Ye(t, a, i), Ye(t < 120 ? t + 240 : t - 120, a, i), this.opacity)
            }, displayable: function () {
                return (0 <= this.s && this.s <= 1 || isNaN(this.s)) && 0 <= this.l && this.l <= 1 && 0 <= this.opacity && this.opacity <= 1
            }
        }));
        var We = Math.PI / 180, Xe = 180 / Math.PI, Ke = 18, Ze = .96422, Je = 1, Qe = .82521, tn = 4 / 29, en = 6 / 29,
            nn = 3 * en * en, an = en * en * en;

        function rn(t) {
            if (t instanceof on) return new on(t.l, t.a, t.b, t.opacity);
            if (t instanceof pn) {
                if (isNaN(t.h)) return new on(t.l, 0, 0, t.opacity);
                var e = t.h * We;
                return new on(t.l, Math.cos(e) * t.c, Math.sin(e) * t.c, t.opacity)
            }
            t instanceof Be || (t = De(t));
            var n, i, a = fn(t.r), r = fn(t.g), s = fn(t.b), o = cn((.2225045 * a + .7168786 * r + .0606169 * s) / Je);
            return a === r && r === s ? n = i = o : (n = cn((.4360747 * a + .3850649 * r + .1430804 * s) / Ze), i = cn((.0139322 * a + .0971045 * r + .7141733 * s) / Qe)), new on(116 * o - 16, 500 * (n - o), 200 * (o - i), t.opacity)
        }

        function sn(t, e, n, i) {
            return 1 === arguments.length ? rn(t) : new on(t, e, n, null == i ? 1 : i)
        }

        function on(t, e, n, i) {
            this.l = +t, this.a = +e, this.b = +n, this.opacity = +i
        }

        function cn(t) {
            return t > an ? Math.pow(t, 1 / 3) : t / nn + tn
        }

        function un(t) {
            return t > en ? t * t * t : nn * (t - tn)
        }

        function ln(t) {
            return 255 * (t <= .0031308 ? 12.92 * t : 1.055 * Math.pow(t, 1 / 2.4) - .055)
        }

        function fn(t) {
            return (t /= 255) <= .04045 ? t / 12.92 : Math.pow((t + .055) / 1.055, 2.4)
        }

        function dn(t) {
            if (t instanceof pn) return new pn(t.h, t.c, t.l, t.opacity);
            if (t instanceof on || (t = rn(t)), 0 === t.a && 0 === t.b) return new pn(NaN, 0, t.l, t.opacity);
            var e = Math.atan2(t.b, t.a) * Xe;
            return new pn(e < 0 ? e + 360 : e, Math.sqrt(t.a * t.a + t.b * t.b), t.l, t.opacity)
        }

        function hn(t, e, n, i) {
            return 1 === arguments.length ? dn(t) : new pn(t, e, n, null == i ? 1 : i)
        }

        function pn(t, e, n, i) {
            this.h = +t, this.c = +e, this.l = +n, this.opacity = +i
        }

        me(on, sn, ye(xe, {
            brighter: function (t) {
                return new on(this.l + Ke * (null == t ? 1 : t), this.a, this.b, this.opacity)
            }, darker: function (t) {
                return new on(this.l - Ke * (null == t ? 1 : t), this.a, this.b, this.opacity)
            }, rgb: function () {
                var t = (this.l + 16) / 116, e = isNaN(this.a) ? t : t + this.a / 500,
                    n = isNaN(this.b) ? t : t - this.b / 200;
                return e = Ze * un(e), t = Je * un(t), n = Qe * un(n), new Be(ln(3.1338561 * e - 1.6168667 * t - .4906146 * n), ln(-.9787684 * e + 1.9161415 * t + .033454 * n), ln(.0719453 * e - .2289914 * t + 1.4052427 * n), this.opacity)
            }
        })), me(pn, hn, ye(xe, {
            brighter: function (t) {
                return new pn(this.h, this.c, this.l + Ke * (null == t ? 1 : t), this.opacity)
            }, darker: function (t) {
                return new pn(this.h, this.c, this.l - Ke * (null == t ? 1 : t), this.opacity)
            }, rgb: function () {
                return rn(this).rgb()
            }
        }));
        var bn = -.14861, vn = 1.78277, _n = -.29227, gn = -.90649, mn = 1.97294, yn = mn * gn, xn = mn * vn,
            wn = vn * _n - gn * bn;

        function On(t) {
            if (t instanceof Cn) return new Cn(t.h, t.s, t.l, t.opacity);
            t instanceof Be || (t = De(t));
            var e = t.r / 255, n = t.g / 255, i = t.b / 255, a = (wn * i + yn * e - xn * n) / (wn + yn - xn), r = i - a,
                s = (mn * (n - a) - _n * r) / gn, o = Math.sqrt(s * s + r * r) / (mn * a * (1 - a)),
                c = o ? Math.atan2(s, r) * Xe - 120 : NaN;
            return new Cn(c < 0 ? c + 360 : c, o, a, t.opacity)
        }

        function Mn(t, e, n, i) {
            return 1 === arguments.length ? On(t) : new Cn(t, e, n, null == i ? 1 : i)
        }

        function Cn(t, e, n, i) {
            this.h = +t, this.s = +e, this.l = +n, this.opacity = +i
        }

        function kn(t, e, n, i, a) {
            var r = t * t, s = r * t;
            return ((1 - 3 * t + 3 * r - s) * e + (4 - 6 * r + 3 * s) * n + (1 + 3 * t + 3 * r - 3 * s) * i + s * a) / 6
        }

        me(Cn, Mn, ye(xe, {
            brighter: function (t) {
                return t = null == t ? Oe : Math.pow(Oe, t), new Cn(this.h, this.s, this.l * t, this.opacity)
            }, darker: function (t) {
                return t = null == t ? we : Math.pow(we, t), new Cn(this.h, this.s, this.l * t, this.opacity)
            }, rgb: function () {
                var t = isNaN(this.h) ? 0 : (this.h + 120) * We, e = +this.l,
                    n = isNaN(this.s) ? 0 : this.s * e * (1 - e), i = Math.cos(t), a = Math.sin(t);
                return new Be(255 * (e + n * (bn * i + vn * a)), 255 * (e + n * (_n * i + gn * a)), 255 * (e + n * (mn * i)), this.opacity)
            }
        }));
        var An = function (t) {
            var e = t.length - 1;
            return function (n) {
                var i = n <= 0 ? n = 0 : n >= 1 ? (n = 1, e - 1) : Math.floor(n * e), a = t[i], r = t[i + 1],
                    s = i > 0 ? t[i - 1] : 2 * a - r, o = i < e - 1 ? t[i + 2] : 2 * r - a;
                return kn((n - i / e) * e, s, a, r, o)
            }
        }, Tn = function (t) {
            var e = t.length;
            return function (n) {
                var i = Math.floor(((n %= 1) < 0 ? ++n : n) * e), a = t[(i + e - 1) % e], r = t[i % e],
                    s = t[(i + 1) % e], o = t[(i + 2) % e];
                return kn((n - i / e) * e, a, r, s, o)
            }
        }, Nn = function (t) {
            return function () {
                return t
            }
        };

        function Pn(t, e) {
            return function (n) {
                return t + n * e
            }
        }

        function Sn(t, e, n) {
            return t = Math.pow(t, n), e = Math.pow(e, n) - t, n = 1 / n, function (i) {
                return Math.pow(t + i * e, n)
            }
        }

        function $n(t, e) {
            var n = e - t;
            return n ? Pn(t, n > 180 || n < -180 ? n - 360 * Math.round(n / 360) : n) : Nn(isNaN(t) ? e : t)
        }

        function En(t) {
            return 1 === (t = +t) ? jn : function (e, n) {
                return n - e ? Sn(e, n, t) : Nn(isNaN(e) ? n : e)
            }
        }

        function jn(t, e) {
            var n = e - t;
            return n ? Pn(t, n) : Nn(isNaN(t) ? e : t)
        }

        var In = function t(e) {
            var n = En(e);

            function i(t, e) {
                var i = n((t = Ve(t)).r, (e = Ve(e)).r), a = n(t.g, e.g), r = n(t.b, e.b), s = jn(t.opacity, e.opacity);
                return function (e) {
                    return t.r = i(e), t.g = a(e), t.b = r(e), t.opacity = s(e), t + ""
                }
            }

            return i.gamma = t, i
        }(1);

        function Ln(t) {
            return function (e) {
                var n, i, a = e.length, r = new Array(a), s = new Array(a), o = new Array(a);
                for (n = 0; n < a; ++n) i = Ve(e[n]), r[n] = i.r || 0, s[n] = i.g || 0, o[n] = i.b || 0;
                return r = t(r), s = t(s), o = t(o), i.opacity = 1, function (t) {
                    return i.r = r(t), i.g = s(t), i.b = o(t), i + ""
                }
            }
        }

        var Un = Ln(An), Rn = (Ln(Tn), function (t, e) {
            var n, i = e ? e.length : 0, a = t ? Math.min(i, t.length) : 0, r = new Array(a), s = new Array(i);
            for (n = 0; n < a; ++n) r[n] = Zn(t[n], e[n]);
            for (; n < i; ++n) s[n] = e[n];
            return function (t) {
                for (n = 0; n < a; ++n) s[n] = r[n](t);
                return s
            }
        }), Dn = function (t, e) {
            var n = new Date;
            return t = +t, e -= t, function (i) {
                return n.setTime(t + e * i), n
            }
        }, Vn = function (t, e) {
            return t = +t, e -= t, function (n) {
                return t + e * n
            }
        }, Bn = function (t, e) {
            var n, i = {}, a = {};
            for (n in null !== t && "object" === typeof t || (t = {}), null !== e && "object" === typeof e || (e = {}), e) n in t ? i[n] = Zn(t[n], e[n]) : a[n] = e[n];
            return function (t) {
                for (n in i) a[n] = i[n](t);
                return a
            }
        }, Fn = /[-+]?(?:\d+\.?\d*|\.?\d+)(?:[eE][-+]?\d+)?/g, Hn = new RegExp(Fn.source, "g");

        function qn(t) {
            return function () {
                return t
            }
        }

        function Gn(t) {
            return function (e) {
                return t(e) + ""
            }
        }

        var zn, Yn, Wn, Xn, Kn = function (t, e) {
                var n, i, a, r = Fn.lastIndex = Hn.lastIndex = 0, s = -1, o = [], c = [];
                t += "", e += "";
                while ((n = Fn.exec(t)) && (i = Hn.exec(e))) (a = i.index) > r && (a = e.slice(r, a), o[s] ? o[s] += a : o[++s] = a), (n = n[0]) === (i = i[0]) ? o[s] ? o[s] += i : o[++s] = i : (o[++s] = null, c.push({
                    i: s,
                    x: Vn(n, i)
                })), r = Hn.lastIndex;
                return r < e.length && (a = e.slice(r), o[s] ? o[s] += a : o[++s] = a), o.length < 2 ? c[0] ? Gn(c[0].x) : qn(e) : (e = c.length, function (t) {
                    for (var n, i = 0; i < e; ++i) o[(n = c[i]).i] = n.x(t);
                    return o.join("")
                })
            }, Zn = function (t, e) {
                var n, i = typeof e;
                return null == e || "boolean" === i ? Nn(e) : ("number" === i ? Vn : "string" === i ? (n = Le(e)) ? (e = n, In) : Kn : e instanceof Le ? In : e instanceof Date ? Dn : Array.isArray(e) ? Rn : "function" !== typeof e.valueOf && "function" !== typeof e.toString || isNaN(e) ? Bn : Vn)(t, e)
            }, Jn = function (t, e) {
                return t = +t, e -= t, function (n) {
                    return Math.round(t + e * n)
                }
            }, Qn = 180 / Math.PI, ti = {translateX: 0, translateY: 0, rotate: 0, skewX: 0, scaleX: 1, scaleY: 1},
            ei = function (t, e, n, i, a, r) {
                var s, o, c;
                return (s = Math.sqrt(t * t + e * e)) && (t /= s, e /= s), (c = t * n + e * i) && (n -= t * c, i -= e * c), (o = Math.sqrt(n * n + i * i)) && (n /= o, i /= o, c /= o), t * i < e * n && (t = -t, e = -e, c = -c, s = -s), {
                    translateX: a,
                    translateY: r,
                    rotate: Math.atan2(e, t) * Qn,
                    skewX: Math.atan(c) * Qn,
                    scaleX: s,
                    scaleY: o
                }
            };

        function ni(t) {
            return "none" === t ? ti : (zn || (zn = document.createElement("DIV"), Yn = document.documentElement, Wn = document.defaultView), zn.style.transform = t, t = Wn.getComputedStyle(Yn.appendChild(zn), null).getPropertyValue("transform"), Yn.removeChild(zn), t = t.slice(7, -1).split(","), ei(+t[0], +t[1], +t[2], +t[3], +t[4], +t[5]))
        }

        function ii(t) {
            return null == t ? ti : (Xn || (Xn = document.createElementNS("http://www.w3.org/2000/svg", "g")), Xn.setAttribute("transform", t), (t = Xn.transform.baseVal.consolidate()) ? (t = t.matrix, ei(t.a, t.b, t.c, t.d, t.e, t.f)) : ti)
        }

        function ai(t, e, n, i) {
            function a(t) {
                return t.length ? t.pop() + " " : ""
            }

            function r(t, i, a, r, s, o) {
                if (t !== a || i !== r) {
                    var c = s.push("translate(", null, e, null, n);
                    o.push({i: c - 4, x: Vn(t, a)}, {i: c - 2, x: Vn(i, r)})
                } else (a || r) && s.push("translate(" + a + e + r + n)
            }

            function s(t, e, n, r) {
                t !== e ? (t - e > 180 ? e += 360 : e - t > 180 && (t += 360), r.push({
                    i: n.push(a(n) + "rotate(", null, i) - 2,
                    x: Vn(t, e)
                })) : e && n.push(a(n) + "rotate(" + e + i)
            }

            function o(t, e, n, r) {
                t !== e ? r.push({
                    i: n.push(a(n) + "skewX(", null, i) - 2,
                    x: Vn(t, e)
                }) : e && n.push(a(n) + "skewX(" + e + i)
            }

            function c(t, e, n, i, r, s) {
                if (t !== n || e !== i) {
                    var o = r.push(a(r) + "scale(", null, ",", null, ")");
                    s.push({i: o - 4, x: Vn(t, n)}, {i: o - 2, x: Vn(e, i)})
                } else 1 === n && 1 === i || r.push(a(r) + "scale(" + n + "," + i + ")")
            }

            return function (e, n) {
                var i = [], a = [];
                return e = t(e), n = t(n), r(e.translateX, e.translateY, n.translateX, n.translateY, i, a), s(e.rotate, n.rotate, i, a), o(e.skewX, n.skewX, i, a), c(e.scaleX, e.scaleY, n.scaleX, n.scaleY, i, a), e = n = null, function (t) {
                    var e, n = -1, r = a.length;
                    while (++n < r) i[(e = a[n]).i] = e.x(t);
                    return i.join("")
                }
            }
        }

        var ri = ai(ni, "px, ", "px)", "deg)"), si = ai(ii, ", ", ")", ")");
        Math.SQRT2;

        function oi(t) {
            return function (e, n) {
                var i = t((e = Ge(e)).h, (n = Ge(n)).h), a = jn(e.s, n.s), r = jn(e.l, n.l),
                    s = jn(e.opacity, n.opacity);
                return function (t) {
                    return e.h = i(t), e.s = a(t), e.l = r(t), e.opacity = s(t), e + ""
                }
            }
        }

        oi($n), oi(jn);

        function ci(t) {
            return function (e, n) {
                var i = t((e = hn(e)).h, (n = hn(n)).h), a = jn(e.c, n.c), r = jn(e.l, n.l),
                    s = jn(e.opacity, n.opacity);
                return function (t) {
                    return e.h = i(t), e.c = a(t), e.l = r(t), e.opacity = s(t), e + ""
                }
            }
        }

        ci($n), ci(jn);

        function ui(t) {
            return function e(n) {
                function i(e, i) {
                    var a = t((e = Mn(e)).h, (i = Mn(i)).h), r = jn(e.s, i.s), s = jn(e.l, i.l),
                        o = jn(e.opacity, i.opacity);
                    return function (t) {
                        return e.h = a(t), e.s = r(t), e.l = s(Math.pow(t, n)), e.opacity = o(t), e + ""
                    }
                }

                return n = +n, i.gamma = e, i
            }(1)
        }

        ui($n);
        var li = ui(jn);
        var fi, di, hi = 0, pi = 0, bi = 0, vi = 1e3, _i = 0, gi = 0, mi = 0,
            yi = "object" === typeof performance && performance.now ? performance : Date,
            xi = "object" === typeof window && window.requestAnimationFrame ? window.requestAnimationFrame.bind(window) : function (t) {
                setTimeout(t, 17)
            };

        function wi() {
            return gi || (xi(Oi), gi = yi.now() + mi)
        }

        function Oi() {
            gi = 0
        }

        function Mi() {
            this._call = this._time = this._next = null
        }

        function Ci(t, e, n) {
            var i = new Mi;
            return i.restart(t, e, n), i
        }

        function ki() {
            wi(), ++hi;
            var t, e = fi;
            while (e) (t = gi - e._time) >= 0 && e._call.call(null, t), e = e._next;
            --hi
        }

        function Ai() {
            gi = (_i = yi.now()) + mi, hi = pi = 0;
            try {
                ki()
            } finally {
                hi = 0, Ni(), gi = 0
            }
        }

        function Ti() {
            var t = yi.now(), e = t - _i;
            e > vi && (mi -= e, _i = t)
        }

        function Ni() {
            var t, e, n = fi, i = 1 / 0;
            while (n) n._call ? (i > n._time && (i = n._time), t = n, n = n._next) : (e = n._next, n._next = null, n = t ? t._next = e : fi = e);
            di = t, Pi(i)
        }

        function Pi(t) {
            if (!hi) {
                pi && (pi = clearTimeout(pi));
                var e = t - gi;
                e > 24 ? (t < 1 / 0 && (pi = setTimeout(Ai, t - yi.now() - mi)), bi && (bi = clearInterval(bi))) : (bi || (_i = yi.now(), bi = setInterval(Ti, vi)), hi = 1, xi(Ai))
            }
        }

        Mi.prototype = Ci.prototype = {
            constructor: Mi, restart: function (t, e, n) {
                if ("function" !== typeof t) throw new TypeError("callback is not a function");
                n = (null == n ? wi() : +n) + (null == e ? 0 : +e), this._next || di === this || (di ? di._next = this : fi = this, di = this), this._call = t, this._time = n, Pi()
            }, stop: function () {
                this._call && (this._call = null, this._time = 1 / 0, Pi())
            }
        };
        var Si = function (t, e, n) {
                var i = new Mi;
                return e = null == e ? 0 : +e, i.restart(function (n) {
                    i.stop(), t(n + e)
                }, e, n), i
            }, $i = O("start", "end", "cancel", "interrupt"), Ei = [], ji = 0, Ii = 1, Li = 2, Ui = 3, Ri = 4, Di = 5,
            Vi = 6, Bi = function (t, e, n, i, a, r) {
                var s = t.__transition;
                if (s) {
                    if (n in s) return
                } else t.__transition = {};
                Gi(t, n, {
                    name: e,
                    index: i,
                    group: a,
                    on: $i,
                    tween: Ei,
                    time: r.time,
                    delay: r.delay,
                    duration: r.duration,
                    ease: r.ease,
                    timer: null,
                    state: ji
                })
            };

        function Fi(t, e) {
            var n = qi(t, e);
            if (n.state > ji) throw new Error("too late; already scheduled");
            return n
        }

        function Hi(t, e) {
            var n = qi(t, e);
            if (n.state > Ui) throw new Error("too late; already running");
            return n
        }

        function qi(t, e) {
            var n = t.__transition;
            if (!n || !(n = n[e])) throw new Error("transition not found");
            return n
        }

        function Gi(t, e, n) {
            var i, a = t.__transition;

            function r(t) {
                n.state = Ii, n.timer.restart(s, n.delay, n.time), n.delay <= t && s(t - n.delay)
            }

            function s(r) {
                var u, l, f, d;
                if (n.state !== Ii) return c();
                for (u in a) if (d = a[u], d.name === n.name) {
                    if (d.state === Ui) return Si(s);
                    d.state === Ri ? (d.state = Vi, d.timer.stop(), d.on.call("interrupt", t, t.__data__, d.index, d.group), delete a[u]) : +u < e && (d.state = Vi, d.timer.stop(), d.on.call("cancel", t, t.__data__, d.index, d.group), delete a[u])
                }
                if (Si(function () {
                        n.state === Ui && (n.state = Ri, n.timer.restart(o, n.delay, n.time), o(r))
                    }), n.state = Li, n.on.call("start", t, t.__data__, n.index, n.group), n.state === Li) {
                    for (n.state = Ui, i = new Array(f = n.tween.length), u = 0, l = -1; u < f; ++u) (d = n.tween[u].value.call(t, t.__data__, n.index, n.group)) && (i[++l] = d);
                    i.length = l + 1
                }
            }

            function o(e) {
                var a = e < n.duration ? n.ease.call(null, e / n.duration) : (n.timer.restart(c), n.state = Di, 1),
                    r = -1, s = i.length;
                while (++r < s) i[r].call(t, a);
                n.state === Di && (n.on.call("end", t, t.__data__, n.index, n.group), c())
            }

            function c() {
                for (var i in n.state = Vi, n.timer.stop(), delete a[e], a) return;
                delete t.__transition
            }

            a[e] = n, n.timer = Ci(r, 0, n.time)
        }

        var zi = function (t, e) {
            var n, i, a, r = t.__transition, s = !0;
            if (r) {
                for (a in e = null == e ? null : e + "", r) (n = r[a]).name === e ? (i = n.state > Li && n.state < Di, n.state = Vi, n.timer.stop(), n.on.call(i ? "interrupt" : "cancel", t, t.__data__, n.index, n.group), delete r[a]) : s = !1;
                s && delete t.__transition
            }
        }, Yi = function (t) {
            return this.each(function () {
                zi(this, t)
            })
        };

        function Wi(t, e) {
            var n, i;
            return function () {
                var a = Hi(this, t), r = a.tween;
                if (r !== n) {
                    i = n = r;
                    for (var s = 0, o = i.length; s < o; ++s) if (i[s].name === e) {
                        i = i.slice(), i.splice(s, 1);
                        break
                    }
                }
                a.tween = i
            }
        }

        function Xi(t, e, n) {
            var i, a;
            if ("function" !== typeof n) throw new Error;
            return function () {
                var r = Hi(this, t), s = r.tween;
                if (s !== i) {
                    a = (i = s).slice();
                    for (var o = {name: e, value: n}, c = 0, u = a.length; c < u; ++c) if (a[c].name === e) {
                        a[c] = o;
                        break
                    }
                    c === u && a.push(o)
                }
                r.tween = a
            }
        }

        var Ki = function (t, e) {
            var n = this._id;
            if (t += "", arguments.length < 2) {
                for (var i, a = qi(this.node(), n).tween, r = 0, s = a.length; r < s; ++r) if ((i = a[r]).name === t) return i.value;
                return null
            }
            return this.each((null == e ? Wi : Xi)(n, t, e))
        };

        function Zi(t, e, n) {
            var i = t._id;
            return t.each(function () {
                var t = Hi(this, i);
                (t.value || (t.value = {}))[e] = n.apply(this, arguments)
            }), function (t) {
                return qi(t, i).value[e]
            }
        }

        var Ji = function (t, e) {
            var n;
            return ("number" === typeof e ? Vn : e instanceof Le ? In : (n = Le(e)) ? (e = n, In) : Kn)(t, e)
        };

        function Qi(t) {
            return function () {
                this.removeAttribute(t)
            }
        }

        function ta(t) {
            return function () {
                this.removeAttributeNS(t.space, t.local)
            }
        }

        function ea(t, e, n) {
            var i, a, r = n + "";
            return function () {
                var s = this.getAttribute(t);
                return s === r ? null : s === i ? a : a = e(i = s, n)
            }
        }

        function na(t, e, n) {
            var i, a, r = n + "";
            return function () {
                var s = this.getAttributeNS(t.space, t.local);
                return s === r ? null : s === i ? a : a = e(i = s, n)
            }
        }

        function ia(t, e, n) {
            var i, a, r;
            return function () {
                var s, o, c = n(this);
                if (null != c) return s = this.getAttribute(t), o = c + "", s === o ? null : s === i && o === a ? r : (a = o, r = e(i = s, c));
                this.removeAttribute(t)
            }
        }

        function aa(t, e, n) {
            var i, a, r;
            return function () {
                var s, o, c = n(this);
                if (null != c) return s = this.getAttributeNS(t.space, t.local), o = c + "", s === o ? null : s === i && o === a ? r : (a = o, r = e(i = s, c));
                this.removeAttributeNS(t.space, t.local)
            }
        }

        var ra = function (t, e) {
            var n = k(t), i = "transform" === n ? si : Ji;
            return this.attrTween(t, "function" === typeof e ? (n.local ? aa : ia)(n, i, Zi(this, "attr." + t, e)) : null == e ? (n.local ? ta : Qi)(n) : (n.local ? na : ea)(n, i, e))
        };

        function sa(t, e) {
            return function (n) {
                this.setAttribute(t, e(n))
            }
        }

        function oa(t, e) {
            return function (n) {
                this.setAttributeNS(t.space, t.local, e(n))
            }
        }

        function ca(t, e) {
            var n, i;

            function a() {
                var a = e.apply(this, arguments);
                return a !== i && (n = (i = a) && oa(t, a)), n
            }

            return a._value = e, a
        }

        function ua(t, e) {
            var n, i;

            function a() {
                var a = e.apply(this, arguments);
                return a !== i && (n = (i = a) && sa(t, a)), n
            }

            return a._value = e, a
        }

        var la = function (t, e) {
            var n = "attr." + t;
            if (arguments.length < 2) return (n = this.tween(n)) && n._value;
            if (null == e) return this.tween(n, null);
            if ("function" !== typeof e) throw new Error;
            var i = k(t);
            return this.tween(n, (i.local ? ca : ua)(i, e))
        };

        function fa(t, e) {
            return function () {
                Fi(this, t).delay = +e.apply(this, arguments)
            }
        }

        function da(t, e) {
            return e = +e, function () {
                Fi(this, t).delay = e
            }
        }

        var ha = function (t) {
            var e = this._id;
            return arguments.length ? this.each(("function" === typeof t ? fa : da)(e, t)) : qi(this.node(), e).delay
        };

        function pa(t, e) {
            return function () {
                Hi(this, t).duration = +e.apply(this, arguments)
            }
        }

        function ba(t, e) {
            return e = +e, function () {
                Hi(this, t).duration = e
            }
        }

        var va = function (t) {
            var e = this._id;
            return arguments.length ? this.each(("function" === typeof t ? pa : ba)(e, t)) : qi(this.node(), e).duration
        };

        function _a(t, e) {
            if ("function" !== typeof e) throw new Error;
            return function () {
                Hi(this, t).ease = e
            }
        }

        var ga = function (t) {
            var e = this._id;
            return arguments.length ? this.each(_a(e, t)) : qi(this.node(), e).ease
        }, ma = function (t) {
            "function" !== typeof t && (t = L(t));
            for (var e = this._groups, n = e.length, i = new Array(n), a = 0; a < n; ++a) for (var r, s = e[a], o = s.length, c = i[a] = [], u = 0; u < o; ++u) (r = s[u]) && t.call(r, r.__data__, u, s) && c.push(r);
            return new Ga(i, this._parents, this._name, this._id)
        }, ya = function (t) {
            if (t._id !== this._id) throw new Error;
            for (var e = this._groups, n = t._groups, i = e.length, a = n.length, r = Math.min(i, a), s = new Array(i), o = 0; o < r; ++o) for (var c, u = e[o], l = n[o], f = u.length, d = s[o] = new Array(f), h = 0; h < f; ++h) (c = u[h] || l[h]) && (d[h] = c);
            for (; o < i; ++o) s[o] = e[o];
            return new Ga(s, this._parents, this._name, this._id)
        };

        function xa(t) {
            return (t + "").trim().split(/^|\s+/).every(function (t) {
                var e = t.indexOf(".");
                return e >= 0 && (t = t.slice(0, e)), !t || "start" === t
            })
        }

        function wa(t, e, n) {
            var i, a, r = xa(e) ? Fi : Hi;
            return function () {
                var s = r(this, t), o = s.on;
                o !== i && (a = (i = o).copy()).on(e, n), s.on = a
            }
        }

        var Oa = function (t, e) {
            var n = this._id;
            return arguments.length < 2 ? qi(this.node(), n).on.on(t) : this.each(wa(n, t, e))
        };

        function Ma(t) {
            return function () {
                var e = this.parentNode;
                for (var n in this.__transition) if (+n !== t) return;
                e && e.removeChild(this)
            }
        }

        var Ca = function () {
            return this.on("end.remove", Ma(this._id))
        }, ka = function (t) {
            var e = this._name, n = this._id;
            "function" !== typeof t && (t = S(t));
            for (var i = this._groups, a = i.length, r = new Array(a), s = 0; s < a; ++s) for (var o, c, u = i[s], l = u.length, f = r[s] = new Array(l), d = 0; d < l; ++d) (o = u[d]) && (c = t.call(o, o.__data__, d, u)) && ("__data__" in o && (c.__data__ = o.__data__), f[d] = c, Bi(f[d], e, n, d, f, qi(o, n)));
            return new Ga(r, this._parents, e, n)
        }, Aa = function (t) {
            var e = this._name, n = this._id;
            "function" !== typeof t && (t = j(t));
            for (var i = this._groups, a = i.length, r = [], s = [], o = 0; o < a; ++o) for (var c, u = i[o], l = u.length, f = 0; f < l; ++f) if (c = u[f]) {
                for (var d, h = t.call(c, c.__data__, f, u), p = qi(c, n), b = 0, v = h.length; b < v; ++b) (d = h[b]) && Bi(d, e, n, b, h, p);
                r.push(h), s.push(c)
            }
            return new Ga(r, s, e, n)
        }, Ta = he.prototype.constructor, Na = function () {
            return new Ta(this._groups, this._parents)
        };

        function Pa(t, e) {
            var n, i, a;
            return function () {
                var r = vt(this, t), s = (this.style.removeProperty(t), vt(this, t));
                return r === s ? null : r === n && s === i ? a : a = e(n = r, i = s)
            }
        }

        function Sa(t) {
            return function () {
                this.style.removeProperty(t)
            }
        }

        function $a(t, e, n) {
            var i, a, r = n + "";
            return function () {
                var s = vt(this, t);
                return s === r ? null : s === i ? a : a = e(i = s, n)
            }
        }

        function Ea(t, e, n) {
            var i, a, r;
            return function () {
                var s = vt(this, t), o = n(this), c = o + "";
                return null == o && (this.style.removeProperty(t), c = o = vt(this, t)), s === c ? null : s === i && c === a ? r : (a = c, r = e(i = s, o))
            }
        }

        function ja(t, e) {
            var n, i, a, r, s = "style." + e, o = "end." + s;
            return function () {
                var c = Hi(this, t), u = c.on, l = null == c.value[s] ? r || (r = Sa(e)) : void 0;
                u === n && a === l || (i = (n = u).copy()).on(o, a = l), c.on = i
            }
        }

        var Ia = function (t, e, n) {
            var i = "transform" === (t += "") ? ri : Ji;
            return null == e ? this.styleTween(t, Pa(t, i)).on("end.style." + t, Sa(t)) : "function" === typeof e ? this.styleTween(t, Ea(t, i, Zi(this, "style." + t, e))).each(ja(this._id, t)) : this.styleTween(t, $a(t, i, e), n).on("end.style." + t, null)
        };

        function La(t, e, n) {
            return function (i) {
                this.style.setProperty(t, e(i), n)
            }
        }

        function Ua(t, e, n) {
            var i, a;

            function r() {
                var r = e.apply(this, arguments);
                return r !== a && (i = (a = r) && La(t, r, n)), i
            }

            return r._value = e, r
        }

        var Ra = function (t, e, n) {
            var i = "style." + (t += "");
            if (arguments.length < 2) return (i = this.tween(i)) && i._value;
            if (null == e) return this.tween(i, null);
            if ("function" !== typeof e) throw new Error;
            return this.tween(i, Ua(t, e, null == n ? "" : n))
        };

        function Da(t) {
            return function () {
                this.textContent = t
            }
        }

        function Va(t) {
            return function () {
                var e = t(this);
                this.textContent = null == e ? "" : e
            }
        }

        var Ba = function (t) {
            return this.tween("text", "function" === typeof t ? Va(Zi(this, "text", t)) : Da(null == t ? "" : t + ""))
        }, Fa = function () {
            for (var t = this._name, e = this._id, n = Ya(), i = this._groups, a = i.length, r = 0; r < a; ++r) for (var s, o = i[r], c = o.length, u = 0; u < c; ++u) if (s = o[u]) {
                var l = qi(s, e);
                Bi(s, t, n, u, o, {time: l.time + l.delay + l.duration, delay: 0, duration: l.duration, ease: l.ease})
            }
            return new Ga(i, this._parents, t, n)
        }, Ha = function () {
            var t, e, n = this, i = n._id, a = n.size();
            return new Promise(function (r, s) {
                var o = {value: s}, c = {
                    value: function () {
                        0 === --a && r()
                    }
                };
                n.each(function () {
                    var n = Hi(this, i), a = n.on;
                    a !== t && (e = (t = a).copy(), e._.cancel.push(o), e._.interrupt.push(o), e._.end.push(c)), n.on = e
                })
            })
        }, qa = 0;

        function Ga(t, e, n, i) {
            this._groups = t, this._parents = e, this._name = n, this._id = i
        }

        function za(t) {
            return he().transition(t)
        }

        function Ya() {
            return ++qa
        }

        var Wa = he.prototype;

        function Xa(t) {
            return ((t *= 2) <= 1 ? t * t * t : (t -= 2) * t * t + 2) / 2
        }

        Ga.prototype = za.prototype = {
            constructor: Ga,
            select: ka,
            selectAll: Aa,
            filter: ma,
            merge: ya,
            selection: Na,
            transition: Fa,
            call: Wa.call,
            nodes: Wa.nodes,
            node: Wa.node,
            size: Wa.size,
            empty: Wa.empty,
            each: Wa.each,
            on: Oa,
            attr: ra,
            attrTween: la,
            style: Ia,
            styleTween: Ra,
            text: Ba,
            remove: Ca,
            tween: Ki,
            delay: ha,
            duration: va,
            ease: ga,
            end: Ha
        };
        var Ka = 3;
        (function t(e) {
            function n(t) {
                return Math.pow(t, e)
            }

            return e = +e, n.exponent = t, n
        })(Ka), function t(e) {
            function n(t) {
                return 1 - Math.pow(1 - t, e)
            }

            return e = +e, n.exponent = t, n
        }(Ka), function t(e) {
            function n(t) {
                return ((t *= 2) <= 1 ? Math.pow(t, e) : 2 - Math.pow(2 - t, e)) / 2
            }

            return e = +e, n.exponent = t, n
        }(Ka), Math.PI;
        var Za = 1.70158, Ja = (function t(e) {
            function n(t) {
                return t * t * ((e + 1) * t - e)
            }

            return e = +e, n.overshoot = t, n
        }(Za), function t(e) {
            function n(t) {
                return --t * t * ((e + 1) * t + e) + 1
            }

            return e = +e, n.overshoot = t, n
        }(Za), function t(e) {
            function n(t) {
                return ((t *= 2) < 1 ? t * t * ((e + 1) * t - e) : (t -= 2) * t * ((e + 1) * t + e) + 2) / 2
            }

            return e = +e, n.overshoot = t, n
        }(Za), 2 * Math.PI), Qa = 1, tr = .3, er = (function t(e, n) {
            var i = Math.asin(1 / (e = Math.max(1, e))) * (n /= Ja);

            function a(t) {
                return e * Math.pow(2, 10 * --t) * Math.sin((i - t) / n)
            }

            return a.amplitude = function (e) {
                return t(e, n * Ja)
            }, a.period = function (n) {
                return t(e, n)
            }, a
        }(Qa, tr), function t(e, n) {
            var i = Math.asin(1 / (e = Math.max(1, e))) * (n /= Ja);

            function a(t) {
                return 1 - e * Math.pow(2, -10 * (t = +t)) * Math.sin((t + i) / n)
            }

            return a.amplitude = function (e) {
                return t(e, n * Ja)
            }, a.period = function (n) {
                return t(e, n)
            }, a
        }(Qa, tr), function t(e, n) {
            var i = Math.asin(1 / (e = Math.max(1, e))) * (n /= Ja);

            function a(t) {
                return ((t = 2 * t - 1) < 0 ? e * Math.pow(2, 10 * t) * Math.sin((i - t) / n) : 2 - e * Math.pow(2, -10 * t) * Math.sin((i + t) / n)) / 2
            }

            return a.amplitude = function (e) {
                return t(e, n * Ja)
            }, a.period = function (n) {
                return t(e, n)
            }, a
        }(Qa, tr), {time: null, delay: 0, duration: 250, ease: Xa});

        function nr(t, e) {
            var n;
            while (!(n = t.__transition) || !(n = n[e])) if (!(t = t.parentNode)) return er.time = wi(), er;
            return n
        }

        var ir = function (t) {
            var e, n;
            t instanceof Ga ? (e = t._id, t = t._name) : (e = Ya(), (n = er).time = wi(), t = null == t ? null : t + "");
            for (var i = this._groups, a = i.length, r = 0; r < a; ++r) for (var s, o = i[r], c = o.length, u = 0; u < c; ++u) (s = o[u]) && Bi(s, t, e, u, o, n || nr(s, e));
            return new Ga(i, this._parents, t, e)
        };
        he.prototype.interrupt = Yi, he.prototype.transition = ir;
        ["e", "w"].map(ar), ["n", "s"].map(ar), ["n", "e", "s", "w", "nw", "ne", "se", "sw"].map(ar);

        function ar(t) {
            return {type: t}
        }

        Math.cos, Math.sin, Math.PI, Math.max;
        Array.prototype.slice;
        var rr = Math.PI, sr = 2 * rr, or = 1e-6, cr = sr - or;

        function ur() {
            this._x0 = this._y0 = this._x1 = this._y1 = null, this._ = ""
        }

        function lr() {
            return new ur
        }

        ur.prototype = lr.prototype = {
            constructor: ur, moveTo: function (t, e) {
                this._ += "M" + (this._x0 = this._x1 = +t) + "," + (this._y0 = this._y1 = +e)
            }, closePath: function () {
                null !== this._x1 && (this._x1 = this._x0, this._y1 = this._y0, this._ += "Z")
            }, lineTo: function (t, e) {
                this._ += "L" + (this._x1 = +t) + "," + (this._y1 = +e)
            }, quadraticCurveTo: function (t, e, n, i) {
                this._ += "Q" + +t + "," + +e + "," + (this._x1 = +n) + "," + (this._y1 = +i)
            }, bezierCurveTo: function (t, e, n, i, a, r) {
                this._ += "C" + +t + "," + +e + "," + +n + "," + +i + "," + (this._x1 = +a) + "," + (this._y1 = +r)
            }, arcTo: function (t, e, n, i, a) {
                t = +t, e = +e, n = +n, i = +i, a = +a;
                var r = this._x1, s = this._y1, o = n - t, c = i - e, u = r - t, l = s - e, f = u * u + l * l;
                if (a < 0) throw new Error("negative radius: " + a);
                if (null === this._x1) this._ += "M" + (this._x1 = t) + "," + (this._y1 = e); else if (f > or) if (Math.abs(l * o - c * u) > or && a) {
                    var d = n - r, h = i - s, p = o * o + c * c, b = d * d + h * h, v = Math.sqrt(p), _ = Math.sqrt(f),
                        g = a * Math.tan((rr - Math.acos((p + f - b) / (2 * v * _))) / 2), m = g / _, y = g / v;
                    Math.abs(m - 1) > or && (this._ += "L" + (t + m * u) + "," + (e + m * l)), this._ += "A" + a + "," + a + ",0,0," + +(l * d > u * h) + "," + (this._x1 = t + y * o) + "," + (this._y1 = e + y * c)
                } else this._ += "L" + (this._x1 = t) + "," + (this._y1 = e); else ;
            }, arc: function (t, e, n, i, a, r) {
                t = +t, e = +e, n = +n;
                var s = n * Math.cos(i), o = n * Math.sin(i), c = t + s, u = e + o, l = 1 ^ r, f = r ? i - a : a - i;
                if (n < 0) throw new Error("negative radius: " + n);
                null === this._x1 ? this._ += "M" + c + "," + u : (Math.abs(this._x1 - c) > or || Math.abs(this._y1 - u) > or) && (this._ += "L" + c + "," + u), n && (f < 0 && (f = f % sr + sr), f > cr ? this._ += "A" + n + "," + n + ",0,1," + l + "," + (t - s) + "," + (e - o) + "A" + n + "," + n + ",0,1," + l + "," + (this._x1 = c) + "," + (this._y1 = u) : f > or && (this._ += "A" + n + "," + n + ",0," + +(f >= rr) + "," + l + "," + (this._x1 = t + n * Math.cos(a)) + "," + (this._y1 = e + n * Math.sin(a))))
            }, rect: function (t, e, n, i) {
                this._ += "M" + (this._x0 = this._x1 = +t) + "," + (this._y0 = this._y1 = +e) + "h" + +n + "v" + +i + "h" + -n + "Z"
            }, toString: function () {
                return this._
            }
        };
        var fr = lr;
        var dr = "$";

        function hr() {
        }

        function pr(t, e) {
            var n = new hr;
            if (t instanceof hr) t.each(function (t, e) {
                n.set(e, t)
            }); else if (Array.isArray(t)) {
                var i, a = -1, r = t.length;
                if (null == e) while (++a < r) n.set(a, t[a]); else while (++a < r) n.set(e(i = t[a], a, t), i)
            } else if (t) for (var s in t) n.set(s, t[s]);
            return n
        }

        hr.prototype = pr.prototype = {
            constructor: hr, has: function (t) {
                return dr + t in this
            }, get: function (t) {
                return this[dr + t]
            }, set: function (t, e) {
                return this[dr + t] = e, this
            }, remove: function (t) {
                var e = dr + t;
                return e in this && delete this[e]
            }, clear: function () {
                for (var t in this) t[0] === dr && delete this[t]
            }, keys: function () {
                var t = [];
                for (var e in this) e[0] === dr && t.push(e.slice(1));
                return t
            }, values: function () {
                var t = [];
                for (var e in this) e[0] === dr && t.push(this[e]);
                return t
            }, entries: function () {
                var t = [];
                for (var e in this) e[0] === dr && t.push({key: e.slice(1), value: this[e]});
                return t
            }, size: function () {
                var t = 0;
                for (var e in this) e[0] === dr && ++t;
                return t
            }, empty: function () {
                for (var t in this) if (t[0] === dr) return !1;
                return !0
            }, each: function (t) {
                for (var e in this) e[0] === dr && t(this[e], e.slice(1), this)
            }
        };
        var br = pr;

        function vr() {
        }

        var _r = br.prototype;

        function gr(t, e) {
            var n = new vr;
            if (t instanceof vr) t.each(function (t) {
                n.add(t)
            }); else if (t) {
                var i = -1, a = t.length;
                if (null == e) while (++i < a) n.add(t[i]); else while (++i < a) n.add(e(t[i], i, t))
            }
            return n
        }

        vr.prototype = gr.prototype = {
            constructor: vr, has: _r.has, add: function (t) {
                return t += "", this[dr + t] = t, this
            }, remove: _r.remove, clear: _r.clear, values: _r.keys, size: _r.size, empty: _r.empty, each: _r.each
        };
        var mr = Array.prototype;
        mr.slice;
        var yr = {}, xr = {}, wr = 34, Or = 10, Mr = 13;

        function Cr(t) {
            return new Function("d", "return {" + t.map(function (t, e) {
                return JSON.stringify(t) + ": d[" + e + "]"
            }).join(",") + "}")
        }

        function kr(t, e) {
            var n = Cr(t);
            return function (i, a) {
                return e(n(i), a, t)
            }
        }

        function Ar(t) {
            var e = Object.create(null), n = [];
            return t.forEach(function (t) {
                for (var i in t) i in e || n.push(e[i] = i)
            }), n
        }

        function Tr(t, e) {
            var n = t + "", i = n.length;
            return i < e ? new Array(e - i + 1).join(0) + n : n
        }

        function Nr(t) {
            return t < 0 ? "-" + Tr(-t, 6) : t > 9999 ? "+" + Tr(t, 6) : Tr(t, 4)
        }

        function Pr(t) {
            var e = t.getUTCHours(), n = t.getUTCMinutes(), i = t.getUTCSeconds(), a = t.getUTCMilliseconds();
            return isNaN(t) ? "Invalid Date" : Nr(t.getUTCFullYear(), 4) + "-" + Tr(t.getUTCMonth() + 1, 2) + "-" + Tr(t.getUTCDate(), 2) + (a ? "T" + Tr(e, 2) + ":" + Tr(n, 2) + ":" + Tr(i, 2) + "." + Tr(a, 3) + "Z" : i ? "T" + Tr(e, 2) + ":" + Tr(n, 2) + ":" + Tr(i, 2) + "Z" : n || e ? "T" + Tr(e, 2) + ":" + Tr(n, 2) + "Z" : "")
        }

        var Sr = function (t) {
                var e = new RegExp('["' + t + "\n\r]"), n = t.charCodeAt(0);

                function i(t, e) {
                    var n, i, r = a(t, function (t, a) {
                        if (n) return n(t, a - 1);
                        i = t, n = e ? kr(t, e) : Cr(t)
                    });
                    return r.columns = i || [], r
                }

                function a(t, e) {
                    var i, a = [], r = t.length, s = 0, o = 0, c = r <= 0, u = !1;

                    function l() {
                        if (c) return xr;
                        if (u) return u = !1, yr;
                        var e, i, a = s;
                        if (t.charCodeAt(a) === wr) {
                            while (s++ < r && t.charCodeAt(s) !== wr || t.charCodeAt(++s) === wr) ;
                            return (e = s) >= r ? c = !0 : (i = t.charCodeAt(s++)) === Or ? u = !0 : i === Mr && (u = !0, t.charCodeAt(s) === Or && ++s), t.slice(a + 1, e - 1).replace(/""/g, '"')
                        }
                        while (s < r) {
                            if ((i = t.charCodeAt(e = s++)) === Or) u = !0; else if (i === Mr) u = !0, t.charCodeAt(s) === Or && ++s; else if (i !== n) continue;
                            return t.slice(a, e)
                        }
                        return c = !0, t.slice(a, r)
                    }

                    t.charCodeAt(r - 1) === Or && --r, t.charCodeAt(r - 1) === Mr && --r;
                    while ((i = l()) !== xr) {
                        var f = [];
                        while (i !== yr && i !== xr) f.push(i), i = l();
                        e && null == (f = e(f, o++)) || a.push(f)
                    }
                    return a
                }

                function r(e, n) {
                    return e.map(function (e) {
                        return n.map(function (t) {
                            return l(e[t])
                        }).join(t)
                    })
                }

                function s(e, n) {
                    return null == n && (n = Ar(e)), [n.map(l).join(t)].concat(r(e, n)).join("\n")
                }

                function o(t, e) {
                    return null == e && (e = Ar(t)), r(t, e).join("\n")
                }

                function c(t) {
                    return t.map(u).join("\n")
                }

                function u(e) {
                    return e.map(l).join(t)
                }

                function l(t) {
                    return null == t ? "" : t instanceof Date ? Pr(t) : e.test(t += "") ? '"' + t.replace(/"/g, '""') + '"' : t
                }

                return {parse: i, parseRows: a, format: s, formatBody: o, formatRows: c}
            }, $r = Sr(","), Er = $r.parse, jr = ($r.parseRows, $r.format, $r.formatBody, $r.formatRows, Sr("\t")),
            Ir = jr.parse;
        jr.parseRows, jr.format, jr.formatBody, jr.formatRows;

        function Lr(t) {
            if (!t.ok) throw new Error(t.status + " " + t.statusText);
            return t.text()
        }

        var Ur = function (t, e) {
            return fetch(t, e).then(Lr)
        };

        function Rr(t) {
            return function (e, n, i) {
                return 2 === arguments.length && "function" === typeof n && (i = n, n = void 0), Ur(e, n).then(function (e) {
                    return t(e, i)
                })
            }
        }

        Rr(Er), Rr(Ir);

        function Dr(t) {
            return function (e, n) {
                return Ur(e, n).then(function (e) {
                    return (new DOMParser).parseFromString(e, t)
                })
            }
        }

        Dr("application/xml"), Dr("text/html"), Dr("image/svg+xml");
        var Vr = function (t) {
            var e = +this._x.call(null, t), n = +this._y.call(null, t);
            return Br(this.cover(e, n), e, n, t)
        };

        function Br(t, e, n, i) {
            if (isNaN(e) || isNaN(n)) return t;
            var a, r, s, o, c, u, l, f, d, h = t._root, p = {data: i}, b = t._x0, v = t._y0, _ = t._x1, g = t._y1;
            if (!h) return t._root = p, t;
            while (h.length) if ((u = e >= (r = (b + _) / 2)) ? b = r : _ = r, (l = n >= (s = (v + g) / 2)) ? v = s : g = s, a = h, !(h = h[f = l << 1 | u])) return a[f] = p, t;
            if (o = +t._x.call(null, h.data), c = +t._y.call(null, h.data), e === o && n === c) return p.next = h, a ? a[f] = p : t._root = p, t;
            do {
                a = a ? a[f] = new Array(4) : t._root = new Array(4), (u = e >= (r = (b + _) / 2)) ? b = r : _ = r, (l = n >= (s = (v + g) / 2)) ? v = s : g = s
            } while ((f = l << 1 | u) === (d = (c >= s) << 1 | o >= r));
            return a[d] = h, a[f] = p, t
        }

        function Fr(t) {
            var e, n, i, a, r = t.length, s = new Array(r), o = new Array(r), c = 1 / 0, u = 1 / 0, l = -1 / 0,
                f = -1 / 0;
            for (n = 0; n < r; ++n) isNaN(i = +this._x.call(null, e = t[n])) || isNaN(a = +this._y.call(null, e)) || (s[n] = i, o[n] = a, i < c && (c = i), i > l && (l = i), a < u && (u = a), a > f && (f = a));
            if (c > l || u > f) return this;
            for (this.cover(c, u).cover(l, f), n = 0; n < r; ++n) Br(this, s[n], o[n], t[n]);
            return this
        }

        var Hr = function (t, e) {
            if (isNaN(t = +t) || isNaN(e = +e)) return this;
            var n = this._x0, i = this._y0, a = this._x1, r = this._y1;
            if (isNaN(n)) a = (n = Math.floor(t)) + 1, r = (i = Math.floor(e)) + 1; else {
                var s, o, c = a - n, u = this._root;
                while (n > t || t >= a || i > e || e >= r) switch (o = (e < i) << 1 | t < n, s = new Array(4), s[o] = u, u = s, c *= 2, o) {
                    case 0:
                        a = n + c, r = i + c;
                        break;
                    case 1:
                        n = a - c, r = i + c;
                        break;
                    case 2:
                        a = n + c, i = r - c;
                        break;
                    case 3:
                        n = a - c, i = r - c;
                        break
                }
                this._root && this._root.length && (this._root = u)
            }
            return this._x0 = n, this._y0 = i, this._x1 = a, this._y1 = r, this
        }, qr = function () {
            var t = [];
            return this.visit(function (e) {
                if (!e.length) do {
                    t.push(e.data)
                } while (e = e.next)
            }), t
        }, Gr = function (t) {
            return arguments.length ? this.cover(+t[0][0], +t[0][1]).cover(+t[1][0], +t[1][1]) : isNaN(this._x0) ? void 0 : [[this._x0, this._y0], [this._x1, this._y1]]
        }, zr = function (t, e, n, i, a) {
            this.node = t, this.x0 = e, this.y0 = n, this.x1 = i, this.y1 = a
        }, Yr = function (t, e, n) {
            var i, a, r, s, o, c, u, l = this._x0, f = this._y0, d = this._x1, h = this._y1, p = [], b = this._root;
            b && p.push(new zr(b, l, f, d, h)), null == n ? n = 1 / 0 : (l = t - n, f = e - n, d = t + n, h = e + n, n *= n);
            while (c = p.pop()) if (!(!(b = c.node) || (a = c.x0) > d || (r = c.y0) > h || (s = c.x1) < l || (o = c.y1) < f)) if (b.length) {
                var v = (a + s) / 2, _ = (r + o) / 2;
                p.push(new zr(b[3], v, _, s, o), new zr(b[2], a, _, v, o), new zr(b[1], v, r, s, _), new zr(b[0], a, r, v, _)), (u = (e >= _) << 1 | t >= v) && (c = p[p.length - 1], p[p.length - 1] = p[p.length - 1 - u], p[p.length - 1 - u] = c)
            } else {
                var g = t - +this._x.call(null, b.data), m = e - +this._y.call(null, b.data), y = g * g + m * m;
                if (y < n) {
                    var x = Math.sqrt(n = y);
                    l = t - x, f = e - x, d = t + x, h = e + x, i = b.data
                }
            }
            return i
        }, Wr = function (t) {
            if (isNaN(r = +this._x.call(null, t)) || isNaN(s = +this._y.call(null, t))) return this;
            var e, n, i, a, r, s, o, c, u, l, f, d, h = this._root, p = this._x0, b = this._y0, v = this._x1,
                _ = this._y1;
            if (!h) return this;
            if (h.length) while (1) {
                if ((u = r >= (o = (p + v) / 2)) ? p = o : v = o, (l = s >= (c = (b + _) / 2)) ? b = c : _ = c, e = h, !(h = h[f = l << 1 | u])) return this;
                if (!h.length) break;
                (e[f + 1 & 3] || e[f + 2 & 3] || e[f + 3 & 3]) && (n = e, d = f)
            }
            while (h.data !== t) if (i = h, !(h = h.next)) return this;
            return (a = h.next) && delete h.next, i ? (a ? i.next = a : delete i.next, this) : e ? (a ? e[f] = a : delete e[f], (h = e[0] || e[1] || e[2] || e[3]) && h === (e[3] || e[2] || e[1] || e[0]) && !h.length && (n ? n[d] = h : this._root = h), this) : (this._root = a, this)
        };

        function Xr(t) {
            for (var e = 0, n = t.length; e < n; ++e) this.remove(t[e]);
            return this
        }

        var Kr = function () {
            return this._root
        }, Zr = function () {
            var t = 0;
            return this.visit(function (e) {
                if (!e.length) do {
                    ++t
                } while (e = e.next)
            }), t
        }, Jr = function (t) {
            var e, n, i, a, r, s, o = [], c = this._root;
            c && o.push(new zr(c, this._x0, this._y0, this._x1, this._y1));
            while (e = o.pop()) if (!t(c = e.node, i = e.x0, a = e.y0, r = e.x1, s = e.y1) && c.length) {
                var u = (i + r) / 2, l = (a + s) / 2;
                (n = c[3]) && o.push(new zr(n, u, l, r, s)), (n = c[2]) && o.push(new zr(n, i, l, u, s)), (n = c[1]) && o.push(new zr(n, u, a, r, l)), (n = c[0]) && o.push(new zr(n, i, a, u, l))
            }
            return this
        }, Qr = function (t) {
            var e, n = [], i = [];
            this._root && n.push(new zr(this._root, this._x0, this._y0, this._x1, this._y1));
            while (e = n.pop()) {
                var a = e.node;
                if (a.length) {
                    var r, s = e.x0, o = e.y0, c = e.x1, u = e.y1, l = (s + c) / 2, f = (o + u) / 2;
                    (r = a[0]) && n.push(new zr(r, s, o, l, f)), (r = a[1]) && n.push(new zr(r, l, o, c, f)), (r = a[2]) && n.push(new zr(r, s, f, l, u)), (r = a[3]) && n.push(new zr(r, l, f, c, u))
                }
                i.push(e)
            }
            while (e = i.pop()) t(e.node, e.x0, e.y0, e.x1, e.y1);
            return this
        };

        function ts(t) {
            return t[0]
        }

        var es = function (t) {
            return arguments.length ? (this._x = t, this) : this._x
        };

        function ns(t) {
            return t[1]
        }

        var is = function (t) {
            return arguments.length ? (this._y = t, this) : this._y
        };

        function as(t, e, n) {
            var i = new rs(null == e ? ts : e, null == n ? ns : n, NaN, NaN, NaN, NaN);
            return null == t ? i : i.addAll(t)
        }

        function rs(t, e, n, i, a, r) {
            this._x = t, this._y = e, this._x0 = n, this._y0 = i, this._x1 = a, this._y1 = r, this._root = void 0
        }

        function ss(t) {
            var e = {data: t.data}, n = e;
            while (t = t.next) n = n.next = {data: t.data};
            return e
        }

        var os = as.prototype = rs.prototype;
        os.copy = function () {
            var t, e, n = new rs(this._x, this._y, this._x0, this._y0, this._x1, this._y1), i = this._root;
            if (!i) return n;
            if (!i.length) return n._root = ss(i), n;
            t = [{source: i, target: n._root = new Array(4)}];
            while (i = t.pop()) for (var a = 0; a < 4; ++a) (e = i.source[a]) && (e.length ? t.push({
                source: e,
                target: i.target[a] = new Array(4)
            }) : i.target[a] = ss(e));
            return n
        }, os.add = Vr, os.addAll = Fr, os.cover = Hr, os.data = qr, os.extent = Gr, os.find = Yr, os.remove = Wr, os.removeAll = Xr, os.root = Kr, os.size = Zr, os.visit = Jr, os.visitAfter = Qr, os.x = es, os.y = is;
        Math.PI, Math.sqrt(5);
        var cs = function (t, e) {
            if ((n = (t = e ? t.toExponential(e - 1) : t.toExponential()).indexOf("e")) < 0) return null;
            var n, i = t.slice(0, n);
            return [i.length > 1 ? i[0] + i.slice(2) : i, +t.slice(n + 1)]
        }, us = function (t) {
            return t = cs(Math.abs(t)), t ? t[1] : NaN
        }, ls = function (t, e) {
            return function (n, i) {
                var a = n.length, r = [], s = 0, o = t[0], c = 0;
                while (a > 0 && o > 0) {
                    if (c + o + 1 > i && (o = Math.max(1, i - c)), r.push(n.substring(a -= o, a + o)), (c += o + 1) > i) break;
                    o = t[s = (s + 1) % t.length]
                }
                return r.reverse().join(e)
            }
        }, fs = function (t) {
            return function (e) {
                return e.replace(/[0-9]/g, function (e) {
                    return t[+e]
                })
            }
        }, ds = /^(?:(.)?([<>=^]))?([+\-( ])?([$#])?(0)?(\d+)?(,)?(\.\d+)?(~)?([a-z%])?$/i;

        function hs(t) {
            return new ps(t)
        }

        function ps(t) {
            if (!(e = ds.exec(t))) throw new Error("invalid format: " + t);
            var e;
            this.fill = e[1] || " ", this.align = e[2] || ">", this.sign = e[3] || "-", this.symbol = e[4] || "", this.zero = !!e[5], this.width = e[6] && +e[6], this.comma = !!e[7], this.precision = e[8] && +e[8].slice(1), this.trim = !!e[9], this.type = e[10] || ""
        }

        hs.prototype = ps.prototype, ps.prototype.toString = function () {
            return this.fill + this.align + this.sign + this.symbol + (this.zero ? "0" : "") + (null == this.width ? "" : Math.max(1, 0 | this.width)) + (this.comma ? "," : "") + (null == this.precision ? "" : "." + Math.max(0, 0 | this.precision)) + (this.trim ? "~" : "") + this.type
        };
        var bs, vs, _s, gs, ms = function (t) {
                t:for (var e, n = t.length, i = 1, a = -1; i < n; ++i) switch (t[i]) {
                    case".":
                        a = e = i;
                        break;
                    case"0":
                        0 === a && (a = i), e = i;
                        break;
                    default:
                        if (a > 0) {
                            if (!+t[i]) break t;
                            a = 0
                        }
                        break
                }
                return a > 0 ? t.slice(0, a) + t.slice(e + 1) : t
            }, ys = function (t, e) {
                var n = cs(t, e);
                if (!n) return t + "";
                var i = n[0], a = n[1], r = a - (bs = 3 * Math.max(-8, Math.min(8, Math.floor(a / 3)))) + 1, s = i.length;
                return r === s ? i : r > s ? i + new Array(r - s + 1).join("0") : r > 0 ? i.slice(0, r) + "." + i.slice(r) : "0." + new Array(1 - r).join("0") + cs(t, Math.max(0, e + r - 1))[0]
            }, xs = function (t, e) {
                var n = cs(t, e);
                if (!n) return t + "";
                var i = n[0], a = n[1];
                return a < 0 ? "0." + new Array(-a).join("0") + i : i.length > a + 1 ? i.slice(0, a + 1) + "." + i.slice(a + 1) : i + new Array(a - i.length + 2).join("0")
            }, ws = {
                "%": function (t, e) {
                    return (100 * t).toFixed(e)
                }, b: function (t) {
                    return Math.round(t).toString(2)
                }, c: function (t) {
                    return t + ""
                }, d: function (t) {
                    return Math.round(t).toString(10)
                }, e: function (t, e) {
                    return t.toExponential(e)
                }, f: function (t, e) {
                    return t.toFixed(e)
                }, g: function (t, e) {
                    return t.toPrecision(e)
                }, o: function (t) {
                    return Math.round(t).toString(8)
                }, p: function (t, e) {
                    return xs(100 * t, e)
                }, r: xs, s: ys, X: function (t) {
                    return Math.round(t).toString(16).toUpperCase()
                }, x: function (t) {
                    return Math.round(t).toString(16)
                }
            }, Os = function (t) {
                return t
            }, Ms = ["y", "z", "a", "f", "p", "n", "Âµ", "m", "", "k", "M", "G", "T", "P", "E", "Z", "Y"],
            Cs = function (t) {
                var e = t.grouping && t.thousands ? ls(t.grouping, t.thousands) : Os, n = t.currency, i = t.decimal,
                    a = t.numerals ? fs(t.numerals) : Os, r = t.percent || "%";

                function s(t) {
                    t = hs(t);
                    var s = t.fill, o = t.align, c = t.sign, u = t.symbol, l = t.zero, f = t.width, d = t.comma,
                        h = t.precision, p = t.trim, b = t.type;
                    "n" === b ? (d = !0, b = "g") : ws[b] || (null == h && (h = 12), p = !0, b = "g"), (l || "0" === s && "=" === o) && (l = !0, s = "0", o = "=");
                    var v = "$" === u ? n[0] : "#" === u && /[boxX]/.test(b) ? "0" + b.toLowerCase() : "",
                        _ = "$" === u ? n[1] : /[%p]/.test(b) ? r : "", g = ws[b], m = /[defgprs%]/.test(b);

                    function y(t) {
                        var n, r, u, y = v, x = _;
                        if ("c" === b) x = g(t) + x, t = ""; else {
                            t = +t;
                            var w = t < 0;
                            if (t = g(Math.abs(t), h), p && (t = ms(t)), w && 0 === +t && (w = !1), y = (w ? "(" === c ? c : "-" : "-" === c || "(" === c ? "" : c) + y, x = ("s" === b ? Ms[8 + bs / 3] : "") + x + (w && "(" === c ? ")" : ""), m) {
                                n = -1, r = t.length;
                                while (++n < r) if (u = t.charCodeAt(n), 48 > u || u > 57) {
                                    x = (46 === u ? i + t.slice(n + 1) : t.slice(n)) + x, t = t.slice(0, n);
                                    break
                                }
                            }
                        }
                        d && !l && (t = e(t, 1 / 0));
                        var O = y.length + t.length + x.length, M = O < f ? new Array(f - O + 1).join(s) : "";
                        switch (d && l && (t = e(M + t, M.length ? f - x.length : 1 / 0), M = ""), o) {
                            case"<":
                                t = y + t + x + M;
                                break;
                            case"=":
                                t = y + M + t + x;
                                break;
                            case"^":
                                t = M.slice(0, O = M.length >> 1) + y + t + x + M.slice(O);
                                break;
                            default:
                                t = M + y + t + x;
                                break
                        }
                        return a(t)
                    }

                    return h = null == h ? 6 : /[gprs]/.test(b) ? Math.max(1, Math.min(21, h)) : Math.max(0, Math.min(20, h)), y.toString = function () {
                        return t + ""
                    }, y
                }

                function o(t, e) {
                    var n = s((t = hs(t), t.type = "f", t)), i = 3 * Math.max(-8, Math.min(8, Math.floor(us(e) / 3))),
                        a = Math.pow(10, -i), r = Ms[8 + i / 3];
                    return function (t) {
                        return n(a * t) + r
                    }
                }

                return {format: s, formatPrefix: o}
            };

        function ks(t) {
            return vs = Cs(t), _s = vs.format, gs = vs.formatPrefix, vs
        }

        ks({decimal: ".", thousands: ",", grouping: [3], currency: ["$", ""]});
        var As = function (t) {
            return Math.max(0, -us(Math.abs(t)))
        }, Ts = function (t, e) {
            return Math.max(0, 3 * Math.max(-8, Math.min(8, Math.floor(us(e) / 3))) - us(Math.abs(t)))
        }, Ns = function (t, e) {
            return t = Math.abs(t), e = Math.abs(e) - t, Math.max(0, us(e) - us(t)) + 1
        }, Ps = function () {
            return new Ss
        };

        function Ss() {
            this.reset()
        }

        Ss.prototype = {
            constructor: Ss, reset: function () {
                this.s = this.t = 0
            }, add: function (t) {
                Es($s, t, this.t), Es(this, $s.s, this.s), this.s ? this.t += $s.t : this.s = $s.t
            }, valueOf: function () {
                return this.s
            }
        };
        var $s = new Ss;

        function Es(t, e, n) {
            var i = t.s = e + n, a = i - e, r = i - a;
            t.t = e - r + (n - a)
        }

        var js = 1e-6, Is = 1e-12, Ls = Math.PI, Us = Ls / 2, Rs = Ls / 4, Ds = 2 * Ls, Vs = Ls / 180, Bs = Math.abs,
            Fs = Math.atan, Hs = Math.atan2, qs = Math.cos, Gs = (Math.ceil, Math.exp), zs = (Math.floor, Math.log),
            Ys = (Math.pow, Math.sin), Ws = (Math.sign, Math.sqrt), Xs = Math.tan;

        function Ks(t) {
            return t > 1 ? 0 : t < -1 ? Ls : Math.acos(t)
        }

        function Zs(t) {
            return t > 1 ? Us : t < -1 ? -Us : Math.asin(t)
        }

        function Js() {
        }

        Ps(), Ps();

        function Qs(t) {
            var e = t[0], n = t[1], i = qs(n);
            return [i * qs(e), i * Ys(e), Ys(n)]
        }

        function to(t, e) {
            return [t[1] * e[2] - t[2] * e[1], t[2] * e[0] - t[0] * e[2], t[0] * e[1] - t[1] * e[0]]
        }

        function eo(t) {
            var e = Ws(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
            t[0] /= e, t[1] /= e, t[2] /= e
        }

        Ps();

        function no(t, e) {
            return [Bs(t) > Ls ? t + Math.round(-t / Ds) * Ds : t, e]
        }

        no.invert = no;
        var io = function () {
            var t, e = [];
            return {
                point: function (e, n) {
                    t.push([e, n])
                }, lineStart: function () {
                    e.push(t = [])
                }, lineEnd: Js, rejoin: function () {
                    e.length > 1 && e.push(e.pop().concat(e.shift()))
                }, result: function () {
                    var n = e;
                    return e = [], t = null, n
                }
            }
        }, ao = function (t, e) {
            return Bs(t[0] - e[0]) < js && Bs(t[1] - e[1]) < js
        };

        function ro(t, e, n, i) {
            this.x = t, this.z = e, this.o = n, this.e = i, this.v = !1, this.n = this.p = null
        }

        var so = function (t, e, n, i, a) {
            var r, s, o = [], c = [];
            if (t.forEach(function (t) {
                    if (!((e = t.length - 1) <= 0)) {
                        var e, n, i = t[0], s = t[e];
                        if (ao(i, s)) {
                            for (a.lineStart(), r = 0; r < e; ++r) a.point((i = t[r])[0], i[1]);
                            a.lineEnd()
                        } else o.push(n = new ro(i, t, null, !0)), c.push(n.o = new ro(i, null, n, !1)), o.push(n = new ro(s, t, null, !1)), c.push(n.o = new ro(s, null, n, !0))
                    }
                }), o.length) {
                for (c.sort(e), oo(o), oo(c), r = 0, s = c.length; r < s; ++r) c[r].e = n = !n;
                var u, l, f = o[0];
                while (1) {
                    var d = f, h = !0;
                    while (d.v) if ((d = d.n) === f) return;
                    u = d.z, a.lineStart();
                    do {
                        if (d.v = d.o.v = !0, d.e) {
                            if (h) for (r = 0, s = u.length; r < s; ++r) a.point((l = u[r])[0], l[1]); else i(d.x, d.n.x, 1, a);
                            d = d.n
                        } else {
                            if (h) for (u = d.p.z, r = u.length - 1; r >= 0; --r) a.point((l = u[r])[0], l[1]); else i(d.x, d.p.x, -1, a);
                            d = d.p
                        }
                        d = d.o, u = d.z, h = !h
                    } while (!d.v);
                    a.lineEnd()
                }
            }
        };

        function oo(t) {
            if (e = t.length) {
                var e, n, i = 0, a = t[0];
                while (++i < e) a.n = n = t[i], n.p = a, a = n;
                a.n = n = t[0], n.p = a
            }
        }

        var co = Ps(), uo = function (t, e) {
            var n = e[0], i = e[1], a = Ys(i), r = [Ys(n), -qs(n), 0], s = 0, o = 0;
            co.reset(), 1 === a ? i = Us + js : -1 === a && (i = -Us - js);
            for (var c = 0, u = t.length; c < u; ++c) if (f = (l = t[c]).length) for (var l, f, d = l[f - 1], h = d[0], p = d[1] / 2 + Rs, b = Ys(p), v = qs(p), _ = 0; _ < f; ++_, h = m, b = x, v = w, d = g) {
                var g = l[_], m = g[0], y = g[1] / 2 + Rs, x = Ys(y), w = qs(y), O = m - h, M = O >= 0 ? 1 : -1,
                    C = M * O, k = C > Ls, A = b * x;
                if (co.add(Hs(A * M * Ys(C), v * w + A * qs(C))), s += k ? O + M * Ds : O, k ^ h >= n ^ m >= n) {
                    var T = to(Qs(d), Qs(g));
                    eo(T);
                    var N = to(r, T);
                    eo(N);
                    var P = (k ^ O >= 0 ? -1 : 1) * Zs(N[2]);
                    (i > P || i === P && (T[0] || T[1])) && (o += k ^ O >= 0 ? 1 : -1)
                }
            }
            return (s < -js || s < js && co < -js) ^ 1 & o
        }, lo = function (t, e, n, i) {
            return function (a) {
                var r, s, o, c = e(a), u = io(), l = e(u), f = !1, d = {
                    point: h, lineStart: b, lineEnd: _, polygonStart: function () {
                        d.point = g, d.lineStart = m, d.lineEnd = y, s = [], r = []
                    }, polygonEnd: function () {
                        d.point = h, d.lineStart = b, d.lineEnd = _, s = v(s);
                        var t = uo(r, i);
                        s.length ? (f || (a.polygonStart(), f = !0), so(s, ho, t, n, a)) : t && (f || (a.polygonStart(), f = !0), a.lineStart(), n(null, null, 1, a), a.lineEnd()), f && (a.polygonEnd(), f = !1), s = r = null
                    }, sphere: function () {
                        a.polygonStart(), a.lineStart(), n(null, null, 1, a), a.lineEnd(), a.polygonEnd()
                    }
                };

                function h(e, n) {
                    t(e, n) && a.point(e, n)
                }

                function p(t, e) {
                    c.point(t, e)
                }

                function b() {
                    d.point = p, c.lineStart()
                }

                function _() {
                    d.point = h, c.lineEnd()
                }

                function g(t, e) {
                    o.push([t, e]), l.point(t, e)
                }

                function m() {
                    l.lineStart(), o = []
                }

                function y() {
                    g(o[0][0], o[0][1]), l.lineEnd();
                    var t, e, n, i, c = l.clean(), d = u.result(), h = d.length;
                    if (o.pop(), r.push(o), o = null, h) if (1 & c) {
                        if (n = d[0], (e = n.length - 1) > 0) {
                            for (f || (a.polygonStart(), f = !0), a.lineStart(), t = 0; t < e; ++t) a.point((i = n[t])[0], i[1]);
                            a.lineEnd()
                        }
                    } else h > 1 && 2 & c && d.push(d.pop().concat(d.shift())), s.push(d.filter(fo))
                }

                return d
            }
        };

        function fo(t) {
            return t.length > 1
        }

        function ho(t, e) {
            return ((t = t.x)[0] < 0 ? t[1] - Us - js : Us - t[1]) - ((e = e.x)[0] < 0 ? e[1] - Us - js : Us - e[1])
        }

        lo(function () {
            return !0
        }, po, vo, [-Ls, -Us]);

        function po(t) {
            var e, n = NaN, i = NaN, a = NaN;
            return {
                lineStart: function () {
                    t.lineStart(), e = 1
                }, point: function (r, s) {
                    var o = r > 0 ? Ls : -Ls, c = Bs(r - n);
                    Bs(c - Ls) < js ? (t.point(n, i = (i + s) / 2 > 0 ? Us : -Us), t.point(a, i), t.lineEnd(), t.lineStart(), t.point(o, i), t.point(r, i), e = 0) : a !== o && c >= Ls && (Bs(n - a) < js && (n -= a * js), Bs(r - o) < js && (r -= o * js), i = bo(n, i, r, s), t.point(a, i), t.lineEnd(), t.lineStart(), t.point(o, i), e = 0), t.point(n = r, i = s), a = o
                }, lineEnd: function () {
                    t.lineEnd(), n = i = NaN
                }, clean: function () {
                    return 2 - e
                }
            }
        }

        function bo(t, e, n, i) {
            var a, r, s = Ys(t - n);
            return Bs(s) > js ? Fs((Ys(e) * (r = qs(i)) * Ys(n) - Ys(i) * (a = qs(e)) * Ys(t)) / (a * r * s)) : (e + i) / 2
        }

        function vo(t, e, n, i) {
            var a;
            if (null == t) a = n * Us, i.point(-Ls, a), i.point(0, a), i.point(Ls, a), i.point(Ls, 0), i.point(Ls, -a), i.point(0, -a), i.point(-Ls, -a), i.point(-Ls, 0), i.point(-Ls, a); else if (Bs(t[0] - e[0]) > js) {
                var r = t[0] < e[0] ? Ls : -Ls;
                a = n * r / 2, i.point(-r, a), i.point(0, a), i.point(r, a)
            } else i.point(e[0], e[1])
        }

        Ps();
        Ps(), Ps();

        function _o(t) {
            this._context = t
        }

        _o.prototype = {
            _radius: 4.5, pointRadius: function (t) {
                return this._radius = t, this
            }, polygonStart: function () {
                this._line = 0
            }, polygonEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._point = 0
            }, lineEnd: function () {
                0 === this._line && this._context.closePath(), this._point = NaN
            }, point: function (t, e) {
                switch (this._point) {
                    case 0:
                        this._context.moveTo(t, e), this._point = 1;
                        break;
                    case 1:
                        this._context.lineTo(t, e);
                        break;
                    default:
                        this._context.moveTo(t + this._radius, e), this._context.arc(t, e, this._radius, 0, Ds);
                        break
                }
            }, result: Js
        };
        Ps();

        function go() {
            this._string = []
        }

        function mo(t) {
            return "m0," + t + "a" + t + "," + t + " 0 1,1 0," + -2 * t + "a" + t + "," + t + " 0 1,1 0," + 2 * t + "z"
        }

        go.prototype = {
            _radius: 4.5, _circle: mo(4.5), pointRadius: function (t) {
                return (t = +t) !== this._radius && (this._radius = t, this._circle = null), this
            }, polygonStart: function () {
                this._line = 0
            }, polygonEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._point = 0
            }, lineEnd: function () {
                0 === this._line && this._string.push("Z"), this._point = NaN
            }, point: function (t, e) {
                switch (this._point) {
                    case 0:
                        this._string.push("M", t, ",", e), this._point = 1;
                        break;
                    case 1:
                        this._string.push("L", t, ",", e);
                        break;
                    default:
                        null == this._circle && (this._circle = mo(this._radius)), this._string.push("M", t, ",", e, this._circle);
                        break
                }
            }, result: function () {
                if (this._string.length) {
                    var t = this._string.join("");
                    return this._string = [], t
                }
                return null
            }
        };

        function yo(t) {
            return function (e) {
                var n = new xo;
                for (var i in t) n[i] = t[i];
                return n.stream = e, n
            }
        }

        function xo() {
        }

        xo.prototype = {
            constructor: xo, point: function (t, e) {
                this.stream.point(t, e)
            }, sphere: function () {
                this.stream.sphere()
            }, lineStart: function () {
                this.stream.lineStart()
            }, lineEnd: function () {
                this.stream.lineEnd()
            }, polygonStart: function () {
                this.stream.polygonStart()
            }, polygonEnd: function () {
                this.stream.polygonEnd()
            }
        };
        qs(30 * Vs);
        yo({
            point: function (t, e) {
                this.stream.point(t * Vs, e * Vs)
            }
        });

        function wo(t) {
            return function (e, n) {
                var i = qs(e), a = qs(n), r = t(i * a);
                return [r * a * Ys(e), r * Ys(n)]
            }
        }

        function Oo(t) {
            return function (e, n) {
                var i = Ws(e * e + n * n), a = t(i), r = Ys(a), s = qs(a);
                return [Hs(e * r, i * s), Zs(i && n * r / i)]
            }
        }

        var Mo = wo(function (t) {
            return Ws(2 / (1 + t))
        });
        Mo.invert = Oo(function (t) {
            return 2 * Zs(t / 2)
        });
        var Co = wo(function (t) {
            return (t = Ks(t)) && t / Ys(t)
        });
        Co.invert = Oo(function (t) {
            return t
        });

        function ko(t, e) {
            return [t, zs(Xs((Us + e) / 2))]
        }

        ko.invert = function (t, e) {
            return [t, 2 * Fs(Gs(e)) - Us]
        };

        function Ao(t, e) {
            return [t, e]
        }

        Ao.invert = Ao;
        var To = 1.340264, No = -.081106, Po = 893e-6, So = .003796, $o = Ws(3) / 2, Eo = 12;

        function jo(t, e) {
            var n = Zs($o * Ys(e)), i = n * n, a = i * i * i;
            return [t * qs(n) / ($o * (To + 3 * No * i + a * (7 * Po + 9 * So * i))), n * (To + No * i + a * (Po + So * i))]
        }

        jo.invert = function (t, e) {
            for (var n, i, a, r = e, s = r * r, o = s * s * s, c = 0; c < Eo; ++c) if (i = r * (To + No * s + o * (Po + So * s)) - e, a = To + 3 * No * s + o * (7 * Po + 9 * So * s), r -= n = i / a, s = r * r, o = s * s * s, Bs(n) < Is) break;
            return [$o * t * (To + 3 * No * s + o * (7 * Po + 9 * So * s)) / qs(r), Zs(Ys(r) / $o)]
        };

        function Io(t, e) {
            var n = qs(e), i = qs(t) * n;
            return [n * Ys(t) / i, Ys(e) / i]
        }

        Io.invert = Oo(Fs);

        function Lo(t, e) {
            var n = e * e, i = n * n;
            return [t * (.8707 - .131979 * n + i * (i * (.003971 * n - .001529 * i) - .013791)), e * (1.007226 + n * (.015085 + i * (.028874 * n - .044475 - .005916 * i)))]
        }

        Lo.invert = function (t, e) {
            var n, i = e, a = 25;
            do {
                var r = i * i, s = r * r;
                i -= n = (i * (1.007226 + r * (.015085 + s * (.028874 * r - .044475 - .005916 * s))) - e) / (1.007226 + r * (.045255 + s * (.259866 * r - .311325 - .005916 * 11 * s)))
            } while (Bs(n) > js && --a > 0);
            return [t / (.8707 + (r = i * i) * (r * (r * r * r * (.003971 - .001529 * r) - .013791) - .131979)), i]
        };

        function Uo(t, e) {
            return [qs(e) * Ys(t), Ys(e)]
        }

        Uo.invert = Oo(Zs);

        function Ro(t, e) {
            var n = qs(e), i = 1 + qs(t) * n;
            return [n * Ys(t) / i, Ys(e) / i]
        }

        Ro.invert = Oo(function (t) {
            return 2 * Fs(t)
        });

        function Do(t, e) {
            return [zs(Xs((Us + e) / 2)), -t]
        }

        Do.invert = function (t, e) {
            return [-e, 2 * Fs(Gs(t)) - Us]
        };

        function Vo(t) {
            var e = 0, n = t.children, i = n && n.length;
            if (i) while (--i >= 0) e += n[i].value; else e = 1;
            t.value = e
        }

        var Bo = function () {
            return this.eachAfter(Vo)
        }, Fo = function (t) {
            var e, n, i, a, r = this, s = [r];
            do {
                e = s.reverse(), s = [];
                while (r = e.pop()) if (t(r), n = r.children, n) for (i = 0, a = n.length; i < a; ++i) s.push(n[i])
            } while (s.length);
            return this
        }, Ho = function (t) {
            var e, n, i = this, a = [i];
            while (i = a.pop()) if (t(i), e = i.children, e) for (n = e.length - 1; n >= 0; --n) a.push(e[n]);
            return this
        }, qo = function (t) {
            var e, n, i, a = this, r = [a], s = [];
            while (a = r.pop()) if (s.push(a), e = a.children, e) for (n = 0, i = e.length; n < i; ++n) r.push(e[n]);
            while (a = s.pop()) t(a);
            return this
        }, Go = function (t) {
            return this.eachAfter(function (e) {
                var n = +t(e.data) || 0, i = e.children, a = i && i.length;
                while (--a >= 0) n += i[a].value;
                e.value = n
            })
        }, zo = function (t) {
            return this.eachBefore(function (e) {
                e.children && e.children.sort(t)
            })
        }, Yo = function (t) {
            var e = this, n = Wo(e, t), i = [e];
            while (e !== n) e = e.parent, i.push(e);
            var a = i.length;
            while (t !== n) i.splice(a, 0, t), t = t.parent;
            return i
        };

        function Wo(t, e) {
            if (t === e) return t;
            var n = t.ancestors(), i = e.ancestors(), a = null;
            t = n.pop(), e = i.pop();
            while (t === e) a = t, t = n.pop(), e = i.pop();
            return a
        }

        var Xo = function () {
            var t = this, e = [t];
            while (t = t.parent) e.push(t);
            return e
        }, Ko = function () {
            var t = [];
            return this.each(function (e) {
                t.push(e)
            }), t
        }, Zo = function () {
            var t = [];
            return this.eachBefore(function (e) {
                e.children || t.push(e)
            }), t
        }, Jo = function () {
            var t = this, e = [];
            return t.each(function (n) {
                n !== t && e.push({source: n.parent, target: n})
            }), e
        };

        function Qo(t, e) {
            var n, i, a, r, s, o = new ac(t), c = +t.value && (o.value = t.value), u = [o];
            null == e && (e = ec);
            while (n = u.pop()) if (c && (n.value = +n.data.value), (a = e(n.data)) && (s = a.length)) for (n.children = new Array(s), r = s - 1; r >= 0; --r) u.push(i = n.children[r] = new ac(a[r])), i.parent = n, i.depth = n.depth + 1;
            return o.eachBefore(ic)
        }

        function tc() {
            return Qo(this).eachBefore(nc)
        }

        function ec(t) {
            return t.children
        }

        function nc(t) {
            t.data = t.data.data
        }

        function ic(t) {
            var e = 0;
            do {
                t.height = e
            } while ((t = t.parent) && t.height < ++e)
        }

        function ac(t) {
            this.data = t, this.depth = this.height = 0, this.parent = null
        }

        ac.prototype = Qo.prototype = {
            constructor: ac,
            count: Bo,
            each: Fo,
            eachAfter: qo,
            eachBefore: Ho,
            sum: Go,
            sort: zo,
            path: Yo,
            ancestors: Xo,
            descendants: Ko,
            leaves: Zo,
            links: Jo,
            copy: tc
        };
        Array.prototype.slice;
        var rc = function (t, e, n, i, a) {
            var r, s = t.children, o = -1, c = s.length, u = t.value && (i - e) / t.value;
            while (++o < c) r = s[o], r.y0 = n, r.y1 = a, r.x0 = e, r.x1 = e += r.value * u
        };

        function sc(t, e) {
            this._ = t, this.parent = null, this.children = null, this.A = null, this.a = this, this.z = 0, this.m = 0, this.c = 0, this.s = 0, this.t = null, this.i = e
        }

        sc.prototype = Object.create(ac.prototype);
        var oc = function (t, e, n, i, a) {
            var r, s = t.children, o = -1, c = s.length, u = t.value && (a - n) / t.value;
            while (++o < c) r = s[o], r.x0 = e, r.x1 = i, r.y0 = n, r.y1 = n += r.value * u
        }, cc = (1 + Math.sqrt(5)) / 2;

        function uc(t, e, n, i, a, r) {
            var s, o, c, u, l, f, d, h, p, b, v, _ = [], g = e.children, m = 0, y = 0, x = g.length, w = e.value;
            while (m < x) {
                c = a - n, u = r - i;
                do {
                    l = g[y++].value
                } while (!l && y < x);
                for (f = d = l, b = Math.max(u / c, c / u) / (w * t), v = l * l * b, p = Math.max(d / v, v / f); y < x; ++y) {
                    if (l += o = g[y].value, o < f && (f = o), o > d && (d = o), v = l * l * b, h = Math.max(d / v, v / f), h > p) {
                        l -= o;
                        break
                    }
                    p = h
                }
                _.push(s = {
                    value: l,
                    dice: c < u,
                    children: g.slice(m, y)
                }), s.dice ? rc(s, n, i, a, w ? i += u * l / w : r) : oc(s, n, i, w ? n += c * l / w : a, r), w -= l, m = y
            }
            return _
        }

        (function t(e) {
            function n(t, n, i, a, r) {
                uc(e, t, n, i, a, r)
            }

            return n.ratio = function (e) {
                return t((e = +e) > 1 ? e : 1)
            }, n
        })(cc), function t(e) {
            function n(t, n, i, a, r) {
                if ((s = t._squarify) && s.ratio === e) {
                    var s, o, c, u, l, f = -1, d = s.length, h = t.value;
                    while (++f < d) {
                        for (o = s[f], c = o.children, u = o.value = 0, l = c.length; u < l; ++u) o.value += c[u].value;
                        o.dice ? rc(o, n, i, a, i += (r - i) * o.value / h) : oc(o, n, i, n += (a - n) * o.value / h, r), h -= o.value
                    }
                } else t._squarify = s = uc(e, t, n, i, a, r), s.ratio = e
            }

            return n.ratio = function (e) {
                return t((e = +e) > 1 ? e : 1)
            }, n
        }(cc);
        var lc = function () {
            return Math.random()
        }, fc = (function t(e) {
            function n(t, n) {
                return t = null == t ? 0 : +t, n = null == n ? 1 : +n, 1 === arguments.length ? (n = t, t = 0) : n -= t, function () {
                    return e() * n + t
                }
            }

            return n.source = t, n
        }(lc), function t(e) {
            function n(t, n) {
                var i, a;
                return t = null == t ? 0 : +t, n = null == n ? 1 : +n, function () {
                    var r;
                    if (null != i) r = i, i = null; else do {
                        i = 2 * e() - 1, r = 2 * e() - 1, a = i * i + r * r
                    } while (!a || a > 1);
                    return t + n * r * Math.sqrt(-2 * Math.log(a) / a)
                }
            }

            return n.source = t, n
        }(lc)), dc = (function t(e) {
            function n() {
                var t = fc.source(e).apply(this, arguments);
                return function () {
                    return Math.exp(t())
                }
            }

            return n.source = t, n
        }(lc), function t(e) {
            function n(t) {
                return function () {
                    for (var n = 0, i = 0; i < t; ++i) n += e();
                    return n
                }
            }

            return n.source = t, n
        }(lc));
        (function t(e) {
            function n(t) {
                var n = dc.source(e)(t);
                return function () {
                    return n() / t
                }
            }

            return n.source = t, n
        })(lc), function t(e) {
            function n(t) {
                return function () {
                    return -Math.log(1 - e()) / t
                }
            }

            return n.source = t, n
        }(lc);

        function hc(t, e) {
            switch (arguments.length) {
                case 0:
                    break;
                case 1:
                    this.range(t);
                    break;
                default:
                    this.range(e).domain(t);
                    break
            }
            return this
        }

        var pc = Array.prototype, bc = pc.map, vc = pc.slice;
        var _c = function (t) {
            return function () {
                return t
            }
        }, gc = function (t) {
            return +t
        }, mc = [0, 1];

        function yc(t) {
            return t
        }

        function xc(t, e) {
            return (e -= t = +t) ? function (n) {
                return (n - t) / e
            } : _c(isNaN(e) ? NaN : .5)
        }

        function wc(t) {
            var e, n = t[0], i = t[t.length - 1];
            return n > i && (e = n, n = i, i = e), function (t) {
                return Math.max(n, Math.min(i, t))
            }
        }

        function Oc(t, e, n) {
            var i = t[0], a = t[1], r = e[0], s = e[1];
            return a < i ? (i = xc(a, i), r = n(s, r)) : (i = xc(i, a), r = n(r, s)), function (t) {
                return r(i(t))
            }
        }

        function Mc(t, e, n) {
            var i = Math.min(t.length, e.length) - 1, a = new Array(i), r = new Array(i), s = -1;
            t[i] < t[0] && (t = t.slice().reverse(), e = e.slice().reverse());
            while (++s < i) a[s] = xc(t[s], t[s + 1]), r[s] = n(e[s], e[s + 1]);
            return function (e) {
                var n = c(t, e, 1, i) - 1;
                return r[n](a[n](e))
            }
        }

        function Cc(t, e) {
            return e.domain(t.domain()).range(t.range()).interpolate(t.interpolate()).clamp(t.clamp()).unknown(t.unknown())
        }

        function kc() {
            var t, e, n, i, a, r, s = mc, o = mc, c = Zn, u = yc;

            function l() {
                return i = Math.min(s.length, o.length) > 2 ? Mc : Oc, a = r = null, f
            }

            function f(e) {
                return isNaN(e = +e) ? n : (a || (a = i(s.map(t), o, c)))(t(u(e)))
            }

            return f.invert = function (n) {
                return u(e((r || (r = i(o, s.map(t), Vn)))(n)))
            }, f.domain = function (t) {
                return arguments.length ? (s = bc.call(t, gc), u === yc || (u = wc(s)), l()) : s.slice()
            }, f.range = function (t) {
                return arguments.length ? (o = vc.call(t), l()) : o.slice()
            }, f.rangeRound = function (t) {
                return o = vc.call(t), c = Jn, l()
            }, f.clamp = function (t) {
                return arguments.length ? (u = t ? wc(s) : yc, f) : u !== yc
            }, f.interpolate = function (t) {
                return arguments.length ? (c = t, l()) : c
            }, f.unknown = function (t) {
                return arguments.length ? (n = t, f) : n
            }, function (n, i) {
                return t = n, e = i, l()
            }
        }

        function Ac(t, e) {
            return kc()(t, e)
        }

        var Tc = function (t, e, n, i) {
            var a, r = b(t, e, n);
            switch (i = hs(null == i ? ",f" : i), i.type) {
                case"s":
                    var s = Math.max(Math.abs(t), Math.abs(e));
                    return null != i.precision || isNaN(a = Ts(r, s)) || (i.precision = a), gs(i, s);
                case"":
                case"e":
                case"g":
                case"p":
                case"r":
                    null != i.precision || isNaN(a = Ns(r, Math.max(Math.abs(t), Math.abs(e)))) || (i.precision = a - ("e" === i.type));
                    break;
                case"f":
                case"%":
                    null != i.precision || isNaN(a = As(r)) || (i.precision = a - 2 * ("%" === i.type));
                    break
            }
            return _s(i)
        };

        function Nc(t) {
            var e = t.domain;
            return t.ticks = function (t) {
                var n = e();
                return h(n[0], n[n.length - 1], null == t ? 10 : t)
            }, t.tickFormat = function (t, n) {
                var i = e();
                return Tc(i[0], i[i.length - 1], null == t ? 10 : t, n)
            }, t.nice = function (n) {
                null == n && (n = 10);
                var i, a = e(), r = 0, s = a.length - 1, o = a[r], c = a[s];
                return c < o && (i = o, o = c, c = i, i = r, r = s, s = i), i = p(o, c, n), i > 0 ? (o = Math.floor(o / i) * i, c = Math.ceil(c / i) * i, i = p(o, c, n)) : i < 0 && (o = Math.ceil(o * i) / i, c = Math.floor(c * i) / i, i = p(o, c, n)), i > 0 ? (a[r] = Math.floor(o / i) * i, a[s] = Math.ceil(c / i) * i, e(a)) : i < 0 && (a[r] = Math.ceil(o * i) / i, a[s] = Math.floor(c * i) / i, e(a)), t
            }, t
        }

        function Pc() {
            var t = Ac(yc, yc);
            return t.copy = function () {
                return Cc(t, Pc())
            }, hc.apply(t, arguments), Nc(t)
        }

        var Sc = new Date, $c = new Date;

        function Ec(t, e, n, i) {
            function a(e) {
                return t(e = new Date(+e)), e
            }

            return a.floor = a, a.ceil = function (n) {
                return t(n = new Date(n - 1)), e(n, 1), t(n), n
            }, a.round = function (t) {
                var e = a(t), n = a.ceil(t);
                return t - e < n - t ? e : n
            }, a.offset = function (t, n) {
                return e(t = new Date(+t), null == n ? 1 : Math.floor(n)), t
            }, a.range = function (n, i, r) {
                var s, o = [];
                if (n = a.ceil(n), r = null == r ? 1 : Math.floor(r), !(n < i) || !(r > 0)) return o;
                do {
                    o.push(s = new Date(+n)), e(n, r), t(n)
                } while (s < n && n < i);
                return o
            }, a.filter = function (n) {
                return Ec(function (e) {
                    if (e >= e) while (t(e), !n(e)) e.setTime(e - 1)
                }, function (t, i) {
                    if (t >= t) if (i < 0) while (++i <= 0) while (e(t, -1), !n(t)) ; else while (--i >= 0) while (e(t, 1), !n(t)) ;
                })
            }, n && (a.count = function (e, i) {
                return Sc.setTime(+e), $c.setTime(+i), t(Sc), t($c), Math.floor(n(Sc, $c))
            }, a.every = function (t) {
                return t = Math.floor(t), isFinite(t) && t > 0 ? t > 1 ? a.filter(i ? function (e) {
                    return i(e) % t === 0
                } : function (e) {
                    return a.count(0, e) % t === 0
                }) : a : null
            }), a
        }

        var jc = Ec(function () {
        }, function (t, e) {
            t.setTime(+t + e)
        }, function (t, e) {
            return e - t
        });
        jc.every = function (t) {
            return t = Math.floor(t), isFinite(t) && t > 0 ? t > 1 ? Ec(function (e) {
                e.setTime(Math.floor(e / t) * t)
            }, function (e, n) {
                e.setTime(+e + n * t)
            }, function (e, n) {
                return (n - e) / t
            }) : jc : null
        };
        jc.range;
        var Ic = 1e3, Lc = 6e4, Uc = 36e5, Rc = 864e5, Dc = 6048e5, Vc = Ec(function (t) {
            t.setTime(t - t.getMilliseconds())
        }, function (t, e) {
            t.setTime(+t + e * Ic)
        }, function (t, e) {
            return (e - t) / Ic
        }, function (t) {
            return t.getUTCSeconds()
        }), Bc = (Vc.range, Ec(function (t) {
            t.setTime(t - t.getMilliseconds() - t.getSeconds() * Ic)
        }, function (t, e) {
            t.setTime(+t + e * Lc)
        }, function (t, e) {
            return (e - t) / Lc
        }, function (t) {
            return t.getMinutes()
        })), Fc = (Bc.range, Ec(function (t) {
            t.setTime(t - t.getMilliseconds() - t.getSeconds() * Ic - t.getMinutes() * Lc)
        }, function (t, e) {
            t.setTime(+t + e * Uc)
        }, function (t, e) {
            return (e - t) / Uc
        }, function (t) {
            return t.getHours()
        })), Hc = (Fc.range, Ec(function (t) {
            t.setHours(0, 0, 0, 0)
        }, function (t, e) {
            t.setDate(t.getDate() + e)
        }, function (t, e) {
            return (e - t - (e.getTimezoneOffset() - t.getTimezoneOffset()) * Lc) / Rc
        }, function (t) {
            return t.getDate() - 1
        })), qc = Hc;
        Hc.range;

        function Gc(t) {
            return Ec(function (e) {
                e.setDate(e.getDate() - (e.getDay() + 7 - t) % 7), e.setHours(0, 0, 0, 0)
            }, function (t, e) {
                t.setDate(t.getDate() + 7 * e)
            }, function (t, e) {
                return (e - t - (e.getTimezoneOffset() - t.getTimezoneOffset()) * Lc) / Dc
            })
        }

        var zc = Gc(0), Yc = Gc(1), Wc = Gc(2), Xc = Gc(3), Kc = Gc(4), Zc = Gc(5), Jc = Gc(6),
            Qc = (zc.range, Yc.range, Wc.range, Xc.range, Kc.range, Zc.range, Jc.range, Ec(function (t) {
                t.setDate(1), t.setHours(0, 0, 0, 0)
            }, function (t, e) {
                t.setMonth(t.getMonth() + e)
            }, function (t, e) {
                return e.getMonth() - t.getMonth() + 12 * (e.getFullYear() - t.getFullYear())
            }, function (t) {
                return t.getMonth()
            })), tu = (Qc.range, Ec(function (t) {
                t.setMonth(0, 1), t.setHours(0, 0, 0, 0)
            }, function (t, e) {
                t.setFullYear(t.getFullYear() + e)
            }, function (t, e) {
                return e.getFullYear() - t.getFullYear()
            }, function (t) {
                return t.getFullYear()
            }));
        tu.every = function (t) {
            return isFinite(t = Math.floor(t)) && t > 0 ? Ec(function (e) {
                e.setFullYear(Math.floor(e.getFullYear() / t) * t), e.setMonth(0, 1), e.setHours(0, 0, 0, 0)
            }, function (e, n) {
                e.setFullYear(e.getFullYear() + n * t)
            }) : null
        };
        var eu = tu, nu = (tu.range, Ec(function (t) {
            t.setUTCSeconds(0, 0)
        }, function (t, e) {
            t.setTime(+t + e * Lc)
        }, function (t, e) {
            return (e - t) / Lc
        }, function (t) {
            return t.getUTCMinutes()
        })), iu = (nu.range, Ec(function (t) {
            t.setUTCMinutes(0, 0, 0)
        }, function (t, e) {
            t.setTime(+t + e * Uc)
        }, function (t, e) {
            return (e - t) / Uc
        }, function (t) {
            return t.getUTCHours()
        })), au = (iu.range, Ec(function (t) {
            t.setUTCHours(0, 0, 0, 0)
        }, function (t, e) {
            t.setUTCDate(t.getUTCDate() + e)
        }, function (t, e) {
            return (e - t) / Rc
        }, function (t) {
            return t.getUTCDate() - 1
        })), ru = au;
        au.range;

        function su(t) {
            return Ec(function (e) {
                e.setUTCDate(e.getUTCDate() - (e.getUTCDay() + 7 - t) % 7), e.setUTCHours(0, 0, 0, 0)
            }, function (t, e) {
                t.setUTCDate(t.getUTCDate() + 7 * e)
            }, function (t, e) {
                return (e - t) / Dc
            })
        }

        var ou = su(0), cu = su(1), uu = su(2), lu = su(3), fu = su(4), du = su(5), hu = su(6),
            pu = (ou.range, cu.range, uu.range, lu.range, fu.range, du.range, hu.range, Ec(function (t) {
                t.setUTCDate(1), t.setUTCHours(0, 0, 0, 0)
            }, function (t, e) {
                t.setUTCMonth(t.getUTCMonth() + e)
            }, function (t, e) {
                return e.getUTCMonth() - t.getUTCMonth() + 12 * (e.getUTCFullYear() - t.getUTCFullYear())
            }, function (t) {
                return t.getUTCMonth()
            })), bu = (pu.range, Ec(function (t) {
                t.setUTCMonth(0, 1), t.setUTCHours(0, 0, 0, 0)
            }, function (t, e) {
                t.setUTCFullYear(t.getUTCFullYear() + e)
            }, function (t, e) {
                return e.getUTCFullYear() - t.getUTCFullYear()
            }, function (t) {
                return t.getUTCFullYear()
            }));
        bu.every = function (t) {
            return isFinite(t = Math.floor(t)) && t > 0 ? Ec(function (e) {
                e.setUTCFullYear(Math.floor(e.getUTCFullYear() / t) * t), e.setUTCMonth(0, 1), e.setUTCHours(0, 0, 0, 0)
            }, function (e, n) {
                e.setUTCFullYear(e.getUTCFullYear() + n * t)
            }) : null
        };
        var vu = bu;
        bu.range;

        function _u(t) {
            if (0 <= t.y && t.y < 100) {
                var e = new Date(-1, t.m, t.d, t.H, t.M, t.S, t.L);
                return e.setFullYear(t.y), e
            }
            return new Date(t.y, t.m, t.d, t.H, t.M, t.S, t.L)
        }

        function gu(t) {
            if (0 <= t.y && t.y < 100) {
                var e = new Date(Date.UTC(-1, t.m, t.d, t.H, t.M, t.S, t.L));
                return e.setUTCFullYear(t.y), e
            }
            return new Date(Date.UTC(t.y, t.m, t.d, t.H, t.M, t.S, t.L))
        }

        function mu(t) {
            return {y: t, m: 0, d: 1, H: 0, M: 0, S: 0, L: 0}
        }

        function yu(t) {
            var e = t.dateTime, n = t.date, i = t.time, a = t.periods, r = t.days, s = t.shortDays, o = t.months,
                c = t.shortMonths, u = Pu(a), l = Su(a), f = Pu(r), d = Su(r), h = Pu(s), p = Su(s), b = Pu(o),
                v = Su(o), _ = Pu(c), g = Su(c), m = {
                    a: E,
                    A: j,
                    b: I,
                    B: L,
                    c: null,
                    d: Zu,
                    e: Zu,
                    f: nl,
                    H: Ju,
                    I: Qu,
                    j: tl,
                    L: el,
                    m: il,
                    M: al,
                    p: U,
                    Q: $l,
                    s: El,
                    S: rl,
                    u: sl,
                    U: ol,
                    V: cl,
                    w: ul,
                    W: ll,
                    x: null,
                    X: null,
                    y: fl,
                    Y: dl,
                    Z: hl,
                    "%": Sl
                }, y = {
                    a: R,
                    A: D,
                    b: V,
                    B: B,
                    c: null,
                    d: pl,
                    e: pl,
                    f: ml,
                    H: bl,
                    I: vl,
                    j: _l,
                    L: gl,
                    m: yl,
                    M: xl,
                    p: F,
                    Q: $l,
                    s: El,
                    S: wl,
                    u: Ol,
                    U: Ml,
                    V: Cl,
                    w: kl,
                    W: Al,
                    x: null,
                    X: null,
                    y: Tl,
                    Y: Nl,
                    Z: Pl,
                    "%": Sl
                }, x = {
                    a: k,
                    A: A,
                    b: T,
                    B: N,
                    c: P,
                    d: Bu,
                    e: Bu,
                    f: Yu,
                    H: Hu,
                    I: Hu,
                    j: Fu,
                    L: zu,
                    m: Vu,
                    M: qu,
                    p: C,
                    Q: Xu,
                    s: Ku,
                    S: Gu,
                    u: Eu,
                    U: ju,
                    V: Iu,
                    w: $u,
                    W: Lu,
                    x: S,
                    X: $,
                    y: Ru,
                    Y: Uu,
                    Z: Du,
                    "%": Wu
                };

            function w(t, e) {
                return function (n) {
                    var i, a, r, s = [], o = -1, c = 0, u = t.length;
                    n instanceof Date || (n = new Date(+n));
                    while (++o < u) 37 === t.charCodeAt(o) && (s.push(t.slice(c, o)), null != (a = Mu[i = t.charAt(++o)]) ? i = t.charAt(++o) : a = "e" === i ? " " : "0", (r = e[i]) && (i = r(n, a)), s.push(i), c = o + 1);
                    return s.push(t.slice(c, o)), s.join("")
                }
            }

            function O(t, e) {
                return function (n) {
                    var i, a, r = mu(1900), s = M(r, t, n += "", 0);
                    if (s != n.length) return null;
                    if ("Q" in r) return new Date(r.Q);
                    if ("p" in r && (r.H = r.H % 12 + 12 * r.p), "V" in r) {
                        if (r.V < 1 || r.V > 53) return null;
                        "w" in r || (r.w = 1), "Z" in r ? (i = gu(mu(r.y)), a = i.getUTCDay(), i = a > 4 || 0 === a ? cu.ceil(i) : cu(i), i = ru.offset(i, 7 * (r.V - 1)), r.y = i.getUTCFullYear(), r.m = i.getUTCMonth(), r.d = i.getUTCDate() + (r.w + 6) % 7) : (i = e(mu(r.y)), a = i.getDay(), i = a > 4 || 0 === a ? Yc.ceil(i) : Yc(i), i = qc.offset(i, 7 * (r.V - 1)), r.y = i.getFullYear(), r.m = i.getMonth(), r.d = i.getDate() + (r.w + 6) % 7)
                    } else ("W" in r || "U" in r) && ("w" in r || (r.w = "u" in r ? r.u % 7 : "W" in r ? 1 : 0), a = "Z" in r ? gu(mu(r.y)).getUTCDay() : e(mu(r.y)).getDay(), r.m = 0, r.d = "W" in r ? (r.w + 6) % 7 + 7 * r.W - (a + 5) % 7 : r.w + 7 * r.U - (a + 6) % 7);
                    return "Z" in r ? (r.H += r.Z / 100 | 0, r.M += r.Z % 100, gu(r)) : e(r)
                }
            }

            function M(t, e, n, i) {
                var a, r, s = 0, o = e.length, c = n.length;
                while (s < o) {
                    if (i >= c) return -1;
                    if (a = e.charCodeAt(s++), 37 === a) {
                        if (a = e.charAt(s++), r = x[a in Mu ? e.charAt(s++) : a], !r || (i = r(t, n, i)) < 0) return -1
                    } else if (a != n.charCodeAt(i++)) return -1
                }
                return i
            }

            function C(t, e, n) {
                var i = u.exec(e.slice(n));
                return i ? (t.p = l[i[0].toLowerCase()], n + i[0].length) : -1
            }

            function k(t, e, n) {
                var i = h.exec(e.slice(n));
                return i ? (t.w = p[i[0].toLowerCase()], n + i[0].length) : -1
            }

            function A(t, e, n) {
                var i = f.exec(e.slice(n));
                return i ? (t.w = d[i[0].toLowerCase()], n + i[0].length) : -1
            }

            function T(t, e, n) {
                var i = _.exec(e.slice(n));
                return i ? (t.m = g[i[0].toLowerCase()], n + i[0].length) : -1
            }

            function N(t, e, n) {
                var i = b.exec(e.slice(n));
                return i ? (t.m = v[i[0].toLowerCase()], n + i[0].length) : -1
            }

            function P(t, n, i) {
                return M(t, e, n, i)
            }

            function S(t, e, i) {
                return M(t, n, e, i)
            }

            function $(t, e, n) {
                return M(t, i, e, n)
            }

            function E(t) {
                return s[t.getDay()]
            }

            function j(t) {
                return r[t.getDay()]
            }

            function I(t) {
                return c[t.getMonth()]
            }

            function L(t) {
                return o[t.getMonth()]
            }

            function U(t) {
                return a[+(t.getHours() >= 12)]
            }

            function R(t) {
                return s[t.getUTCDay()]
            }

            function D(t) {
                return r[t.getUTCDay()]
            }

            function V(t) {
                return c[t.getUTCMonth()]
            }

            function B(t) {
                return o[t.getUTCMonth()]
            }

            function F(t) {
                return a[+(t.getUTCHours() >= 12)]
            }

            return m.x = w(n, m), m.X = w(i, m), m.c = w(e, m), y.x = w(n, y), y.X = w(i, y), y.c = w(e, y), {
                format: function (t) {
                    var e = w(t += "", m);
                    return e.toString = function () {
                        return t
                    }, e
                }, parse: function (t) {
                    var e = O(t += "", _u);
                    return e.toString = function () {
                        return t
                    }, e
                }, utcFormat: function (t) {
                    var e = w(t += "", y);
                    return e.toString = function () {
                        return t
                    }, e
                }, utcParse: function (t) {
                    var e = O(t, gu);
                    return e.toString = function () {
                        return t
                    }, e
                }
            }
        }

        var xu, wu, Ou, Mu = {"-": "", _: " ", 0: "0"}, Cu = /^\s*\d+/, ku = /^%/, Au = /[\\^$*+?|[\]().{}]/g;

        function Tu(t, e, n) {
            var i = t < 0 ? "-" : "", a = (i ? -t : t) + "", r = a.length;
            return i + (r < n ? new Array(n - r + 1).join(e) + a : a)
        }

        function Nu(t) {
            return t.replace(Au, "\\$&")
        }

        function Pu(t) {
            return new RegExp("^(?:" + t.map(Nu).join("|") + ")", "i")
        }

        function Su(t) {
            var e = {}, n = -1, i = t.length;
            while (++n < i) e[t[n].toLowerCase()] = n;
            return e
        }

        function $u(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 1));
            return i ? (t.w = +i[0], n + i[0].length) : -1
        }

        function Eu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 1));
            return i ? (t.u = +i[0], n + i[0].length) : -1
        }

        function ju(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.U = +i[0], n + i[0].length) : -1
        }

        function Iu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.V = +i[0], n + i[0].length) : -1
        }

        function Lu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.W = +i[0], n + i[0].length) : -1
        }

        function Uu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 4));
            return i ? (t.y = +i[0], n + i[0].length) : -1
        }

        function Ru(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.y = +i[0] + (+i[0] > 68 ? 1900 : 2e3), n + i[0].length) : -1
        }

        function Du(t, e, n) {
            var i = /^(Z)|([+-]\d\d)(?::?(\d\d))?/.exec(e.slice(n, n + 6));
            return i ? (t.Z = i[1] ? 0 : -(i[2] + (i[3] || "00")), n + i[0].length) : -1
        }

        function Vu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.m = i[0] - 1, n + i[0].length) : -1
        }

        function Bu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.d = +i[0], n + i[0].length) : -1
        }

        function Fu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 3));
            return i ? (t.m = 0, t.d = +i[0], n + i[0].length) : -1
        }

        function Hu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.H = +i[0], n + i[0].length) : -1
        }

        function qu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.M = +i[0], n + i[0].length) : -1
        }

        function Gu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 2));
            return i ? (t.S = +i[0], n + i[0].length) : -1
        }

        function zu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 3));
            return i ? (t.L = +i[0], n + i[0].length) : -1
        }

        function Yu(t, e, n) {
            var i = Cu.exec(e.slice(n, n + 6));
            return i ? (t.L = Math.floor(i[0] / 1e3), n + i[0].length) : -1
        }

        function Wu(t, e, n) {
            var i = ku.exec(e.slice(n, n + 1));
            return i ? n + i[0].length : -1
        }

        function Xu(t, e, n) {
            var i = Cu.exec(e.slice(n));
            return i ? (t.Q = +i[0], n + i[0].length) : -1
        }

        function Ku(t, e, n) {
            var i = Cu.exec(e.slice(n));
            return i ? (t.Q = 1e3 * +i[0], n + i[0].length) : -1
        }

        function Zu(t, e) {
            return Tu(t.getDate(), e, 2)
        }

        function Ju(t, e) {
            return Tu(t.getHours(), e, 2)
        }

        function Qu(t, e) {
            return Tu(t.getHours() % 12 || 12, e, 2)
        }

        function tl(t, e) {
            return Tu(1 + qc.count(eu(t), t), e, 3)
        }

        function el(t, e) {
            return Tu(t.getMilliseconds(), e, 3)
        }

        function nl(t, e) {
            return el(t, e) + "000"
        }

        function il(t, e) {
            return Tu(t.getMonth() + 1, e, 2)
        }

        function al(t, e) {
            return Tu(t.getMinutes(), e, 2)
        }

        function rl(t, e) {
            return Tu(t.getSeconds(), e, 2)
        }

        function sl(t) {
            var e = t.getDay();
            return 0 === e ? 7 : e
        }

        function ol(t, e) {
            return Tu(zc.count(eu(t), t), e, 2)
        }

        function cl(t, e) {
            var n = t.getDay();
            return t = n >= 4 || 0 === n ? Kc(t) : Kc.ceil(t), Tu(Kc.count(eu(t), t) + (4 === eu(t).getDay()), e, 2)
        }

        function ul(t) {
            return t.getDay()
        }

        function ll(t, e) {
            return Tu(Yc.count(eu(t), t), e, 2)
        }

        function fl(t, e) {
            return Tu(t.getFullYear() % 100, e, 2)
        }

        function dl(t, e) {
            return Tu(t.getFullYear() % 1e4, e, 4)
        }

        function hl(t) {
            var e = t.getTimezoneOffset();
            return (e > 0 ? "-" : (e *= -1, "+")) + Tu(e / 60 | 0, "0", 2) + Tu(e % 60, "0", 2)
        }

        function pl(t, e) {
            return Tu(t.getUTCDate(), e, 2)
        }

        function bl(t, e) {
            return Tu(t.getUTCHours(), e, 2)
        }

        function vl(t, e) {
            return Tu(t.getUTCHours() % 12 || 12, e, 2)
        }

        function _l(t, e) {
            return Tu(1 + ru.count(vu(t), t), e, 3)
        }

        function gl(t, e) {
            return Tu(t.getUTCMilliseconds(), e, 3)
        }

        function ml(t, e) {
            return gl(t, e) + "000"
        }

        function yl(t, e) {
            return Tu(t.getUTCMonth() + 1, e, 2)
        }

        function xl(t, e) {
            return Tu(t.getUTCMinutes(), e, 2)
        }

        function wl(t, e) {
            return Tu(t.getUTCSeconds(), e, 2)
        }

        function Ol(t) {
            var e = t.getUTCDay();
            return 0 === e ? 7 : e
        }

        function Ml(t, e) {
            return Tu(ou.count(vu(t), t), e, 2)
        }

        function Cl(t, e) {
            var n = t.getUTCDay();
            return t = n >= 4 || 0 === n ? fu(t) : fu.ceil(t), Tu(fu.count(vu(t), t) + (4 === vu(t).getUTCDay()), e, 2)
        }

        function kl(t) {
            return t.getUTCDay()
        }

        function Al(t, e) {
            return Tu(cu.count(vu(t), t), e, 2)
        }

        function Tl(t, e) {
            return Tu(t.getUTCFullYear() % 100, e, 2)
        }

        function Nl(t, e) {
            return Tu(t.getUTCFullYear() % 1e4, e, 4)
        }

        function Pl() {
            return "+0000"
        }

        function Sl() {
            return "%"
        }

        function $l(t) {
            return +t
        }

        function El(t) {
            return Math.floor(+t / 1e3)
        }

        function jl(t) {
            return xu = yu(t), xu.format, xu.parse, wu = xu.utcFormat, Ou = xu.utcParse, xu
        }

        jl({
            dateTime: "%x, %X",
            date: "%-m/%-d/%Y",
            time: "%-I:%M:%S %p",
            periods: ["AM", "PM"],
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            shortMonths: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        });
        var Il = "%Y-%m-%dT%H:%M:%S.%LZ";

        function Ll(t) {
            return t.toISOString()
        }

        Date.prototype.toISOString || wu(Il);

        function Ul(t) {
            var e = new Date(t);
            return isNaN(e) ? null : e
        }

        +new Date("2000-01-01T00:00:00.000Z") || Ou(Il);
        var Rl = function (t) {
                var e = t.length / 6 | 0, n = new Array(e), i = 0;
                while (i < e) n[i] = "#" + t.slice(6 * i, 6 * ++i);
                return n
            },
            Dl = (Rl("1f77b4ff7f0e2ca02cd627289467bd8c564be377c27f7f7fbcbd2217becf"), Rl("7fc97fbeaed4fdc086ffff99386cb0f0027fbf5b17666666"), Rl("1b9e77d95f027570b3e7298a66a61ee6ab02a6761d666666"), Rl("a6cee31f78b4b2df8a33a02cfb9a99e31a1cfdbf6fff7f00cab2d66a3d9affff99b15928"), Rl("fbb4aeb3cde3ccebc5decbe4fed9a6ffffcce5d8bdfddaecf2f2f2"), Rl("b3e2cdfdcdaccbd5e8f4cae4e6f5c9fff2aef1e2cccccccc"), Rl("e41a1c377eb84daf4a984ea3ff7f00ffff33a65628f781bf999999"), Rl("66c2a5fc8d628da0cbe78ac3a6d854ffd92fe5c494b3b3b3"), Rl("8dd3c7ffffb3bebadafb807280b1d3fdb462b3de69fccde5d9d9d9bc80bdccebc5ffed6f"), function (t) {
                return Un(t[t.length - 1])
            }),
            Vl = new Array(3).concat("d8b365f5f5f55ab4ac", "a6611adfc27d80cdc1018571", "a6611adfc27df5f5f580cdc1018571", "8c510ad8b365f6e8c3c7eae55ab4ac01665e", "8c510ad8b365f6e8c3f5f5f5c7eae55ab4ac01665e", "8c510abf812ddfc27df6e8c3c7eae580cdc135978f01665e", "8c510abf812ddfc27df6e8c3f5f5f5c7eae580cdc135978f01665e", "5430058c510abf812ddfc27df6e8c3c7eae580cdc135978f01665e003c30", "5430058c510abf812ddfc27df6e8c3f5f5f5c7eae580cdc135978f01665e003c30").map(Rl),
            Bl = (Dl(Vl), new Array(3).concat("af8dc3f7f7f77fbf7b", "7b3294c2a5cfa6dba0008837", "7b3294c2a5cff7f7f7a6dba0008837", "762a83af8dc3e7d4e8d9f0d37fbf7b1b7837", "762a83af8dc3e7d4e8f7f7f7d9f0d37fbf7b1b7837", "762a839970abc2a5cfe7d4e8d9f0d3a6dba05aae611b7837", "762a839970abc2a5cfe7d4e8f7f7f7d9f0d3a6dba05aae611b7837", "40004b762a839970abc2a5cfe7d4e8d9f0d3a6dba05aae611b783700441b", "40004b762a839970abc2a5cfe7d4e8f7f7f7d9f0d3a6dba05aae611b783700441b").map(Rl)),
            Fl = (Dl(Bl), new Array(3).concat("e9a3c9f7f7f7a1d76a", "d01c8bf1b6dab8e1864dac26", "d01c8bf1b6daf7f7f7b8e1864dac26", "c51b7de9a3c9fde0efe6f5d0a1d76a4d9221", "c51b7de9a3c9fde0eff7f7f7e6f5d0a1d76a4d9221", "c51b7dde77aef1b6dafde0efe6f5d0b8e1867fbc414d9221", "c51b7dde77aef1b6dafde0eff7f7f7e6f5d0b8e1867fbc414d9221", "8e0152c51b7dde77aef1b6dafde0efe6f5d0b8e1867fbc414d9221276419", "8e0152c51b7dde77aef1b6dafde0eff7f7f7e6f5d0b8e1867fbc414d9221276419").map(Rl)),
            Hl = (Dl(Fl), new Array(3).concat("998ec3f7f7f7f1a340", "5e3c99b2abd2fdb863e66101", "5e3c99b2abd2f7f7f7fdb863e66101", "542788998ec3d8daebfee0b6f1a340b35806", "542788998ec3d8daebf7f7f7fee0b6f1a340b35806", "5427888073acb2abd2d8daebfee0b6fdb863e08214b35806", "5427888073acb2abd2d8daebf7f7f7fee0b6fdb863e08214b35806", "2d004b5427888073acb2abd2d8daebfee0b6fdb863e08214b358067f3b08", "2d004b5427888073acb2abd2d8daebf7f7f7fee0b6fdb863e08214b358067f3b08").map(Rl)),
            ql = (Dl(Hl), new Array(3).concat("ef8a62f7f7f767a9cf", "ca0020f4a58292c5de0571b0", "ca0020f4a582f7f7f792c5de0571b0", "b2182bef8a62fddbc7d1e5f067a9cf2166ac", "b2182bef8a62fddbc7f7f7f7d1e5f067a9cf2166ac", "b2182bd6604df4a582fddbc7d1e5f092c5de4393c32166ac", "b2182bd6604df4a582fddbc7f7f7f7d1e5f092c5de4393c32166ac", "67001fb2182bd6604df4a582fddbc7d1e5f092c5de4393c32166ac053061", "67001fb2182bd6604df4a582fddbc7f7f7f7d1e5f092c5de4393c32166ac053061").map(Rl)),
            Gl = (Dl(ql), new Array(3).concat("ef8a62ffffff999999", "ca0020f4a582bababa404040", "ca0020f4a582ffffffbababa404040", "b2182bef8a62fddbc7e0e0e09999994d4d4d", "b2182bef8a62fddbc7ffffffe0e0e09999994d4d4d", "b2182bd6604df4a582fddbc7e0e0e0bababa8787874d4d4d", "b2182bd6604df4a582fddbc7ffffffe0e0e0bababa8787874d4d4d", "67001fb2182bd6604df4a582fddbc7e0e0e0bababa8787874d4d4d1a1a1a", "67001fb2182bd6604df4a582fddbc7ffffffe0e0e0bababa8787874d4d4d1a1a1a").map(Rl)),
            zl = (Dl(Gl), new Array(3).concat("fc8d59ffffbf91bfdb", "d7191cfdae61abd9e92c7bb6", "d7191cfdae61ffffbfabd9e92c7bb6", "d73027fc8d59fee090e0f3f891bfdb4575b4", "d73027fc8d59fee090ffffbfe0f3f891bfdb4575b4", "d73027f46d43fdae61fee090e0f3f8abd9e974add14575b4", "d73027f46d43fdae61fee090ffffbfe0f3f8abd9e974add14575b4", "a50026d73027f46d43fdae61fee090e0f3f8abd9e974add14575b4313695", "a50026d73027f46d43fdae61fee090ffffbfe0f3f8abd9e974add14575b4313695").map(Rl)),
            Yl = (Dl(zl), new Array(3).concat("fc8d59ffffbf91cf60", "d7191cfdae61a6d96a1a9641", "d7191cfdae61ffffbfa6d96a1a9641", "d73027fc8d59fee08bd9ef8b91cf601a9850", "d73027fc8d59fee08bffffbfd9ef8b91cf601a9850", "d73027f46d43fdae61fee08bd9ef8ba6d96a66bd631a9850", "d73027f46d43fdae61fee08bffffbfd9ef8ba6d96a66bd631a9850", "a50026d73027f46d43fdae61fee08bd9ef8ba6d96a66bd631a9850006837", "a50026d73027f46d43fdae61fee08bffffbfd9ef8ba6d96a66bd631a9850006837").map(Rl)),
            Wl = (Dl(Yl), new Array(3).concat("fc8d59ffffbf99d594", "d7191cfdae61abdda42b83ba", "d7191cfdae61ffffbfabdda42b83ba", "d53e4ffc8d59fee08be6f59899d5943288bd", "d53e4ffc8d59fee08bffffbfe6f59899d5943288bd", "d53e4ff46d43fdae61fee08be6f598abdda466c2a53288bd", "d53e4ff46d43fdae61fee08bffffbfe6f598abdda466c2a53288bd", "9e0142d53e4ff46d43fdae61fee08be6f598abdda466c2a53288bd5e4fa2", "9e0142d53e4ff46d43fdae61fee08bffffbfe6f598abdda466c2a53288bd5e4fa2").map(Rl)),
            Xl = (Dl(Wl), new Array(3).concat("e5f5f999d8c92ca25f", "edf8fbb2e2e266c2a4238b45", "edf8fbb2e2e266c2a42ca25f006d2c", "edf8fbccece699d8c966c2a42ca25f006d2c", "edf8fbccece699d8c966c2a441ae76238b45005824", "f7fcfde5f5f9ccece699d8c966c2a441ae76238b45005824", "f7fcfde5f5f9ccece699d8c966c2a441ae76238b45006d2c00441b").map(Rl)),
            Kl = (Dl(Xl), new Array(3).concat("e0ecf49ebcda8856a7", "edf8fbb3cde38c96c688419d", "edf8fbb3cde38c96c68856a7810f7c", "edf8fbbfd3e69ebcda8c96c68856a7810f7c", "edf8fbbfd3e69ebcda8c96c68c6bb188419d6e016b", "f7fcfde0ecf4bfd3e69ebcda8c96c68c6bb188419d6e016b", "f7fcfde0ecf4bfd3e69ebcda8c96c68c6bb188419d810f7c4d004b").map(Rl)),
            Zl = (Dl(Kl), new Array(3).concat("e0f3dba8ddb543a2ca", "f0f9e8bae4bc7bccc42b8cbe", "f0f9e8bae4bc7bccc443a2ca0868ac", "f0f9e8ccebc5a8ddb57bccc443a2ca0868ac", "f0f9e8ccebc5a8ddb57bccc44eb3d32b8cbe08589e", "f7fcf0e0f3dbccebc5a8ddb57bccc44eb3d32b8cbe08589e", "f7fcf0e0f3dbccebc5a8ddb57bccc44eb3d32b8cbe0868ac084081").map(Rl)),
            Jl = (Dl(Zl), new Array(3).concat("fee8c8fdbb84e34a33", "fef0d9fdcc8afc8d59d7301f", "fef0d9fdcc8afc8d59e34a33b30000", "fef0d9fdd49efdbb84fc8d59e34a33b30000", "fef0d9fdd49efdbb84fc8d59ef6548d7301f990000", "fff7ecfee8c8fdd49efdbb84fc8d59ef6548d7301f990000", "fff7ecfee8c8fdd49efdbb84fc8d59ef6548d7301fb300007f0000").map(Rl)),
            Ql = (Dl(Jl), new Array(3).concat("ece2f0a6bddb1c9099", "f6eff7bdc9e167a9cf02818a", "f6eff7bdc9e167a9cf1c9099016c59", "f6eff7d0d1e6a6bddb67a9cf1c9099016c59", "f6eff7d0d1e6a6bddb67a9cf3690c002818a016450", "fff7fbece2f0d0d1e6a6bddb67a9cf3690c002818a016450", "fff7fbece2f0d0d1e6a6bddb67a9cf3690c002818a016c59014636").map(Rl)),
            tf = (Dl(Ql), new Array(3).concat("ece7f2a6bddb2b8cbe", "f1eef6bdc9e174a9cf0570b0", "f1eef6bdc9e174a9cf2b8cbe045a8d", "f1eef6d0d1e6a6bddb74a9cf2b8cbe045a8d", "f1eef6d0d1e6a6bddb74a9cf3690c00570b0034e7b", "fff7fbece7f2d0d1e6a6bddb74a9cf3690c00570b0034e7b", "fff7fbece7f2d0d1e6a6bddb74a9cf3690c00570b0045a8d023858").map(Rl)),
            ef = (Dl(tf), new Array(3).concat("e7e1efc994c7dd1c77", "f1eef6d7b5d8df65b0ce1256", "f1eef6d7b5d8df65b0dd1c77980043", "f1eef6d4b9dac994c7df65b0dd1c77980043", "f1eef6d4b9dac994c7df65b0e7298ace125691003f", "f7f4f9e7e1efd4b9dac994c7df65b0e7298ace125691003f", "f7f4f9e7e1efd4b9dac994c7df65b0e7298ace125698004367001f").map(Rl)),
            nf = (Dl(ef), new Array(3).concat("fde0ddfa9fb5c51b8a", "feebe2fbb4b9f768a1ae017e", "feebe2fbb4b9f768a1c51b8a7a0177", "feebe2fcc5c0fa9fb5f768a1c51b8a7a0177", "feebe2fcc5c0fa9fb5f768a1dd3497ae017e7a0177", "fff7f3fde0ddfcc5c0fa9fb5f768a1dd3497ae017e7a0177", "fff7f3fde0ddfcc5c0fa9fb5f768a1dd3497ae017e7a017749006a").map(Rl)),
            af = (Dl(nf), new Array(3).concat("edf8b17fcdbb2c7fb8", "ffffcca1dab441b6c4225ea8", "ffffcca1dab441b6c42c7fb8253494", "ffffccc7e9b47fcdbb41b6c42c7fb8253494", "ffffccc7e9b47fcdbb41b6c41d91c0225ea80c2c84", "ffffd9edf8b1c7e9b47fcdbb41b6c41d91c0225ea80c2c84", "ffffd9edf8b1c7e9b47fcdbb41b6c41d91c0225ea8253494081d58").map(Rl)),
            rf = (Dl(af), new Array(3).concat("f7fcb9addd8e31a354", "ffffccc2e69978c679238443", "ffffccc2e69978c67931a354006837", "ffffccd9f0a3addd8e78c67931a354006837", "ffffccd9f0a3addd8e78c67941ab5d238443005a32", "ffffe5f7fcb9d9f0a3addd8e78c67941ab5d238443005a32", "ffffe5f7fcb9d9f0a3addd8e78c67941ab5d238443006837004529").map(Rl)),
            sf = (Dl(rf), new Array(3).concat("fff7bcfec44fd95f0e", "ffffd4fed98efe9929cc4c02", "ffffd4fed98efe9929d95f0e993404", "ffffd4fee391fec44ffe9929d95f0e993404", "ffffd4fee391fec44ffe9929ec7014cc4c028c2d04", "ffffe5fff7bcfee391fec44ffe9929ec7014cc4c028c2d04", "ffffe5fff7bcfee391fec44ffe9929ec7014cc4c02993404662506").map(Rl)),
            of = (Dl(sf), new Array(3).concat("ffeda0feb24cf03b20", "ffffb2fecc5cfd8d3ce31a1c", "ffffb2fecc5cfd8d3cf03b20bd0026", "ffffb2fed976feb24cfd8d3cf03b20bd0026", "ffffb2fed976feb24cfd8d3cfc4e2ae31a1cb10026", "ffffccffeda0fed976feb24cfd8d3cfc4e2ae31a1cb10026", "ffffccffeda0fed976feb24cfd8d3cfc4e2ae31a1cbd0026800026").map(Rl)),
            cf = (Dl(of), new Array(3).concat("deebf79ecae13182bd", "eff3ffbdd7e76baed62171b5", "eff3ffbdd7e76baed63182bd08519c", "eff3ffc6dbef9ecae16baed63182bd08519c", "eff3ffc6dbef9ecae16baed64292c62171b5084594", "f7fbffdeebf7c6dbef9ecae16baed64292c62171b5084594", "f7fbffdeebf7c6dbef9ecae16baed64292c62171b508519c08306b").map(Rl)),
            uf = (Dl(cf), new Array(3).concat("e5f5e0a1d99b31a354", "edf8e9bae4b374c476238b45", "edf8e9bae4b374c47631a354006d2c", "edf8e9c7e9c0a1d99b74c47631a354006d2c", "edf8e9c7e9c0a1d99b74c47641ab5d238b45005a32", "f7fcf5e5f5e0c7e9c0a1d99b74c47641ab5d238b45005a32", "f7fcf5e5f5e0c7e9c0a1d99b74c47641ab5d238b45006d2c00441b").map(Rl)),
            lf = (Dl(uf), new Array(3).concat("f0f0f0bdbdbd636363", "f7f7f7cccccc969696525252", "f7f7f7cccccc969696636363252525", "f7f7f7d9d9d9bdbdbd969696636363252525", "f7f7f7d9d9d9bdbdbd969696737373525252252525", "fffffff0f0f0d9d9d9bdbdbd969696737373525252252525", "fffffff0f0f0d9d9d9bdbdbd969696737373525252252525000000").map(Rl)),
            ff = (Dl(lf), new Array(3).concat("efedf5bcbddc756bb1", "f2f0f7cbc9e29e9ac86a51a3", "f2f0f7cbc9e29e9ac8756bb154278f", "f2f0f7dadaebbcbddc9e9ac8756bb154278f", "f2f0f7dadaebbcbddc9e9ac8807dba6a51a34a1486", "fcfbfdefedf5dadaebbcbddc9e9ac8807dba6a51a34a1486", "fcfbfdefedf5dadaebbcbddc9e9ac8807dba6a51a354278f3f007d").map(Rl)),
            df = (Dl(ff), new Array(3).concat("fee0d2fc9272de2d26", "fee5d9fcae91fb6a4acb181d", "fee5d9fcae91fb6a4ade2d26a50f15", "fee5d9fcbba1fc9272fb6a4ade2d26a50f15", "fee5d9fcbba1fc9272fb6a4aef3b2ccb181d99000d", "fff5f0fee0d2fcbba1fc9272fb6a4aef3b2ccb181d99000d", "fff5f0fee0d2fcbba1fc9272fb6a4aef3b2ccb181da50f1567000d").map(Rl)),
            hf = (Dl(df), new Array(3).concat("fee6cefdae6be6550d", "feeddefdbe85fd8d3cd94701", "feeddefdbe85fd8d3ce6550da63603", "feeddefdd0a2fdae6bfd8d3ce6550da63603", "feeddefdd0a2fdae6bfd8d3cf16913d948018c2d04", "fff5ebfee6cefdd0a2fdae6bfd8d3cf16913d948018c2d04", "fff5ebfee6cefdd0a2fdae6bfd8d3cf16913d94801a636037f2704").map(Rl));
        Dl(hf), li(Mn(300, .5, 0), Mn(-240, .5, 1)), li(Mn(-100, .75, .35), Mn(80, 1.5, .8)), li(Mn(260, .75, .35), Mn(80, 1.5, .8)), Mn(), Ve(), Math.PI, Math.PI;

        function pf(t) {
            var e = t.length;
            return function (n) {
                return t[Math.max(0, Math.min(e - 1, Math.floor(n * e)))]
            }
        }

        pf(Rl("44015444025645045745055946075a46085c460a5d460b5e470d60470e6147106347116447136548146748166848176948186a481a6c481b6d481c6e481d6f481f70482071482173482374482475482576482677482878482979472a7a472c7a472d7b472e7c472f7d46307e46327e46337f463480453581453781453882443983443a83443b84433d84433e85423f854240864241864142874144874045884046883f47883f48893e49893e4a893e4c8a3d4d8a3d4e8a3c4f8a3c508b3b518b3b528b3a538b3a548c39558c39568c38588c38598c375a8c375b8d365c8d365d8d355e8d355f8d34608d34618d33628d33638d32648e32658e31668e31678e31688e30698e306a8e2f6b8e2f6c8e2e6d8e2e6e8e2e6f8e2d708e2d718e2c718e2c728e2c738e2b748e2b758e2a768e2a778e2a788e29798e297a8e297b8e287c8e287d8e277e8e277f8e27808e26818e26828e26828e25838e25848e25858e24868e24878e23888e23898e238a8d228b8d228c8d228d8d218e8d218f8d21908d21918c20928c20928c20938c1f948c1f958b1f968b1f978b1f988b1f998a1f9a8a1e9b8a1e9c891e9d891f9e891f9f881fa0881fa1881fa1871fa28720a38620a48621a58521a68522a78522a88423a98324aa8325ab8225ac8226ad8127ad8128ae8029af7f2ab07f2cb17e2db27d2eb37c2fb47c31b57b32b67a34b67935b77937b87838b9773aba763bbb753dbc743fbc7340bd7242be7144bf7046c06f48c16e4ac16d4cc26c4ec36b50c46a52c56954c56856c66758c7655ac8645cc8635ec96260ca6063cb5f65cb5e67cc5c69cd5b6ccd5a6ece5870cf5773d05675d05477d1537ad1517cd2507fd34e81d34d84d44b86d54989d5488bd6468ed64590d74393d74195d84098d83e9bd93c9dd93ba0da39a2da37a5db36a8db34aadc32addc30b0dd2fb2dd2db5de2bb8de29bade28bddf26c0df25c2df23c5e021c8e020cae11fcde11dd0e11cd2e21bd5e21ad8e219dae319dde318dfe318e2e418e5e419e7e419eae51aece51befe51cf1e51df4e61ef6e620f8e621fbe723fde725")), pf(Rl("00000401000501010601010802010902020b02020d03030f03031204041405041606051806051a07061c08071e0907200a08220b09240c09260d0a290e0b2b100b2d110c2f120d31130d34140e36150e38160f3b180f3d19103f1a10421c10441d11471e114920114b21114e22115024125325125527125829115a2a115c2c115f2d11612f116331116533106734106936106b38106c390f6e3b0f703d0f713f0f72400f74420f75440f764510774710784910784a10794c117a4e117b4f127b51127c52137c54137d56147d57157e59157e5a167e5c167f5d177f5f187f601880621980641a80651a80671b80681c816a1c816b1d816d1d816e1e81701f81721f817320817521817621817822817922827b23827c23827e24828025828125818326818426818627818827818928818b29818c29818e2a81902a81912b81932b80942c80962c80982d80992d809b2e7f9c2e7f9e2f7fa02f7fa1307ea3307ea5317ea6317da8327daa337dab337cad347cae347bb0357bb2357bb3367ab5367ab73779b83779ba3878bc3978bd3977bf3a77c03a76c23b75c43c75c53c74c73d73c83e73ca3e72cc3f71cd4071cf4070d0416fd2426fd3436ed5446dd6456cd8456cd9466bdb476adc4869de4968df4a68e04c67e24d66e34e65e44f64e55064e75263e85362e95462ea5661eb5760ec5860ed5a5fee5b5eef5d5ef05f5ef1605df2625df2645cf3655cf4675cf4695cf56b5cf66c5cf66e5cf7705cf7725cf8745cf8765cf9785df9795df97b5dfa7d5efa7f5efa815ffb835ffb8560fb8761fc8961fc8a62fc8c63fc8e64fc9065fd9266fd9467fd9668fd9869fd9a6afd9b6bfe9d6cfe9f6dfea16efea36ffea571fea772fea973feaa74feac76feae77feb078feb27afeb47bfeb67cfeb77efeb97ffebb81febd82febf84fec185fec287fec488fec68afec88cfeca8dfecc8ffecd90fecf92fed194fed395fed597fed799fed89afdda9cfddc9efddea0fde0a1fde2a3fde3a5fde5a7fde7a9fde9aafdebacfcecaefceeb0fcf0b2fcf2b4fcf4b6fcf6b8fcf7b9fcf9bbfcfbbdfcfdbf")), pf(Rl("00000401000501010601010802010a02020c02020e03021004031204031405041706041907051b08051d09061f0a07220b07240c08260d08290e092b10092d110a30120a32140b34150b37160b39180c3c190c3e1b0c411c0c431e0c451f0c48210c4a230c4c240c4f260c51280b53290b552b0b572d0b592f0a5b310a5c320a5e340a5f3609613809623909633b09643d09653e0966400a67420a68440a68450a69470b6a490b6a4a0c6b4c0c6b4d0d6c4f0d6c510e6c520e6d540f6d550f6d57106e59106e5a116e5c126e5d126e5f136e61136e62146e64156e65156e67166e69166e6a176e6c186e6d186e6f196e71196e721a6e741a6e751b6e771c6d781c6d7a1d6d7c1d6d7d1e6d7f1e6c801f6c82206c84206b85216b87216b88226a8a226a8c23698d23698f24699025689225689326679526679727669827669a28659b29649d29649f2a63a02a63a22b62a32c61a52c60a62d60a82e5fa92e5eab2f5ead305dae305cb0315bb1325ab3325ab43359b63458b73557b93556ba3655bc3754bd3853bf3952c03a51c13a50c33b4fc43c4ec63d4dc73e4cc83f4bca404acb4149cc4248ce4347cf4446d04545d24644d34743d44842d54a41d74b3fd84c3ed94d3dda4e3cdb503bdd513ade5238df5337e05536e15635e25734e35933e45a31e55c30e65d2fe75e2ee8602de9612bea632aeb6429eb6628ec6726ed6925ee6a24ef6c23ef6e21f06f20f1711ff1731df2741cf3761bf37819f47918f57b17f57d15f67e14f68013f78212f78410f8850ff8870ef8890cf98b0bf98c0af98e09fa9008fa9207fa9407fb9606fb9706fb9906fb9b06fb9d07fc9f07fca108fca309fca50afca60cfca80dfcaa0ffcac11fcae12fcb014fcb216fcb418fbb61afbb81dfbba1ffbbc21fbbe23fac026fac228fac42afac62df9c72ff9c932f9cb35f8cd37f8cf3af7d13df7d340f6d543f6d746f5d949f5db4cf4dd4ff4df53f4e156f3e35af3e55df2e661f2e865f2ea69f1ec6df1ed71f1ef75f1f179f2f27df2f482f3f586f3f68af4f88ef5f992f6fa96f8fb9af9fc9dfafda1fcffa4")), pf(Rl("0d088710078813078916078a19068c1b068d1d068e20068f2206902406912605912805922a05932c05942e05952f059631059733059735049837049938049a3a049a3c049b3e049c3f049c41049d43039e44039e46039f48039f4903a04b03a14c02a14e02a25002a25102a35302a35502a45601a45801a45901a55b01a55c01a65e01a66001a66100a76300a76400a76600a76700a86900a86a00a86c00a86e00a86f00a87100a87201a87401a87501a87701a87801a87a02a87b02a87d03a87e03a88004a88104a78305a78405a78606a68707a68808a68a09a58b0aa58d0ba58e0ca48f0da4910ea3920fa39410a29511a19613a19814a099159f9a169f9c179e9d189d9e199da01a9ca11b9ba21d9aa31e9aa51f99a62098a72197a82296aa2395ab2494ac2694ad2793ae2892b02991b12a90b22b8fb32c8eb42e8db52f8cb6308bb7318ab83289ba3388bb3488bc3587bd3786be3885bf3984c03a83c13b82c23c81c33d80c43e7fc5407ec6417dc7427cc8437bc9447aca457acb4679cc4778cc4977cd4a76ce4b75cf4c74d04d73d14e72d24f71d35171d45270d5536fd5546ed6556dd7566cd8576bd9586ada5a6ada5b69db5c68dc5d67dd5e66de5f65de6164df6263e06363e16462e26561e26660e3685fe4695ee56a5de56b5de66c5ce76e5be76f5ae87059e97158e97257ea7457eb7556eb7655ec7754ed7953ed7a52ee7b51ef7c51ef7e50f07f4ff0804ef1814df1834cf2844bf3854bf3874af48849f48948f58b47f58c46f68d45f68f44f79044f79143f79342f89441f89540f9973ff9983ef99a3efa9b3dfa9c3cfa9e3bfb9f3afba139fba238fca338fca537fca636fca835fca934fdab33fdac33fdae32fdaf31fdb130fdb22ffdb42ffdb52efeb72dfeb82cfeba2cfebb2bfebd2afebe2afec029fdc229fdc328fdc527fdc627fdc827fdca26fdcb26fccd25fcce25fcd025fcd225fbd324fbd524fbd724fad824fada24f9dc24f9dd25f8df25f8e125f7e225f7e425f6e626f6e826f5e926f5eb27f4ed27f3ee27f3f027f2f227f1f426f1f525f0f724f0f921"));
        var bf = function (t) {
                return function () {
                    return t
                }
            }, vf = Math.abs, _f = Math.atan2, gf = Math.cos, mf = Math.max, yf = Math.min, xf = Math.sin, wf = Math.sqrt,
            Of = 1e-12, Mf = Math.PI, Cf = Mf / 2, kf = 2 * Mf;

        function Af(t) {
            return t > 1 ? 0 : t < -1 ? Mf : Math.acos(t)
        }

        function Tf(t) {
            return t >= 1 ? Cf : t <= -1 ? -Cf : Math.asin(t)
        }

        function Nf(t) {
            return t.innerRadius
        }

        function Pf(t) {
            return t.outerRadius
        }

        function Sf(t) {
            return t.startAngle
        }

        function $f(t) {
            return t.endAngle
        }

        function Ef(t) {
            return t && t.padAngle
        }

        function jf(t, e, n, i, a, r, s, o) {
            var c = n - t, u = i - e, l = s - a, f = o - r, d = f * c - l * u;
            if (!(d * d < Of)) return d = (l * (e - r) - f * (t - a)) / d, [t + d * c, e + d * u]
        }

        function If(t, e, n, i, a, r, s) {
            var o = t - n, c = e - i, u = (s ? r : -r) / wf(o * o + c * c), l = u * c, f = -u * o, d = t + l, h = e + f,
                p = n + l, b = i + f, v = (d + p) / 2, _ = (h + b) / 2, g = p - d, m = b - h, y = g * g + m * m,
                x = a - r, w = d * b - p * h, O = (m < 0 ? -1 : 1) * wf(mf(0, x * x * y - w * w)),
                M = (w * m - g * O) / y, C = (-w * g - m * O) / y, k = (w * m + g * O) / y, A = (-w * g + m * O) / y,
                T = M - v, N = C - _, P = k - v, S = A - _;
            return T * T + N * N > P * P + S * S && (M = k, C = A), {
                cx: M,
                cy: C,
                x01: -l,
                y01: -f,
                x11: M * (a / x - 1),
                y11: C * (a / x - 1)
            }
        }

        var Lf = function () {
            var t = Nf, e = Pf, n = bf(0), i = null, a = Sf, r = $f, s = Ef, o = null;

            function c() {
                var c, u, l = +t.apply(this, arguments), f = +e.apply(this, arguments),
                    d = a.apply(this, arguments) - Cf, h = r.apply(this, arguments) - Cf, p = vf(h - d), b = h > d;
                if (o || (o = c = fr()), f < l && (u = f, f = l, l = u), f > Of) if (p > kf - Of) o.moveTo(f * gf(d), f * xf(d)), o.arc(0, 0, f, d, h, !b), l > Of && (o.moveTo(l * gf(h), l * xf(h)), o.arc(0, 0, l, h, d, b)); else {
                    var v, _, g = d, m = h, y = d, x = h, w = p, O = p, M = s.apply(this, arguments) / 2,
                        C = M > Of && (i ? +i.apply(this, arguments) : wf(l * l + f * f)),
                        k = yf(vf(f - l) / 2, +n.apply(this, arguments)), A = k, T = k;
                    if (C > Of) {
                        var N = Tf(C / l * xf(M)), P = Tf(C / f * xf(M));
                        (w -= 2 * N) > Of ? (N *= b ? 1 : -1, y += N, x -= N) : (w = 0, y = x = (d + h) / 2), (O -= 2 * P) > Of ? (P *= b ? 1 : -1, g += P, m -= P) : (O = 0, g = m = (d + h) / 2)
                    }
                    var S = f * gf(g), $ = f * xf(g), E = l * gf(x), j = l * xf(x);
                    if (k > Of) {
                        var I, L = f * gf(m), U = f * xf(m), R = l * gf(y), D = l * xf(y);
                        if (p < Mf && (I = jf(S, $, R, D, L, U, E, j))) {
                            var V = S - I[0], B = $ - I[1], F = L - I[0], H = U - I[1],
                                q = 1 / xf(Af((V * F + B * H) / (wf(V * V + B * B) * wf(F * F + H * H))) / 2),
                                G = wf(I[0] * I[0] + I[1] * I[1]);
                            A = yf(k, (l - G) / (q - 1)), T = yf(k, (f - G) / (q + 1))
                        }
                    }
                    O > Of ? T > Of ? (v = If(R, D, S, $, f, T, b), _ = If(L, U, E, j, f, T, b), o.moveTo(v.cx + v.x01, v.cy + v.y01), T < k ? o.arc(v.cx, v.cy, T, _f(v.y01, v.x01), _f(_.y01, _.x01), !b) : (o.arc(v.cx, v.cy, T, _f(v.y01, v.x01), _f(v.y11, v.x11), !b), o.arc(0, 0, f, _f(v.cy + v.y11, v.cx + v.x11), _f(_.cy + _.y11, _.cx + _.x11), !b), o.arc(_.cx, _.cy, T, _f(_.y11, _.x11), _f(_.y01, _.x01), !b))) : (o.moveTo(S, $), o.arc(0, 0, f, g, m, !b)) : o.moveTo(S, $), l > Of && w > Of ? A > Of ? (v = If(E, j, L, U, l, -A, b), _ = If(S, $, R, D, l, -A, b), o.lineTo(v.cx + v.x01, v.cy + v.y01), A < k ? o.arc(v.cx, v.cy, A, _f(v.y01, v.x01), _f(_.y01, _.x01), !b) : (o.arc(v.cx, v.cy, A, _f(v.y01, v.x01), _f(v.y11, v.x11), !b), o.arc(0, 0, l, _f(v.cy + v.y11, v.cx + v.x11), _f(_.cy + _.y11, _.cx + _.x11), b), o.arc(_.cx, _.cy, A, _f(_.y11, _.x11), _f(_.y01, _.x01), !b))) : o.arc(0, 0, l, x, y, b) : o.lineTo(E, j)
                } else o.moveTo(0, 0);
                if (o.closePath(), c) return o = null, c + "" || null
            }

            return c.centroid = function () {
                var n = (+t.apply(this, arguments) + +e.apply(this, arguments)) / 2,
                    i = (+a.apply(this, arguments) + +r.apply(this, arguments)) / 2 - Mf / 2;
                return [gf(i) * n, xf(i) * n]
            }, c.innerRadius = function (e) {
                return arguments.length ? (t = "function" === typeof e ? e : bf(+e), c) : t
            }, c.outerRadius = function (t) {
                return arguments.length ? (e = "function" === typeof t ? t : bf(+t), c) : e
            }, c.cornerRadius = function (t) {
                return arguments.length ? (n = "function" === typeof t ? t : bf(+t), c) : n
            }, c.padRadius = function (t) {
                return arguments.length ? (i = null == t ? null : "function" === typeof t ? t : bf(+t), c) : i
            }, c.startAngle = function (t) {
                return arguments.length ? (a = "function" === typeof t ? t : bf(+t), c) : a
            }, c.endAngle = function (t) {
                return arguments.length ? (r = "function" === typeof t ? t : bf(+t), c) : r
            }, c.padAngle = function (t) {
                return arguments.length ? (s = "function" === typeof t ? t : bf(+t), c) : s
            }, c.context = function (t) {
                return arguments.length ? (o = null == t ? null : t, c) : o
            }, c
        };

        function Uf(t) {
            this._context = t
        }

        Uf.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._point = 0
            }, lineEnd: function () {
                (this._line || 0 !== this._line && 1 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1, this._line ? this._context.lineTo(t, e) : this._context.moveTo(t, e);
                        break;
                    case 1:
                        this._point = 2;
                    default:
                        this._context.lineTo(t, e);
                        break
                }
            }
        };
        var Rf = function (t) {
            return new Uf(t)
        };

        function Df(t) {
            return t[0]
        }

        function Vf(t) {
            return t[1]
        }

        var Bf = function () {
            var t = Df, e = Vf, n = bf(!0), i = null, a = Rf, r = null;

            function s(s) {
                var o, c, u, l = s.length, f = !1;
                for (null == i && (r = a(u = fr())), o = 0; o <= l; ++o) !(o < l && n(c = s[o], o, s)) === f && ((f = !f) ? r.lineStart() : r.lineEnd()), f && r.point(+t(c, o, s), +e(c, o, s));
                if (u) return r = null, u + "" || null
            }

            return s.x = function (e) {
                return arguments.length ? (t = "function" === typeof e ? e : bf(+e), s) : t
            }, s.y = function (t) {
                return arguments.length ? (e = "function" === typeof t ? t : bf(+t), s) : e
            }, s.defined = function (t) {
                return arguments.length ? (n = "function" === typeof t ? t : bf(!!t), s) : n
            }, s.curve = function (t) {
                return arguments.length ? (a = t, null != i && (r = a(i)), s) : a
            }, s.context = function (t) {
                return arguments.length ? (null == t ? i = r = null : r = a(i = t), s) : i
            }, s
        }, Ff = function (t, e) {
            return e < t ? -1 : e > t ? 1 : e >= t ? 0 : NaN
        }, Hf = function (t) {
            return t
        }, qf = function () {
            var t = Hf, e = Ff, n = null, i = bf(0), a = bf(kf), r = bf(0);

            function s(s) {
                var o, c, u, l, f, d = s.length, h = 0, p = new Array(d), b = new Array(d),
                    v = +i.apply(this, arguments), _ = Math.min(kf, Math.max(-kf, a.apply(this, arguments) - v)),
                    g = Math.min(Math.abs(_) / d, r.apply(this, arguments)), m = g * (_ < 0 ? -1 : 1);
                for (o = 0; o < d; ++o) (f = b[p[o] = o] = +t(s[o], o, s)) > 0 && (h += f);
                for (null != e ? p.sort(function (t, n) {
                    return e(b[t], b[n])
                }) : null != n && p.sort(function (t, e) {
                    return n(s[t], s[e])
                }), o = 0, u = h ? (_ - d * m) / h : 0; o < d; ++o, v = l) c = p[o], f = b[c], l = v + (f > 0 ? f * u : 0) + m, b[c] = {
                    data: s[c],
                    index: o,
                    value: f,
                    startAngle: v,
                    endAngle: l,
                    padAngle: g
                };
                return b
            }

            return s.value = function (e) {
                return arguments.length ? (t = "function" === typeof e ? e : bf(+e), s) : t
            }, s.sortValues = function (t) {
                return arguments.length ? (e = t, n = null, s) : e
            }, s.sort = function (t) {
                return arguments.length ? (n = t, e = null, s) : n
            }, s.startAngle = function (t) {
                return arguments.length ? (i = "function" === typeof t ? t : bf(+t), s) : i
            }, s.endAngle = function (t) {
                return arguments.length ? (a = "function" === typeof t ? t : bf(+t), s) : a
            }, s.padAngle = function (t) {
                return arguments.length ? (r = "function" === typeof t ? t : bf(+t), s) : r
            }, s
        };
        zf(Rf);

        function Gf(t) {
            this._curve = t
        }

        function zf(t) {
            function e(e) {
                return new Gf(t(e))
            }

            return e._curve = t, e
        }

        Gf.prototype = {
            areaStart: function () {
                this._curve.areaStart()
            }, areaEnd: function () {
                this._curve.areaEnd()
            }, lineStart: function () {
                this._curve.lineStart()
            }, lineEnd: function () {
                this._curve.lineEnd()
            }, point: function (t, e) {
                this._curve.point(e * Math.sin(t), e * -Math.cos(t))
            }
        };
        Array.prototype.slice;
        Math.sqrt(1 / 3);
        var Yf = Math.sin(Mf / 10) / Math.sin(7 * Mf / 10),
            Wf = (Math.sin(kf / 10), Math.cos(kf / 10), Math.sqrt(3), Math.sqrt(3), Math.sqrt(12), function () {
            });

        function Xf(t, e, n) {
            t._context.bezierCurveTo((2 * t._x0 + t._x1) / 3, (2 * t._y0 + t._y1) / 3, (t._x0 + 2 * t._x1) / 3, (t._y0 + 2 * t._y1) / 3, (t._x0 + 4 * t._x1 + e) / 6, (t._y0 + 4 * t._y1 + n) / 6)
        }

        function Kf(t) {
            this._context = t
        }

        Kf.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x0 = this._x1 = this._y0 = this._y1 = NaN, this._point = 0
            }, lineEnd: function () {
                switch (this._point) {
                    case 3:
                        Xf(this, this._x1, this._y1);
                    case 2:
                        this._context.lineTo(this._x1, this._y1);
                        break
                }
                (this._line || 0 !== this._line && 1 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1, this._line ? this._context.lineTo(t, e) : this._context.moveTo(t, e);
                        break;
                    case 1:
                        this._point = 2;
                        break;
                    case 2:
                        this._point = 3, this._context.lineTo((5 * this._x0 + this._x1) / 6, (5 * this._y0 + this._y1) / 6);
                    default:
                        Xf(this, t, e);
                        break
                }
                this._x0 = this._x1, this._x1 = t, this._y0 = this._y1, this._y1 = e
            }
        };

        function Zf(t) {
            this._context = t
        }

        Zf.prototype = {
            areaStart: Wf, areaEnd: Wf, lineStart: function () {
                this._x0 = this._x1 = this._x2 = this._x3 = this._x4 = this._y0 = this._y1 = this._y2 = this._y3 = this._y4 = NaN, this._point = 0
            }, lineEnd: function () {
                switch (this._point) {
                    case 1:
                        this._context.moveTo(this._x2, this._y2), this._context.closePath();
                        break;
                    case 2:
                        this._context.moveTo((this._x2 + 2 * this._x3) / 3, (this._y2 + 2 * this._y3) / 3), this._context.lineTo((this._x3 + 2 * this._x2) / 3, (this._y3 + 2 * this._y2) / 3), this._context.closePath();
                        break;
                    case 3:
                        this.point(this._x2, this._y2), this.point(this._x3, this._y3), this.point(this._x4, this._y4);
                        break
                }
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1, this._x2 = t, this._y2 = e;
                        break;
                    case 1:
                        this._point = 2, this._x3 = t, this._y3 = e;
                        break;
                    case 2:
                        this._point = 3, this._x4 = t, this._y4 = e, this._context.moveTo((this._x0 + 4 * this._x1 + t) / 6, (this._y0 + 4 * this._y1 + e) / 6);
                        break;
                    default:
                        Xf(this, t, e);
                        break
                }
                this._x0 = this._x1, this._x1 = t, this._y0 = this._y1, this._y1 = e
            }
        };

        function Jf(t) {
            this._context = t
        }

        Jf.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x0 = this._x1 = this._y0 = this._y1 = NaN, this._point = 0
            }, lineEnd: function () {
                (this._line || 0 !== this._line && 3 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1;
                        break;
                    case 1:
                        this._point = 2;
                        break;
                    case 2:
                        this._point = 3;
                        var n = (this._x0 + 4 * this._x1 + t) / 6, i = (this._y0 + 4 * this._y1 + e) / 6;
                        this._line ? this._context.lineTo(n, i) : this._context.moveTo(n, i);
                        break;
                    case 3:
                        this._point = 4;
                    default:
                        Xf(this, t, e);
                        break
                }
                this._x0 = this._x1, this._x1 = t, this._y0 = this._y1, this._y1 = e
            }
        };

        function Qf(t, e) {
            this._basis = new Kf(t), this._beta = e
        }

        Qf.prototype = {
            lineStart: function () {
                this._x = [], this._y = [], this._basis.lineStart()
            }, lineEnd: function () {
                var t = this._x, e = this._y, n = t.length - 1;
                if (n > 0) {
                    var i, a = t[0], r = e[0], s = t[n] - a, o = e[n] - r, c = -1;
                    while (++c <= n) i = c / n, this._basis.point(this._beta * t[c] + (1 - this._beta) * (a + i * s), this._beta * e[c] + (1 - this._beta) * (r + i * o))
                }
                this._x = this._y = null, this._basis.lineEnd()
            }, point: function (t, e) {
                this._x.push(+t), this._y.push(+e)
            }
        };
        (function t(e) {
            function n(t) {
                return 1 === e ? new Kf(t) : new Qf(t, e)
            }

            return n.beta = function (e) {
                return t(+e)
            }, n
        })(.85);

        function td(t, e, n) {
            t._context.bezierCurveTo(t._x1 + t._k * (t._x2 - t._x0), t._y1 + t._k * (t._y2 - t._y0), t._x2 + t._k * (t._x1 - e), t._y2 + t._k * (t._y1 - n), t._x2, t._y2)
        }

        function ed(t, e) {
            this._context = t, this._k = (1 - e) / 6
        }

        ed.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x0 = this._x1 = this._x2 = this._y0 = this._y1 = this._y2 = NaN, this._point = 0
            }, lineEnd: function () {
                switch (this._point) {
                    case 2:
                        this._context.lineTo(this._x2, this._y2);
                        break;
                    case 3:
                        td(this, this._x1, this._y1);
                        break
                }
                (this._line || 0 !== this._line && 1 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1, this._line ? this._context.lineTo(t, e) : this._context.moveTo(t, e);
                        break;
                    case 1:
                        this._point = 2, this._x1 = t, this._y1 = e;
                        break;
                    case 2:
                        this._point = 3;
                    default:
                        td(this, t, e);
                        break
                }
                this._x0 = this._x1, this._x1 = this._x2, this._x2 = t, this._y0 = this._y1, this._y1 = this._y2, this._y2 = e
            }
        };
        (function t(e) {
            function n(t) {
                return new ed(t, e)
            }

            return n.tension = function (e) {
                return t(+e)
            }, n
        })(0);

        function nd(t, e) {
            this._context = t, this._k = (1 - e) / 6
        }

        nd.prototype = {
            areaStart: Wf, areaEnd: Wf, lineStart: function () {
                this._x0 = this._x1 = this._x2 = this._x3 = this._x4 = this._x5 = this._y0 = this._y1 = this._y2 = this._y3 = this._y4 = this._y5 = NaN, this._point = 0
            }, lineEnd: function () {
                switch (this._point) {
                    case 1:
                        this._context.moveTo(this._x3, this._y3), this._context.closePath();
                        break;
                    case 2:
                        this._context.lineTo(this._x3, this._y3), this._context.closePath();
                        break;
                    case 3:
                        this.point(this._x3, this._y3), this.point(this._x4, this._y4), this.point(this._x5, this._y5);
                        break
                }
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1, this._x3 = t, this._y3 = e;
                        break;
                    case 1:
                        this._point = 2, this._context.moveTo(this._x4 = t, this._y4 = e);
                        break;
                    case 2:
                        this._point = 3, this._x5 = t, this._y5 = e;
                        break;
                    default:
                        td(this, t, e);
                        break
                }
                this._x0 = this._x1, this._x1 = this._x2, this._x2 = t, this._y0 = this._y1, this._y1 = this._y2, this._y2 = e
            }
        };
        (function t(e) {
            function n(t) {
                return new nd(t, e)
            }

            return n.tension = function (e) {
                return t(+e)
            }, n
        })(0);

        function id(t, e) {
            this._context = t, this._k = (1 - e) / 6
        }

        id.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x0 = this._x1 = this._x2 = this._y0 = this._y1 = this._y2 = NaN, this._point = 0
            }, lineEnd: function () {
                (this._line || 0 !== this._line && 3 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1;
                        break;
                    case 1:
                        this._point = 2;
                        break;
                    case 2:
                        this._point = 3, this._line ? this._context.lineTo(this._x2, this._y2) : this._context.moveTo(this._x2, this._y2);
                        break;
                    case 3:
                        this._point = 4;
                    default:
                        td(this, t, e);
                        break
                }
                this._x0 = this._x1, this._x1 = this._x2, this._x2 = t, this._y0 = this._y1, this._y1 = this._y2, this._y2 = e
            }
        };
        (function t(e) {
            function n(t) {
                return new id(t, e)
            }

            return n.tension = function (e) {
                return t(+e)
            }, n
        })(0);

        function ad(t, e, n) {
            var i = t._x1, a = t._y1, r = t._x2, s = t._y2;
            if (t._l01_a > Of) {
                var o = 2 * t._l01_2a + 3 * t._l01_a * t._l12_a + t._l12_2a, c = 3 * t._l01_a * (t._l01_a + t._l12_a);
                i = (i * o - t._x0 * t._l12_2a + t._x2 * t._l01_2a) / c, a = (a * o - t._y0 * t._l12_2a + t._y2 * t._l01_2a) / c
            }
            if (t._l23_a > Of) {
                var u = 2 * t._l23_2a + 3 * t._l23_a * t._l12_a + t._l12_2a, l = 3 * t._l23_a * (t._l23_a + t._l12_a);
                r = (r * u + t._x1 * t._l23_2a - e * t._l12_2a) / l, s = (s * u + t._y1 * t._l23_2a - n * t._l12_2a) / l
            }
            t._context.bezierCurveTo(i, a, r, s, t._x2, t._y2)
        }

        function rd(t, e) {
            this._context = t, this._alpha = e
        }

        rd.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x0 = this._x1 = this._x2 = this._y0 = this._y1 = this._y2 = NaN, this._l01_a = this._l12_a = this._l23_a = this._l01_2a = this._l12_2a = this._l23_2a = this._point = 0
            }, lineEnd: function () {
                switch (this._point) {
                    case 2:
                        this._context.lineTo(this._x2, this._y2);
                        break;
                    case 3:
                        this.point(this._x2, this._y2);
                        break
                }
                (this._line || 0 !== this._line && 1 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                if (t = +t, e = +e, this._point) {
                    var n = this._x2 - t, i = this._y2 - e;
                    this._l23_a = Math.sqrt(this._l23_2a = Math.pow(n * n + i * i, this._alpha))
                }
                switch (this._point) {
                    case 0:
                        this._point = 1, this._line ? this._context.lineTo(t, e) : this._context.moveTo(t, e);
                        break;
                    case 1:
                        this._point = 2;
                        break;
                    case 2:
                        this._point = 3;
                    default:
                        ad(this, t, e);
                        break
                }
                this._l01_a = this._l12_a, this._l12_a = this._l23_a, this._l01_2a = this._l12_2a, this._l12_2a = this._l23_2a, this._x0 = this._x1, this._x1 = this._x2, this._x2 = t, this._y0 = this._y1, this._y1 = this._y2, this._y2 = e
            }
        };
        (function t(e) {
            function n(t) {
                return e ? new rd(t, e) : new ed(t, 0)
            }

            return n.alpha = function (e) {
                return t(+e)
            }, n
        })(.5);

        function sd(t, e) {
            this._context = t, this._alpha = e
        }

        sd.prototype = {
            areaStart: Wf, areaEnd: Wf, lineStart: function () {
                this._x0 = this._x1 = this._x2 = this._x3 = this._x4 = this._x5 = this._y0 = this._y1 = this._y2 = this._y3 = this._y4 = this._y5 = NaN, this._l01_a = this._l12_a = this._l23_a = this._l01_2a = this._l12_2a = this._l23_2a = this._point = 0
            }, lineEnd: function () {
                switch (this._point) {
                    case 1:
                        this._context.moveTo(this._x3, this._y3), this._context.closePath();
                        break;
                    case 2:
                        this._context.lineTo(this._x3, this._y3), this._context.closePath();
                        break;
                    case 3:
                        this.point(this._x3, this._y3), this.point(this._x4, this._y4), this.point(this._x5, this._y5);
                        break
                }
            }, point: function (t, e) {
                if (t = +t, e = +e, this._point) {
                    var n = this._x2 - t, i = this._y2 - e;
                    this._l23_a = Math.sqrt(this._l23_2a = Math.pow(n * n + i * i, this._alpha))
                }
                switch (this._point) {
                    case 0:
                        this._point = 1, this._x3 = t, this._y3 = e;
                        break;
                    case 1:
                        this._point = 2, this._context.moveTo(this._x4 = t, this._y4 = e);
                        break;
                    case 2:
                        this._point = 3, this._x5 = t, this._y5 = e;
                        break;
                    default:
                        ad(this, t, e);
                        break
                }
                this._l01_a = this._l12_a, this._l12_a = this._l23_a, this._l01_2a = this._l12_2a, this._l12_2a = this._l23_2a, this._x0 = this._x1, this._x1 = this._x2, this._x2 = t, this._y0 = this._y1, this._y1 = this._y2, this._y2 = e
            }
        };
        (function t(e) {
            function n(t) {
                return e ? new sd(t, e) : new nd(t, 0)
            }

            return n.alpha = function (e) {
                return t(+e)
            }, n
        })(.5);

        function od(t, e) {
            this._context = t, this._alpha = e
        }

        od.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x0 = this._x1 = this._x2 = this._y0 = this._y1 = this._y2 = NaN, this._l01_a = this._l12_a = this._l23_a = this._l01_2a = this._l12_2a = this._l23_2a = this._point = 0
            }, lineEnd: function () {
                (this._line || 0 !== this._line && 3 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                if (t = +t, e = +e, this._point) {
                    var n = this._x2 - t, i = this._y2 - e;
                    this._l23_a = Math.sqrt(this._l23_2a = Math.pow(n * n + i * i, this._alpha))
                }
                switch (this._point) {
                    case 0:
                        this._point = 1;
                        break;
                    case 1:
                        this._point = 2;
                        break;
                    case 2:
                        this._point = 3, this._line ? this._context.lineTo(this._x2, this._y2) : this._context.moveTo(this._x2, this._y2);
                        break;
                    case 3:
                        this._point = 4;
                    default:
                        ad(this, t, e);
                        break
                }
                this._l01_a = this._l12_a, this._l12_a = this._l23_a, this._l01_2a = this._l12_2a, this._l12_2a = this._l23_2a, this._x0 = this._x1, this._x1 = this._x2, this._x2 = t, this._y0 = this._y1, this._y1 = this._y2, this._y2 = e
            }
        };
        (function t(e) {
            function n(t) {
                return e ? new od(t, e) : new id(t, 0)
            }

            return n.alpha = function (e) {
                return t(+e)
            }, n
        })(.5);

        function cd(t) {
            this._context = t
        }

        cd.prototype = {
            areaStart: Wf, areaEnd: Wf, lineStart: function () {
                this._point = 0
            }, lineEnd: function () {
                this._point && this._context.closePath()
            }, point: function (t, e) {
                t = +t, e = +e, this._point ? this._context.lineTo(t, e) : (this._point = 1, this._context.moveTo(t, e))
            }
        };

        function ud(t) {
            return t < 0 ? -1 : 1
        }

        function ld(t, e, n) {
            var i = t._x1 - t._x0, a = e - t._x1, r = (t._y1 - t._y0) / (i || a < 0 && -0),
                s = (n - t._y1) / (a || i < 0 && -0), o = (r * a + s * i) / (i + a);
            return (ud(r) + ud(s)) * Math.min(Math.abs(r), Math.abs(s), .5 * Math.abs(o)) || 0
        }

        function fd(t, e) {
            var n = t._x1 - t._x0;
            return n ? (3 * (t._y1 - t._y0) / n - e) / 2 : e
        }

        function dd(t, e, n) {
            var i = t._x0, a = t._y0, r = t._x1, s = t._y1, o = (r - i) / 3;
            t._context.bezierCurveTo(i + o, a + o * e, r - o, s - o * n, r, s)
        }

        function hd(t) {
            this._context = t
        }

        function pd(t) {
            this._context = new bd(t)
        }

        function bd(t) {
            this._context = t
        }

        function vd(t) {
            this._context = t
        }

        function _d(t) {
            var e, n, i = t.length - 1, a = new Array(i), r = new Array(i), s = new Array(i);
            for (a[0] = 0, r[0] = 2, s[0] = t[0] + 2 * t[1], e = 1; e < i - 1; ++e) a[e] = 1, r[e] = 4, s[e] = 4 * t[e] + 2 * t[e + 1];
            for (a[i - 1] = 2, r[i - 1] = 7, s[i - 1] = 8 * t[i - 1] + t[i], e = 1; e < i; ++e) n = a[e] / r[e - 1], r[e] -= n, s[e] -= n * s[e - 1];
            for (a[i - 1] = s[i - 1] / r[i - 1], e = i - 2; e >= 0; --e) a[e] = (s[e] - a[e + 1]) / r[e];
            for (r[i - 1] = (t[i] + a[i - 1]) / 2, e = 0; e < i - 1; ++e) r[e] = 2 * t[e + 1] - a[e + 1];
            return [a, r]
        }

        hd.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x0 = this._x1 = this._y0 = this._y1 = this._t0 = NaN, this._point = 0
            }, lineEnd: function () {
                switch (this._point) {
                    case 2:
                        this._context.lineTo(this._x1, this._y1);
                        break;
                    case 3:
                        dd(this, this._t0, fd(this, this._t0));
                        break
                }
                (this._line || 0 !== this._line && 1 === this._point) && this._context.closePath(), this._line = 1 - this._line
            }, point: function (t, e) {
                var n = NaN;
                if (t = +t, e = +e, t !== this._x1 || e !== this._y1) {
                    switch (this._point) {
                        case 0:
                            this._point = 1, this._line ? this._context.lineTo(t, e) : this._context.moveTo(t, e);
                            break;
                        case 1:
                            this._point = 2;
                            break;
                        case 2:
                            this._point = 3, dd(this, fd(this, n = ld(this, t, e)), n);
                            break;
                        default:
                            dd(this, this._t0, n = ld(this, t, e));
                            break
                    }
                    this._x0 = this._x1, this._x1 = t, this._y0 = this._y1, this._y1 = e, this._t0 = n
                }
            }
        }, (pd.prototype = Object.create(hd.prototype)).point = function (t, e) {
            hd.prototype.point.call(this, e, t)
        }, bd.prototype = {
            moveTo: function (t, e) {
                this._context.moveTo(e, t)
            }, closePath: function () {
                this._context.closePath()
            }, lineTo: function (t, e) {
                this._context.lineTo(e, t)
            }, bezierCurveTo: function (t, e, n, i, a, r) {
                this._context.bezierCurveTo(e, t, i, n, r, a)
            }
        }, vd.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x = [], this._y = []
            }, lineEnd: function () {
                var t = this._x, e = this._y, n = t.length;
                if (n) if (this._line ? this._context.lineTo(t[0], e[0]) : this._context.moveTo(t[0], e[0]), 2 === n) this._context.lineTo(t[1], e[1]); else for (var i = _d(t), a = _d(e), r = 0, s = 1; s < n; ++r, ++s) this._context.bezierCurveTo(i[0][r], a[0][r], i[1][r], a[1][r], t[s], e[s]);
                (this._line || 0 !== this._line && 1 === n) && this._context.closePath(), this._line = 1 - this._line, this._x = this._y = null
            }, point: function (t, e) {
                this._x.push(+t), this._y.push(+e)
            }
        };

        function gd(t, e) {
            this._context = t, this._t = e
        }

        gd.prototype = {
            areaStart: function () {
                this._line = 0
            }, areaEnd: function () {
                this._line = NaN
            }, lineStart: function () {
                this._x = this._y = NaN, this._point = 0
            }, lineEnd: function () {
                0 < this._t && this._t < 1 && 2 === this._point && this._context.lineTo(this._x, this._y), (this._line || 0 !== this._line && 1 === this._point) && this._context.closePath(), this._line >= 0 && (this._t = 1 - this._t, this._line = 1 - this._line)
            }, point: function (t, e) {
                switch (t = +t, e = +e, this._point) {
                    case 0:
                        this._point = 1, this._line ? this._context.lineTo(t, e) : this._context.moveTo(t, e);
                        break;
                    case 1:
                        this._point = 2;
                    default:
                        if (this._t <= 0) this._context.lineTo(this._x, e), this._context.lineTo(t, e); else {
                            var n = this._x * (1 - this._t) + t * this._t;
                            this._context.lineTo(n, this._y), this._context.lineTo(n, e)
                        }
                        break
                }
                this._x = t, this._y = e
            }
        };

        function md() {
            this._ = null
        }

        function yd(t) {
            t.U = t.C = t.L = t.R = t.P = t.N = null
        }

        function xd(t, e) {
            var n = e, i = e.R, a = n.U;
            a ? a.L === n ? a.L = i : a.R = i : t._ = i, i.U = a, n.U = i, n.R = i.L, n.R && (n.R.U = n), i.L = n
        }

        function wd(t, e) {
            var n = e, i = e.L, a = n.U;
            a ? a.L === n ? a.L = i : a.R = i : t._ = i, i.U = a, n.U = i, n.L = i.R, n.L && (n.L.U = n), i.R = n
        }

        function Od(t) {
            while (t.L) t = t.L;
            return t
        }

        md.prototype = {
            constructor: md, insert: function (t, e) {
                var n, i, a;
                if (t) {
                    if (e.P = t, e.N = t.N, t.N && (t.N.P = e), t.N = e, t.R) {
                        t = t.R;
                        while (t.L) t = t.L;
                        t.L = e
                    } else t.R = e;
                    n = t
                } else this._ ? (t = Od(this._), e.P = null, e.N = t, t.P = t.L = e, n = t) : (e.P = e.N = null, this._ = e, n = null);
                e.L = e.R = null, e.U = n, e.C = !0, t = e;
                while (n && n.C) i = n.U, n === i.L ? (a = i.R, a && a.C ? (n.C = a.C = !1, i.C = !0, t = i) : (t === n.R && (xd(this, n), t = n, n = t.U), n.C = !1, i.C = !0, wd(this, i))) : (a = i.L, a && a.C ? (n.C = a.C = !1, i.C = !0, t = i) : (t === n.L && (wd(this, n), t = n, n = t.U), n.C = !1, i.C = !0, xd(this, i))), n = t.U;
                this._.C = !1
            }, remove: function (t) {
                t.N && (t.N.P = t.P), t.P && (t.P.N = t.N), t.N = t.P = null;
                var e, n, i, a = t.U, r = t.L, s = t.R;
                if (n = r ? s ? Od(s) : r : s, a ? a.L === t ? a.L = n : a.R = n : this._ = n, r && s ? (i = n.C, n.C = t.C, n.L = r, r.U = n, n !== s ? (a = n.U, n.U = t.U, t = n.R, a.L = t, n.R = s, s.U = n) : (n.U = a, a = n, t = n.R)) : (i = t.C, t = n), t && (t.U = a), !i) if (t && t.C) t.C = !1; else {
                    do {
                        if (t === this._) break;
                        if (t === a.L) {
                            if (e = a.R, e.C && (e.C = !1, a.C = !0, xd(this, a), e = a.R), e.L && e.L.C || e.R && e.R.C) {
                                e.R && e.R.C || (e.L.C = !1, e.C = !0, wd(this, e), e = a.R), e.C = a.C, a.C = e.R.C = !1, xd(this, a), t = this._;
                                break
                            }
                        } else if (e = a.L, e.C && (e.C = !1, a.C = !0, wd(this, a), e = a.L), e.L && e.L.C || e.R && e.R.C) {
                            e.L && e.L.C || (e.R.C = !1, e.C = !0, xd(this, e), e = a.L), e.C = a.C, a.C = e.L.C = !1, wd(this, a), t = this._;
                            break
                        }
                        e.C = !0, t = a, a = a.U
                    } while (!t.C);
                    t && (t.C = !1)
                }
            }
        };
        var Md = md;

        function Cd(t, e, n, i) {
            var a = [null, null], r = Qd.push(a) - 1;
            return a.left = t, a.right = e, n && Ad(a, t, e, n), i && Ad(a, e, t, i), Zd[t.index].halfedges.push(r), Zd[e.index].halfedges.push(r), a
        }

        function kd(t, e, n) {
            var i = [e, n];
            return i.left = t, i
        }

        function Ad(t, e, n, i) {
            t[0] || t[1] ? t.left === n ? t[1] = i : t[0] = i : (t[0] = i, t.left = e, t.right = n)
        }

        function Td(t, e, n, i, a) {
            var r, s = t[0], o = t[1], c = s[0], u = s[1], l = o[0], f = o[1], d = 0, h = 1, p = l - c, b = f - u;
            if (r = e - c, p || !(r > 0)) {
                if (r /= p, p < 0) {
                    if (r < d) return;
                    r < h && (h = r)
                } else if (p > 0) {
                    if (r > h) return;
                    r > d && (d = r)
                }
                if (r = i - c, p || !(r < 0)) {
                    if (r /= p, p < 0) {
                        if (r > h) return;
                        r > d && (d = r)
                    } else if (p > 0) {
                        if (r < d) return;
                        r < h && (h = r)
                    }
                    if (r = n - u, b || !(r > 0)) {
                        if (r /= b, b < 0) {
                            if (r < d) return;
                            r < h && (h = r)
                        } else if (b > 0) {
                            if (r > h) return;
                            r > d && (d = r)
                        }
                        if (r = a - u, b || !(r < 0)) {
                            if (r /= b, b < 0) {
                                if (r > h) return;
                                r > d && (d = r)
                            } else if (b > 0) {
                                if (r < d) return;
                                r < h && (h = r)
                            }
                            return !(d > 0 || h < 1) || (d > 0 && (t[0] = [c + d * p, u + d * b]), h < 1 && (t[1] = [c + h * p, u + h * b]), !0)
                        }
                    }
                }
            }
        }

        function Nd(t, e, n, i, a) {
            var r = t[1];
            if (r) return !0;
            var s, o, c = t[0], u = t.left, l = t.right, f = u[0], d = u[1], h = l[0], p = l[1], b = (f + h) / 2,
                v = (d + p) / 2;
            if (p === d) {
                if (b < e || b >= i) return;
                if (f > h) {
                    if (c) {
                        if (c[1] >= a) return
                    } else c = [b, n];
                    r = [b, a]
                } else {
                    if (c) {
                        if (c[1] < n) return
                    } else c = [b, a];
                    r = [b, n]
                }
            } else if (s = (f - h) / (p - d), o = v - s * b, s < -1 || s > 1) if (f > h) {
                if (c) {
                    if (c[1] >= a) return
                } else c = [(n - o) / s, n];
                r = [(a - o) / s, a]
            } else {
                if (c) {
                    if (c[1] < n) return
                } else c = [(a - o) / s, a];
                r = [(n - o) / s, n]
            } else if (d < p) {
                if (c) {
                    if (c[0] >= i) return
                } else c = [e, s * e + o];
                r = [i, s * i + o]
            } else {
                if (c) {
                    if (c[0] < e) return
                } else c = [i, s * i + o];
                r = [e, s * e + o]
            }
            return t[0] = c, t[1] = r, !0
        }

        function Pd(t, e, n, i) {
            var a, r = Qd.length;
            while (r--) Nd(a = Qd[r], t, e, n, i) && Td(a, t, e, n, i) && (Math.abs(a[0][0] - a[1][0]) > th || Math.abs(a[0][1] - a[1][1]) > th) || delete Qd[r]
        }

        function Sd(t) {
            return Zd[t.index] = {site: t, halfedges: []}
        }

        function $d(t, e) {
            var n = t.site, i = e.left, a = e.right;
            return n === a && (a = i, i = n), a ? Math.atan2(a[1] - i[1], a[0] - i[0]) : (n === i ? (i = e[1], a = e[0]) : (i = e[0], a = e[1]), Math.atan2(i[0] - a[0], a[1] - i[1]))
        }

        function Ed(t, e) {
            return e[+(e.left !== t.site)]
        }

        function jd(t, e) {
            return e[+(e.left === t.site)]
        }

        function Id() {
            for (var t, e, n, i, a = 0, r = Zd.length; a < r; ++a) if ((t = Zd[a]) && (i = (e = t.halfedges).length)) {
                var s = new Array(i), o = new Array(i);
                for (n = 0; n < i; ++n) s[n] = n, o[n] = $d(t, Qd[e[n]]);
                for (s.sort(function (t, e) {
                    return o[e] - o[t]
                }), n = 0; n < i; ++n) o[n] = e[s[n]];
                for (n = 0; n < i; ++n) e[n] = o[n]
            }
        }

        function Ld(t, e, n, i) {
            var a, r, s, o, c, u, l, f, d, h, p, b, v = Zd.length, _ = !0;
            for (a = 0; a < v; ++a) if (r = Zd[a]) {
                s = r.site, c = r.halfedges, o = c.length;
                while (o--) Qd[c[o]] || c.splice(o, 1);
                o = 0, u = c.length;
                while (o < u) h = jd(r, Qd[c[o]]), p = h[0], b = h[1], l = Ed(r, Qd[c[++o % u]]), f = l[0], d = l[1], (Math.abs(p - f) > th || Math.abs(b - d) > th) && (c.splice(o, 0, Qd.push(kd(s, h, Math.abs(p - t) < th && i - b > th ? [t, Math.abs(f - t) < th ? d : i] : Math.abs(b - i) < th && n - p > th ? [Math.abs(d - i) < th ? f : n, i] : Math.abs(p - n) < th && b - e > th ? [n, Math.abs(f - n) < th ? d : e] : Math.abs(b - e) < th && p - t > th ? [Math.abs(d - e) < th ? f : t, e] : null)) - 1), ++u);
                u && (_ = !1)
            }
            if (_) {
                var g, m, y, x = 1 / 0;
                for (a = 0, _ = null; a < v; ++a) (r = Zd[a]) && (s = r.site, g = s[0] - t, m = s[1] - e, y = g * g + m * m, y < x && (x = y, _ = r));
                if (_) {
                    var w = [t, e], O = [t, i], M = [n, i], C = [n, e];
                    _.halfedges.push(Qd.push(kd(s = _.site, w, O)) - 1, Qd.push(kd(s, O, M)) - 1, Qd.push(kd(s, M, C)) - 1, Qd.push(kd(s, C, w)) - 1)
                }
            }
            for (a = 0; a < v; ++a) (r = Zd[a]) && (r.halfedges.length || delete Zd[a])
        }

        var Ud, Rd = [];

        function Dd() {
            yd(this), this.x = this.y = this.arc = this.site = this.cy = null
        }

        function Vd(t) {
            var e = t.P, n = t.N;
            if (e && n) {
                var i = e.site, a = t.site, r = n.site;
                if (i !== r) {
                    var s = a[0], o = a[1], c = i[0] - s, u = i[1] - o, l = r[0] - s, f = r[1] - o,
                        d = 2 * (c * f - u * l);
                    if (!(d >= -eh)) {
                        var h = c * c + u * u, p = l * l + f * f, b = (f * h - u * p) / d, v = (c * p - l * h) / d,
                            _ = Rd.pop() || new Dd;
                        _.arc = t, _.site = a, _.x = b + s, _.y = (_.cy = v + o) + Math.sqrt(b * b + v * v), t.circle = _;
                        var g = null, m = Jd._;
                        while (m) if (_.y < m.y || _.y === m.y && _.x <= m.x) {
                            if (!m.L) {
                                g = m.P;
                                break
                            }
                            m = m.L
                        } else {
                            if (!m.R) {
                                g = m;
                                break
                            }
                            m = m.R
                        }
                        Jd.insert(g, _), g || (Ud = _)
                    }
                }
            }
        }

        function Bd(t) {
            var e = t.circle;
            e && (e.P || (Ud = e.N), Jd.remove(e), Rd.push(e), yd(e), t.circle = null)
        }

        var Fd = [];

        function Hd() {
            yd(this), this.edge = this.site = this.circle = null
        }

        function qd(t) {
            var e = Fd.pop() || new Hd;
            return e.site = t, e
        }

        function Gd(t) {
            Bd(t), Kd.remove(t), Fd.push(t), yd(t)
        }

        function zd(t) {
            var e = t.circle, n = e.x, i = e.cy, a = [n, i], r = t.P, s = t.N, o = [t];
            Gd(t);
            var c = r;
            while (c.circle && Math.abs(n - c.circle.x) < th && Math.abs(i - c.circle.cy) < th) r = c.P, o.unshift(c), Gd(c), c = r;
            o.unshift(c), Bd(c);
            var u = s;
            while (u.circle && Math.abs(n - u.circle.x) < th && Math.abs(i - u.circle.cy) < th) s = u.N, o.push(u), Gd(u), u = s;
            o.push(u), Bd(u);
            var l, f = o.length;
            for (l = 1; l < f; ++l) u = o[l], c = o[l - 1], Ad(u.edge, c.site, u.site, a);
            c = o[0], u = o[f - 1], u.edge = Cd(c.site, u.site, null, a), Vd(c), Vd(u)
        }

        function Yd(t) {
            var e, n, i, a, r = t[0], s = t[1], o = Kd._;
            while (o) if (i = Wd(o, s) - r, i > th) o = o.L; else {
                if (a = r - Xd(o, s), !(a > th)) {
                    i > -th ? (e = o.P, n = o) : a > -th ? (e = o, n = o.N) : e = n = o;
                    break
                }
                if (!o.R) {
                    e = o;
                    break
                }
                o = o.R
            }
            Sd(t);
            var c = qd(t);
            if (Kd.insert(e, c), e || n) {
                if (e === n) return Bd(e), n = qd(e.site), Kd.insert(c, n), c.edge = n.edge = Cd(e.site, c.site), Vd(e), void Vd(n);
                if (n) {
                    Bd(e), Bd(n);
                    var u = e.site, l = u[0], f = u[1], d = t[0] - l, h = t[1] - f, p = n.site, b = p[0] - l,
                        v = p[1] - f, _ = 2 * (d * v - h * b), g = d * d + h * h, m = b * b + v * v,
                        y = [(v * g - h * m) / _ + l, (d * m - b * g) / _ + f];
                    Ad(n.edge, u, p, y), c.edge = Cd(u, t, null, y), n.edge = Cd(t, p, null, y), Vd(e), Vd(n)
                } else c.edge = Cd(e.site, c.site)
            }
        }

        function Wd(t, e) {
            var n = t.site, i = n[0], a = n[1], r = a - e;
            if (!r) return i;
            var s = t.P;
            if (!s) return -1 / 0;
            n = s.site;
            var o = n[0], c = n[1], u = c - e;
            if (!u) return o;
            var l = o - i, f = 1 / r - 1 / u, d = l / u;
            return f ? (-d + Math.sqrt(d * d - 2 * f * (l * l / (-2 * u) - c + u / 2 + a - r / 2))) / f + i : (i + o) / 2
        }

        function Xd(t, e) {
            var n = t.N;
            if (n) return Wd(n, e);
            var i = t.site;
            return i[1] === e ? i[0] : 1 / 0
        }

        var Kd, Zd, Jd, Qd, th = 1e-6, eh = 1e-12;

        function nh(t, e, n) {
            return (t[0] - n[0]) * (e[1] - t[1]) - (t[0] - e[0]) * (n[1] - t[1])
        }

        function ih(t, e) {
            return e[1] - t[1] || e[0] - t[0]
        }

        function ah(t, e) {
            var n, i, a, r = t.sort(ih).pop();
            Qd = [], Zd = new Array(t.length), Kd = new Md, Jd = new Md;
            while (1) if (a = Ud, r && (!a || r[1] < a.y || r[1] === a.y && r[0] < a.x)) r[0] === n && r[1] === i || (Yd(r), n = r[0], i = r[1]), r = t.pop(); else {
                if (!a) break;
                zd(a.arc)
            }
            if (Id(), e) {
                var s = +e[0][0], o = +e[0][1], c = +e[1][0], u = +e[1][1];
                Pd(s, o, c, u), Ld(s, o, c, u)
            }
            this.edges = Qd, this.cells = Zd, Kd = Jd = Qd = Zd = null
        }

        ah.prototype = {
            constructor: ah, polygons: function () {
                var t = this.edges;
                return this.cells.map(function (e) {
                    var n = e.halfedges.map(function (n) {
                        return Ed(e, t[n])
                    });
                    return n.data = e.site.data, n
                })
            }, triangles: function () {
                var t = [], e = this.edges;
                return this.cells.forEach(function (n, i) {
                    if (r = (a = n.halfedges).length) {
                        var a, r, s, o = n.site, c = -1, u = e[a[r - 1]], l = u.left === o ? u.right : u.left;
                        while (++c < r) s = l, u = e[a[c]], l = u.left === o ? u.right : u.left, s && l && i < s.index && i < l.index && nh(o, s, l) < 0 && t.push([o.data, s.data, l.data])
                    }
                }), t
            }, links: function () {
                return this.edges.filter(function (t) {
                    return t.right
                }).map(function (t) {
                    return {source: t.left.data, target: t.right.data}
                })
            }, find: function (t, e, n) {
                var i, a, r = this, s = r._found || 0, o = r.cells.length;
                while (!(a = r.cells[s])) if (++s >= o) return null;
                var c = t - a.site[0], u = e - a.site[1], l = c * c + u * u;
                do {
                    a = r.cells[i = s], s = null, a.halfedges.forEach(function (n) {
                        var i = r.edges[n], o = i.left;
                        if (o !== a.site && o || (o = i.right)) {
                            var c = t - o[0], u = e - o[1], f = c * c + u * u;
                            f < l && (l = f, s = o.index)
                        }
                    })
                } while (null !== s);
                return r._found = i, null == n || l <= n * n ? a.site : null
            }
        };

        function rh(t, e, n) {
            this.k = t, this.x = e, this.y = n
        }

        rh.prototype = {
            constructor: rh, scale: function (t) {
                return 1 === t ? this : new rh(this.k * t, this.x, this.y)
            }, translate: function (t, e) {
                return 0 === t & 0 === e ? this : new rh(this.k, this.x + this.k * t, this.y + this.k * e)
            }, apply: function (t) {
                return [t[0] * this.k + this.x, t[1] * this.k + this.y]
            }, applyX: function (t) {
                return t * this.k + this.x
            }, applyY: function (t) {
                return t * this.k + this.y
            }, invert: function (t) {
                return [(t[0] - this.x) / this.k, (t[1] - this.y) / this.k]
            }, invertX: function (t) {
                return (t - this.x) / this.k
            }, invertY: function (t) {
                return (t - this.y) / this.k
            }, rescaleX: function (t) {
                return t.copy().domain(t.range().map(this.invertX, this).map(t.invert, t))
            }, rescaleY: function (t) {
                return t.copy().domain(t.range().map(this.invertY, this).map(t.invert, t))
            }, toString: function () {
                return "translate(" + this.x + "," + this.y + ") scale(" + this.k + ")"
            }
        };
        var sh = new rh(1, 0, 0);

        function oh(t) {
            return t.__zoom || sh
        }

        oh.prototype = rh.prototype;
        n.d(e, "d", function () {
            return Ve
        }), n.d(e, "e", function () {
            return Pc
        }), n.d(e, "f", function () {
            return pe
        }), n.d(e, "a", function () {
            return Lf
        }), n.d(e, "b", function () {
            return Bf
        }), n.d(e, "c", function () {
            return qf
        })
    }, "5d16": function (t, e, n) {
        "use strict";
        var i = n("aaf7"), a = n.n(i);
        a.a
    }, 6259: function (t, e, n) {
    }, "62c8": function (t, e, n) {
    }, "649d": function (t, e, n) {
        "use strict";
        (function (t) {
            n("20d6");
            var i = n("75fc"), a = n("768b"), r = n("f087"), s = n("a772");
            e["a"] = {
                name: "AdsSelection",
                props: {
                    type: {type: String, default: "WEB"}, selected: {
                        type: Array, default: function () {
                            return []
                        }
                    }, showLabel: {type: Boolean, default: !0}, options: {
                        type: Array, default: function () {
                            return []
                        }
                    }
                },
                render: function () {
                    var t = arguments[0];
                    return t(r["h"], [this.showLabel && t(r["e"], [this.$t("COMMON.ads_selection_text")]), t(r["g"], [this.options.map(this.renderBox)])])
                },
                methods: {
                    renderBox: function (t) {
                        var e = this, n = this.$createElement, i = t.channel, s = this.meta(t),
                            o = Object(a["a"])(s, 2), c = o[0], u = o[1];
                        return n(r["d"], {
                            attrs: {selected: this.idxByValue(t) > -1}, on: {
                                click: function () {
                                    return e.toggleSelected(t)
                                }
                            }
                        }, [n(r["f"], {attrs: {src: u}}), n(r["c"], [n(r["a"], {attrs: {src: "/img/icons/".concat(i, "_logo_goal_selection.png")}}), n(r["b"], [c])])])
                    }, toggleSelected: function (t) {
                        var e = this.idxByValue(t), n = Object(i["a"])(this._selected);
                        e > -1 ? n.splice(e, 1) : n.push(t), this._selected = n
                    }, idxByValue: function () {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, n = e.channel,
                            i = e.creativeType;
                        return t.findIndex(t.whereEq({channel: n, creativeType: i}), this._selected)
                    }, meta: function (e) {
                        var n = this;
                        return t.cond([[s["n"], function () {
                            return [n.$t("COMMON.ad_types_type_facebookFeed"), "/img/icons/facebook_thumb_icon.png"]
                        }], [s["u"], function () {
                            return [n.$t("COMMON.ad_types_type_instagramFeed"), "/img/icons/instagram_thumb_icon.png"]
                        }], [s["r"], function () {
                            return [n.$t("COMMON.ad_types_type_googleSearch"), "/img/icons/gs_thumb_icon.png"]
                        }], [s["q"], function () {
                            return [n.$t("COMMON.ad_types_type_googleDisplay"), "/img/onboarding/googleDisplay_preview.png"]
                        }], [s["t"], function () {
                            return [n.$t("COMMON.ad_types_type_googleUniversalApp"), "/img/icons/uac_thumb_icon.png"]
                        }], [s["m"], function () {
                            return [n.$t("COMMON.ad_types_type_appleSearch"), "/img/icons/apple_thumb_icon.png"]
                        }]])(e)
                    }
                },
                computed: {
                    _selected: {
                        get: t.prop("selected"), set: function (t) {
                            this.$emit("update:selected", t)
                        }
                    }
                }
            }
        }).call(this, n("b17e"))
    }, 6743: function (t, e, n) {
        "use strict";
        var i = function () {
            var t = this, e = t.$createElement;
            t._self._c;
            return t._m(0)
        }, a = [function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "tooltip-google-wrapper"}, [n("img", {attrs: {src: "/img/proposal/display_etc_ex.png"}}), n("span", [t._v("\n        Through Google Display Network, your ads will reach people while they're browsing their favorite websites, checking their Gmail account, or using mobile devices.\n    ")])])
        }], r = (n("2b0e6"), n("2877")), s = {}, o = Object(r["a"])(s, i, a, !1, null, "10815d52", null);
        e["a"] = o.exports
    }, "68b6": function (t, e, n) {
        "use strict";
        (function (t) {
            n("7514"), n("ac6a");
            var i = n("75fc"), a = (n("c5f6"), n("2f62"), n("e380")), r = n.n(a), s = n("b047"), o = n.n(s),
                c = n("eafc"), u = n.n(c), l = n("4c6b"), f = n("6e77"), d = n("1b92"), h = n("a748"), p = n("17f5"),
                b = n("c4cc"), v = n("4677"), _ = n("ebb6"), g = n("5670"), m = n("d792"), y = n("1a2d"), x = n("9f2d"),
                w = n("baba"), O = n("12e7"), M = n("da7a");
            e["a"] = {
                created: function () {
                    this.searchFunc && (this._searchFunc = r()(t.unary(this.searchFunc)), this.$nextTick(this.registerSearch))
                },
                props: {
                    active: {type: Boolean, default: !1},
                    type: {type: String, required: !0},
                    specialsGiven: {type: [Array, void 0], default: void 0},
                    editable: {type: Boolean, default: !0},
                    headerText: String,
                    placeholder: String,
                    addType: {type: String, default: "general"},
                    searchFunc: {type: [Function, void 0], default: void 0},
                    nameGetter: {type: Function, default: t.identity},
                    valueGetter: {type: Function, default: t.identity},
                    inputValidator: {type: Function, default: t.always(!0)},
                    inputWarnMessage: {type: String, default: "warning"},
                    addedValues: {type: Array, default: t.always([])},
                    currentEditIdx: Number,
                    isKeepSearchType: {type: Boolean, default: !1}
                },
                data: function () {
                    return {
                        values: Object(i["a"])(this.addedValues),
                        channels: [],
                        top: void 0,
                        left: "auto",
                        checked: [],
                        autoCompleteData: [],
                        searchKeyword: "",
                        isValid: void 0,
                        phone: "",
                        isLoading: !1,
                        tempKeyword: "",
                        tempAutoCompleteData: []
                    }
                },
                computed: {
                    wrapperStyle: function () {
                        return {top: this.top, left: this.left}
                    }, _active: {
                        get: t.prop("active"), set: function (t) {
                            this.$emit("update:active", t)
                        }
                    }, autoCompleteArr: function () {
                        return this.autoCompleteData.map(this.nameGetter)
                    }
                },
                components: {"editable-box": w["a"], "editable-input": O["a"], ClipLoader: M["a"]},
                methods: {
                    initValues: function () {
                        this.values = Object(i["a"])(this.addedValues), this.searchKeyword = ""
                    }, confirm: function () {
                        "contact" === this.type ? this.addPhone(!0) : "general" === this.addType && this.addValue(!0);
                        var e = this.values;
                        t.all(t.is(String), e) && (e = t.uniqBy(t.toLower, e)), this.$emit("confirm", e), this.initValues()
                    }, cancel: function () {
                        this._active = !1, this.$emit("cancel"), this.initValues()
                    }, addValue: function (e) {
                        var n = this, i = this.searchKeyword.trim();
                        if (this.$nextTick(function () {
                                n.$refs.input && n.$refs.input.focus()
                            }), i) {
                            var a = t.find(t.compose(t.equals(t.toLower(i)), t.toLower), this.values);
                            if (a) return !0 !== e && this.toast(l["b"]);
                            if (this.searchKeyword = "", !this.inputValidator(i)) return this.searchKeyword = i, void(!0 !== e && this.toast(this.inputWarnMessage));
                            this.values.push(i)
                        }
                    }, dAddValue: o()(function (t) {
                        this.addValue(t)
                    }, 120), addAutoCompleteValue: function (t) {
                        var e = this;
                        if (t) {
                            var n, i = this.autoCompleteArr.indexOf(t);
                            if (n = i > -1 ? this.autoCompleteData[i] : t, this.values.push(n), this.isKeepSearchType) return this.tempKeyword = this.searchKeyword, void u()(function () {
                                return e.searchKeyword = e.tempKeyword
                            }, 0);
                            this.autoCompleteData = [], this.$refs.input.focus()
                        }
                    }, focusInput: function () {
                        this.isKeepSearchType && (this.autoCompleteData = this.tempAutoCompleteData)
                    }, handleDelete: function (t) {
                        this.values.splice(t, 1)
                    }, handleChange: function (t) {
                        var e = t.idx, n = t.value;
                        this.values.splice(e, 1, n)
                    }, handleChannelClick: function (t) {
                        var e = this.channels.indexOf(t);
                        e > -1 ? this.channels.splice(e, 1) : this.channels.push(t)
                    }, onInput: function (t) {
                        t.number;
                        var e = t.isValid, n = t.country;
                        this.country = n, this.isValid = e
                    }, addPhone: function (e) {
                        if (this.isValid) {
                            var n = {
                                reservation: !1,
                                type: "call",
                                clickTarget: {countryCode: this.country.iso2, phoneNumber: this.searchKeyword}
                            }, i = t.find(t.anyPass([t.equals(n), t.compose(t.contains(n), t.values)]), this.values);
                            if (i) return !0 !== e && this.toast("There's already the same one");
                            this.searchKeyword = "", this.values.push(n), this.isValid = !1
                        } else !0 !== e && this.toast("please enter right phone number")
                    }, registerSearch: function () {
                        var e, n = this, i = this.$refs.input.$eventToObservable("input");
                        e = this.$refs.searchBtn ? Object(f["a"])(this.$refs.searchBtn, "click") : Object(d["b"])();
                        var a = Object(h["a"])(i, e).pipe(Object(b["a"])(function () {
                            return n.autoCompleteData = []
                        }), Object(v["a"])(250), Object(_["a"])(function () {
                            return n.searchKeyword.trim()
                        }), Object(g["a"])(t.propSatisfies(t.gt(t.__, 0), "length")), Object(b["a"])(function () {
                            return n.isLoading = !0
                        }), Object(m["a"])(this._searchFunc), Object(_["a"])(t.without(this.values)), Object(b["a"])(function () {
                            return n.isLoading = !1
                        }));
                        this.$subscribeTo(a, function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                            n.autoCompleteData = t, n.isKeepSearchType && (n.tempAutoCompleteData = n.autoCompleteData)
                        })
                    }
                },
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        void 0 === t.currentEditIdx && t.$refs.input && t.$refs.input.focus()
                    })
                },
                subscriptions: function () {
                    var e = Object(h["a"])(Object(p["a"])(""), Object(f["a"])(window, "resize")).pipe(Object(y["a"])(200), Object(_["a"])(function () {
                        return .8 * window.innerHeight
                    }), Object(_["a"])(t.flip(t.subtract)(200)), Object(x["a"])(this.getBeforeDestroy$()));
                    return {maxHeight: e}
                }
            }
        }).call(this, n("b17e"))
    }, "68fe": function (t, e, n) {
        "use strict";
        var i = n("b024"), a = n.n(i);
        a.a
    }, "6a20": function (t, e, n) {
    }, "6b11": function (t, e, n) {
        "use strict";
        var i = n("1fcf"), a = n.n(i);
        a.a
    }, "6c82": function (t, e, n) {
    }, "72bf": function (t, e, n) {
        "use strict";
        var i = n("6259"), a = n.n(i);
        a.a
    }, "72f0": function (t, e) {
        function n(t) {
            return function () {
                return t
            }
        }

        t.exports = n
    }, "73ff": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "on-off-label-list-wrapper"}, t._l(t.values, function (e, i) {
                    return n("span", {
                        key: t.nameGetter(e),
                        staticClass: "on-off-list-element",
                        class: "on-off-list-element" + i
                    }, [n("img", {
                        staticClass: "thumb-img",
                        attrs: {src: t.images[i]}
                    }), n("span", {staticClass: "on-off-label"}, [t._v(t._s(t.nameGetter(e)))]), n("span", {staticClass: "on-off-active-switch-wrapper"}, [n("switch-view", {
                        ref: "switch",
                        refInFor: !0,
                        attrs: {className: "on-off-active-switch", value: t.valueGetter(e)},
                        on: {
                            "update:value": function (e) {
                                return t.switchValueChanged(i, e)
                            }
                        },
                        nativeOn: {
                            click: function (t) {
                                t.stopPropagation()
                            }
                        }
                    })], 1)])
                }), 0)
            }, a = [], r = n("38aa"), s = r["a"], o = (n("c610"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, "d9507668", null);
        e["a"] = c.exports
    }, "766f": function (t, e, n) {
        "use strict";
        var i = n("f71a"), a = n.n(i);
        a.a
    }, 7738: function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return l
        });
        var i = n("9ab4"), a = n("9e46"), r = n("8ac6"), s = n("3060"), o = n("ce8b"), c = n("2144"), u = {};

        function l() {
            for (var t = [], e = 0; e < arguments.length; e++) t[e] = arguments[e];
            var n = null, i = null;
            return Object(a["a"])(t[t.length - 1]) && (i = t.pop()), "function" === typeof t[t.length - 1] && (n = t.pop()), 1 === t.length && Object(r["a"])(t[0]) && (t = t[0]), Object(c["a"])(t, i).lift(new f(n))
        }

        var f = function () {
            function t(t) {
                this.resultSelector = t
            }

            return t.prototype.call = function (t, e) {
                return e.subscribe(new d(t, this.resultSelector))
            }, t
        }(), d = function (t) {
            function e(e, n) {
                var i = t.call(this, e) || this;
                return i.resultSelector = n, i.active = 0, i.values = [], i.observables = [], i
            }

            return i["a"](e, t), e.prototype._next = function (t) {
                this.values.push(u), this.observables.push(t)
            }, e.prototype._complete = function () {
                var t = this.observables, e = t.length;
                if (0 === e) this.destination.complete(); else {
                    this.active = e, this.toRespond = e;
                    for (var n = 0; n < e; n++) {
                        var i = t[n];
                        this.add(Object(o["a"])(this, i, i, n))
                    }
                }
            }, e.prototype.notifyComplete = function (t) {
                0 === (this.active -= 1) && this.destination.complete()
            }, e.prototype.notifyNext = function (t, e, n, i, a) {
                var r = this.values, s = r[n], o = this.toRespond ? s === u ? --this.toRespond : this.toRespond : 0;
                r[n] = e, 0 === o && (this.resultSelector ? this._tryResultSelector(r) : this.destination.next(r.slice()))
            }, e.prototype._tryResultSelector = function (t) {
                var e;
                try {
                    e = this.resultSelector.apply(this, t)
                } catch (n) {
                    return void this.destination.error(n)
                }
                this.destination.next(e)
            }, e
        }(s["a"])
    }, "7c36": function (t, e, n) {
    }, "830c": function (t, e, n) {
        "use strict";
        n.d(e, "d", function () {
            return h
        }), n.d(e, "b", function () {
            return p
        }), n.d(e, "c", function () {
            return b
        }), n.d(e, "a", function () {
            return v
        }), n.d(e, "e", function () {
            return _
        });
        var i = n("aede"), a = n("9c56"), r = n("bd2c");

        function s() {
            var t = Object(i["a"])(["\n    width: 90%;\n"]);
            return s = function () {
                return t
            }, t
        }

        function o() {
            var t = Object(i["a"])(["\n    ", "\n    height: 30px;\n    border-radius: 3px;\n    background-color: #5aabe3;\n    ", "\n    font-size: 13px;\n    font-weight: 600;\n    text-align: center;\n    color: white;\n    margin-top: 15px;\n"]);
            return o = function () {
                return t
            }, t
        }

        function c() {
            var t = Object(i["a"])(["\n    ", "\n    font-size: 14px;\n    text-align: center;\n    color: #333333;\n    white-space: pre-line;\n"]);
            return c = function () {
                return t
            }, t
        }

        function u() {
            var t = Object(i["a"])(["\n    height: 150px;\n    border-radius: 5px;\n    background-color: white;\n    ", "\n    flex-direction: column;\n    box-shadow: 0px 3px 10px 0 rgba(0, 0, 0, 0.3);\n    border: solid 2px #5aabe3;\n"]);
            return u = function () {
                return t
            }, t
        }

        function l() {
            var t = Object(i["a"])(["\n    top: 0;\n    border-radius: 5px;\n    background-color: rgba(255, 255, 255, 0.7);\n"]);
            return l = function () {
                return t
            }, t
        }

        function f() {
            var t = Object(i["a"])(["\n    position: absolute;\n    top: 45px;\n    right: 0;\n    left: 0;\n    bottom: 0;\n    background-color: rgba(90, 171, 227, 0.3);\n    z-index: 2;\n    ", "\n    border-bottom-right-radius: 5px;\n    border-bottom-left-radius: 5px;\n"]);
            return f = function () {
                return t
            }, t
        }

        var d = a["b"].div(f(), r["f"]), h = Object(a["b"])(d)(l()), p = a["b"].div(u(), r["f"]),
            b = a["b"].div(c(), r["g"]), v = a["b"].button(o(), r["g"], r["f"]), _ = r["j"].extend(s())
    }, 8400: function (t, e, n) {
    }, "85e3": function (t, e) {
        function n(t, e, n) {
            switch (n.length) {
                case 0:
                    return t.call(e);
                case 1:
                    return t.call(e, n[0]);
                case 2:
                    return t.call(e, n[0], n[1]);
                case 3:
                    return t.call(e, n[0], n[1], n[2])
            }
            return t.apply(e, n)
        }

        t.exports = n
    }, 8697: function (t, e, n) {
    }, "89df": function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("2638"), a = n.n(i), r = n("774e"), s = n.n(r), o = n("c082"), c = n("e811"), u = n("55de"),
                l = n("0d8e"), f = (n("4d91d"), n("4c6b"));
            e["a"] = {
                name: "FacebookFeed", mixins: [l["a"]], data: function () {
                    return {channel: "facebook"}
                }, props: {
                    value: {
                        type: Object, default: function () {
                            return {
                                channel: "facebook",
                                creativeType: "photo",
                                creative: {media: "", description: "", title: "", linkDescription: ""}
                            }
                        }
                    }
                }, render: function () {
                    var e = this, n = arguments[0], i = this.editMode, a = this._type, r = void 0 === a ? "" : a,
                        o = this.urlVisible, c = void 0 !== o && o, l = ["onboarding-ad-container", "facebook"];
                    this.editable || l.push("not-editable");
                    var f = t.pathOr("/img/onboarding/fb_profile.png", ["facebookProfile", "logo"], this),
                        d = t.pathOr(this.$t("ONBOARDING.facebook_feed_id"), ["facebookProfile", "name"], this);
                    return n("div", {
                        class: this.classJoin(l),
                        ref: "elem"
                    }, [n("div", {class: "template-facebook-feed-container"}, [n("div", {class: "template-facebook-feed--body"}, [n("div", {
                        class: "top-header",
                        on: {
                            click: function () {
                                return e.$emit("click:profile")
                            }
                        }
                    }, [n("img", {attrs: {src: f}}), n("div", {class: "infos"}, [n("span", {class: "infos--id"}, [d]), n("span", {class: "infos--sponsor"}, ["Sponsored"])])]), this.renderDescription(i), i && n("div", {class: "feed-edit-box"}, [n("button", {
                        class: "adriel-flex-center",
                        on: {
                            click: function () {
                                var t = e.showImageTemplate();
                                t.$on("complete", e.mediaNext("carousel:add", "media").bind(e))
                            }
                        }
                    }, [n("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "plus"
                        }
                    }), this.$t("COMMON.ads_add_slide")])]), this.renderImage(i), n("div", {class: "feed-middle ".concat(i ? "feed-middle--edit-mode" : "")}, [n("div", {class: "feed-middle__left"}, [this.renderTitle(i), this.renderLinkDescription(i), this.renderUrl({
                        editMode: i,
                        type: r,
                        urlVisible: c
                    }), this.renderAppStoreUrl({
                        editMode: i,
                        type: r,
                        urlVisible: c
                    }), this.renderPlayStoreUrl({
                        editMode: i,
                        type: r,
                        urlVisible: c
                    }), this.renderOptionalUrl({
                        editMode: i,
                        type: r,
                        urlVisible: c
                    })]), n("div", {class: "feed-middle__right"}, [n(u["a"], {
                        attrs: {
                            isEditMode: i,
                            options: this.ctaOptions,
                            selected: t.path(["_value", "creative", "callToAction"], this)
                        }, ref: "cta", nativeOn: {
                            click: function (t) {
                                i || (t.stopPropagation(), e.setMode(!0), e.handleDropdownClick())
                            }
                        }, on: {input: this.updateCreative("callToAction").bind(this)}
                    })])]), n("div", {class: "feed-bottom"}, [s()({length: 3}).map(function (t, i) {
                        return n("span", {class: "adriel-flex-center"}, [n("img", {attrs: {src: "/img/onboarding/fb_icon_".concat(i, ".png")}}), e.$t("ONBOARDING.facebook_feed_bottom_".concat(i))])
                    })])])])])
                }, methods: {
                    renderDescription: function (t) {
                        var e = this, n = this.$createElement, i = this.getParamInfo("description"), a = i.value,
                            r = void 0 === a ? "" : a, s = i.error, c = i.reason, u = [];
                        return t ? (u.push("feed-description-input"), n(o["a"], {
                            attrs: {
                                type: "textarea",
                                error: this._dirty && s ? this.$t("COMMON.ad_validation_".concat(c)) : "",
                                value: r,
                                autofocus: this.autofocus,
                                placeholder: this.$t("COMMON.facebook_feed_description_placeholder")
                            }, class: u, on: {input: this.updateCreative("description").bind(this)}, ref: "description"
                        })) : (u.push("feed-description", "hover-edit", "word-break"), u = this.addClassIfEmpty(r, u), n("div", {
                            class: u,
                            on: {
                                click: function () {
                                    return e.setMode(!0, "description")
                                }
                            }
                        }, [this.valueOrPlaceholder(r)]))
                    }, renderLinkDescription: function (t) {
                        var e = this, n = this.$createElement, i = this.getParamInfo("linkDescription"), r = i.value,
                            s = i.error, c = [];
                        return t || s ? (this._dirty && s && c.push("adriel-field-error"), c.push("link-description-input"), n(o["a"], a()([{}, f["n"], {
                            class: c,
                            attrs: {
                                value: r,
                                limit: 90,
                                placeholder: this.$t("COMMON.facebook_feed_link_description_placeholder")
                            },
                            ref: "linkDescription",
                            on: {input: this.updateCreative("linkDescription").bind(this)}
                        }]))) : n("div", {
                            class: "link-description hover-edit", on: {
                                click: function () {
                                    return e.setMode(!0, "linkDescription")
                                }
                            }
                        }, [this.valueOrPlaceholder(r)])
                    }, renderTitle: function (t) {
                        var e = this, n = this.$createElement, i = this.getParamInfo("title"), r = i.value, s = i.error,
                            c = [];
                        return t ? (this._dirty && s && c.push("adriel-field-error"), c.push("feed-title-input"), n(o["a"], a()([{}, f["n"], {
                            class: c,
                            attrs: {value: r, placeholder: this.$t("COMMON.facebook_feed_headline_placeholder")},
                            on: {input: this.updateCreative("title").bind(this)},
                            ref: "title"
                        }]))) : n("div", {
                            class: "feed-title hover-edit", on: {
                                click: function () {
                                    return e.setMode(!0, "title")
                                }
                            }
                        }, [r])
                    }, renderImage: function (e) {
                        var n = this, i = this.$createElement, r = {
                            attrs: {
                                src: t.path(["_value", "creative", "media"], this),
                                meta: t.path(["_value", "creative", "meta", "media"], this),
                                type: "media",
                                editMode: e,
                                campaignId: this.campaignId,
                                isLoading: this.isLoading,
                                error: this._dirty,
                                channel: "facebook"
                            }, on: {
                                click: function () {
                                    return n.setMode(!0)
                                }, complete: this.mediaNext("single", "media").bind(this), remove: function () {
                                    return n.updateCreative("media", void 0)
                                }
                            }
                        };
                        return i(c["a"], a()([{}, r]))
                    }, renderUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "WEB" !== n) return null;
                        var s = [], c = this.getParamInfo("url"), u = c.value, l = c.error, d = c.reason;
                        return this._dirty && l && s.push("adriel-field-error"), r(o["a"], a()([{}, f["n"], {
                            class: s,
                            attrs: {
                                value: u,
                                error: this._dirty && l ? this.$t("COMMON.ad_validation_".concat(d)) : "",
                                placeholder: this.$t("COMMON.ad_landing_url_placeholder")
                            },
                            ref: "url",
                            on: {input: this.updateCreative("url").bind(this)},
                            style: {"margin-top": "10px"}
                        }]))
                    }, renderOptionalUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "APP" !== n) return null;
                        var s = this.getParamInfo("url"), c = s.value;
                        return r(o["a"], a()([{}, f["n"], {
                            attrs: {
                                value: c,
                                placeholder: "".concat(this.$t("COMMON.ad_landing_url_placeholder"), " (").concat(this.$t("COMMON.optional"), ")")
                            },
                            ref: "OptionalUrl",
                            on: {input: this.updateCreative("url").bind(this)},
                            style: {"margin-top": "10px"}
                        }]))
                    }, renderAppStoreUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "APP" !== n) return null;
                        var s = [], c = this.getParamInfo("appStoreUrl"), u = c.value, l = c.error, d = c.reason;
                        return this._dirty && l && s.push("adriel-field-error"), r(o["a"], a()([{}, f["n"], {
                            class: s,
                            attrs: {
                                value: u,
                                error: this._dirty && l ? this.$t("COMMON.ad_validation_".concat(d)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_appstore_url")
                            },
                            ref: "appStoreUrl",
                            on: {input: this.updateCreative("appStoreUrl").bind(this)},
                            style: {"margin-top": "10px"}
                        }]))
                    }, renderPlayStoreUrl: function (t) {
                        var e = t.editMode, n = t.type, i = t.urlVisible, r = this.$createElement;
                        if (!i || !e || "APP" !== n) return null;
                        var s = [], c = this.getParamInfo("playStoreUrl"), u = c.value, l = c.error, d = c.reason;
                        return this._dirty && l && s.push("adriel-field-error"), r(o["a"], a()([{}, f["n"], {
                            class: s,
                            attrs: {
                                value: u,
                                error: this._dirty && l ? this.$t("COMMON.ad_validation_".concat(d)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_playstore_url")
                            },
                            ref: "playStoreUrl",
                            on: {input: this.updateCreative("playStoreUrl").bind(this)},
                            style: {"margin-top": "10px"}
                        }]))
                    }
                }
            }
        }).call(this, n("b17e"))
    }, "8beb": function (t, e, n) {
    }, "8db5": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "gender-gauge text-center"}, [n("div", {staticClass: "gender-content"}, [n("div", {staticClass: "gender-value man"}, [t._t("left"), "percentage" === t.type ? [t.isEditing ? n("div", {staticClass: "category-input"}, [n("input", {
                    directives: [{
                        name: "model",
                        rawName: "v-model.number",
                        value: t.leftValue,
                        expression: "leftValue",
                        modifiers: {number: !0}
                    }],
                    ref: "leftInput",
                    attrs: {type: "number"},
                    domProps: {value: t.leftValue},
                    on: {
                        keyup: function (e) {
                            return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.$emit("complete")
                        }, input: function (e) {
                            e.target.composing || (t.leftValue = t._n(e.target.value))
                        }, blur: function (e) {
                            return t.$forceUpdate()
                        }
                    }
                })]) : n("div", {staticClass: "gender-percentage"}, [t._v(t._s(t.leftValue))])] : [n("switch-view", {
                    ref: "switch",
                    staticClass: "gender-switch",
                    attrs: {value: t.value[0]},
                    on: {
                        "update:value": function (e) {
                            return t.switchValueChanged(0, e)
                        }
                    },
                    nativeOn: {
                        click: function (t) {
                            t.stopPropagation()
                        }
                    }
                })]], 2), !1 !== t.hideGauge ? n("div", {attrs: {id: "power-gauge"}}) : t._e(), n("div", {staticClass: "gender-value woman"}, [t._t("right"), "percentage" === t.type ? [t.isEditing ? n("div", {staticClass: "category-input"}, [n("input", {
                    directives: [{
                        name: "model",
                        rawName: "v-model.number",
                        value: t.rightValue,
                        expression: "rightValue",
                        modifiers: {number: !0}
                    }],
                    ref: "rightInput",
                    attrs: {type: "number"},
                    domProps: {value: t.rightValue},
                    on: {
                        keyup: function (e) {
                            return !e.type.indexOf("key") && t._k(e.keyCode, "enter", 13, e.key, "Enter") ? null : t.$emit("complete")
                        }, input: function (e) {
                            e.target.composing || (t.rightValue = t._n(e.target.value))
                        }, blur: function (e) {
                            return t.$forceUpdate()
                        }
                    }
                })]) : n("div", {staticClass: "gender-percentage"}, [t._v(t._s(t.rightValue))])] : [n("switch-view", {
                    ref: "switch",
                    staticClass: "gender-switch",
                    attrs: {value: t.value[1]},
                    on: {
                        "update:value": function (e) {
                            return t.switchValueChanged(1, e)
                        }
                    },
                    nativeOn: {
                        click: function (t) {
                            t.stopPropagation()
                        }
                    }
                })]], 2)])])
            }, a = [], r = n("e2e1"), s = r["a"], o = (n("d7ef"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, null, null);
        e["a"] = c.exports
    }, "8f6b": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    staticClass: "pixels-modal",
                    class: {howTo: "howTo" === t.status},
                    attrs: {active: t._active, canCancel: ["escape", "x"], "has-modal-card": !0},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }
                    }
                }, [n("confirm-modal", {
                    attrs: {
                        active: t.isWarningModalActive,
                        text: t.$t("PROPOSAL.confirm_pixel_sharing"),
                        "cancel-text": t.$t("COMMON.yes"),
                        "confirm-text": t.$t("PROPOSAL.confirm_pixel_confirm")
                    }, on: {
                        "update:active": function (e) {
                            t.isWarningModalActive = e
                        }, cancel: t.cancel
                    }
                }), n("div", {ref: "wrapper", staticClass: "pixels-modal-wrapper"}, [n("div", {
                    ref: "header",
                    staticClass: "pixels-modal-header"
                }, [t._v("Select Facebook Pixel")]), n("div", {staticClass: "pixels-modal-body"}, ["notAuth" === t.status ? n("div", {staticClass: "notAuth-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("PROPOSAL.select_fb_page_desc")))]), n("img", {
                    attrs: {src: "/img/proposal/ads_fb_login_btn.png"},
                    on: {
                        click: function (e) {
                            return e.stopPropagation(), t.fetchFacebookProfile()
                        }
                    }
                })]) : t._e(), "noPages" === t.status ? n("div", {staticClass: "noPages-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("PROPOSAL.no_pixel_detected")))])]) : t._e(), "pages" === t.status ? n("div", {staticClass: "pages-contents"}, [n("span", {staticClass: "desc-span"}, [t._v(t._s(t.$t("PROPOSAL.select_fb_pixel_ment")))]), n("div", {staticClass: "page-elems-wrapper"}, t._l(t.pages, function (e) {
                    return n("div", {key: e.id, staticClass: "page-elem"}, [n("b-radio", {
                        staticClass: "custom-radio",
                        class: {selected: t.selectedPixel.id === e.id},
                        attrs: {"native-value": e},
                        on: {
                            input: function (n) {
                                return t.radioSelected(e)
                            }
                        },
                        model: {
                            value: t.selectedPixel, callback: function (e) {
                                t.selectedPixel = e
                            }, expression: "selectedPixel"
                        }
                    }, [n("span", {staticClass: "page-elem-box"}, [n("img", {
                        staticClass: "page-elem-img",
                        attrs: {src: e.logo || "/img/proposal/fb_logo_none.png"}
                    }), n("span", {staticClass: "page-name"}, [t._v(t._s(e.name))]), e.websiteUrl ? n("span", {staticClass: "page-url"}, [t._v(t._s("| " + e.websiteUrl))]) : t._e()])])], 1)
                }), 0)]) : t._e(), "howTo" === t.status ? n("div", {staticClass: "pixel-how-to"}, [n("span", {
                    staticClass: "pixel-title",
                    domProps: {innerHTML: t._s(t.$t(t.currentHowTo.title, t.selectedPixel))}
                }), n("div", {staticClass: "pixel-how-to--img-container"}, [t.howToPage > 0 ? n("span", {
                    staticClass: "prev",
                    on: {
                        click: function (e) {
                            t.howToPage--
                        }
                    }
                }, [n("img", {attrs: {src: "/img/proposal/pixel_modal_prev.png"}})]) : t._e(), n("img", {attrs: {src: t.currentHowTo.image}}), t.lastHowTo ? t._e() : n("span", {
                    staticClass: "next",
                    on: {
                        click: function (e) {
                            t.howToPage++
                        }
                    }
                }, [n("img", {attrs: {src: "/img/proposal/pixel_modal_next.png"}})])]), n("div", {staticClass: "pages"}, t._l(t.howTo, function (e, i) {
                    return n("span", {
                        key: i, class: {active: i === t.howToPage}, on: {
                            click: function (e) {
                                t.howToPage = i
                            }
                        }
                    })
                }), 0)]) : t._e(), n("buttons", {
                    staticClass: "pixels-buttons",
                    class: t.status,
                    attrs: {
                        types: t.buttonTypes,
                        "cancel-text": t.buttonTypes.length > 1 ? t.$t("COMMON.cancel") : t.$t("COMMON.close"),
                        "confirm-text": t.confirmText
                    },
                    on: {cancel: t.cancel, confirm: t.confirm}
                })], 1)])], 1)
            }, a = [], r = n("3d5c"), s = r["a"], o = (n("68fe"), n("faa9"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, "6173406a", null);
        e["a"] = c.exports
    }, "8fda": function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "locations"}, [n("card-view", {
                    attrs: {
                        label: t.$t("PROPOSAL.ap_card_title_location"),
                        className: t.className,
                        editable: t.editable,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        settingImgSrc: "/img/proposal/card_add_btn.png",
                        settingLabel: t.$t("PROPOSAL.ap_card_edit_locations"),
                        confirmLabel: "I'd like to change this setting"
                    }, on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [t.values.length ? n("div", {staticClass: "locations-values"}, [n("span", {staticClass: "locations-ment-guide"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_location_info")))]), n("tags", {
                    staticClass: "locations-tags",
                    attrs: {
                        values: t.values, "value-getter": function (t) {
                            return t.text
                        }
                    },
                    on: {
                        "update:remove": function (e) {
                            return t.$emit("update:remove", e)
                        }, click: function (e) {
                            return t.$emit("edit:start", e)
                        }
                    }
                })], 1) : n("div", {staticClass: "locations-no-values"}, [n("no-content", {
                    attrs: {imgUrl: "/img/proposal/profile_no_location.png"},
                    on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.ap_card_location_info_empty")))])], 1)])], 1)
            }, a = [], r = n("3de4"), s = n("2d2d"), o = {
                mixins: [r["a"]], created: function () {
                }, data: function () {
                    return {}
                }, props: {}, computed: {}, components: {tags: s["a"]}
            }, c = o, u = (n("ca6a"), n("4adc"), n("5d16"), n("2877")),
            l = Object(u["a"])(c, i, a, !1, null, "661586b6", null);
        e["a"] = l.exports
    }, "938b": function (t, e, n) {
        "use strict";
        var i = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", [n("card-view", {
                attrs: {
                    label: t.$t("PROPOSAL.ap_card_title_relation"),
                    className: t.className,
                    editable: t.editable,
                    isEditing: t.isEditing,
                    dimmed: t.dimmed,
                    resetable: t.resetable,
                    resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                    confirmLabel: "I'd like to change this setting"
                }, on: {
                    "edit:start": function (e) {
                        return t.$emit("edit:start")
                    }, "edit:end": function (e) {
                        return t.$emit("edit:end")
                    }, "edit:reset": t.reset
                }
            }, [n("on-off-list", {
                attrs: {
                    "name-getter": function (e) {
                        return t.$t("PROPOSAL.ap_card_relation_" + e.name)
                    }, "value-getter": function (t) {
                        return t.value
                    }, values: t.values, images: t.images
                }, on: {
                    "update:value": function (e) {
                        return t.$emit("update:value", e)
                    }
                }
            })], 1)], 1)
        }, a = [], r = n("3de4"), s = n("73ff"), o = {
            mixins: [r["a"]], created: function () {
            }, data: function () {
                return {images: ["/img/proposal/relationship_single.png", "/img/proposal/relationship_relationship.png", "/img/proposal/relationship_married.png"]}
            }, methods: {}, props: {}, components: {"on-off-list": s["a"]}
        }, c = o, u = n("2877"), l = Object(u["a"])(c, i, a, !1, null, null, null);
        e["a"] = l.exports
    }, "97cc": function (t, e, n) {
        "use strict";
        var i = n("eda3"), a = n.n(i);
        a.a
    }, "98d0": function (t, e, n) {
    }, 9915: function (t, e, n) {
    }, "99b8": function (t, e, n) {
        "use strict";
        var i = n("9a08"), a = n.n(i);
        a.a
    }, "9a08": function (t, e, n) {
    }, "9ca3": function (t, e, n) {
    }, "9f19": function (t, e, n) {
    }, a1f6: function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {staticClass: "audience-interests"}, [n("card-view", {
                    attrs: {
                        label: t.$t("PROPOSAL.ap_card_title_interests"),
                        className: t.className,
                        editable: t.editable,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        settingImgSrc: "/img/proposal/card_add_btn.png",
                        confirmLabel: "I'd like to change this setting",
                        settingLabel: t.$t("PROPOSAL.ap_card_edit_interests")
                    }, on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [t.values.length ? n("div", {staticClass: "audience-interests-values"}, [n("span", {staticClass: "audience-interests-ment-guide"}, [t._v(t._s(t.$t("PROPOSAL.ap_card_interests_info")))]), n("tags", {
                    staticClass: "audience-interests-tags",
                    attrs: {
                        values: t.values,
                        "value-getter": t.valueGetter,
                        mainColor: "",
                        "max-width": "100%",
                        deleteImgSrc: "/img/proposal/profile_delete_icon_dark.png"
                    },
                    on: {
                        "update:remove": function (e) {
                            return t.$emit("update:remove", e)
                        }, click: function (e) {
                            return t.$emit("edit:start", e)
                        }
                    }
                })], 1) : n("div", {staticClass: "audience-interests-no-values"}, [n("no-content", {
                    on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                }, [t._v("\n                " + t._s(t.$t("PROPOSAL.ap_card_no_interests")) + "\n                "), n("tags", {
                    staticClass: "audience-interests-tags",
                    attrs: {
                        values: [t.$t("PROPOSAL.ap_card_let_adriel")],
                        removable: !1,
                        "max-width": "300",
                        mainColor: "",
                        deleteImgSrc: "/img/proposal/profile_delete_icon_dark.png"
                    },
                    on: {
                        click: function (e) {
                            return t.$emit("edit:start")
                        }
                    }
                })], 1)], 1)])], 1)
            }, a = [], r = n("cebc"), s = (n("6762"), n("2fdb"), n("7f7f"), n("3de4")), o = n("2d2d"), c = n("2f62"), u = {
                mixins: [s["a"]],
                created: function () {
                },
                data: function () {
                    return {}
                },
                methods: {
                    valueGetter: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, e = t.name || t.title;
                        return t && ["interests", "behaviors", "family_statuses"].includes(t.type) && (e = this.$t("PROPOSAL.".concat(t.type)) + ": " + e), this.isAdmin && t.channel && (e += " (" + t.channel + ")"), e
                    }
                },
                props: {},
                computed: Object(r["a"])({}, Object(c["mapGetters"])("user", ["isAdmin"])),
                components: {tags: o["a"]}
            }, l = u, f = (n("4ace"), n("4460"), n("0e88"), n("2877")),
            d = Object(f["a"])(l, i, a, !1, null, "612d1f06", null);
        e["a"] = d.exports
    }, a454: function (t, e, n) {
        var i = n("72f0"), a = n("3b4a"), r = n("cd9d"), s = a ? function (t, e) {
            return a(t, "toString", {configurable: !0, enumerable: !1, value: i(e), writable: !0})
        } : r;
        t.exports = s
    }, a501: function (t, e, n) {
        "use strict";
        var i = n("98d0"), a = n.n(i);
        a.a
    }, a5f84: function (t, e, n) {
    }, aaf7: function (t, e, n) {
    }, ad27: function (t, e, n) {
        "use strict";
        var i = function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "gender-gauge-wrapper"}, [n("card-view", {
                attrs: {
                    label: t.$t("PROPOSAL.ap_card_title_gender"),
                    className: t.className,
                    editable: t.editable,
                    isEditing: t.isEditing,
                    dimmed: t.dimmed,
                    resetable: t.resetable,
                    resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                    confirmLabel: "I'd like to change this setting"
                }, on: {
                    "edit:start": function (e) {
                        return t.$emit("edit:start")
                    }, "edit:end": function (e) {
                        return t.$emit("edit:end")
                    }, "edit:reset": t.reset
                }
            }, [n("gender-gauge-view", {
                attrs: {
                    isEditing: t.isEditing,
                    value: t._values,
                    colors: ["#2b80ba", "#ef646f"],
                    type: "toggle",
                    hideGauge: !0
                }, on: {"update:value": t.updateValue}
            }, [n("div", {
                staticClass: "gender-gauge-slot",
                attrs: {slot: "left"},
                slot: "left"
            }, [n("span", {staticClass: "gender-gauge-title"}, [n("span", [t._v(t._s(t.$t("COMMON.gender_men")))])]), n("img", {attrs: {src: "/img/icons/male_icon.png"}})]), n("div", {
                staticClass: "gender-gauge-slot",
                attrs: {slot: "right"},
                slot: "right"
            }, [n("span", {staticClass: "gender-gauge-title"}, [n("span", [t._v(t._s(t.$t("COMMON.gender_women")))])]), n("img", {attrs: {src: "/img/icons/female_icon.png"}})])])], 1)], 1)
        }, a = [], r = (n("ac6a"), n("768b")), s = (n("2f62"), n("3de4")), o = n("8db5"), c = {
            created: function () {
            }, computed: {
                _values: function () {
                    var t = Object(r["a"])(this.values, 2), e = t[0], n = t[1];
                    return [e.value, n.value]
                }
            }, mixins: [s["a"]], components: {"gender-gauge-view": o["a"]}
        }, u = c, l = (n("e22a"), n("2877")), f = Object(l["a"])(u, i, a, !1, null, null, null);
        e["a"] = f.exports
    }, ada6: function (t, e, n) {
        "use strict";
        (function (t) {
            n("6762"), n("2fdb"), n("c5f6"), n("28a5"), n("2f62");
            var i = n("6743"), a = n("d0ae"), r = {
                google: "Various websites",
                facebook: "facebook",
                instagram: "instagram",
                linkedin: "LinkedIn",
                googlesearch: "Google Search",
                googleSearch: "Google Search",
                twitter: "Twitter"
            }, s = t.compose(t.join(""), t.map(t.toLower), t.split(" "), t.trim);
            e["a"] = {
                components: {"tooltip-google": i["a"], "tooltip-google-search": a["a"]},
                data: function () {
                    return {isEditing: !1, inputText: this.value}
                },
                props: {
                    value: [Number, String],
                    deletable: {type: Boolean, default: !0},
                    editable: {type: Boolean, default: !0},
                    specials: {type: Array, default: t.always([])},
                    text: {type: String}
                },
                filters: {
                    special: function (e, n) {
                        var i = t.contains(t.__, (n || []).map(t.toLower));
                        return t.ifElse(t.compose(i, s), t.prop(t.__, r), t.identity)(String(e))
                    }
                },
                computed: {
                    additionalClass: function () {
                        var e = t.contains(t.__, this.specials.map(t.toLower));
                        return t.ifElse(t.compose(e, s), t.compose(t.append(t.__, ["edit-input-special"]), t.concat("edit-input-special-")), t.always(""))(String(this.value))
                    }
                },
                methods: {
                    handleDelete: function () {
                        this.deletable && this.$emit("click:delete")
                    }, handleEdit: function () {
                        this.editable && (this.isEditing = !0, this.inputText = this.value, this.$refs.input && this.$refs.input.focus())
                    }, handleBlur: function () {
                        this.$emit("edit:complete", this.inputText.trim() || this.value), this.isEditing = !1
                    }, getTooltip: function (t) {
                        if (this.specials.includes(t)) return {
                            google: "tooltip-google",
                            googleSearch: "tooltip-google-search"
                        }[t]
                    }
                }
            }
        }).call(this, n("b17e"))
    }, b024: function (t, e, n) {
    }, b041: function (t, e, n) {
        "use strict";
        var i = n("cbc1"), a = n.n(i);
        a.a
    }, b151: function (t, e, n) {
        "use strict";
        var i = n("5024"), a = n.n(i);
        a.a
    }, b22b: function (t, e, n) {
        "use strict";
        var i = n("0bba"), a = n.n(i);
        a.a
    }, b311: function (t, e, n) {
        /*!
         * clipboard.js v2.0.4
         * https://zenorocha.github.io/clipboard.js
         *
         * Licensed MIT Â© Zeno Rocha
         */
        (function (e, n) {
            t.exports = n()
        })(0, function () {
            return function (t) {
                var e = {};

                function n(i) {
                    if (e[i]) return e[i].exports;
                    var a = e[i] = {i: i, l: !1, exports: {}};
                    return t[i].call(a.exports, a, a.exports, n), a.l = !0, a.exports
                }

                return n.m = t, n.c = e, n.d = function (t, e, i) {
                    n.o(t, e) || Object.defineProperty(t, e, {enumerable: !0, get: i})
                }, n.r = function (t) {
                    "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(t, "__esModule", {value: !0})
                }, n.t = function (t, e) {
                    if (1 & e && (t = n(t)), 8 & e) return t;
                    if (4 & e && "object" === typeof t && t && t.__esModule) return t;
                    var i = Object.create(null);
                    if (n.r(i), Object.defineProperty(i, "default", {
                            enumerable: !0,
                            value: t
                        }), 2 & e && "string" != typeof t) for (var a in t) n.d(i, a, function (e) {
                        return t[e]
                    }.bind(null, a));
                    return i
                }, n.n = function (t) {
                    var e = t && t.__esModule ? function () {
                        return t["default"]
                    } : function () {
                        return t
                    };
                    return n.d(e, "a", e), e
                }, n.o = function (t, e) {
                    return Object.prototype.hasOwnProperty.call(t, e)
                }, n.p = "", n(n.s = 0)
            }([function (t, e, n) {
                "use strict";
                var i = "function" === typeof Symbol && "symbol" === typeof Symbol.iterator ? function (t) {
                    return typeof t
                } : function (t) {
                    return t && "function" === typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
                }, a = function () {
                    function t(t, e) {
                        for (var n = 0; n < e.length; n++) {
                            var i = e[n];
                            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
                        }
                    }

                    return function (e, n, i) {
                        return n && t(e.prototype, n), i && t(e, i), e
                    }
                }(), r = n(1), s = f(r), o = n(3), c = f(o), u = n(4), l = f(u);

                function f(t) {
                    return t && t.__esModule ? t : {default: t}
                }

                function d(t, e) {
                    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
                }

                function h(t, e) {
                    if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return !e || "object" !== typeof e && "function" !== typeof e ? t : e
                }

                function p(t, e) {
                    if ("function" !== typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
                    t.prototype = Object.create(e && e.prototype, {
                        constructor: {
                            value: t,
                            enumerable: !1,
                            writable: !0,
                            configurable: !0
                        }
                    }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
                }

                var b = function (t) {
                    function e(t, n) {
                        d(this, e);
                        var i = h(this, (e.__proto__ || Object.getPrototypeOf(e)).call(this));
                        return i.resolveOptions(n), i.listenClick(t), i
                    }

                    return p(e, t), a(e, [{
                        key: "resolveOptions", value: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            this.action = "function" === typeof t.action ? t.action : this.defaultAction, this.target = "function" === typeof t.target ? t.target : this.defaultTarget, this.text = "function" === typeof t.text ? t.text : this.defaultText, this.container = "object" === i(t.container) ? t.container : document.body
                        }
                    }, {
                        key: "listenClick", value: function (t) {
                            var e = this;
                            this.listener = (0, l.default)(t, "click", function (t) {
                                return e.onClick(t)
                            })
                        }
                    }, {
                        key: "onClick", value: function (t) {
                            var e = t.delegateTarget || t.currentTarget;
                            this.clipboardAction && (this.clipboardAction = null), this.clipboardAction = new s.default({
                                action: this.action(e),
                                target: this.target(e),
                                text: this.text(e),
                                container: this.container,
                                trigger: e,
                                emitter: this
                            })
                        }
                    }, {
                        key: "defaultAction", value: function (t) {
                            return v("action", t)
                        }
                    }, {
                        key: "defaultTarget", value: function (t) {
                            var e = v("target", t);
                            if (e) return document.querySelector(e)
                        }
                    }, {
                        key: "defaultText", value: function (t) {
                            return v("text", t)
                        }
                    }, {
                        key: "destroy", value: function () {
                            this.listener.destroy(), this.clipboardAction && (this.clipboardAction.destroy(), this.clipboardAction = null)
                        }
                    }], [{
                        key: "isSupported", value: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : ["copy", "cut"],
                                e = "string" === typeof t ? [t] : t, n = !!document.queryCommandSupported;
                            return e.forEach(function (t) {
                                n = n && !!document.queryCommandSupported(t)
                            }), n
                        }
                    }]), e
                }(c.default);

                function v(t, e) {
                    var n = "data-clipboard-" + t;
                    if (e.hasAttribute(n)) return e.getAttribute(n)
                }

                t.exports = b
            }, function (t, e, n) {
                "use strict";
                var i = "function" === typeof Symbol && "symbol" === typeof Symbol.iterator ? function (t) {
                    return typeof t
                } : function (t) {
                    return t && "function" === typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
                }, a = function () {
                    function t(t, e) {
                        for (var n = 0; n < e.length; n++) {
                            var i = e[n];
                            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
                        }
                    }

                    return function (e, n, i) {
                        return n && t(e.prototype, n), i && t(e, i), e
                    }
                }(), r = n(2), s = o(r);

                function o(t) {
                    return t && t.__esModule ? t : {default: t}
                }

                function c(t, e) {
                    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
                }

                var u = function () {
                    function t(e) {
                        c(this, t), this.resolveOptions(e), this.initSelection()
                    }

                    return a(t, [{
                        key: "resolveOptions", value: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            this.action = t.action, this.container = t.container, this.emitter = t.emitter, this.target = t.target, this.text = t.text, this.trigger = t.trigger, this.selectedText = ""
                        }
                    }, {
                        key: "initSelection", value: function () {
                            this.text ? this.selectFake() : this.target && this.selectTarget()
                        }
                    }, {
                        key: "selectFake", value: function () {
                            var t = this, e = "rtl" == document.documentElement.getAttribute("dir");
                            this.removeFake(), this.fakeHandlerCallback = function () {
                                return t.removeFake()
                            }, this.fakeHandler = this.container.addEventListener("click", this.fakeHandlerCallback) || !0, this.fakeElem = document.createElement("textarea"), this.fakeElem.style.fontSize = "12pt", this.fakeElem.style.border = "0", this.fakeElem.style.padding = "0", this.fakeElem.style.margin = "0", this.fakeElem.style.position = "absolute", this.fakeElem.style[e ? "right" : "left"] = "-9999px";
                            var n = window.pageYOffset || document.documentElement.scrollTop;
                            this.fakeElem.style.top = n + "px", this.fakeElem.setAttribute("readonly", ""), this.fakeElem.value = this.text, this.container.appendChild(this.fakeElem), this.selectedText = (0, s.default)(this.fakeElem), this.copyText()
                        }
                    }, {
                        key: "removeFake", value: function () {
                            this.fakeHandler && (this.container.removeEventListener("click", this.fakeHandlerCallback), this.fakeHandler = null, this.fakeHandlerCallback = null), this.fakeElem && (this.container.removeChild(this.fakeElem), this.fakeElem = null)
                        }
                    }, {
                        key: "selectTarget", value: function () {
                            this.selectedText = (0, s.default)(this.target), this.copyText()
                        }
                    }, {
                        key: "copyText", value: function () {
                            var t = void 0;
                            try {
                                t = document.execCommand(this.action)
                            } catch (e) {
                                t = !1
                            }
                            this.handleResult(t)
                        }
                    }, {
                        key: "handleResult", value: function (t) {
                            this.emitter.emit(t ? "success" : "error", {
                                action: this.action,
                                text: this.selectedText,
                                trigger: this.trigger,
                                clearSelection: this.clearSelection.bind(this)
                            })
                        }
                    }, {
                        key: "clearSelection", value: function () {
                            this.trigger && this.trigger.focus(), window.getSelection().removeAllRanges()
                        }
                    }, {
                        key: "destroy", value: function () {
                            this.removeFake()
                        }
                    }, {
                        key: "action", set: function () {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "copy";
                            if (this._action = t, "copy" !== this._action && "cut" !== this._action) throw new Error('Invalid "action" value, use either "copy" or "cut"')
                        }, get: function () {
                            return this._action
                        }
                    }, {
                        key: "target", set: function (t) {
                            if (void 0 !== t) {
                                if (!t || "object" !== ("undefined" === typeof t ? "undefined" : i(t)) || 1 !== t.nodeType) throw new Error('Invalid "target" value, use a valid Element');
                                if ("copy" === this.action && t.hasAttribute("disabled")) throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');
                                if ("cut" === this.action && (t.hasAttribute("readonly") || t.hasAttribute("disabled"))) throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');
                                this._target = t
                            }
                        }, get: function () {
                            return this._target
                        }
                    }]), t
                }();
                t.exports = u
            }, function (t, e) {
                function n(t) {
                    var e;
                    if ("SELECT" === t.nodeName) t.focus(), e = t.value; else if ("INPUT" === t.nodeName || "TEXTAREA" === t.nodeName) {
                        var n = t.hasAttribute("readonly");
                        n || t.setAttribute("readonly", ""), t.select(), t.setSelectionRange(0, t.value.length), n || t.removeAttribute("readonly"), e = t.value
                    } else {
                        t.hasAttribute("contenteditable") && t.focus();
                        var i = window.getSelection(), a = document.createRange();
                        a.selectNodeContents(t), i.removeAllRanges(), i.addRange(a), e = i.toString()
                    }
                    return e
                }

                t.exports = n
            }, function (t, e) {
                function n() {
                }

                n.prototype = {
                    on: function (t, e, n) {
                        var i = this.e || (this.e = {});
                        return (i[t] || (i[t] = [])).push({fn: e, ctx: n}), this
                    }, once: function (t, e, n) {
                        var i = this;

                        function a() {
                            i.off(t, a), e.apply(n, arguments)
                        }

                        return a._ = e, this.on(t, a, n)
                    }, emit: function (t) {
                        var e = [].slice.call(arguments, 1), n = ((this.e || (this.e = {}))[t] || []).slice(), i = 0,
                            a = n.length;
                        for (i; i < a; i++) n[i].fn.apply(n[i].ctx, e);
                        return this
                    }, off: function (t, e) {
                        var n = this.e || (this.e = {}), i = n[t], a = [];
                        if (i && e) for (var r = 0, s = i.length; r < s; r++) i[r].fn !== e && i[r].fn._ !== e && a.push(i[r]);
                        return a.length ? n[t] = a : delete n[t], this
                    }
                }, t.exports = n
            }, function (t, e, n) {
                var i = n(5), a = n(6);

                function r(t, e, n) {
                    if (!t && !e && !n) throw new Error("Missing required arguments");
                    if (!i.string(e)) throw new TypeError("Second argument must be a String");
                    if (!i.fn(n)) throw new TypeError("Third argument must be a Function");
                    if (i.node(t)) return s(t, e, n);
                    if (i.nodeList(t)) return o(t, e, n);
                    if (i.string(t)) return c(t, e, n);
                    throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")
                }

                function s(t, e, n) {
                    return t.addEventListener(e, n), {
                        destroy: function () {
                            t.removeEventListener(e, n)
                        }
                    }
                }

                function o(t, e, n) {
                    return Array.prototype.forEach.call(t, function (t) {
                        t.addEventListener(e, n)
                    }), {
                        destroy: function () {
                            Array.prototype.forEach.call(t, function (t) {
                                t.removeEventListener(e, n)
                            })
                        }
                    }
                }

                function c(t, e, n) {
                    return a(document.body, t, e, n)
                }

                t.exports = r
            }, function (t, e) {
                e.node = function (t) {
                    return void 0 !== t && t instanceof HTMLElement && 1 === t.nodeType
                }, e.nodeList = function (t) {
                    var n = Object.prototype.toString.call(t);
                    return void 0 !== t && ("[object NodeList]" === n || "[object HTMLCollection]" === n) && "length" in t && (0 === t.length || e.node(t[0]))
                }, e.string = function (t) {
                    return "string" === typeof t || t instanceof String
                }, e.fn = function (t) {
                    var e = Object.prototype.toString.call(t);
                    return "[object Function]" === e
                }
            }, function (t, e, n) {
                var i = n(7);

                function a(t, e, n, i, a) {
                    var r = s.apply(this, arguments);
                    return t.addEventListener(n, r, a), {
                        destroy: function () {
                            t.removeEventListener(n, r, a)
                        }
                    }
                }

                function r(t, e, n, i, r) {
                    return "function" === typeof t.addEventListener ? a.apply(null, arguments) : "function" === typeof n ? a.bind(null, document).apply(null, arguments) : ("string" === typeof t && (t = document.querySelectorAll(t)), Array.prototype.map.call(t, function (t) {
                        return a(t, e, n, i, r)
                    }))
                }

                function s(t, e, n, a) {
                    return function (n) {
                        n.delegateTarget = i(n.target, e), n.delegateTarget && a.call(t, n)
                    }
                }

                t.exports = r
            }, function (t, e) {
                var n = 9;
                if ("undefined" !== typeof Element && !Element.prototype.matches) {
                    var i = Element.prototype;
                    i.matches = i.matchesSelector || i.mozMatchesSelector || i.msMatchesSelector || i.oMatchesSelector || i.webkitMatchesSelector
                }

                function a(t, e) {
                    while (t && t.nodeType !== n) {
                        if ("function" === typeof t.matches && t.matches(e)) return t;
                        t = t.parentNode
                    }
                }

                t.exports = a
            }])
        })
    }, ba09: function (t, e, n) {
        "use strict";
        var i = n("4d96"), a = n.n(i);
        a.a
    }, baba: function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", {class: [t.className, {"no-contents": !t.values.length}]}, [t.values.length ? n("div", {staticClass: "journey-editable-wrapper"}, t._l(t.values, function (e, i) {
                    return n("edit-input", {
                        key: t.valueGetter(e),
                        ref: "input",
                        refInFor: !0,
                        attrs: {value: t.nameGetter(e), editable: t.editable, deletable: t.deletable, specials: t.specials},
                        on: {
                            "click:delete": function (e) {
                                return t.handleDelete(i)
                            }, "edit:complete": function (e) {
                                return t.handleComplete(e, i)
                            }
                        }
                    })
                }), 1) : t._e(), t.btnSrc && t.values.length ? n("button", {
                    staticClass: "journey-editable-button",
                    class: {"btn-disabled": t.btnDisabled}
                }, [t.btnSrc ? n("img", {
                    attrs: {src: t.btnSrc}, on: {
                        click: function (e) {
                            return t.$emit("click:add")
                        }
                    }
                }) : t._e()]) : t._e(), t.emptyText && !t.values.length ? n("button", {
                    staticClass: "journey-editable-no-content-button",
                    domProps: {textContent: t._s(t.emptyText)},
                    on: {
                        click: function (e) {
                            return t.$emit("click:add")
                        }
                    }
                }) : t._e()])
            }, a = [], r = n("2d26"), s = r["a"], o = (n("72bf"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, "4a81e3e6", null);
        e["a"] = c.exports
    }, bc0c: function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return a
        });
        var i = n("e9b9");

        function a(t, e, n) {
            return void 0 === t && (t = 0), new i["a"](function (i) {
                void 0 === e && (e = t, t = 0);
                var a = 0, s = t;
                if (n) return n.schedule(r, 0, {index: a, count: e, start: t, subscriber: i});
                do {
                    if (a++ >= e) {
                        i.complete();
                        break
                    }
                    if (i.next(s++), i.closed) break
                } while (1)
            })
        }

        function r(t) {
            var e = t.start, n = t.index, i = t.count, a = t.subscriber;
            n >= i ? a.complete() : (a.next(e), a.closed || (t.index = n + 1, t.start = e + 1, this.schedule(t)))
        }
    }, be6e: function (t, e, n) {
        "use strict";
        (function (t) {
            n("c5f6"), n("b311"), n("2f62");
            var i = n("dde5"), a = n("baba"), r = n("12e7"), s = n("6510");
            n("fa7d");
            e["a"] = {
                name: "AppstoreModal",
                props: {
                    active: {type: Boolean, default: !1},
                    campaignId: {type: [Number, String], required: !0},
                    id: {type: [Number, String], required: !0}
                },
                data: function () {
                    return {howToPage: 0, isWarningModalActive: !1}
                },
                computed: {
                    _active: {
                        get: t.prop("active"), set: function (t) {
                            t || this.$emit("cancel"), this.$emit("update:active", t)
                        }
                    }, lastHowTo: function () {
                        return this.howToPage === t.dec(this.howTo.length)
                    }, buttonTypes: function () {
                        return ["cancel", "confirm"]
                    }, howTo: function () {
                        return this.$t("PROPOSAL.connect_appstore_steps")
                    }, currentHowTo: function () {
                        return this.howTo[this.howToPage]
                    }, confirmText: function () {
                        return this.howToPage === this.howTo.length - 1 ? this.$t("COMMON.done") : this.$t("COMMON.next")
                    }
                },
                components: {"journey-editable-box": a["a"], "journey-edit-input": r["a"], buttons: s["a"]},
                methods: {
                    cancel: function () {
                        this.$emit("cancel"), this._active = !1
                    }, confirm: function () {
                        var t = this;
                        if (this.howToPage === this.howTo.length - 1) return this.checkAdded().then(function () {
                            t.$emit("confirm", t.id), t._active = !1
                        }, function () {
                            return t.isWarningModalActive = !0
                        });
                        this.howToPage++
                    }, checkAdded: function () {
                        return i["d"].checkAppstoreConnected(this.campaignId, this.id)
                    }
                }
            }
        }).call(this, n("b17e"))
    }, c124: function (t, e, n) {
        "use strict";
        (function (t) {
            n("7f7f");
            var i = n("3de4"), a = n("2c47");
            e["a"] = {
                mixins: [i["a"]],
                created: function () {
                },
                data: function () {
                    return {images: ["/img/proposal/proposal_age_icon1.png", "/img/proposal/proposal_age_icon2.png", "/img/proposal/proposal_age_icon3.png", "/img/proposal/proposal_age_icon4.png", "/img/proposal/proposal_age_icon5.png"]}
                },
                props: {},
                components: {"on-off-view": a["a"]},
                computed: {_values: t.prop("values")},
                methods: {
                    nameGetter: function (t) {
                        var e = t.name;
                        return "".concat(e.min, " - ").concat(e.max)
                    }, valueGetter: t.prop("value")
                }
            }
        }).call(this, n("b17e"))
    }, c1c9: function (t, e, n) {
        var i = n("a454"), a = n("f3c1"), r = a(i);
        t.exports = r
    }, c2e3: function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("div", [n("card-view", {
                    attrs: {
                        label: t.$t("PROPOSAL.ap_card_title_age"),
                        className: t.className,
                        editable: t.editable,
                        isEditing: t.isEditing,
                        dimmed: t.dimmed,
                        resetable: t.resetable,
                        resetLabel: t.$t("PROPOSAL.ap_card_edit_reset"),
                        confirmLabel: "I'd like to change this setting"
                    }, on: {
                        "edit:start": function (e) {
                            return t.$emit("edit:start")
                        }, "edit:end": function (e) {
                            return t.$emit("edit:end")
                        }, "edit:reset": t.reset
                    }
                }, [n("div", {staticClass: "age-card-body age-on-off"}, [n("on-off-view", {
                    attrs: {
                        editable: t.editable,
                        isEditing: t.isEditing,
                        values: t._values,
                        images: t.images,
                        "name-getter": t.nameGetter,
                        "value-getter": t.valueGetter
                    }, on: {
                        "update:value": function (e) {
                            return t.$emit("update:value", e)
                        }
                    }
                })], 1)])], 1)
            }, a = [], r = n("c124"), s = r["a"], o = (n("4a44"), n("3763"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, null, null);
        e["a"] = c.exports
    }, c4ca: function (t, e, n) {
        "use strict";
        var i = n("4830"), a = n.n(i);
        a.a
    }, c4cd: function (t, e, n) {
        "use strict";
        var i = n("9f19"), a = n.n(i);
        a.a
    }, c610: function (t, e, n) {
        "use strict";
        var i = n("6c82"), a = n.n(i);
        a.a
    }, c655: function (t, e, n) {
    }, c771: function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("31a2");
            e["a"] = {
                created: function () {
                },
                props: {
                    isEditing: {type: Boolean, default: !1},
                    values: {type: Array, default: t.always([])},
                    images: {
                        type: Array, default: function () {
                            return []
                        }
                    },
                    sizeBig: {
                        type: Object, default: function () {
                            return {width: 52, height: 50}
                        }
                    },
                    sizeNormal: {
                        type: Object, default: function () {
                            return {width: 42, height: 40}
                        }
                    },
                    nameGetter: {type: Function, default: t.identity},
                    valueGetter: {type: Function, default: t.identity},
                    getImgSize: {type: Function, default: void 0}
                },
                methods: {
                    switchValueChanged: function (t, e) {
                        this.$emit("update:value", {idx: t, value: e})
                    }, _getImgSize: function (e) {
                        var n;
                        return n = t.path(["values", e, "value"], this) ? this.sizeBig : this.sizeNormal, {
                            width: "".concat(n.width, "px"),
                            height: "".concat(n.height, "px")
                        }
                    }, size: function (t) {
                        return (this.getImgSize || this._getImgSize)(t)
                    }
                },
                components: {"switch-view": i["a"]}
            }
        }).call(this, n("b17e"))
    }, c82c: function (t, e, n) {
        "use strict";
        var i = n("4daa"), a = n.n(i);
        a.a
    }, ca6a: function (t, e, n) {
        "use strict";
        var i = n("8400"), a = n.n(i);
        a.a
    }, cbc1: function (t, e, n) {
    }, cd9d: function (t, e) {
        function n(t) {
            return t
        }

        t.exports = n
    }, d0ae: function (t, e, n) {
        "use strict";
        var i = function () {
            var t = this, e = t.$createElement;
            t._self._c;
            return t._m(0)
        }, a = [function () {
            var t = this, e = t.$createElement, n = t._self._c || e;
            return n("div", {staticClass: "tooltip-google-wrapper"}, [n("img", {attrs: {src: "/img/proposal/search_ads_ex.png"}}), n("span", [t._v("\n        Your ads will show up in Google search engine results for keywords your audience search for.\n    ")])])
        }], r = (n("3242"), n("2877")), s = {}, o = Object(r["a"])(s, i, a, !1, null, "5c322dd0", null);
        e["a"] = o.exports
    }, d349: function (t, e, n) {
    }, d5c6: function (t, e, n) {
        "use strict";
        var i = n("a5f84"), a = n.n(i);
        a.a
    }, d6ac: function (t, e, n) {
        "use strict";
        var i = n("1b50"), a = n.n(i);
        a.a
    }, d70f: function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("db0c"), a = n.n(i);
            e["a"] = {
                props: {active: {type: Boolean, default: !1}}, computed: {
                    _active: {
                        get: function () {
                            return this.active
                        }, set: function (t) {
                            this.$emit("update:active", t)
                        }
                    }, contents: function () {
                        var e = this;
                        return t.times(t.inc, 6).map(function (t) {
                            return {
                                title: e.$t("PROPOSAL.billing_faq_title_".concat(t)),
                                content: e.$t("PROPOSAL.billing_faq_content_".concat(t))
                            }
                        }).filter(t.compose(t.all(t.identity), a.a))
                    }
                }
            }
        }).call(this, n("b17e"))
    }, d7ef: function (t, e, n) {
        "use strict";
        var i = n("f252"), a = n.n(i);
        a.a
    }, daa4: function (t, e, n) {
    }, dfd1: function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("2638"), a = n.n(i), r = n("cebc"), s = n("75fc"), o = n("768b"), c = (n("c5f6"), n("774e")),
                u = n.n(c), l = n("c082"), f = n("1980"), d = n.n(f), h = n("e811"), p = n("55de"), b = n("2bd2"),
                v = n("a748"), _ = n("7738"), g = n("6e77"), m = n("bc0c"), y = n("808d"), x = n("1585"), w = n("69dd"),
                O = n("ebb6"), M = n("5670"), C = n("742d"), k = n("a744"), A = n("c4cc"), T = n("4677"), N = n("3e18"),
                P = n("fa7d"), S = n("a772"), $ = n("0d8e"), E = (n("068f"), n("4c6b")), j = t.compose(t.not, t.isNil),
                I = {url: "", meta: "", media: "", title: "", appStoreUrl: "", description: "", playStoreUrl: ""};
            e["a"] = {
                name: "FacebookCarousel",
                mixins: [$["a"]],
                data: function () {
                    return {channel: "facebook", move: 10, navClick$: new b["a"], newCard: []}
                },
                props: {
                    value: {
                        type: Object, default: function () {
                            return {
                                adType: "link",
                                channel: "facebook",
                                enabled: !1,
                                creative: {cards: [t.clone(I), t.clone(I)], mainUrl: "", mainDescription: ""},
                                executable: {value: !1, reason: {type: "invalid", paramName: "url"}},
                                creativeType: "carousel"
                            }
                        }
                    }
                },
                render: function () {
                    var e = this, n = arguments[0], i = this.editMode, a = ["onboarding-ad-container", "facebook"];
                    this.editable || a.push("not-editable");
                    var r = t.pathOr("/img/onboarding/fb_profile.png", ["facebookProfile", "logo"], this),
                        s = t.pathOr(this.$t("ONBOARDING.facebook_feed_id"), ["facebookProfile", "name"], this);
                    return n("div", {
                        class: this.classJoin(a),
                        ref: "elem"
                    }, [n("div", {class: "in-facebook-carousel-container ".concat(i ? "editing" : "")}, [n("div", {class: "carousel--body"}, [n("div", {
                        class: "top-header",
                        on: {
                            click: function () {
                                return e.$emit("click:profile")
                            }
                        }
                    }, [n("img", {attrs: {src: r}}), n("div", {class: "infos"}, [n("span", {class: "infos--id"}, [s]), n("span", {class: "infos--sponsor"}, ["Sponsored"])])]), this.renderDescription(i), this.renderEditBox(i), n("div", {class: "swiper ".concat(this.idxClass)}, [n("button", {
                        class: "prev",
                        on: {
                            click: function () {
                                return e.navClick$.next({value: t.dec(e._cardIndex)})
                            }
                        }
                    }, [n("span", {class: "adriel-flex-center arrow-left"}, [n("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "chevron-left"
                        }
                    })])]), n("button", {
                        class: "next", on: {
                            click: function () {
                                return e.navClick$.next({value: t.inc(e._cardIndex)})
                            }
                        }
                    }, [n("span", {class: "adriel-flex-center arrow-right"}, [n("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "chevron-right"
                        }
                    })])]), n("ul", {class: "swiper__list", style: this.listStyle}, [this.cards.map(function (t, n) {
                        return e.renderCard(i, t, n)
                    })])]), this.renderNavigation(i), i && n("div", {class: "divider-area"}, [n("hr"), this.renderCta()]), this.renderCardContents(i, this._cardIndex, this.cards[this._cardIndex])]), n("div", {class: "carousel--bottom"}, [u()({length: 3}).map(function (t, i) {
                        return n("span", {class: "adriel-flex-center"}, [n("img", {attrs: {src: "/img/onboarding/fb_icon_".concat(i, ".png")}}), e.$t("ONBOARDING.facebook_feed_bottom_".concat(i))])
                    })])])])
                },
                methods: {
                    renderCard: function (t, e, n) {
                        var i = this, r = this.$createElement, s = e.media, o = e.meta;
                        o = void 0 === o ? {} : o;
                        var c = o.media, u = void 0 === c ? {} : c, l = e.title, f = void 0 === l ? " " : l,
                            d = e.description, p = void 0 === d ? " " : d, b = .6 * this.containerWidth + "px", v = b,
                            _ = {
                                attrs: {class: "bottom-headline hover-edit word-break"},
                                directives: [{name: "line-clamp", value: 2}],
                                on: {
                                    click: function () {
                                        return i.setMode(!0, "title")
                                    }
                                }
                            }, g = this.getParamInfo("media"), m = g.error, y = {
                                attrs: {
                                    src: s,
                                    meta: u,
                                    type: "media",
                                    editMode: t,
                                    campaignId: this.campaignId,
                                    isLoading: this.isLoading,
                                    error: this.dirty && m,
                                    channel: "facebook"
                                }, on: {
                                    click: function () {
                                        return i.setMode(!0)
                                    },
                                    complete: this.carouselMediaNext("carousel:edit", "media", n).bind(this),
                                    remove: function () {
                                        return i.updateCard(n, "media", void 0)
                                    }
                                }
                            };
                        return r("li", {
                            class: "swiper__item",
                            style: {width: b}
                        }, [r("div", {class: "product-card"}, [r("div", {
                            class: "product-card__inner",
                            style: {height: v}
                        }, [r("div", {
                            class: "product-card__inner-pic",
                            style: {width: b, height: v}
                        }, [r(h["a"], a()([{}, y])), ";"])]), !t && r("div", {class: "product-card__inner-headline"}, [r("div", a()([{}, _]), [f]), this.renderCta(n)]), !t && r("div", {
                            class: "bottom-desc adriel-ellipsis hover-edit",
                            on: {
                                click: function () {
                                    return i.setMode(!0, "description")
                                }
                            }
                        }, [p, r("br")])])])
                    }, init: function () {
                        var e = this,
                            n = this.$watchAsObservable("_cardIndex", {immediate: !0}).pipe(Object(x["a"])("newValue"), Object(w["a"])(), Object(O["a"])(t.always)),
                            i = this.navClick$.pipe(Object(O["a"])(function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                return t.target ? t.target.getAttribute("data-index") : t.value
                            }), Object(M["a"])(j), Object(O["a"])(t.compose(t.always, Number))),
                            a = Object(v["a"])(n, i).pipe(Object(C["a"])(function (t, e) {
                                return e(t)
                            }, 0), Object(k["a"])(0), Object(w["a"])(), Object(O["a"])(function (n) {
                                return t.clamp(0, e.cards.length - 1, n)
                            }), Object(A["a"])(function (t) {
                                return e._cardIndex = t
                            })),
                            r = this.$watchAsObservable("containerWidth", {immediate: !0}).pipe(Object(x["a"])("newValue")),
                            s = Object(_["a"])(a, r).pipe(Object(O["a"])(function (n) {
                                var i = Object(o["a"])(n, 2), a = i[0], r = i[1], s = .6 * r, c = s + 5;
                                if (a) {
                                    if (a === t.dec(e.length)) return -c * (a - 1) - .36 * s - 12.5;
                                    var u = s - 57 * r / 310;
                                    return -u - (a - 1) * c + 2
                                }
                                return 10
                            }));
                        this.$subscribeTo(s, function (t) {
                            return e.move = t
                        }, function (t) {
                            return console.log(t)
                        })
                    }, renderDescription: function (t) {
                        var e = this, n = this.$createElement, i = this.getParamInfo("mainDescription"), a = i.value,
                            r = void 0 === a ? "" : a, s = i.error, o = i.reason;
                        if (t) return n(l["a"], {
                            attrs: {
                                type: "textarea",
                                value: r,
                                error: this._dirty && s ? this.$t("COMMON.ad_validation_".concat(o)) : "",
                                autofocus: this.autofocus,
                                placeholder: this.$t("COMMON.facebook_feed_description_placeholder")
                            },
                            class: "carousel--description-input",
                            ref: "mainDescription",
                            on: {input: this.updateCreative("mainDescription").bind(this)}
                        });
                        var c = ["carousel--description hover-edit word-break"];
                        return c = this.addClassIfEmpty(r, c), n("div", {
                            class: c, on: {
                                click: function () {
                                    return e.setMode(!0, "mainDescription")
                                }
                            }
                        }, [this.valueOrPlaceholder(r)])
                    }, renderEditBox: function (t) {
                        var e = this, n = this.$createElement;
                        if (t) {
                            var i = this._cardIndex;
                            return n("div", {class: "carousel--edit-area"}, [n("button", {
                                class: "adriel-flex-center remove-btn",
                                on: {
                                    click: function () {
                                        return e.deleteCard(i)
                                    }
                                }
                            }, [this.$t("COMMON.ads_delete_slide")]), this.cards.length < 10 && n("button", {
                                class: "adriel-flex-center add-btn",
                                on: {
                                    click: function () {
                                        var t = e.showImageTemplate();
                                        t.$on("complete", e.mediaNext("carousel:add", "media").bind(e))
                                    }
                                }
                            }, [n("b-icon", {attrs: {pack: "far", icon: "plus"}}), this.$t("COMMON.ads_add_slide")])])
                        }
                    }, renderNavigation: function (e) {
                        var n = this, i = this.$createElement;
                        if (e) return i(d.a, {
                            attrs: {
                                list: Object(s["a"])(this.cards),
                                disabled: this.cards.length < 2,
                                animation: "250"
                            },
                            on: {change: this.handleSort},
                            class: "adriel-flex-center carousel--navigation-container",
                            nativeOn: {
                                click: function (t) {
                                    return n.navClick$.next(t)
                                }
                            }
                        }, [this.cards.map(function (e, a) {
                            var r = ["adriel-flex-center", "nav-btn"];
                            return n._cardIndex === a && r.push("selected"), i("button", {
                                class: n.classJoin(r),
                                attrs: {"data-index": a}
                            }, [t.inc(a)])
                        })])
                    }, renderUrlField: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n;
                        if ("WEB" === i) {
                            var r = this.getParamInfo("url", t), s = r.value, o = r.error, c = r.reason,
                                u = ["url-input"];
                            return this._dirty && o && u.push("adriel-field-error"), e(l["a"], a()([{}, E["n"], {
                                class: u,
                                attrs: {
                                    value: s,
                                    error: this._dirty && o ? this.$t("COMMON.ad_validation_".concat(c)) : "",
                                    placeholder: this.$t("COMMON.ad_landing_url_placeholder")
                                },
                                on: {input: this.updateCard(t, "url").bind(this)}
                            }]))
                        }
                    }, renderCardContents: function (t, e) {
                        var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
                            i = this.$createElement;
                        if (t) {
                            var r = n.title, s = void 0 === r ? "" : r, o = n.description, c = void 0 === o ? "" : o;
                            return i("div", {class: "carousel--card-contents-input"}, [i(l["a"], a()([{}, E["n"], {
                                class: "title-input",
                                attrs: {
                                    value: s,
                                    limit: 40,
                                    placeholder: this.$t("COMMON.facebook_feed_headline_placeholder")
                                },
                                ref: "title",
                                on: {input: this.updateCard(e, "title").bind(this)}
                            }])), i(l["a"], a()([{}, E["n"], {
                                class: "description-input",
                                attrs: {
                                    value: c,
                                    placeholder: this.$t("COMMON.facebook_carousel_description_placeholder")
                                },
                                ref: "description",
                                on: {input: this.updateCard(e, "description").bind(this)}
                            }])), this.renderUrlField(e), this.renderAppStoreUrl(e), this.renderPlayStoreUrl(e), this.renderOptionalUrl(e)])
                        }
                    }, deleteCard: function (e) {
                        var n = Object(S["j"])(this.value).removeGroup(Object(r["a"])({}, this.value), e);
                        this._value = n;
                        var i = t.path(["creative", "cards"], n);
                        i && this.navClick$.next({value: t.clamp(0, t.dec(i.length), this._cardIndex)})
                    }, renderCta: function (e) {
                        var n = this, i = this.$createElement;
                        e = void 0 === e ? this._cardIndex : e;
                        var a = t.path(["cards", e, "callToAction"], this);
                        return i(p["a"], {
                            class: "cta-btn",
                            attrs: {isEditMode: this.editMode, options: this.ctaOptions, selected: a},
                            ref: "cta",
                            nativeOn: {
                                click: function (t) {
                                    n.editMode || (t.stopPropagation(), n.setMode(!0), n.handleDropdownClick())
                                }
                            },
                            on: {input: this.updateCard(e, "callToAction").bind(this)}
                        })
                    }, renderAppStoreUrl: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n, r = this.urlVisible;
                        if ("APP" !== i || !r) return null;
                        var s = [], o = this.getParamInfo("appStoreUrl", t), c = o.value, u = o.error, f = o.reason;
                        return this._dirty && u && s.push("adriel-field-error"), e(l["a"], a()([{}, E["n"], {
                            class: s,
                            attrs: {
                                value: c,
                                error: this._dirty && u ? this.$t("COMMON.ad_validation_".concat(f)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_appstore_url")
                            },
                            ref: "appStoreUrl",
                            on: {input: this.updateCard(t, "appStoreUrl").bind(this)}
                        }]))
                    }, renderPlayStoreUrl: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n, r = this.urlVisible;
                        if ("APP" !== i || !r) return null;
                        var s = [], o = this.getParamInfo("playStoreUrl", t), c = o.value, u = o.error, f = o.reason;
                        return this._dirty && u && s.push("adriel-field-error"), e(l["a"], a()([{}, E["n"], {
                            class: s,
                            attrs: {
                                value: c,
                                error: this._dirty && u ? this.$t("COMMON.ad_validation_".concat(f)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_playstore_url")
                            },
                            ref: "playStoreUrl",
                            on: {input: this.updateCard(t, "playStoreUrl").bind(this)}
                        }]))
                    }, renderOptionalUrl: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n, r = this.urlVisible;
                        if ("APP" !== i || !r) return null;
                        var s = this.getParamInfo("url", t), o = s.value;
                        return e(l["a"], a()([{}, E["n"], {
                            attrs: {
                                value: o,
                                placeholder: "".concat(this.$t("COMMON.ad_landing_url_placeholder"), " (").concat(this.$t("COMMON.optional"), ")")
                            }, ref: "OptionalUrl", on: {input: this.updateCard(t, "url").bind(this)}
                        }]))
                    }
                },
                subscriptions: function () {
                    var t = this,
                        e = Object(v["a"])(Object(g["a"])(window, "resize").pipe(Object(T["a"])(200)), Object(m["a"])(0, 5).pipe(Object(N["a"])(function (t) {
                            return Object(y["a"])(250 + 600 * t)
                        }))).pipe(Object(O["a"])(function () {
                            return Object(P["S"])(t.$refs.elem, !0) || 0
                        }));
                    return {containerWidth: e}
                },
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        return t.init()
                    })
                },
                computed: {
                    length: t.path(["cards", "length"]),
                    cards: t.pathOr([], ["value", "creative", "cards"]),
                    listStyle: function () {
                        return {transform: "translateX(".concat(this.move, "px)")}
                    },
                    idxClass: function () {
                        var e = this._cardIndex;
                        return 0 === e ? "first" : e === t.dec(this.length) ? "last" : "normal"
                    }
                }
            }
        }).call(this, n("b17e"))
    }, e22a: function (t, e, n) {
        "use strict";
        var i = n("c655"), a = n.n(i);
        a.a
    }, e25b: function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("e380"), a = n.n(i), r = t.prop("data");

            function s(t) {
                return axios.post("/bannerCreator", t).then(r)
            }

            var o = a()(function (t) {
                return axios.get("/utils/userInterest?q=".concat(t)).then(r)
            }), c = a()(function (t) {
                return axios.get("/utils/suggestions", {params: {website: t}}).then(r)
            });
            e["a"] = {createBanner: s, getInterests: o, getSuggestions: c}
        }).call(this, n("b17e"))
    }, e274: function (t, e, n) {
    }, e2e1: function (t, e, n) {
        "use strict";
        (function (t) {
            n("c5f6");
            var i = n("5698"), a = n("31a2");
            e["a"] = {
                created: function () {
                },
                components: {"switch-view": a["a"]},
                name: "GenderGauge",
                props: {
                    value: {type: [Number, Array], default: 50},
                    isEditing: {type: Boolean, required: !1},
                    colors: {
                        type: Array, default: function () {
                            return ["#2c87c8", "#a483d4"]
                        }
                    },
                    type: {type: String, default: "percentage"},
                    hideGauge: {type: Boolean, default: !1}
                },
                data: function () {
                    return {}
                },
                watch: {
                    value: function (t) {
                        "toggle" === this.type && this.gauge && this.gauge.update(this.leftValue)
                    }, leftValue: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        this.gauge && this.gauge.update(t)
                    }, rightValue: function () {
                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                        this.gauge && this.gauge.update(100 - t)
                    }
                },
                computed: {
                    leftValue: {
                        get: function () {
                            return "toggle" === this.type ? this.value[0] ? this.value[1] ? 50 : 100 : 0 : this.value
                        }, set: function (e) {
                            e = t.compose(t.min(100), t.max(0))(e), this.$refs.leftInput.value = e, this.$emit("update:value", e)
                        }
                    }, rightValue: {
                        get: function () {
                            return 100 - this.leftValue
                        }, set: function (e) {
                            e = t.compose(t.min(100), t.max(0))(e), this.$refs.rightInput.value = e, this.$emit("update:value", 100 - e)
                        }
                    }
                },
                methods: {
                    gauge: function (e, n) {
                        if (!0 === this.hideGauge) return {render: t.identity, update: t.identity};
                        var a = {}, r = {
                                size: 200,
                                clipWidth: 200,
                                clipHeight: 110,
                                ringInset: 0,
                                ringWidth: 20,
                                pointerWidth: 12,
                                pointerTailLength: 3,
                                pointerHeadLengthPercent: .55,
                                minValue: 0,
                                maxValue: 100,
                                minAngle: -90,
                                maxAngle: 90,
                                transitionMs: 750,
                                majorTicks: 2,
                                arcColor: [i["d"]("#2c87c8"), i["d"]("#a483d4")]
                            }, s = void 0, o = void 0, c = void 0, u = void 0, l = void 0, f = void 0, d = void 0,
                            h = void 0, p = void 0;
                        i["c"]();

                        function b(t) {
                            return t * Math.PI / 180
                        }

                        function v(t) {
                            var e = void 0;
                            for (e in t) r[e] = t[e];
                            s = r.maxAngle - r.minAngle, o = r.size / 2, c = Math.round(o * r.pointerHeadLengthPercent), f = i["e"]().range([0, 1]).domain([r.minValue, r.maxValue]), d = [.5, .5], l = i["a"]().innerRadius(o - r.ringWidth - r.ringInset).outerRadius(o - r.ringInset).startAngle(function (t, e) {
                                var n = 0;
                                return e > 0 && (n = d[e - 1]), b(r.minAngle + n * s)
                            }).endAngle(function (t, e) {
                                var n = t * (e + 1);
                                return 1 == e && (n = 1), b(r.minAngle + n * s)
                            })
                        }

                        function _(t) {
                            return "translate(" + o + "," + o + ")"
                        }

                        function g(t) {
                            u = i["f"](e).append("svg:svg").attr("class", "gauge").attr("width", r.clipWidth).attr("height", r.clipHeight);
                            var n = _();
                            t && (d = [t / 100, 1 - t / 100]), p = u.append("g").attr("class", "arc").attr("transform", n).selectAll("path"), p.data(d).enter().append("path").attr("fill", function (t, e) {
                                return r.arcColor[e]
                            }).attr("d", l);
                            var a = [[r.pointerWidth / 2, 0], [0, -c], [-r.pointerWidth / 2, 0], [0, r.pointerTailLength], [r.pointerWidth / 2, 0]],
                                s = i["b"](),
                                f = u.append("g").data([a]).attr("class", "pointer").style("fill", "#666").attr("transform", "translate(".concat(o, ",").concat(o, ")"));
                            h = f.append("path").attr("d", s).attr("transform", "rotate(0)"), m(void 0 === t ? 0 : t)
                        }

                        function m() {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0, e = f(t),
                                n = r.minAngle + e * s;
                            h.transition().duration(r.transitionMs).attr("transform", "rotate(" + n + ")"), d = [t / 100, 1 - t / 100];
                            var i = _();
                            p.exit().remove(), p = u.append("g").attr("class", "arc").attr("transform", i), p = p.selectAll("path").data(d).enter().append("path").attr("fill", function (t, e) {
                                return r.arcColor[e]
                            }).attr("d", l)
                        }

                        return a.configure = v, a.render = g, a.update = m, v(n), a
                    }, switchValueChanged: function (e, n) {
                        var i = t.update(e, n, this.value);
                        t.all(t.equals(!1), i) && (i[1 - e] = !0), this.$emit("update:value", i)
                    }
                },
                mounted: function () {
                    this.gauge = this.gauge(this.$el.querySelector("#power-gauge"), {
                        size: 130,
                        clipWidth: 130,
                        clipHeight: 80,
                        ringWidth: 5,
                        maxValue: 100,
                        transitionMs: 1500,
                        arcColor: t.map(i["d"], this.colors)
                    }), this.gauge.render(), this.gauge.update(this.leftValue)
                }
            }
        }).call(this, n("b17e"))
    }, e391: function (t, e, n) {
        "use strict";
        (function (t) {
            n("7f7f");
            var i = n("2638"), a = n.n(i), r = n("774e"), s = n.n(r), o = (n("7514"), n("cebc")), c = n("75fc"),
                u = n("768b"), l = (n("c5f6"), n("c082")), f = n("1980"), d = n.n(f), h = n("e811"), p = n("55de"),
                b = n("2bd2"), v = n("a748"), _ = n("7738"), g = n("6e77"), m = n("bc0c"), y = n("808d"), x = n("1585"),
                w = n("69dd"), O = n("ebb6"), M = n("5670"), C = n("742d"), k = n("a744"), A = n("c4cc"), T = n("4677"),
                N = n("3e18"), P = n("fa7d"), S = n("a772"), $ = n("0d8e"), E = (n("6a20"), n("4c6b")),
                j = t.compose(t.not, t.isNil);
            e["a"] = {
                name: "InstagramCarousel",
                mixins: [$["a"]],
                mounted: function () {
                    var t = this;
                    this.$nextTick(function () {
                        return t.init()
                    })
                },
                data: function () {
                    return {channel: "instagram", move: 0, navClick$: new b["a"]}
                },
                props: {
                    value: {
                        type: Object, default: function () {
                            return {
                                adType: "link",
                                channel: "instagram",
                                enabled: !1,
                                creative: {
                                    cards: [{
                                        url: "",
                                        meta: "",
                                        media: "",
                                        title: "",
                                        appStoreUrl: "",
                                        description: "",
                                        playStoreUrl: ""
                                    }, {
                                        url: "",
                                        meta: "",
                                        media: "",
                                        title: "",
                                        appStoreUrl: "",
                                        description: "",
                                        playStoreUrl: ""
                                    }], mainUrl: "", mainDescription: ""
                                },
                                executable: {value: !1, reason: {type: "invalid", paramName: "url"}},
                                creativeType: "carousel"
                            }
                        }
                    }
                },
                render: function () {
                    var e = this, n = arguments[0], i = this.editMode, a = ["onboarding-ad-container", "instagram"];
                    return this.editable || a.push("not-editable"), n("div", {
                        class: this.classJoin(a),
                        ref: "elem"
                    }, [n("div", {class: "in-instagram-carousel-container"}, [n("div", {class: "carousel--body"}, [n("div", {
                        class: "top-header",
                        on: {
                            click: function () {
                                return e.$emit("click:profile")
                            }
                        }
                    }, [n("img", {attrs: {src: this.profileImg}}), n("div", {class: "infos"}, [n("span", {class: "infos--id"}, [this.profileName]), n("span", {class: "infos--sponsor"}, ["Sponsored"])])]), this.renderEditBox(i), n("div", {class: "swiper ".concat(this.idxClass)}, [n("button", {
                        class: "prev",
                        on: {
                            click: function () {
                                return e.navClick$.next({value: t.dec(e._cardIndex)})
                            }
                        }
                    }, [n("span", {class: "adriel-flex-center arrow-left"}, [n("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "chevron-left"
                        }
                    })])]), n("button", {
                        class: "next", on: {
                            click: function () {
                                return e.navClick$.next({value: t.inc(e._cardIndex)})
                            }
                        }
                    }, [n("span", {class: "adriel-flex-center arrow-right"}, [n("b-icon", {
                        attrs: {
                            pack: "far",
                            icon: "chevron-right"
                        }
                    })])]), n("ul", {class: "swiper__list", style: this.listStyle}, [this.cards.map(function (t, n) {
                        return e.renderCard(i, t, n)
                    })])]), this.renderNavigation(i), this.renderSeeMore(i), n("div", {class: "carousel--contents-divider"}), this.renderAdditionals(i), this.renderBottom(i)])])])
                },
                methods: {
                    renderCard: function (t, e, n) {
                        var i = this, r = this.$createElement, s = e.media, o = e.meta;
                        o = void 0 === o ? {} : o;
                        var c = o.media, u = void 0 === c ? {} : c, l = this.containerWidth + "px", f = l,
                            d = this.getParamInfo("media"), p = d.error, b = {
                                attrs: {
                                    src: s,
                                    meta: u,
                                    type: "media",
                                    editMode: t,
                                    campaignId: this.campaignId,
                                    isLoading: this.isLoading,
                                    validations: Object(S["l"])(this.value, e),
                                    error: this.dirty && p,
                                    channel: "instagram"
                                }, on: {
                                    click: function () {
                                        return i.setMode(!0)
                                    },
                                    complete: this.carouselMediaNext("carousel:edit", "media", n).bind(this),
                                    remove: function () {
                                        return i.updateCard(n, "media", void 0)
                                    }
                                }
                            };
                        return r("li", {
                            class: "swiper__item",
                            style: {width: l}
                        }, [r("div", {class: "product-card"}, [r("div", {
                            class: "product-card__inner",
                            style: {height: f}
                        }, [r("div", {
                            class: "product-card__inner-pic",
                            style: {width: l, height: f}
                        }, [r(h["a"], a()([{}, b]))])])])])
                    }, init: function () {
                        var e = this,
                            n = this.$watchAsObservable("_cardIndex", {immediate: !0}).pipe(Object(x["a"])("newValue"), Object(w["a"])(), Object(O["a"])(t.always)),
                            i = this.navClick$.pipe(Object(O["a"])(function () {
                                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                return t.target ? t.target.getAttribute("data-index") : t.value
                            }), Object(M["a"])(j), Object(O["a"])(t.compose(t.always, Number))),
                            a = Object(v["a"])(n, i).pipe(Object(C["a"])(function (t, e) {
                                return e(t)
                            }, 0), Object(k["a"])(0), Object(w["a"])(), Object(O["a"])(function (n) {
                                return t.clamp(0, e.cards.length - 1, n)
                            }), Object(A["a"])(function (t) {
                                return e._cardIndex = t
                            })),
                            r = this.$watchAsObservable("containerWidth", {immediate: !0}).pipe(Object(x["a"])("newValue")),
                            s = Object(_["a"])(a, r).pipe(Object(O["a"])(function (e) {
                                var n = Object(u["a"])(e, 2), i = n[0], a = n[1];
                                return -t.multiply(a, i)
                            }));
                        this.$subscribeTo(s, function (t) {
                            return e.move = t
                        }, function (t) {
                            return console.log(t)
                        })
                    }, renderDescription: function (t) {
                        var e = this, n = this.$createElement, i = this.getParamInfo("mainDescription"), a = i.value,
                            r = i.error, s = i.reason;
                        return t ? n(l["a"], {
                            class: "carousel--description-input",
                            attrs: {
                                type: "textarea",
                                value: a,
                                error: this._dirty && r ? this.$t("COMMON.ad_validation_".concat(s)) : "",
                                autofocus: this.autofocus,
                                placeholder: this.$t("COMMON.facebook_feed_description_placeholder")
                            },
                            on: {input: this.updateCreative("mainDescription").bind(this)}
                        }) : n("div", {
                            class: "carousel--description hover-edit", on: {
                                click: function () {
                                    return e.setMode(!0)
                                }
                            }
                        }, [a])
                    }, renderEditBox: function (t) {
                        var e = this, n = this.$createElement;
                        if (t) return n("div", {class: "carousel--edit-area"}, [n("button", {
                            class: "adriel-flex-center remove-btn",
                            on: {
                                click: function () {
                                    return e.deleteCard(e._cardIndex)
                                }
                            }
                        }, [this.$t("COMMON.ads_delete_slide")]), this.cards.length < 10 && n("button", {
                            class: "adriel-flex-center add-btn",
                            on: {
                                click: function () {
                                    var t = e.showImageTemplate();
                                    t.$on("complete", e.mediaNext("carousel:add", "media").bind(e))
                                }
                            }
                        }, [n("b-icon", {attrs: {pack: "far", icon: "plus"}}), this.$t("COMMON.ads_add_slide")])])
                    }, renderNavigation: function (e) {
                        var n = this, i = this.$createElement;
                        if (e) return i(d.a, {
                            attrs: {
                                list: Object(c["a"])(this.cards),
                                disabled: this.cards.length < 2,
                                animation: "250"
                            },
                            on: {change: this.handleSort},
                            class: "adriel-flex-center carousel--navigation-container",
                            nativeOn: {
                                click: function (t) {
                                    return n.navClick$.next(t)
                                }
                            }
                        }, [this.cards.map(function (e, a) {
                            var r = ["adriel-flex-center", "nav-btn"];
                            return n._cardIndex === a && r.push("selected"), i("button", {
                                class: n.classJoin(r),
                                attrs: {"data-index": a}
                            }, [t.inc(a)])
                        })])
                    }, deleteCard: function (e) {
                        var n = Object(S["j"])(this.value).removeGroup(Object(o["a"])({}, this.value), e);
                        this._value = n;
                        var i = t.path(["creative", "cards"], n);
                        i && this.navClick$.next({value: t.clamp(0, t.dec(i.length), this._cardIndex)})
                    }, renderUrlField: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n;
                        if ("WEB" !== i) return null;
                        var r = this.getParamInfo("url", t), s = r.value, o = r.error, c = r.reason, u = [];
                        return e(l["a"], a()([{}, E["n"], {
                            class: u,
                            attrs: {
                                value: s,
                                error: this._dirty && o ? this.$t("COMMON.ad_validation_".concat(c)) : "",
                                placeholder: this.$t("COMMON.ad_landing_url_placeholder")
                            },
                            on: {input: this.updateCard(t, "url").bind(this)}
                        }]))
                    }, renderSeeMore: function (e) {
                        var n, i = this, a = this.$createElement;
                        if (e) {
                            var r = this._cardIndex;
                            n = a("div", {class: "see-more-edit-mode"}, [this.renderCta(r), this.renderUrlField(r), this.renderAppStoreUrl(r), this.renderPlayStoreUrl(r), this.renderOptionalUrl(r)])
                        } else {
                            var s = t.path(["cards", this._cardIndex, "callToAction"], this);
                            s = t.compose(t.prop("text"), t.defaultTo({}), t.find(t.propEq("value", s)))(this.ctaOptions), n = a("span", {
                                on: {
                                    click: function (t) {
                                        t.stopPropagation(), i.setMode(!0), i.handleDropdownClick()
                                    }
                                }
                            }, [s, a("b-icon", {attrs: {pack: "fal", icon: "chevron-right"}})])
                        }
                        var o = ["adriel-flex-center"];
                        return e ? o.push("carousel--see-more-input") : o.push("carousel--see-more"), a("div", {class: this.classJoin(o)}, [n])
                    }, renderAdditionals: function (e) {
                        var n = this, i = this.$createElement;
                        return i("div", {class: "carousel--additionals"}, [i("div", {class: "left-part"}, [s()({length: 3}).map(function (e, n) {
                            return i("img", {attrs: {src: "/img/onboarding/instagram_icon_".concat(t.inc(n), ".png")}})
                        })]), !e && i("div", {class: "adriel-flex-center middle-part"}, [this.cards.map(function (t, e) {
                            var a = ["indicator"];
                            return e === n._cardIndex && a.push("selected"), i("span", {class: n.classJoin(a)})
                        })]), i("div", {class: "right-part"}, [i("img", {attrs: {src: "/img/onboarding/instagram_icon_4.png"}})])])
                    }, renderBottom: function (e) {
                        var n = this, i = this.$createElement,
                            r = t.path(["_value", "creative", "mainDescription"], this), s = this._cardIndex,
                            o = t.path(["cards", s, "title"], this);
                        if (e) return i("div", {class: "carousel--bottom-container input-type"}, [i("div", {class: "adriel-flex-center top"}, [i(l["a"], a()([{}, E["n"], {
                            attrs: {
                                value: o,
                                autofocus: this.autofocus,
                                limit: 40,
                                placeholder: this.$t("COMMON.instagram_carousel_title_placeholder")
                            }, on: {input: this.updateCard(s, "title").bind(this)}
                        }]))]), i("div", {class: "bottom"}, [this.renderDescription(e)])]);
                        var u = function () {
                                return n.setMode(!0)
                            }, f = {directives: [{name: "line-clamp", value: 2}], on: {click: u}},
                            d = ["contents-area hover-edit"];
                        return d = this.addClassIfEmpty(r, d), i("div", {class: "carousel--bottom-container"}, [o && i("span", {
                            class: [].concat(Object(c["a"])(d), ["adriel-display-block"]),
                            on: {click: u}
                        }, [this.valueOrPlaceholder(o)]), i("span", a()([{class: d}, f]), [this.valueOrPlaceholder(r)])])
                    }, renderCta: function () {
                        var e = this.$createElement, n = this._cardIndex,
                            i = t.path(["cards", n, "callToAction"], this);
                        return e(p["a"], {
                            class: "cta-btn",
                            attrs: {isEditMode: !0, selected: i, options: this.ctaOptions},
                            ref: "cta",
                            on: {input: this.updateCard(n, "callToAction").bind(this)}
                        })
                    }, renderAppStoreUrl: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n;
                        if ("APP" !== i) return null;
                        var r = [], s = this.getParamInfo("appStoreUrl", t), o = s.value, c = s.error, u = s.reason;
                        return this._dirty && c && r.push("adriel-field-error"), e(l["a"], a()([{}, E["n"], {
                            class: r,
                            attrs: {
                                value: o,
                                error: this._dirty && c ? this.$t("COMMON.ad_validation_".concat(u)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_appstore_url")
                            },
                            ref: "appStoreUrl",
                            on: {input: this.updateCreative("appStoreUrl").bind(this)}
                        }]))
                    }, renderPlayStoreUrl: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n;
                        if ("APP" !== i) return null;
                        var r = ["playstore-url"], s = this.getParamInfo("playStoreUrl", t), o = s.value, c = s.error,
                            u = s.reason;
                        return this._dirty && c && r.push("adriel-field-error"), e(l["a"], a()([{}, E["n"], {
                            class: r,
                            attrs: {
                                value: o,
                                error: this._dirty && c ? this.$t("COMMON.ad_validation_".concat(u)) : "",
                                placeholder: this.$t("PROPOSAL.ad_config_playstore_url")
                            },
                            ref: "playStoreUrl",
                            on: {input: this.updateCreative("playStoreUrl").bind(this)}
                        }]))
                    }, renderOptionalUrl: function (t) {
                        var e = this.$createElement, n = this._type, i = void 0 === n ? "" : n, r = this.urlVisible;
                        if ("APP" !== i || !r) return null;
                        var s = this.getParamInfo("url", t), o = s.value;
                        return e(l["a"], a()([{}, E["n"], {
                            attrs: {
                                value: o,
                                placeholder: "".concat(this.$t("COMMON.ad_landing_url_placeholder"), " (").concat(this.$t("COMMON.optional"), ")")
                            }, ref: "OptionalUrl", on: {input: this.updateCard(t, "url").bind(this)}
                        }]))
                    }
                },
                subscriptions: function () {
                    var t = this,
                        e = Object(v["a"])(Object(g["a"])(window, "resize").pipe(Object(T["a"])(200)), Object(m["a"])(0, 5).pipe(Object(N["a"])(function (t) {
                            return Object(y["a"])(250 + 600 * t)
                        }))).pipe(Object(O["a"])(function () {
                            return Object(P["S"])(t.$refs.elem, !0) || 0
                        }));
                    return {containerWidth: e}
                },
                computed: {
                    length: t.path(["cards", "length"]),
                    cards: t.pathOr([], ["value", "creative", "cards"]),
                    listStyle: function () {
                        return {transform: "translateX(".concat(this.move, "px)")}
                    },
                    idxClass: function () {
                        var e = this._cardIndex;
                        return 0 === e ? "first" : e === t.dec(this.length) ? "last" : "normal"
                    },
                    profileName: function () {
                        var e = t.path(["facebookProfile", "instagram"], this);
                        return e ? e.username || e.name : t.pathOr(this.$t("ONBOARDING.instagram_feed_id"), ["facebookProfile", "name"], this)
                    },
                    profileImg: function () {
                        var e = t.path(t.__, this), n = e(["facebookProfile", "instagram", "profile_pic"]),
                            i = e(["facebookProfile", "logo"]);
                        return n || i || "/img/onboarding/fb_profile.png"
                    }
                }
            }
        }).call(this, n("b17e"))
    }, e6b3: function (t, e, n) {
        "use strict";
        (function (t) {
            var i = n("3de4"), a = n("2c47");
            e["a"] = {
                mixins: [i["a"]], created: function () {
                }, data: function () {
                    return {images: ["/img/proposal/proposal_income_icon1.png", "/img/proposal/proposal_income_icon2.png", "/img/proposal/proposal_income_icon3.png", "/img/proposal/proposal_income_icon4.png"]}
                }, methods: {
                    getImgSize: function (e) {
                        return t.path(["values", e, "value"], this) ? {width: "50px", height: "62px"} : {
                            width: "40px",
                            height: "52px"
                        }
                    }
                }, props: {}, components: {"on-off-view": a["a"]}
            }
        }).call(this, n("b17e"))
    }, eafc: function (t, e, n) {
        var i = n("4a68"), a = n("100e"), r = n("b4b0"), s = a(function (t, e, n) {
            return i(t, r(e) || 0, n)
        });
        t.exports = s
    }, ec2d: function (t, e, n) {
    }, eda3: function (t, e, n) {
    }, f087: function (t, e, n) {
        "use strict";
        n.d(e, "h", function () {
            return _
        }), n.d(e, "e", function () {
            return g
        }), n.d(e, "g", function () {
            return m
        }), n.d(e, "d", function () {
            return y
        }), n.d(e, "f", function () {
            return x
        }), n.d(e, "c", function () {
            return w
        }), n.d(e, "a", function () {
            return O
        }), n.d(e, "b", function () {
            return M
        });
        var i = n("aede"), a = n("2b0e"), r = n("9c56"), s = n("288f");

        function o() {
            var t = Object(i["a"])(["\n    ", "\n    font-size: 16px;\n    color: #333333;\n    margin-left: 10px;\n    @media screen and (max-width: 800px) {\n        font-size: 14px;\n    }\n"]);
            return o = function () {
                return t
            }, t
        }

        function c() {
            var t = Object(i["a"])([""]);
            return c = function () {
                return t
            }, t
        }

        function u() {
            var t = Object(i["a"])(["\n    display: flex;\n    align-items: center;\n    margin-top: 15px;\n"]);
            return u = function () {
                return t
            }, t
        }

        function l() {
            var t = Object(i["a"])([""]);
            return l = function () {
                return t
            }, t
        }

        function f() {
            var t = Object(i["a"])(["\n    width: calc(50% - 15px);\n    height: 150px;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    margin: 15px 0;\n    @media screen and (max-width: 800px) {\n        width: calc(50% - 7.5px);\n    }\n"]);
            return f = function () {
                return t
            }, t
        }

        function d() {
            var t = Object(i["a"])(["\n    display: flex;\n    flex-wrap: wrap;\n    justify-content: space-between;\n    margin: 25px 0 -15px 0;\n    width: 100%;\n    @media screen and (max-width: 800px) {\n        margin: 25px 0 -7.5px 0;\n    }\n"]);
            return d = function () {
                return t
            }, t
        }

        function h() {
            var t = Object(i["a"])(["\n    ", "\n    font-size: 20px;\n    text-align: center;\n    color: #333333;\n    @media screen and (max-width: 800px) {\n        font-size: 18px;\n    }\n"]);
            return h = function () {
                return t
            }, t
        }

        function p() {
            var t = Object(i["a"])(["\n    ", "\n    max-width: 750px;\n    margin: 0 auto;\n    border-radius: 5px;\n    background-color: white;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    @media screen and (max-width: 800px) {\n        width: 90vw;\n    }\n"]);
            return p = function () {
                return t
            }, t
        }

        function b() {
            var t = Object(i["a"])(["\n    font-family: 'Rubik', 'sans-serif', 'Noto Sans';\n"]);
            return b = function () {
                return t
            }, t
        }

        var v = Object(r["a"])(b()), _ = r["b"].div(p(), v), g = r["b"].span(h(), v), m = r["b"].div(d()),
            y = Object(r["b"])(a["default"].component("ads-selection-box", s["a"]))(f()), x = r["b"].img(l()),
            w = r["b"].div(u()), O = r["b"].img(c()), M = r["b"].span(o(), v)
    }, f252: function (t, e, n) {
    }, f3c1: function (t, e) {
        var n = 800, i = 16, a = Date.now;

        function r(t) {
            var e = 0, r = 0;
            return function () {
                var s = a(), o = i - (s - r);
                if (r = s, o > 0) {
                    if (++e >= n) return arguments[0]
                } else e = 0;
                return t.apply(void 0, arguments)
            }
        }

        t.exports = r
    }, f53a: function (t, e, n) {
    }, f71a: function (t, e, n) {
    }, f754: function (t, e, n) {
        "use strict";
        var i = function () {
                var t = this, e = t.$createElement, n = t._self._c || e;
                return n("b-modal", {
                    staticClass: "tracker-modal",
                    attrs: {active: t._active, canCancel: ["escape", "x"], width: "450px"},
                    on: {
                        "update:active": function (e) {
                            t._active = e
                        }, close: t.cancel
                    }
                }, [n("div", {ref: "wrapper", staticClass: "tracker-modal-wrapper"}, [n("div", {
                    ref: "header",
                    staticClass: "tracker-modal-header"
                }, [t._v(t._s(t.$t("PROPOSAL.tracker_modal_title")))]), n("div", {staticClass: "tracker-modal-adder-wrapper"}, [t.properties.length ? [n("div", {
                    staticClass: "page-elems",
                    style: {"max-height": t.maxHeight + "px"}
                }, t._l(t.properties, function (e) {
                    return n("div", {key: e.id, staticClass: "page-elem"}, [n("b-radio", {
                        staticClass: "custom-radio",
                        class: {selected: t.compSelectedId === e.id},
                        attrs: {"native-value": e.id},
                        on: {
                            input: function (n) {
                                return t.radioSelected(e)
                            }
                        },
                        model: {
                            value: t.compSelectedId, callback: function (e) {
                                t.compSelectedId = e
                            }, expression: "compSelectedId"
                        }
                    }, [n("span", {staticClass: "page-elem-box"}, [n("span", {staticClass: "page-name"}, [t._v(t._s(e.name))]), n("span", {staticClass: "page-url"}, [t._v(t._s(" | " + e.websiteUrl + " " + e.id))])])])], 1)
                }), 0)] : [n("div", {staticClass: "no-tracker"}, [n("span", [t._v(t._s(t.$t("PROPOSAL.tracker_modal_no_ga")))]), n("a", {
                    attrs: {
                        href: "https://support.google.com/analytics/answer/1008015",
                        target: "_blank"
                    }
                }, [t._v(t._s(t.$t("PROPOSAL.tracker_modal_no_ga_help")))])])]], 2), n("div", {staticClass: "buttons-wrapper"}, [n("button", {
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.cancel
                    }
                }, [t._v(t._s(t.$t("COMMON.cancel")))]), n("button", {
                    on: {
                        mousedown: function (t) {
                            t.stopPropagation()
                        }, click: t.confirm
                    }
                }, [t._v(t._s(t.$t("COMMON.ok")))])])])])
            }, a = [], r = n("52a1"), s = r["a"], o = (n("c82c"), n("6b11"), n("2877")),
            c = Object(o["a"])(s, i, a, !1, null, "1ebb83fb", null);
        e["a"] = c.exports
    }, f876: function (t, e, n) {
        "use strict";
        (function (t) {
            n("20d6"), n("c5f6");
            var i = n("6510"), a = n("f43e");
            e["a"] = {
                props: {
                    active: {type: Boolean, default: !1},
                    coupons: {type: Array, default: t.always([])},
                    campaignId: {type: Number | String, required: !0}
                },
                data: function () {
                    return {couponCode: "", selected: []}
                },
                components: {Coupon: a["a"], AdrielButtons: i["a"]},
                computed: {
                    _active: {
                        get: t.prop("active"), set: function (t) {
                            this.$emit("update:active", t)
                        }
                    }
                },
                methods: {
                    addCoupon: function () {
                        this.couponCode && this.$emit("update:add", this.couponCode)
                    }, confirm: function () {
                        this.$emit("update:bind", this.selected), this._active = !1
                    }, setSelected: function () {
                        this.campaignId && (this.selected = t.uniq(this.coupons.filter(t.propEq("campaign_id", this.campaignId)).map(t.prop("id")).concat(this.selected)))
                    }, getImg: function (t) {
                        var e = t.id, n = "/img/proposal/";
                        return this.isActive(e) ? n + "coupon_checkbox_active.png" : n + "coupon_checkbox.png"
                    }, isActive: function (e) {
                        return t.findIndex(t.equals(e), this.selected) > -1
                    }, toggleActive: function (e) {
                        var n = e.id;
                        this.isActive(n) ? this.selected = t.without([n], this.selected) : this.selected.push(n)
                    }
                },
                watch: {
                    coupons: function () {
                        arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                        this.couponId = "", this.setSelected()
                    }, campaignId: {
                        handler: function (t) {
                            t && this.setSelected()
                        }, immediate: !0
                    }
                }
            }
        }).call(this, n("b17e"))
    }, f9d1: function (t, e, n) {
    }, faa9: function (t, e, n) {
        "use strict";
        var i = n("8beb"), a = n.n(i);
        a.a
    }
}]);
//# sourceMappingURL=onboarding~proposal.5804c6bf.js.map