<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserSub extends Model
{
    //
    /**
     * @var string
     * table name 변경
     */
    protected $table = 'user_sub';

    /**
     * @var string
     * primary key 변경
     */
    protected $primaryKey = 'user_sub_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_master_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
//        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
//        'email_verified_at' => 'datetime',
    ];
}
