<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class AdCampaign extends Model
{
    //
    /**
     * @var string
     */
    protected $table = 'ad_campaign_info';

    /**
     * @var string
     */
    protected $primaryKey = 'ad_campaign_id';

    protected $fillable = [
        'ad_campaign_type','name','platform','user_master_id','user_sub_id','ads_id'
    ];
}
