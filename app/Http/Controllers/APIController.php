<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIController extends Controller
{
    public function getCampaign(){
        echo getGuzzleRequest('/campaign/get');
    }

    public function getAdGroup(){
        echo getGuzzleRequest('/adgroup/get');
    }
}
